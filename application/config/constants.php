<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

define('ADMIN_DIR','administrator/');
define('MSI_DIR','msi/');

define('ADMIN_SESSION','innomap_admin');
define('USER_SESSION','innomap_user');
define('MSI_SESSION','innomap_msi');

define('USER_NOT_ACTIVE',0);
define('USER_ACTIVE',1);

define('ROLE_ADMIN',1);
define('ROLE_INNOVATOR',4);
define('ROLE_EXPERT',5);
define('ROLE_MANAGER',6);
define('ROLE_DATA_ADMIN',3);
define('ROLE_IDEC_HEAD',7);
define('ROLE_DATA_ANALYST',2);
define('ROLE_BUSINESS_ANALYST',12);
define('ROLE_PROGRAM_DIRECTOR', 13);
define('ROLE_COMMITTEE', 14);
define('ROLE_HIP6_ACCOUNT_MANAGER', 15);
define('ROLE_HIP6_STAKEHOLDER', 16);

/*INNOVATOR IDENTITY*/
define('INNOVATOR_IDENTITY', serialize(array(
	1=>'Inovator Individu',
	2=>'Penerus Inovasi',
	3=>'Inovator Institusi',
	4=>'Inovator Berkumpulan'
)));
define('INNOVATOR_IDENTITY_TEAM',4);

/*INNOVATOR CITIZENSHIP*/
define('INNOVATOR_CITIZENSHIP', serialize(array(
	1=>'Warganegara',
	2=>'Bukan Warganegara'
)));

/*INNOVATOR RACE*/
define('INNOVATOR_RACE', serialize(array(
	1=>'Melayu',
	2=>'China',
	3=>'India',
	4=>'Bumiputera',
	5=>'Lain-lain'
)));

/*INNOVATOR MARITAL STATUS*/
define('INNOVATOR_MARITAL_STATUS', serialize(array(
	1=>'Bujang',
	2=>'Berkahwin',
	3=>'Duda',
	4=>'Janda'
)));

/*DEMOGRAPHIC LOCATION TYPE*/
define('DEMOGRAPHIC_LOCATION_TYPE', serialize(array(
	1=>'Bandar',
	2=>'Pinggir Bandar',
	3=>'Luar Bandar'
)));

/*DEMOGRAPHIC LOCATION TYPE*/
define('DEMOGRAPHIC_EDUCATION_LEVEL', serialize(array(
	1=>'UPSR',
	2=>'PMR',
	3=>'SPM',
	4=>'STPM',
	5=>'Diploma',
	6=>'Ijazah Sarjana Muda',
	7=>'Ijazah Sarjana',
	8=>'Ijazah Kedoktoran',
	9=>'Lain-lain',
)));

/*COMPANY FUNDING SOURCES*/
define('DEMOGRAPHIC_FUNDING_SOURCE', serialize(array(
	1=>'Sendiri',
	2=>'Pelabur',
	3=>'Geran',
	4=>'Pinjaman'
)));

/*HEIR RELATIONSHIPS*/
define('HEIR_RELATIONSHIP', serialize(array(
	1=>'Suami',
	2=>'Isteri',
	3=>'Anak',
	4=>'Adik-Beradik',
	5=>'Saudara',
	6=>'Bapa Saudara',
	7=>'Lain-lain',
	8=>'Bapa/Ibu'
)));

/*INNOVATION TARGET*/
define('INNOVATION_TARGET', serialize(array(
	1=>'Individu',
	2=>'Organisasi/Institusi',
	3=>'Peniaga'
)));

/*ATTACHMENT PATH*/
define('PATH_TO_INNOVATOR_PICTURE', 'assets/attachment/innovator_picture/');
define('PATH_TO_ARTICLE_IMAGE','assets/attachment/article/');
define('PATH_TO_INNOVATION_PICTURE', 'assets/attachment/innovation_picture/');
define('PATH_TO_PORTFOLIO', 'assets/attachment/portfolio/');
define('PATH_TO_INNOVATOR_COMPANY_CERT', 'assets/attachment/company_certificate/');
define('PATH_TO_PORTFOLIO_ASSET', 'assets/attachment/portfolio_asset/');
define('PATH_TO_EXPERT_PICTURE', 'assets/attachment/expert_picture/');
define('PATH_TO_PORTFOLIO_NOTE', 'assets/attachment/portfolio_note/');
define('PATH_TO_COMMITTEE_MEETING_ATTACHMENT', 'assets/attachment/committee_meeting/');
define('PATH_TO_INNOVATION_PICTURE_THUMB', 'assets/attachment/innovation_picture/thumbnail/');
define('PATH_TO_MARKETPLACE_SOCIAL_MEDIA_ICON', 'assets/attachment/marketplace_social_media_icon/');

define('ARTICLE_STATUS',serialize(array(
		0 => "Unpublish",
		1 => "Publish",
	)));
define('ARTICLE_UNPUBLISH',0);
define('ARTICLE_PUBLISH',1);

/*ACCESS TYPE*/
define('CANT_ACCESS', 0);
define('CAN_ACCESS', 1);

/*URI SEGMENT ADMIN*/
define('URI_ADMIN', 2);
define('URI_USER', 1);

/*URL IN ADMIN THAT CAN ACCESS*/
define('URL_ADMIN_ACCESS',serialize(array(
		0 => "administrator",
		1 => "administrator/site/login",
		2 => "administrator/site/logout",
		3 => "administrator/site/validate"
)));

/*URL IN USER THAT CAN ACCESS*/
define('URL_USER_ACCESS',serialize(array(
		0 => "site/index",
		1 => "site/login",
		2 => "site/logout",
		3 => "site/validate"
)));

/*INNOVATION STATUS*/
define('INNO_DRAFT', 0);
define('INNO_SENT_APPROVAL', 1);
define('INNO_APPROVED', 2);
define('INNO_REJECTED', 3);

define('INNO_STATUSES',serialize(array(
	0 => array('name' => "Draft", 'color' => 'black', 'class'=>'status_pending'),
	1 => array('name' => "Sent for Approval", 'color' => 'blue', 'class'=>'status_pending'),
	2 => array('name' => "Approved", 'color' => 'green', 'class'=>'status_approved'),
	3 => array('name' => "Rejected", 'color' => 'red', 'class'=> 'status_unapproved')
)));

/*Article Position on Landing Page*/
define('ARTICLE_POSITION',serialize(array(
	100 => "-",
	1 	=> 1,
	2 	=> 2,
	3 	=> 3,
	4 	=> 4,
	5 	=> 5,
	6 	=> 6,
	7 	=> 7,
)));

define('HOMEPAGE_CONTENT_TYPE',serialize(array(
		1 => 'Article',
		2 => 'Category',
		3 => 'Innovation',
	)));

define('HOMEPAGE_CONTENT_ARTICLE', 1);
define('HOMEPAGE_CONTENT_CATEGORY', 2);
define('HOMEPAGE_CONTENT_INNOVATION', 3);

/*PORTFOLIO STATUS*/
define('PORTFOLIO_TASK_DONE',1);

/*PORTFOLIO ITEMS*/
define('PORTFOLIO_ITEMS',serialize(array(
	1 => 'Penyelidikan pasaran ',
	2 => 'Perancangan perniagaan',
	3 => 'Pemfailan IP',
	4 => 'Perkongsian strategik',
	5 => 'Promosi',
))); //counter is initialized in portfolio.js file

/*Evaluation*/
define('EVALUATION_DRAFT',0);
define('EVALUATION_DONE',1);

define('MINIMUM_SCORE',0);

/*Notification Type*/
define('NOTIF_STATUS_NEW',0);
define('NOTIF_STATUS_READ',1);

/*Notification Type*/
define('NOTIF_TASK_ASSIGN', 1); // send to selected expert
define('NOTIF_NEW_INNOVATOR', 2); // send to all data admin
define('NOTIF_APPROVED_INNOVATOR', 3); //send to all idec head and innovator's creator
define('NOTIF_EVALUATION_DONE', 4); //send to task assigner
define('NOTIF_PORTFOLIO_ASSIGN', 5); //send to selected account manager
define('NOTIF_REJECTED_INNOVATOR', 6); //send to innovator's creator
define('NOTIF_APPROVED_INNOVATION', 7); //send to all idec head and innovation's creator
define('NOTIF_REJECTED_INNOVATION', 8); //send to innovation's creator
define('NOTIF_PORTFOLIO_COMMENT', 9); //send to portfolio manager
define('NOTIF_NEW_INNOVATION', 10); // send to all data admin
define('NOTIF_PORTFOLIO_DONE', 11); //send to portfolio assigner
define('NOTIF_EDIT_INNOVATION', 12); //send to innovations's creator and aprrover 
define('NOTIF_EDIT_INNOVATOR', 13); //send to innovations's creator and aprrover 
define('NOTIF_DELETE_INNOVATION', 14); //send to innovations's creator and aprrover 
define('NOTIF_DELETE_INNOVATOR', 15); //send to innovations's creator and aprrover 
define('NOTIF_DELETE_TASK', 16); //send to innovations's creator and aprrover 
define('NOTIF_DELETE_PORTFOLIO', 17); //send to innovations's creator and aprrover 
define('NOTIF_COMMITTEE_MEETING', 18); //send notes meeting to committee from project

define('NOTIF_TITLE',serialize(array(
	1 => "assigned you a task ",
	2 => "added new Innovator ",
	3 => "approved Innovator ",
	4 => " has been evaluated Innovation ",
	5 => "assigned you a portfolio ",
	6 => "rejected Innovator ",
	7 => "approved Innovation ",
	8 => "rejected Innovation ",
	9 => "commented on portfolio ",
	10 => "asked you to approve innovation ",
	11 => " has been uploaded Portfolio ",
	12 => "changed innovation",
	13 => "changed innovator",
	14 => "deleted innovation",
	15 => "deleted innovator",
	16 => "deleted task",
	17 => "deleted portfolio",
	18 => "added new committee meeting",
)));

define('NOTIF_TITLE_MELAYU',serialize(array(
	1 => "menyerahkan anda tugas ",
	2 => "menambahkan Inovator baru ",
	3 => "meluluskan Innovator ",
	4 => " telah selesai mengisi borang penilaian Inovasi ",
	5 => "menyerahkan anda portfolio ",
	6 => "menolak Innovator ",
	7 => "meluluskan Inovasi ",
	8 => "menolak Inovasi ",
	9 => "mengomentari portfolio ",
	10 => "meminta anda untuk meluluskan inovasi ",
	11 => " telah memuat naik Portfolio ",
	12 => "telah mengubah inovasi",
	13 => "telah mengubah inovator",
	14 => "telah memadam inovasi",
	15 => "telah memadam inovator",
	16 => "telah memadam tugasan",
	17 => "telah memadam portfolio",
	18 => "mesyuarat jawatankuasa baru ditambah"
)));

define('NOTIF_CLASS',serialize(array(
	1 => "assign",
	2 => "add",
	3 => "approve",
	4 => "approve",
	5 => "assign",
	6 => "",
	7 => "approve",
	8 => "",
	9 => "edit",
	10 => "assign",
	11 => "",
	12 => "edit",
	13 => "edit",
	14 => "",
	15 => "",
	16 => "",
	17 => "",
	18 => "add",
)));

/*Language*/
define('LANGUAGE_MELAYU','melayu');
define('LANGUAGE_ENGLISH','english');

/*Innovator has business*/
define('INNOVATOR_HAS_BUSINESS',1);
define('INNOVATOR_HASNT_BUSINESS',2);

/*Portfolio asset type*/
define('PORTFOLIO_ASSET_TYPE',serialize(array(
	1 => "Article",
	2 => "Video",
	3 => "Audio",
	4 => "Social Media",
	5 => "Images",
	6 => "Other"
)));

/*Locator*/
define('LOCATOR_CONTENT_TYPE', serialize(array(
	1=>'Innovation',
	2=>'University',
	3=>'Subject Matter Expert',
	4=>'Project'
)));

define('LOCATOR_CONTENT_TYPE_MELAYU', serialize(array(
	1=>'Inovasi',
	2=>'Universiti',
	3=>'Perkara Pakar',
	4=>'MSI'
)));

/*CONTENT TYPE*/
define('LOCATOR_INNOVATION_TYPE', 1);
define('LOCATOR_UNVERSITY_TYPE', 2);
define('LOCATOR_EXPERT_TYPE', 3);
define('LOCATOR_PROJECT_TYPE', 4);

/*TYPE COMMERCIAL CENTER*/
define('COMMERCIAL_TYPE', serialize(array(
	1=>'UTC',
	2=>'RTC'
)));

define('NOTIF_PER_PAGE',10);

define('REPORT_YEAR_MIN','2008');

/*DATATABLES NAME*/
define('DATATABLES_STORAGE_NAME','Datatables_innomap_q2dlUiIYB');

/*INNOVATION DATA SOURCE*/
define('DATA_SOURCES', serialize(array(
	1=>'Jejak Inovasi',
	2=>'HIP6',
	3=>'Others'
)));

define('DATA_SOURCE_JEJAK_INOVASI', 1);
define('DATA_SOURCE_HIP6', 2);
define('DATA_SOURCE_OTHERS', 3);

//API
// define('BIZCON_API_URL','http://mobilus.no-ip.org/card-scanner/api/ibme/');
define('INNOMAP_API_URL','http://bizcontineo.com/api/innomap/');
// define('INNOMAP_API_URL','http://localhost/card-scanner/api/innomap/');
/* End of file constants.php */
/* Location: ./application/config/constants.php */