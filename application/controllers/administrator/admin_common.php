<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Admin_common extends CI_Controller {
	function __construct($module=null) {
		parent::__construct();
		
		$this->load->model(array('admin_session'));
		$this->load->helper('language');
		$this->session->set_userdata('language', LANGUAGE_MELAYU);

		$user = $this->admin_session->get();
		$this->user_data = $user;
		if (!$user){
			if($module != 'site'){
				redirect(site_url(ADMIN_DIR."site/no_access/" ));
			}
			$this->is_logged_in = false;
		}else{
			$this->is_logged_in = true;
		}

		$this->layout = "admin";
		$data_head = array('user' => $user,
						   'lang' =>  $this->session->userdata('language'));
							
		$this->parts['header'] = $this->load->view(ADMIN_DIR.'partial/header', $data_head, true);
		$this->parts['footer'] = $this->load->view(ADMIN_DIR.'partial/footer');

		//language
		if($this->session->userdata('language') == NULL){
			$this->session->set_userdata('language', 'melayu');
		}

		//set language
		$this->inno_language = $this->session->userdata("language");
		$this->lang->load('inno',$this->inno_language);
	}

	function set_lang($lang){
		$this->load->library('user_agent');
		$this->session->set_userdata('language', $lang);
		redirect($this->agent->referrer());
	}
}