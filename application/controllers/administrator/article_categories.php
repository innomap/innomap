<?php 
require_once('admin_common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Article_categories extends Admin_common {
	
	function __construct() {
		parent::__construct("site");
		
		$this->meta 			= array();
		$this->scripts 			= array('tiny_mce/tiny_mce','mce_loader');
		$this->styles 			= array();
		$this->title 			= "";
		$this->load->model(array('article_category'));
	}
	
	public function index(){
		$data = array('categories' => $this->article_category->get_list()->result_array(),
					  'status' => unserialize(ARTICLE_STATUS),
					  'message'	=> $this->session->flashdata('success_msg'));
		$this->load->view(ADMIN_DIR.'article_category/index',$data);
	}

	function add(){
		$data = array('mode' => 'ADD',
					  'status' => unserialize(ARTICLE_STATUS),
					);
		$this->load->view(ADMIN_DIR.'article_category/form',$data);	
	}

	function edit($category_id){
		$data = array('mode' => 'EDIT',
					  'category' => $this->article_category->get(array('article_category_id' => $category_id))->row_array(),
					  'status' => unserialize(ARTICLE_STATUS),
					);
		$this->load->view(ADMIN_DIR.'article_category/form',$data);
	}

	function view($category_id){
		$data = array('mode' => 'VIEW',
					  'category' => $this->article_category->get(array('article_category_id' => $category_id))->row_array(),
					  'status' => unserialize(ARTICLE_STATUS),
					);
		$this->load->view(ADMIN_DIR.'article_category/form',$data);
	}

	function delete($category_id){
		if($category = $this->article_category->delete($category_id)){
			$this->session->set_flashdata('success_msg', 'Article category has been deleted.');
			redirect(site_url(ADMIN_DIR.'article_categories'));
		}
	}

	function save(){
		$this->layout = FALSE;
		if(isset($_POST['submit'])){
			date_default_timezone_set("Asia/Bangkok");
			$current_date = date("Y-m-d H:i:s", time());

			$mode = $this->input->post('mode');
		
			$data_post = array('name' 					=> $this->input->post('name'), 
							   'name_in_melayu' 		=> $this->input->post('name_in_melayu'), 
							   'description' 			=> $this->input->post('description'),
							   'description_in_melayu'	=> $this->input->post('description_in_melayu'),
							   'status' 				=> $this->input->post('status'),
							   );

			if($this->input->post('status') == ARTICLE_PUBLISH){
				$data_post['published_date'] = $current_date;
			}

			if($mode == 'ADD'){
				if($category_id = $this->article_category->add($data_post)){
					$this->session->set_flashdata('success_msg', 'Article category has been saved.');
				}
			}else if($mode == 'EDIT'){
				$category_id = $this->input->post('category_id');
				if($this->article_category->edit($category_id,$data_post)){
					$this->session->set_flashdata('success_msg', 'Article category has been saved.');
				}
			}
			redirect(site_url(ADMIN_DIR.'article_categories'));
		}
	}

	function update_status($category_id){
		$this->layout = FALSE;

		$category = $this->article_category->get(array('article_category_id' => $category_id))->row_array();
		date_default_timezone_set("Asia/Bangkok");
		$current_date = date("Y-m-d H:i:s", time());
		
		if($category['status'] == ARTICLE_PUBLISH){
			$data = array('status' => ARTICLE_UNPUBLISH,'published_date' => $current_date);
		}else if($category['status'] == ARTICLE_UNPUBLISH){
			$data = array('status' => ARTICLE_PUBLISH,'published_date' => $current_date);
		}
		
		if($this->article_category->edit($category_id,$data)){
			redirect(site_url(ADMIN_DIR.'article_categories'));	
		}
	}
}