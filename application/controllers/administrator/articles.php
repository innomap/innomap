<?php 
require_once('admin_common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Articles extends Admin_common {
	
	function __construct() {
		parent::__construct("articles");
		
		$this->meta 			= array();
		$this->scripts 			= array('tiny_mce/tiny_mce','mce_loader');
		$this->styles 			= array();
		$this->title 			= "";
		$this->load->model(array('article','article_category'));
	}
	
	public function index(){
		$data = array('articles' => $this->article->get_list()->result_array(),
					  'status' => unserialize(ARTICLE_STATUS),
					  'message'	=> $this->session->flashdata('success_msg'));
		$this->load->view(ADMIN_DIR.'article/index',$data);
	}

	function add(){
		$data = array('mode' => 'ADD',
					  'categories' => $this->article_category->get_list(array('status' => ARTICLE_PUBLISH))->result_array(),
					  'status' => unserialize(ARTICLE_STATUS)
					);
		$this->load->view(ADMIN_DIR.'article/form',$data);	
	}

	function edit($article_id){
		$data = array('mode' => 'EDIT',
					  'article' => $this->article->get_list(array('article_id' => $article_id))->row_array(),
					  'categories' => $this->article_category->get(array('status' => ARTICLE_PUBLISH))->result_array(),
					  'status' => unserialize(ARTICLE_STATUS)
					);
		$this->load->view(ADMIN_DIR.'article/form',$data);
	}

	function view($article_id){
		$data = array('mode' => 'VIEW',
					  'article' => $this->article->get(array('article_id' => $article_id))->row_array(),
					  'categories' => $this->article_category->get()->result_array(),
					  'status' => unserialize(ARTICLE_STATUS)
					);
		$this->load->view(ADMIN_DIR.'article/form',$data);
	}

	function delete($article_id){
		if($article = $this->article->get(array('article_id' => $article_id))->row_array()){
			$this->_remove_file($article['image']);
			if($this->article->delete($article_id)){
				$this->session->set_flashdata('success_msg', 'Article has been deleted.');
				redirect(site_url(ADMIN_DIR.'articles'));
			}
		}
	}

	function save(){
		$this->layout = FALSE;
		if(isset($_POST['submit'])){
			date_default_timezone_set("Asia/Bangkok");
			$current_date = date("Y-m-d H:i:s", time());	

			$mode = $this->input->post('mode');
			$image = ($_FILES['image']['name'] != NULL ? rand().str_replace(" ","_",$_FILES['image']['name']) : '');
			$h_image = $this->input->post('h_image');

			$data_post = array('creator_id' => $this->user_data['user_id'], 
							   'title' => $this->input->post('title'),
							   'title_in_melayu' => $this->input->post('title_in_melayu'),
							   'content' => $this->input->post('content'),
							   'content_in_melayu' => $this->input->post('content_in_melayu'),
							   'article_category_id' => $this->input->post('category'),
							   'status' => $this->input->post('status'),
							   'image' => ($image != "" ? $image : ($h_image != "" ? $h_image : '')));
			if($this->input->post('status') == ARTICLE_PUBLISH){
				$data_post['published_date'] = $current_date;
			}

			if($mode == 'ADD'){
				if($article_id = $this->article->add($data_post)){
					if($image != ""){
						$this->_upload($image,PATH_TO_ARTICLE_IMAGE,'image');
					}
					$this->session->set_flashdata('success_msg', 'Article has been saved.');
				}
			}else if($mode == 'EDIT'){
				$article_id = $this->input->post('article_id');
				if($this->article->edit($article_id,$data_post)){
					if($image != ""){
						if($h_image != ""){
							$this->_remove_file($h_image);
						}
						$this->_upload($image,PATH_TO_ARTICLE_IMAGE,'image');
					}
					$this->session->set_flashdata('success_msg', 'Article has been saved.');
				}
			}
			redirect(site_url(ADMIN_DIR.'articles'));
		}
	}

	public function _upload($filename, $upload_path, $field_name) {
		$this->load->library('upload');
		$config['file_name'] = $filename;
		$config['upload_path'] = $upload_path;
		$config['allowed_types'] = 'png|jpg|gif|bmp|jpeg';
		$config['remove_spaces'] = TRUE;
		$config['overwrite'] = TRUE;
		$config['max_size']	= '500';

		$this->upload->initialize($config);
		if (!$this->upload->do_upload($field_name, true)) {
			echo $this->upload->display_errors();
			return false;
		} else {
			$upload_data = $this->upload->data();
			return $upload_data['file_name'];
		}
	}

	public function _remove_file($name){
		if($name != NULL){
			$url = "./assets/attachment/article/".$name;
			if (file_exists(realpath(APPPATH . '../assets/attachment/article/') . DIRECTORY_SEPARATOR . $name)) {
				$remove = unlink($url);
			}else{
				return false;
			}
		}
		return true;
	}

	function update_status($article_id){
		$this->layout = FALSE;
		$article = $this->article->get(array('article_id' => $article_id))->row_array();
		date_default_timezone_set("Asia/Bangkok");
		$current_date = date("Y-m-d H:i:s", time());

		if($article['status'] == ARTICLE_PUBLISH){
			$data = array('status' => ARTICLE_UNPUBLISH,'published_date' => $current_date);
		}else if($article['status'] == ARTICLE_UNPUBLISH){
			$data = array('status' => ARTICLE_PUBLISH,'published_date' => $current_date);
		}
		if($this->article->edit($article_id,$data)){
			redirect(site_url(ADMIN_DIR.'articles'));	
		}
	}
}