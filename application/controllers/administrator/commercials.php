<?php 
require_once('admin_common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Commercials extends Admin_common {
	function __construct() {
		parent::__construct('commercials');

		$this->load->model(array('commercial'));
		$this->meta 				= array();
		$this->scripts 				= array("jquery.validate","administrator/form_validation","administrator/general");
		$this->styles 				= array();
		$this->title 				= "Commercial Center";
		
	}

	public function index() {
		$data = array(  'commercials'	=> $this->commercial->get()->result_array(),
						'type'			=> unserialize(COMMERCIAL_TYPE),
						'message'		=> $this->session->flashdata('success_msg'));
		$this->load->view(ADMIN_DIR.'commercial/index',$data);
	}

	function add(){

		$data = array(	'mode' 		=> 'ADD',
						'type'		=> unserialize(COMMERCIAL_TYPE),
						'states'	=> $this->commercial->get_states()->result_array());
		$this->load->view(ADMIN_DIR.'commercial/form',$data);
	}

	function edit($id){
		$data = array('mode' 		=> 'EDIT',
					  'id'			=> $id, 
					  'type'		=> unserialize(COMMERCIAL_TYPE),
					  'commercial'	=> $this->commercial->get(array('commercial_center.commercial_id' => $id))->row_array(),
					  'states'		=> $this->commercial->get_states()->result_array());
		$this->load->view(ADMIN_DIR.'commercial/form',$data);	
	}

	function view($id){
		$data = array('mode' 		=> 'VIEW',
					  'id'			=> $id, 
					  'type'		=> unserialize(COMMERCIAL_TYPE),
					  'commercial'	=> $this->commercial->get(array('commercial_center.commercial_id' => $id))->row_array(),
					  'states'		=> $this->commercial->get_states()->result_array());
		$this->load->view(ADMIN_DIR.'commercial/form',$data);	
	}

	function delete($id){
		if($this->commercial->delete($id)){
			$this->session->set_flashdata('form_msg','Your data has been deleted.');
			redirect(site_url().ADMIN_DIR."commercials");
		}	
	}

	function save(){
		$this->layout = FALSE;
		$mode = $this->input->post('mode');
		$data = array(
					'name' 			=> $this->input->post('name'),
					'type'			=> $this->input->post('type'),
					'telp'			=> $this->input->post('telp'),
					'address' 		=> $this->input->post('address'),
					'geo_location'	=> $this->input->post('geo_location'),
					'state_id' 		=> $this->input->post('state'));

		if($mode == 'ADD'){
			$add_commercial = $this->commercial->add($data);
			if($add_commercial){
				$this->session->set_flashdata('success_msg', 'Data successfully saved');
				redirect(site_url(ADMIN_DIR.'commercials'));
			}
		}else if($mode == 'EDIT'){
			$id = $this->input->post('commercial_id');
			$edit_commercial = $this->commercial->edit($id,$data);
			if($edit_commercial){
				$this->session->set_flashdata('success_msg', 'Data successfully edited');
				redirect(site_url(ADMIN_DIR.'commercials'));
			}
		}
	}
}