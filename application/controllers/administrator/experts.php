<?php 
require_once('admin_common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Experts extends Admin_common {
	function __construct() {
		parent::__construct('experts');

		$this->load->model(array('user','expert', 'category', 'site'));
		$this->meta 				= array();
		$this->scripts 				= array("jquery.validate","front/form_validation");
		$this->styles 				= array();
		$this->title 				= "expert";
		
	}

	public function index() {
		$data = array(	'experts'		=> $this->expert->get()->result_array(),
			 		  	'message'		=> $this->session->flashdata('success_msg'),
			 		  	'categories'	=> $this->category->get()->result_array());
		$this->load->view(ADMIN_DIR.'expert/index',$data);
	}

	function add(){
		$expertise = $this->category->get()->result_array();
		$states = $this->expert->get_state()->result_array();
		$data = array(	'mode' 		=> 'ADD',
						'expertise'	=> $expertise,
						'kad_no'	=> array(),
						'states'	=> $states);
		$this->load->view(ADMIN_DIR.'expert/form',$data);
	}

	function edit($id){
		$expertise = $this->category->get()->result_array();
		$states = $this->expert->get_state()->result_array();
		$data = array('mode' 		=> 'EDIT',
					  'id'			=> $id, 
					  'expertise'	=> $expertise,
					  'expert'		=> $this->expert->get(array('expert.expert_id' => $id))->row_array(),
					  'states'		=> $states);
		$data['kad_no'] = explode("-",$data['expert']['kad_no']);
		$this->load->view(ADMIN_DIR.'expert/form',$data);	
	}

	function view($id){
		$expertise = $this->category->get()->result_array();
		$states = $this->expert->get_state()->result_array();
		$data = array('mode' 		=> 'VIEW',
					  'id'			=> $id, 
					  'expertise'	=> $expertise,
					  'expert'		=> $this->expert->get(array('expert.expert_id' => $id))->row_array(),
					  'states'		=> $states);
		$this->load->view(ADMIN_DIR.'expert/view',$data);	
	}

	function delete($id){
		if($this->expert->delete($id)){
			$this->user->delete($id);
			$this->session->set_flashdata('form_msg','Your data has been deleted.');
			redirect(site_url().ADMIN_DIR."experts");
		}	
	}

	function save(){
		$this->layout = FALSE;
		$mode = $this->input->post('mode');
		$data_user_input = array(
									'username' 	=> $this->input->post('username'),
									'password'	=> $this->input->post('password'),
									'email' 	=> $this->input->post('email'),
									'role_id' 	=> ROLE_EXPERT,
									'status' 	=> $this->input->post('status')
								);
		if($mode == 'ADD'){
			if($this->input->post('password') == ""){
				$data_user_input = array_diff_key($data_user_input,array("password" => ""));
			}

			if($this->input->post('username') == ""){
				$data_user_input = array_diff_key($data_user_input,array("username" => ""));
			}

			$user_id = $this->user->add($data_user_input);
			$id = $user_id;

		}else{

			$id = $this->input->post('expert_id');
		}

			$data_input = array(
							'name'					=> $this->input->post('name'),
							'kad_no' 				=> $this->input->post('kad_no1')."-".$this->input->post('kad_no2')."-".$this->input->post('kad_no3'),
							'expertise'				=> $this->input->post('expertise'),
							'address' 				=> $this->input->post('address'),
							'state_id'				=> $this->input->post('state'),
							'organization_address'	=> $this->input->post('organization_address'),
							'office_phone_no' 		=> $this->input->post('office_phone_no'),
							'home_phone_no' 		=> $this->input->post('home_phone_no'),
							'mobile_phone_no' 		=> $this->input->post('mobile_phone_no'),
							'highest_education' 	=> $this->input->post('highest_education'),
							'bank_account' 			=> $this->input->post('bank_account'),
							'account_no' 			=> $this->input->post('account_no')
						);

		if($_FILES['picture']['name'] != NULL){
			
			$filename 			  = $id."_".$_FILES['picture']['name'];
			$data_input['picture'] = $this->site->_upload($filename,'picture',PATH_TO_EXPERT_PICTURE);
		}
		if($mode == 'ADD'){
			$data_input['expert_id'] = $id;
			$add_expert = $this->expert->add($data_input);
			if($add_expert){
				$this->session->set_flashdata('success_msg', 'Data successfully saved');
				redirect(site_url(ADMIN_DIR.'experts'));
			}
		}else if($mode == 'EDIT'){
			$edit_expert = $this->expert->edit($id,$data_input);
			$edit_user = $this->user->edit($id,$data_user_input);
			if($edit_expert != "" && $edit_user != ""){
				$this->session->set_flashdata('success_msg', 'Data successfully edited');
				redirect(site_url(ADMIN_DIR.'experts'));
			}
		}
	}
}