<?php 
require_once('admin_common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Homepage_contents extends Admin_common {
	
	function __construct() {
		parent::__construct("homepage_contents");
		
		$this->meta 			= array();
		$this->scripts 			= array('administrator/homepage_content');
		$this->styles 			= array();
		$this->title 			= "";
		$this->load->model(array('homepage_content','article','article_category','innovation'));
	}
	
	public function index(){
		$contents = $this->homepage_content->get()->result_array();

		foreach ($contents as $key => $value) {
			$contents[$key]['content'] = $this->get_content($value);
		}
		$data = array('contents' => $contents,
					  'type' => unserialize(HOMEPAGE_CONTENT_TYPE),
					  'message'	=> $this->session->flashdata('success_msg'));
		$this->load->view(ADMIN_DIR.'homepage_content/index',$data);
	}

	function edit($id){
		$content = $this->homepage_content->get(array('homepage_content_id' => $id))->row_array();
		$content['content'] = $this->get_content($content);
		
		$data = array('mode' => 'EDIT',
					  'type' => unserialize(HOMEPAGE_CONTENT_TYPE),
					  'content' => $content);
		$this->load->view(ADMIN_DIR.'homepage_content/form',$data);
	}

	function save(){
		$this->layout = FALSE;
		$id = $this->input->post('homepage_content_id');
		$data_post = array('type' => $this->input->post('type'),
							'content_id' => $this->input->post('content_id'));
		
		if(isset($_POST['submit'])){
			if($this->homepage_content->edit($id,$data_post)){
				$this->session->set_flashdata('success_msg', 'Your data has been saved.');
			}
			redirect(site_url(ADMIN_DIR.'homepage_contents'));
		}else if($this->input->is_ajax_request()){
			if($this->homepage_content->edit($id,$data_post)){
				$response = array('status' => 1);
			}else{
				$response = array('status' => 0);
			}
			echo json_encode($response);
		}
	}

	function get_content_list(){
		$this->layout = FALSE;
		$obj = $this->input->post('obj');

		if($obj == HOMEPAGE_CONTENT_ARTICLE){
			$result = $this->article->get_list_for_site(array('status' => ARTICLE_PUBLISH))->result_array();
		}else if($obj == HOMEPAGE_CONTENT_CATEGORY){
			$result = $this->article_category->get_list_for_site(array('status' => ARTICLE_PUBLISH))->result_array();
		}else if($obj == HOMEPAGE_CONTENT_INNOVATION){
			$result = $this->innovation->get_content_list_for_site(array('innovation.status' => INNO_APPROVED))->result_array();
		}

		echo json_encode($result);
	}

	function get_content_detail($type,$content_id){
		$this->layout = FALSE;
		if($type == HOMEPAGE_CONTENT_ARTICLE){
			$result = $this->article->get_list_for_site(array('article_id' => $content_id))->row_array();
		}else if($type == HOMEPAGE_CONTENT_CATEGORY){
			$result = $this->article_category->get_list_for_site(array('article_category_id' => $content_id))->row_array();
		}else if($type == HOMEPAGE_CONTENT_INNOVATION){
			$result = $this->innovation->get_content_list_for_site(array('innovation.innovation_id' => $content_id))->row_array();
		}

		echo json_encode($result);
	}

	function get_content($value){
		if($value['type'] == HOMEPAGE_CONTENT_ARTICLE){
			$content = $this->article->get_list_for_site(array('article_id' => $value['content_id']))->row_array();
		}else if($value['type'] == HOMEPAGE_CONTENT_CATEGORY){
			$content = $this->article_category->get_list_for_site(array('article_category_id' => $value['content_id']))->row_array();
		}else if($value['type'] == HOMEPAGE_CONTENT_INNOVATION){
			$content = $this->innovation->get_content_list_for_site(array('innovation.innovation_id' => $value['content_id']))->row_array();
		}
		if(!$content){	
			$content = array('id'=> "",'title' => "", 'content' => "");
		}
		return $content;
	}
}