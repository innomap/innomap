<?php 
require_once('admin_common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Innovations extends Admin_common {
	
	function __construct() {
		parent::__construct("articles");
		
		$this->meta 			= array();
		$this->scripts 			= array('administrator/innovation');
		$this->styles 			= array();
		$this->title 			= "";
		$this->load->model(array('innovation'));
	}
	
	public function index(){
		$innovations = $this->innovation->get_list_for_admin()->result_array();
		//add score
		for($i = 0; $i < count($innovations); $i++){
			$innovations[$i]['score_average'] = $this->innovation->get_average_score($innovations[$i]['innovation_id']);
		}
		$data = array('innovations' 			=> $innovations,
					  'count_top_innovation'	=> $this->innovation->get_list_for_admin(array('top_innovation'=>1))->num_rows());
		$this->load->view(ADMIN_DIR.'innovation/index',$data);
	}

	function view($id){
		$this->load->model('category');
		
		$inno_cats = $this->innovation->get_innovation_category(array('innovation_id'=>$id));
		for($i=0;$i<count($inno_cats);$i++){
			$inno_cats[$i]['subcategories'] = $this->category->get_subcategory(array('category_id' => $inno_cats[$i]['category_id']))->result_array();
		}		
		$data['detail']			= $this->innovation->get(array('innovation_id'=>$id))->row_array();
		$data['inno_subs']		= $this->innovation->get_innovation_subcategory($id);
		$data['inno_pics']		= $this->innovation->get_innovation_picture($id);
		$data['inno_cats']		= $inno_cats;
		$data['targets']	  	= unserialize(INNOVATION_TARGET);
		$data['categories']		= $this->category->get()->result_array();	
		$data['data_sources']	= unserialize(DATA_SOURCES);
		$this->load->view(ADMIN_DIR.'innovation/detail',$data);
	}

	function set_top_innovation(){		
		$id = $this->input->post('innovation_id');
		$data = array('top_innovation'	=> $this->input->post('top_innovation'));
		$result = $this->innovation->edit($id, $data);
		echo json_encode($result);
	}

}