<?php 
require_once('admin_common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Marketplace extends Admin_common {
	
	function __construct() {
		parent::__construct("site");
		
		$this->meta 			= array();
		$this->scripts 			= array("jquery.validate","front/form_validation");
		$this->styles 			= array();
		$this->title 			= "";
		$this->load->model(array('innovation', 'open_cart'));
	}
	
	public function index(){
		$data = array('innovations' => $this->innovation->get(array('status' => INNO_APPROVED))->result_array(),
					  'message'	=> $this->session->flashdata('alert'));
		$this->load->view(ADMIN_DIR.'marketplace/index',$data);
	}

	function add($id){
		$this->layout = FALSE;

		$product = $this->open_cart->get_product(array('innovation_id' => $id))->row_array();

		if($product){
			if($product['status'] == 0){
				$this->open_cart->update_product($product['product_id'],array('status' => 1));
			}
			$this->session->set_flashdata('alert', 'Data has been added.');
		}else{
			$innovation = $this->innovation->get(array('innovation_id' => $id))->row_array();
			if($innovation){
				$product = $this->open_cart->get_product(array('innovation_id' => $id))->row_array();
				if(!$product){
					$pictures = $this->innovation->get_innovation_picture($id);
					if(count($pictures) > 0){
						$main_picture = $pictures[0]['picture'];
					}

					$innovator = $this->innovation->get_innovation_innovator(array('innovation_id' => $id));
					$datapost = array('innovation_id' => $id, 'model' => 'Innovation', 'price' => $innovation['selling_price'], 'status' => 1,'quantity' => 1,'image' => $main_picture, 'date_added' => date("Y-m-d H:i:s"), 'date_modified' => date("Y-m-d H:i:s"),'innovator_id' => $innovator[0]['innovator_id'], 'innovator' => $innovator[0]['name']);
					
					if($product_id = $this->open_cart->add_product($datapost)){
						$data_desc = array('product_id' => $product_id, 'language_id' => 1, 'name' => $innovation['name_in_melayu'], 'description' => $innovation['description_in_melayu'], 'meta_title' => $innovation['name_in_melayu']);
						$this->open_cart->add_product_desc($data_desc);

						$categories = $this->innovation->get_innovation_category(array('innovation_id' => $innovation['innovation_id']));
						foreach ($categories as $key => $value) {
							$category = $this->open_cart->get_category(array('ref_category_id' => $value['category_id']))->row_array();
							$data_cat = array('product_id' => $product_id, 'category_id' => $category['category_id']);
							$this->open_cart->add_product_to_category($data_cat);
						}

						foreach ($pictures as $key => $value) {
							$data_pic = array('product_id' => $product_id, 'image' => $value['picture']);
							$this->open_cart->add_product_image($data_pic);
						}

						$this->open_cart->add_product_to_store(array('product_id' => $product_id, 'store_id' => 0));
						$this->innovation->edit($id, array('is_mp_product' => 1));
						$this->session->set_flashdata('alert', 'Data has been added.');
					}else{
						$this->session->set_flashdata('alert', 'Error occured. Data cannot be added.');
					}		
				}else{
					$this->session->set_flashdata('alert', 'This innovation already added o marketplace.');
				}	
			}else{
				$this->session->set_flashdata('alert', 'Innovation data not found.');
			}
		}

		redirect(site_url(ADMIN_DIR.'marketplace'));
	}

	function disabled($id){
		$product = $this->open_cart->get_product(array('innovation_id' => $id))->row_array();

		if($product){
			if($product['status'] == 1){
				$this->innovation->edit($id, array('is_mp_product' => 0));
				$this->open_cart->update_product($product['product_id'],array('status' => 0));
			}
			$this->session->set_flashdata('alert', 'Data has been deleted.');
		}else{
			$this->session->set_flashdata('alert', 'Innovation not found');
		}

		redirect(site_url(ADMIN_DIR.'marketplace'));
	}
}