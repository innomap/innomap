<?php 
require_once('admin_common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Marketplace_social_medias extends Admin_common {
	function __construct() {
		parent::__construct('Marketplace_social_medias');

		$this->load->model(array('marketplace_social_media'));
		$this->meta 				= array();
		$this->scripts 				= array("jquery.validate","administrator/form_validation");
		$this->styles 				= array();
		$this->title 				= "Market Place Social Media";
		
	}

	public function index() {
		$data = array('social_medias'	=> $this->marketplace_social_media->get()->result_array(),
			 		  'message'			=> $this->session->flashdata('success_msg'));
		$this->load->view(ADMIN_DIR.'marketplace_social_media/index',$data);
	}

	function add(){
		$data = array('mode' 		=> 'ADD');
		$this->load->view(ADMIN_DIR.'marketplace_social_media/form',$data);
	}

	function edit($id){
		$data = array('mode' 		=> 'EDIT',
					  'id'			=> $id, 
					  'detail'		=> $this->marketplace_social_media->get(array('id_social_media' => $id))->row_array());
		$this->load->view(ADMIN_DIR.'marketplace_social_media/form',$data);	
	}

	function save(){
		$this->layout = FALSE;
		$this->load->model('site');
		$mode = $this->input->post('mode');
		$data_input = array(
						'name' 	=> $this->input->post('name'),
						'icon' 	=> $this->input->post('icon'),
						'url'	=> $this->input->post('url'));
		
		//upload social media icon
		if(isset($_FILES['icon'])){
			if($_FILES['icon']['name'] != NULL){
				$bef_pict = $this->input->post('bef_picture');
				if($bef_pict != NULL){
					$this->site->_remove($bef_pict,PATH_TO_MARKETPLACE_SOCIAL_MEDIA_ICON);
				}
				$rand = substr(md5(microtime()),rand(0,26),5);
				$filename = $rand."_".$_FILES['icon']['name'];
				$data_input['icon'] = $this->site->_upload($filename,'icon',PATH_TO_MARKETPLACE_SOCIAL_MEDIA_ICON);
			}
		}

		if($mode == 'ADD'){
			$social_media_id = $this->marketplace_social_media->add($data_input); 
			if($social_media_id){
				$this->session->set_flashdata('success_msg', 'Data successfully saved');
				redirect(site_url(ADMIN_DIR.'marketplace_social_medias'));
			}
		}else if($mode == 'EDIT'){
			$id = $this->input->post('id_social_media');
			if($this->marketplace_social_media->edit($id,$data_input)){
				$this->session->set_flashdata('success_msg', 'Data successfully edited');
				redirect(site_url(ADMIN_DIR.'marketplace_social_medias'));
			}
		}
	}

	function delete($id){
		if($social_media = $this->marketplace_social_media->delete($id)){
			$this->session->set_flashdata('success_msg', 'Social Media has been deleted.');
			redirect(site_url(ADMIN_DIR.'marketplace_social_medias'));
		}
	}
}