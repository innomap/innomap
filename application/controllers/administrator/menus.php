<?php
require_once('admin_common.php');

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Menus extends Admin_common {
	function __construct() {
		parent::__construct('menus');
		$this->load->library('session');
		
		$this->scripts 			= array();
		$this->styles 			= array();
		$this->meta 			= array();
		$this->title 			= "Menus";
		$this->load->model('menu');
	}

	function index(){
		$menus = $this->menu->get(array('parent_id' => NULL))->result_array();
		for($i=0;$i<count($menus);$i++){
			$menus[$i]['childs'] = $this->menu->get(array('parent_id' => $menus[$i]['menu_id']))->result_array();
		}
		
		$data = array('menus' 	 => $menus,
					  'messages' => $this->session->flashdata('form_msg'));
		$this->load->view(ADMIN_DIR.'menu/index',$data);
	}
	
	function add(){
		$this->scripts = array(ADMIN_DIR."menu");
		$data = array('mode'		 => 'ADD',
					  'parents'		 => $this->menu->get(array('parent_id' => NULL))->result_array(),
					  'menu_modules' => NULL);
		$this->load->view(ADMIN_DIR.'menu/form',$data);
	}
	
	function edit($id){
		$this->scripts = array(ADMIN_DIR."menu");
		$data = array('mode' 		 => 'EDIT',
					  'parents'		 => $this->menu->get(array('parent_id' => NULL))->result_array(),
					  'menu'		 => $this->menu->get(array('menu_id' => $id))->row_array(),
					  'menu_modules' => $this->menu->get_module(array('menu_id' => $id))->result_array());
		$this->load->view(ADMIN_DIR.'menu/form',$data);
	}
	
	function save(){
		$mode		= $this->input->post('mode');
		$data_post	= array('name' 					=> $this->input->post('name'),
							'name_in_melayu' 		=> $this->input->post('melayu_name'),
						    'parent_id' 			=> $this->input->post('parent_id') == 0 ? NULL : $this->input->post('parent_id'),
							'url' 					=> $this->input->post('url'),
							'description'			=> $this->input->post('description'),
							'description_in_melayu' => $this->input->post('description_in_melayu'),
							'order' 				=> $this->input->post('order'),
							'status'				=> 1);
							
		//saving menu					
		if($mode == 'ADD'){
			$menu_id = $this->menu->add($data_post);
			if($menu_id !=null){
				$this->session->set_flashdata('form_msg','Your data has been saved.');
			}		
		}else{
			$menu_id = $this->input->post('menu_id');
			if($this->menu->edit($menu_id,$data_post)){
				$this->session->set_flashdata('form_msg','Your data has been saved.');
			}
		}
		
		//saving module
		$module_name = $this->input->post('module_name');
		$module_url  = $this->input->post('module_url');
		$module_id	 = $this->input->post('module_id');
		for($i=0;$i<count($module_name);$i++){
			$data_module = array('menu_id' => $menu_id,
								 'name'    => $module_name[$i],
								 'url'     => $module_url[$i]);
			
			if(!isset($module_id[$i])){
				$this->menu->add_module($data_module);
			}else{
				$this->menu->edit_module($module_id[$i],$data_module);
			}
		}
		
		redirect(site_url().ADMIN_DIR."menus");
	}
	
	function delete($type,$id){
		if($type == 'menu'){
			if($this->menu->delete($id)){
				$this->session->set_flashdata('form_msg','Your data has been deleted.');
				redirect(site_url().ADMIN_DIR."menus");
			}		
		}else{
			$this->layout = false;
			if($this->menu->delete_module($id)){
				echo 'success';
			}else{
				echo 'fail';
			}
		}
	}

}