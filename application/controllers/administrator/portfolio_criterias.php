<?php 
require_once('admin_common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Portfolio_criterias extends Admin_common {
	
	function __construct() {
		parent::__construct("site");
		
		$this->meta 			= array();
		$this->scripts 			= array("jquery.validate","front/form_validation");
		$this->styles 			= array();
		$this->title 			= "";
		$this->load->model(array('portfolio_criteria'));
	}
	
	public function index(){
		$data = array('criterias' => $this->portfolio_criteria->get(array('is_deleted' => 0), 'position')->result_array(),
					  'status' => unserialize(ARTICLE_STATUS),
					  'message'	=> $this->session->flashdata('success_msg'));
		$this->load->view(ADMIN_DIR.'portfolio_criteria/index',$data);
	}

	function add(){
		$data = array('mode' => 'ADD', 'position' => $this->portfolio_criteria->get(array('is_deleted' => 0))->num_rows()+1);
		$this->load->view(ADMIN_DIR.'portfolio_criteria/form',$data);	
	}

	function edit($id){
		$data = array('mode' => 'EDIT',
					  'criteria' => $this->portfolio_criteria->get(array('criteria_id' => $id))->row_array(),
					  'id' => $id,
					  'position' => $this->portfolio_criteria->get(array('is_deleted' => 0))->num_rows()
					);
		$this->load->view(ADMIN_DIR.'portfolio_criteria/form',$data);
	}

	function view($id){
		$data = array('mode' => 'VIEW',
					  'criteria' => $this->portfolio_criteria->get(array('criteria_id' => $id))->row_array(),
					  'id' => $id,
					  'position' => $this->portfolio_criteria->get(array('is_deleted' => 0))->num_rows()
					);
		$this->load->view(ADMIN_DIR.'portfolio_criteria/form',$data);
	}

	function delete($id,$position){
		if($criteria = $this->portfolio_criteria->edit($id, array('is_deleted' => 1,'position' => -1))){
			$this->re_order_on_delete($id,$position);
			$this->session->set_flashdata('success_msg', 'Portfolio Criteria has been deleted.');
			redirect(site_url(ADMIN_DIR.'portfolio_criterias'));
		}
	}

	function save(){
		$this->layout = FALSE;
		if(isset($_POST['submit'])){
			$mode = $this->input->post('mode');

			$cur_position = $this->input->post('current_position');
			$last_position = $this->input->post('last_position');
			$position = $this->input->post('position');
		
			$data_post = array('name' 					=> $this->input->post('name_in_melayu'), 
							   'name_in_melayu' 		=> $this->input->post('name_in_melayu'), 
							   'description' 			=> $this->input->post('description_in_melayu'),
							   'description_in_melayu'	=> $this->input->post('description_in_melayu'),
							   'position'				=> $position
							   );

			if($mode == 'ADD'){
				if($criteria_id = $this->portfolio_criteria->add($data_post)){
					if($position != $last_position){
						$this->re_order_on_add($criteria_id, $position);	
					}
					$this->session->set_flashdata('success_msg', 'Portfolio Criteria has been saved.');
				}
			}else if($mode == 'EDIT'){
				$criteria_id = $this->input->post('criteria_id');
				if($this->portfolio_criteria->edit($criteria_id,$data_post)){
					if($cur_position != $data_post['position']){
						$this->re_order($criteria_id, $data_post['position'], $cur_position);	
					}
					
					$this->session->set_flashdata('success_msg', 'Portfolio Criteria has been saved.');
				}
			}
			redirect(site_url(ADMIN_DIR.'portfolio_criterias'));
		}
	}

	private function re_order($criteria_id, $new_position, $cur_position){
		//move down
		if($new_position > $cur_position){
			$re_order_rows = $this->portfolio_criteria->get(array('position <= ' => $new_position, 'position > ' => $cur_position, 'criteria_id != ' => $criteria_id))->result_array();
			foreach ($re_order_rows as $key => $value) {
				$this->portfolio_criteria->edit($value['criteria_id'],array('position' => $value['position']-1));
			}
		}else{ //move up
			$re_order_rows = $this->portfolio_criteria->get(array('position >= ' => $new_position, 'position < ' => $cur_position, 'criteria_id != ' => $criteria_id))->result_array();
			foreach ($re_order_rows as $key => $value) {
				$this->portfolio_criteria->edit($value['criteria_id'],array('position' => $value['position']+1));
			}
		}

		return true;
	}

	private function re_order_on_add($criteria_id, $position){
		$re_order_rows = $this->portfolio_criteria->get(array('position >=' => $position,'criteria_id != ' => $criteria_id))->result_array();
		foreach ($re_order_rows as $key => $value) {
			$this->portfolio_criteria->edit($value['criteria_id'],array('position' => $value['position']+1));
		}
	}

	private function re_order_on_delete($criteria_id, $position){
		$re_order_rows = $this->portfolio_criteria->get(array('position >=' => $position,'criteria_id != ' => $criteria_id))->result_array();
		foreach ($re_order_rows as $key => $value) {
			$this->portfolio_criteria->edit($value['criteria_id'],array('position' => $value['position']-1));
		}
	}
}