<?php 
require_once('admin_common.php');

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Roles extends admin_common {
	function __construct() {
		parent::__construct('roles');
		$this->load->library('session');
		$this->meta 			= array();
		$this->scripts 			= array();
		$this->styles 			= array();
		$this->load->model(array('role','menu'));
		$this->title 			= "Roles";
	}

	function index() {
		$roles = $this->role->get(array('role_id !=' => ROLE_ADMIN))->result_array();
		$menus = $this->menu->get(array('url != '=>''))->result_array();

		for($i=0;$i<count($roles);$i++){
			$access_role = array();
			for($j=0;$j<count($menus);$j++){
				$modules 	 = $this->menu->get_module(array('menu_id' => $menus[$j]['menu_id']))->result_array();
				$access_role[$j] = "";
				foreach($modules as $module){
					$role_module = $this->role->get_role_module(array('role_id' => $roles[$i]['role_id'], 'module_id' => $module['module_id']))->num_rows();
					if($role_module > 0){
						$access_role[$j] .= '<div class="access_'.$module['name'].'" title="'.$module['name'].'">&nbsp;</div>';
					}
				}
			}
			$roles[$i]['access_role'] = $access_role;
		}
		
		$data = array('roles' 		=> $roles,
					  'menus'		=> $menus,
					  'messages' 	=> $this->session->flashdata('form_msg'));
		$this->load->view(ADMIN_DIR.'role/index',$data);
	}
	
	function add(){
		$menus = $this->menu->get(array('url !=' => ''))->result_array();
		for($i=0;$i<count($menus);$i++){
			$menus[$i]['module'] = $this->menu->get_module(array('menu_id' => $menus[$i]['menu_id']))->result_array();
		}
		$data = array('mode' 	 => 'ADD',
					  'messages' => "",
					  'menus' 	 => $menus);
		$this->load->view(ADMIN_DIR.'role/form',$data);
	}
	
	function edit($id){
		$role  = $this->role->get(array('role_id' => $id))->row_array();
		$menus = $this->menu->get(array('url !=' => ''))->result_array();
		for($i=0;$i<count($menus);$i++){
			$modules = $this->menu->get_module(array('menu_id' => $menus[$i]['menu_id']))->result_array();
			for($j=0;$j<count($modules);$j++){
				$modules[$j]['is_accessable'] = $this->role->get_role_module(array('role_id' => $id, 'module_id' => $modules[$j]['module_id']))->num_rows() > 0;
			}
			$menus[$i]['module'] = $modules;
		}
		
		$data = array('mode' 	 => 'EDIT',
					  'role' 	 => $role,
					  'messages' => $this->session->flashdata('form_msg'),
					  'menus'    => $menus);
		$this->load->view(ADMIN_DIR.'role/form',$data);
	}
	
	function save(){
		$mode		= $this->input->post('mode');
		$modules 	= $this->input->post('module_id');
		$role_data 	= array('name'					=> $this->input->post('name'),
							'name_in_melayu'		=> $this->input->post('name_in_melayu'),
							'description'			=> $this->input->post('description'),
							'description_in_melayu'	=> $this->input->post('description_in_melayu'));
							
		//saving role
		if($mode == 'ADD'){
			$role_id = $this->role->add($role_data);
		}else{
			$role_id = $this->input->post('role_id');
			$this->role->edit($role_id,$role_data);
		}
		
		//saving role module
		if($this->role->delete_role_module(array('role_id' => $role_id))){
			foreach($modules as $module){
				$this->role->add_role_module(array('module_id' => $module, 'role_id' => $role_id));
			}
			
			$this->session->set_flashdata('form_msg','Your data has been saved.');
			redirect(base_url().ADMIN_DIR."roles");
		}
	}
	
	function delete($id){
		if($this->role->delete($id)){
			$this->session->set_flashdata('form_msg','Your data has been deleted.');
		}
		redirect(site_url(ADMIN_DIR.'roles'));
	}


}

