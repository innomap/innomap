<?php 
require_once('admin_common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends Admin_common {
	
	function __construct() {
		parent::__construct("site");
		
		$this->meta 			= array();
		$this->scripts 			= array();
		$this->styles 			= array();
		$this->title 			= "";
		$this->load->model(array('admin_session'));
	}
	
	public function index(){
		if ($user = $this->admin_session->get()) {
			$data['user'] = $user;
			$this->load->view(ADMIN_DIR."site/index",$data);
		}else{
			redirect(site_url(ADMIN_DIR."site/login"));
		}
	}

	function login() {
		$data['message'] = $this->session->flashdata('login_msg');
		$this->load->view(ADMIN_DIR."site/login",$data);
	}
	
	function validate() {
		$this->layout = false;
		$login_success = false;
		if ($this->input->post("submit")) {
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			
			if (isset($username) && isset($password)) {
				if ($this->admin_session->create($username, $password)) {
					$login_success = true;
				}
			}
		}
		if(!$login_success){
			$this->session->set_flashdata('login_msg', 'Access denied. Incorrect username/password');
			redirect(site_url(ADMIN_DIR.'site/login'));
		}else{
			$this->session->set_flashdata('login_msg', 'Login Success');
			redirect(site_url(ADMIN_DIR.'site'));
		}
		echo json_encode($response);
	}

	function logout() {
		$this->admin_session->clear();
		redirect(site_url(ADMIN_DIR));
	}
	
	function no_access(){
		redirect(site_url(ADMIN_DIR));
	}

	/*function add_all_data_ontology(){
		$this->scripts 	= array("front/semantic_ontology");
		$this->load->view(ADMIN_DIR.'site/testing_ontology');
	}

	function get_all_data_innovator(){
		$this->layout = false;
		$this->load->model(array('innovator'));
		$innovators = $this->innovator->get(array('status'=>INNO_APPROVED))->result_array();
		echo json_encode($innovators);
	}

	function get_all_data_innovation(){
		$this->layout = false;
		$this->load->model(array('innovation'));
		$innovations = $this->innovation->get(array('status'=>INNO_APPROVED))->result_array();
		echo json_encode($innovations);
	}*/
}