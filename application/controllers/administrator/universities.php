<?php
require_once('admin_common.php');

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Universities extends Admin_common {

	function __construct() {
		parent::__construct('university');

		$this->load->model(array('university'));
		$this->scripts 			= array();
		$this->styles 			= array();
		$this->meta 			= array();
		$this->title 			= "Universities";		
	}

	function index(){
		$data = array('universities' => $this->university->get_universities()->result_array(),
					  'message' 	 => $this->session->flashdata('form_msg'));
		$this->load->view(ADMIN_DIR.'university/index',$data);
	}

	function add(){
		$this->load->model(array('category','site'));
		$data = array('mode' 		=> 'ADD',
					  'specialties'	=> $this->category->get()->result_array(),
					  'states' 		=> $this->site->get_state());
		$this->load->view(ADMIN_DIR.'university/form',$data);
	}

	function edit($id){
		$this->load->model(array('category','site'));
		$data = array('mode' 		 => 'EDIT',
					  'university'   => $this->university->get(array('university_id' => $id))->row_array(),
					  'specialties'  => $this->category->get()->result_array(),
					  'states' 		 => $this->site->get_state());
		$this->load->view(ADMIN_DIR.'university/form',$data);
	}

	function save(){
		$mode		= $this->input->post('mode');
		$data_post 	= array('university_name' => $this->input->post('university_name'),
							'address'		  => $this->input->post('address'),
							'specialty'		  => $this->input->post('specialty'),
							'state'			  => $this->input->post('state'),
							'contact_no'	  => $this->input->post('contact_no'),
							'geo_location'	  => $this->input->post('geo_location'));
					
		if($mode == 'ADD'){
			$success = $this->university->add($data_post);
		}else{
			$success = $this->university->edit($this->input->post('university_id'),$data_post);
		}
		
		if($success){
			$this->session->set_flashdata('form_msg','Your data has been saved.');
		}else{
			$this->session->set_flashdata('form_msg','Your data has been saved.');
		}			
		redirect(base_url().ADMIN_DIR."universities");
	}
	
	function delete($id){
		if($this->university->delete($id)){
			$this->session->set_flashdata('form_msg','Your data has been deleted.');
		}
		redirect(base_url().ADMIN_DIR."universities");
	}
	
	//RETURN GEO LOCATION FROM GIVEN ADDRESS
	function get_geo_location(){
		$this->layout	= false;
		$address 		= $this->input->post('address');
		$address 		= str_replace(" ", "+", $address);
		$json 			= file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false");
		$json 			= json_decode($json);
		if($json->status != 'ZERO_RESULTS'){			
			$result		= array('status' => true,'latitude' => $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'},'longitude' => $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'});
		}else{
			$result		= array('status' => false,'message' => "Geographic Location not found!");
		}
		echo json_encode($result);
	}

}