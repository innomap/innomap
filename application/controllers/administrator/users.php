<?php 
require_once('admin_common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends Admin_common {
	function __construct() {
		parent::__construct('users');

		$this->load->model(array('user','role'));
		$this->meta 				= array();
		$this->scripts 				= array("jquery.validate","front/form_validation");
		$this->styles 				= array();
		$this->title 				= "user";
		
	}

	public function index() {
		$data = array('users'		=> $this->user->get_based_user_role(),
					  'roles'		=> $this->role->get()->result_array(),
			 		  'message'		=> $this->session->flashdata('success_msg'));
		$this->load->view(ADMIN_DIR.'user/index',$data);
	}

	function add(){
		$data = array('mode' 		=> 'ADD',
					  'roles'		=> $this->role->get()->result_array());
		$this->load->view(ADMIN_DIR.'user/form',$data);
	}

	function edit($id){
		$user = $this->user->get_user(array('user.user_id' => $id));
		$data = array('mode' 		=> 'EDIT',
					  'id'			=> $id, 
					  'roles'		=> $this->role->get()->result_array(),
					  'user'		=> $user[0]
					  );
		$this->load->view(ADMIN_DIR.'user/form',$data);	
	}

	function save(){
		$this->layout = FALSE;
		$mode = $this->input->post('mode');
		$roles_id = $this->input->post('role_id');
		$data_input = array(
						'username' 	=> $this->input->post('username'),
						'email' 	=> $this->input->post('email'),
						'password'	=> $this->input->post('password'),
						'role_id'	=> $roles_id,
						'status' 	=> $this->input->post('status')
					);
		
		if($this->input->post('password') == ""){
			unset($data_input['password']);
		}

		if($this->input->post('username') == ""){
			$data_input = array_diff_key($data_input,array("username" => ""));
		}
		
		if($mode == 'ADD'){
			$user_id = $this->user->add($data_input); 
			if($user_id){
				$this->session->set_flashdata('success_msg', 'Data successfully saved');
				redirect(site_url(ADMIN_DIR.'users'));
			}
		}else if($mode == 'EDIT'){
			$id = $this->input->post('user_id');
			$edit_user = $this->user->edit($id,$data_input);
			if($edit_user){
				$this->session->set_flashdata('success_msg', 'Data successfully edited');
				redirect(site_url(ADMIN_DIR.'users'));
			}
		}
	}

	function exist_username(){
		$this->layout = false;
		$username = $this->input->post('username');
		$user_id = $this->input->post('user_id');
		$where = array('username' => $username);
		if($user_id != ""){
			$where['user_id != '] = $user_id;
		}
		$data_user = $this->user->get($where)->num_rows();
		if($data_user > 0){
			$exist = true;
		}else{
			$exist = false;
		}
		echo json_encode($exist);
	}

	function delete($id){
		if($user = $this->user->delete($id)){
			$this->session->set_flashdata('success_msg', 'User has been deleted.');
			redirect(site_url(ADMIN_DIR.'users'));
		}
	}
}