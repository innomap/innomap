<?php
require_once('common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Committee_meetings extends Common {
	function __construct() {
		parent::__construct();
		
		$this->meta 			= array();
		$this->scripts 			= array();
		$this->styles 			= array();
		$this->title 			= "Committee Meeting";
		
		$this->load->model('committee_meeting');
	}

	public function index(){
		$this->scripts 	= array("jquery.dataTables.min","dataTables_initialize");
		$this->styles 	= array('jquery.dataTables');
		$this->load->helper('mystring');

		$data['meetings'] = $this->committee_meeting->get()->result_array();
		$data['access'] = $this->accessible;
		$data['flashdata'] = $this->session->flashdata('inno_form_msg');

		$this->load->view('committee_meeting/list',$data);
	}

	public function add(){
		$this->scripts = array('underscore-min','jquery.elastic','jquery.mentionsInput','jquery.validate','front/form_validation','front/committee_meeting');
		$this->styles = array('jquery.mentionsInput');
		$this->load->model('innovation');

		$data['innovations'] = $this->innovation->get_for_mention_list(array('data_source' => DATA_SOURCE_HIP6))->result_array();
		$this->load->view('committee_meeting/form',$data);
	}

	public function edit($id){
		$this->load->model(array('committee_meeting_attachment','innovation'));
		$this->scripts = array('underscore-min','jquery.elastic','jquery.mentionsInput','jquery.validate','front/form_validation','front/committee_meeting');
		$this->styles = array('jquery.mentionsInput');

		$data['innovations'] = $this->innovation->get_for_mention_list(array('data_source' => DATA_SOURCE_HIP6))->result_array();
		$data['meeting'] = $this->committee_meeting->get(array('committee_meeting_id'=>$id))->row_array();
		if($data['meeting']){
			$data['attachments'] = $this->committee_meeting_attachment->get(array('committee_meeting_id' => $data['meeting']['committee_meeting_id']))->result_array();
		}
		$this->load->view('committee_meeting/form', $data);
	}

	public function delete($id){
		if($this->committee_meeting->delete($id)){
			$this->session->set_flashdata('inno_form_msg', array('success' =>true, 'msg' => lang('msg_success_delete_meeting')));
		}else{
			$this->session->set_flashdata('inno_form_msg', array('success' =>false, 'msg' => lang('msg_failed_delete_meeting')));
		}
		redirect(site_url('committee_meetings'));
	}

	public function view($id){
		$this->load->model('committee_meeting_attachment');
		$this->scripts[] = 'front/committee_meeting';
		$this->styles[] = 'media_print';

		$data['detail'] = $this->committee_meeting->get(array('committee_meeting_id'=>$id))->row_array();
		if($data['detail']){
			$data['attachments'] = $this->committee_meeting_attachment->get(array('committee_meeting_id' => $data['detail']['committee_meeting_id']))->result_array();
			$data['detail']['notes'] = $this->pre_process_mention($data['detail']['notes']);
		}
		$this->load->view('committee_meeting/detail', $data);
	}

	public function save(){
		$this->layout = FALSE;
		$this->load->model(array('user','user_notification'));

		$committee_meeting_id = $this->input->post('committee_meeting_id');
		$data = array('user_id' => $this->user_sess['user_id'],
						'notes' => $this->input->post('notes'),
						'date' 	=> $this->input->post('date'),
						'title' => $this->input->post('title'),
						'attendance' => $this->input->post('attendance'));

		if($committee_meeting_id == '-1'){
			$data['created_date'] = date('Y-m-d H:i:s');
			$new_committee_meeting_id = $this->committee_meeting->add($data);
			if(count($_FILES) > 0){
				$this->upload_attachment($new_committee_meeting_id,$_FILES);
			}

			$committees = $this->user->get_user_with_role(array('user_role.role_id'=>ROLE_COMMITTEE))->result_array();
			//send notification to committee
			for($i=0;$i<count($committees);$i++){
				$data_notif = array('user_id_from' => $this->user_sess['user_id'],
							'user_id_to' => $committees[$i]['user_id'],
							'notif_type' => NOTIF_COMMITTEE_MEETING,
							'target_id' => $new_committee_meeting_id);
				$this->user_notification->add($data_notif);	
			}
			$this->session->set_flashdata('inno_form_msg', array('success' =>true, 'msg' => lang('msg_success_add_meeting')));

		}else{
			$this->committee_meeting->edit($committee_meeting_id,$data);
			if(count($_FILES) > 0){
				$this->upload_attachment($committee_meeting_id,$_FILES);
			}
			$deleted_attachment = $this->input->post('deleted_attachment');
			if(isset($deleted_attachment)){
				$this->delete_attachment($deleted_attachment);
			}
			$this->session->set_flashdata('inno_form_msg', array('success' =>true, 'msg' => lang('msg_success_edit_meeting')));
		}

		redirect(site_url('committee_meetings'));
	}

	private function upload_attachment($cm_id,$files){
		$this->layout = FALSE;
		$this->load->model(array('committee_meeting_attachment','site'));
		
		for($i=0; $i < count($files); $i++){
			if($files['attachment_'.$i]['name'] != ""){
				$filename = str_replace(" ", "_", $cm_id."_".$files['attachment_'.$i]['name']);
				$this->site->_upload($filename,'attachment_'.$i,PATH_TO_COMMITTEE_MEETING_ATTACHMENT);
				$attachment_id = $this->committee_meeting_attachment->add(array('committee_meeting_id' => $cm_id, 'filename' => $filename));
			}
		}		
	}

	private function delete_attachment($deleted_attachment){
		$this->layout = FALSE;
		$this->load->model(array('committee_meeting_attachment','site'));

		foreach ($deleted_attachment as $key => $value) {
			$attachment = $this->committee_meeting_attachment->get(array('attachment_id' => $value))->row_array();
			if($this->committee_meeting_attachment->delete($value)){
				$this->site->_remove($attachment['filename'],PATH_TO_COMMITTEE_MEETING_ATTACHMENT);
			}
		}
	}

	private function pre_process_mention($message){
		$this->load->model('innovation');
		$this->load->helper('charset');

		$pattern = '/@\[([^\]]+)\]\(([^:]+):(\d+)\)/';
		$matches = array();
		if (preg_match_all($pattern, $message, $matches)) {
			foreach ($matches[0] as $mention) {
				$innovation_id_pattern = '/[0-9]+/';
				$innovation_id_matches = array();
				if (preg_match($innovation_id_pattern, $mention, $innovation_id_matches)) {
					$innovation_id = $innovation_id_matches[0];
					if ($innovation = $this->innovation->get(array('innovation_id' => $innovation_id))->row_array()) {
						$mention_link = '<a class="innovation-name" href="#" data-id="'.$innovation_id.'">' . charset_htmlspecialchars($innovation['name_in_melayu']) . '</a>';
						$message = str_replace($mention, $mention_link, $message);
					}
				}
			}
		}

		return $message;
	}
}