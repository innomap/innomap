<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Common extends CI_Controller {
	function __construct($module=null) {
		parent::__construct();
		
		//language
		$this->load->helper('language');
		$this->session->set_userdata('language', LANGUAGE_MELAYU);
		$this->site_lang = $this->session->userdata('language');
		$this->lang->load('inno',$this->site_lang);		
		
		$this->load->model(array('user_session','role','menu'));
		$this->user_sess = $this->user_session->get();
		$status = true;
		$access	= NULL;
		$two_uri = false;
		
		/* Restrict Access */
		if($this->user_session){
			$curr_menu = $this->menu->get(array('url' => $this->uri->segment(1)))->row_array();
			if($curr_menu == NULL){
				$curr_menu = $this->menu->get(array('url' => $this->uri->segment(1)."/".$this->uri->segment(2)))->row_array();
				$two_uri = true;
			}
			if($module != "site" && count($_POST) == 0 && !$this->input->is_ajax_request() && $module != "galleries" && $module != "hip6_evaluation"){ //exception for site page, POST method and ajax request
				if(!$this->uri->segment(2) || $two_uri){ //if opened page not module but only controller
					$curr_module = $this->menu->get_module(array('menu_id' => $curr_menu['menu_id']))->row_array(); 
				}else{ //if opened page is module page
					$curr_module = $this->menu->get_module(array('url' => $this->uri->segment(1)."/".$this->uri->segment(2)))->row_array(); 
				}
				
				if($curr_module){
					if($this->role->get_role_module(array('module_id' => $curr_module['module_id'], 'role_id' => $this->user_sess['role_id']))->num_rows() <= 0){
						$status = false;
					}
				}else{
					if($this->uri->segment(2) != 'delete' && $this->uri->segment('delete_portfolio')){ //exception for delete url
						echo 'Page Not found!';die;
					}
				}
				
				//menu access - for checking in view
				$access = array();
				if($this->uri->segment(2).'/'.$this->uri->segment(3) == 'manage/innovation'){
					$curr_menu = $this->menu->get(array('url' => 'innovations'))->row_array();
				}
				$modules = $this->menu->get_module(array('menu_id' => $curr_menu['menu_id']))->result_array();
				foreach($modules as $module){
					$access[$module['name']] = ($this->role->get_role_module(array('module_id' => $module['module_id'], 'role_id' => $this->user_sess['role_id']))->num_rows() > 0);
				}
			}
		}else{
			$status = false;
		}
		
		if(!$status){
			echo 'Access Denied!';die;
		}
		
		$this->current_menu 	= $curr_menu;
		$this->module_access	= $status;
		$this->accessible		= $access;
		$data_head = array('message' => $this->session->flashdata('login_msg'),
						   'user' 	 => $this->user_sess,
						   'role'	 => $this->role->get(array('role_id' => $this->user_sess['role_id']))->row_array(),
						   'roles'	 => $this->role->get()->result_array(),
						   'menus'	 => $this->menu->get_front_menu($this->user_sess['role_id']),
						   'lang'	 => $this->site_lang,
						   'user_roles' => $this->user_sess['roles']);
		$this->parts['head'] 	= $this->load->view('partial/head', $data_head, true);
		$this->parts['footer'] 	= NULL;
	}
}