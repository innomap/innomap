<?php 
require_once('common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Evaluations extends Common {
	
	function __construct() {
		parent::__construct();
		
		$this->meta 			= array();
		$this->scripts 			= array('jquery.validate','front/form_validation','front/evaluation','front/innovation',"jquery.dataTables.min","dataTables_initialize");
		$this->styles 			= array('jquery.dataTables');
		$this->title 			= "Evaluations";
		
		$this->load->model(array('innovation','task','evaluation','user_notification'));
		$this->user_data = $this->user_session->get();
	}

	function index(){
		$data['tasks'] = $this->evaluation->get_task_list(array('expert_task_expert.expert_id' => $this->user_data['user_id']))->result_array();
		$data['message'] = $this->session->flashdata('ev_form_msg');
		$this->load->view('evaluation/task',$data);
	}

	function task($task_id){
		date_default_timezone_set("Asia/Bangkok");
		$current_date = date("Y-m-d", time());
		$task = $this->task->get_list_detail(array('expert_task.expert_task_id' => $task_id,'expert_id' => $this->user_data['user_id']))->result_array();
		//add average score
		for($i = 0; $i < count($task); $i++){
			$task[$i]['score_average'] = $this->innovation->get_average_score($task[$i]['innovation_id']);
		}
		
		$data = array('current_date' => $current_date,
					  'task' => $task);
		$this->load->view('evaluation/index',$data);
	}

	function add($innovation_id){
		date_default_timezone_set("Asia/Bangkok");
		$current_date = date("Y-m-d", time());
		
		$content = $this->evaluation->get_innovation_task(array('expert_task_innovation.innovation_id' => $innovation_id, 'expert_task_expert.expert_id' => $this->user_data['user_id']))->row_array();

		$data = array('mode' 			=> 'ADD',
					  'current_date' 	=> $current_date,
					  'content' 		=> $content,
					  'header'			=> array('mode' => 'ADD','id' => $innovation_id),
					  'message' 		=> $this->session->flashdata('ev_form_msg'));
		$this->load->view('evaluation/form1',$data);
	}

	function edit($form = "form1", $evaluation_id){
		$evaluation = $this->evaluation->get_detail(array('evaluation_id' => $evaluation_id))->row_array();
		if($evaluation['evaluator_expert_id'] == $this->user_sess['user_id']){
			$content = array('expert_id' => $evaluation['evaluator_expert_id'],'expert_name' => $evaluation['expert_name'], 'innovation_id' => $evaluation['innovation_id'],'innovation_name' => $evaluation['innovation_name'],'expert_task_id' => $evaluation['expert_task_id']);

			$data = array('mode' 			=> 'EDIT',
						  'current_date' 	=> date('Y-m-d', strtotime($evaluation['created_date'])),
						  'content' 		=> $content,
						  'evaluation' 		=> $evaluation,
						  'header'			=> array('mode' => 'EDIT','id' => $evaluation_id),
						  'message' 		=> $this->session->flashdata('ev_form_msg'),
						  'form'			=> $form,
						);
			if($form == "form1"){
				$this->load->view('evaluation/form1',$data);
			}else if($form == "form2"){
				$this->load->view('evaluation/form2',$data);
			}
		}else{
			$this->load->view('site/error_page',array('message' => "You are not allowed to edit this evaluation"));
		}
	}

	function view($form = "form1", $evaluation_id){
		$evaluation = $this->evaluation->get_detail(array('evaluation_id' => $evaluation_id))->row_array();

		$content = array('expert_id' => $evaluation['evaluator_expert_id'],'expert_name' => $evaluation['expert_name'], 'innovation_id' => $evaluation['innovation_id'],'innovation_name' => $evaluation['innovation_name'], 'expert_task_id' => $evaluation['expert_task_id']);

		$data = array('mode' 			=> 'VIEW',
					  'current_date' 	=> date('Y-m-d', strtotime($evaluation['created_date'])),
					  'content' 		=> $content,
					  'evaluation' 		=> $evaluation,
					  'header'			=> array('mode' => 'VIEW','id' => $evaluation_id),
					  'form'			=> $form,
					);
		if($form == "form1"){
			$this->load->view('evaluation/form1',$data);
		}else if($form == "form2"){
			$this->load->view('evaluation/form2',$data);
		}
	}

	function save_form1(){
		$this->layout = FALSE;

		if($_POST['form_evaluation_1'] == 1){
			$mode = $this->input->post('mode');

			$innovation_id = $this->input->post('innovation_id');
			$total = $this->input->post('total');

			$data_post = array(
					'expert_task_id' => $this->input->post('expert_task_id'),
					'innovation_id' => $innovation_id,
					'evaluator_expert_id' =>$this->user_data['user_id'],
					'total_creativity' => $this->input->post('total_creativity'),
					'creativity_originality' => $this->input->post('creativity_ori'),
					'creativity_adaptation' => $this->input->post('creativity_adap'),
					'implementation_level_full' => $this->input->post('implementation_level_full'),
					'implementation_level_trial' => $this->input->post('implementation_level_trial'),
					'replicability_free' => $this->input->post('replicability_free'),
					'replicability_not_free' => $this->input->post('replicability_not_free'),
					'total_output' => $this->input->post('total_output'),
					'efficiency_time' => $this->input->post('efficiency_time'),
					'efficiency_costs' => $this->input->post('efficiency_costs'),
					'efficiency_productivity' => $this->input->post('efficiency_productivity'),
					'efficiency_design' => $this->input->post('efficiency_design'),
					'significance_global' => $this->input->post('significance_global'),
					'significance_national' => $this->input->post('significance_national'),
					'significance_local' => $this->input->post('significance_local'),
					'total_outcome' => $this->input->post('total_outcome'),
					'commitment_finance' => $this->input->post('commitment_finance'),
					'commitment_hr' => $this->input->post('commitment_hr'),
					'commitment_incentive' => $this->input->post('commitment_incentive'),
					'commitment_reward' => $this->input->post('commitment_reward'),
					'commitment_equipment' => $this->input->post('commitment_equipment'),
					'total_commitment' => $this->input->post('total_commitment'),
					'relevancy' => $this->input->post('relevancy'),
					'effectiveness' => $this->input->post('effectiveness'),
					'quality' => $this->input->post('quality'),
					'potential' => $this->input->post('potential'),
					'total' => $total,
				);

			if($mode == 'ADD'){
				$exist_eval = $this->evaluation->get(array('innovation_id' => $data_post['innovation_id'],'evaluator_expert_id' => $data_post['evaluator_expert_id']))->row_array();
				if(count($exist_eval) > 0){
					$evaluation_id = $exist_eval['evaluation_id'];
					$this->evaluation->edit($evaluation_id,$data_post);
				}else{
					if($evaluation_id = $this->evaluation->add($data_post)){
						$this->session->set_flashdata('ev_form_msg', array('success' =>true, 'msg' => "Your data has been saved."));
					}
				}
				if(isset($_POST['draft'])){
					redirect(site_url('evaluations/edit/form1/'.$evaluation_id));
				}else{
					redirect(site_url('evaluations/edit/form2/'.$evaluation_id));
				}
			}else if($mode == 'EDIT'){
				$evaluation_id = $this->input->post('evaluation_id');
				if($this->evaluation->edit($evaluation_id,$data_post)){
					$this->session->set_flashdata('ev_form_msg', array('success' =>true, 'msg' => "Your data has been saved."));
					if(isset($_POST['draft'])){
						redirect(site_url('evaluations/edit/form1/'.$evaluation_id));
					}else{
						redirect(site_url('evaluations/edit/form2/'.$evaluation_id));
					}
				}
			}
		}
	}

	function save_form2(){
		$this->layout = FALSE;
		if($_POST['form_evaluation_2'] == 1){
			$data_post = array(
					'market_needs' => $this->input->post('market_needs'),
					'market_needs_review' => $this->input->post('market_needs_review'),
					'best_method' => $this->input->post('best_method'),
					'best_method_review' => $this->input->post('best_method_review'),
					'benefit' => $this->input->post('benefit'),
					'benefit_review' => $this->input->post('benefit_review'),
					'competition' => $this->input->post('competition'),
					'competition_review' => $this->input->post('competition_review'),
					'expertise_review' => $this->input->post('expertise_review'),
				);

			$evaluation_id = $this->input->post('evaluation_id');
			$innovation_id = $this->input->post('innovation_id');
			$total		   = $this->input->post('total'); 

			if(isset($_POST['send'])){
				$data_post['status'] = EVALUATION_DONE;
			}
			if($this->evaluation->edit($evaluation_id,$data_post)){
				if(isset($_POST['draft'])){
					$this->session->set_flashdata('ev_form_msg', array('success' =>true, 'msg' => "Your data has been saved."));
					redirect(site_url('evaluations/edit/form2/'.$evaluation_id));
				}else{
					$this->task->edit_expert_task_innovation($innovation_id,array('score' => $total));
					$task 		   = $this->evaluation->get_task(array('innovation.innovation_id' => $innovation_id))->row_array();

					$data_notif = array('user_id_from' => $this->user_data['user_id'],
									'user_id_to' => $task['assigner_id'],
									'notif_type' => NOTIF_EVALUATION_DONE,
									'target_id' => $innovation_id);
					$this->user_notification->add($data_notif);

					$this->session->set_flashdata('ev_form_msg', array('success' =>true, 'msg' => "Your data has been saved."));
					redirect(site_url('evaluations'));
				}
			}
		}
	}

	function view_detail_popup(){
		$this->layout = FALSE;
		$evaluation_id = $this->input->post('id');

		$evaluation = $this->evaluation->get_detail(array('evaluation_id' => $evaluation_id))->row_array();
		$content = array('expert_id' => $evaluation['evaluator_expert_id'],'expert_name' => $evaluation['expert_name'], 'innovation_id' => $evaluation['innovation_id'],'innovation_name' => $evaluation['innovation_name'], 'expert_task_id' => $evaluation['expert_task_id']);

		$data = array('evaluation' => $evaluation,
					  'content' => $content,
					  'current_date' => date('Y-m-d', strtotime($evaluation['created_date'])),
					  'mode' => 'VIEW');
		$this->load->view('evaluation/detail',$data);				
	}
}

