<?php 
require_once('common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Galleries extends Common {
	function __construct() {
		parent::__construct("galleries");
		
		$this->meta 			= array();
		$this->scripts 			= array('jquery.flexslider-min');
		$this->styles 			= array('flexslider');
		$this->title 			= "";
		$this->load->model('gallery');
	}
	
	public function index(){		
		$this->view();
	}

	function index_list(){
		$data = array('galleries' => $this->gallery->get(NULL,'innovation.innovation_id')->result_array());
		$this->load->view('gallery/list',$data);
	}

	function detail($id = NULL){
		$where = array();
		if($id != NULL){
			$where = array('innovation.innovation_id' => $id);
		}
		$data = array('galleries' => $this->gallery->get($where)->result_array());
		$this->load->view('gallery/detail',$data);
	}

	function view($id = NULL){
		$where = array();
		if($id != NULL){
			$where = array('innovation.innovation_id' => $id);
			$data = array('galleries' => $this->gallery->get($where)->result_array());
		}else{
			$data = array('galleries' => $this->gallery->get(NULL,'innovation.innovation_id')->result_array());
		}
		$this->load->view('gallery/index',$data);
	}
}	
	

/* End of file galleries.php */
/* Location: ./application/controllers/galleries.php */
?>