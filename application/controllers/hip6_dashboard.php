<?php 
require_once('common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hip6_dashboard extends Common {
	function __construct() {
		parent::__construct("management_dashboard");
		
		$this->meta 			= array();
		$this->scripts 			= array('jquery.dataTables.min','jquery.form', 'highcharts','highcharts-more', 'front/management_dashboard');
		$this->styles 			= array();
		$this->title 			= "";
		
		$this->load->model('portfolio');
	}
	
	public function index(){		
		$this->scripts[] = 'front/hip6_dashboard';
		$this->load->model('portfolio_criteria');
		$item_per_page = 6;

		//$all_criteria = unserialize(PORTFOLIO_ITEMS);
		$innovations =  $this->portfolio->get_innovation_in_portfolio(array('pa.is_hip6' => 1))->result_array();
		$data['total_innovation'] = count($innovations);
 	
 		$inno_item_first_page = ($item_per_page > $data['total_innovation'] ? $data['total_innovation'] : $item_per_page);
		for($i=0;$i<$inno_item_first_page;$i++){
			$data['innovations'][$i] = $innovations[$i];
			$data['innovations'][$i]['criteria_done'] = $this->portfolio->get_innovation_portfolio(array('innovation_id' => $innovations[$i]['innovation_id']))->num_rows();
			$data['innovations'][$i]['criteria_in_progress'] = $this->portfolio_criteria->get_by_innovation($innovations[$i]['innovation_id'],array('criteria_done' => 0, 'is_deleted' => 0),array('created_at < ' => $innovations[$i]['assignment_date']))->num_rows();
		}

		$portfolios = $this->portfolio->get_with_manager(array('p.is_hip6' => 1))->result_array();
		$data['total_portfolio'] = count($portfolios);
		$port_item_first_page = ($item_per_page > $data['total_portfolio'] ? $data['total_portfolio'] : $item_per_page);
		for($i=0;$i<$port_item_first_page;$i++){
			$data['portfolios'][$i] = $portfolios[$i];
			$data['portfolios'][$i]['criteria_done'] = $this->portfolio->get_innovation_portfolio(array('portfolio_id' => $portfolios[$i]['portfolio_id']))->num_rows();
			$all_criteria = $this->portfolio_criteria->get_by_portfolio($portfolios[$i]['portfolio_id'],NULL,array('created_at < ' => $portfolios[$i]['assignment_date']))->result_array();
			$deleted_criteria = 0;
			foreach ($all_criteria as $key => $value) {
				if($value['is_deleted'] == 1){
					$deleted_criteria = $deleted_criteria + ($portfolios[$i]['has_innovation'] - $value['criteria_done']);
				}
			}
			
			$criterias = (count($all_criteria)*$portfolios[$i]['has_innovation']) - $deleted_criteria;
			$data['portfolios'][$i]['criteria_in_progress'] = $criterias - $data['portfolios'][$i]['criteria_done'];	
		}
		
		$this->load->view('hip6_dashboard/index',$data);
	}

	function load_more_innovation($id = 0){
		$this->layout  = FALSE;
		$this->load->model('portfolio_criteria');

		$content = "";
		$item_per_page = 6;

		$page_number = $this->input->post('page_number');
		$position = ($page_number*$item_per_page);
		
		$results = $this->portfolio->get_innovation_in_portfolio(array('pa.is_hip6' => 1),$item_per_page, $position)->result_array();
		foreach ($results as $key => $value) {
			$results[$key]['criteria_done'] = $this->portfolio->get_innovation_portfolio(array('innovation_id' => $value['innovation_id']))->num_rows();
			$results[$key]['criteria_in_progress'] = $this->portfolio_criteria->get_by_innovation($value['innovation_id'],array('criteria_done' => 0, 'is_deleted' => 0),array('created_at < ' => $value['assignment_date']))->num_rows();
		}
		
		echo json_encode($results);
	}

	function load_more_portfolio($id = 0){
		$this->layout  = FALSE;
		$this->load->model('portfolio_criteria');

		$content = "";
		$item_per_page = 6;

		$page_number = $this->input->post('page_number');
		$position = ($page_number*$item_per_page);
		
		$results = $this->portfolio->get_with_manager(array('p.is_hip6' => 1),$item_per_page, $position)->result_array();
		foreach ($results as $key => $value) {
			$results[$key]['criteria_done'] = $this->portfolio->get_innovation_portfolio(array('portfolio_id' => $value['portfolio_id']))->num_rows();
			$all_criteria = $this->portfolio_criteria->get_by_portfolio($value['portfolio_id'],NULL,array('created_at < ' => $value['assignment_date']))->result_array();
			$deleted_criteria = 0;
			foreach ($all_criteria as $criteria) {
				if($criteria['is_deleted'] == 1){
					$deleted_criteria = $deleted_criteria + ($value['has_innovation'] - $criteria['criteria_done']);
				}
			}
			
			$criterias = (count($all_criteria)*$value['has_innovation']) - $deleted_criteria;
			$results[$key]['criteria_in_progress'] = $criterias - $results[$key]['criteria_done'];	
		}
		
		echo json_encode($results);
	}
}