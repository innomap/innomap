<?php 
require_once('common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hip6_evaluations extends Common {
	
	function __construct() {
		parent::__construct("hip6_evaluation");
		
		$this->meta 			= array();
		$this->scripts 			= array('jquery.validate','front/form_validation','front/hip6_evaluation','front/innovation',"jquery.dataTables.min","dataTables_initialize");
		$this->styles 			= array('jquery.dataTables');
		$this->title 			= "HIP6 Evaluations";
		
		$this->load->model(array('innovation','task','hip6_evaluation','user_notification'));
		$this->user_data = $this->user_session->get();
	}

	function index(){

	}

	function form($innovation_id, $redirect_to_form = 0){
		$this->styles[] = 'media_print';
		
		$data['innovation'] = $this->innovation->get(array('innovation_id' => $innovation_id))->row_array();
		if($evaluation = $this->hip6_evaluation->get_detail(array('eval.innovation_id' => $innovation_id))->row_array()){
			$data['evaluation'] = $evaluation;
		}
		$data['message'] = $this->session->flashdata('ev_form_msg');
		$data['redirect_to_form'] = $redirect_to_form;
		if($this->user_data['role_id'] == ROLE_PROGRAM_DIRECTOR || $this->user_data['role_id'] == ROLE_HIP6_STAKEHOLDER){
			$data['view_mode'] = 1;
		}
		$this->load->view('hip6_evaluation/form',$data);
	}

	function save(){
		$this->layout = FALSE;

		$evaluation_id = $this->input->post('evaluation_id');
		$data = array('innovation_id' => $this->input->post('innovation_id'),
					  'originality_innovation' => $this->input->post('originality_innovation'),
					  'comparable' => $this->input->post('comparable'),
					  'innovation_impact' => $this->input->post('innovation_impact'),
					  'unit_cost' => $this->input->post('unit_cost'),
					  'cost_of_assistance' => $this->input->post('cost_of_assistance'),
					  'excluded_group' => $this->input->post('excluded_group'),
					  'market_size' => $this->input->post('market_size'),
					  'attitude_of_entrepreneur' => $this->input->post('attitude_of_entrepreneur'),
					  'diffusion_platform' => $this->input->post('diffusion_platform'),
					  'strategic_linkages' => $this->input->post('strategic_linkages'),
					  'total' => $this->input->post('total'),
					  'comment' => $this->input->post('comment'),
					  'ba_id' => $this->input->post('ba_id'),
					  );

		if($evaluation_id != ""){
			if($this->hip6_evaluation->edit($evaluation_id,$data)){
				$this->session->set_flashdata('ev_form_msg', array('success' =>true, 'msg' => "Your data has been saved."));
			}
		}else{
			if($this->hip6_evaluation->add($data)){
				$this->session->set_flashdata('ev_form_msg', array('success' =>true, 'msg' => "Your data has been saved."));
			}
		}
		
		$redirect_to_form = $this->input->post('redirect_to_form');

		if($redirect_to_form == 0){
			redirect(site_url('hip6_evaluations/form/'.$data['innovation_id']));
		}else{
			redirect(site_url('innovations/edit/0/'.$data['innovation_id']));
		}
		
	}
}

