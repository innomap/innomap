<?php 
require_once('common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Innovations extends Common {
	function __construct() {
		parent::__construct("innovations");
		
		$this->meta 			= array();
		$this->scripts 			= array();
		$this->styles 			= array();
		$this->title 			= "Innovations";
		
		$this->load->model('innovation');
		$this->load->model('open_cart');
	}

	public function index(){
		$this->scripts 	= array("jquery.dataTables.min","dataTables_initialize","front/innovation");
		$this->styles 	= array('jquery.dataTables');
		$this->load->helper('mystring');

		$this->load->model('innovator');
		
		if($this->user_sess['role_id'] != ROLE_INNOVATOR){
			if($this->user_sess['role_id'] == ROLE_BUSINESS_ANALYST || $this->user_sess['role_id'] == ROLE_PROGRAM_DIRECTOR || $this->user_sess['role_id'] == ROLE_HIP6_STAKEHOLDER){
				$innovations = $this->innovation->get(array('data_source'=>DATA_SOURCE_HIP6,'status' => INNO_APPROVED))->result_array();
			}else{
				$innovations = $this->innovation->get()->result_array();
			}

			foreach($innovations as $key=>$value){
				$str_innovators = '';
				$innovators = $this->innovation->get_innovation_innovator(array('ii.innovation_id' => $value['innovation_id']));

				foreach($innovators as $innovator){
					$str_innovators .= "- ".$innovator['name']."<br/>"; 
				}
				$innovations[$key]['innovator'] = $str_innovators;
			}
			
			$data = array('innovations' => $innovations, 'access' => $this->accessible,'flashdata' => $this->session->flashdata('inno_form_msg'));
			$this->load->view('innovation/idx_list',$data);
		}else{
			//redirect to current innovator form page if logged in as innovator
			redirect(base_url()."innovators/manage/account/".$this->user_sess['user_id']);
		}
	}

	function pending(){
		$this->scripts 	= array("jquery.validate","front/form_validation","jquery.dataTables.min","dataTables_initialize","front/innovation");
		$this->styles 	= array('jquery.dataTables');
		
		$innovations = $this->innovation->get(array('status'=>INNO_SENT_APPROVAL))->result_array();
		for($i=0;$i<count($innovations);$i++){
			$innovator = $this->innovation->get_innovation_innovator(array('ii.innovation_id' => $innovations[$i]['innovation_id']));
			$innovations[$i]['innovator'] = $innovator[0]['name'];
			$innovations[$i]['name']	  = ($this->site_lang == LANGUAGE_MELAYU ? $innovations[$i]['name_in_melayu'] : $innovations[$i]['name']);	
		}

		$data = array('innovations'	=> $innovations, 'flashdata' => $this->session->flashdata('inno_form_msg'), 'access' => $this->accessible);		
		$this->load->view('innovation/pending',$data);
	}
	
	function add($id){
		$this->load->model('category');
		$this->scripts = array("front/innovation","jquery.validate","front/form_validation");
		
		$data['innovator_id'] 	= $id;
		$data['targets']	  	= unserialize(INNOVATION_TARGET);
		$data['categories']		= $this->category->get()->result_array();
		$data['data_sources']	= unserialize(DATA_SOURCES);
		$data['mode']			= 'ADD';
		$data['flashdata']		= $this->session->flashdata('inno_form_msg');
		$this->load->view('innovation/form',$data);
	}
	
	function edit($innovator_id,$id){
		$this->load->model(array('category','innovation_note','hip6_evaluation'));
		$this->scripts = array("front/innovation","jquery.validate","front/form_validation",'jquery.dataTables.min','dataTables_initialize');
		$this->styles[] = "jquery.dataTables";
		
		$inno_cats = $this->innovation->get_innovation_category(array('innovation_id'=>$id));
		for($i=0;$i<count($inno_cats);$i++){
			$inno_cats[$i]['subcategories'] = $this->category->get_subcategory(array('category_id' => $inno_cats[$i]['category_id']))->result_array();
		}

		$innovators = array();

		if($innovator_id == 0){
			$innovators = $this->innovation->get_innovation_innovator(array('ii.innovation_id' => $id));
		}
		
		$data['innovator_id']	= $innovator_id;
		$data['detail']			= $this->innovation->get(array('innovation_id'=>$id))->row_array();
		$data['inno_subs']		= $this->innovation->get_innovation_subcategory($id);
		$data['inno_pics']		= $this->innovation->get_innovation_picture($id);
		$data['inno_cats']		= $inno_cats;
		$data['targets']	  	= unserialize(INNOVATION_TARGET);
		$data['categories']		= $this->category->get()->result_array();
		$data['mode']			= 'EDIT';
		$data['data_sources']	= unserialize(DATA_SOURCES);
		$data['flashdata']		= $this->session->flashdata('inno_form_msg');
		$data['innovator']		= $innovators;
		$data['notes']			= $this->innovation_note->get_list(array('innovation_id' => $id))->result_array();

		if($this->user_sess['role_id'] == ROLE_BUSINESS_ANALYST || $this->user_sess['role_id'] == ROLE_PROGRAM_DIRECTOR){
			$data['hip6_score'] = $this->hip6_evaluation->get(array('innovation_id' => $id))->row_array();
		}
		$this->load->view('innovation/form',$data);
	}
	
	function save(){	
		$this->load->model(array('innovator','site','user_notification','innovation_note'));
		
		$mode 		  =	$this->input->post('mode');
		$innovator_id = $this->input->post('innovator_id');
		$ba_score	  = $this->input->post('ba_score');
		$data_post 	  = array('name'					=> $this->input->post('name_in_melayu'),
						      'name_in_melayu'			=> $this->input->post('name_in_melayu'),
							  'created_date'			=> $this->input->post('created_date'),
							  'discovered_date'			=> $this->input->post('discovered_date'),
						      'inspiration'				=> $this->input->post('inspiration'),
						      'inspiration_in_melayu'	=> $this->input->post('inspiration_in_melayu'),
						      'description'				=> $this->input->post('description'),
						      'description_in_melayu'	=> $this->input->post('description_in_melayu'),
						      'materials'				=> $this->input->post('materials'),
						      'materials_in_melayu'		=> $this->input->post('materials_in_melayu'),
						      'manufacturing_costs'		=> $this->input->post('manufacturing_costs'),
							  'selling_price'			=> $this->input->post('selling_price'),
						      'how_to_use'				=> $this->input->post('how_to_use'),
							  'how_to_use_in_melayu'	=> $this->input->post('how_to_use_in_melayu'),
						      'target'					=> (count($this->input->post('target')) > 0 ? json_encode($this->input->post('target')) : ""),
						      'myipo_protection'		=> $this->input->post('myipo_protection'),
						      'special_achievement'		=> $this->input->post('special_achievement'),
							  'special_achievement_in_melayu' => $this->input->post('special_achievement_in_melayu'),
							  'keyword'					=> (count($this->input->post('keyword')) > 0 ? json_encode($this->input->post('keyword')) : ""),
							  'data_source'				=> $this->input->post('data_source'));
						   
		//save innovation
		if($mode == 'ADD'){
			$data_post['analyst_creator_id'] = $this->user_sess['user_id'];
			$innovation_id = $this->innovation->add($data_post);
			
			//innovator innovation
			$invtr_invs_data_post = array('innovator_id' 	=> $innovator_id,
										  'innovation_id' 	=> $innovation_id);
			$this->innovator->add_innovator_innovation($invtr_invs_data_post);
		}else{
			$innovation_id = $this->input->post('innovation_id');
			//Get marketplace data
			$product = $this->open_cart->get_product(array('innovation_id' => $id))->row_array();
			if($this->innovation->edit($innovation_id,$data_post)){
				//Edit Marketplace data
				if($product){
					$this->open_cart->update_product($product['product_id'],array('price' => $data_post['selling_price']));
					$this->open_cart->update_product_desc($product['product_id'],array('name' => $data_post['name_in_melayu'], 'description' => $data_post['description_in_melayu']));
				}

				if($this->input->post('status') == INNO_APPROVED){
					$this->user_notification->send_multi_user(NOTIF_EDIT_INNOVATION,$innovation_id);
				}

				//add information memo
				$note = $this->input->post('information_memo');
				if($note != ""){
					$this->innovation_note->add(array('innovation_id' => $innovation_id,
														'user_id' => $this->user_sess['user_id'],
														'note' => $note,
														'created_at' => date('Y-m-d H:i:s'),
														'updated_at' => date('Y-m-d H:i:s')));
				}
			}
		}
		
		//innovation category
		$categories = array_unique($this->input->post('category_id'));
		if(count($categories) > 0){
			//delete all innovation category first
			$this->innovation->delete_innovation_category($innovation_id);
			if($product){
				$this->open_cart->delete_product_to_category(array('product_id' => $product_product_id));
			}
			
			//save innovation category
			foreach($categories as $cat){
				$cat_data_post = array('innovation_id' => $innovation_id, 'category_id' => $cat);
				$this->innovation->add_innovation_category($cat_data_post);

				$category = $this->open_cart->get_category(array('ref_category_id' => $cat))->row_array();
				$data_cat = array('product_id' => $product['product_id'], 'category_id' => $category['category_id']);
				$this->open_cart->add_product_to_category($data_cat);
			}
		}
		
		//innovation sub category
		if($this->input->post('subcategory')){
			$subcategories = array_unique($this->input->post('subcategory'));
			if(count($subcategories) > 0){
				//delete all innovation subcategory first
				$this->innovation->delete_innovation_subcategory($innovation_id);
				
				//save innovation subcategory
				foreach($subcategories as $sub){
					$sub_data_post = array('innovation_id' => $innovation_id, 'subcategory_id' => $sub);
					$this->innovation->add_innovation_subcategory($sub_data_post);
				}
			}
		}
		
		//innovation picture
		$pict_nums = (int) $this->input->post('pict_num');
		for($i=0;$i<=$pict_nums;$i++){
			if(isset($_FILES['picture_'.$i])){
				if($_FILES['picture_'.$i]['name'] != NULL){
					$bef_pict = $this->input->post('bef_picture_'.$i);
					if($bef_pict != NULL && $bef_pict != ""){
						if($this->innovation->delete_innovation_picture(array('innovation_id'=>$innovation_id, 'picture'=>$bef_pict))){
							$this->site->_remove($bef_pict,PATH_TO_INNOVATION_PICTURE);
							$this->site->_remove($bef_pict,PATH_TO_INNOVATION_PICTURE_THUMB);
						}
					}
					$picture_name = preg_replace('/\s+/', '_', $_FILES['picture_'.$i]['name']);
					$filename 		  = $innovation_id."_".$picture_name;
					$uploaded_picture = $this->site->_upload($filename,'picture_'.$i,PATH_TO_INNOVATION_PICTURE,"img",PATH_TO_INNOVATION_PICTURE_THUMB);
					$this->innovation->add_innovation_picture(array('innovation_id' => $innovation_id, 'picture' => $uploaded_picture));
				}
			}
		}

		if($pict_nums > 0 && $product){
			$pictures = $this->innovation->get_innovation_picture($id);
			if($pictures){
				$this->open_cart->update_product($product['product_id'],array('image' => $pictures[0]['picture']));
			}

			foreach ($pictures as $key => $value) {
				$data_pic = array('product_id' => $product_id, 'image' => $value['picture']);
				$this->open_cart->add_product_image($data_pic);
			}
		}
		
		//set flashdata
		$this->session->set_flashdata('inno_form_msg', array('success' =>true, 'msg' => lang('msg_success_save_innovation')));	
		$this->layout = false;

		if($innovator_id == 0){
			$res_url = base_url()."innovations/edit/0/".$innovation_id;
		}else{
			$res_url = base_url()."innovators/manage/innovation/".$innovator_id;
		}
		$result = array('innovation_id' => $innovation_id, 'res_url' => $res_url);
		echo json_encode($result);	
	}
	
	//DELETE INNOVATION
	function delete($type,$id){
		$this->load->model(array('site','user_notification'));
		
		if($type == 'innovation'){
			//delete innovation picture
			$pictures = $this->innovation->get_innovation_picture($id);
			foreach($pictures as $picture){
				$this->site->_remove($picture['picture'],PATH_TO_INNOVATION_PICTURE);
				$this->site->_remove($picture['picture'],PATH_TO_INNOVATION_PICTURE_THUMB);
			}

			$innovation = $this->innovation->get(array('innovation_id' => $id))->row_array();
			if($innovation['status'] == INNO_APPROVED){
				$this->user_notification->send_multi_user(NOTIF_DELETE_INNOVATION,$id);
			}

			$product = $this->open_cart->get_product(array('innovation_id' => $id))->row_array();

			if($product){
				if($product['status'] == 1){
					$this->innovation->edit($id, array('is_mp_product' => 0));
					$this->open_cart->update_product($product['product_id'],array('status' => 0));
				}
			}
			//set flashdata
			if($this->innovation->delete($id)){
				$this->session->set_flashdata('inno_form_msg', array('success' =>true, 'msg' => lang('msg_success_delete_innovation')));		
			}else{
				$this->session->set_flashdata('inno_form_msg', array('success' =>false, 'msg' => lang('msg_failed_delete_innovation')));		
			}
		}elseif($type == 'picture'){
			$picture = $this->innovation->get_innovation_picture(array('innovation_picture_id' =>$id));
			$this->site->_remove($picture[0]['picture'],PATH_TO_INNOVATION_PICTURE);
			$this->site->_remove($picture[0]['picture'],PATH_TO_INNOVATION_PICTURE_THUMB);
			
			//set flashdata
			if($this->innovation->delete_innovation_picture(array('innovation_picture_id' => $id))){
				$this->session->set_flashdata('inno_form_msg', array('success' =>true, 'msg' => lang('msg_success_delete_innovation_picture')));		
			}else{
				$this->session->set_flashdata('inno_form_msg', array('success' =>false, 'msg' => lang('msg_failed_delete_innovation_picture')));		
			}
		}
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	//VIEW INNOVATION
	function view($id,$show_innovator = 0,$show_back_btn = 1){	
		$this->load->model(array('innovation_note','category','hip6_evaluation'));
		$this->scripts = array('jquery.dataTables.min','dataTables_initialize');
		$this->styles[] = 'media_print';
		$this->styles[] = "jquery.dataTables";
		
		$inno_cats = $this->innovation->get_innovation_category(array('innovation_id'=>$id));
		for($i=0;$i<count($inno_cats);$i++){
			$inno_cats[$i]['subcategories'] = $this->category->get_subcategory(array('category_id' => $inno_cats[$i]['category_id']))->result_array();
		}

		$innovators = array();

		if($show_innovator != 0){
			$innovators = $this->innovation->get_innovation_innovator(array('ii.innovation_id' => $id));
		}
	
		$data['detail']			= $this->innovation->get(array('innovation_id'=>$id))->row_array();
		$data['inno_subs']		= $this->innovation->get_innovation_subcategory($id);
		$data['inno_pics']		= $this->innovation->get_innovation_picture($id);
		$data['inno_cats']		= $inno_cats;
		$data['targets']	  	= unserialize(INNOVATION_TARGET);
		$data['categories']		= $this->category->get()->result_array();	
		$data['innovator']		= $innovators;
		$data['show_back_btn']  = $show_back_btn;	
		$data['data_sources']	= unserialize(DATA_SOURCES);
		$data['notes']			= $this->innovation_note->get_list(array('innovation_id' => $id))->result_array();

		if($this->user_sess['role_id'] == ROLE_BUSINESS_ANALYST || $this->user_sess['role_id'] == ROLE_PROGRAM_DIRECTOR){
			$data['hip6_score'] = $this->hip6_evaluation->get(array('innovation_id' => $id))->row_array();
		}
		$this->load->view('innovation/view',$data);
	}
	
	//SEND APPROVAL REQUEST
	function send_approval($id){
		$this->load->model(array('user','user_notification'));
		if($this->innovation->edit($id,array('status' => INNO_SENT_APPROVAL))){
			//send notification
			$this->user_notification->send_multi_user(NOTIF_NEW_INNOVATION,$id);
			
			$this->session->set_flashdata('inno_form_msg', array('success' =>true, 'msg' => lang('msg_success_send_request')));
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	
	//AJAX CALL - GET SUBCATEGORY FROM GIVEN CATEGORY_ID
	function subcategory($category_id){
		$this->layout = false;
		$this->load->model('category');
		echo json_encode($this->category->get_subcategory(array('category_id'=>$category_id))->result_array());
	}

	//INNOVATION SELF APPROVAL
	function approval(){
		$this->load->model(array('user','user_notification'));
		$innovation_id  = $this->input->post('app_innovation_id');
		$status =  $this->input->post('app_status');
		$approval_data  = array('status' 		=> $this->input->post('app_status'),
							   'approval_note'	=> $this->input->post('app_note'),
							   'approval_date'	=> date('Y-m-d'),
							   'approver_id'	=> $this->user_sess['user_id']);
		
		if($this->innovation->edit($innovation_id,$approval_data)){
			//send notification
			$innovation = $this->innovation->get(array('innovation_id' => $innovation_id))->row_array();
			if($status == INNO_APPROVED){
				$this->user_notification->send_multi_user(NOTIF_APPROVED_INNOVATION,$innovation_id);
			}else{
				$this->user_notification->send_multi_user(NOTIF_REJECTED_INNOVATION,$innovation_id);
			}
			
			//set flashdata
			$this->session->set_flashdata('inno_form_msg', array('success' =>true, 'msg' => ($this->input->post('app_status') == INNO_APPROVED ? lang('msg_success_approve_innovation') : lang('msg_success_reject_innovation'))));
			redirect($_SERVER['HTTP_REFERER']);
		}
	}

	function list_approved(){
		$data = array('innovations' => $this->innovation->get(array('status' => INNO_APPROVED))->result_array());
		$this->load->view('innovation/list_approved',$data);
	}

	function view_notes(){
		$this->layout = FALSE;
		$innovation_id = $this->input->post('id');
		$response = array('status' => 0);
		$status = unserialize(INNO_STATUSES);

		$innovation = $this->innovation->get(array('innovation_id' => $innovation_id))->row_array();
		if($innovation){
			$response['status'] = 1;
			$response['innovation'] = ($this->session->userdata('language') == LANGUAGE_MELAYU ? $innovation['name_in_melayu'] : $innovation['name']);
			$response['status_approval'] = $status[$innovation['status']]['name'];
			$response['notes'] = $innovation['approval_note'];
		}

		echo json_encode($response);
	}

	function view_detail_popup(){
		$this->layout = FALSE;
		$this->load->model('category');

		$innovation_id = $this->input->post('id');
		$response = array('status' => 0);

		$inno_cats = $this->innovation->get_innovation_category(array('innovation_id'=>$innovation_id));
		for($i=0;$i<count($inno_cats);$i++){
			$inno_cats[$i]['subcategories'] = $this->category->get_subcategory(array('category_id' => $inno_cats[$i]['category_id']))->result_array();
		}		
		$data['detail']			= $this->innovation->get(array('innovation_id'=>$innovation_id))->row_array();
		$data['inno_subs']		= $this->innovation->get_innovation_subcategory($innovation_id);
		$data['inno_pics']		= $this->innovation->get_innovation_picture($innovation_id);
		$data['inno_cats']		= $inno_cats;
		$data['targets']	  	= unserialize(INNOVATION_TARGET);
		$data['data_sources']	= unserialize(DATA_SOURCES);
		$data['categories']		= $this->category->get()->result_array();

		$this->load->view('innovation/detail',$data);
		
	}

	function executive_summary($id){
		$this->styles[] = 'media_print';
		$this->load->model('innovation_executive_summary');
		$data['innovation']	= $this->innovation->get(array('innovation_id'=>$id))->row_array();
		$data['innovators'] = $this->innovation->get_innovation_innovator(array('ii.innovation_id' => $id));
		$summary = $this->innovation_executive_summary->get(array('innovation_id' => $id))->row_array();
		if($summary){
			$data['summary'] = $summary;
		}

		if($this->user_sess['role_id'] == ROLE_HIP6_STAKEHOLDER){
			$data['view_mode'] = true;
		}else{
			$data['view_mode'] = false;
		}
		$data['flashdata']		= $this->session->flashdata('inno_form_msg');
		$this->load->view('innovation/form_executive_summary',$data);
	}

	function save_executive_summary(){
		$this->layout = FALSE;
		$this->load->model('innovation_executive_summary');

		$id = $this->input->post('summary_id');
		$innovation_id = $this->input->post('innovation_id');
		$data = array('innovation_id' => $innovation_id,
						'user_id' => $this->user_sess['user_id'],
						'summary' => $this->input->post('summary'),
						'updated_at' => $data['created_at'] = date('Y-m-d H:i:s'));
		
		if($id == '-1'){
			$data['created_at'] = date('Y-m-d H:i:s');
			$id = $this->innovation_executive_summary->add($data);
		}else{
			$this->innovation_executive_summary->edit($id,$data);
		}

		$this->session->set_flashdata('inno_form_msg', array('success' =>true, 'msg' => lang('msg_success_save_exc_summary')));	
		redirect(site_url().'innovations/executive_summary/'.$innovation_id);
	}

	//list innovation where source not HIP6
	function list_innovation(){
		$this->scripts 	= array("jquery.dataTables.min","dataTables_initialize","front/innovation");
		$this->styles 	= array('jquery.dataTables');
		$this->load->helper('mystring');

		$this->load->model('innovator');
		
		$innovations = $this->innovation->get(array('data_source !='=>DATA_SOURCE_HIP6,'status' => INNO_APPROVED))->result_array();
		foreach($innovations as $key=>$value){
			$str_innovators = '';
			$innovators = $this->innovation->get_innovation_innovator(array('ii.innovation_id' => $value['innovation_id']));

			foreach($innovators as $innovator){
				$str_innovators .= "- ".$innovator['name']."<br/>"; 
			}
			$innovations[$key]['innovator'] = $str_innovators;
		}
		
		$data = array('innovations' => $innovations, 'data_source' => unserialize(DATA_SOURCES), 'flashdata' => $this->session->flashdata('inno_form_msg'));
		$this->load->view('innovation/pd_list',$data);
	}

	function update_source(){
		$this->layout = false;
		$this->load->model(array('innovation'));

		$innovation_id = $this->input->post('innovation_id');

		$data = array('data_source'=>DATA_SOURCE_HIP6);
		if($this->innovation->edit($innovation_id, $data)){
			echo json_encode(true);
		}else{
			echo json_encode(false);
		}
	}

	function hip6_approval(){
		$this->load->model(array('user','user_notification'));
		$innovation_id  = $this->input->post('app_innovation_id');
		$status =  $this->input->post('app_status');
		$approval_data  = array('hip6_status' 		=> $this->input->post('app_status'),
							   'hip6_approval_date'	=> date('Y-m-d H:i:s'),
							   'hip6_approver_id'	=> $this->user_sess['user_id']);
		
		if($this->innovation->edit($innovation_id,$approval_data)){
			//set flashdata
			$this->session->set_flashdata('inno_form_msg', array('success' =>true, 'msg' => ($this->input->post('app_status') == INNO_APPROVED ? lang('msg_success_approve_innovation') : lang('msg_success_reject_innovation'))));
			redirect(site_url('innovations'));
		}
	}

	/*Resize uploaded innovation picture*/
	public function do_resize($limit,$offset){
		$this->load->model('site');
		$images = $this->innovation->get_innovation_pict_list($limit,$offset)->result_array();
		foreach ($images as $key => $value) {
			$filename = $value['picture'];
			$source_path = PATH_TO_INNOVATION_PICTURE . $filename;
		    $target_path = PATH_TO_INNOVATION_PICTURE_THUMB;

		    if (file_exists(realpath(APPPATH . '../'.PATH_TO_INNOVATION_PICTURE) . DIRECTORY_SEPARATOR . $filename)) {
			    $imgInfo = getimagesize("./assets/attachment/innovation_picture/" . $filename);
				
				$this->site->create_thumbnail($imgInfo[0],$imgInfo[1],PATH_TO_INNOVATION_PICTURE,$filename, PATH_TO_INNOVATION_PICTURE_THUMB);
			}else{
				echo "file doesn't exist<br/>";
			}
		}	    
	}

	function edit_information_note(){
		$this->layout = FALSE;
		$this->load->model("innovation_note");

		$note_id = $this->input->post("dlg_note_id");
		$innovation_id = $this->input->post("dlg_innovation_id");
		$note = $this->input->post("dlg_note");

		$data = array("note" => $note, "updated_at" => date('Y-m-d H:i:s'));
		if($this->innovation_note->edit($note_id, $data)){
			redirect(base_url()."innovations/edit/0/".$innovation_id."#note");
		}
	}

	function delete_information_note($info_id, $innovation_id){
		$this->load->model("innovation_note");
		if($this->innovation_note->delete($info_id)){
			redirect(base_url()."innovations/edit/0/".$innovation_id."#note");
		}
	}
}