<?php 
require_once('common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Innovators extends Common {
	function __construct() {
		parent::__construct("innovators");
		
		$this->meta 			= array();
		$this->scripts 			= array();
		$this->styles 			= array();
		$this->title 			= "";
		
		$this->load->model('innovator');
		
		//special case: restrict access when innovator status is sent for approval or rejected (can access manage form & redirect to view page)
		if($this->uri->segment(2) == 'manage'){
			$innovator_id = $this->uri->segment(4);
			$detail		  = $this->innovator->get(array('innovator_id'=>$innovator_id))->row_array();
			if($detail['status'] == INNO_SENT_APPROVAL || $detail['status'] == INNO_REJECTED){
				redirect(base_url()."innovators/view/".$this->uri->segment(3)."/".$innovator_id);
			}
		}
	}
	
	public function index(){		
		$this->scripts 	= array("jquery.validate","front/form_validation","front/innovator","jquery.dataTables.min","dataTables_initialize");
		$this->styles 	= array('jquery.dataTables');
		
		if($this->user_sess['role_id'] != ROLE_INNOVATOR){
			$innovators = $this->innovator->get()->result_array();
			for($i=0;$i<count($innovators);$i++){
				$innovators[$i]['innovation'] = "";
				$innovations = $this->innovator->get_innovator_innovation(array('ii.innovator_id' => $innovators[$i]['innovator_id']));
				foreach($innovations as $innovation){
					$innovators[$i]['innovation'] .= "- ".($this->site_lang == LANGUAGE_MELAYU ? $innovation['name_in_melayu'] : $innovation['name'])."<br/>"; 
				}
			}
			
			$data = array('innovators' => $innovators, 'access' => $this->accessible, 'statuses' => unserialize(INNO_STATUSES), 'flashdata' => $this->session->flashdata('inno_form_msg'));
			$this->load->view('innovator/list',$data);
		}else{
			//redirect to current innovator form page if logged in as innovator
			redirect(base_url()."innovators/manage/account/".$this->user_sess['user_id']);
		}
	}
	
	//INNOVATOR ACCOUNT
	public function account_form($id = NULL){
		$this->load->model('user');
		$this->scripts 	= array("front/innovator","jquery.validate","front/form_validation");
		
		$mode = ($id == NULL ? 'ADD' : 'EDIT');
		
		$data['mode']		 	= $mode;
		$data['links']			= $this->set_form_links('account',$id);
		if($mode == 'EDIT'){
			$data['detail']		= $this->user->get(array('user_id'=>$id))->row_array();
			$innovator 			= $this->innovator->get(array('innovator_id'=>$id))->row_array();
			$data['links']		= $this->set_form_links('account',$id,($innovator['d_innovator_has'] == INNOVATOR_HAS_BUSINESS));
			$data['innovator_status'] = $innovator['status'];
		}
		$this->load->view('innovator/account',$data);
	}
	
	//SAVE ACCOUNT
	function save_account(){
		$this->load->model(array('user','user_notification'));
		
		$mode	   = $this->input->post('mode');
		$data_post = array('username' => $this->input->post('username'),
						   'email'	  => $this->input->post('email'),
						   'role_id'  => ROLE_INNOVATOR,
						   'status'	  => USER_ACTIVE);
		
		if($this->input->post('password') != ""){
			$data_post['password'] = $this->input->post('password');
		}
		
		if($mode == 'ADD'){
			$user_id = $this->user->add($data_post);
			if($user_id){
				$user_data = array('innovator_id' 		=> $user_id,
								   'analyst_creator_id' => $this->user_sess['user_id']);
				$this->innovator->add($user_data);
			}	
		}else{
			$user_id = $this->input->post('user_id');
			if($this->input->post('innovator_status') == INNO_APPROVED){
				$this->user_notification->send_multi_user(NOTIF_EDIT_INNOVATOR,$user_id);
			}
			$this->user->edit($user_id,$data_post);
		}		
		
		if($user_id){
			$this->session->set_flashdata('inno_form_msg', array('success' =>true, 'msg' => lang('msg_success_save_account')));
		}else{
			$this->session->set_flashdata('inno_form_msg', array('success' =>false, 'msg' => lang('msg_failed_save_account')));
		}
		
		redirect(base_url()."innovators/manage/innovator/".$user_id);
	}
	
	//INNOVATOR DATA
	public function innovator_form($id){
		$this->load->model('site');
		$this->scripts 	= array("front/innovator","jquery.validate","front/form_validation","front/innovator","jquery.form");
		
		$data['identities']	 		= unserialize(INNOVATOR_IDENTITY);
		$data['states']				= $this->site->get_state();
		$data['citizenships']		= unserialize(INNOVATOR_CITIZENSHIP);
		$data['races']				= unserialize(INNOVATOR_RACE);
		$data['marital_statuses'] 	= unserialize(INNOVATOR_MARITAL_STATUS);
		$data['detail']				= $this->innovator->get(array('innovator_id'=>$id))->row_array();
		$data['links']			 	= $this->set_form_links('innovator',$id,($data['detail']['d_innovator_has'] == INNOVATOR_HAS_BUSINESS));
		$data['kod_no']				= explode("-",$data['detail']['kod_no']);
		$data['team_experts']		= $this->innovator->get_innovator_team_expert(array('innovator_id'=>$id))->result_array();
		$this->load->view('innovator/innovator',$data);
	}
	
	//SAVE INNOVATOR
	function save_innovator(){
		$this->load->model(array('site','user_notification'));
		
		$id			= $this->input->post('innovator_id');
		$data_post 	= array('identity' 		=> $this->input->post('identity'),
							'name' 			=> $this->input->post('name'),
							'email' 		=> $this->input->post('email'),
							'kod_no' 		=> $this->input->post('kod_no1')."-".$this->input->post('kod_no2')."-".$this->input->post('kod_no3'),
							'gender' 		=> $this->input->post('gender'),
							'age' 			=> $this->input->post('age'),
							'place_of_birth'=> $this->input->post('place_of_birth'),
							'citizenship' 	=> $this->input->post('citizenship'),
							'race' 			=> $this->input->post('race'),
							'marital_status'=> $this->input->post('marital_status'),
							'address' 		=> $this->input->post('address'),
							'postcode' 		=> $this->input->post('postcode'),
							'city' 			=> $this->input->post('city'),
							'state_id' 		=> $this->input->post('state_id'),
							'geo_location'	=> $this->input->post('geo_location'));
		
		//upload innovator picture
		if(isset($_FILES['picture'])){
			if($_FILES['picture']['name'] != NULL){
				$bef_pict = $this->input->post('bef_picture');
				if($bef_pict != NULL){
					$this->site->_remove($bef_pict,PATH_TO_INNOVATOR_PICTURE);
				}
				$filename 			  = $id."_".$_FILES['picture']['name'];
				$data_post['picture'] = $this->site->_upload($filename,'picture',PATH_TO_INNOVATOR_PICTURE);
			}
		}
		
		//innovator team
		if($data_post['identity'] == INNOVATOR_IDENTITY_TEAM){
			$data_post['name'] 	= $this->input->post('team_leader');
			$team_experts 		= $this->input->post('team_expert');
			$team_expert_ids	= $this->input->post('team_expert_id');
			for($i=0;$i<count($team_experts);$i++){
				if(!empty($team_experts[$i])){
					if(isset($team_expert_ids[$i])){
						$this->innovator->edit_innovator_team_expert($team_expert_ids[$i],array('name'=> $team_experts[$i]));
					}else{
						$this->innovator->add_innovator_team_expert(array('innovator_id' => $id,'name'=> $team_experts[$i]));
					}
				}
			}
		}
		
		if($this->innovator->edit($id,$data_post)){
			if($this->input->post('innovator_status') == INNO_APPROVED){
				$this->user_notification->send_multi_user(NOTIF_EDIT_INNOVATOR,$id);
			}

			$this->session->set_flashdata('inno_form_msg', array('success' =>true, 'msg' => lang('msg_success_save_innovator')));
		}else{
			$this->session->set_flashdata('inno_form_msg', array('success' =>false, 'msg' => lang('msg_failed_save_innovator')));
		}

		$this->layout = false;
		$result = array('innovator_id' => $id, 'res_url' => base_url()."innovators/manage/demographic/".$id);
		echo json_encode($result);
	}
	
	//INNOVATOR DEMOGRAPHIC FORM
	public function demographic_form($id){
		$this->scripts 	= array("jquery.validate","front/form_validation","front/innovator");
		
		$data['locations']	 = unserialize(DEMOGRAPHIC_LOCATION_TYPE);
		$data['educations']	 = unserialize(DEMOGRAPHIC_EDUCATION_LEVEL);
		$data['detail']		 = $this->innovator->get(array('innovator_id'=>$id))->row_array();
		$data['links']		 = $this->set_form_links('demographic',$id,($data['detail']['d_innovator_has'] == INNOVATOR_HAS_BUSINESS));
		$this->load->view('innovator/demographic',$data);
	}
	
	//SAVE DEMOGRAPHIC
	function save_demographic(){
		$this->load->model(array('user_notification'));

		$id			= $this->input->post('innovator_id');
		$data_post 	= array('d_location_type' 		=> $this->input->post('d_location_type'),
							'd_home_phone_no' 		=> $this->input->post('d_home_phone_no'),
							'd_official_phone_no' 	=> $this->input->post('d_official_phone_no'),
							'd_mobile_phone_no' 	=> $this->input->post('d_mobile_phone_no'),
							'd_fax_no' 				=> $this->input->post('d_fax_no'),
							'd_education_level' 	=> $this->input->post('d_education_level'),
							'd_employment'			=> $this->input->post('d_employment'),
							'd_institution_name' 	=> $this->input->post('d_institution_name'),
							'd_employer' 			=> $this->input->post('d_employer'),
							'd_position'			=> ucfirst($this->input->post('d_position')),
							'd_innovator_has'		=> $this->input->post('d_innovator_has'));
		
		if($this->innovator->edit($id,$data_post)){
			if($this->input->post('innovator_status') == INNO_APPROVED){
				$this->user_notification->send_multi_user(NOTIF_EDIT_INNOVATOR,$id);
			}

			$this->session->set_flashdata('inno_form_msg', array('success' =>true, 'msg' => lang('msg_success_save_demographic')));
		}else{
			$this->session->set_flashdata('inno_form_msg', array('success' =>false, 'msg' => lang('msg_success_failed_demographic')));
		}
		
		if($data_post['d_innovator_has'] == INNOVATOR_HAS_BUSINESS){
			redirect(base_url()."innovators/manage/company/".$id);
		}else{
			redirect(base_url()."innovators/manage/heir/".$id);
		}
	}
	
	//INNOVATOR COMPANY FORM
	public function company_form($id){
		$this->load->model(array('site'));
		$this->scripts 	= array("jquery.validate","front/form_validation");
		
		$data['states']	 	  = $this->site->get_state();
		$data['fund_sources'] = unserialize(DEMOGRAPHIC_FUNDING_SOURCE);
		$data['detail']		  = $this->innovator->get(array('innovator_id'=>$id))->row_array();
		$data['links']		  = $this->set_form_links('company',$id,($data['detail']['d_innovator_has'] == INNOVATOR_HAS_BUSINESS));
		$data['bef_reg_cert'] = $this->site->show_attachment($data['detail']['c_registered_certification'],PATH_TO_INNOVATOR_COMPANY_CERT,550,500,250);
		$this->load->view('innovator/company',$data);
	}
	
	//SAVE COMPANY
	function save_company(){
		$this->load->model(array('site','user_notification'));
		
		$id			= $this->input->post('innovator_id');
		$data_post 	= array('c_name' 				=> $this->input->post('c_name'),
							'c_registration_no' 	=> $this->input->post('c_registration_no'),
							'c_registration_date' 	=> $this->input->post('c_registration_date'),
							'c_operation_date' 		=> $this->input->post('c_operation_date'),
							'c_address' 			=> $this->input->post('c_address'),
							'c_postcode' 			=> $this->input->post('c_postcode'),
							'c_city'				=> $this->input->post('c_city'),
							'c_state_id' 			=> $this->input->post('c_state_id'),
							'c_telp_no' 			=> $this->input->post('c_telp_no'),
							'c_fax_no'				=> $this->input->post('c_fax_no'),
							'c_email'				=> $this->input->post('c_email'),
							'c_business_nature'		=> $this->input->post('c_business_nature'),
							'c_funding_sources' 	=> $this->input->post('c_funding_sources'),
							'c_grant_loan_detail'	=> $this->input->post('c_grant_loan_detail'));
							
		//upload registered certification
		if($_FILES['reg_cert']['name'] != NULL){
			$bef_cert = $this->input->post('bef_reg_cert');
			if($bef_cert != NULL){
				$this->site->_remove($bef_cert,PATH_TO_INNOVATOR_COMPANY_CERT);
			}
			$filename 			  					 = $id."_".$_FILES['reg_cert']['name'];
			$data_post['c_registered_certification'] = $this->site->_upload($filename,'reg_cert',PATH_TO_INNOVATOR_COMPANY_CERT,'attach');
		}
			
		if($this->innovator->edit($id,$data_post)){
			if($this->input->post('innovator_status') == INNO_APPROVED){
				$this->user_notification->send_multi_user(NOTIF_EDIT_INNOVATOR,$id);
			}

			$this->session->set_flashdata('inno_form_msg', array('success' =>true, 'msg' => lang('msg_success_save_company')));
		}else{
			$this->session->set_flashdata('inno_form_msg', array('success' =>false, 'msg' => lang('msg_failed_save_company')));
		}
		redirect(base_url()."innovators/manage/heir/".$id);
	}
	
	//INNOVATOR HEIR FORM
	public function heir_form($id){
		$this->scripts 	= array("front/innovator","jquery.validate","front/form_validation");
		
		$data['detail']		  = $this->innovator->get(array('innovator_id'=>$id))->row_array();
		$data['links']		  = $this->set_form_links('heir',$id,($data['detail']['d_innovator_has'] == INNOVATOR_HAS_BUSINESS));
		$data['kod_no']		  = explode("-",$data['detail']['h_kod_no']);
		$data['relations']	  = unserialize(HEIR_RELATIONSHIP);
		$this->load->view('innovator/heir',$data);
	}
	
	//SAVE HEIR
	function save_heir(){
		$this->load->model(array('user_notification'));

		$id			= $this->input->post('innovator_id');
		$data_post 	= array('h_name' 			=> $this->input->post('h_name'),
							'h_kod_no' 			=> $this->input->post('kod_no1')."-".$this->input->post('kod_no2')."-".$this->input->post('kod_no3'),
							'h_relationship' 	=> $this->input->post('h_relationship'),
							'h_age' 			=> $this->input->post('h_age'),
							'h_mobile_phone_no' => $this->input->post('h_mobile_phone_no'),
							'h_employment' 		=> $this->input->post('h_employment'));
		
		//set flashdata
		if($this->innovator->edit($id,$data_post)){
			if($this->input->post('innovator_status') == INNO_APPROVED){
				$this->user_notification->send_multi_user(NOTIF_EDIT_INNOVATOR,$id);
			}

			$this->session->set_flashdata('inno_form_msg', array('success' =>true, 'msg' => lang('msg_success_save_heir')));
		}else{
			$this->session->set_flashdata('inno_form_msg', array('success' =>false, 'msg' => lang('msg_failed_save_heir')));
		}
		redirect(base_url()."innovators/manage/innovation/".$id);
	}
	
	//INNOVATOR INNOVATION FORM
	public function innovation_form($id){
		$this->scripts = array("jquery.validate","front/form_validation","front/innovation","jquery.dataTables.min","dataTables_initialize");
		$this->styles 	= array('jquery.dataTables');
		
		$data['innovator']	 = $this->innovator->get(array('innovator_id'=>$id))->row_array();
		$data['innovations'] = $this->innovator->get_innovator_innovation(array('ii.innovator_id' => $id));
		$data['links']		 = $this->set_form_links('innovation',$id,($data['innovator']['d_innovator_has'] == INNOVATOR_HAS_BUSINESS));
		if($data['innovator']['d_innovator_has'] == INNOVATOR_HASNT_BUSINESS){
			$data['can_sent_ap'] = ($this->innovator->get(array('innovator_id'=>$id, 'name !='=>'', 'd_mobile_phone_no !='=>'', 'h_name !='=>'', 'status'=>INNO_DRAFT))->num_rows() && count($data['innovations']) > 0) ;
		}else{
			$data['can_sent_ap'] = ($this->innovator->get(array('innovator_id'=>$id, 'name !='=>'', 'd_mobile_phone_no !='=>'', 'c_name !='=>'', 'h_name !='=>'', 'status'=>INNO_DRAFT))->num_rows() && count($data['innovations']) > 0) ;
		}
			
		$data['id']			 = $id;
		$data['statuses']	 = unserialize(INNO_STATUSES);
		$data['access']		 = $this->accessible;
		$this->load->view('innovation/list',$data);
	}
	
	//VIEW INNOVATOR DETAIL
	public function view($page,$id,$show_back_btn = 1){
		$this->scripts = array("jquery.dataTables.min","dataTables_initialize");
		$this->styles 	= array('jquery.dataTables');
		
		$data['page'] 	= $page;
		$data['detail'] = $this->innovator->get(array('innovator_id' => $id))->row_array();
		if($page == 'innovator'){
			$this->load->model('site');
			$data['identities']	 	  = unserialize(INNOVATOR_IDENTITY);
			$data['states']			  = $this->site->get_state();
			$data['citizenships']	  = unserialize(INNOVATOR_CITIZENSHIP);
			$data['races']			  = unserialize(INNOVATOR_RACE);
			$data['marital_statuses'] = unserialize(INNOVATOR_MARITAL_STATUS);
			$data['teams']			  = NULL;
			if($data['detail']['identity'] == INNOVATOR_IDENTITY_TEAM){
				$data['teams'] = $this->innovator->get_innovator_team_expert(array('innovator_id'=>$id))->result_array();
			}
		}elseif($page == 'demographic'){
			$data['locations']	 	  = unserialize(DEMOGRAPHIC_LOCATION_TYPE);
			$data['educations']	 	  = unserialize(DEMOGRAPHIC_EDUCATION_LEVEL);
		}elseif($page == 'company'){
			$this->load->model('site');
			$data['states']			  = $this->site->get_state();
			$data['fund_sources'] 	  = unserialize(DEMOGRAPHIC_FUNDING_SOURCE);
			for($i=0;$i<count($data['detail']);$i++){
				$data['detail']['reg_cert'] = $this->site->show_attachment($data['detail']['c_registered_certification'],PATH_TO_INNOVATOR_COMPANY_CERT,550,500,250);
			}
		}elseif($page == 'heir'){
			$data['relations']		  = unserialize(HEIR_RELATIONSHIP);
		}else{
			$data['innovations']	  = $this->innovator->get_innovator_innovation(array('ii.innovator_id' => $id));
			$data['statuses']	 	  = unserialize(INNO_STATUSES);
		}
		
		$data['forms']	= array(0 => array('name' => 'Inovator', 'url' => 'innovator', 'part' => 'A'),
								1 => array('name' => 'Demografik', 'url' => 'demographic', 'part' => 'B'),
								3 => array('name' => 'Waris', 'url' => 'heir', 'part' => 'D'),
								4 => array('name' => 'Inovasi', 'url' => 'innovation', 'part' => 'E'));
		if($data['detail']['c_name'] != ""){
			$data['forms'][2] = array('name' => 'Syarikat', 'url' => 'company', 'part' => 'C');
		}
		ksort($data['forms']);
		$data['show_back_btn'] = $show_back_btn;		
		$this->load->view('innovator/view',$data);
	}
	
	//SEND APPROVAL REQUEST
	function send_approval(){
		$this->load->model(array('innovation','user','user_notification'));
		$id = $this->input->post('innovator_id');
		
		//update innovator status
		if($this->innovator->edit($id,array('status' => INNO_SENT_APPROVAL))){
			//update innovations status
			$innovations = $this->innovation->get_innovation_innovator(array('ii.innovator_id'=>$id));		
			foreach($innovations as $innovation){
				$this->innovation->edit($innovation['innovation_id'], array('status'=>INNO_SENT_APPROVAL));
			}
			//send notification
			$this->user_notification->send_multi_user(NOTIF_NEW_INNOVATOR,$id);

			$this->session->set_flashdata('inno_form_msg', array('success' =>true, 'msg' => lang('msg_success_send_request')));
			redirect(base_url()."innovators");
		}
	}
	
	//INNOVATOR APPROVE & REJECT
	function approval(){
		if($this->user_sess){
			$this->load->model(array('innovation','user','user_notification'));
			$innovator_id  = $this->input->post('app_innovator_id');
			$approval_data = array('status' 		=> $this->input->post('app_status'),
								   'approval_note'	=> $this->input->post('app_note'),
								   'approval_date'	=> date('Y-m-d'),
								   'approver_id'	=> $this->user_sess['user_id']);
			
			if($this->innovator->edit($innovator_id,$approval_data)){
				$this->load->model('innovation');
				$innovations = $this->innovator->get_innovator_innovation(array('ii.innovator_id' => $innovator_id));
				
				//approve innovator's innovation
				foreach($innovations as $innovation){
					$this->innovation->edit($innovation['innovation_id'],$approval_data);
				}

				//send notification
				$innovator = $this->innovator->get(array('innovator_id' => $innovator_id))->row_array();
				if($this->input->post('app_status') == INNO_APPROVED){
					$this->user_notification->send_multi_user(NOTIF_APPROVED_INNOVATOR,$innovator_id);
				}else{
					$this->user_notification->send_multi_user(NOTIF_REJECTED_INNOVATOR,$innovator_id);
				}
				
				$this->session->set_flashdata('inno_form_msg', array('success' =>true, 'msg' => ($this->input->post('app_status') == INNO_APPROVED ? lang('msg_success_approve_innovator') : lang('msg_success_reject_innovator'))));
				redirect(base_url()."innovators");
			}
		}else{
			$this->load->view('site/error_page',array("message" => "Your session has expired."));
		}
	}
	
	//DELETE INNOVATOR
	public function delete($id){
		$this->load->model(array('user_notification'));
		
		$detail = $this->innovator->get(array('innovator_id'=>$id))->row_array();

		if($detail['status'] == INNO_APPROVED){
			$this->user_notification->send_multi_user(NOTIF_DELETE_INNOVATOR,$id);
		}

		if($this->innovator->delete($id)){
			//delete innovator user account
			$this->load->model('user');
			$this->user->delete($id);
			
			//delete innovator picture
			$this->load->model('site');
			if($detail['picture'] != ""){
				$this->site->_remove($detail['picture'],PATH_TO_INNOVATOR_PICTURE);
			}
			
			//delete innovator company certificate
			if($detail['c_registered_certification'] != ""){
				$this->site->_remove($detail['c_registered_certification'],PATH_TO_INNOVATOR_COMPANY_CERT);
			}
			
			$this->session->set_flashdata('inno_form_msg', array('success' =>true, 'msg' => lang('msg_success_delete_innovator')));
			redirect(base_url()."innovators");
		}
	}
	
	//DELETE INNOVATOR TEAM EXPERT
	public function delete_team_expert($id){
		$this->layout = false;
		if($this->innovator->delete_innovator_team_expert(array('team_expert_id'=>$id))){
			echo 'success';
		}else{
			echo 'failed';
		}
	}
	
	//RETURN INNOVATOR 5 FORM LINK ON TOP OF PAGE
	function set_form_links($current,$id = NULL,$show_company = true){
		$links_string = "";
		
		//flashdata
		$flashdata 	   = $this->session->flashdata('inno_form_msg');
		if($flashdata != NULL){
			$links_string .= "<div class='form-info ".($flashdata['success'] ? 'success' : 'fail')."'>".$flashdata['msg']."</div>";
		}
		
		$menus = array(0 => array('label' => lang('title_account_form'), 'url' => "account"),
					   1 => array('label' => lang('title_innovator_form'), 'url' => "innovator"),
					   2 => array('label' => lang('title_demographic_form'), 'url' => "demographic"),
					   4 => array('label' => lang('title_heir_form'), 'url' => "heir"),
					   5 => array('label' => lang('title_innovation_form'), 'url' => "innovation"));
		if($show_company){
			$menus[3] = array('label' => lang('title_company_form'), 'url' => "company");
		}
		ksort($menus);
		
		$links_string .= '<ul class="grid_11" id="tabs">';
		foreach($menus as $menu){
			if($menu['url'] != $current){
				if($id != NULL){
					$links_string .= "<li><a href='".base_url()."innovators/manage/".$menu['url']."/".$id."'>".$menu['label']."</a></li>";
				}else{
					$links_string .= "<li><a href='#'>".$menu['label']."</a></li>";
				}
			}else{
				$links_string .= "<li class='active'><a href='#'>".$menu['label']."</a></li>";
			}
		}
		$links_string .= '</ul>';
		return $links_string;
	}

	function get_geo_location(){
		$this->layout	= false;
		$address 		= $this->input->post('address');
		$address 		= str_replace(" ", "+", $address);
		$json 			= file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false");
		$json 			= json_decode($json);
		if($json->status != 'ZERO_RESULTS'){			
			$result		= array('status'	=> true,
								'latitude'  => $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'}, 
								'longitude' => $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'});
		}else{
			$result		= array('status'	=> false,
								'message'	=> "Geographic Location not found!");
		}
		echo json_encode($result);
	}

	function manual_approval($limit_id){
		$this->layout = FALSE;
		if($this->user_sess){
			$this->load->model(array('innovation','user','user_notification'));

			$innovators = $this->innovator->get("status < 2 AND innovator_id < ".$limit_id)->result_array();

			foreach ($innovators as $key => $value) {
				$innovator_id  = $value['innovator_id'];
				$approval_data = array('status' 		=> INNO_APPROVED,
									   'approval_note'	=> "-",
									   'approval_date'	=> date('Y-m-d'),
									   'approver_id'	=> $this->user_sess['user_id']);
				
				if($this->innovator->edit($innovator_id,$approval_data)){
					$this->load->model('innovation');
					$innovations = $this->innovator->get_innovator_innovation(array('ii.innovator_id' => $innovator_id));
					
					//approve innovator's innovation
					foreach($innovations as $innovation){
						$this->innovation->edit($innovation['innovation_id'],$approval_data);
					}

					//send notification
					$innovator = $this->innovator->get(array('innovator_id' => $innovator_id))->row_array();
					if($approval_data['status'] == INNO_APPROVED){
						$this->user_notification->send_multi_user(NOTIF_APPROVED_INNOVATOR,$innovator_id);
					}else{
						$this->user_notification->send_multi_user(NOTIF_REJECTED_INNOVATOR,$innovator_id);
					}
					
					//$this->session->set_flashdata('inno_form_msg', array('success' =>true, 'msg' => ($this->input->post('app_status') == INNO_APPROVED ? lang('msg_success_approve_innovator') : lang('msg_success_reject_innovator'))));
					//redirect(base_url()."innovators");
				}
			}
			
		}else{
			$this->load->view('site/error_page',array("message" => "Your session has expired."));
		}
	}
}

/* End of file innovators.php */
/* Location: ./application/controllers/innovators.php */