<?php 
require_once('common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Management_dashboard extends Common {
	function __construct() {
		parent::__construct("management_dashboard");
		
		$this->meta 			= array();
		$this->scripts 			= array('jquery.dataTables.min','jquery.form', 'highcharts','highcharts-more', 'front/management_dashboard');
		$this->styles 			= array();
		$this->title 			= "";
		
		$this->load->model('report');
	}
	
	public function index(){		
		$this->view();
	}

	public function view(){
		$this->load->view('management_dashboard/index');	
	}

	public function innovation_year_handler(){
		$this->layout 	= FALSE;
		$years 			= $this->report->get_year()->result_array();
		
		//result data
		foreach($years as $key=>$year){
			$y = $this->report->get_innovation(array('YEAR(discovered_date)' => $year['year']))->num_rows();
			$result['value'][] = array('id' => $key,'seri' => $year['year'],'y' => $y);
			$result['title'][] = $year['year'];
		}
		echo json_encode($result);
	}

	public function innovation_category_handler(){
		$this->layout 	= FALSE;
		$categories 	= $this->report->get_category()->result_array();
		$year			= $this->input->post('year');
		$data_exist 	= $this->report->get_innovation_by_category(array('YEAR(i.discovered_date)'=> $year))->num_rows();
		
		if($data_exist > 0){
			foreach($categories as $category){			
				$counter = $this->report->get_innovation_by_category(array('ic.category_id'=> $category['category_id'],'YEAR(i.discovered_date)'=> $year))->num_rows();
				$result['title'][] = $category['name'];
				$result['data'][]  = array('name' => $category['name'],'y' => $counter,'id' => $category['category_id']);
			}
		}
		$result['data_exist'] = $data_exist;
		$result['year'] = $year;
		echo json_encode($result);
	}

	public function innovation_state_handler(){
		$this->layout 	= FALSE;
		$states 		= $this->report->get_state()->result_array();
		$year			= $this->input->post('year');
		$data_exist 	= $this->report->get_innovation_by_state(array('YEAR(i.discovered_date)'=> $year))->num_rows();

		if($data_exist > 0){
			foreach($states as $state){			
				$counter = $this->report->get_innovation_by_state(array('inv.state_id'=> $state['state_id'],'YEAR(i.discovered_date)'=> $year))->num_rows();
				$result['title'][] = $state['name'];
				$result['data'][]  = array('name' => $state['name'],'y' => $counter,'id' => $state['state_id']);
			}
		}
		$result['data_exist'] = $data_exist;
		$result['year'] = $year;
		echo json_encode($result);	
	}

	public function innovation_evaluation_handler(){
		$this->layout = FALSE;
		$count_innovation_eval = $this->report->get_expert_task()->num_rows();
		$count_eval_done = $this->report->get_innovation_evaluation(array('innovation_evaluation.status' => EVALUATION_DONE))->num_rows();
		$result = array('min' => 0, 'max' => $count_innovation_eval, "data" => array($count_eval_done));
		echo json_encode($result);
	}

	public function innovation_portfolio_handler(){
		$this->layout = FALSE;
		$count_portfolio = $this->report->get_portfolio_assignment()->num_rows();
		$count_portfolio_done = $this->report->get_portfolio_assignment(array('status' => PORTFOLIO_TASK_DONE))->num_rows();
		$result = array('min' => 0, 'max' => $count_portfolio, "data" => array($count_portfolio_done));
		echo json_encode($result);
	}
}