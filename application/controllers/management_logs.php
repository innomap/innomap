<?php 
require_once('common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Management_logs extends Common {
	function __construct() {
		parent::__construct("innovations");
		
		$this->meta 			= array();
		$this->scripts 			= array();
		$this->styles 			= array();
		$this->title 			= "Management Logs";
		
		$this->load->model('management_log');
	}

	public function index(){
		$this->scripts 	= array("jquery.dataTables.min","dataTables_initialize");
		$this->styles 	= array('jquery.dataTables');
		$this->load->helper('mystring');

		$data['logs'] = $this->management_log->get_list(array('user_id' => $this->user_sess['user_id']))->result_array();
		$data['access'] = $this->accessible;
		$data['flashdata'] = $this->session->flashdata('inno_form_msg');

		$this->load->view('management_log/list',$data);
	}

	public function add(){
		$this->scripts[] = 'front/management_log';
		$this->load->model('innovation');
		$data['innovations'] = $this->management_log->get_list_innovation_by_user(array('pa.manager_id' => $this->user_sess['user_id']))->result_array();

		if(count($data['innovations']) > 0){
			$data['innovators'] = $this->innovation->get_innovation_innovator(array('ii.innovation_id' => $data['innovations'][0]['innovation_id']));
		}
		$this->load->view('management_log/form',$data);
	}

	public function save(){
		$this->layout = FALSE;

		$log_id = $this->input->post('management_log_id');
		$data = array('user_id' => $this->user_sess['user_id'],
						'date' => $this->input->post('date'),
						'venue' => $this->input->post('venue'),
						'notes' => $this->input->post('notes'),
						'updated_at' => date('Y-m-d H:i:s'));

		if($log_id == '-1'){
			$data['innovation_id'] = $this->input->post('innovation_id');
			$data['created_at'] = date('Y-m-d H:i:s');
			$this->management_log->add($data);
		}else{
			$this->management_log->edit($log_id,$data);
		}

		$this->session->set_flashdata('inno_form_msg', array('success' =>true, 'msg' => lang('msg_success_save_log')));	
		redirect(site_url('management_logs'));
	}

	public function edit($id){
		$this->load->model('innovation');
		$this->scripts[] = 'front/management_log';
		$data['mode_edit'] = true;
		$data['log'] = $this->management_log->get_list(array('management_log_id' => $id))->row_array();
		$data['innovators'] = $this->innovation->get_innovation_innovator(array('ii.innovation_id' => $data['log']['innovation_id']));
		$this->load->view('management_log/form',$data);
	}

	public function view($id){
		$this->load->model('innovation');
		$this->styles[] = "media_print";

		$data['log'] = $this->management_log->get_list(array('management_log_id' => $id))->row_array();
		$data['innovators'] = $this->innovation->get_innovation_innovator(array('ii.innovation_id' => $data['log']['innovation_id']));
		$this->load->view('management_log/detail',$data);
	}

	public function get_innovators($innovation_id){
		$this->layout = FALSE;
		$this->load->model('innovation');
		$result = $this->innovation->get_innovation_innovator(array('ii.innovation_id' => $innovation_id));

		echo json_encode($result);
	}
}