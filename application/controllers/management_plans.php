<?php 
require_once('common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Management_plans extends Common {
	function __construct() {
		parent::__construct("innovations");
		
		$this->meta 			= array();
		$this->scripts 			= array();
		$this->styles 			= array();
		$this->title 			= "Management Plans";
		
		$this->load->model('management_plan');
	}

	public function index(){
		$this->scripts 	= array("jquery.dataTables.min","dataTables_initialize");
		$this->styles 	= array('jquery.dataTables');
		$this->load->helper('mystring');

		$this->load->model(array('innovation'));
		
		$innovations = $this->management_plan->get_list(array('user_id' => $this->user_sess['user_id']))->result_array();
		
		foreach($innovations as $key=>$value){
			$str_innovators = '';
			$innovators = $this->innovation->get_innovation_innovator(array('ii.innovation_id' => $value['innovation_id']));

			foreach($innovators as $innovator){
				$str_innovators .= "- ".$innovator['name']."<br/>"; 
			}
			$innovations[$key]['innovator'] = $str_innovators;
		}
		
		$data = array('innovations' => $innovations, 'access' => $this->accessible,'flashdata' => $this->session->flashdata('inno_form_msg'));
		$this->load->view('management_plan/list',$data);
	}

	function add(){
		$this->scripts[] = 'front/management_plan';
		$this->load->model(array('innovation'));

		$innovation_list = $this->management_plan->get_list_innovation_by_user(array('pa.manager_id' => $this->user_sess['user_id']))->result_array();

		if(count($innovation_list) > 0){
			$data = $this->get_innovation($innovation_list[0]['innovation_id']);
			$data['innovation_list'] = $innovation_list;

			$data['plan_challenges'] = array();
			$data['plan_targets'] = array();
			$data['plan_notes'] = array();	
		}
		$data['flashdata']		= $this->session->flashdata('inno_form_msg');
		
		$this->load->view('management_plan/form',$data);
	}

	function edit($id){
		$this->load->model(array('management_plan','innovation'));
		$this->scripts[] = 'front/management_plan';

		$plan = $this->management_plan->get(array('management_plan_id' => $id))->row_array();
		if($plan){
			$data = $this->get_innovation($plan['innovation_id']);

			$data['plan'] = $plan;
			$data['plan_challenges'] = $this->management_plan->get_challenge(array('management_plan_id' => $plan['management_plan_id']))->result_array();
			$data['plan_targets'] = $this->management_plan->get_target(array('management_plan_id' => $plan['management_plan_id']))->result_array();
			$data['plan_notes'] = $this->management_plan->get_note(array('management_plan_id' => $plan['management_plan_id']))->result_array();
		}else{
			$data['plan_challenges'] = array();
			$data['plan_targets'] = array();
			$data['plan_notes'] = array();
		}
		
		$data['flashdata']		= $this->session->flashdata('inno_form_msg');
		$data['mode_edit']	= true;
		$this->load->view('management_plan/form',$data);
	}

	function save_management_plan(){
		$this->layout = FALSE;
		$this->load->model('management_plan');

		$id = $this->input->post('management_plan_id');
		$innovation_id = $this->input->post('innovation_id');
		$data = array('user_id' => $this->user_sess['user_id'],
						'innovation_id' => $innovation_id,
						'stage_no' => $this->input->post('stage_no'),
						'certificate_no' => $this->input->post('certificate_no'),
						'updated_at' => date('Y-m-d H:i:s'));

		if($id == '-1'){
			$data['created_at'] = date('Y-m-d H:i:s');
			$id = $this->management_plan->add($data);
		}else{
			$this->management_plan->edit($id,$data);
		}

		$challenge_ids = $this->input->post('challenge_ids');
		$challenges = $this->input->post('challenges');
		$mitigations = $this->input->post('mitigations');

		foreach ($challenges as $key => $value) {
			if($challenge_ids[$key] == '-1'){
				$this->management_plan->add_challenge(array('management_plan_id' => $id, 'challenge' => $value, 'mitigation' => $mitigations[$key]));
			}else{
				$this->management_plan->edit_challenge($challenge_ids[$key],array('management_plan_id' => $id, 'challenge' => $value, 'mitigation' => $mitigations[$key]));
			}
		}

		$target_ids = $this->input->post('target_ids');
		$targets = $this->input->post('targets');
		$deadlines = $this->input->post('deadlines');
		$remarks = $this->input->post('remarks');

		foreach ($targets as $key => $value) {
			if($target_ids[$key] == '-1'){
				$this->management_plan->add_target(array('management_plan_id' => $id, 'year' => date('Y'), 'target' => $value, 'deadline' => $deadlines[$key], 'remark' => $remarks[$key]));
			}else{
				$this->management_plan->edit_target($target_ids[$key],array('management_plan_id' => $id, 'year' => date('Y'), 'target' => $value, 'deadline' => $deadlines[$key], 'remark' => $remarks[$key]));
			}
		}

		$deleted_challenges = $this->input->post('deleted_challenges');
		foreach ($deleted_challenges as $key => $value) {
			if($value != '-1'){
				$this->management_plan->delete_challenge($value);
			}
		}

		$deleted_targets = $this->input->post('deleted_targets');
		foreach ($deleted_targets as $key => $value) {
			if($value != '-1'){
				$this->management_plan->delete_target($value);
			}
		}

		$revision_note = $this->input->post('revision_note_remarks');
		if($revision_note != ""){
			$this->management_plan->add_note(array('management_plan_id' => $id, 'user_id' => $this->user_sess['user_id'], 'remarks' => $revision_note,'created_at' => date('Y-m-d H:i:s')));
		}
		
		$this->session->set_flashdata('inno_form_msg', array('success' =>true, 'msg' => lang('msg_success_save_plan')));	
		redirect(site_url('management_plans/edit/'.$id));
	}

	function view($id){
		$this->load->model(array('management_plan','innovation'));
		$this->styles[] = 'media_print';

		$plan = $this->management_plan->get(array('management_plan_id' => $id))->row_array();
		if($plan){
			$data = $this->get_innovation($plan['innovation_id']);

			$data['plan'] = $plan;
			$data['plan_challenges'] = $this->management_plan->get_challenge(array('management_plan_id' => $plan['management_plan_id']))->result_array();
			$data['plan_targets'] = $this->management_plan->get_target(array('management_plan_id' => $plan['management_plan_id']))->result_array();
			$data['plan_notes'] = $this->management_plan->get_note(array('management_plan_id' => $plan['management_plan_id']))->result_array();
		}else{
			$data['plan_challenges'] = array();
			$data['plan_targets'] = array();
			$data['plan_notes'] = array();
		}
		
		$data['flashdata']		= $this->session->flashdata('inno_form_msg');
		$this->load->view('management_plan/detail',$data);
	}

	function get_innovation_data($innovation_id){
		$this->layout = FALSE;
		$this->load->model('innovation');

		$result = $this->get_innovation($innovation_id);

		echo json_encode($result);
	}

	private function get_innovation($innovation_id){
		$data['innovation'] = $this->innovation->get(array('innovation_id' => $innovation_id))->row_array();
		$data['innovation_images'] = $this->innovation->get_innovation_picture($innovation_id);
		$data['innovator'] = $this->innovation->get_innovation_innovator(array('ii.innovation_id' => $innovation_id), 1);
		if(isset($data['innovator'][0])){
			$data['innovator'] = $data['innovator'][0];
			$birth_date = explode('-',$data['innovator']['kod_no']);

			$data['innovator']['birth_date'] = "19".rtrim(chunk_split($birth_date[0], 2, '/'),'/'); 
			$data['innovator']['gender_label'] = ($data['innovator']['gender'] == 1 ? lang('value_male') : lang('value_female'));
		}

		return $data;
	}
}