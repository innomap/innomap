<?php 
require_once('msi_common.php');

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Locators extends Msi_Common {
	function __construct() {
		parent::__construct();

		$this->meta 			= array();
		$this->scripts 			= array();
		$this->styles 			= array('map','new-front');
		$this->title 			= "Innovation Locator";
		$this->load->helper('mystring_helper');
		$this->load->model(array('locator'));
	}
	
	function index(){
		$this->load->library('googlemaps');
		
		$state	= $this->input->post('state');
		$types 	= $this->input->post('innovation');
		$keyword = $this->input->post('keyword');
		$state = strtolower($state);
		$year = $this->input->post('year');
	
		$data = $this->get_locator($state,$types,$keyword,$state,$year);
		
		$this->load->view(MSI_DIR.'locator/locator', $data);
	}

	function get_locator($state = NULL, $types = NULL, $keyword = NULL, $state = NULL, $year = NULL){
		$state_list = $this->locator->get_state()->result_array();

		//set configuration for google map
		$config['map_height'] 	= '610px';
		
		//set map marker filter by posted data
		if(isset($_POST['search_location'])){
			$pos_state = $state;
			$pos_year = $year;

			$selected_state = $this->locator->get_state(array('state_id' => $pos_state))->row_array();
				if($selected_state){
					$config['center'] = $selected_state['geo_location'];
					if($pos_state == 11){
						$config['zoom'] = '8';
					}else{
						$config['zoom'] = '9';
					}
					$pins 			  = $this->locator->search($state, $types, $keyword, $year);	
				}else{
					$config['center'] = '2.416276,108.655273';
					$config['zoom']   = '6';
					$pins 			  = $this->locator->search(NULL, $types, $keyword, $year);		
				}
			$data['data_search'] = array('state' => $keyword,'types' => $types);	
		}else{
			$pos_state = 1;
			$pos_year = "";
			$selected_state = $this->locator->get_state(array('state_id' => $pos_state))->row_array();
			$config['center'] 		= $selected_state['geo_location'];
			$config['zoom'] 		= '9';

			$pins = $this->locator->search(1, array(1,2,3,4),"",$pos_year);
		}
		$this->googlemaps->initialize($config);
		
		$count_innovation = 0;
		$count_university = 0;
		$count_expert 	  = 0;
		$count_project 	  = 0;
		$kosong = 0;
		
		if($pins){
			foreach($pins as $pin){
				if($pin['geo_location'] != ""){
					$geo_location = explode(",", $pin['geo_location']);
					$pin['latitude'] = (array_key_exists(0, $geo_location) ? $geo_location[0] : '');
					$pin['longitude'] = (array_key_exists(1, $geo_location) ? $geo_location[1] : '');
				}

				if (!empty($pin['latitude']) && !empty($pin['longitude'])){		
					if(isset($pin['innovation_id'])){
						$count_innovation++;
						if($pin['latitude'] == 0){
							$kosong++;
						}
					}else if(isset($pin['university_id'])){
						$count_university++;
					}else if(isset($pin['expert_id'])){
						$count_expert++;
					}else if (isset($pin['project_id'])) {
						$count_project++;
					}
					
					$marker = array();
					$marker['position'] 		  	= $pin['latitude'].",".$pin['longitude'];
					$marker['infowindow_content'] 	= preg_replace("/\r\n|\r|\n/",'<br/>',$this->set_locator_info_window($pin));
					$marker['animation'] 			= 'DROP';
					$marker['icon'] 				= $this->set_locator_icon($pin);
					$marker['title']	 			= (isset($pin['innovation_id'])? htmlspecialchars($pin['innovation']) : (isset($pin['university_id']) ? $pin['university_name'] : (isset($pin['expert_name']) ? $pin['expert_name'] : '')));
					$this->googlemaps->add_marker($marker);
				} 
			}
		}
		
		//pass form data into view
		if($this->session->userdata('language') == LANGUAGE_ENGLISH){
			$data['types']		 = unserialize(LOCATOR_CONTENT_TYPE);
		}else{
			$data['types']		 = unserialize(LOCATOR_CONTENT_TYPE_MELAYU);
		}
		$data['map'] 		= $this->googlemaps->create_map();
		$data['state']		= $state_list;
		$data['pos_state'] = $pos_state; 
		$data['pos_year'] = $pos_year;
		$data['highlight'] = array('innovation' => $count_innovation,'university' => $count_university,'expert' => $count_expert, 'project' => $count_project);

		return $data;
	}
	
	function set_locator_info_window($content){
		$language = $this->session->userdata('language');
		$info_window 	  = '';
		
		//set info if type is innovation
		if(isset($content['innovation_id'])){
			$str_innovator = '';
			$i = 1;
			foreach($content['innovator'][0] as $innovator){
				$str_innovator .= $innovator['name'].($i == count($content['innovator'][0]) ? "" : ", ");
			$i++;}
			
			$str_category = '';
			$j = 1;
			foreach($content['category'][0] as $category){
				$str_category .= $category['category_name'].($j == count($content['category'][0]) ? "" : ", ");
			$j++;}

			$str_picture = '';
			foreach($content['pictures'][0] as $picture){
				$str_picture .= '<img class=pin_pict src='.base_url().'assets/attachment/innovation_picture/'.$picture['picture'].' />';
			$j++;}

			$info_window .= '<strong class=mr>'.lang('label_innovation').'</strong>: '.$content['innovation'].'<br/>';
			$info_window .= '<strong class=mr>'.lang('label_innovator').'</strong>: '.$str_innovator.'<br/>';
			$info_window .= '<div><strong>'.lang('label_product_desc').'</strong>: </div><div style=display:block;width:400px;max-height:100px;>'.htmlspecialchars(ellipsis($content['product_description'],170)).'<a href='.base_url().'innovations/view/'.$content['innovation_id'].' style=font-weight:bold;color:blue;cursor:pointer>Readmore</a></div><br>';
			$info_window .= '<strong class=mr>'.lang('label_category').'</strong>: '.$str_category.'<br/>';
			$info_window .= '<strong class=mr>'.lang('label_address').'</strong>: '.$content['address'].'<br/>';
			$info_window .= '<strong class=mr>'.lang('label_state').'</strong>: '.$content['state_name'].'<br/>';
			$info_window .= '<strong class=mr>Geo-location</strong>: ['.$content['latitude'].','.$content['longitude'].']<br/>';
			$info_window .=  $str_picture;
			
		//set info if type is university
		}elseif(isset($content['university_id'])){
			$info_window .= '<strong>'.lang('label_university').'/'.lang('label_college').'</strong>: '.$content['university_name'].'<br/>';
			$info_window .= '<strong>'.lang('label_address').'</strong>: '.$content['address'].'<br/>';
			$info_window .= '<strong>'.lang('label_state').'</strong>: '.$content['state_name'].'<br/>';
			$info_window .= '<strong>'.lang('label_specialty').'</strong>: '.$content['category'].'<br/>';
			$info_window .= '<strong>'.lang('label_contact_no').'</strong>: '.$content['contact_no'].'<br/>';
			$info_window .= '<strong>Geo-location</strong>: ['.$content['latitude'].','.$content['longitude'].']';
			
		//set info if type is expert
		}elseif(isset($content['expert_id'])){
			$info_window .= '<strong>'.lang('label_expert').'</strong>: '.$content['name'].'<br/>';
			$info_window .= '<strong>'.lang('label_address').'</strong>: '.$content['address'].'<br/>';
			$info_window .= '<strong>'.lang('label_state').'</strong>: '.$content['state_name'].'<br/>';
			$info_window .= '<strong>'.lang('label_expertise').'</strong>: '.$content['category'].'<br/>';
		//	$info_window .= '<strong>'.lang('label_experience').'</strong>: '.$content['experience'].'<br/>';
			$info_window .= '<strong>Geo-location</strong>: ['.$content['latitude'].','.$content['longitude'].']';
		}

		//set info if type is project
		elseif(isset($content['project_id'])){
			$info_window .= '<strong>'.lang('label_project_title').'</strong>: '.$content['title'].'<br/>';
			$info_window .= '<strong>'.lang('label_ref_no').'</strong>: '.$content['ref_no'].'<br/>';
			$info_window .= '<strong>'.lang('label_beneficiary_community').'</strong>: '.$content['beneficiary_community'].'<br/>';
			$info_window .= '<strong>'.lang('label_lead_agency').'</strong>: '.$content['lead_agency'].'<br/>';
			$info_window .= '<strong>'.lang('label_collaborator').'</strong>: '.$content['collaborator'].'<br/>';
			$info_window .= '<strong>'.lang('label_category').'</strong>: '.$content['category'].'<br/>';
			$info_window .= '<strong>'.lang('label_address').'</strong>: '.$content['address'].'<br/>';
			$info_window .= '<strong>Geo-location</strong>: ['.$content['latitude'].','.$content['longitude'].']<br/>';
			$info_window .= '<strong>'.lang('label_duration').'</strong>: '.$content['duration'].'<br/>';
			$info_window .= '<strong>'.lang('label_amount').'</strong>: '.$content['amount'].'<br/>';
		}
		
		return $info_window;
	}
	
	function set_locator_icon($content){
		//set icon if type is innovation
		if(isset($content['innovation_id'])){
			$icon = base_url().'assets/img/map_marker/innovation.png';
			
		//set icon if type is university
		}elseif(isset($content['university_id'])){
			$icon = base_url().'assets/img/map_marker/university.png';
			
		//set icon if type is expert
		}elseif(isset($content['expert_id'])){
			$icon = base_url().'assets/img/map_marker/expert.png';

		//set icon if type is project
		}elseif(isset($content['project_id'])){
			$icon = base_url().'assets/img/map_marker/project.png';
		}
		
		return $icon;
	}
	
	function state_income(){
		$data['categories'] = $this->locator->get_category()->result();
		$this->load->view(MSI_DIR."locator/state_income",$data);
	}

	function elections(){
		$state_list = $this->locator->get_state()->result_array();
		$data['states'] = $state_list;
		$data['categories'] = $this->locator->get_category()->result();
		$this->load->view(MSI_DIR."locator/elections",$data);	
	}

	function get_data_state_income(){
		$this->layout = FALSE;
		$this->load->model(array("locator", "commercial"));
		$data = array();
		$category_id = $this->input->post('category_id');
		$state_id = $this->input->post('state_id');
		$election = $this->input->post('election');
		$sort_by = $this->input->post('sort_by');
		$commercial_center = $this->input->post('commercial_center');

		$data['marker']['innovation'] = array();
		$data['marker']['university'] = array();
		$data['marker']['expert'] = array();
		$data['marker']['election'] = array();
		$data['marker']['utc']	= array();
		$data['marker']['rtc'] = array();
		
		//innovation
		$where = '';
		if($state_id != 0){
			$where .= 'innovator.state_id = '.$state_id;
		}
		if($category_id[0] == 0){
			$innovations = $this->locator->get_innovation_category_locator($where);
		}else{
			if(count($category_id) != 0){
				$where .= ($where != '' ? ' AND ' : '');
				$where .= (count($category_id) > 1 ? '(' :'');
				for($i=0;$i<count($category_id);$i++){
					$where .= 'inno_cat.category_id = ';
					$where .= $category_id[$i];
					if($i != count($category_id)-1){
						$where .= ' OR ';
					}
				}
				$where .= (count($category_id) > 1 ? ')' :'');
			}

			$innovations = $this->locator->get_innovation_category_locator($where);
		}
		
		foreach($innovations as $innovation){
			$data['marker']['innovation'][] = $innovation;
		}
		
		//university
		$where_univ = array();
		if($state_id != 0){
			$where_univ = array('university.state' => $state_id);
		}
		$universities = $this->locator->get_university_locator($where_univ)->result();
		foreach($universities as $university){
			$data['marker']['university'][] = $university;
		}
		
		//expert
		$data_expert = $this->locator->get_expert_locator()->result();
		foreach($data_expert as $expert){
			$data['marker']['expert'][] = $expert;
		}

		//elections
		if($election){
			$where_election = array();
			if($state_id != 0){
				$where_election['state_id'] = $state_id;
			}
			if($sort_by != ""){
				$where_election['type'] = $sort_by;
			}
			$data_election = $this->locator->get_map_election($where_election)->result_array();
			
			foreach($data_election as $election){
				$data['marker']['election'][] = $election;
			}			
		}else{
			if($commercial_center == 0 || $commercial_center == 1){
				$data_utc = $this->commercial->get(array('type'=>1))->result_array();
				foreach ($data_utc as $utc) {
					$data['marker']['utc'][] = $utc;
				}
			}

			if($commercial_center == 0 || $commercial_center == 2){
				$data_rtc = $this->commercial->get(array('type'=>2))->result_array();
				foreach ($data_rtc as $rtc) {
					$data['marker']['rtc'][] = $rtc;
				}
			}
		}
		
		echo json_encode($data);
	}
}