<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Msi_common extends CI_Controller {
	function __construct($module=null) {
		parent::__construct();
		
		$this->load->model(array('msi_session','user_session'));
		$this->load->helper('language');
		$this->session->set_userdata('language', LANGUAGE_MELAYU);

		$user = $this->msi_session->get();
		$user_session = $this->user_session->get();
		$this->user_data = $user;
		if (!$user){
			if(!$user_session){
				if($module != 'site'){
					redirect(site_url(MSI_DIR."site/no_access/" ));
				}
				$this->is_logged_in = false;
			}else{
				$this->user_data = $user_session;
				$this->is_logged_in = true;
			}
		}else{
			$this->is_logged_in = true;
		}

		$this->layout = "msi";
		$data_head = array('user' => $this->user_data,
						   'lang' =>  $this->session->userdata('language'));
							
		$this->parts['header'] = $this->load->view(MSI_DIR.'partial/header', $data_head, true);
		$this->parts['footer'] = "";

		//language
		if($this->session->userdata('language') == NULL){
			$this->session->set_userdata('language', 'melayu');
		}

		//set language
		$this->inno_language = $this->session->userdata("language");
		$this->lang->load('inno',$this->inno_language);
	}

	function set_lang($lang){
		$this->load->library('user_agent');
		$this->session->set_userdata('language', $lang);
		redirect($this->agent->referrer());
	}
}