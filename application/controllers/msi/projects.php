<?php 
require_once('msi_common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Projects extends Msi_common {
	
	function __construct() {
		parent::__construct();
		
		$this->meta 			= array();
		$this->scripts 			= array("jquery.validate","front/form_validation",'msi/general');
		$this->styles 			= array();
		$this->title 			= "";
		$this->load->model(array('project'));
	}
	
	public function index(){
		$data = array('projects' => $this->project->get()->result_array(),
					  'message'	=> $this->session->flashdata('success_msg'));
		$this->load->view(MSI_DIR.'project/index',$data);
	}

	function add(){
		$data = array('mode' => 'ADD','states' => $this->project->get_state()->result_array());
		$this->load->view(MSI_DIR.'project/form',$data);	
	}

	function edit($id){
		$data = array('mode' => 'EDIT',
					  'project' => $this->project->get(array('project_id' => $id))->row_array(),
					  'states' => $this->project->get_state()->result_array(),
					  'id' => $id,
					);
		$this->load->view(MSI_DIR.'project/form',$data);
	}

	function view($id){
		$data = array('mode' => 'VIEW',
					  'project' => $this->project->get(array('project_id' => $id))->row_array(),
					  'states' => $this->project->get_state()->result_array(),
					  'id' => $id,
					);
		$this->load->view(MSI_DIR.'project/form',$data);
	}

	function delete($id){
		if($this->project->delete($id)){
			$this->session->set_flashdata('success_msg', 'Project has been deleted.');
			redirect(site_url(MSI_DIR.'projects'));
		}
	}

	function save(){
		$this->layout = FALSE;
		if(isset($_POST['submit'])){
			$mode = $this->input->post('mode');
		
			$data_post = array('title' 					=> $this->input->post('title'), 
							   'ref_no' 				=> $this->input->post('ref_no'), 
							   'beneficiary_community' 	=> $this->input->post('beneficiary_community'),
							   'lead_agency'			=> $this->input->post('lead_agency'),
							   'collaborator'			=> $this->input->post('collaborator'),
							   'category'				=> $this->input->post('category'),
							   'address'				=> $this->input->post('address'),
							   'state_id'				=> $this->input->post('state_id'),
							   'geo_location' 			=> $this->input->post('geo_location'),
							   'duration' 				=> $this->input->post('duration'),
							   'amount'		 			=> $this->input->post('amount'),
							   );

			if($mode == 'ADD'){
				if($project_id = $this->project->add($data_post)){
					$this->session->set_flashdata('success_msg', 'Project has been saved.');
				}
			}else if($mode == 'EDIT'){
				$project_id = $this->input->post('project_id');
				if($this->project->edit($project_id,$data_post)){
					$this->session->set_flashdata('success_msg', 'Portfolio Criteria has been saved.');
				}
			}
			redirect(site_url(MSI_DIR.'projects'));
		}
	}
}