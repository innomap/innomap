<?php 
require_once('msi_common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends Msi_common {
	
	function __construct() {
		parent::__construct("site");
		
		$this->meta 			= array();
		$this->scripts 			= array();
		$this->styles 			= array();
		$this->title 			= "";
		$this->load->model(array('msi_session','user_session'));
	}
	
	public function index(){
		$msi_user = $this->msi_session->get();
		$user = $this->user_session->get();

		if($user != NULL || $msi_user != NULL){
			redirect(base_url(MSI_DIR."projects"));
		}else{
			redirect(site_url(MSI_DIR."site/login"));
		}
	}

	function login() {
		$data['message'] = $this->session->flashdata('login_msg');
		$this->load->view(MSI_DIR."site/login",$data);
	}
	
	function validate() {
		$this->layout = false;
		$login_success = false;
		if ($this->input->post("submit")) {
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			
			if (isset($username) && isset($password)) {
				if ($this->msi_session->create($username, $password)) {
					$login_success = true;
				}
			}
		}
		if(!$login_success){
			$this->session->set_flashdata('login_msg', 'Access denied. Incorrect username/password');
			redirect(site_url(MSI_DIR.'site/login'));
		}else{
			$this->session->set_flashdata('login_msg', 'Login Success');
			redirect(site_url(MSI_DIR.'site'));
		}
		echo json_encode($response);
	}

	function logout() {
		$this->msi_session->clear();
		redirect(site_url(MSI_DIR));
	}
	
	function no_access(){
		redirect(site_url(MSI_DIR));
	}

	function get_geo_location(){
		$this->layout	= false;
		$address 		= $this->input->post('address');
		$address 		= str_replace(" ", "+", $address);
		$json 			= file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false");
		$json 			= json_decode($json);
		if($json->status != 'ZERO_RESULTS'){			
			$result		= array('status' => true,'latitude' => $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'},'longitude' => $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'});
		}else{
			$result		= array('status' => false,'message' => "Geographic Location not found!");
		}
		echo json_encode($result);
	}
}