<?php 
require_once('common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notes extends Common {
	function __construct() {
		parent::__construct("notes");
		
		$this->meta 			= array();
		$this->scripts 			= array('');
		$this->styles 			= array('');
		$this->title 			= "Notes";
	}
	
	public function index(){
		$user = $this->user_sess;
		$this->load->helper('api');
		$url=INNOMAP_API_URL.'get_notes?email='.$user['email'];
		$data['notes'] = api_get($url);
		$this->scripts = array('gridloading/masonry.pkgd.min');
		$this->styles = array('gridloading/component');
		$this->load->view('note/index',$data);
	}

	function get_detail($note_id){
		$this->layout = FALSE;
		$this->load->helper('api');
		$url=INNOMAP_API_URL.'get_notes_detail/'.$note_id;
		$data['detail'] = api_get($url);
		echo json_encode($data['detail']);
	}
}	
	

/* End of file notes.php */
/* Location: ./application/controllers/notes.php */
?>