<?php require_once('common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notifications extends CI_Controller {
	function __construct() {
		parent::__construct("tasks");

		$this->meta 			= array();
		$this->scripts 			= array('jquery.validate','front/form_validation','front/notification');
		$this->styles 			= array();
		$this->title 			= "Notifications";
		$this->load->model(array('user_notification','user_session','task','role','menu','innovator','evaluation','portfolio','innovation'));
		$this->user_sess = $this->user_session->get();
		
		$this->load->helper('language');
		$this->site_lang = $this->session->userdata('language');
		$this->session->set_userdata('language', 'melayu');
		$this->lang->load('inno',$this->site_lang);		
		
		$data_head = array('message' => $this->session->flashdata('login_msg'),
						   'user' 	 => $this->user_sess,
						   'role'	 => $this->role->get(array('role_id' => $this->user_sess['role_id']))->row_array(),
						   'menus'	 => $this->menu->get_front_menu($this->user_sess['role_id']),
						   'lang'	 => $this->site_lang,
						   'user_roles' => $this->user_sess['roles']);
		$this->parts['head'] 	= $this->load->view('partial/head', $data_head, true);
	}

	public function index(){
		$notif = $this->user_notification->get_by_user($this->user_sess['user_id'], NOTIF_PER_PAGE, 0);
		$notif_data = $this->user_notification->check_notif_content($notif['data']);
		
		$data = array('notif' => array_values($notif_data), 'notif_class'=> unserialize(NOTIF_CLASS),'notif_total' => $notif['total']);
		$this->load->view('notification/index',$data);
	}

	function new_notif(){
		$this->layout = FALSE;
		$notif = $this->user_notification->get(array('user_id_to' => $this->user_sess['user_id'], 'user_notification.status' => NOTIF_STATUS_NEW))->result_array();
		$notif = $this->user_notification->check_notif_content($notif);
		echo json_encode(count($notif));
	}

	function read($notification_id){
		$this->layout = FALSE;
		$notif = $this->user_notification->get(array('notification_id' => $notification_id))->row_array();
		if($this->user_notification->edit($notification_id,array('status' => NOTIF_STATUS_READ))){
			if($notif['notif_type'] == NOTIF_TASK_ASSIGN){
				redirect(site_url('evaluations/task/'.$notif['target_id']));
			}else if($notif['notif_type'] == NOTIF_NEW_INNOVATOR){
				redirect(site_url('innovators/view/innovator/'.$notif['target_id']));
			}else if($notif['notif_type'] == NOTIF_APPROVED_INNOVATOR){
				redirect(site_url('innovators/view/innovator/'.$notif['target_id']));
			}else if($notif['notif_type'] == NOTIF_EVALUATION_DONE){
				redirect(site_url('tasks/evaluation_result/'.$notif['target_id']));
			}else if($notif['notif_type'] == NOTIF_PORTFOLIO_ASSIGN){
				redirect(site_url('portfolios/check/'.$notif['target_id']));
			}else if($notif['notif_type'] == NOTIF_REJECTED_INNOVATOR){
				redirect(site_url('innovators/view/innovator/'.$notif['target_id']));
			}else if($notif['notif_type'] == NOTIF_APPROVED_INNOVATION){
				redirect(site_url('innovations/view/'.$notif['target_id']));
			}else if($notif['notif_type'] == NOTIF_REJECTED_INNOVATION){
				redirect(site_url('innovations/view/'.$notif['target_id']));
			}else if($notif['notif_type'] == NOTIF_PORTFOLIO_COMMENT){
				redirect(site_url('portfolios/view_evaluation/'.$notif['target_id']));
			}else if($notif['notif_type'] == NOTIF_NEW_INNOVATION){
				redirect(site_url('innovations/view/'.$notif['target_id']));
			}else if($notif['notif_type'] == NOTIF_PORTFOLIO_DONE){
				redirect(site_url('portfolios/view_evaluation/'.$notif['target_id']));
			}else if($notif['notif_type'] == NOTIF_EDIT_INNOVATION){
				redirect(site_url('innovations/view/'.$notif['target_id']));
			}else if($notif['notif_type'] == NOTIF_EDIT_INNOVATOR){
				redirect(site_url('innovators/view/innovator/'.$notif['target_id']));
			}else if($notif['notif_type'] == NOTIF_DELETE_INNOVATION || $notif['notif_type'] == NOTIF_DELETE_INNOVATOR || $notif['notif_type'] == NOTIF_DELETE_TASK){
				redirect(site_url('notifications'));
			}else if($notif['notif_type'] == NOTIF_COMMITTEE_MEETING){
				redirect(site_url('committee_meetings/view/'.$notif['target_id']));
			}
		}
	}

	function test_mail($id){
		$this->layout 	= false;
		$this->user_notification->send_email($id);
	}

	function test_mail_user($sender){
		if($sender == 1){
			$from = "admin@mobilus-interactive.com";
		}else{
			$from = "admin@mynef.com";
		}
		$this->load->helper('phpmailer');
		$mail_param 	= array('from'		=> $from,
								'fromname' 	=> "Innomap",
								'to' 		=> "kiki@mobilus-interactive.com",
								'subject' 	=> "Notification",
								'message' 	=> "Test email innomap");
		return phpmailer_send($mail_param, true) || TRUE;
	}

	function load_more(){
		$this->layout  = FALSE;
		$content = "";

		$page_number = $this->input->post('page_number');
		$item_per_page = NOTIF_PER_PAGE;
		$position = ($page_number*$item_per_page);
		
		$notif = $this->user_notification->get_by_user($this->user_sess['user_id'], NOTIF_PER_PAGE, $position);
		
		$notif_data = $this->user_notification->check_notif_content($notif['data']);
		$show_load_more = ($notif['total'] > $position+$item_per_page ? 1 : 0);
		
		foreach($notif_data as $key => $value){
			$content .= $this->create_notif_elem($value);
		}
		echo json_encode(array('content' => $content, 'show_lm' => $show_load_more));
	}

	function create_notif_elem($value){
		$notif_class = unserialize(NOTIF_CLASS);

		$elemen = '';
		$elemen .= '<li>';
		$elemen .= 		'<div class="'.$notif_class[$value['notif_type']].'">';
		$elemen .=			$value['username']." ".$value['message']." <a href='".base_url()."notifications/read/".$value['notification_id']."'>".$value['target']."</a>";
		$elemen .=			'<br/><i>'.$value['created_date'].'</i>';
		$elemen .=		'</div>';
		$elemen .= '</li>';

		return $elemen;
	}

	function send_manager_daily_notif(){
		if(!$this->input->is_cli_request()){ 
	       echo 'Not allowed';
	       exit();
	    }

		$this->layout = FALSE;
		$this->load->helper('phpmailer');
		$this->load->model(array('user', 'portfolio'));
		
		$managers = $this->user->get_user_with_role('user_role.role_id = ' . ROLE_MANAGER . ' OR ' . 'user_role.role_id = ' . ROLE_HIP6_ACCOUNT_MANAGER)->result_array();
		//print_r($managers);
		foreach ($managers as $key => $manager) {
			$tasks = $this->portfolio->get_task_by_manager($manager['user_id']);
			$notes = $this->portfolio->get_note_by_manager($manager['user_id']);
			//echo "<br/><br/>".$manager['username'] . "<br/>";
			//print_r($tasks);
			if(count($tasks) > 0 || count($notes) > 0){
				$message = "Hello ". $manager['username'] .", <br/><h4>Ini adalah tugasan anda untuk hari ini dan esok:</h4>";
				if(count($tasks) > 0){
					$message .= '<table border="1">
									<tr>
										<th>'. lang('portfolio_title'). '</th>
										<th>'. lang('label_criteria'). '</th>
										<th>'. lang('label_task_name'). '</th>
										<th>'. lang('label_start_date'). '</th>
										<th>'. lang('label_end_date'). '</th>
										<th>'. lang('label_weightage'). '</th>
									</tr>';
					foreach ($tasks as $key => $task) {
						$message .= '<tr>
										<td>'. $task['portfolio_title'] . '</td>
										<td>'. $task['criteria_name'] . '</td>
										<td>'. $task['task'] . '</td>
										<td>'. $task['start_date'] . '</td>
										<td>'. $task['end_date'] . '</td>
										<td>'. $task['weightage'] . '%</td>
									</tr>';
					}
					$message .= '</table>';
				}

				if(count($notes) > 0){
					$message .= '<table border="1">
									<tr>
										<th>'. lang('portfolio_title'). '</th>
										<th>'. lang('label_title'). '</th>
										<th>'. lang('label_content'). '</th>
										<th>'. lang('label_creation_date'). '</th>
									</tr>';
					foreach ($notes as $key => $note) {
						$message .= '<tr>
										<td>'. $note['portfolio_title'] . '</td>
										<td>'. $note['title'] . '</td>
										<td>'. $note['content'] . '</td>
										<td>'. $note['creation_date'] . '</td>
									</tr>';
					}
					$message .= '</table>';
				}

				$mail_param = array(
					'from'		=> "admin@mynef.com",
					'fromname' 	=> "Innomap",
					'to' 		=> $manager['email'],
					'subject' 	=> "Task and Note for Today and Tomorrow [Manager Notification]",
					'message' 	=> $message );
				phpmailer_send($mail_param, true);
			}
		}
	}
}