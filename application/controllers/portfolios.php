<?php 
require_once('common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Portfolios extends Common {
	
	function __construct() {
		parent::__construct();
		
		$this->meta 			= array();
		$this->scripts 			= array('jquery.dataTables.min','dataTables_initialize');
		$this->styles 			= array('jquery.dataTables');
		$this->title 			= "Portfolio";
		
		$this->load->model('portfolio');
		$this->user_data = $this->user_session->get();
		$this->available_data = (count($this->portfolio->get_available_manager()) > 0 && count($this->portfolio->get_available_innovation()) > 0);
	}
	
	/* PORTFOLIO ASSIGNMENT BY [IDeC Head] */
	function index(){
		if($this->user_data['role_id'] == ROLE_MANAGER || $this->user_data['role_id'] == ROLE_HIP6_ACCOUNT_MANAGER){
			$this->manager_task();
		}else{
			$is_hip6 = ($this->user_data['role_id'] == ROLE_PROGRAM_DIRECTOR ? 1 : 0);
			$portfolios = $this->portfolio->get_with_manager(array('p.is_hip6' => $is_hip6))->result_array();
			for($i=0;$i<count($portfolios);$i++){
				$portfolios[$i]['innovations'] = $this->portfolio->get_portfolio_innovation(array('pai.portfolio_id'=>$portfolios[$i]['portfolio_id']))->result_array();
			}
			$data['portfolios'] = $portfolios;
			$data['flashdata']	= $this->session->flashdata('inno_form_msg');
			$data['can_add']	= $this->available_data;
			$data['lang']		= $this->site_lang;
			$data['user_sess']  = $this->user_sess;
			$this->load->view('portfolio/list',$data);
		}
	}
	
	function assign(){
		if($this->available_data){
			$this->scripts 			= array("jquery.validate","front/form_validation");
			
			if($this->user_data['role_id'] == ROLE_PROGRAM_DIRECTOR){
				$data['managers'] = $this->portfolio->get_available_manager_hip6();
				$data['innovations'] = $this->portfolio->get_available_innovation_hip6();
			}else{
				$data['managers'] 		= $this->portfolio->get_available_manager();
				$data['innovations']	= $this->portfolio->get_available_innovation();
			}
			$this->load->view('portfolio/assign_form',$data);
		}else{
			echo "There's no manager or innovation available. <a href='".base_url()."portfolios"."'><< Back to Portfolios List</a>";
			die;
		}
	}
	
	function save_assign(){
		$this->load->model('user_notification');
		$portfolio_id = $this->input->post('portfolio_id');
		$data_post = array('title' 		 => $this->input->post('title'),
						   'assigner_id' => $this->user_data['user_id'],
						   'is_hip6'	 => ($this->user_data['role_id'] == ROLE_PROGRAM_DIRECTOR ? 1 : 0));
		if($portfolio_id > 0){
			$this->portfolio->edit($portfolio_id, $data_post);	
		}else{
			$data_post['manager_id'] = $this->input->post('manager_id');
			$portfolio_id = $this->portfolio->add($data_post);	
		}
		
		$innovations  = $this->input->post('innovation');		
		foreach($innovations as $innovation){
			$porto_inno = array('portfolio_id' => $portfolio_id, 'innovation_id' => $innovation);
			$this->portfolio->add_portfolio_innovation($porto_inno);
		}
		
		if($portfolio_id){
			$data_notif = array('user_id_from' => $this->user_data['user_id'],
							'user_id_to' => $this->input->post('manager_id'),
							'notif_type' => NOTIF_PORTFOLIO_ASSIGN,
							'target_id' => $portfolio_id);
			$this->user_notification->add($data_notif);

			$this->session->set_flashdata('inno_form_msg', array('success' =>true, 'msg' => lang('msg_success_save_portfolio')));
		}else{
			$this->session->set_flashdata('inno_form_msg', array('success' =>false, 'msg' => lang('msg_failed_save_portfolio')));
		}
		redirect(base_url()."portfolios");
	}
	
	function delete_portfolio($portfolio_id){
		$this->load->model(array('user_notification'));

		$detail = $this->portfolio->get(array('portfolio_id'=>$portfolio_id))->row_array();
		if($detail['assigner_id'] == $this->user_sess['user_id']){
			$this->user_notification->send_multi_user(NOTIF_DELETE_PORTFOLIO,$portfolio_id);

			if($this->portfolio->delete($portfolio_id)){
				$this->session->set_flashdata('inno_form_msg', array('success' =>true, 'msg' => lang('msg_success_delete_portfolio')));
			}else{
				$this->session->set_flashdata('inno_form_msg', array('success' =>true, 'msg' => lang('msg_success_delete_portfolio')));
			}
			redirect($_SERVER['HTTP_REFERER']);
		}else{
			echo 'Access Denied !'; die;
		}
	}
	/* END OF ASSIGNMENT BY [IDeC Head] */
	
	/* PORTFOLIO TASK BY [Account Manager] */
	function manager_task($notif = FALSE){
		$this->load->model(array('portfolio_scope', 'portfolio'));

		$portfolios = $this->portfolio->get(array('manager_id'=>$this->user_data['user_id']))->result_array();
		for ($i=0; $i<count($portfolios); $i++) {
			$portfolios[$i]['is_scope_exist'] = $this->portfolio_scope->get(array('portfolio_id' => $portfolios[$i]['portfolio_id']))->num_rows() > 0;
		}
		$data['portfolios'] = $portfolios;
		$data['flashdata']	= $this->session->flashdata('inno_form_msg');
		$data['show_notif'] = ($notif == "notif");
		if($data['show_notif']){
			$data['notif_tasks'] = $this->portfolio->get_task_by_manager($this->user_sess['user_id']);
			$data['notif_notes'] = $this->portfolio->get_note_by_manager($this->user_sess['user_id']);
		}
		$this->load->view('portfolio/task_list',$data);
	}

	function get_scope_tasks($portfolio_id){
		$this->layout = FALSE;
		$this->load->model('portfolio_scope');
		$date = $this->input->post('date');
		$tasks = $this->portfolio_scope->get_with_task('ps.portfolio_id = ' . $portfolio_id . ' AND (pst.start_date <= "'.$date.'" AND pst.end_date >= "'.$date.'")');
		echo json_encode($tasks);
	}
	
	function check($id){
		$this->load->model(array('portfolio_criteria', 'portfolio_scope'));
		$data['detail'] = $this->portfolio->get(array('portfolio_id' => $id))->row_array();
		
		if($data['detail']['status'] == PORTFOLIO_TASK_DONE){ //checking if tas already done, then redirect to view evaluation result
			redirect(base_url()."portfolios/view_evaluation/".$id);
		}
		
		$this->scripts = array("jquery.validate","highcharts","front/form_validation","front/portfolio");
		
		$innovations = $this->portfolio->get_portfolio_innovation(array('pai.portfolio_id' => $id))->result_array();
		for($i=0;$i<count($innovations);$i++){
			$ips = $this->portfolio->get_innovation_portfolio(array('innovation_id'=>$innovations[$i]['innovation_id']))->result_array();
			$innovations[$i]['item_checked'] = $ips;
			$innovations[$i]['items'] = $this->portfolio_scope->get_with_criteria($innovations[$i]['portfolio_id'], $innovations[$i]['innovation_id']);
		}		
		$data['innovations'] = $innovations;
		$data['notes']		 = $this->portfolio->get_note(array('portfolio_id' => $id, 'creation_date' => date('Y-m-d')))->result_array();	
		$data['asset_types'] = unserialize(PORTFOLIO_ASSET_TYPE);
		$data['flashdata']	 = $this->session->flashdata('inno_form_msg');
		$data['scopes']		 = $this->portfolio_criteria->get(array('is_deleted' => 0))->result_array();

		//highlight date for calendar
		//$note_dates	 		 = $this->portfolio->get_note_date($id);
		$scope_task_dates	 = $this->portfolio_scope->get_highlight_dates($id);

		//$data['highlight_dates'] = $note_dates . ($scope_task_dates != "" ? $scope_task_dates : "");
		$data['highlight_dates'] = $scope_task_dates;
		$this->load->view('portfolio/check',$data);
	}

	function save_amount_spend(){
		$this->layout = FALSE;
		$this->load->model('portfolio');
		$task_id = $this->input->post('amount_task_id');
		$portfolio_id = $this->input->post('amount_portfolio_id');
		$innovation_id = $this->input->post('amount_innovation_id');
		$amount_spend = $this->input->post('amount_spend');

		//check innovation portfolio is already exist
		if($this->portfolio->get_innovation_portfolio(array('innovation_id' => $innovation_id, 'task_id' => $task_id))->num_rows() > 0){
			$innovation_portfolio_id = $this->input->post('amount_innovation_portfolio_id');
			$status = $this->portfolio->edit_innovation_portfolio($innovation_portfolio_id, array('amount_spend' => $amount_spend));
		}else{
			$data_post = array('portfolio_id'  => $portfolio_id,
						   	   'innovation_id' => $innovation_id,
						       'task_id'	   => $task_id,
						       'amount_spend'  => $amount_spend);
			$status = $this->portfolio->add_innovation_portfolio($data_post);
		}		

		if($status){
			$this->session->set_flashdata('inno_form_msg', array('success' =>true, 'msg' => lang('msg_success_save_amount_spend')));
		}else{
			$this->session->set_flashdata('inno_form_msg', array('success' =>false, 'msg' => lang('msg_failed_save_amount_spend')));
		}

		$item_num = $this->input->post('item_num');
		redirect(base_url() ."portfolios/check/". $portfolio_id ."?item=". $item_num);
	}
	
	function save_attachment(){
		$this->layout = FALSE;
		$this->load->model(array('site','user_notification','innovation_portfolio_attachment'));
		$portfolio_id = $this->input->post('portfolio_id');
		$note = $this->input->post('note');
		
		$data_post = array('portfolio_id'  => $portfolio_id,
						   'innovation_id' => $this->input->post('innovation_id'),
						   'task_id'	   => $this->input->post('task_id'),
						   'progress'	   => $this->input->post('task_progress'),
						   'note'		   => $note);
		if($this->input->post('task_done') == 1){
			$data_post['progress'] = 100;
		}
		
		//insert data if data is not exist
		$inno_portfolio = $this->portfolio->get_innovation_portfolio(array('innovation_id'=>$data_post['innovation_id'],'task_id'=>$data_post['task_id']))->row_array();
		if(count($inno_portfolio) <= 0){
			$innovation_port_id = $this->portfolio->add_innovation_portfolio($data_post);
		}else{ //edit portfolio
			$innovation_port_id = $this->input->post('innovation_portfolio_id');
			if($inno_portfolio['progress'] == 100){ //edit done task, change status to PM Approval
				$data_post['is_pending_pm_approval'] = TRUE;
			}
			$this->portfolio->edit_innovation_portfolio($innovation_port_id, $data_post);
		}
		
		//upload attachment
		$pict_num = count($_FILES);
    	for ($i = 0; $i <= $pict_num; $i++) {
    		if (isset($_FILES['attachment_' . $i])) {
				if($_FILES['attachment_'. $i]['name'] != NULL && $innovation_port_id > 0){
					$filename = $innovation_port_id."_".$_FILES['attachment_'. $i]['name'];
					$attchmnt = $this->site->_upload($filename,'attachment_'. $i,PATH_TO_PORTFOLIO,'attach');
					if($attchmnt){
						$this->innovation_portfolio_attachment->add(array('innovation_portfolio_id' => $innovation_port_id,'name'=>$attchmnt));
					}
				}
			}
		}

		if(isset($_POST['deleted_attachment'])){
			$deleted_attachment = $this->input->post('deleted_attachment');
			foreach ($deleted_attachment as $key => $value) {
				$get_attachment = $this->innovation_portfolio_attachment->get(array('id' => $value))->row_array();
				if($get_attachment){
					$this->site->_remove($get_attachment['name'],PATH_TO_PORTFOLIO);	
					$this->innovation_portfolio_attachment->delete($value);
				}
			}
		}
		
		//check if innovation attachment after uploaded all
		if($this->portfolio->check_portfolio_done($portfolio_id)){
			$this->portfolio->edit($portfolio_id,array('status'=>PORTFOLIO_TASK_DONE));
			$data_portfolio = $this->portfolio->get(array('portfolio_id' => $portfolio_id))->row_array();

			$data_notif = array('user_id_from' => $this->user_data['user_id'],
								'user_id_to' => $data_portfolio['assigner_id'],
								'notif_type' => NOTIF_PORTFOLIO_DONE,
								'target_id' => $data_portfolio['portfolio_id']);
			$this->user_notification->add($data_notif);
		}
		
		if($innovation_port_id){
			$this->session->set_flashdata('inno_form_msg', array('success' =>true, 'msg' => lang('msg_success_save_attachment')));
		}else{
			$this->session->set_flashdata('inno_form_msg', array('success' =>false, 'msg' => lang('msg_failed_save_attachment')));
		}

		$item_num = $this->input->post('item_num');
		$response['res_url'] = base_url() ."portfolios/check/". $portfolio_id ."?item=". $item_num;
		echo json_encode($response);
	}
	
	function view_evaluation($id){
		$this->scripts = array("jquery.validate","highcharts","front/form_validation","front/portfolio","front/general");
		$this->load->model(array('portfolio_criteria', 'portfolio_scope'));
		
		$innovations = $this->portfolio->get_portfolio_innovation(array('pai.portfolio_id' => $id))->result_array();
		for($i=0;$i<count($innovations);$i++){
			$ips = $this->portfolio->get_innovation_portfolio(array('innovation_id'=>$innovations[$i]['innovation_id']))->result_array();
			$innovations[$i]['item_checked'] = $ips;
			$innovations[$i]['items'] = $this->portfolio_scope->get_with_criteria($innovations[$i]['portfolio_id'], $innovations[$i]['innovation_id']);
		}		
		$data['detail'] 	 = $this->portfolio->get(array('portfolio_id' => $id))->row_array();
		$data['innovations'] = $innovations;
		$data['items']		 = $this->portfolio_criteria->get(array('created_at < ' => $data['detail']['assignment_date']),'position')->result_array();
		$data['flashdata']	 = $this->session->flashdata('inno_form_msg');
		$data['notes']		 = $this->portfolio->get_note(array('creation_date' => date('Y-m-d')))->result_array();	
		$data['note_dates']	 = $this->portfolio->get_note_date($id);
		$data['scopes']		 = $this->portfolio_criteria->get(array('is_deleted' => 0))->result_array();
		/*comment*/
		$data['comments']	 = $this->portfolio->get_comment($id);
		$data['is_creator']  = ($data['detail']['assigner_id'] == $this->user_sess['user_id']);

		$scope_task_dates	 = $this->portfolio_scope->get_highlight_dates($id);
		$data['highlight_dates'] = $scope_task_dates;
		$this->load->view('portfolio/view_evaluation',$data);
	}
	
	function view_uploaded($innovation_id,$task_id){
		$this->layout = false;
		$this->load->model(array('site','innovation_portfolio_attachment'));
		
		$detail 	  		= $this->portfolio->get_innovation_portfolio(array('innovation_id'=>$innovation_id,'task_id'=>$task_id))->row_array();
		$attachment 		= $this->innovation_portfolio_attachment->get(array('innovation_portfolio_id' => $detail['innovation_portfolio_id']))->result_array();
		$content_attachment = "";
		foreach ($attachment as $key => $value) {
			$content_attachment .= "<tr>";
			$content_attachment .= 		"<td></td><td>";
			$content_attachment .= 			$this->site->show_attachment($value['name'],PATH_TO_PORTFOLIO,700,400,400);
			$content_attachment .=		"</td>";
			$content_attachment .= "</tr>";
		}
		$res['attachment']  = $content_attachment;
		$res['notes']		= $detail['note'];
		echo json_encode($res);
	}
	
	function save_asset(){
		$this->load->model('site');
		$data_post = array('innovation_id' => $this->input->post('asset_innovation_id'),
						   'title'		   => $this->input->post('asset_title'),
						   'type'		   => $this->input->post('asset_type'),
						   'description'   => $this->input->post('asset_description'));
		
		//set asset url
		if($_FILES['asset_attachment']['name'] != NULL){
			$attachment = $this->site->_upload($_FILES['asset_attachment']['name'],'asset_attachment',PATH_TO_PORTFOLIO_ASSET,'attach');
			$data_post['url']  = $attachment;
		}else{
			$data_post['url'] = $this->input->post('asset_url');
		}
		
		if($this->portfolio->add_asset($data_post)){
			$this->session->set_flashdata('inno_form_msg', array('success' =>true, 'msg' => lang('msg_success_save_asset')));
		}else{
			$this->session->set_flashdata('inno_form_msg', array('success' =>false, 'msg' => lang('msg_failed_save_asset')));
		}
		redirect($_SERVER['HTTP_REFERER']);
	}
	
	function view_saved_asset($inno_id){
		$this->layout = false;
		$this->load->model('site');
		
		$types  = unserialize(PORTFOLIO_ASSET_TYPE);
		$assets = $this->portfolio->get_asset(array('innovation_id'=>$inno_id))->result_array();
		for($i=0;$i<count($assets);$i++){
			$assets[$i]['type_name'] = $types[$assets[$i]['type']];
			if(filter_var($assets[$i]['url'], FILTER_VALIDATE_URL)){ 
				$assets[$i]['content'] = "<a target='_blank' href='".$assets[$i]['url']."'>".$assets[$i]['url']."</a>";
			}else{
				$assets[$i]['content'] = "<a target='_blank' href='".base_url().PATH_TO_PORTFOLIO_ASSET.$assets[$i]['url']."'>Lihat Attachment</a>";
			}			
		}
		echo json_encode($assets);
	}
	
	function delete($innovation_id,$item_id){
		$this->layout = false;
		$detail = $this->portfolio->get_innovation_portfolio(array('innovation_id'=>$innovation_id,'item'=>$item_id))->row_array();
		
		//delete attachment file
		$this->load->model('site');
		if($detail['attachment'] != ""){
			$this->site->_remove($detail['attachment'],PATH_TO_PORTFOLIO);
		}
		
		//delete innovation portfolio record & set flashdata
		if($this->portfolio->delete_innovation_portfolio(array('innovation_id'=>$innovation_id,'item'=>$item_id))){
			$this->session->set_flashdata('inno_form_msg', array('success' =>true, 'msg' => lang('msg_success_delete_attachment')));
		}else{
			$this->session->set_flashdata('inno_form_msg', array('success' =>false, 'msg' => lang('msg_failed_save_attachment')));
		}
		redirect($_SERVER['HTTP_REFERER']);	
	}
	
	function save_comment(){
		$this->load->model('user_notification');
		$data_post = array('portfolio_id' => $this->input->post('comment_portfolio_id'),
						   'content'	  => $this->input->post('comment_content'));
		
		//save comment and set flashdata
		if($this->portfolio->add_comment($data_post)){
			//send notification
			$portfolio = $this->portfolio->get(array('portfolio_id' => $this->input->post('comment_portfolio_id')))->row_array();
			$data_notif = array('user_id_from' 	=> $this->user_data['user_id'],
								'user_id_to' 	=> $portfolio['manager_id'],
								'notif_type' 	=> NOTIF_PORTFOLIO_COMMENT,
								'target_id' 	=> $portfolio['portfolio_id']);
			$this->user_notification->add($data_notif);	

			$this->session->set_flashdata('inno_form_msg', array('success' =>true, 'msg' => lang('msg_success_save_comment')));
		}else{
			$this->session->set_flashdata('inno_form_msg', array('success' =>false, 'msg' => lang('msg_failed_save_comment')));
		}
		redirect($_SERVER['HTTP_REFERER']);	
	}
	
	function get_notes(){
		$this->layout = false;
		$date = $this->input->post('date');
		echo json_encode($this->portfolio->get_note(array('creation_date' => $date))->result_array());
	}
	
	function save_note(){
		$this->load->model('site');
		$note_id   = $this->input->post('note_id');
		$success   = false;
		$data_post = array('portfolio_id' 	=> $this->input->post('note_portfolio_id'),
						   'title'		   	=> $this->input->post('note_title'),
						   'content'		=> $this->input->post('note_content'),
						   'creation_date'	=> $this->input->post('note_creation_date'));
		if($_FILES['note_attachment']['name'] != NULL){
			if($note_id > 0){
				$detail = $this->portfolio->get_note(array('portfolio_note_id'=>$note_id))->row_array();
				if($detail['attachment'] != ""){
					$this->site->_remove($detail['attachment'],PATH_TO_PORTFOLIO_NOTE);
				}
			}
			$data_post['attachment'] = $this->site->_upload($this->input->post('note_portfolio_id')."_".$_FILES['note_attachment']['name'],'note_attachment',PATH_TO_PORTFOLIO_NOTE,'attach');
		}
		
		if($note_id > 0){
			if($this->portfolio->edit_note($note_id,$data_post)){
				$status = true;
			}
		}else{
			if($this->portfolio->add_note($data_post)){
				$status = true;
			}
		}
		
		//set flashdata
		if($status){
			$this->session->set_flashdata('inno_form_msg', array('success' =>true, 'msg' =>lang('msg_success_save_note')));
		}else{
			$this->session->set_flashdata('inno_form_msg', array('success' =>false, 'msg' => lang('msg_failed_save_note')));
		}
		redirect($_SERVER['HTTP_REFERER']);	
	}
	
	function view_note($note_id){
		$this->layout = false;
		$this->load->model('site');
		$detail 			  = $this->portfolio->get_note(array('portfolio_note_id'=>$note_id))->row_array();
		$detail['attachment'] = $this->site->show_attachment($detail['attachment'],PATH_TO_PORTFOLIO_NOTE,700,400,400);
		echo json_encode($detail);
	}
	
	function delete_note($note_id){
		$this->layout = false;
		$this->load->model('site');
		
		//delete attachment file
		$detail = $this->portfolio->get_note(array('portfolio_note_id'=>$note_id))->row_array();
		if($detail['attachment'] != ""){
			$this->site->_remove($detail['attachment'],PATH_TO_PORTFOLIO_NOTE);
		}
		
		//delete note then set flashdata
		if($this->portfolio->delete_note($note_id)){
			echo json_encode(array('success' =>true, 'msg' =>lang('msg_success_delete_note')));
		}else{
			echo json_encode(array('success' =>false, 'msg' =>lang('msg_failed_delete_note')));
		}
	}

	function view_innovation($id){	
		$this->layout = false;
		$this->load->model(array('category','innovation'));
		
		$inno_cats = $this->innovation->get_innovation_category(array('innovation_id'=>$id));
		for($i=0;$i<count($inno_cats);$i++){
			$inno_cats[$i]['subcategories'] = $this->category->get_subcategory(array('category_id' => $inno_cats[$i]['category_id']))->result_array();
		}

		$innovators = array();
		$innovators = $this->innovation->get_innovation_innovator(array('ii.innovation_id' => $id));
	
		$data['detail']			= $this->innovation->get(array('innovation_id'=>$id))->row_array();
		$data['inno_subs']		= $this->innovation->get_innovation_subcategory($id);
		$data['inno_pics']		= $this->innovation->get_innovation_picture($id);
		$data['inno_cats']		= $inno_cats;
		$data['targets']	  	= unserialize(INNOVATION_TARGET);
		$data['categories']		= $this->category->get()->result_array();	
		$data['innovator']		= $innovators;
		$this->load->view('portfolio/modal_detail_innovation',$data);
	}

	function edit_criteria_value($innovation_portfolio_id){
		$this->layout = false;
		$this->load->model(array('site','innovation_portfolio_attachment'));
		
		$detail 	  		= $this->portfolio->get_innovation_portfolio(array('innovation_portfolio_id'=>$innovation_portfolio_id))->row_array();
		$attachment 		= $this->innovation_portfolio_attachment->get(array('innovation_portfolio_id' => $detail['innovation_portfolio_id']))->result_array();
		$content_attachment = "";
		foreach ($attachment as $key => $value) {
			$content_attachment .= "<tr>";
			$content_attachment .= 		"<td></td><td>";
			$content_attachment .= 			$this->site->show_attachment($value['name'],PATH_TO_PORTFOLIO,700,400,400);
			$content_attachment .=		"</td>";
			$content_attachment .=		"<td><a href='#' class='delete-attachment' data-id='".$value['id']."'>(-) Padam</a></td>";
			$content_attachment .= "</tr>";
		}
		$res['attachment']  = $content_attachment;
		$res['data']		= $detail;
		echo json_encode($res);
	}

	function edit_portfolio($portfolio_id){
		$this->scripts 			= array("jquery.validate","front/form_validation");
		
		$data['portfolio'] = $this->portfolio->get(array('portfolio_id' => $portfolio_id))->row_array();
		if($this->user_data['role_id'] == ROLE_PROGRAM_DIRECTOR){
			$data['managers'] = $this->portfolio->get_available_manager_hip6();
			$data['innovations'] = $this->portfolio->get_available_innovation_hip6('(pai.portfolio_id IS NULL OR pai.portfolio_id = '.$portfolio_id.')', 1);
		}else{
			$data['managers'] 		= $this->portfolio->get_available_manager();
			$data['innovations']	= $this->portfolio->get_available_innovation('(pai.portfolio_id IS NULL OR pai.portfolio_id = '.$portfolio_id.')', 1);
		}
		$this->load->view('portfolio/assign_form',$data);
	}

	function scope($portfolio_id){
		$this->scripts[] = "front/portfolio-scope";
		$this->load->model(array('portfolio_scope', 'portfolio_criteria'));

		$data['criterias'] = $this->portfolio_criteria->get()->result_array();
		$data['portfolio_id'] = $portfolio_id;
		$this->load->view('portfolio/scope_form',$data);	
	}

	function save_scope(){
		$this->load->model('portfolio_scope');

		$portfolio_id = $this->input->post('portfolio_id');
		$criterias = $this->input->post('criteria_id');
		$weightage = $this->input->post('weightage');
		$budget = $this->input->post('budget');
		$task_names = $this->input->post('task_name');
		$task_start_date = $this->input->post('task_start_date');
		$task_end_date = $this->input->post('task_end_date');
		$task_weightage = $this->input->post('task_weightage');
		$status = false;
		foreach ($criterias as $criteria) {
			if($weightage[$criteria] > 0){
				$data_post = array('portfolio_id' 	=> $portfolio_id,
								   'criteria_id'	=> $criteria,
								   'weightage'		=> $weightage[$criteria],
								   'budget'			=> $budget[$criteria]);
				$scope_id = $this->portfolio_scope->add($data_post);

				//save scope task
				foreach ($task_names[$criteria] as $key => $task) {
					if($task_weightage[$criteria][$key] > 0){
						$data_post = array('scope_id' 	=> $scope_id,
									   	   'task'		=> $task,
									   	   'start_date'	=> date("Y-m-d", strtotime($task_start_date[$criteria][$key])),
									   	   'end_date'	=> date("Y-m-d", strtotime($task_end_date[$criteria][$key])),
									   	   'weightage'	=> $task_weightage[$criteria][$key]);
						$this->portfolio_scope->add_task($data_post);

						if($criteria == end($criterias) && $task == end($task_names[$criteria])){
							$status = true;
						}
					}
				}
			}
		}

		if($status){
			$this->session->set_flashdata('inno_form_msg', array('success' =>true, 'msg' =>lang('msg_success_save_portfolio_scope')));
		}else{
			$this->session->set_flashdata('inno_form_msg', array('success' =>false, 'msg' => lang('msg_failed_save_portfolio_scope')));
		}

		redirect(base_url(). "portfolios");
	}

	function print_pdf($portfolio_id, $innovation_id, $scope_id = NULL){
		$this->load->library('pdf');
		$this->load->model(array('innovation', 'category', 'portfolio_scope'));

		$data['full'] 			= false;
		$data['innovation']		= $this->innovation->get(array('innovation_id'=>$innovation_id))->row_array();
		$data['innovator']		= $this->innovation->get_innovation_innovator(array('ii.innovation_id' => $innovation_id));
		if($scope_id == NULL){
			$inno_cats = $this->innovation->get_innovation_category(array('innovation_id'=>$innovation_id));
			for($i=0;$i<count($inno_cats);$i++){
				$inno_cats[$i]['subcategories'] = $this->category->get_subcategory(array('category_id' => $inno_cats[$i]['category_id']))->result_array();
			}

			$data['full']		= true;
			$data['inno_subs']	= $this->innovation->get_innovation_subcategory($innovation_id);
			$data['inno_pics']	= $this->innovation->get_innovation_picture($innovation_id);
			$data['inno_cats']	= $inno_cats;
			$data['targets']	= unserialize(INNOVATION_TARGET);
			$data['categories']	= $this->category->get()->result_array();	
		}

		if($scope_id != NULL){
			$data['items'] = $this->portfolio_scope->get_with_criteria($portfolio_id, $innovation_id, $scope_id);
		}else{
			$data['items'] = $this->portfolio_scope->get_with_criteria($portfolio_id, $innovation_id);
		}

		//$this->load->view('portfolio/print_pdf', $data);
		$this->pdf->load_view('portfolio/print_pdf', $data);
		$this->pdf->render();
		$this->pdf->stream("Innovation Portfolio " . $data['innovation']['name_in_melayu'] . date('d-m-Y H-i-s'));
	}

	function approve_task($innovation_portfolio_id){
		$this->layout = FALSE;
		$this->load->model('portfolio');

		$status = $this->portfolio->edit_innovation_portfolio($innovation_portfolio_id, array('is_pending_pm_approval' => FALSE));

		if($status){
			$this->session->set_flashdata('inno_form_msg', array('success' =>true, 'msg' => lang('msg_success_approve_task')));
		}else{
			$this->session->set_flashdata('inno_form_msg', array('success' =>false, 'msg' => lang('msg_failed_approve_task')));
		}

		redirect($_SERVER['HTTP_REFERER']);
	}
}