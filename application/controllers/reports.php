<?php
require_once('common.php');

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Reports extends Common {

	function __construct() {
		parent::__construct('report');

		$this->load->model('report');
		$this->scripts 			= array('jquery.dataTables.min', 'jquery.form', 'highcharts', 'front/report');
		$this->styles 			= array('media_print');
		$this->meta 			= array();
		$this->title 			= "Reports";
	}
	
	function innovation_state(){
		$data = array('states' => $this->report->get_state()->result_array(),
					  'years'  => $this->report->get_year()->result_array());
		$this->load->view('report/innovation_state',$data);
	}
	
	function innovation_state_handler(){
		$this->layout 	= FALSE;
		$states 		= $this->report->get_state()->result_array();
		$years 			= $this->report->get_year()->result_array();
		$req_states		= $this->input->post('innovation_state');
		$req_year		= $this->input->post('filter_year');
		
		//is isset filter
		$is_filtered_cat  = ($req_states[0] != 0 ? true : false);
		$is_filtered_year = ($req_year != 'all' ? true : false);
		
		//result data
		$i = 0;
		foreach($years as $year){
			$cond1 = ($is_filtered_year ? ($year['year'] == $req_year ? true : false) : true);
			if($cond1){
				$result['category'][$i]['name'] = $year['year'];
				$j = 0;
				foreach($states as $state){
					$cond2 = ($is_filtered_cat ? (in_array($state['state_id'],$req_states) ? true : false) : true);
					if($cond2){
						$y = $this->report->get_innovation_by_state(array('inv.state_id' => $state['state_id'],'YEAR(i.discovered_date)' => $year['year']))->num_rows();
						$result['category'][$i]['data'][$j] = array('id' => $state['state_id'],'seri' => $year['year'],'y' => $y);
						$j++;
					}
				}
				$i++;
			}
		}
		
		//result title
		foreach($states as $state){
			$add_title = ($is_filtered_cat ? (in_array($state['state_id'],$req_states) ? true : false) : true);
			if($add_title){
				$result['title'][] = str_replace("Wilayah Persekutuan","WP",$state['name']);
			}
		}
		echo json_encode($result);
	}

	function innovation_category(){
		$data = array('categories' 	=> $this->report->get_category()->result_array(),
					  'years'		=> $this->report->get_year()->result_array());
		$this->load->view('report/innovation_category',$data);
	}

	function innovation_category_handler(){
		$this->layout 	= FALSE;
		$categories 	= $this->report->get_category()->result_array();
		$years 			= $this->report->get_year()->result_array();
		$req_categories = $this->input->post('innovation_category');
		$req_year		= $this->input->post('filter_year');
		
		//is isset filter
		$is_filtered_cat  = ($req_categories[0] != 0 ? true : false);
		$is_filtered_year = ($req_year != 'all' ? true : false);
		
		//result data
		$i = 0;
		foreach($years as $year){
			$cond1 = ($is_filtered_year ? ($year['year'] == $req_year ? true : false) : true);
			if($cond1){
				$result['category'][$i]['name'] = $year['year'];
				$j = 0;
				foreach($categories as $category){
					$cond2 = ($is_filtered_cat ? (in_array($category['category_id'],$req_categories) ? true : false) : true);
					if($cond2){
						$y = $this->report->get_innovation_by_category(array('ic.category_id' => $category['category_id'],'YEAR(i.discovered_date)' => $year['year']))->num_rows();
						$result['category'][$i]['data'][$j] = array('id' => $category['category_id'],'seri' => $year['year'],'y' => $y);
						$j++;
					}
				}
				$i++;
			}
		}
		
		//result title
		foreach($categories as $category){
			$add_title = ($is_filtered_cat ? (in_array($category['category_id'],$req_categories) ? true : false) : true);
			if($add_title){
				$result['title'][] = $category['name'];
			}
		}
		echo json_encode($result);
	}
	
	function innovation_year(){
		$data = array('years' => $this->report->get_year()->result_array());
		$this->load->view('report/innovation_year',$data);
	}

	function innovation_year_handler(){
		$this->layout 	= FALSE;
		$years 			= $this->report->get_year()->result_array();
		$req_year		= $this->input->post('filter_year');
		
		//is isset filter
		$is_filtered_year = ($req_year != 'all' ? true : false);
		
		//result data
		foreach($years as $key=>$year){
			$cond1 = ($is_filtered_year ? ($year['year'] == $req_year ? true : false) : true);
			if($cond1){
				$y = $this->report->get_innovation(array('YEAR(discovered_date)' => $year['year']))->num_rows();
				$result['value'][] = array('id' => $key,'seri' => $year['year'],'y' => $y);
				$result['title'][] = $year['year'];
			}
		}
		echo json_encode($result);
	}
	
	function innovator_state(){
		$data = array('states' => $this->report->get_state()->result_array());
		$this->load->view('report/innovator_state',$data);
	}
	
	function innovator_state_handler(){
		$this->layout 	= FALSE;
		$states 		= $this->report->get_state()->result_array();
		$req_states		= $this->input->post('innovator_state');
		
		//is isset filter
		$is_filtered	= ($req_states[0] != 0 ? true : false);
		
		//result data
		foreach($states as $state){
			$cond = ($is_filtered ? (in_array($state['state_id'],$req_states) ? true : false) : true);
			if($cond){
				$y = $this->report->get_innovator(array('i.state_id' => $state['state_id']))->num_rows();
				$result['value'][] = array('id' => $state['state_id'],'y' => $y);
				$result['title'][] = str_replace("Wilayah Persekutuan","WP",$state['name']);
			}
		}
		echo json_encode($result);
	}
	
	function innovator_category(){
		$data = array('categories' 	=> $this->report->get_category()->result_array());
		$this->load->view('report/innovator_category',$data);
	}
	
	function innovator_category_handler(){
		$this->layout 	= FALSE;
		$categories		= $this->report->get_category()->result_array();
		$req_categories	= $this->input->post('innovator_category');
		
		//is isset filter
		$is_filtered	= ($req_categories[0] != 0 ? true : false);
		
		//result data
		foreach($categories as $category){
			$cond = ($is_filtered ? (in_array($category['category_id'],$req_categories) ? true : false) : true);
			if($cond){
				$y = $this->report->get_innovator_by_category(array('ic.category_id' => $category['category_id']))->num_rows();
				$result['value'][] = array('id' => $category['category_id'],'y' => $y);
				$result['title'][] = $category['name'];
			}
		}
		echo json_encode($result);
	}
	
	function innovator_gender(){
		$this->load->view('report/innovator_gender');
	}
	
	function innovator_gender_handler(){
		$this->layout 	= FALSE;
		$genders 		= array(array('id'=>1,'name'=>'Male'),array('id'=>2,'name'=>'Female'));
		$req_genders	= $this->input->post('innovator_gender');
		
		//is isset filter
		$is_filtered	= ($req_genders[0] != 0 ? true : false);
		
		//result data
		foreach($genders as $gender){
			$cond = ($is_filtered ? (in_array($gender['id'],$req_genders) ? true : false) : true);
			if($cond){				
				$y = $this->report->get_innovator(array('gender'=> $gender['id']), true)->num_rows();
				$result['title'][] = $gender['name'];
				$result['data'][]  = array('name' => $gender['name'],'y' => $y,'id' => $gender['id']);
			}
		}
		echo json_encode($result);
	}
	
	function expert_state(){
		$data = array('states' => $this->report->get_state()->result_array());
		$this->load->view('report/expert_state',$data);
	}
	
	function expert_state_handler(){
		$this->layout 	= FALSE;
		$states 		= $this->report->get_state()->result_array();
		$req_states		= $this->input->post('expert_state');
		
		//is isset filter
		$is_filtered	= ($req_states[0] != 0 ? true : false);
		
		//result data
		foreach($states as $state){
			$cond = ($is_filtered ? (in_array($state['state_id'],$req_states) ? true : false) : true);
			if($cond){
				$y = $this->report->get_expert(array('e.state_id' => $state['state_id']))->num_rows();
				$result['value'][] = array('id' => $state['state_id'],'y' => $y);
				$result['title'][] = str_replace("Wilayah Persekutuan","WP",$state['name']);
			}
		}
		echo json_encode($result);
	}
	
	function university_state(){
		$data = array('states' => $this->report->get_state()->result_array());
		$this->load->view('report/university_state',$data);
	}
	
	function university_state_handler(){
		$this->layout 	= FALSE;
		$states 		= $this->report->get_state()->result_array();
		$req_states		= $this->input->post('university_state');
		
		//is isset filter
		$is_filtered	= ($req_states[0] != 0 ? true : false);
		
		//result data
		foreach($states as $state){
			$cond = ($is_filtered ? (in_array($state['state_id'],$req_states) ? true : false) : true);
			if($cond){
				$y = $this->report->get_university_by_state(array('u.state' => $state['state_id']))->num_rows();
				$result['value'][] = array('id' => $state['state_id'],'y' => $y);
				$result['title'][] = str_replace("Wilayah Persekutuan","WP",$state['name']);
			}
		}
		echo json_encode($result);
	}
	
	function university_category(){
		$data = array('categories' => $this->report->get_category()->result_array());
		$this->load->view('report/university_category',$data);
	}
	
	function university_category_handler(){
		$this->layout 	= FALSE;
		$categories		= $this->report->get_category()->result_array();
		$req_categories	= $this->input->post('university_category');
		
		//is isset filter
		$is_filtered	= ($req_categories[0] != 0 ? true : false);
		
		//result data
		foreach($categories as $category){
			$cond = ($is_filtered ? (in_array($category['category_id'],$req_categories) ? true : false) : true);
			if($cond){
				$y = $this->report->get_university_by_category(array('u.specialty' => $category['category_id']))->num_rows();
				$result['value'][] = array('id' => $category['category_id'],'y' => $y);
				$result['title'][] = $category['name'];
			}
		}
		echo json_encode($result);
	}
	
	/* BAR DETAIL */
	function get_person($year = NULL){
		$this->layout = FALSE;
		$data_id	  = $this->input->post('id');
		$type 		  = $this->input->post('content_type');
		$this->load->model('innovation');
		if($type == "innovation_state"){
			$result = $this->report->get_innovation_by_state(array('inv.state_id' => $data_id,'YEAR(i.discovered_date)' => $year))->result_array();
			for($i=0;$i<count($result);$i++){
				$result[$i]['innovator'] = $this->report->get_innovation_innovator($result[$i]['innovation_id']);
				$state				     = $this->report->get_state(array('state_id'=>$result[$i]['state_id']))->row_array();
				$result[$i]['state']	 = $state['name'];
				$result[$i]['score_average'] = $this->innovation->get_average_score($result[$i]['innovation_id']);
			}
		}elseif($type == "innovation_category"){
			$result = $this->report->get_innovation_by_category(array('ic.category_id' => $data_id,'YEAR(i.discovered_date)' => $year))->result_array();
			for($i=0;$i<count($result);$i++){
				$result[$i]['innovator'] = $this->report->get_innovation_innovator($result[$i]['innovation_id']);
				$result[$i]['category']  = $this->report->get_innovation_category($result[$i]['innovation_id']);
				$result[$i]['score_average'] = $this->innovation->get_average_score($result[$i]['innovation_id']);
			}
		}elseif($type == "innovation_year"){
			$result = $this->report->get_innovation(array('YEAR(discovered_date)' => $year))->result_array();
			for($i=0;$i<count($result);$i++){
				$result[$i]['innovator'] = $this->report->get_innovation_innovator($result[$i]['innovation_id']);
				$result[$i]['score_average'] = $this->innovation->get_average_score($result[$i]['innovation_id']);
			}
		}elseif($type == "innovator_state"){
			$result = $this->report->get_innovator(array('i.state_id' => $data_id))->result_array();
		}elseif($type == "innovator_category"){
			$result = $this->report->get_innovator_by_category(array('ic.category_id' => $data_id))->result_array();
		}elseif($type == "innovator_gender"){
			$result = $this->report->get_innovator(array('i.gender' => $data_id), true)->result_array();
		}elseif($type == "expert_state"){
			$result = $this->report->get_expert(array('e.state_id' => $data_id))->result_array();
		}elseif($type == "university_state"){
			$result = $this->report->get_university_by_state(array('u.state' => $data_id))->result_array();
		}elseif($type == "university_category"){
			$result = $this->report->get_university_by_category(array('u.specialty' => $data_id))->result_array();
		}elseif($type == "innovation_eval_burndown"){
			$result = $this->report->get_innovation_evaluation(array('innovation_evaluation.status' => EVALUATION_DONE))->result_array();
		}elseif($type == "portfolio_burndown"){
			$result = $this->report->get_portfolio_assignment(array('status' => PORTFOLIO_TASK_DONE))->result_array();
		}
		echo json_encode($result);
	}
	
	function innovation_evaluation(){
		$this->scripts[] = "magicsuggest-1.3.1";
		$this->scripts[] = "front/evaluation";
		$this->styles[]  = "magicsuggest-1.3.1";
		$data['all_experts'] = $this->report->get_expert(NULL)->result_array();
		$data['innovations'] = $this->report->get_evaluated_innovation()->result_array();
		array_unshift($data['innovations'], array('id' => -1, 'name' => "- ALL INNOVATION -"));
		$data['innovators'] = $this->report->get_evaluated_innovation(NULL, FALSE, 'inv.innovator_id')->result_array();
		array_unshift($data['innovators'], array('id' => -1, 'name' => "- ALL INNOVATOR -"));
		$data['experts'] = $this->report->get_evaluated_innovation(NULL, FALSE, 'e.expert_id')->result_array();
		array_unshift($data['experts'], array('id' => -1, 'name' => "- ALL EXPERT -"));
		
		$data['is_topscore'] = FALSE;
		if(isset($_POST['generate'])){
			$filter_by 	= $this->input->post('filter_by');
			$ids 		= json_decode($this->input->post('ids'));
			if(count($ids) <= 0 || $ids[0] == -1){ //if selected option is ALL
				$res	= $this->report->get_evaluated_innovation()->result_array();
			}else{
				$res 	= $this->report->get_evaluation_result($filter_by, $ids)->result_array();
			}
			if($filter_by == 'expert'){
				$data['all_experts'] = $this->report->get_evaluation_expert($ids)->result_array();
			}
		}else{
			$res = $this->report->get_evaluated_innovation(NULL, TRUE)->result_array();
			$data['is_topscore'] = TRUE;
		}
		
		if(count($res) > 0){
			$total_avg = 0;
			for($i = 0; $i < count($res); $i++){
				$res[$i]['scores'] 	= array();
				foreach($data['all_experts'] as $exps){
					$res[$i]['scores'][] = $this->report->get_evaluation_score($res[$i]['id'], $exps['expert_id']);
				}
				$res[$i]['average'] = round($res[$i]['average'], 2);
				$total_avg 			+= $res[$i]['average'];
			}
			$data['overall_avg'] = round($total_avg / count($res), 2);
		}
		$data['results'] = $res;
		$this->load->view('report/innovation_evaluation',$data);
	}
	
	function group_scoring($group = 1){
		$this->scripts[] = "front/evaluation";
		
		$group_list = $this->report->get_expert_group()->result_array();

		$experts = $this->report->get_expert_group_expert(array('expert_group_id' => $group))->result_array();
		$get_title = $this->report->get_expert_group(array('expert_group_id' => $group))->row_array();
		$title = (array_key_exists('name',$get_title) ? $get_title['name'] : '');
		
		if($get_title && $experts){
			$where = '(';
			foreach($experts as $row){
				$where .= 'e.expert_id = '.$row['expert_id'].($row != end($experts) ? ' OR ' : ')');
			}
			$innovations = $this->report->get_evaluated_innovation($where)->result_array();
			foreach($innovations as $key=>$value){
				$total = 0;
				$row = 0;
				foreach($experts as $expert){
					$score = $this->report->get_evaluation_score($value['id'],$expert['expert_id']);
				
					if(array_key_exists('total', $score)){
						$innovations[$key][$expert['expert_id']] = $score;
						$total += $score['total'];
						$row += 1;

					}else{
						$innovations[$key][$expert['expert_id']] = '-';
					}
				}
				if($total != 0){
					$innovations[$key]['total'] = round($total,2);
					$innovations[$key]['average'] = round($total/$row,2);
				}else{
					$innovations[$key]['total'] = '-';
					$innovations[$key]['average'] = '-';
				}
			}
		}else{
			$innovations = array();
			$title = "";
			$experts = array();
		}

		$data = array('innovations' => $innovations, 'title' => $title,'experts' => $experts,'group' => $group,'group_list' => $group_list);
		$this->load->view('report/group_scoring',$data);
	}
	
/*************************************************** EXPORT TO EXCEL ****************************************************************/
	
	function export_innovation(){
		$this->layout = false;
		$title 		  = $this->input->post('report_title');
		$type  		  = $this->input->post('report_type');
		$req_year	  = $this->input->post('filter_year');
		$where 		  = NULL;
		$targets 	  = unserialize(INNOVATION_TARGET);
		
		if($type == 'innovation_state'){
			$req_states		= $this->input->post('innovation_state');
			if($req_states[0] != 0 || $req_year != 'all'){				
				if($req_states[0] != 0){
					$where = '(';
					foreach($req_states as $state){
						$where .= 'inv.state_id = '.$state.($state != end($req_states) ? ' OR ' : '');
					}
					$where .= ') '.($req_year != 'all' ? 'AND ' : '');
				}
				
				if($req_year != 'all'){
					$where .= 'YEAR(i.discovered_date) = '.$req_year;
				}
			}
			$data = $this->report->export_innovation_by_state($where)->result_array();
		}elseif($type == 'innovation_category'){
			$req_categories = $this->input->post('innovation_category');
			
			if($req_categories[0] != 0 || $req_year != 'all'){				
				if($req_categories[0] != 0){
					$where = '(';
					foreach($req_categories as $cat){
						$where .= 'ic.category_id = '.$cat.($cat != end($req_categories) ? ' OR ' : '');
					}
					$where .= ') '.($req_year != 'all' ? 'AND ' : '');
				}
				
				if($req_year != 'all'){
					$where .= 'YEAR(i.discovered_date) = '.$req_year;
				}
			}
			$data = $this->report->export_innovation_by_category($where)->result_array();
		}elseif($type == 'innovation_year'){
			if($req_year != 'all'){		
				$where = 'YEAR(discovered_date) = '.$req_year;
			}
			$data = $this->report->export_innovation_by_year($where)->result_array();
		}
		
		if(count($data) > 0){
			//set some data to display in excel
			for($i=0;$i<count($data);$i++){
				$inv_target = json_decode($data[$i]['target']);
				$data[$i]['target'] = '';
				if($inv_target){ foreach($inv_target as $tar){ $data[$i]['target'] .= $targets[$tar].($tar != end($inv_target) ? ', ' : ''); } }
				$data[$i]['myipo_protection'] = ($data[$i]['myipo_protection'] == 1 ? 'Yes' : 'No');
				$data[$i]['innovator'] 		  = strip_tags($this->report->get_innovation_innovator($data[$i]['innovation_id']));
				$data[$i]['category']  		  = $this->report->get_innovation_category($data[$i]['innovation_id']);	
				unset($data[$i]['innovation_id']);
			}
			//export data to excel
			$this->export_to_excel($data,$title);
		}else{
			echo 'Tidak ada data inovasi.';
		}
	}
	
	function export_innovator(){
		$this->layout = false;
		$title 		  = $this->input->post('report_title');
		$type  	      = $this->input->post('report_type');
		$where 		  = NULL;
		
		if($type == 'innovator_state'){
			$req_states		= $this->input->post('innovator_state');
			if($req_states[0] != 0){
				$where = '(';
				foreach($req_states as $state){
					$where .= 'i.state_id = '.$state.($state != end($req_states) ? ' OR ' : '');
				}
				$where .= ')';
			}
			$data = $this->report->get_innovator($where)->result_array();
		}elseif($type == 'innovator_category'){
			$req_categories = $this->input->post('innovator_category');
			if($req_categories[0] != 0){
				$where = '(';
				foreach($req_categories as $cat){
					$where .= 'ic.category_id = '.$cat.($cat != end($req_categories) ? ' OR ' : '');
				}
				$where .= ')';
			}
			$data = $this->report->get_innovator_by_category($where)->result_array();
		}elseif($type == 'innovator_gender'){
			$req_genders	= $this->input->post('innovator_gender');
			if($req_genders[0] != 0){
				$where = '(';
				foreach($req_genders as $gender){
					$where .= 'i.gender = '.$gender.($gender != end($req_genders) ? ' OR ' : '');
				}
				$where .= ')';
			}
			$data = $this->report->get_innovator($where, true)->result_array();
		}
		
		if(count($data) > 0){
			//set some data to display in excel
			for($i=0;$i<count($data);$i++){
				$data[$i]['gender'] = ($data[$i]['gender'] == 1 ? 'Male' : 'Female');
				unset($data[$i]['innovator_id']);
			}
			//export data to excel
			$this->export_to_excel($data,$title);
		}else{
			echo 'Tidak ada data inovator.';
		}
	}
	
	function export_expert(){
		$this->layout = false;
		$title 		  = $this->input->post('report_title');
		$where 		  = NULL;
		$req_states	  = $this->input->post('expert_state');
		
		if($req_states[0] != 0){
			$where = '(';
			foreach($req_states as $state){
				$where .= 'e.state_id = '.$state.($state != end($req_states) ? ' OR ' : '');
			}
			$where .= ')';
		}
		$data = $this->report->get_expert($where)->result_array();
		
		if(count($data) > 0){
			for($i=0;$i<count($data);$i++){
				unset($data[$i]['expert_id']);
			}
			//export data to excel
			$this->export_to_excel($data,$title);
		}else{
			echo 'Tidak ada data pakar';
		}
	}
	
	function export_university(){
		$this->layout = false;
		$title 		  = $this->input->post('report_title');
		$type  	      = $this->input->post('report_type');
		$where 		  = NULL;
		
		if($type == 'university_state'){
			$req_states		= $this->input->post('university_state');
			if($req_states[0] != 0){
				$where = '(';
				foreach($req_states as $state){
					$where .= 'u.state = '.$state.($state != end($req_states) ? ' OR ' : '');
				}
				$where .= ')';
			}
			$data = $this->report->get_university_by_state($where)->result_array();
		}elseif($type == 'university_category'){
			$req_categories		= $this->input->post('university_category');
			if($req_categories[0] != 0){
				$where = '(';
				foreach($req_categories as $cat){
					$where .= 'u.specialty = '.$cat.($cat != end($req_categories) ? ' OR ' : '');
				}
				$where .= ')';
			}
			$data = $this->report->get_university_by_category($where)->result_array();
		}
		
		if(count($data) > 0){
			for($i=0;$i<count($data);$i++){
				unset($data[$i]['university_id']);
			}
			//export data to excel
			$this->export_to_excel($data,$title);
		}else{
			echo 'Tidak ada data pakar';
		}
	}
	
	function export_evaluation(){
		$this->layout = FALSE;
		$title 		  = $this->input->post('report_title');
		$experts	  = $this->report->get_expert(NULL)->result_array();
		$filter_by	  = $this->input->post('filter_by');
		
		if($filter_by != NULL){
			$ids 	  = json_decode($this->input->post('ids'));
			$res	  = $this->report->get_evaluation_result($filter_by, $ids)->result_array();
			if($filter_by == 'expert'){
				$experts = $this->report->get_evaluation_expert($ids)->result_array();
			}
		}else{
			$res = $this->report->get_evaluated_innovation(NULL, TRUE)->result_array();
		}
		
		if(count($res) > 0){
			$data 	 = array();
			for($i=0;$i<count($res);$i++){
				$data[$i]['inovasi'] = $res[$i]['name'];
				foreach($experts as $expert){
					$score = $this->report->get_evaluation_score($res[$i]['id'], $expert['expert_id']);
					$data[$i][$expert['name']] = (count($score) > 0 ? $score['total'] : "-");
				}
				$data[$i]['markah terkumpul'] = $res[$i]['total_score'];
				$data[$i]['markah purata'] = round($res[$i]['total_score'] / $res[$i]['score_num'], 2);
			}
			//export data to excel
			$this->export_to_excel($data,$title);
		}else{
			echo 'Tidak ada data evaluasi';
		}
	}
	
	function export_group_scoring(){
		$this->layout = FALSE;
		$group 		  = $this->input->post('group');
		
		$experts = $this->report->get_expert_group_expert(array('expert_group_id' => $group))->result_array();
		$get_title = $this->report->get_expert_group(array('expert_group_id' => $group))->row_array();
		$title = (array_key_exists('name',$get_title) ? $get_title['name'] : '');
		
		if($get_title && $experts){
			$where = '(';
			foreach($experts as $row){
				$where .= 'e.expert_id = '.$row['expert_id'].($row != end($experts) ? ' OR ' : ')');
			}
			$res = $this->report->get_evaluated_innovation($where)->result_array();
			
			if(count($res) > 0){
				$data 	 = array();
				for($i = 0; $i < count($res); $i++){
					$data[$i]['inovasi'] = $res[$i]['name'];
					foreach($experts as $expert){
						$score = $this->report->get_evaluation_score($res[$i]['id'], $expert['expert_id']);
						$data[$i][$expert['name']] = (count($score) > 0 ? $score['total'] : "-");
					}
					$data[$i]['markah terkumpul'] = $res[$i]['total_score'];
					$data[$i]['markah purata'] = round($res[$i]['total_score'] / $res[$i]['score_num'], 2);
				}
				//export data to excel
				$this->export_to_excel($data, lang('title_group_scoring')." - ".$title);
			}else{
				echo 'Tidak ada data evaluasi';
			}
		}else{
			echo 'Tidak ada data kumpulan. <a href="'.base_url().'reports/group_scoring">Kembali</a>';
		}
	}
	
	function export_to_excel($datas,$filename){
		$this->layout = false;
        $this->load->library('excel');
		
		$sheet = new PHPExcel();
		$sheet->getProperties()->setTitle('Attendance Report')->setDescription('Attendance Report');
		$sheet->setActiveSheetIndex(0);
		
		//sheet field
		$fields = array_keys($datas[0]);
		$sheet->getActiveSheet()->getStyle('1')->getFont()->setBold(true);
		foreach($fields as $col=>$field){
			$sheet->getActiveSheet()->setCellValueByColumnAndRow($col, 1, ucfirst(str_replace("_"," ",$field)));
		}
		
		//sheet value
		$row = 2;
		foreach ($datas as $data) {
			$col = 0;
			foreach ($data as $val) {
				$sheet->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $val);
				$col++;
			}
			$row++;
		}
		$sheet->getActiveSheet()->getDefaultColumnDimension()->setWidth(30);
		
		$sheet_writer = PHPExcel_IOFactory::createWriter($sheet, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.' - '.date('dMy:His').'.xls"');
		header('Cache-Control: max-age=0');

		$sheet_writer->save('php://output');
	}

	function export_innovator_by_gender($gender = 0){
		$this->layout = false;
		$title 		  = "Innovator by Gender";
		$where 		  = NULL;

		if($gender != 0){
			$where = '(';
			$where .= 'i.gender = '.$gender;
			$where .= ')';
		}
		$data = $this->report->get_innovator_date($where)->result_array();
		
		if(count($data) > 0){
			//set some data to display in excel
			for($i=0;$i<count($data);$i++){
				$data[$i]['gender'] = ($data[$i]['gender'] == 1 ? 'Male' : 'Female');
				unset($data[$i]['innovator_id']);
			}
			//export data to excel
			$this->export_to_excel($data,$title);
		}else{
			echo 'Tidak ada data inovator.';
		}
	}
}