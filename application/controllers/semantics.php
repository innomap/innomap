<?php require_once('common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Semantics extends Common {
	function __construct() {
		parent::__construct("semantics");

		$this->meta 			= array();
		$this->title 			= "Semantics";
		$this->scripts			= array('d3.v2','front/semantic');
		$this->styles			= array();
	}

	public function search(){
		$this->load->view('semantic/search');
	}

	public function get_innovator_semantic($id){
		$this->layout = false;
		$this->load->model('innovator');

		$citizenship 	= unserialize(INNOVATOR_CITIZENSHIP);
		$race 			= unserialize(INNOVATOR_RACE);
		$marital 		= unserialize(INNOVATOR_MARITAL_STATUS);
		$innovator 		= $this->innovator->get(array('innovator_id'=>$id))->row_array();
		$data 			= array();
		$data['id'] 			= $innovator['innovator_id'];
		$data['name'] 			= $innovator['name'];
		$data['email'] 			= $innovator['email'];
		$data['ic_number'] 		= $innovator['kod_no'];
		$data['gender'] 		= ($innovator['gender'] == 1 ? "Lelaki" : "Perempuan");
		$data['citizenship'] 	= $citizenship[$innovator['citizenship']];
		$data['race']			= $race[$innovator['race']];
		$data['innovation']		= "";

		echo json_encode($data);
	}

	public function get_innovation_semantic($id){
		$this->layout = false;
		$this->load->model(array('innovation','category'));
		$target 		= unserialize(INNOVATION_TARGET);
		$innovation 	= $this->innovation->get(array('innovation_id'=>$id))->row_array();
		$categories 	= $this->innovation->get_innovation_category(array('innovation_id'=>$id));
		$pictures 		= $this->innovation->get_innovation_picture($id);
		$innovators 	= $this->innovation->get_innovation_innovator(array('ii.innovation_id'=>$id));

		$string_category = "";
		foreach ($categories as $key => $category) {
			if($key == count($categories)-1){
				$string_category .= $category['category_id'];
			}else{
				$string_category .= $category['category_id'].",";
			}
		}

		$string_target = "";
		if($innovation['target'] != "false" && $innovation['target'] != ""){
			$target_array = json_decode($innovation['target']);
			foreach ($target_array as $key => $value) {
				if($key == count($target_array)-1){
					$string_target .= $target[$value];
				}else{
					$string_target .= $target[$value].",";
				}
			}
		}

		$string_picture = "";
		foreach ($pictures as $key => $pict) {
			if($key == count($pictures)-1){
				$string_picture .= $pict['picture'];
			}else{
				$string_picture .= $pict['picture'].",";
			}
		}

		$string_innovator = "";
		foreach ($innovators as $key => $innovator) {
			if($key == count($innovators)-1){
				$string_innovator .= $innovator['innovator_id'];
			}else{
				$string_innovator .= $innovator['innovator_id'].",";
			}
		}

		$string_keyword = "";
		if($innovation['keyword'] != "false" && $innovation['keyword'] != ""){
			$array_keyword = json_decode($innovation['keyword']);
			foreach ($array_keyword as $key => $keyword) {
				if($key == count($array_keyword)-1){
					$string_keyword .= $keyword;
				}else{
					$string_keyword .= $keyword.",";
				}
			}
		}

		$data['id'] 					= $innovation['innovation_id'];
		$data['name_my']				= $innovation['name_in_melayu'];
		$data['name_en']				= $innovation['name'];
		$data['created_date']			= $innovation['created_date'];
		$data['discovered_date']		= $innovation['discovered_date'];
		$data['idea']					= trim(preg_replace('/\s+/', ' ', $innovation['inspiration_in_melayu']));
		$data['desc_my']				= trim(preg_replace('/\s+/', ' ', $innovation['description_in_melayu']));
		$data['desc_en']				= trim(preg_replace('/\s+/', ' ', $innovation['description']));
		$data['material']				= trim(preg_replace('/\s+/', ' ', $innovation['materials_in_melayu']));
		$data['cost']					= $innovation['manufacturing_costs'];
		$data['how_to_use']				= $innovation['how_to_use_in_melayu'];
		$data['special_achievement']	= trim(preg_replace('/\s+/', ' ', $innovation['special_achievement_in_melayu']));;
		$data['price']					= $innovation['selling_price'];
		$data['category']				= $string_category;
		$data['target']					= $string_target;
		$data['myipo_protection']		= $innovation['myipo_protection'] == 1 ? "Yes" : "No";
		$data['picture']				= $string_picture;
		$data['innovator']				= $string_innovator;
		$data['keyword']				= $string_keyword;
		echo json_encode($data);
	}

	public function get_innovation_innovator_semantic(){
		$innovation_id = $this->input->post('innovation_id');
		$innovator_id = $this->input->post('innovator_id');
		$this->layout = false;
		$this->load->model(array('innovation'));
		if($innovation_id){
			$innovator_innovation = $this->innovation->get_approval_innovation_innovator(array('innovator_innovation.innovation_id'=>$innovation_id));
		}else if($innovator_id){
			$innovator_innovation = $this->innovation->get_approval_innovation_innovator(array('innovator_innovation.innovator_id'=>$innovator_id));
		}else{
			$innovator_innovation = $this->innovation->get_approval_innovation_innovator(array('innovation.status'=>INNO_APPROVED,'innovator.status'=>INNO_APPROVED));
		}
		echo json_encode($innovator_innovation);
	}

	public function get_semantic_category_id(){
		$category_name = $this->input->post('category_name');
		$this->layout = false;
		$this->load->model(array('category'));
		$category = $this->category->get(array('name_in_melayu'=>$category_name))->row_array();
		echo json_encode($category['category_id']);
	}
}