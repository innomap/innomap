<?php require_once('common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends Common {
	function __construct() {
		parent::__construct("site");

		$this->meta 			= array();
		$this->scripts 			= array('jquery.flexslider-min');
		$this->styles 			= array('flexslider');
		$this->title 			= "Welcome";
	}

	public function index(){
		if ($this->user_session->get()) {
			$user = $this->user_session->get();
			$this->load->model('menu');
			$menus = $this->menu->get_front_menu($user['role_id']);
			$search_menu = array();
			$report_menu = array();
			$data_menu = array();
			foreach ($menus as $key => $menu) {
				if($menu['name'] == "Search"){
					foreach ($menu['childs'] as $child) {
						$search_menu[] = $child;
					}
				}
			}
			foreach ($menus as $key => $menu) {
				if($menu['name'] == "Report"){
					foreach ($menu['childs'] as $child) {
						$report_menu[] = $child;
					}
				}
			}
			foreach ($menus as $key => $menu) {
				if($menu['name'] == "Data"){
					foreach ($menu['childs'] as $child) {
						$data_menu[] = $child;
					}
				}
			}
			$data['search_menus'] = $search_menu;
			$data['report_menus'] = $report_menu;
			$data['data_menus']	= $data_menu;
			
			/*set management dashboard accessable or not for link in homepage*/
			$curr_menu 	 = $this->menu->get(array('url' => 'management_dashboard'))->row_array();
			$curr_module = $this->menu->get_module(array('menu_id' => $curr_menu['menu_id']))->row_array(); 
			$data['mng_dashboard_accessable'] = ($this->role->get_role_module(array('module_id' => $curr_module['module_id'], 'role_id' => $this->user_sess['role_id']))->num_rows() > 0);
			
			$this->load->view('site/index', $data);
		}else{
			$this->load->helper('mystring_helper');
			$this->load->model('homepage_content');
			$contents = $this->homepage_content->get_for_site();
			$data = array('contents' => $contents);
			$this->load->view("site/homepage",$data);
		}
	}

	function validate($role_id = null) {
		$this->layout = false;
		$login_success = false;
		if ($this->input->post("submit")) {
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			
			if (isset($username) && isset($password)) {
				$data = $this->user_session->create($username, $password);
				if ($data) {
					$login_success = true;
					if(count($data['roles']) > 1){
						$data['role_id'] = null;						
					}else{
						$data['role_id'] = $data['roles'][0]['role_id'];						
					}					
					$this->user_session->set($data);
					if($data['role_id'] == null){
						redirect(site_url('site/select_role'));
					}
				}
			}
		}
		if(!$login_success){
			$this->session->set_flashdata('login_msg', 'Access denied. Incorrect username/password');
			$response = array('status' => false,'message' => 'Incorrect username/password');
			redirect(site_url('site'));
		}else{
			$this->session->set_flashdata('login_msg', 'Login Success');
			$response = array('status' => true,'message' => 'Login successfull.');
			$log = $this->user_session->get();
			
			//set default view for every role
			if($log['role_id'] == ROLE_DATA_ANALYST){
				redirect(site_url('innovators'));
			}elseif($log['role_id'] == ROLE_EXPERT){
				redirect(site_url('evaluations'));
			}elseif($log['role_id'] == ROLE_MANAGER || $log['role_id'] == ROLE_HIP6_ACCOUNT_MANAGER){
				redirect(site_url('portfolios/manager_task/notif'));
			}else{
				redirect(site_url('site'));
			}
		}
		echo json_encode($response);
	}

	function select_role(){
		if(isset($_POST['submit'])){
			$role_id = $this->input->post('role');
			$session = $this->user_session->get();
			$session['role_id'] = $role_id;
			$this->user_session->set($session);
			$log = $this->user_session->get();
			
			//set default view for every role
			if($log['role_id'] == ROLE_DATA_ANALYST || $log['role_id'] == ROLE_DATA_ADMIN){
				redirect(site_url('innovators'));
			}elseif($log['role_id'] == ROLE_IDEC_HEAD){
				redirect(site_url('tasks'));
			}elseif($log['role_id'] == ROLE_EXPERT){
				redirect(site_url('evaluations'));
			}elseif($log['role_id'] == ROLE_MANAGER || $log['role_id'] == ROLE_HIP6_ACCOUNT_MANAGER){
				redirect(site_url('portfolios/manager_task/notif'));
			}else{
				redirect(site_url('site'));
			}
		}else{
			$session = $this->user_session->get();
			$data['roles'] = $session['roles'];
			$this->load->view("site/select_role", $data);
		}
	}

	function logout() {
		$this->user_session->clear();
		redirect(site_url());
	}

	function content($type,$content_id){
		$this->load->model(array('homepage_content','article'));
		if($type == "article"){
			$article = $this->article->get_list_for_site(array('article_id' => $content_id))->row_array();
			$content[0]['content'] = $article;
			$content[0]['type'] = "";
		}else{
			$content = $this->homepage_content->get_for_site(array('content.homepage_content_id' => $content_id));
		}
		$data = array('content' => $content);
		$this->load->view("site/article",$data);
	}
	
	function management_dashboard(){
		$this->load->view('site/management_dashboard');
	}
	
	function edit_password(){
		$this->scripts[] = "jquery.validate";
		$this->scripts[] = "front/form_validation";
		$data['flashdata']	= $this->session->flashdata('inno_form_msg');
		$this->load->view('site/edit_password',$data);
	}
	
	function save_password(){
		$this->load->model('user');
		$user 			  = $this->user->get(array('user_id'=>$this->user_sess['user_id']))->row_array();		
		$inputed_old_pass = $this->user->get_hash($user['username'],$this->input->post('old_password'));
		
		if($inputed_old_pass == $user['password']){ //check whether inputed old password is right or not
			$data_update = array('username' => $user['username'], 'password' => $this->input->post('new_password'));
			if($this->user->edit($user['user_id'],$data_update)){
				$this->session->set_flashdata('inno_form_msg', array('success' =>true, 'msg' => lang('msg_success_save_password')));
			}else{
				$this->session->set_flashdata('inno_form_msg', array('success' =>false, 'msg' => lang('msg_failed_save_password')));
			}
		}else{
			$this->session->set_flashdata('inno_form_msg', array('success' =>false, 'msg' => lang('msg_failed_oldpass_save_password')));
		}
		redirect(site_url('edit_password'));
	}
	
	function top_innovation($id = NULL, $show_back_btn = 1){
		$this->scripts = array('jquery.dataTables.min','dataTables_initialize');
		$this->styles[] = 'media_print';
		$this->styles[] = "jquery.dataTables";
		$this->load->model(array('innovation','innovation_note'));
		if($id == NULL){
			$this->scripts[] = 'front/top_innovation';
			$innovations = $this->innovation->get(array('top_innovation'=>1),11)->result_array();
			foreach ($innovations as $key => $innovation) {
				$innovations[$key]['picture'] = $this->innovation->get_innovation_picture($innovation['innovation_id']);
			}
			$data['innovations'] = $innovations;
			$this->load->view('site/top_innovation', $data);
		}else{
			$this->load->model('category');
			
			$inno_cats = $this->innovation->get_innovation_category(array('innovation_id'=>$id));
			for($i=0;$i<count($inno_cats);$i++){
				$inno_cats[$i]['subcategories'] = $this->category->get_subcategory(array('category_id' => $inno_cats[$i]['category_id']))->result_array();
			}		

			$data['detail']			= $this->innovation->get(array('innovation_id'=>$id))->row_array();
			$data['inno_subs']		= $this->innovation->get_innovation_subcategory($id);
			$data['inno_pics']		= $this->innovation->get_innovation_picture($id);
			$data['inno_cats']		= $inno_cats;
			$data['targets']	  	= unserialize(INNOVATION_TARGET);
			$data['categories']		= $this->category->get()->result_array();		
			$data['innovator']		= $this->innovation->get_innovation_innovator(array('ii.innovation_id' => $id));
			$data['show_back_btn']  = $show_back_btn;	
			$data['data_sources']	= unserialize(DATA_SOURCES);
			$data['notes']			= $this->innovation_note->get_list(array('innovation_id' => $id))->result_array();
			$this->load->view('innovation/view',$data);		
		}
	}
	
	function top_innovation_data($offset){
		$this->layout = false;
		$this->load->model('innovation');
		$innovations = $this->innovation->get(array('top_innovation'=>1),6,$offset)->result_array();
		foreach ($innovations as $key => $innovation) {
			$innovations[$key]['picture'] = $this->innovation->get_innovation_picture($innovation['innovation_id']);
		}
		echo json_encode($innovations);
	}

	function set_password(){
		$this->layout = FALSE;
		$this->load->model('user');

		$users = $this->user->get(array('user_id > ' => 119))->result_array();

		foreach($users as $user){
			$data = array('username' => $user['username'],'password' => 'testing');
			$this->user->edit($user['user_id'],$data);
		}
	}

	function rand_string($length) {
		$characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUFWXYZ";
		$string 	= "";    
		for ($p = 0; $p < $length; $p++) {
			$string .= $characters[mt_rand(0, strlen($characters))];
		}
		return $string;
	}

	function forgot_password(){
		$this->load->view('site/forgot_password');
	}

	function send_new_password(){
		$this->load->model('user');
		$this->layout 	= false;
		$email			= $this->input->post('email');
		$user_info		= $this->user->get(array('email' => $email))->row_array();
		if($user_info){
			$new_password	= $this->rand_string(10);
			if($this->user->edit($user_info['user_id'], array('username'=>$user_info['username'], 'password'=>$new_password))){
				$mail_param = array(
					'from'		=> "admin@innomap.my",
					'fromname' 	=> "Innomap - Forgot Password",
					'to' 		=> $email,
					'subject' 	=> "New Password",
					'message' 	=> "Hello ".$user_info['username'].", <br/>This is your new password: <b>".$new_password."</b><br/> You can change this password in 'Ubah Kata Laluan' menu after login."
				);
				$this->load->helper('phpmailer');
				if(phpmailer_send($mail_param, true)){
					$this->session->set_flashdata('forget_pswd_msg', 'true');
				}
			}
		}else{
			$this->session->set_flashdata('forget_pswd_msg', 'false');
		}
		redirect(base_url()."site/forgot_password");
	}

	function test_sent_email(){
		$mail_param = array(
			'from'		=> "admin@innomap.my",
			'fromname' 	=> "Innomap - Forgot Password",
			'to' 		=> "ririn@mobilus-interactive.com",
			'subject' 	=> "New Password",
			'message' 	=> "Hello this is checking email function"
		);
		$this->load->helper('phpmailer');
		if(phpmailer_send($mail_param, true)){
			echo 'email sent';
		}
		die;
	}
}