<?php require_once('common.php');
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tasks extends Common {
	function __construct() {
		parent::__construct("tasks");

		$this->meta 			= array();
		$this->scripts 			= array('jquery.validate','magicsuggest-1.3.1','front/form_validation','front/innovation','jquery.dataTables.min','dataTables_initialize','front/evaluation');
		$this->styles 			= array('jquery.dataTables','magicsuggest-1.3.1');
		$this->title 			= "Task";
		$this->load->model(array('task','expert','innovation','user_notification'));
		$this->user_data = $this->user_session->get();
	}

	public function index(){
		$data = array('tasks' => $this->task->get_list(array('assigner_id' => $this->user_data['user_id'])),
					  'innovations' => $this->task->get_innovation_list()->num_rows(),
					  'flashdata'	=> $this->session->flashdata('inno_form_msg'));
		$this->load->view('task/index',$data);
	}

	function add(){
		$data = array('mode' => 'ADD',
					  'experts' => $this->expert->get_name_list()->result_array(),
					  'innovations' => $this->task->get_innovation_list()->result_array());
		$this->load->view('task/form',$data);
	}

	function view($id){
		$this->load->model('innovation');
		$task = $this->task->get_detail($id);
		for($i = 0; $i < count($task['innovation']); $i++){
			$task['innovation'][$i]['score_average'] = $this->innovation->get_average_score($task['innovation'][$i]['innovation_id']);
		}
		$data = array('mode' => 'VIEW',
					  'task' => $task);
		$this->load->view('task/detail',$data);
	}

	function save(){
		$this->layout = FALSE;
		if(isset($_POST['save'])){
			$innovation_id_arr = $this->input->post('innovation_id_arr');
			$innovation_id_arr = json_decode($innovation_id_arr);
			$innovation_id = $this->input->post('innovation_id');
			$expert_ids = json_decode($this->input->post('expert_id'));

			$data_post = array('title' => $this->input->post('title'),
								'assigner_id' => $this->user_data['user_id']
								);
			if($task_id = $this->task->add($data_post)){
				for($i=0;$i<count($innovation_id_arr);$i++){
					$data[$i] = array('expert_task_id' => $task_id,
									  'innovation_id' => $innovation_id_arr[$i]->value);
					$this->task->add_task_innovation($data[$i]);	
				}

				for($i=0;$i<count($expert_ids);$i++){
					$data[$i] = array('expert_task_id' => $task_id,
									  'expert_id' => $expert_ids[$i]);
					$this->task->add_task_expert($data[$i]);	
				}

				//send notification
				for($i=0;$i<count($expert_ids);$i++){
					$data_notif = array('user_id_from' => $this->user_data['user_id'],
								'user_id_to' => $expert_ids[$i],
								'notif_type' => NOTIF_TASK_ASSIGN,
								'target_id' => $task_id);
					$this->user_notification->add($data_notif);	
				}
			}
			redirect(site_url('tasks'));
		}
	}

	function evaluation_result($innovation_id){
		$this->styles 			= array('jquery.dataTables','magicsuggest-1.3.1','ui-customize');
		$this->load->model('evaluation');
		$evaluation_list = $this->evaluation->get_detail(array('innovation_evaluation.innovation_id' => $innovation_id))->result_array();
		$data = array('list' => $evaluation_list);
		$this->load->view('task/evaluation_list',$data);
	}

	function set_password(){
		$this->load->model('user');
		$this->user->set_user_password();
	}

	function delete($task_id){
		$this->user_notification->send_multi_user(NOTIF_DELETE_TASK,$task_id);

		if($this->task->delete($task_id)){
			$this->session->set_flashdata('inno_form_msg', array('success' =>true, 'msg' => lang('msg_success_delete_task')));
		}else{
			$this->session->set_flashdata('inno_form_msg', array('success' =>true, 'msg' => lang('msg_success_delete_task')));
		}
		redirect(site_url('tasks'));
	}
}