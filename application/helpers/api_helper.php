<?php

if ( ! function_exists('api_get')){
	function api_get($url, $decode = TRUE){
		$curl = curl_init($url);

		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_USERAGENT, 'BIZCONTINEO');

		$response = curl_exec($curl);
		curl_close($curl);
                if($decode){
                        $response = json_decode($response,true);
                }
                return $response;
//		return json_decode($response,true);
	}
}
if ( ! function_exists('post_to_url')){
	function post_to_url($url, $data) {
	   
	   $fields = '';
	   foreach($data as $key => $value) { 
	      $fields .= $key . '=' . $value . '&'; 
	   }
	   rtrim($fields, '&');

	   $curl = curl_init();
	   
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_USERAGENT, 'BIZCONTINEO');
		curl_setopt($curl, CURLOPT_POST, count($data));
		curl_setopt($curl, CURLOPT_POSTFIELDS, $fields);

	   $result = curl_exec($curl);

	   curl_close($curl);
	   return $result;
	}
}
if ( ! function_exists('meta_get')){
	function meta_get(&$url, &$content_type = NULL) {
		if(strpos($url, '://') === FALSE){
			$url='http://'.$url;
		}
		$curl = curl_init($url);
		
		curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER  => TRUE,
			CURLOPT_FOLLOWLOCATION  => TRUE,
			CURLOPT_SSL_VERIFYHOST  => FALSE,
			CURLOPT_SSL_VERIFYPEER  => FALSE,
		));

		$html_result = curl_exec($curl);
		$content_type = curl_getinfo($curl, CURLINFO_CONTENT_TYPE);
		curl_close($curl);

		if(!$html_result) {
			return NULL;
		}
		$libxml_initial_value = libxml_use_internal_errors(TRUE);
		$doc = new DOMDocument();
		$doc->loadHTML($html_result);
		$result=array('title'=>null,'description'=>null,'image'=>null);
		libxml_clear_errors();
		libxml_use_internal_errors($libxml_initial_value);
		$titles = $doc->getElementsByTagName('title');
		if($titles->length > 0) {
			$result['title'] = $titles->item(0)->nodeValue;
		}

		$metas = $doc->getElementsByTagName('meta');
		foreach($metas as $meta) {
			$meta_name = $meta->getAttribute('name');
			switch($meta_name) {
				case 'title':
					$result['title'] = $result['title'] ?: $meta->getAttribute('content');
					break;
				case 'description':
					$result['description'] = $meta->getAttribute('content');
					break;
			}
			
			$meta_property = $meta->getAttribute('property');
			switch($meta_property) {
				case 'og:image':
					$result['image'] = $meta->getAttribute('content');
					break;
				case 'og:title':
					$result['title'] = $result['title'] ?: $meta->getAttribute('content');
					break;
			}
		}	
		$images = $doc->getElementsByTagName('img');
		foreach($images as $image) {
			$src = $image->getAttribute('src');
			if($src && strpos($src, 'logo') !== FALSE) {
				$result['image'] = $result['image'] ?: $src;
			}
		}
		return $result;
	}
if ( ! function_exists('return_img_str')){	
	function return_img_str($thumb_url, $web_url){
		if(strpos($thumb_url, '://') === FALSE){
			$thumb_url=$web_url.'/'.$thumb_url;						
		}
		if(is_image($thumb_url)){
			return $thumb_url;
		}
		return '';
	}
}
if(!function_exists('is_image')){
	function is_image($url){
		$params = array('http' => array(
	                  'method' => 'HEAD'
	               ));
	     $ctx = stream_context_create($params);
	     $fp = @fopen($url, 'rb', false, $ctx);
	     if (!$fp) 
	        return false;  // Problem with url

	    $meta = stream_get_meta_data($fp);
	    if ($meta === false)
	    {
	        fclose($fp);
	        return false;  // Problem reading data from url
	    }

	    $wrapper_data = $meta["wrapper_data"];
	    if(is_array($wrapper_data)){
	      foreach(array_keys($wrapper_data) as $hh){
	          if (substr($wrapper_data[$hh], 0, 19) == "Content-Type: image") // strlen("Content-Type: image") == 19 
	          {
	            fclose($fp);
	            return true;
	          }
	      }
	    }

	    fclose($fp);
	    return false;
	}
}
if(!function_exists('get_names')){
	function get_names($arr,$id){
		$result=array();
		if(isset($arr[$id])){
			foreach ($arr[$id] as $k => $v) {
				$result[]=$v['name'];
			}
		}
		return $result;
	}
}
if(!function_exists('get_vals')){
	function get_vals($arr,$id,$id2=null){
		if(!is_null($id2)){
			return isset($arr[$id]) ? $arr[$id][$id2] : '';			
		}
		return isset($arr[$id]) ? $arr[$id] : array();
	}
}
if(!function_exists('dd')){
	function dd($var){
		var_dump($var);die();
	}
}
}