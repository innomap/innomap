<?php

function charset_ensure_utf8($string) {
	/* http://www.w3.org/International/questions/qa-forms-utf-8.en.php */
	ini_set('pcre.backtrack_limit', 500);
	if(preg_match('%^(?:
      [\x09\x0A\x0D\x20-\x7E]            # ASCII
    | [\xC2-\xDF][\x80-\xBF]             # non-overlong 2-byte
    | \xE0[\xA0-\xBF][\x80-\xBF]         # excluding overlongs
    | [\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}  # straight 3-byte
    | \xED[\x80-\x9F][\x80-\xBF]         # excluding surrogates
    | \xF0[\x90-\xBF][\x80-\xBF]{2}      # planes 1-3
    | [\xF1-\xF3][\x80-\xBF]{3}          # planes 4-15
    | \xF4[\x80-\x8F][\x80-\xBF]{2}      # plane 16
    )*$%xs', $string)) {
		return $string;
	} else {
		return iconv('UTF-8', 'UTF-8//IGNORE', $string);
	}
}

function charset_ensure_utf8_array($array) {
	foreach($array as $key => &$value) {
		$value = charset_ensure_utf8($value);
	}

	return $array;
}

function charset_htmlspecialchars($string) {
	return htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
}