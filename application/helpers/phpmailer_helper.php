<?php

function phpmailer_send($param,$ishtml=false) {
	/* $param = array(
	 *   'from'     => '',
	 *   'fromname' => '',
	 *   'to'       => '',
	 *   'bcc'      => '',
	 *   'subject'  => '',
	 *   'message'  => ''
	 * );
	 */
	require_once APPPATH . 'libraries/phpmailer/class.phpmailer.php';

	$mail = new PHPMailer();

// using sendmail from server rather than telnet (smtp)

	$mail->AddCustomHeader("X-Bounce-Tracking: 999adasi7r34");
	//$mail->AddReplyTo(isset($param['reply_to']) ? $param['reply_to'] : MAIL_REPLY_TO, isset($param['fromname']) ? $param['fromname'] : MAIL_FROMNAME);
	$mail->From = isset($param['from']) ? $param['from'] : "from";
	$mail->FromName = isset($param['fromname']) ? $param['fromname'] : "from name";
	$mail->Sender = "admin@mynef.com";
	
	if(isset($param['to'])) {
		if(is_array($param['to'])) {
			foreach($param['to'] as $a_to) {
				$mail->AddAddress($a_to, '');
			}
		} else {
			$mail->AddAddress($param['to'], '');
		}
	}
	
	if(isset($param['bcc'])) {
		if(is_array($param['bcc'])) {
			foreach($param['bcc'] as $a_to) {
				$mail->AddBCC($a_to, '');
			}
		} else {
			$mail->AddBCC($param['bcc'], '');
		}
	}
	
	//$mail->AddReplyTo($mail->From, $mail->FromName);
	$mail->WordWrap = 65;
	$mail->IsHTML($ishtml);
	$mail->Subject = $param['subject'];
	$mail->Body = $param['message'];
	
	return $mail->Send();
}