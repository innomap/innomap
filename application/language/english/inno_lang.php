<?php
/* partial/header.php */
	$lang['menu_language'] = 'Language';
	$lang['menu_setting'] = 'Setting';
	$lang['menu_notif'] = 'Notification';
	$lang['menu_logout'] = 'Logout';
	$lang['label_username'] = 'Username';
	$lang['label_password'] = 'Password';
	$lang['forgot'] = 'Forgot';
	$lang['homepage_search'] = 'Search';
	$lang['homepage_report'] = 'Report';
	$lang['select_role'] = "Select Role";
	$lang['menu_switch_role'] = "Switch Role";
	
/* innovator/list.php */
	$lang['innovator'] = 'Innovator';
	$lang['add'] = 'Add';
	$lang['innovator_name'] = 'Innovator Name';
	$lang['innovation'] = 'Innovation';
	$lang['action'] = 'Action';
	$lang['link_view'] = 'View';
	$lang['link_delete'] = 'Delete';
	$lang['link_approve'] = 'Approve';
	$lang['link_reject'] = 'Reject';
	$lang['link_approval_note'] = 'Approval Note';
	$lang['approve_confirm1'] = 'Would you like to';
	$lang['approve_confirm2'] = 'this innovator for further processing? If yes, please leave a note and click Yes';
	$lang['approve_hip6_confirm2'] = 'this innovation for further processing? If yes, please click Yes';
	
/* innovator/account.php */
	$lang['title_account_form'] = 'Account Form';
	$lang['label_retype'] = 'Retype';
	$lang['button_cancel'] = 'Cancel';
	$lang['button_next_step'] = 'Next Step';
	
/* innovator/innovator.php */
	$lang['title_section'] = 'Section';
	$lang['title_innovator_form'] = 'Innovator Form';
	$lang['label_identity'] = 'Identity';
	$lang['label_name'] = 'Name';
	$lang['label_group_leader_name'] = 'Group Leader Name';
	$lang['label_group_expert_name'] = 'Group Expert Name';
	$lang['link_add_more'] = 'Add More';
	$lang['link_remove'] = 'Remove';
	$lang['label_email_address'] = 'Email Address';
	$lang['info_example'] = 'Ex';
	$lang['label_identification_code_no'] = 'Identification Kad No.';
	$lang['label_gender'] = 'Gender';
	$lang['value_male'] = 'Male';
	$lang['value_female'] = 'Female';
	$lang['label_birth_date'] = 'Birth Date';
	$lang['label_age'] = 'Age';
	$lang['label_place'] = 'Birth Place';
	$lang['label_citizenship'] = 'Citizenship';
	$lang['label_race'] = 'Race';
	$lang['label_marital_status'] = 'Marital Status';
	$lang['label_address'] = 'Address';
	$lang['label_postcode'] = 'Postcode';
	$lang['label_city'] = 'City';
	$lang['label_state'] = 'State';
	$lang['label_picture'] = 'Picture';
	$lang['label_keyword'] = 'Keyword';
	$lang['info_years_old'] = 'years old';
	
/* innovator/demographic.php */
	$lang['title_demographic_form'] = 'Demographic Form';
	$lang['label_location_type'] = 'Location Type';
	$lang['label_home_phone'] = 'Home Phone No.';
	$lang['label_official_phone'] = 'Official Phone No.';
	$lang['label_mobile_phone'] = 'Mobile Phone No.';
	$lang['label_fax_no'] = 'Fax No.';
	$lang['label_education_level'] = 'Education Level';
	$lang['label_job'] = 'Job';
	$lang['label_institution_name'] = 'Institution Name';
	$lang['label_employer'] = 'Employer';
	$lang['label_position'] = 'Position';
	$lang['label_innovator_has'] = 'Innovator has business';
	$lang['value_yes'] = 'Yes';
	$lang['value_no'] = 'No';
	
/* innovator/company.php */
	$lang['title_company_form'] = 'Company Form';
	$lang['label_registration_no'] = 'Registration No.';
	$lang['label_registration_date'] = 'Registration Date';
	$lang['label_operation_date'] = 'Operation Date';
	$lang['label_telephone_no'] = 'Telephone No.';
	$lang['label_funding_sources'] = 'Funding Sources';
	$lang['label_grant_loan_detail'] = 'Grant & Loan Detail';
	$lang['label_business_nature'] = 'Business Nature';
	$lang['label_registered_certification'] = 'Registered Certification';

/* innovator/heir.php */
	$lang['title_heir_form'] = 'Heir Form';
	$lang['label_relationship'] = 'Relationship';
	
/* controllers/innovators.php */
	$lang['msg_success_save_account'] = "Account data has been saved";
	$lang['msg_failed_save_account'] = "Account data didn't saved";
	$lang['msg_success_save_innovator'] = "Innovator data has been saved";
	$lang['msg_failed_save_innovator'] = "Innovator data didn't saved";
	$lang['msg_success_save_demographic'] = "Demographic data has been saved";
	$lang['msg_failed_save_demographic'] = "Demographic data didn't saved";
	$lang['msg_success_save_company'] = "Company data has been saved";
	$lang['msg_failed_save_company'] = "Company data didn't saved";
	$lang['msg_success_save_heir'] = "Heir data has been saved";
	$lang['msg_failed_save_heir'] = "Heir data didn't saved";
	$lang['msg_success_send_request'] = "Approval request has been sent";
	$lang['msg_success_approve_innovator'] = "Innovator has been approved";
	$lang['msg_success_reject_innovator'] = "Innovator has been rejected";
	$lang['msg_success_delete_innovator'] = "Innovator has been deleted";
	
/* controllers/innovations/php */
	$lang['msg_success_save_innovation'] = "Innovation data has been saved";
	$lang['msg_success_delete_innovation'] = "Innovation has been deleted";
	$lang['msg_failed_delete_innovation'] = "Innovation didn't delete";
	$lang['msg_success_delete_innovation_picture'] = "Innovation picture has been deleted";
	$lang['msg_failed_delete_innovation_picture'] = "Innovation picture didn't delete";
	$lang['msg_success_approve_innovation'] = "Innovation has been approved";
	$lang['msg_success_reject_innovation'] = "Innovation has been rejected";
	$lang['label_pending_innovation'] = "Pending Innovation";
	$lang['button_print'] = "Print";
	$lang['button_print_graph'] = "Print Graph";
	
/* task/index.php, task/form.php, evaluation/task.php  */
	$lang['title_task_list'] = "Task List";
	$lang['button_new_task'] = "Create new Task";
	$lang['label_no'] = "No";
	$lang['label_task'] = "Task";
	$lang['label_expert'] = "Expert Name";
	$lang['label_action'] = "Action";
	$lang['button_view_detail'] = "View Detail";

	$lang['title_detail_task'] = "Detail Task List";
	$lang['label_innovation_list'] = "Innovation List";
	$lang['label_innovation_name'] = "Innovation Name";
	$lang['label_innovator'] = "Innovator";
	$lang['label_submission_date'] = "Submission date";
	$lang['label_score'] = "Score";
	$lang['button_view_notes'] = "View Notes";
	$lang['button_back'] = "Back";
	$lang['button_save'] = "Save";

	$lang['title_task_form'] = "Task Form";

	$lang['button_view_evaluation_result'] = "View Evaluation Result";
	$lang['title_evaluation_detail'] = "Evaluation Detail";
	$lang['title_evaluation_result'] = "Evaluation Result";
	$lang['msg_success_delete_task'] = "Task has been deleted.";
	
/* innovation/index.php */
	$lang['title_innovation_form'] = "Innovation Product Form";
	$lang['button_send'] = "Send";
	$lang['info_before_send'] = "You must fill out all the form so you can send this innovator";
	$lang['label_view_detail'] = "View Detail";
	$lang['label_approved'] = "Approved";
	$lang['label_rejected'] = "Rejected";
	
/* innovation/form.php */
	$lang['link_add_more_category'] = "Add More Category";
	$lang['link_add_more_pict'] = "Add More Picture";
	$lang['link_remove_picture'] = "Remove Picture";
	$lang['info_keyword'] = "separate keywords with commas";
	$lang['value_select'] = "Select";
	$lang['label_data_source'] = "Data Source";
	$lang['label_information_memo'] = "Information Memo";
	$lang['label_recent_information_memo'] = "Recent Information Memo";
	$lang['label_last_updated'] = "Last Updated";
	$lang['label_ba_score'] = "Business Analyst Score";

/* innovation/detail.php */
	$lang['title_detail_innovation'] = "Innovation Detail";
	$lang['label_in_bahasa_melayu'] = "In Bahasa Melayu";
	$lang['label_inspiration'] = "Inspiration";
	$lang['label_product_description'] = "Product Description";
	$lang['label_material'] = "Materials";
	$lang['label_created_date'] = "Created date";
	$lang['label_discovered_date'] = "Discovered date";
	$lang['label_manufacturing_cost'] = "Manufacturing Cost";
	$lang['label_selling_price'] = "Selling Price";
	$lang['label_how_to_use'] = "How to use";
	$lang['label_innovation_category'] = "Innovation Category";
	$lang['label_subcategory'] = "Subcategory";
	$lang['label_target'] = "Target";
	$lang['label_myipo_protection'] = "Does this Innovation had MYIPO Protection?";
	$lang['label_special_achievement'] = "Special Achievement";
	$lang['label_innovation_picture'] = "Innovation Picture";

/* evaluation/index.php */
	$lang['title_evaluation_task'] = "Evaluation Task";
	$lang['label_status'] = "Status";
	$lang['button_evaluate'] = "Evaluate";
	$lang['button_view_evaluation'] = "View Evaluation";

/* evaluation/form1.php */
	$lang['title_evaluation_form'] = "Evaluation Form";
	$lang['label_date'] = "Date";
	$lang['label_evaluation_point'] = "Evaluation Point";
	$lang['label_criteria'] = "Criteria";
	$lang['label_point'] = "Point";
	$lang['value_point_1'] = "Idea";
	$lang['value_point_1_1'] = "Kreativiti"; 
	$lang['value_point_1_1_a'] = "Keaslian - Inovasi yang dihasilkan adalah idea asli";
	$lang['value_point_1_1_b'] = "Adaptasi - Inovasi yang dihasilkan merupakan penambahbaikan atau pengubahsuaian idea sedia ada";
	$lang['value_point_2'] = "Hasil Inovasi (Output)";
	$lang['value_point_2_2'] = "Tahap Pelaksanaan";
	$lang['value_point_2_2_a'] = "Secara penuh (melepasi peringkat percubaan)";
	$lang['value_point_2_2_b'] = "Secara percubaan (di peringkat prototaip)";
	$lang['value_point_2_3'] = "Replicability";
	$lang['value_point_2_3_a'] = "Boleh dilaksanakan secara terus di pelbagai tempat";
	$lang['value_point_2_3_b'] = "Dilaksanakan dengan pengubahsuaian mengikut keperluan tanpa melibatkan kos yang tinggi";
	$lang['value_point_3'] = "Impak (Outcome)";
	$lang['value_point_3_4'] = "Efisien (Kecekapan)";
	$lang['value_point_3_4_a'] = "Penjimatan masa";
	$lang['value_point_3_4_b'] = "Penjimatan kos";
	$lang['value_point_3_4_c'] = "Peningkatan produktiviti";
	$lang['value_point_3_4_d'] = "Mudah digunakan (user-friendly)";
	$lang['value_point_3_5'] = "Signifikan";
	$lang['value_point_3_5_desc'] = "Memberi kesan atau impak ketara kepada komuniti dari segi penjimatan kos atau masa dan inovasi boleh diterima pakai atau dilaksanakan di komuniti lain. Keberhasilan atau potensi keberhasilan yang signifikan dan berimpak tinggi pada peringkat";
	$lang['value_point_3_5_a'] = "Tinggi (Global)";
	$lang['value_point_3_5_b'] = "Sederhana (Nasional)";
	$lang['value_point_3_5_c'] = "Rendah (Tempatan)";
	$lang['value_point_4'] = "Pengurusan";
	$lang['value_point_4_6'] = "Komitmen";
	$lang['value_point_4_6_a'] = "Kewenangan";
	$lang['value_point_4_6_b'] = "Pengurusan Sumber Manusia";
	$lang['value_point_4_6_c'] = "Insentif";
	$lang['value_point_4_6_d'] = "Penghargaan";
	$lang['value_point_4_6_e'] = "Peralatan";
	$lang['value_point_5'] = "Relevan";
	$lang['value_point_5_desc'] = "Citaan berupaya memenuhi keperluan komuniti atau menyelesaikan masalah. Memaparkan perubahan yang selaras dengan aspek-aspek agenda nasional seperti sumbangan kepada pembentukan ekonomi nilai tinggi, penjimatan kos yang ketara kepada operasi Kerajaan, penjanaan hasil kerajaan yang ketara, kolaborasi unik dengan rakyat, usaha nation building yang berkesan dan sebagainya";
	$lang['value_point_6'] = "Keberkesanan";
	$lang['value_point_6_desc'] = "Memaparkan faedah yang nyata dengan keberhasilan yang mampan serta meliputi tempoh masa yang jelas membuktikan keberkesanannya.";
	$lang['value_point_7'] = "Kualiti";
	$lang['value_point_7_desc'] = "Rekabentuk inovasi mengambil kira dan memenuhi kehendak dan keperluan pengguna.";
	$lang['value_point_8'] = "Potensi";
	$lang['value_point_8_desc'] = "Inovasi ini berpotensi untuk berkembang dan mempunyai perancangan masa hadapan.";
	$lang['label_total'] = "Total";
	$lang['button_save_to_draft'] = "Save to Draft";
	$lang['label_max_value'] = "Maximum value for this criteria is";

/* evaluation/header.php */
	$lang['button_evaluation_form_1'] = "Evaluation Form 1";
	$lang['button_evaluation_form_2'] = "Evaluation Form 2";

/* evaluation/form2.php */
	$lang['value_point_f2_a'] = "Adakah terdapat keperluan bagi produk inovasi ini dipasaran?";
	$lang['value_point_f2_b'] = "Adakah kaedah yang digunakan oleh inovator merupakan kaedah terbaik dipasaran?";
	$lang['value_point_f2_c'] = "Adakah inovasi ini mempunyai manfaat kepada masyarakat/organisasi/individu?";
	$lang['value_point_f2_d'] = "Adakah kemungkinan terdapat pesaingan dalam pasaran bagi inovasi ini?";
	$lang['value_point_f2_e'] = "Ulasan dan cadangan penambahbaikan terhadap inovasi mengikut bidang kepakaran.";
	$lang['label_review'] = "Review";
	$lang['button_send_evaluation'] = "Send Evaluation";
	$lang['msg_confirm_send_evaluation'] = "Are you sure want to send this evaluation?";
	
/* portfolio/list.php */
	$lang['title_portfolio'] = "Portfolio";
	$lang['link_assign'] = "Assign";
	$lang['label_title'] = "Title";
	$lang['label_manager'] = "Manager";
	$lang['label_assignment_date'] = "Assignment Date";
	$lang['link_view_evaluation_result'] = "View Evaluation Result";
	$lang['info_task_status_done'] = "Done";
	$lang['info_task_status_not_yet'] = "Not Yet";
	$lang['label_assign_portfolio'] = "Assign Portfolio";
	$lang['link_edit'] = 'Edit';
	
/* portfolio/view_evaluation.php */
	$lang['title_attachment_detail'] = "Attachment Detail";
	$lang['label_item'] = "Item";
	$lang['label_attachment'] = "Attachment";
	$lang['label_notes'] = "Notes";
	$lang['info_no_comment'] = "There is no comment by IDec Head";
	$lang['task_updates'] = "Notes Updates";
	$lang['task_comment'] = "Task Comment";
	
/* portfolio/check.php */
	$lang['title_portfolio_attachment'] = "Portfolio Attachment";
	$lang['info_allowed_file'] = "Allowed file type";
	$lang['link_add_asset'] = "Add Asset";
	$lang['link_title_uploaded_asset'] = "Saved Asset";
	$lang['label_type'] = "Type";
	$lang['label_description'] = "Description";
	$lang['label_content_type'] = "Content Type";
	$lang['title_portfolio_progress'] = "Portfolio Progress";
	$lang['info_uploaded'] = "Completed Activities";
	$lang['info_not_uploaded'] = "Outstanding Activities";
	$lang['title_portfolio_note'] = 'Portfolio Note';
	$lang['label_content'] = 'Content';
	$lang['label_creation_date'] = 'Creation Date';
	$lang['info_no_notes'] = 'No note on this date';
	$lang['scope'] = 'Scope';
	$lang['label_progress'] = 'Progress';
	$lang['label_amount_spend'] = 'Amount Spend';
	$lang['save'] = 'Save';
	
/* portfolio/task_list.php */
	$lang['link_setup_scope'] = "Set Up Scope";
	$lang['link_check_portfolio'] = "Check Portfolio";
	$lang['info_dont_have_task'] = "You don't have any portfolio task";
	$lang['label_portfolio_task'] = "Portfolio Task";
	$lang['subtitle_notif_task'] = "Portfolio Task for today and tomorrow";
	$lang['subtitle_notif_note'] = "Portfolio Note for today and tomorrow";
	$lang['portfolio_title'] = "Portfolio Title";
	$lang['msg_no_notif_task'] = "There is no task";
	$lang['msg_no_notif_note'] = "There is no note";

/* portfolio/scope_form.php */
	$lang['label_weightage'] = "Weightage";
	$lang['label_task_list'] = "Task List";
	$lang['label_task_name'] = "Task Name";
	$lang['label_start_date'] = "Start Date";
	$lang['label_end_date'] = "End Date";
	$lang['label_budget'] = "Budget";
	$lang['label_ballance'] = "Ballance";
	$lang['pending_pd_approval'] = "Pending PD Approval";
	
/* portfolios.php */
	$lang['msg_success_save_portfolio'] = "Portfolio data has been saved";
	$lang['msg_failed_save_portfolio'] = "Portfolio data didn't save";
	$lang['msg_success_save_attachment'] = "Portfolio attachment has been saved";
	$lang['msg_failed_save_attachment'] = "Portfolio attachment didn't save";
	$lang['msg_success_delete_attachment'] = "Portfolio attachment has been deleted";
	$lang['msg_failed_delete_attachment'] = "Portfolio attachment didn't delete";
	$lang['msg_success_save_asset'] = "Innovation asset has been saved";
	$lang['msg_failed_save_asset'] = "Innovation asset didn't save";
	$lang['msg_success_save_comment'] = "Comment has been saved";
	$lang['msg_failed_save_comment'] = "Comment didn't save";
	$lang['msg_success_save_note'] = "Note has been saved";
	$lang['msg_failed_save_note'] = "Note didn't save";
	$lang['msg_success_delete_note'] = "Note has been deleted";
	$lang['msg_failed_delete_note'] = "Note didn't delete";
	$lang['title_popup_detail_innovation'] = "Detail Innovation";
	$lang['msg_success_save_portfolio_scope'] = "Portfolio scope has been saved";
	$lang['msg_failed_save_portfolio_scope'] = "Portfolio scope didn't save";
	$lang['scope_portfolio_warning'] = "Portfolio scope data can't be changed. Make sure the you enter the correct data.";
	$lang['msg_success_save_amount_spend'] = "Amount spend has been saved";
	$lang['msg_failed_save_portfolio_scope'] = "Amount spend didn't save";
	$lang['msg_success_approve_task'] = "Portfolio scope task has been approved";
	$lang['msg_failed_approve_task'] = "Portfoio scope task didn't approved";
	
/* report */
	$lang['title_innovation_by_category'] = "Innovaton by Category";
	$lang['label_innovation_year'] = "Innovation by Year";
	$lang['button_generate_report'] = "Generate Report";
	$lang['info_total_innovation'] = "Total Innovation";
	$lang['info_total_innovator'] = "Total Innovator";
	$lang['title_innovation_by_state'] = "Innovation by State";
	$lang['title_innovation_by_year'] = "Innovation by Year";
	$lang['title_innovator_by_state'] = "Innovator by State";
	$lang['title_innovator_by_category'] = "Innovator by Category";
	$lang['title_innovator_by_gender'] = "Innovator by Gender";
	$lang['title_expert_by_state'] = "Expert by State";
	$lang['info_total_expert'] = "Total Expert";
	$lang['title_university_by_state'] = "University by State";
	$lang['title_university_by_category'] = "University by Category";
	$lang['info_total_university'] = "Total Expert";
	$lang['label_contact_no'] = "Contact No.";
	$lang['title_innovation_eval_burndown'] = "Innovation Evaluation Burndown";
	$lang['title_portfolio_burndown'] = "Portfolio Burndown";
	
/* report/innovation_state */
	$lang['title_innovation_by_state'] = "Innovaton by State";
	$lang['msg_failed_save_comment'] = "Comment didn't save";
	
/* report/evaluation/ */
	$lang['title_innovation_evaluation'] = 'Innovation Evaluation';
	$lang['label_total_score'] = 'Total Score';
	$lang['label_score_average'] = 'Score Average';
	$lang['label_overall_average'] = 'Overall Average';
	$lang['title_top_score'] = '100 Top Score Innovations';
	$lang['title_detail_innovator'] = "Innovator Detail";
	$lang['filter_info'] = "You can choose more than 1 ";
	$lang['label_filter_by'] = 'Filter by';
	$lang['button_export_to_excel'] = 'Export to Excel';

/*locator.php*/
	$lang['label_locator'] = 'Locator';
	$lang['label_search'] = 'Search';
	$lang['label_enter']  = 'Enter';
	//pin detail
		$lang['label_innovation'] = 'Innovation';
		$lang['label_innovator'] = 'Innovator';
		$lang['label_product_desc']   = 'Product Description';
		$lang['label_category']   = 'Category';
		$lang['label_address']   = 'Address';
		$lang['label_state']   = 'State';
		$lang['label_university'] = 'University';
		$lang['label_college'] = 'College';
		$lang['label_specialty'] = 'Specialty';
		$lang['label_contact_no'] = 'Contact No.';
		$lang['label_expert'] = 'Expert';
		$lang['label_expertise'] = 'Expertise';
		$lang['label_experience'] = 'Experience';
	$lang['label_search_result']  = 'Search Result';
	$lang['label_all'] = 'All';
	$lang['label_project_title'] = "Project Title";
	$lang['label_ref_no'] = "Ref No";
	$lang['label_lead_agency'] = "Lead Agency";
	$lang['label_collaborator'] = "Collaborator";
	$lang['label_beneficiary_community'] = "Beneficiary Community";
	$lang['label_duration'] = "Duration (Month)";
	$lang['label_amount'] = "Amount";
	$lang['label_year'] = "Year";

/*State Income Map*/
	$lang['title_map'] = "Malaysia Income Distribution";
	$lang['label_select_gsp_year'] = "Select State Income in:";
	$lang['label_select_category'] = "Select Category:";
	$lang['commercial_center'] = "Commercial Center";

/*Elections Map*/
	$lang['title_elections_map'] = "Malaysian Elections Area Map";
	$lang['label_political_party'] = "Component Party:";
	$lang['label_vote_percentage'] = "Votes Percentage";
	$lang['label_inno_category'] = "Innovation Category";
	$lang['label_display_inno'] = "Display Innovation";
	$lang['label_sort_by'] = "Sort By:";
	
/*Semantic Search*/
	$lang['title_semantic_search'] = "Semantic Search";
	$lang['label_data_details'] = "Data Details";
	$lang['label_graph_details'] = "Graph Details";

/*Gallery*/
	$lang['title_gallery'] = "Gallery";
	$lang['link_gallery'] = "gallery";
	
	$lang['title_management_dashboard'] = "Management Dashboard";
	
/*Edit Password*/
	$lang['title_edit_password'] = "Edit Password";
	$lang['label_old_password'] = "Old Password";
	$lang['label_new_password'] = "New Password";
	$lang['msg_success_save_password'] = "New Password has been saved";
	$lang['msg_failed_save_password'] = "New Password didn't saved";
	$lang['msg_failed_oldpass_save_password'] = "Your Old Password didn't match";

	$lang['title_group_scoring'] = "Group Scoring";

/*innovation/executive_summary*/
	$lang['title_executive_summary'] = "Executive Summary";
	$lang['opt_es_status_research'] = "Research";
	$lang['opt_es_status_active'] = "Active";
	$lang['msg_success_save_exc_summary'] = "Executive Summary has been saved.";

/*management_plan*/
	$lang['title_management_plan'] = "Management Plan";
	$lang['label_background_of'] = "Background of ";
	$lang['label_account_details'] = "Account Details";
	$lang['label_name_of_inventor'] = "Name of Inventor";
	$lang['label_profession'] = "Profession";
	$lang['label_current_address'] = "Current Address";
	$lang['label_next_of_kin_contact'] = "Next of Kin contact";
	$lang['label_innovation_title'] = "Innovation Title";
	$lang['label_stage_no'] = "Stage No";
	$lang['label_certificate_no'] = "Certificate No";
	$lang['label_key_challenges_of_account'] = "Key Challenges of Account";
	$lang['label_account_targets'] = "Account Targets";
	$lang['label_challenges'] = "Challenges";
	$lang['label_mitigation'] = "Mitigation";
	$lang['label_deadline'] = "Deadline";
	$lang['label_remarks'] = "Remarks";
	$lang['btn_add_target'] = "Add Target";
	$lang['btn_add_key_challenges '] = "Add Key Challenges";
	$lang['label_revision_note'] = "Revision Note";
	$lang['label_date_of_revision'] = "Date of Revision";
	$lang['label_revised_by'] = "Revised by";
	$lang['btn_add_management_plan'] = "Add Management Plan";
	$lang['msg_success_save_plan'] = "Management Plan has been saved.";

/*Management Log*/
	$lang['title_management_logs'] = "Management Log";
	$lang['label_venue'] = "Venue";
	$lang['btn_add_management_log'] = "Add Management Log";
	$lang['label_account_name'] = "Account Name";
	$lang['label_account_manager'] = "Account Manager";
	$lang['msg_success_save_log'] = "Management Log has been saved.";

	$lang['title_executive_summary'] = "Executive Summary";

/*List innovation login as project director*/
	$lang['edit_innovation_to_hip6'] = "Edit Innovation to HIP 6";
	$lang['change_source_to_hip6'] = "Change to HIP 6";
	$lang['success_change_to_hip6'] = "Success change to HIP 6";

/*Committee meeting*/
	$lang['title_committee_meeting'] = "Committe Meeting";
	$lang['btn_add_committee_meeting'] = "Add Committee Meeting";
	$lang['created_by_label'] = "Created By";
	$lang['msg_success_delete_meeting'] = "Committee meeting has been deleted.";
	$lang['msg_success_add_meeting'] = "Committee meeting has been added.";
	$lang['msg_success_edit_meeting'] = "Committee meeting has been edited.";
	$lang['msg_failed_delete_meeting'] = "Committee meeting can not deleted.";
	$lang['msg_failed_add_meeting'] = "Committee meeting can not added.";
	$lang['msg_failed_edit_meeting'] = "Committee meeting can not edited.";
	$lang['label_committee_meeting_note'] = "Committee Meeting Note";
	$lang['label_committee_meeting_date'] = "Date";
	$lang['label_committee_meeting_attendance'] = "Attendance";
	$lang['btn_add_attachment'] = "Add Attachment";

	$lang['label_load_more'] = "Load More";

/*hip6 Evaluation*/
	$lang['label_business_analyst'] = "Business Analyst";
	$lang['hip6_label_fa_1'] = "Novelty of Innovation";
	$lang['hip6_label_cr_1_1'] = "Originality Innovation";
	$lang['hip6_label_desc_1_1'] = "Is the innovation novel in the sense that it is unique and can be patented?";
	$lang['hip6_label_fa_2'] = "Excellence";
	$lang['hip6_label_cr_2_1'] = "Comparable with Competitors";
	$lang['hip6_label_desc_2_1'] = "Is the innovation comparable to its competitors in the market?";
	$lang['hip6_label_cr_2_2'] = "Innovation impact";
	$lang['hip6_label_desc_2_2'] = "Will the innovation be able to resolve a large part of a problem or only a small part?";
	$lang['hip6_label_fa_3'] = "Affordability";
	$lang['hip6_label_cr_3_1'] = "Unit Cost";
	$lang['hip6_label_desc_3_1'] = "Is the unit cost when mass produce low i.e. a fraction of existing competitors? ";
	$lang['hip6_label_cr_3_2'] = "Cost of assistance";
	$lang['hip6_label_desc_3_2'] = "What is the cost of assistance to HIP6, high, moderate or low?";
	$lang['hip6_label_fa_4'] = "Target Group";
	$lang['hip6_label_cr_4_1'] = "Excluded group";
	$lang['hip6_label_desc_4_1'] = "Is the innovation targeted to HIP6 target group e.g. single mothers, OKUs, Indigenous people and low income groups?";
	$lang['hip6_label_fa_5'] = "Volume";
	$lang['hip6_label_cr_5_1'] = "Market size";
	$lang['hip6_label_desc_5_1'] = "Will the innovation address a large, medium or small number of consumers?";
	$lang['hip6_label_fa_6'] = "Sustainability";
	$lang['hip6_label_cr_6_1'] = "Attitude of entrepreneur";
	$lang['hip6_label_desc_6_1'] = "Does the innovator have positive attitude in sharing his innovation with market and sharing the HIP6 mission?";
	$lang['hip6_label_cr_6_2'] = "Diffusion platform";
	$lang['hip6_label_desc_6_2'] = "Is there a ready innovation diffusion vehicle i.e. an incorporated company, social enterprise or licensing partners?";
	$lang['hip6_label_cr_6_3'] = "Strategic linkages";
	$lang['hip6_label_desc_6_3'] = "Is there any existing strategic linkages (e.g. agencies, funds) that can be tapped to assist the innovator?";
	$lang['label_comment'] = "Comment";
	$lang['label_change_score'] = "Change Score";

/*MSI*/
	$lang['label_project_listing'] = "Project Listing";
	$lang['label_project'] = "Project";