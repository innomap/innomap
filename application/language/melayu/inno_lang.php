<?php
/* partial/header.php */
	$lang['menu_language'] = 'Bahasa';
	$lang['menu_setting'] = 'Tetapan Sistem';
	$lang['menu_notif'] = 'Notis';
	$lang['menu_logout'] = 'Keluar';
	$lang['label_username'] = 'Nama Pengguna'; //login
	$lang['label_password'] = 'Kata Laluan';
	$lang['forgot'] = 'Lupa';
	$lang['homepage_search'] = 'Carian';
	$lang['homepage_report'] = 'Laporan';
	$lang['select_role'] = "Pilih Peranan";
	$lang['menu_switch_role'] = "Tukar Peranan";
	
/* innovator/list.php */
	$lang['innovator'] = 'Inovator';
	$lang['add'] = 'Tambah';
	$lang['innovator_name'] = 'Nama Inovator';
	$lang['innovation'] = 'Inovasi';
	$lang['action'] = 'Tindakan';
	$lang['link_view'] = 'Lihat';
	$lang['link_delete'] = 'Padam';
	$lang['link_approve'] = 'Luluskan';
	$lang['link_reject'] = 'Tolak';
	$lang['link_approval_note'] = 'Catatan Kelulusan';
	$lang['approve_confirm1'] = 'Adakah anda ingin untuk';
	$lang['approve_confirm2'] = 'inovasi ini untuk proses seterusnya? Jika ya, sila tinggalkan nota dan klik Yes';
	$lang['approve_hip6_confirm2'] = 'inovasi HIP6 ini untuk proses seterusnya? Jika ya, sila klik Yes';
	
/* innovator/account.php */
	$lang['title_account_form'] = 'Info Akaun';
	$lang['label_retype'] = 'Ulangi';
	$lang['button_cancel'] = 'Batal';
	$lang['button_next_step'] = 'Langkah Seterusnya';
	
/* innovator/innovator.php */
	$lang['title_section'] = 'Bahagian';
	$lang['title_innovator_form'] = 'Info Inovator';
	$lang['label_identity'] = 'Identiti';
	$lang['label_name'] = 'Nama';
	$lang['label_group_leader_name'] = 'Nama Ketua Kumpulan';
	$lang['label_group_expert_name'] = 'Nama Ahli Kumpulan';
	$lang['link_add_more'] = 'Tambah';
	$lang['link_remove'] = 'Hapus';
	$lang['label_email_address'] = 'Alamat Email';
	$lang['info_example'] = 'Cth';
	$lang['label_identification_code_no'] = 'No. Kad Pengenalan';
	$lang['label_gender'] = 'Jantina';
	$lang['value_male'] = 'Lelaki';
	$lang['value_female'] = 'Perempuan';
	$lang['label_birth_date'] = 'Tarikh Lahir';
	$lang['label_age'] = 'Umur';
	$lang['label_place'] = 'Tempat Lahir';
	$lang['label_citizenship'] = 'Kewarganegaraan';
	$lang['label_race'] = 'Bangsa';
	$lang['label_marital_status'] = 'Taraf';
	$lang['label_address'] = 'Alamat';
	$lang['label_postcode'] = 'Poskod';
	$lang['label_city'] = 'Bandar';
	$lang['label_state'] = 'Negeri';
	$lang['label_picture'] = 'Gambar';
	$lang['label_keyword'] = 'Kata Kunci';
	$lang['info_years_old'] = 'tahun';
	
/* innovator/demographic.php */
	$lang['title_demographic_form'] = 'Info Demografik';
	$lang['label_location_type'] = 'Jenis Lokasi';
	$lang['label_home_phone'] = 'Telefon Rumah';
	$lang['label_official_phone'] = 'Telefon Pejabat';
	$lang['label_mobile_phone'] = 'Telefon Bimbit';
	$lang['label_fax_no'] = 'No. Faks';
	$lang['label_education_level'] = 'Tahap';
	$lang['label_job'] = 'Pekerjaan';
	$lang['label_institution_name'] = 'Nama Institusi';
	$lang['label_employer'] = 'Majikan';
	$lang['label_position'] = 'Pekerjaan/Jawatan';
	$lang['label_innovator_has'] = 'Inovator memiliki perniagaan';
	$lang['value_yes'] = 'Ya';
	$lang['value_no'] = 'Tiada';
	
/* innovator/company.php */
	$lang['title_company_form'] = 'Info Syarikat';
	$lang['label_registration_no'] = 'No. Pendaftaran';
	$lang['label_registration_date'] = 'Tarikh Mendaftar';
	$lang['label_operation_date'] = 'Tarikh Beroperasi';
	$lang['label_telephone_no'] = 'No. Telefon';
	$lang['label_funding_sources'] = 'Sumber Pembiayaan';
	$lang['label_grant_loan_detail'] = 'Butir-butir geran/pinjaman';
	$lang['label_business_nature'] = 'Jenis Perniagaan';
	$lang['label_registered_certification'] = 'Sijil Berdaftar';
	
/* innovator/heir.php */
	$lang['title_heir_form'] = 'Info Waris';
	$lang['label_relationship'] = 'Pertalian';
	
/* controllers/innovators.php */
	$lang['msg_success_save_account'] = "Data Akaun telah berjaya disimpan";
	$lang['msg_failed_save_account'] = "Data Akaun tidak berjaya disimpan";
	$lang['msg_success_save_innovator'] = "Data Inovator telah berjaya disimpan";
	$lang['msg_failed_save_innovator'] = "Data Inovator tidak berjaya disimpan";
	$lang['msg_success_save_demographic'] = "Data Demografik telah berjaya disimpan";
	$lang['msg_failed_save_demographic'] = "Data Demografik tidak berjaya disimpan";
	$lang['msg_success_save_company'] = "Data Syarikat telah berjaya disimpan";
	$lang['msg_failed_save_company'] = "Data Syarikat tidak berjaya disimpan";
	$lang['msg_success_save_heir'] = "Data Waris telah berjaya disimpan";
	$lang['msg_failed_save_heir'] = "Data Waris tidak berjaya disimpan";
	$lang['msg_success_send_request'] = "Permintaan kelulusan telah dihantar";
	$lang['msg_success_approve_innovator'] = "Inovator telah diluluskan";
	$lang['msg_success_reject_innovator'] = "Inovator telah ditolak";
	$lang['msg_success_delete_innovator'] = "Inovator telah terpadam";
	
/* controllers/innovations/php */
	$lang['msg_success_save_innovation'] = "Data Inovasi telah berjaya disimpan";
	$lang['msg_success_delete_innovation'] = "Inovasi telah berjaya disimpan";
	$lang['msg_failed_delete_innovation'] = "Inovasi tidak terpadam";
	$lang['msg_success_delete_innovation_picture'] = "Gambar Inovasi telah terpadam";
	$lang['msg_failed_delete_innovation_picture'] = "Gambar Inovasi tidk terpadam";
	$lang['msg_success_approve_innovation'] = "Inovasi telah diluluskan";
	$lang['msg_success_reject_innovation'] = "Inovasi telah ditolak";
	$lang['label_pending_innovation'] = "Inovasi Pra-Lulus";
	$lang['button_print'] = "Cetak";
	$lang['button_print_graph'] = "Cetak Grafik";

/* task/index.php, task/form.php, evaluation/task.php  */
	$lang['title_task_list'] = "Pecahan Skop";
	$lang['button_new_task'] = "Tambah Tugasan Baru";
	$lang['label_no'] = "No";
	$lang['label_task'] = "Perkara";
	$lang['label_expert'] = "Nama Pakar";
	$lang['label_action'] = "Tindakan";
	$lang['button_view_detail'] = "Lihat";

	$lang['title_detail_task'] = "Detail Senarai Tugasan";
	$lang['label_innovation_list'] = "Daftar Inovasi";
	$lang['label_innovation_name'] = "Produk Inovasi";
	$lang['label_innovator'] = "Inovator";
	$lang['label_submission_date'] = "Tarikh Penyerahan";
	$lang['label_score'] = "Score";
	$lang['button_view_notes'] = "Lihat Notes";
	$lang['button_back'] = "Kembali";
	$lang['button_save'] = "Simpan";

	$lang['title_task_form'] = "Borang Tugasan";

	$lang['button_view_evaluation_result'] = "Lihat Hasil Penilaian";
	$lang['title_evaluation_detail'] = "Penilaian Terperinci";
	$lang['title_evaluation_result'] = "Hasil Penilaian";
	$lang['msg_success_delete_task'] = "Tugasan telah dihapuskan.";

/* innovation/list.php */
	$lang['title_innovation_form'] = "Info Inovasi";
	$lang['button_send'] = "Hantar";
	$lang['info_before_send'] = "Anda harus mengisi seluruh maklumat sebelum dapat menghantar inovator";
	$lang['label_view_detail'] = "Lihat Butir";
	$lang['label_approved'] = "Telah Diluluskan";
	$lang['label_rejected'] = "Telah Ditolak";
	
/* innovation/form.php */
	$lang['link_add_more_category'] = "Tambah Kategori";
	$lang['link_add_more_pict'] = "Tambah Gambar";
	$lang['link_remove_picture'] = "Padam Gambar";
	$lang['info_keyword'] = "kata kunci berasingan dengan koma";
	$lang['value_select'] = "Pilih";
	$lang['label_data_source'] = "Sumber Data";
	$lang['label_information_memo'] = "Maklumat Memo";
	$lang['label_recent_information_memo'] = "Maklumat Memo Terbaru";
	$lang['label_last_updated'] = "Terakhir Dikemaskini";
	$lang['label_ba_score'] = "Penilaian dari Business Analyst";
	
/* innovation/detail.php */
	$lang['title_detail_innovation'] = "Detail Inovasi";
	$lang['label_in_bahasa_melayu'] = "Malay";
	$lang['label_inspiration'] = "Ilham/Idea";
	$lang['label_product_description'] = "Keterangan Produk";
	$lang['label_material'] = "Bahan Buatan";
	$lang['label_created_date'] = "Tarikh Penciptaan";
	$lang['label_discovered_date'] = "Tarikh Penemuan";
	$lang['label_manufacturing_cost'] = "Kos Pembuatan";
	$lang['label_selling_price'] = "Harga Jualan";
	$lang['label_how_to_use'] = "Cara Penggunaan";
	$lang['label_innovation_category'] = "Kategori Inovasi";
	$lang['label_subcategory'] = "Subcategory";
	$lang['label_target'] = "Kumpulan Sasaran";
	$lang['label_myipo_protection'] = "Adakah Inovasi ini telah mendapat perlindungan MYIPO?";
	$lang['label_special_achievement'] = "Pencapaian Khas";
	$lang['label_innovation_picture'] = "Gambar Inovasi";

	/* evaluation/index.php */
	$lang['title_evaluation_task'] = "Penilaian";
	$lang['label_status'] = "Status";
	$lang['button_evaluate'] = "Isi Penilaian";
	$lang['button_view_evaluation'] = "Lihat Hasil Penilaian";

	/* evaluation/form1.php */
	$lang['title_evaluation_form'] = "Borang Penilaian Inovasi";
	$lang['label_date'] = "Tarikh";
	$lang['label_evaluation_point'] = "Tumpuan Penilaian";
	$lang['label_criteria'] = "Kriteria";
	$lang['label_point'] = "Markah Penuh";
	$lang['value_point_1'] = "Idea";
	$lang['value_point_1_1'] = "Kreativiti"; 
	$lang['value_point_1_1_a'] = "Keaslian - Inovasi yang dihasilkan adalah idea asli";
	$lang['value_point_1_1_b'] = "Adaptasi - Inovasi yang dihasilkan merupakan penambahbaikan atau pengubahsuaian idea sedia ada";
	$lang['value_point_2'] = "Hasil Inovasi (Output)";
	$lang['value_point_2_2'] = "Tahap Pelaksanaan";
	$lang['value_point_2_2_a'] = "Secara penuh (melepasi peringkat percubaan)";
	$lang['value_point_2_2_b'] = "Secara percubaan (di peringkat prototaip)";
	$lang['value_point_2_3'] = "Replicability";
	$lang['value_point_2_3_a'] = "Boleh dilaksanakan secara terus di pelbagai tempat";
	$lang['value_point_2_3_b'] = "Dilaksanakan dengan pengubahsuaian mengikut keperluan tanpa melibatkan kos yang tinggi";
	$lang['value_point_3'] = "Impak (Outcome)";
	$lang['value_point_3_4'] = "Efisien (Kecekapan)";
	$lang['value_point_3_4_a'] = "Penjimatan masa";
	$lang['value_point_3_4_b'] = "Penjimatan kos";
	$lang['value_point_3_4_c'] = "Peningkatan produktiviti";
	$lang['value_point_3_4_d'] = "Mudah digunakan (user-friendly)";
	$lang['value_point_3_5'] = "Signifikan";
	$lang['value_point_3_5_desc'] = "Memberi kesan atau impak ketara kepada komuniti dari segi penjimatan kos atau masa dan inovasi boleh diterima pakai atau dilaksanakan di komuniti lain. Keberhasilan atau potensi keberhasilan yang signifikan dan berimpak tinggi pada peringkat";
	$lang['value_point_3_5_a'] = "Tinggi (Global)";
	$lang['value_point_3_5_b'] = "Sederhana (Nasional)";
	$lang['value_point_3_5_c'] = "Rendah (Tempatan)";
	$lang['value_point_4'] = "Pengurusan";
	$lang['value_point_4_6'] = "Komitmen";
	$lang['value_point_4_6_a'] = "Kewangan";
	$lang['value_point_4_6_b'] = "Pengurusan Sumber Manusia";
	$lang['value_point_4_6_c'] = "Insentif";
	$lang['value_point_4_6_d'] = "Penghargaan";
	$lang['value_point_4_6_e'] = "Peralatan";
	$lang['value_point_5'] = "Relevan";
	$lang['value_point_5_desc'] = "Ciptaan berupaya memenuhi keperluan komuniti atau menyelesaikan masalah. Memaparkan perubahan yang selaras dengan aspek-aspek agenda nasional seperti sumbangan kepada pembentukan ekonomi nilai tinggi, penjimatan kos yang ketara kepada operasi Kerajaan, penjanaan hasil kerajaan yang ketara, kolaborasi unik dengan rakyat, usaha nation building yang berkesan dan sebagainya";
	$lang['value_point_6'] = "Keberkesanan";
	$lang['value_point_6_desc'] = "Memaparkan faedah yang nyata dengan keberhasilan yang mampan serta meliputi tempoh masa yang jelas membuktikan keberkesanannya.";
	$lang['value_point_7'] = "Kualiti";
	$lang['value_point_7_desc'] = "Rekabentuk inovasi mengambil kira dan memenuhi kehendak dan keperluan pengguna.";
	$lang['value_point_8'] = "Potensi";
	$lang['value_point_8_desc'] = "Inovasi ini berpotensi untuk berkembang dan mempunyai perancangan masa hadapan.";
	$lang['label_total'] = "Jumlah";
	$lang['button_save_to_draft'] = "Simpan ke Draft";
	$lang['label_max_value'] = "Markah maximum untuk kriteria ini ialah";

/* evaluation/header.php */
	$lang['button_evaluation_form_1'] = "Borang Penilaian 1";
	$lang['button_evaluation_form_2'] = "Borang Penilaian 2";

/* evaluation/form2.php */
	$lang['value_point_f2_a'] = "Adakah terdapat keperluan bagi produk inovasi ini dipasaran?";
	$lang['value_point_f2_b'] = "Adakah kaedah yang digunakan oleh inovator merupakan kaedah terbaik dipasaran?";
	$lang['value_point_f2_c'] = "Adakah inovasi ini mempunyai manfaat kepada masyarakat/organisasi/individu?";
	$lang['value_point_f2_d'] = "Adakah kemungkinan terdapat pesaingan dalam pasaran bagi inovasi ini?";
	$lang['value_point_f2_e'] = "Ulasan dan cadangan penambahbaikan terhadap inovasi mengikut bidang kepakaran.";
	$lang['label_review'] = "Ulasan/Cadangan";
	$lang['button_send_evaluation'] = "Hantar Penilaian";
	$lang['msg_confirm_send_evaluation'] = "Adakah anda pasti ingin menghantar penilaian ini?";
	
/* portfolio/list.php */
	$lang['title_portfolio'] = "Portfolio";
	$lang['link_assign'] = "Menyerahkan";
	$lang['label_title'] = "Tajuk";
	$lang['label_manager'] = "Pengurus";
	$lang['label_assignment_date'] = "Tarikh Tugasan";
	$lang['link_view_evaluation_result'] = "Lihat Hasil Evaluasi";
	$lang['info_task_status_done'] = "Telah Selesai";
	$lang['info_task_status_not_yet'] = "Belum Selesai";
	$lang['label_assign_portfolio'] = "Penyerahan Portfolio";
	$lang['link_edit'] = 'Ubah';
	
/* portfolio/view_evaluation.php */
	$lang['title_attachment_detail'] = "Lampiran Terperinci";
	$lang['label_item'] = "Skop";
	$lang['label_attachment'] = "Lampiran";
	$lang['label_notes'] = "Catatan";
	$lang['info_no_comment'] = "Tiada Komentar dari IDeC Head";
	$lang['task_updates'] = "Nota";
	$lang['task_comment'] = "Tugasan Komentar";
	
/* portfolio/check.php */
	$lang['title_portfolio_attachment'] = "Lampiran Portfolio";
	$lang['info_allowed_file'] = "Jenis fail yang dibenarkan";
	$lang['link_add_asset'] = "Tambah Aset";
	$lang['link_title_uploaded_asset'] = "Asset URL yang telah berjaya disimpan";
	$lang['label_type'] = "Jenis";
	$lang['label_description'] = "Deskripsi";
	$lang['label_content_type'] = "Jenis Kandungan";
	$lang['title_portfolio_progress'] = "Kemajuan Portfolio";
	$lang['info_uploaded'] = "Completed Activities";
	$lang['info_not_uploaded'] = "Outstanding Activities";
	$lang['title_portfolio_note'] = 'Nota Portfolio';
	$lang['label_content'] = 'Catatan';
	$lang['label_creation_date'] = 'Tarikh Penciptaan';
	$lang['info_no_notes'] = 'Tiada nota pada tarikh ini';
	$lang['scope'] = 'Skop';
	$lang['label_progress'] = 'Kemajuan';
	$lang['label_amount_spend'] = 'Jumlah Perbelanjaan';
	$lang['save'] = 'Simpan';
	
/* portfolio/task_list.php */
	$lang['link_setup_scope'] = "Set Up Scope";
	$lang['link_check_portfolio'] = "Semak Portfolio";
	$lang['info_dont_have_task'] = "Anda belum diserahkan portfolio";
	$lang['label_portfolio_task'] = "Senarai Portfolio";
	$lang['subtitle_notif_task'] = "Tugasan Portfolio untuk hari ini dan esok";
	$lang['subtitle_notif_note'] = "Nota Portfolio untuk hari ini dan esok";
	$lang['portfolio_title'] = "Tajuk Portfolio";
	$lang['msg_no_notif_task'] = "Tidak ada tugasan";
	$lang['msg_no_notif_note'] = "Tidak ada nota";
	
/* portfolio/scope_form.php */
	$lang['label_weightage'] = "Pecahan";
	$lang['label_task_list'] = "Pecahan Skop";
	$lang['label_task_name'] = "Perkara";
	$lang['label_start_date'] = "Tarikh Mula";
	$lang['label_end_date'] = "Tarikh Tamat";
	$lang['label_budget'] = "Bajet";
	$lang['label_ballance'] = "Baki Belanjawan ";
	$lang['pending_pd_approval'] = "Menunggu Kelulusan PD";

/* portfolios.php */
	$lang['msg_success_save_portfolio'] = "Data portfolio telah berjaya disimpan";
	$lang['msg_failed_save_portfolio'] = "Data portfolio tidak berjaya disimpan";
	$lang['msg_success_delete_portfolio'] = "Data portfolio telah dipadam";
	$lang['msg_failed_delete_portfolio'] = "Data portfolio tidak terpadam";
	$lang['msg_success_save_attachment'] = "Lampiran portfolio telah berjaya disimpan";
	$lang['msg_failed_save_attachment'] = "Lampiran portfolio tidak berjaya disimpan";
	$lang['msg_success_delete_attachment'] = "Lampiran portfolio telah dipadam";
	$lang['msg_failed_delete_attachment'] = "Lampiran portfolio tidak terpadam";
	$lang['msg_success_save_asset'] = "Asset Inovasi telah berjaya disimpan";
	$lang['msg_failed_save_asset'] = "Asset Inovasi tidak berjaya disimpan";
	$lang['msg_success_save_comment'] = "Komentar telah berjaya disimpan";
	$lang['msg_failed_save_comment'] = "Komentar tidak berjaya disimpan";
	$lang['msg_success_save_note'] = "Nota telah berjaya disimpan";
	$lang['msg_failed_save_note'] = "Nota tidak berjaya disimpan";
	$lang['msg_success_delete_note'] = "Nota telah terpadam";
	$lang['msg_failed_delete_note'] = "Nota tidak terpadam";
	$lang['title_popup_detail_innovation'] = "Butiran Inovasi";
	$lang['msg_success_save_portfolio_scope'] = "Skop portfolio telah berjaya disimpan";
	$lang['msg_failed_save_portfolio_scope'] = "Skop portfolio tidak berjaya disimpan";
	$lang['scope_portfolio_warning'] = "Data skop portfolio tidak boleh diubah. Pastikan anda memasukkan data yang betul.";
	$lang['msg_success_save_amount_spend'] = "Jumlah perbelanjaan telah berjaya disimpan";
	$lang['msg_failed_save_portfolio_scope'] = "Jumlah perbelanjaan tidak berjaya disimpan";
	$lang['msg_success_approve_task'] = "Portfolio tugasan skop telah berjaya diluluskan";
	$lang['msg_failed_approve_task'] = "Portfolio tugasan skop tidak berjaya diluluskan";
	
/* report */
	$lang['title_innovation_by_category'] = "Inovasi berdasarkan Kategori";
	$lang['label_innovation_year'] = "Tahun Inovasi";
	$lang['button_generate_report'] = "Menjana Laporan";
	$lang['info_total_innovation'] = "Jumlah Inovasi";
	$lang['info_total_innovator'] = "Jumlah Inovator";
	$lang['title_innovation_by_state'] = "Inovasi berdasarkan Negeri";
	$lang['title_innovation_by_year'] = "Inovasi berdasarkan Tahun";
	$lang['title_innovator_by_state'] = "Inovator berdasarkan Negeri";
	$lang['title_innovator_by_category'] = "Inovator berdasarkan Kategori";
	$lang['title_innovator_by_gender'] = "Inovator berdasarkan Jantina";
	$lang['title_expert_by_state'] = "Pakar berdasarkan Negeri";
	$lang['info_total_expert'] = "Jumlah Pakar";
	$lang['title_university_by_state'] = "Universiti berdasarkan Negeri";
	$lang['title_university_by_category'] = "Universiti berdasarkan Kategori";
	$lang['info_total_university'] = "Jumlah Universiti";
	$lang['label_contact_no'] = "Nombor Kontak";
	$lang['title_innovation_eval_burndown'] = "Innovation Evaluation Burndown";
	$lang['title_portfolio_burndown'] = "Portfolio Burndown";
	
/* report/innovation_state */
	$lang['title_innovation_by_state'] = "Inovasi berdasarkan Negeri";
	$lang['msg_failed_save_comment'] = "Komentar tidak berjaya disimpan";

/* report/evaluation/ */
	$lang['title_innovation_evaluation'] = 'Penilaian Inovasi';
	$lang['label_total_score'] = 'Markah Terkumpul';
	$lang['label_score_average'] = 'Markah Purata';
	$lang['label_overall_average'] = 'Purata Keseluruhan';
	$lang['title_top_score'] = '100 Top Score Inovasi';
	$lang['title_detail_innovator'] = "Detail Inovator";
	$lang['filter_info'] = "Anda boleh memilih lebih daripada 1 ";
	$lang['label_filter_by'] = 'Menapis berdasarkan';
	$lang['button_export_to_excel'] = 'Export ke Excel';
	
/*locator.php*/
	$lang['label_locator'] = 'Lokasi';
	$lang['label_search'] = 'Cari';
	$lang['label_enter'] = 'Masukkan';
	//pin detail
		$lang['label_innovation'] = 'Inovasi';
		$lang['label_innovator'] = 'Inovator';
		$lang['label_product_desc']   = 'Penerangan Produk';
		$lang['label_category']   = 'Kategori';
		$lang['label_address']   = 'Alamat';
		$lang['label_state']   = 'Negeri';
		$lang['label_university'] = 'Universiti';
		$lang['label_college'] = 'Kolej';
		$lang['label_specialty'] = 'Kategori';
		$lang['label_contact_no'] = 'No. Telefon';
		$lang['label_expert'] = 'Pakar';
		$lang['label_expertise'] = 'Kepakaran';
		$lang['label_experience'] = 'Pengalaman';
	$lang['label_search_result']  = 'Hasil Pencarian';
	$lang['label_all'] = "Semua";
	$lang['label_project_title'] = "Tajuk Projek";
	$lang['label_ref_no'] = "No. Rujukan";
	$lang['label_lead_agency'] = "Penyedia Teknologi";
	$lang['label_collaborator'] = "Kolaborator";
	$lang['label_beneficiary_community'] = "Penerima Teknologi";
	$lang['label_duration'] = "Tempoh (Bulan)";
	$lang['label_amount'] = "Jumlah Peruntukan";
	$lang['label_year'] = "Tahun";

/*State Income Map*/
	$lang['title_map'] = "Penyebaran Pendapatan Malaysia";
	$lang['label_select_gsp_year'] = "Pilih Penyebaran Pendapatan Tahun:";
	$lang['label_select_category'] = "Pilih Kategori:";
	$lang['commercial_center'] = "Pusat Komersial";

/*Elections Map*/
	$lang['title_elections_map'] 	= "Peta Kawasan Pemilihan Malaysia";
	$lang['label_political_party'] = "Komponen Parti:";
	$lang['label_vote_percentage'] = "Peratus Mengundi";
	$lang['label_inno_category'] = "Kategori Inovasi";
	$lang['label_display_inno'] = "Paparkan Inovasi";
	$lang['label_sort_by'] = "Susun mengikut kerusi:";
	
/*Semantic Search*/
	$lang['title_semantic_search'] = "Carian Semantik";
	$lang['label_data_details'] = "Paparan Data";
	$lang['label_graph_details'] = "Paparan Grafik";

/*Gallery*/
	$lang['title_gallery'] = "Galeri";
	$lang['link_gallery'] = "galeri";
	
	$lang['title_management_dashboard'] = "Dashboard Pengurusan";
	
/*Edit Password*/
	$lang['title_edit_password'] = "Ubah Kata Laluan";
	$lang['label_old_password'] = "Kata Laluan Lama";
	$lang['label_new_password'] = "Kata Laluan Baru";
	$lang['msg_success_save_password'] = "Kata Laluan Baru telah berjaya disimpan";
	$lang['msg_failed_save_password'] = "Kata Laluan Baru tidak berjaya disimpan";
	$lang['msg_failed_oldpass_save_password'] = "Kata Lalulan Lama tidak cocok";

	$lang['title_group_scoring'] = "Penilaian Kumpulan";

/*innovation/executive_summary*/
	$lang['title_executive_summary'] = "Ringkasan Eksekutif";
	$lang['opt_es_status_research'] = "Dalam Kajian";
	$lang['opt_es_status_active'] = "Aktif";
	$lang['msg_success_save_exc_summary'] = "Executive Summary telah berjaya disimpan.";

/*management_plan*/
	$lang['title_management_plan'] = "Plan Akaun";
	$lang['label_background_of'] = "Latar Belakang ";
	$lang['label_account_details'] = "Butiran Akaun";
	$lang['label_name_of_inventor'] = "Nama Pencipta";
	$lang['label_profession'] = "Profesion";
	$lang['label_current_address'] = "Alamat Terkini";
	$lang['label_next_of_kin_contact'] = "Telefon Kerabat";
	$lang['label_innovation_title'] = "Tajuk Inovasi";
	$lang['label_stage_no'] = "Peringkat";
	$lang['label_certificate_no'] = "No. Sijil";
	$lang['label_key_challenges_of_account'] = "Cabaran Utama Akaun";
	$lang['label_account_targets'] = "Sasaran Akaun";
	$lang['label_challenges'] = "Cabaran";
	$lang['label_mitigation'] = "Faktor Pengurangan";
	$lang['label_deadline'] = "Tarikh Siap";
	$lang['label_remarks'] = "Catatan";
	$lang['btn_add_target'] = "Tambah Sasaran";
	$lang['btn_add_key_challenges'] = "Tambah Cabaran Utama";
	$lang['label_revision_note'] = "Sejarah Semakan";
	$lang['label_date_of_revision'] = "Tarikh Semakan";
	$lang['label_revised_by'] = "Disemak oleh";
	$lang['btn_add_management_plan'] = "Tambah Plan Akaun";
	$lang['msg_success_save_plan'] = "Plan Akaun telah berjaya disimpan.";

/*Management Log*/
	$lang['title_management_logs'] = "Catatan Akaun";
	$lang['label_venue'] = "Tempat";
	$lang['btn_add_management_log'] = "Tambah Log";
	$lang['label_account_name'] = "Nama Akaun";
	$lang['label_account_manager'] = "Pengurus Akaun";
	$lang['btn_add_key_challenges'] = "Tambah Cabaran Utama";
	$lang['msg_success_save_log'] = "Catatan Akaun telah berjaya disimpan.";
	$lang['title_executive_summary'] = "Ringkasan Eksekutif";

/*List innovation login as project director*/
	$lang['edit_innovation_to_hip6'] = "Edit Inovasi untuk HIP 6";
	$lang['change_source_to_hip6'] = "Daftar ke dalam HIP6";
	$lang['success_change_to_hip6'] = "Inovasi HIP6";

/*Committee meeting*/
	$lang['title_committee_meeting'] = "Catatan Mesyuarat";
	$lang['btn_add_committee_meeting'] = "Tambah Catatan Mesyuarat";
	$lang['created_by_label'] = "Dicipta Oleh";
	$lang['msg_success_delete_meeting'] = "Mesyuarat Jawatankuasa telah dipadam.";
	$lang['msg_success_add_meeting'] = "Mesyuarat Jawatankuasa telah ditambah.";
	$lang['msg_success_edit_meeting'] = "Mesyuarat Jawatankuasa telah disunting.";
	$lang['msg_failed_delete_meeting'] = "Mesyuarat Jawatankuasa tidak boleh dipadam.";
	$lang['msg_failed_add_meeting'] = "Mesyuarat Jawatankuasa tidak boleh ditambah.";
	$lang['msg_failed_edit_meeting'] = "Mesyuarat Jawatankuasa tidak boleh disunting.";
	$lang['label_committee_meeting_note'] = "Catatan Pengarah Program";
	$lang['label_committee_meeting_date'] = "Tarikh Mesyuarat";
	$lang['label_committee_meeting_attendance'] = "Yang Hadir";
	$lang['btn_add_attachment'] = "Tambah Lampiran";

	$lang['label_load_more'] = "Memuatkan Lebih";

/*hip6 Evaluation*/
	$lang['label_business_analyst'] = "Business Analyst";

/*hip6 Evaluation*/
	$lang['label_business_analyst'] = "Business Analyst";
	$lang['hip6_label_fa_1'] = "Novelty of Innovation";
	$lang['hip6_label_cr_1_1'] = "Originality Innovation";
	$lang['hip6_label_desc_1_1'] = "Is the innovation novel in the sense that it is unique and can be patented?";
	$lang['hip6_label_fa_2'] = "Excellence";
	$lang['hip6_label_cr_2_1'] = "Comparable with Competitors";
	$lang['hip6_label_desc_2_1'] = "Is the innovation comparable to its competitors in the market?";
	$lang['hip6_label_cr_2_2'] = "Innovation impact";
	$lang['hip6_label_desc_2_2'] = "Will the innovation be able to resolve a large part of a problem or only a small part?";
	$lang['hip6_label_fa_3'] = "Affordability";
	$lang['hip6_label_cr_3_1'] = "Unit Cost";
	$lang['hip6_label_desc_3_1'] = "Is the unit cost when mass produce low i.e. a fraction of existing competitors? ";
	$lang['hip6_label_cr_3_2'] = "Cost of assistance";
	$lang['hip6_label_desc_3_2'] = "What is the cost of assistance to HIP6, high, moderate or low?";
	$lang['hip6_label_fa_4'] = "Target Group";
	$lang['hip6_label_cr_4_1'] = "Excluded group";
	$lang['hip6_label_desc_4_1'] = "Is the innovation targeted to HIP6 target group e.g. single mothers, OKUs, Indigenous people and low income groups?";
	$lang['hip6_label_fa_5'] = "Volume";
	$lang['hip6_label_cr_5_1'] = "Market size";
	$lang['hip6_label_desc_5_1'] = "Will the innovation address a large, medium or small number of consumers?";
	$lang['hip6_label_fa_6'] = "Sustainability";
	$lang['hip6_label_cr_6_1'] = "Attitude of entrepreneur";
	$lang['hip6_label_desc_6_1'] = "Does the innovator have positive attitude in sharing his innovation with market and sharing the HIP6 mission?";
	$lang['hip6_label_cr_6_2'] = "Diffusion platform";
	$lang['hip6_label_desc_6_2'] = "Is there a ready innovation diffusion vehicle i.e. an incorporated company, social enterprise or licensing partners?";
	$lang['hip6_label_cr_6_3'] = "Strategic linkages";
	$lang['hip6_label_desc_6_3'] = "Is there any existing strategic linkages (e.g. agencies, funds) that can be tapped to assist the innovator?";
	$lang['label_comment'] = "Komentar";
	$lang['label_change_score'] = "Ubah Markah";

/*MSI*/
	$lang['project_listing'] = "Senarai Projek";
	$lang['label_project'] = "Projek";