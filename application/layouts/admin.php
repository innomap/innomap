<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Innomap Administrator | {{title}}</title>

		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		{{metas}}
		
		<link rel="icon" href="<?= base_url()."assets/img/favicon.ico";?>" />

		<script>var base_url = <?php echo json_encode(base_url().ADMIN_DIR);?>;</script>
		<script>var msi_url = <?php echo json_encode(base_url().MSI_DIR);?>;</script>
		<script>
			var front_url = <?php echo json_encode(base_url());?>;
			var semantic_url 	= <?=json_encode("http://198.61.173.72:8080/Innomap/innomap2/");?>;
		</script>


		<!--BUAT RESPONSIVE-->	
		<noscript>
			<link rel="stylesheet" href="<?= base_url(); ?>assets/css/responsive/mobile.min.css" />
		</noscript>
		<script>
			// Edit to suit your needs.
			var ADAPT_CONFIG = {
			  // Where is your CSS?
			  path: front_url+'assets/css/responsive/',

			  // false = Only run once, when page first loads.
			  // true = Change on window resize and page tilt.
			  dynamic: true,

			  // First range entry is the minimum.
			  // Last range entry is the maximum.
			  // Separate ranges by "to" keyword.
			  range: [
				'0px    to 760px  = mobile.min.css',
				'760px  to 980px  = 720.min.css',
				'980px  to 1280px = 960.min.css',
				'1280px to 1600px = 960.min.css',
				'1600px to 1940px = 1560.min.css',
				'1940px to 2540px = 1920.min.css',
				'2540px           = 2520.min.css'
			  ]
			};
		</script>

		<script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery-2.0.2.min.js"></script>
		<script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery-ui-1.10.4.custom.min.js"></script>
		<script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery.validate.js"></script>
		<script type="text/javascript" src="<?= base_url(); ?>assets/js/administrator/general.js"></script>
		<script type="text/javascript" src="<?= base_url(); ?>assets/js/administrator/form_validation.js"></script>
		<script type="text/javascript" src="<?= base_url(); ?>assets/js/adapt.min.js"></script>
		<script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="<?= base_url(); ?>assets/js/dataTables_initialize.js"></script>
		{{scripts}}
		<link rel="stylesheet" href="<?= base_url() ?>assets/css/ui-lightness/jquery-ui-1.10.4.custom.min.css" />
		<link rel="stylesheet" href="<?= base_url() ?>assets/css/general.css" type="text/css" />
		<link rel="stylesheet" href="<?= base_url() ?>assets/css/admin-style.css" type="text/css" />
		<link rel="stylesheet" href="<?= base_url() ?>assets/css/jquery.dataTables.css" type="text/css" />
		{{styles}}
		
	</head>
	<body>
		<div id="container" class="container_12">
			<!-- header -->
			<div class="header grid_12 alpha omega">
				{{header}}
			</div>
			<div class="clear"></div>
			<!-- end header -->
			<!-- content -->
			<div class="content grid_12 alpha omega">
				{{content}}
			</div>
			<!-- end content -->
			<!-- <div id="footer">{{footer}}</div> -->
		</div>

		<div id="admin-dialog"></div>
	</body>
</html>