<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class API_Controller extends CI_Controller {

	protected $user_id = null;

	function __construct() {
		parent::__construct();
		$this->load->helper('api');
		$this->load->model('api_token');
		$this->layout = false;
	}

	protected function response($data, $http_code = 200) {
		header('HTTP/1.1: ' . $http_code);
		header('Status: ' . $http_code);
		header('Content-Type: application/json');

		exit(json_encode($data));
	}

	protected function check_access_token() {
		$this->user_id = $this->api_token->check_access_token($this->input->get('access_token'));
		if(!$this->user_id) {
			$this->user_id = $this->api_token->check_access_token($this->input->post('access_token'));
		}

		if(!$this->user_id) {
			$this->response(array(
				'status' => -1,
				'message' => 'Invalid access token.'
			), 401);
		}
	}

}
