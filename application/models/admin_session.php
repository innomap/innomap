<?php

class Admin_session extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	function get() {
		return $this->session->userdata(ADMIN_SESSION);
	}
	
	function set($data) {
		$this->session->set_userdata(ADMIN_SESSION, $data);
		$this->session->set_userdata('language','english');
	}
	
	function create($username, $password) {
		$this->load->model('user');
		if($data = $this->user->auth_with_role($username, $password, array('user_role.role_id' => ROLE_ADMIN))){
			unset($data['password']);
			$this->set($data);
			return true;
		}else{
			return false;
		}
	}
	
	function clear() {
		$this->session->unset_userdata(ADMIN_SESSION);
	}
}