<?php
class Article extends CI_Model {
	function __construct() {
		parent::__construct();

		if($this->session->userdata('language') == LANGUAGE_MELAYU){
			$this->article_title = "title_in_melayu";
			$this->article_content = "content_in_melayu";
			$this->category_name = "name_in_melayu";
		}else{
			$this->article_title = "title";
			$this->article_content = "content";
			$this->category_name = "name";
		}
	}
	
	function get($where = NULL, $order_by = NULL){
		$this->db->select('article.*,user.username, article_category.name as category_name');
		$this->db->from('article');
		$this->db->join('user','user.user_id = article.creator_id');
		$this->db->join('article_category','article_category.article_category_id = article.article_category_id');
		if($where != NULL){
			$this->db->where($where);
		}
		if($order_by != NULL){
			$this->db->order_by($order_by,'ASC');	
		}
		return $this->db->get();
	}

	function add($data){
		$this->db->insert('article',$data);
		return $this->db->insert_id();
	}

	function edit($id,$data){
		$this->db->where('article_id',$id);
		return $this->db->update('article',$data);
	}

	function delete($id){
		$this->db->where('article_id',$id);
		return $this->db->delete('article');
	}

	// dynamic language
	function get_list($where = NULL, $order_by = NULL){
		$this->db->select('article.*,'.$this->article_title.' as title, '.$this->article_content.' as content ,user.username, '.$this->category_name.' as category_name');
		$this->db->from('article');
		$this->db->join('user','user.user_id = article.creator_id');
		$this->db->join('article_category','article_category.article_category_id = article.article_category_id');
		if($where != NULL){
			$this->db->where($where);
		}
		if($order_by != NULL){
			$this->db->order_by($order_by,'ASC');	
		}
		return $this->db->get();
	}

	function get_list_for_site($where = NULL){
		$this->db->select('article_id as id, '.$this->article_title.' as title, '.$this->article_content.' as content');
		$this->db->from('article');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}
}
