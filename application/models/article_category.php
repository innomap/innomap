<?php
class Article_category extends CI_Model {
	function __construct() {
		parent::__construct();

		if($this->session->userdata('language') == LANGUAGE_MELAYU){
			$this->category_name = "name_in_melayu";
			$this->category_description = "description_in_melayu";
		}else{
			$this->category_name = "name";
			$this->category_description = "description";
		}
	}
	
	function get($where = NULL,$order_by = NULL, $group_by = NULL){
		$this->db->select('*');
		$this->db->from('article_category');
		if($where != NULL){
			$this->db->where($where);
		}
		if($order_by != NULL){
			$this->db->order_by($order_by,'ASC');	
		}
		if($group_by != NULL){
			$this->db->group_by($group_by);	
		}
		return $this->db->get();
	}

	function add($data){
		$this->db->insert('article_category',$data);
		return $this->db->insert_id();
	}

	function edit($id,$data){
		$this->db->where('article_category_id',$id);
		return $this->db->update('article_category',$data);
	}

	function delete($id){
		$this->db->where('article_category_id',$id);
		return $this->db->delete('article_category');
	}

	//dynamic language
	function get_list($where = NULL){
		$this->db->select('article_category_id,'.$this->category_name.' as name, '.$this->category_description.' as description, status, published_date');
		$this->db->from('article_category');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}

	function get_list_for_site($where = NULL){
		$this->db->select('article_category_id as id, '.$this->category_name.' as title, '.$this->category_description.' as content');
		$this->db->from('article_category');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}
}
