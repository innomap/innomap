<?php
class Category extends CI_Model {
	function __construct() {
		parent::__construct();

		//select field by language option
		if($this->session->userdata('language') == LANGUAGE_MELAYU){
			$this->category_name = 'name_in_melayu';
		}else{
			$this->category_name = 'name';
		}
	}
	
	/*category table*/
	function get($where = null){
		$this->db->select($this->category_name.' as name, category_id, pin_url');
		$this->db->from('category');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}
	
	/*subcategory table*/
	function get_subcategory($where = NULL){
		$this->db->select($this->category_name.' as name, subcategory_id');
		$this->db->from('subcategory');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}
	
}
