<?php
class Commercial extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	//Commercial Center

	function get($where = NULL){
		$this->db->select('commercial_center.*, state.name as state_name');
		$this->db->from('commercial_center');
		$this->db->join('state','commercial_center.state_id=state.state_id','left');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}

	function add($data){
		$this->db->insert('commercial_center',$data);
		return $this->db->insert_id();
	}

	function edit($id, $data){
		$this->db->where('commercial_id', $id);
		$this->db->update('commercial_center', $data);
		return true;
	}

	function delete($id){
		$this->db->where('commercial_id',$id);
		$this->db->delete('commercial_center');
		return true;
	}

	function get_states($where = NULL){
		$this->db->select('*');
		$this->db->from('state');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}
}
