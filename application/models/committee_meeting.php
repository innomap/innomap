<?php
class Committee_meeting extends CI_Model {
	function __construct() {
		parent::__construct();
	}
	
	function get($where = NULL){
		$this->db->select('committee_meeting.*, user.username');
		$this->db->from('committee_meeting');
		$this->db->join('user','user.user_id = committee_meeting.user_id');
		if($where != NULL){
			$this->db->where($where);
		}

		return $this->db->get();
	}
	
	function add($data){
		$this->db->insert('committee_meeting',$data);
		return $this->db->insert_id();
	}
	
	function edit($id,$data){
		$this->db->where('committee_meeting_id',$id);
		return $this->db->update('committee_meeting',$data);
	}
	
	function delete($id){
		$this->db->where('committee_meeting_id',$id);
		return $this->db->delete('committee_meeting');
	}

	function get_list($where = NULL){
		$this->db->select('management_log.*, innovator.name as innovator_name');
		$this->db->from('management_log');
	//	$this->db->join('user','user.user_id = management_log.user_id');
		$this->db->join('innovator','innovator.innovator_id = management_log.innovator_id');
		if($where != NULL){
			$this->db->where($where);
		}

		return $this->db->get();
	}
}