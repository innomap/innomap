<?php
class Committee_meeting_attachment extends CI_Model {
	function __construct() {
		parent::__construct();
	}
	
	function get($where = NULL){
		$this->db->select('*');
		$this->db->from('committee_meeting_attachment');
		if($where != NULL){
			$this->db->where($where);
		}

		return $this->db->get();
	}
	
	function add($data){
		$this->db->insert('committee_meeting_attachment',$data);
		return $this->db->insert_id();
	}
	
	function edit($id,$data){
		$this->db->where('attachment_id',$id);
		return $this->db->update('committee_meeting_attachment',$data);
	}
	
	function delete($id){
		$this->db->where('attachment_id',$id);
		return $this->db->delete('committee_meeting_attachment');
	}
}