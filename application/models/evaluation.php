<?php
class Evaluation extends CI_Model {
	function __construct() {
		parent::__construct();

		//select field by language option
		if($this->session->userdata('language') == LANGUAGE_MELAYU){
			$this->innovation = 'innovation.name_in_melayu';
		}else{
			$this->innovation = 'innovation.name';
		}
	}

	function get($where = NULL){
		$this->db->select('*');
		$this->db->from('innovation_evaluation');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}

	function get_detail($where = NULL){
		$this->db->select('innovation_evaluation.*, '.$this->innovation.' as innovation_name, expert.name as expert_name');
		$this->db->from('innovation_evaluation');
		$this->db->join('innovation','innovation.innovation_id = innovation_evaluation.innovation_id');
		$this->db->join('expert','expert.expert_id = innovation_evaluation.evaluator_expert_id');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}

	function add($data){
		$this->db->insert('innovation_evaluation',$data);
		return $this->db->insert_id();
	}

	function edit($evaluation_id,$data){
		$this->db->where('evaluation_id',$evaluation_id);
		return $this->db->update('innovation_evaluation',$data);
	}

	function get_task($where = NULL){
		$this->db->select('expert_task.expert_task_id,expert_task.assigner_id');
		$this->db->from('expert_task');
		$this->db->join('expert_task_innovation as innovation','expert_task.expert_task_id = innovation.expert_task_id');
		if($where = NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}

	function get_task_list($where = NULL){
		$this->db->select('expert_task.*,(SELECT COUNT(expert_task_id) FROM expert_task_innovation WHERE expert_task.expert_task_id = expert_task_innovation.expert_task_id) as has_innovation',false);
		$this->db->from('expert_task_expert');
		$this->db->join('expert_task','expert_task.expert_task_id = expert_task_expert.expert_task_id');
		$this->db->having('has_innovation > 0');
		if($where != NULL){
			$this->db->where($where);
		}
		
		return $this->db->get();
	}

	function get_innovation_task($where = NULL){
		$this->db->select('expert_task_innovation.*, expert_task.title,'.$this->innovation.' as innovation_name, expert_task_expert.expert_id, expert.name as expert_name');
		$this->db->from('expert_task_innovation');
		$this->db->join('expert_task','expert_task_innovation.expert_task_id = expert_task.expert_task_id');
		$this->db->join('innovation','expert_task_innovation.innovation_id = innovation.innovation_id');
		$this->db->join('expert_task_expert','expert_task.expert_task_id = expert_task_expert.expert_task_id');
		$this->db->join('expert', 'expert_task_expert.expert_id = expert.expert_id');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}
}