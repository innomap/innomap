<?php
class Expert extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function get($where = NULL){
		$this->db->select('*');
		$this->db->from('expert');
		$this->db->join('user', 'expert.expert_id = user.user_id');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}

	function get_name_list($where = NULL){
		$this->db->select('expert_id as id,name');
		$this->db->from('expert');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}

	function add($data){
		if($this->db->insert('expert',$data)){
			return true;
		}else{
			return false;
		}
	}
	
	function edit($id,$data){
		$this->db->where('expert_id',$id);
		return $this->db->update('expert',$data);
	}
	
	function delete($id){
		$this->db->where('expert_id',$id);
		return $this->db->delete('expert');
	}

	function get_state($where = NULL){
		$this->db->select('*');
		$this->db->from('state');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}
}