<?php
class Gallery extends CI_Model {
	function __construct() {
		parent::__construct();

		if($this->session->userdata('language') == LANGUAGE_MELAYU){
			$this->innovation_name = "name_in_melayu";
		}else{
			$this->innovation_name = "name";
		}
	}

	function get($where = NULL, $group_by = NULL){
		$this->db->select('innovation.innovation_id,innovation.'.$this->innovation_name.' as name, picture.picture');
		$this->db->from('innovation');
		$this->db->join('innovation_picture as picture','picture.innovation_id = innovation.innovation_id');
		$this->db->where('innovation.status', INNO_APPROVED);
		$this->db->where('picture.picture !=',0);
		if($where != NULL){
			$this->db->where($where);
		}
		if($group_by != NULL){
			$this->db->group_by($group_by);
		}
		return $this->db->get();
	}
}
?>