<?php
class Hip6_evaluation extends CI_Model {
	function __construct() {
		parent::__construct();

	}

	function get($where = NULL){
		$this->db->select('*');
		$this->db->from('innovation_hip6_evaluation');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}

	function add($data){
		$this->db->insert('innovation_hip6_evaluation',$data);
		return $this->db->insert_id();
	}

	function edit($evaluation_id,$data){
		$this->db->where('hip6_evaluation_id',$evaluation_id);
		return $this->db->update('innovation_hip6_evaluation',$data);
	}

	function get_detail($where = NULL){
		$this->db->select('eval.*, user.username');
		$this->db->from('innovation_hip6_evaluation as eval');
		$this->db->join('user','user.user_id = eval.ba_id');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}
}