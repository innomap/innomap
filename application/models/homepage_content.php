<?php
class Homepage_content extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	function get($where = null){
		$this->db->select('*');
		$this->db->from('homepage_content');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}

	function edit($id,$data){
		$this->db->where('homepage_content_id',$id);
		return $this->db->update('homepage_content',$data);
	}

	function get_for_site($where = NULL){
		$this->load->model(array('article','article_category','innovation'));
		$this->db->select('content.*');
		$this->db->from('homepage_content as content');
		if($where != NULL){
			$this->db->where($where);
		}
		$this->db->order_by('content.position','ASC');
		$this->db->group_by('content.position');
		$result = $this->db->get()->result_array();

		foreach($result as $key => $value){
			$result[$key]['content'] = array('id' => "", "title" => "","content" => "");
			$result[$key]['articles'] = array();

			if($value['type'] == HOMEPAGE_CONTENT_ARTICLE){
				$result[$key]['content'] = $this->article->get_list_for_site(array('article_id' => $value['content_id']))->row_array();
			}else if($value['type'] == HOMEPAGE_CONTENT_CATEGORY){
				$result[$key]['content'] = $this->article_category->get_list_for_site(array('article_category_id' => $value['content_id']))->row_array();
				$result[$key]['articles'] = $this->article->get_list_for_site(array('article_category_id' => $value['content_id']))->result_array();
			}else if($value['type'] == HOMEPAGE_CONTENT_INNOVATION){
				$result[$key]['content'] = $this->innovation->get_content_list_for_site(array('innovation.innovation_id' => $value['content_id']))->row_array();
			}
		}
		return $result;
	}
}