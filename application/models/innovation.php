<?php
class Innovation extends CI_Model {
	function __construct() {
		parent::__construct();

		if($this->session->userdata('language') == LANGUAGE_MELAYU){
			$this->innovation_name = "name_in_melayu";
			$this->innovation_description = "description_in_melayu";
		}else{
			$this->innovation_name = "name";
			$this->innovation_description = "description";
		}
	}

	function get($where = null, $limit = 0, $offset = 0){
		$this->db->select('*');
		$this->db->from('innovation');
		$this->db->where('is_mara_data', 0);
		if($where != NULL){
			$this->db->where($where);
		}
		if($limit > 0){
			$this->db->limit($limit,$offset);
		}
		return $this->db->get();
	}

	function get_innovation_innovator($where, $limit = NULL){
		$this->db->select('*');
		$this->db->from('innovator_innovation as ii');
		$this->db->join('innovator as i','i.innovator_id=ii.innovator_id');
		$this->db->where($where);
		if($limit != NULL){
			$this->db->limit($limit);
		}
		return $this->db->get()->result_array();
	}

	function add($data){
		$this->db->insert('innovation', $data);
		return $this->db->insert_id();
	}
	
	function edit($id,$data) {
		$this->db->where('innovation_id',$id);
		return $this->db->update('innovation', $data);
	}
	
	function delete($id){
		$this->db->where('innovation_id',$id);
		return $this->db->delete('innovation');
	}
	
	function get_innovation_category($where){
		return $this->db->get_where('innovation_category', $where)->result_array();
	}
	
	function add_innovation_category($data){
		$this->db->insert('innovation_category', $data);
	}
	
	function delete_innovation_category($innovation_id){
		$this->db->where('innovation_id',$innovation_id);
		$this->db->delete('innovation_category');
	}
	
	function get_innovation_subcategory($innovation_id){
		return $this->db->get_where('innovation_subcategory', array('innovation_id'=>$innovation_id))->result_array();
	}
	
	function add_innovation_subcategory($data){
		$this->db->insert('innovation_subcategory', $data);
	}
	
	function delete_innovation_subcategory($innovation_id){
		$this->db->where('innovation_id',$innovation_id);
		$this->db->delete('innovation_subcategory');
	}
	
	/*innovation picture*/
	function get_innovation_picture($innovation_id){
		if(is_array($innovation_id)){
			$where = $innovation_id;
		}else{
			$where = array('innovation_id' => $innovation_id);
		}
		return $this->db->get_where('innovation_picture', $where)->result_array();
	}
	
	function add_innovation_picture($data){
		$this->db->insert('innovation_picture',$data);
		return true;
	}
	
	function delete_innovation_picture($where){
		$this->db->where($where);
		$this->db->delete('innovation_picture');
		return true;
	}

	function get_name_list($where = NULL){
		$this->db->select('innovation_id,name');
		$this->db->from('innovation');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}

	function get_content_list($where = NULL){
		$this->db->select('innovation_id as id, name as title, description as content');
		$this->db->from('innovation');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}

	//dynamic language
	function get_content_list_for_site($where = NULL){
		$this->db->select('innovation.innovation_id as id, '.$this->innovation_name.' as title, '.$this->innovation_description.' as content, innovator.name as innovator_name, innovator.email, innovator.kod_no, innovator.address, innovator.postcode, innovator.city, innovator.picture, state.name as state_name');
		$this->db->from('innovation');
		$this->db->join('innovator_innovation','innovator_innovation.innovation_id = innovation.innovation_id');
		$this->db->join('innovator','innovator.innovator_id = innovator_innovation.innovator_id');
		$this->db->join('state','state.state_id = innovator.state_id');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}

	function get_list_for_admin($where = NULL){
		$this->db->select('innovation.innovation_id,innovation.top_innovation,innovation.'.$this->innovation_name.' as name,innovation.'.$this->innovation_description.' as description, (SELECT COUNT(content_id) FROM homepage_content WHERE content_id = innovation.innovation_id AND type = '.HOMEPAGE_CONTENT_INNOVATION.') as is_featured', false);
		$this->db->from('innovation');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}
	
	//get innovation average score
	function get_average_score($id){
		$this->db->select('(SUM(total) / COUNT(evaluation_id)) as average');
		$this->db->from('innovation_evaluation');
		$this->db->where('innovation_id', $id);
		$this->db->group_by('innovation_id');
		$row = $this->db->get()->row_array();
		if(count($row) > 0){
			return round($row['average'], 2);
		}else{
			return '-';
		}
	}

	//get approval innovation and innovator
	function get_approval_innovation_innovator($where = NULL){
		$this->db->select('innovator_innovation.innovator_id, innovator_innovation.innovation_id');
		$this->db->from('innovator_innovation');
		$this->db->join('innovator','innovator.innovator_id=innovator_innovation.innovator_id');
		$this->db->join('innovation','innovation.innovation_id = innovator_innovation.innovation_id');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get()->result_array();
	}

	function get_for_mention_list($where = NULL){
		$this->db->select("innovation_id as id,".$this->innovation_name." as name, 'user_id' as type", false);
		$this->db->from('innovation');
		if($where != NULL){
			$this->db->where($where);
		}

		return $this->db->get();
	}

	function get_innovation_pict_list($limit,$offset){
		$this->db->select('*');
		$this->db->from('innovation_picture');
		$this->db->limit($limit);
		$this->db->offset($offset);
		return $this->db->get();
	}
}