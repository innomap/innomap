<?php
class Innovation_executive_summary extends CI_Model {
	function __construct() {
		parent::__construct();
	}
	
	function get($where = NULL){
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get('innovation_executive_summary');
	}
	
	function add($data){
		$this->db->insert('innovation_executive_summary',$data);
		return $this->db->insert_id();
	}
	
	function edit($id,$data){
		$this->db->where('summary_id',$id);
		return $this->db->update('innovation_executive_summary',$data);
	}
	
	function delete($id){
		$this->db->where('summary_id',$id);
		return $this->db->delete('innovation_executive_summary');
	}

}