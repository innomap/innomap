<?php
class Innovation_note extends CI_Model {
	function __construct() {
		parent::__construct();
	}
	
	function get($where = NULL){
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get('innovation_note');
	}

	function get_list($where = NULL){
		$this->db->select('innovation_note.*, user.username as name');
		$this->db->from('innovation_note');
		$this->db->join('user','user.user_id = innovation_note.user_id');
		if($where != NULL){
			$this->db->where($where);
		}
		$this->db->order_by('innovation_note_id','DESC');

		return $this->db->get();
	}
	
	function add($data){
		$this->db->insert('innovation_note',$data);
		return $this->db->insert_id();
	}
	
	function edit($id,$data){
		$this->db->where('innovation_note_id',$id);
		return $this->db->update('innovation_note',$data);
	}
	
	function delete($id){
		$this->db->where('innovation_note_id',$id);
		return $this->db->delete('innovation_note');
	}

}