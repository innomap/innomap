<?php
class Innovation_portfolio_attachment extends CI_Model {
	function __construct() {
		parent::__construct();
	}
	
	function get($where = NULL){
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get('innovation_portfolio_attachment');
	}
	
	function add($data){
		$this->db->insert('innovation_portfolio_attachment',$data);
		return $this->db->insert_id();
	}
	
	function delete($id){
		$this->db->where('id',$id);
		return $this->db->delete('innovation_portfolio_attachment');
	}

}