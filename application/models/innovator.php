<?php
class Innovator extends CI_Model {
	function __construct() {
		parent::__construct();
		
		if($this->session->userdata('language') == LANGUAGE_MELAYU){
			$this->innovation_name = "name_in_melayu";
			$this->innovation_description = "description_in_melayu";
		}else{
			$this->innovation_name = "name";
			$this->innovation_description = "description";
		}
	}
	
	function get($where = null){
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get('innovator');
	}
	
	function add($data) {
		$this->db->insert('innovator', $data);
		return $this->db->insert_id();
	}
	
	function edit($id,$data) {
		$this->add_innovator_update_history($id);

		$this->db->where('innovator_id',$id);
		return $this->db->update('innovator', $data);
	}
	
	function delete($id) {
		$del_inovation = $this->db->query("DELETE innovation FROM innovation LEFT JOIN innovator_innovation ON innovation.innovation_id = innovator_innovation.innovation_id WHERE innovator_innovation.innovator_id = ".$id);
		if($del_inovation){
			$this->db->where('innovator_id',$id);
			return $this->db->delete('innovator');
		}
	}
	
	/*innovator innovation*/
	function get_innovator_innovation($where){
		$this->db->select('i.*, i.'.$this->innovation_name.' as name, i.'.$this->innovation_description.' as description');
		$this->db->from('innovator_innovation as ii');
		$this->db->join('innovation as i','i.innovation_id=ii.innovation_id');
		$this->db->where($where);
		$innovations = $this->db->get()->result_array();
		
		//add score
        $this->load->model('innovation');
		for($i = 0; $i < count($innovations); $i++){
			$innovations[$i]['score_average'] = $this->innovation->get_average_score($innovations[$i]['innovation_id']);
		}
		return $innovations;
	}
	
	function add_innovator_innovation($data){
		$this->db->insert('innovator_innovation', $data);
	}
	
	/*innovator team expert*/
	function get_innovator_team_expert($where){
		$this->db->where($where);
		return $this->db->get('innovator_team_expert');
	}
	
	function add_innovator_team_expert($data){
		$this->db->insert('innovator_team_expert', $data);
	}
	
	function edit_innovator_team_expert($id,$data) {
		$this->db->where('team_expert_id',$id);
		return $this->db->update('innovator_team_expert', $data);
	}
	
	function delete_innovator_team_expert($where){
		$this->db->where($where);
		return $this->db->delete('innovator_team_expert');
	}

	function add_innovator_update_history($innovator_id){
		$data = array('user_id' => $this->user_sess['user_id'], 'innovator_id' => $innovator_id,'updated_at' => date('Y-m-d H:i:s'));
		$this->db->insert('innovator_update_history', $data);
	}
}
