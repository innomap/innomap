<?php
class Locator extends CI_Model {
	function __construct() {
		parent::__construct();

		if($this->session->userdata('language') == LANGUAGE_MELAYU){
			$this->innovation_name = "name_in_melayu";
			$this->innovation_description = "description_in_melayu";
		}else{
			$this->innovation_name = "name";
			$this->innovation_description = "description";
		}
	}
	
	function search($state = NULL, $types, $keyword = NULL, $year = NULL){
		$results = NULL;
		if(in_array(LOCATOR_INNOVATION_TYPE,$types)){
			$this->db->select('innovation.innovation_id,innovation.'.$this->innovation_name.' as innovation,innovator.state_id as state,innovation.'.$this->innovation_description.' as product_description,innovator.address,innovation.created_date as innovation_date,state.state_id,state.name as state_name,category.name as category_name, innovator.name, innovator.geo_location');
			$this->db->from('innovation');
			$this->db->join('innovation_category as inno_cat','inno_cat.innovation_id=innovation.innovation_id','LEFT');
			$this->db->join('category','inno_cat.category_id = category.category_id');
			$this->db->join('innovator_innovation as inno_vator','inno_vator.innovation_id=innovation.innovation_id','LEFT');
			$this->db->join('innovator','innovator.innovator_id = inno_vator.innovator_id','LEFT');
			$this->db->join('state','state.state_id=innovator.state_id');
			
			if($state != NULL){
				$this->db->where('state.state_id',$state);
			}
			if($year != NULL){
				$this->db->where('YEAR(innovation.created_date)',$year);
			}
			if($keyword != NULL){
				$this->db->where('(innovation.name LIKE "%'.$keyword.'%" OR innovation.description LIKE "%'.$keyword.'%" OR category.name LIKE "%'.$keyword.'%" OR innovator.name LIKE "%'.$keyword.'%")');
			}
			
			$this->db->where('innovation.status',INNO_APPROVED);
			$this->db->group_by('innovation.innovation_id');
			$results = $this->db->get()->result_array();
			for($i=0;$i<count($results);$i++){
				$innovator = $this->get_innovation_innovator_new(array('innovation_id' => $results[$i]['innovation_id']))->result_array();
				$category = $this->get_innovation_category_new(array('innovation_id' => $results[$i]['innovation_id']))->result_array();
				$picture = $this->get_picture_new(array('innovation_id' => $results[$i]['innovation_id']))->result_array();
				$results[$i]['innovator'][] = $innovator;
				$results[$i]['category'][] = $category;
				$results[$i]['pictures'][] = $picture;
			}
		}
		if(in_array(LOCATOR_UNVERSITY_TYPE,$types)){
			$this->db->select('university.*,state.name as state_name,category.'.$this->innovation_name.' as category');
			$this->db->from('university');
			$this->db->join('state','state.state_id=university.state','LEFT');
			$this->db->join('category','category.category_id=university.specialty','LEFT');
			if($state != NULL){
				$this->db->where('state.state_id',$state);
			}
			if($keyword != NULL){
				$this->db->where('(university.university_name LIKE "%'.$keyword.'%" OR category.name LIKE "%'.$keyword.'%")');
			}
			$universities = $this->db->get()->result_array();
			foreach($universities as $university){
				$results[] = $university;
			}
		}
		if(in_array(LOCATOR_EXPERT_TYPE,$types)){
			$this->db->select('expert.*, state.name as state_name, category.'.$this->innovation_name.' as category');
			$this->db->from('expert');
			$this->db->join('state','state.state_id=expert.state_id','LEFT');
			$this->db->join('category','category.category_id = expert.expertise','LEFT');
			if($state != NULL){
				$this->db->where('state.state_id',$state);
			}
			if($keyword != NULL){
			//	$this->db->where('(expert.name LIKE "%'.$keyword.'%" OR expert.expertise LIKE "%'.$keyword.'%" OR expert.experience LIKE "%'.$keyword.'%")');
				$this->db->where('expert.name LIKE "%'.$keyword.'%"');
			}
			$experts = $this->db->get()->result_array();
			foreach($experts as $expert){
				$results[] = $expert;
			}
		}
		if(in_array(LOCATOR_PROJECT_TYPE,$types)){
			$this->db->select('*');
			$this->db->from('project');
			if($state != NULL){
				$this->db->where('project.state_id',$state);
			}
			if($keyword != NULL){
				$this->db->where('(project.title LIKE "%'.$keyword.'%" OR project.ref_no LIKE "%'.$keyword.'%" OR project.beneficiary_community LIKE "%'.$keyword.'%" OR project.lead_agency LIKE "%'.$keyword.'%" OR project.collaborator LIKE "%'.$keyword.'%" OR project.category LIKE "%'.$keyword.'%" OR project.address LIKE "%'.$keyword.'%")');
			}
			$projects = $this->db->get()->result_array();
			foreach($projects as $project){
				$results[] = $project;
			}
		}
		return $results;
	}
	
	function get_innovation_locator(){
		$this->db->select('innovation.*,state.state_name as state');
		$this->db->from('innovation');
		$this->db->join('state','state.state_id=innovation.state','LEFT');
		$this->db->where('innovation.approval_status',APPROVED);
		return $this->db->get();
	}
	
	function get_university_locator($where = NULL){
		$this->db->select('university.*,state.name as state_name,category.'.$this->innovation_name.' as category_name');
		$this->db->from('university');
		$this->db->join('state','state.state_id=university.state','LEFT');
		$this->db->join('category','category.category_id=university.specialty','LEFT');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}
	
	function get_expert_locator(){
		$this->db->select('expert.*,state.name as state_name, category.'.$this->innovation_name.' as category_name');
		$this->db->from('expert');
		$this->db->join('state','state.state_id=expert.state_id','LEFT');
		$this->db->join('category','category.category_id=expert.expertise','LEFT');
		return $this->db->get();
	}

	function get_innovation_category_locator($where = NULL){
		$this->db->select('inno_cat.*,category.'.$this->innovation_name.' as category_name, category.pin_url,innovation.'.$this->innovation_name.' as innovation,innovation.'.$this->innovation_description.' as innovation_description,innovator.innovator_id, innovator.address,innovator.name as innovator_name, state.name as state_name, innovator.geo_location, innovator.picture');
		$this->db->from('innovation_category as inno_cat');
		$this->db->join('category','category.category_id = inno_cat.category_id');
		$this->db->join('innovation','inno_cat.innovation_id = innovation.innovation_id');
		$this->db->join('innovator_innovation','innovator_innovation.innovation_id = innovation.innovation_id');
		$this->db->join('innovator','innovator.innovator_id = innovator_innovation.innovator_id');
		$this->db->join('state','state.state_id=innovator.state_id');
		
		if($where != NULL){
			$this->db->where($where);
		}
		
		$results = $this->db->get()->result_array();
			for($i=0;$i<count($results);$i++){
				$innovation = $this->get_innovator_innovation(array('innovator_innovation.innovator_id' => $results[$i]['innovator_id']))->result_array();
				$picture = $this->get_picture_new(array('innovation_id' => $results[$i]['innovation_id']))->result_array();
				$innovations = $this->get_picture_new(array('innovation_id' => $results[$i]['innovation_id']))->result_array();
				$results[$i]['innovation_list'][] = $innovation;
				$results[$i]['pictures'][] = $picture;
			}
		return $results;
	}
	
	function get_category(){
		$this->db->select('category_id,'.$this->innovation_name.' as name, pin_url');
		$this->db->from('category');
		return $this->db->get();
	}
	
	function up_innovation_geo_location($innovation_id,$data){
		$this->db->where('innovation_id',$innovation_id);
		return $this->db->update('innovation',$data);
	}
	
	function get_state($where = NULL){
		$this->db->select('*');
		$this->db->from('state');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}
	
	function get_innovation_innovator_new($where = NULL){
		$this->db->select('innovator_innovation.*,innovator.name');
		$this->db->from('innovator_innovation');
		$this->db->join('innovator','innovator_innovation.innovator_id = innovator.innovator_id');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}
	
	function get_innovation_category_new($where = NULL){
		$this->db->select('innovation_category.*,category.'.$this->innovation_name.' as category_name');
		$this->db->from('innovation_category');
		$this->db->join('category','innovation_category.category_id = category.category_id');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}

	function get_picture_new($where = NULL){
		$this->db->select('*');
		$this->db->from('innovation_picture');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}

	function get_map_election($where = NULL){
		$this->db->select('*');
		$this->db->from('map_election');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}

	function get_innovator_innovation($where = NULL){
		$this->db->select('innovation.innovation_id, innovation.name');
		$this->db->from('innovator_innovation');
		$this->db->join('innovation','innovator_innovation.innovation_id = innovation.innovation_id');
		$this->db->where('innovation.status',INNO_APPROVED);
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}
}
