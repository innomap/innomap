<?php
class Management_log extends CI_Model {
	function __construct() {
		parent::__construct();
	}
	
	function get($where = NULL){
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get('management_log');
	}
	
	function add($data){
		$this->db->insert('management_log',$data);
		return $this->db->insert_id();
	}
	
	function edit($id,$data){
		$this->db->where('management_log_id',$id);
		return $this->db->update('management_log',$data);
	}
	
	function delete($id){
		$this->db->where('management_log_id',$id);
		return $this->db->delete('management_log');
	}

	function get_list($where = NULL){
		$this->db->select('management_log.*, innovation.name, innovation.name_in_melayu');
		$this->db->from('management_log');
	//	$this->db->join('user','user.user_id = management_log.user_id');
		$this->db->join('innovation','innovation.innovation_id = management_log.innovation_id');
		if($where != NULL){
			$this->db->where($where);
		}

		return $this->db->get();
	}

	function get_list_innovation_by_user($where = NULL){
		$this->db->select('pa.*, pai.*, innovation.innovation_id, innovation.name, innovation.name_in_melayu');
		$this->db->from('portfolio_assignment as pa');
		$this->db->join('portfolio_assignment_innovation as pai','pai.portfolio_id = pa.portfolio_id');
		$this->db->join('innovation', 'innovation.innovation_id = pai.innovation_id');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}
}