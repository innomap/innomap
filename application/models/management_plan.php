<?php
class Management_plan extends CI_Model {
	function __construct() {
		parent::__construct();

		if($this->session->userdata('language') == LANGUAGE_MELAYU){
			$this->innovation_name = "name_in_melayu";
			$this->innovation_description = "description_in_melayu";
		}else{
			$this->innovation_name = "name";
			$this->innovation_description = "description";
		}
	}
	
	function get($where = NULL){
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get('management_plan');
	}

	function get_list($where = NULL){
		$this->db->select('management_plan.*,innovation.'.$this->innovation_name.' as name, innovation.'.$this->innovation_description.' as description');
		$this->db->from('management_plan');
		$this->db->join('innovation','management_plan.innovation_id = innovation.innovation_id');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}
	
	function add($data){
		$this->db->insert('management_plan',$data);
		return $this->db->insert_id();
	}
	
	function edit($id,$data){
		$this->db->where('management_plan_id',$id);
		return $this->db->update('management_plan',$data);
	}
	
	function delete($id){
		$this->db->where('management_plan_id',$id);
		return $this->db->delete('management_plan');
	}

	function get_challenge($where = NULL){
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get('management_plan_challenge');
	}
	
	function add_challenge($data){
		$this->db->insert('management_plan_challenge',$data);
		return $this->db->insert_id();
	}
	
	function edit_challenge($id,$data){
		$this->db->where('challenge_id',$id);
		return $this->db->update('management_plan_challenge',$data);
	}
	
	function delete_challenge($id){
		$this->db->where('challenge_id',$id);
		return $this->db->delete('management_plan_challenge');
	}

	function get_target($where = NULL){
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get('management_plan_target');
	}
	
	function add_target($data){
		$this->db->insert('management_plan_target',$data);
		return $this->db->insert_id();
	}
	
	function edit_target($id,$data){
		$this->db->where('target_id',$id);
		return $this->db->update('management_plan_target',$data);
	}
	
	function delete_target($id){
		$this->db->where('target_id',$id);
		return $this->db->delete('management_plan_target');
	}

	function get_note($where = NULL){
		$this->db->select('management_plan_note.*, user.username');
		$this->db->from('management_plan_note');
		$this->db->join('user','user.user_id = management_plan_note.user_id');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}
	
	function add_note($data){
		$this->db->insert('management_plan_note',$data);
		return $this->db->insert_id();
	}

	function get_list_innovation_by_user($where = NULL){
		$this->db->select('pa.*, pai.*, innovation.innovation_id, innovation.name, innovation.name_in_melayu, management_plan.management_plan_id');
		$this->db->from('portfolio_assignment as pa');
		$this->db->join('portfolio_assignment_innovation as pai','pai.portfolio_id = pa.portfolio_id');
		$this->db->join('innovation', 'innovation.innovation_id = pai.innovation_id');
		$this->db->join('management_plan','management_plan.innovation_id = pai.innovation_id','LEFT');
		if($where != NULL){
			$this->db->where($where);
		}
		$this->db->where('management_plan_id IS NULL');
		return $this->db->get();
	}

}