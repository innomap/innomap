<?php
class Marketplace_social_media extends CI_Model {
	function __construct() {
		parent::__construct();
	}
	
	function get($where = null){
		$this->db->select('*');
		$this->db->from('marketplace_social_media');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}

	function add($data){
		$this->db->insert('marketplace_social_media',$data);
		return $this->db->insert_id();
	}
	
	function edit($id,$data){
		$this->db->where('id_social_media',$id);
		return $this->db->update('marketplace_social_media',$data);
	}
	
	function delete($id){
		$this->db->where('id_social_media',$id);
		return $this->db->delete('marketplace_social_media');
	}
}
