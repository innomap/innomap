<?php
class Menu extends CI_Model {
	function __construct() {
		parent::__construct();
	}
	
	/*MENU*/
	function get($where = NULL){
		if($where != NULL){
			$this->db->where($where);
		}
		$this->db->order_by('order');
		return $this->db->get('menu');
	}
	
	function add($data){
		$this->db->insert('menu',$data);
		return $this->db->insert_id();
	}
	
	function edit($id,$data){
		$this->db->where('menu_id',$id);
		return $this->db->update('menu',$data);
	}
	
	function delete($id){
		$this->db->where('menu_id',$id);
		return $this->db->delete('menu');
	}
	
	function get_user_menu($where){
		$this->db->select('m.*,mn.parent_id as menu_parent');
		$this->db->from('role_module as rm');
		$this->db->join('module as m','m.module_id=rm.module_id');
		$this->db->join('menu as mn','mn.menu_id=m.menu_id');
		$this->db->where($where);
		$this->db->where('mn.status',1);
		$this->db->group_by('m.menu_id');
		$this->db->order_by('mn.order');
		return $this->db->get()->result_array();
	}
	
	function get_front_menu($role_id){
		$modules = $this->get_user_menu(array('rm.role_id' => $role_id));
		$menus   = array();
		foreach($modules as $module){
			$menus[] = $this->get(array('menu_id' => $module['menu_id']))->row_array();
		}
		
		//get menu parents
		$parents = array();
		foreach($menus as $menu){
			if($menu['parent_id'] == NULL){
				$parents[] = $menu;
			}else{
				$temp = $this->get(array('menu_id' => $menu['parent_id']))->row_array();
				if(!in_array($temp,$parents)){
					$parents[] = $temp;
				}
			}
		}
		
		//assign menu parent childs
		for($i=0;$i<count($parents);$i++){
			$parents[$i]['childs'] = array();
			foreach($menus as $menu){
				if($menu['parent_id'] == $parents[$i]['menu_id']){
					$parents[$i]['childs'][] = $menu;
				}
			}
		}
		
		return $parents;
	}
	
	/*MODULE*/
	function get_module($where){
		$this->db->select('*, false as is_accessable',false);
		$this->db->where($where);
		return $this->db->get('module');
	}
	
	function add_module($data){
		$this->db->insert('module',$data);
		return $this->db->insert_id();
	}
	
	function edit_module($id,$data){
		$this->db->where('module_id',$id);
		return $this->db->update('module',$data);
	}
	
	function delete_module($id){
		$this->db->where('module_id',$id);
		return $this->db->delete('module');
	}

}