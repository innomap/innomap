<?php

class Msi_session extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	function get() {
		return $this->session->userdata(MSI_SESSION);
	}
	
	function set($data) {
		$this->session->set_userdata(MSI_SESSION, $data);
		$this->session->set_userdata('language','english');
	}
	
	function create($username, $password) {
		$this->load->model('user');
		if($data = $this->user->auth_with_role($username, $password)){
			unset($data['password']);
			$this->set($data);
			return true;
		}else{
			return false;
		}
	}
	
	function clear() {
		$this->session->unset_userdata(MSI_SESSION);
		$this->session->unset_userdata(USER_SESSION);
	}
}