<?php
class Open_cart extends CI_Model {
	function __construct() {
		parent::__construct();

		$this->oc_db= $this->load->database('oc_db', TRUE);
	}

	function get_product($where = NULL){
		$this->oc_db->select('*');
		$this->oc_db->from('oc_product');
		if($where != NULL){
			$this->oc_db->where($where);
		}
		return $this->oc_db->get();
	}

	function add_product($data){
		$this->oc_db->insert('oc_product',$data);
		return $this->oc_db->insert_id();
	}

	function update_product($id, $data){
		$this->oc_db->where('product_id',$id);
		return $this->oc_db->update('oc_product',$data);
	}

	function add_product_desc($data){
		$this->oc_db->insert('oc_product_description',$data);
		return $this->oc_db->insert_id();
	}

	function update_product_desc($id, $data){
		$this->oc_db->where('product_id',$id);
		return $this->oc_db->update('oc_product_desc',$data);
	}

	function get_category($where = NULL){
		$this->oc_db->select('*');
		$this->oc_db->from('oc_category');
		if($where != NULL){
			$this->oc_db->where($where);
		}
		return $this->oc_db->get();
	}

	function add_product_to_category($data){
		$this->oc_db->insert('oc_product_to_category',$data);
		return $this->oc_db->insert_id();
	}

	function delete_product_to_category($where){
		$this->oc_db->where($where);
		return $this->oc_db->delete('oc_product_to_category');
	}

	function add_product_to_store($data){
		$this->oc_db->insert('oc_product_to_store',$data);
		return $this->oc_db->insert_id();
	}

	function add_product_image($data){
		$this->oc_db->insert('oc_product_image',$data);
		return $this->oc_db->insert_id();
	}

	function delete_product_image($where){
		$this->oc_db->where($where);
		return $this->oc_db->delete('oc_product_image');
	}
}
