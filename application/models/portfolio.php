<?php
class Portfolio extends CI_Model {
	function __construct() {
		parent::__construct();
		
		if($this->session->userdata('language') == LANGUAGE_MELAYU){
			$this->innovation_name = "name_in_melayu";
			$this->innovation_description = "description_in_melayu";
		}else{
			$this->innovation_name = "name";
			$this->innovation_description = "description";
		}
	}

	function get($where = null, $limit = NULL, $offset = NULL){
		$this->db->select('p.*, (SELECT COUNT(portfolio_id) FROM portfolio_assignment_innovation WHERE p.portfolio_id = portfolio_assignment_innovation.portfolio_id) as has_innovation',false);
		$this->db->from('portfolio_assignment as p');
		$this->db->having('has_innovation > 0');
		if($where != NULL){
			$this->db->where($where);
		}
		if($limit != NULL){
			$this->db->limit($limit);
		}
		if($offset != NULL){
			$this->db->offset($offset);
		}
		return $this->db->get();
	}
	
	function get_with_manager($where = NULL, $limit = NULL, $offset = NULL){
		$this->db->select('p.*, u.username as manager_name, (SELECT COUNT(portfolio_id) FROM portfolio_assignment_innovation WHERE p.portfolio_id = portfolio_assignment_innovation.portfolio_id) as has_innovation',false);
		$this->db->from('portfolio_assignment as p');
		$this->db->join('user as u','u.user_id=p.manager_id');
		$this->db->having('has_innovation > 0');
		if($where != NULL){
			$this->db->where($where);
		}
		if($limit != NULL){
			$this->db->limit($limit);
		}
		if($offset != NULL){
			$this->db->offset($offset);
		}
		return $this->db->get();
	}
	
	function add($data) {
		$this->db->insert('portfolio_assignment', $data);
		return $this->db->insert_id();
	}
	
	function edit($id,$data) {
		$this->db->where('portfolio_id',$id);
		return $this->db->update('portfolio_assignment', $data);
	}
	
	function delete($id) {
		$this->db->where('portfolio_id',$id);
		return $this->db->delete('portfolio_assignment');
	}
	
	/*portfolio innovation*/
	function get_portfolio_innovation($where){
		$this->db->select('i.innovation_id,i.name,i.name_in_melayu, pai.portfolio_id');
		$this->db->from('portfolio_assignment_innovation as pai');
		$this->db->join('innovation as i','i.innovation_id=pai.innovation_id');
		$this->db->where($where);
		return $this->db->get();
	}
	
	function add_portfolio_innovation($data){
		$this->db->insert('portfolio_assignment_innovation', $data);
	}
	
	/*available assignment data*/
	function get_available_manager(){
		$this->db->select('*');
		$this->db->from('user as u');
		$this->db->join('user_role as urole','urole.user_id=u.user_id');
		$this->db->join('portfolio_assignment as pa','pa.manager_id=u.user_id','LEFT');
		$this->db->where('urole.role_id',ROLE_MANAGER);
		//$this->db->where('pa.portfolio_id',NULL);
		//$this->db->or_where('pa.status',PORTFOLIO_TASK_DONE);
		return $this->db->get()->result_array();
	}

	function get_available_manager_hip6(){
		$this->db->select('*');
		$this->db->from('user as u');
		$this->db->join('user_role as urole','urole.user_id=u.user_id');
		$this->db->join('portfolio_assignment as pa','pa.manager_id=u.user_id','LEFT');
		$this->db->where('(urole.role_id = '.ROLE_HIP6_ACCOUNT_MANAGER.' OR urole.role_id = '.ROLE_BUSINESS_ANALYST.')');
		//$this->db->where('pa.portfolio_id',NULL);
		//$this->db->or_where('pa.status',PORTFOLIO_TASK_DONE);
		$this->db->group_by('u.user_id');
		return $this->db->get()->result_array();
	}
	
	function get_available_innovation($where = NULL, $is_edit = 0){
		$this->db->select('i.innovation_id, i.'.$this->innovation_name.' as name, i.'.$this->innovation_description.' as description, pai.portfolio_id');
		$this->db->from('innovation as i');
		$this->db->join('innovation_evaluation as ie','ie.innovation_id=i.innovation_id');
		$this->db->join('portfolio_assignment_innovation as pai','pai.innovation_id=i.innovation_id','LEFT');
		$this->db->where('ie.status',EVALUATION_DONE);
		//$this->db->where('ie.total >= ',MINIMUM_SCORE);
		if($is_edit == 0){
			$this->db->where('pai.portfolio_id',NULL);
		}
		if($where != NULL){
			$this->db->where($where);
		}
		$this->db->group_by('i.innovation_id');
		return $this->db->get()->result_array();
	}
	function get_available_innovation_hip6($where = NULL, $is_edit = 0){
		$this->db->select('i.innovation_id, i.'.$this->innovation_name.' as name, i.'.$this->innovation_description.' as description, pai.portfolio_id');
		$this->db->from('innovation as i');
		$this->db->join('portfolio_assignment_innovation as pai','pai.innovation_id=i.innovation_id','LEFT');
		$this->db->where('i.data_source', DATA_SOURCE_HIP6);
		$this->db->where('i.hip6_status', INNO_APPROVED);
		if($is_edit == 0){
			$this->db->where('pai.portfolio_id',NULL);
		}
		if($where != NULL){
			$this->db->where($where);
		}
		$this->db->group_by('i.innovation_id');
		return $this->db->get()->result_array();
	}
	
	/*innovation portfolio*/
	function get_innovation_portfolio($where){
		return $this->db->get_where('innovation_portfolio',$where);
	}
	
	function add_innovation_portfolio($data){
		$this->db->insert('innovation_portfolio', $data);
		return $this->db->insert_id();
	}
	
	function edit_innovation_portfolio($id,$data) {
		$this->db->where('innovation_portfolio_id',$id);
		return $this->db->update('innovation_portfolio', $data);
	}
	
	function delete_innovation_portfolio($where){
		$this->db->where($where);
		return $this->db->delete('innovation_portfolio');
	}
	
	/*others*/
	function check_portfolio_done($portfolio_id){
		$port_innovations = $this->get_portfolio_innovation(array('pai.portfolio_id'=>$portfolio_id))->result_array();
		$done_num   	  = count(unserialize(PORTFOLIO_ITEMS)) * count($port_innovations);
		$current_num 	  = 0;
		foreach($port_innovations as $inno){
			$current_num += $this->get_innovation_portfolio(array('innovation_id'=>$inno['innovation_id']))->num_rows();
		}
		return ($current_num == $done_num);
	}
	
	/*innovation portfolio asset*/
	function get_asset($where){
		return $this->db->get_where('innovation_asset',$where);
	}
	
	function add_asset($data){
		$this->db->insert('innovation_asset', $data);
		return $this->db->insert_id();
	}
	
	/*portfolio comment*/
	function get_comment($portfolio_id){
		$this->db->select('*');
		$this->db->from('portfolio_comment');
		$this->db->where('portfolio_id',$portfolio_id);
		$this->db->order_by('timestamp','desc');
		return $this->db->get()->result_array();
	}
	
	function add_comment($data){
		return $this->db->insert('portfolio_comment', $data);
	}
	
	/*portfolio note*/
	function get_note($where){
		$this->db->select('*');
		$this->db->from('portfolio_note');
		$this->db->where($where);
		$this->db->order_by('creation_date');
		return $this->db->get();
	}
	
	function get_note_date($portfolio_id){
		$dates 		= "";
		$notes 		= $this->get_note(array('portfolio_id' => $portfolio_id))->result_array();
		$num		= count($notes);
		$notes[-1]  = array('creation_date'=>NULL);
		for($i=0;$i<$num;$i++){
			if($notes[$i]['creation_date'] != $notes[$i-1]['creation_date']){
				$dates .= date('n/j/Y',strtotime($notes[$i]['creation_date'])).',';
			}
		}
		return rtrim($dates,",");
	}
	
	function add_note($data){	
		return $this->db->insert('portfolio_note', $data);
	}
	
	function edit_note($id,$data) {
		$this->db->where('portfolio_note_id', $id);
		return $this->db->update('portfolio_note', $data);
	}
	
	function delete_note($id){
		$this->db->where('portfolio_note_id',$id);
		return $this->db->delete('portfolio_note');
	}
	
	function get_innovation_in_portfolio($where = NULL, $limit = NULL, $offset = NULL){
		$this->db->select('pa.*, pai.*, innovation.innovation_id, innovation.name, innovation.name_in_melayu');
		$this->db->from('portfolio_assignment_innovation as pai');
		$this->db->join('innovation','innovation.innovation_id = pai.innovation_id');
		$this->db->join('portfolio_assignment as pa','pai.portfolio_id = pa.portfolio_id');
		if($where != NULL){
			$this->db->where($where);
		}
		if($limit != NULL){
			$this->db->limit($limit);
		}
		if($offset != NULL){
			$this->db->offset($offset);
		}
		
		return $this->db->get();
	}

	/* for manager notification */
	function get_task_by_manager($manager_id){
		$this->db->select('t.*, a.title as portfolio_title, c.name_in_melayu as criteria_name');
		$this->db->from('portfolio_scope_task as t');
		$this->db->join('portfolio_scope as s', 's.scope_id = t.scope_id');
		$this->db->join('portfolio_assignment as a', 'a.portfolio_id = s.portfolio_id');
		$this->db->join('portfolio_criteria as c', 'c.criteria_id = s.criteria_id');
		$this->db->where('a.manager_id', $manager_id);
		$this->db->where('( ( t.start_date <= CURDATE() AND t.end_date >= CURDATE() ) OR
							( t.start_date <= (CURDATE() + INTERVAL 1 DAY) AND t.end_date >= (CURDATE() + INTERVAL 1 DAY) ) )');
		return $this->db->get()->result_array();
	}

	function get_note_by_manager($manager_id){
		$this->db->select('n.*, a.title as portfolio_title,');
		$this->db->from('portfolio_note as n');
		$this->db->join('portfolio_assignment as a', 'a.portfolio_id = n.portfolio_id');
		$this->db->where('a.manager_id', $manager_id);
		$this->db->where('( n.creation_date >= CURDATE() AND n.creation_date <= CURDATE() + INTERVAL 1 DAY)');
		return $this->db->get()->result_array();
	}
}