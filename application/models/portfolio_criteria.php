<?php
class Portfolio_criteria extends CI_Model {
	function __construct() {
		parent::__construct();

		if($this->session->userdata('language') == LANGUAGE_MELAYU){
			$this->criteria_name = "name_in_melayu";
			$this->criteria_description = "description_in_melayu";
		}else{
			$this->criteria_name = "name";
			$this->criteria_description = "description";
		}
	}
	
	function get($where = NULL,$order_by = NULL, $group_by = NULL){
		$this->db->select('*');
		$this->db->from('portfolio_criteria');
		if($where != NULL){
			$this->db->where($where);
		}
		if($order_by != NULL){
			$this->db->order_by($order_by,'ASC');	
		}
		if($group_by != NULL){
			$this->db->group_by($group_by);	
		}
		return $this->db->get();
	}

	function add($data){
		$this->db->insert('portfolio_criteria',$data);
		return $this->db->insert_id();
	}

	function edit($id,$data){
		$this->db->where('criteria_id',$id);
		return $this->db->update('portfolio_criteria',$data);
	}

	function get_by_innovation($innovation_id,$having = NULL,$where = NULL){
		$this->db->select('portfolio_criteria.*,(SELECT COUNT(innovation_id) FROM innovation_portfolio WHERE portfolio_criteria.criteria_id = innovation_portfolio.item AND innovation_portfolio.innovation_id = '.$innovation_id.') as criteria_done',false);
		$this->db->from('portfolio_criteria');
		if($having != NULL){
			$this->db->having($having);
		}
		if($where != NULL){
			$this->db->where($where);
		}

		return $this->db->get();
	}

	function get_by_portfolio($portfolio_id,$having = NULL,$where = NULL){
		$this->db->select('portfolio_criteria.*,(SELECT COUNT(innovation_id) FROM innovation_portfolio WHERE portfolio_criteria.criteria_id = innovation_portfolio.item AND innovation_portfolio.portfolio_id = '.$portfolio_id.') as criteria_done',false);
		$this->db->from('portfolio_criteria');
		if($having != NULL){
			$this->db->having($having);
		}
		if($where != NULL){
			$this->db->where($where);
		}

		return $this->db->get();
	}
}
