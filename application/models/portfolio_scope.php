<?php
class Portfolio_scope extends CI_Model {
	function __construct() {
		parent::__construct();

		$this->table_name = "portfolio_scope";
		$this->primary_key = "scope_id";
	}
	
	function get($where = NULL){
		$this->db->select('*');
		$this->db->from($this->table_name);
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}

	function get_with_criteria($portfolio_id, $innovation_id, $scope_id = NULL){
		$this->db->select('*');
		$this->db->from($this->table_name . " as ps");
		$this->db->join('portfolio_criteria as pc','ps.criteria_id = pc.criteria_id');
		$this->db->where('ps.portfolio_id', $portfolio_id);
		if($scope_id != NULL){
			$this->db->where('ps.' . $this->primary_key, $scope_id);
		}
		$criterias = $this->db->get()->result_array();

		for ($i=0; $i < count($criterias) ; $i++) { 
			$criterias[$i]['tasks'] = $this->get_task(array('scope_id' => $criterias[$i]['scope_id']), $innovation_id);
		}
		return $criterias;
	}

	function get_with_task($where = NULL){
		$this->db->select('*');
		$this->db->from($this->table_name . " as ps");
		$this->db->join('portfolio_scope_task as pst','pst.scope_id = ps.scope_id');
		$this->db->join('portfolio_criteria as pc','pc.criteria_id = ps.criteria_id');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get()->result_array();
	}

	function add($data){
		$this->db->insert($this->table_name, $data);
		return $this->db->insert_id();
	}

	function edit($id, $data){
		$this->db->where($this->primary_key, $id);
		return $this->db->update($this->table_name, $data);
	}

	function get_task($where, $innovation_id = NULL){
		$this->db->select('*');
		$this->db->from('portfolio_scope_task');
		if($where != NULL){
			$this->db->where($where);
		}

		if($innovation_id == NULL){
			return $this->db->get();
		}else{
			$tasks = $this->db->get()->result_array();
			for ($i=0; $i < count($tasks) ; $i++) { 
				$get_asset_data = $this->get_innovation_portfolio($tasks[$i]['task_id'], $innovation_id);
				if(count($get_asset_data) > 0){
					$tasks[$i]['progress'] = $get_asset_data['progress'];
					$tasks[$i]['note'] = $get_asset_data['note'];
					$tasks[$i]['amount_spend'] = $get_asset_data['amount_spend'];
					$tasks[$i]['innovation_portfolio_id'] = $get_asset_data['innovation_portfolio_id'];
					$tasks[$i]['is_pending_pm_approval'] = $get_asset_data['is_pending_pm_approval'];
				}else{
					$tasks[$i]['progress'] = 0;
					$tasks[$i]['note'] = "";
					$tasks[$i]['amount_spend'] = 0;
					$tasks[$i]['innovation_portfolio_id'] = 0;
					$tasks[$i]['is_pending_pm_approval'] = FALSE;
				}
			}
			return $tasks;
		}
	}

	function get_innovation_portfolio($task_id, $innovation_id){
		return $this->db->get_where('innovation_portfolio', array('task_id' => $task_id, 'innovation_id' => $innovation_id))->row_array();
	}

	function add_task($data){
		$this->db->insert('portfolio_scope_task', $data);
		return $this->db->insert_id();
	}

	function get_highlight_dates($portfolio_id){
		$tasks = $this->portfolio_scope->get_with_task('ps.portfolio_id = ' . $portfolio_id);

		$dates = array();
		$highlights = array();
		$highlight_i = 0;
		for ($i = 0; $i < count($tasks); $i++) {
			$end_date = new DateTime($tasks[$i]['end_date']);
			$period = new DatePeriod(
			    new DateTime($tasks[$i]['start_date']),
			    new DateInterval('P1D'),
			    $end_date->modify('+1 day')
			);

			foreach($period as $date) { 
				$added_date = $date->format('n/j/Y');
				if(!in_array($added_date, $dates)){ //date only have one scope
					$dates[] = $added_date; 
					$highlights[$highlight_i] = array('date' => $added_date, 'scope' => $this->get_scope_index($tasks[$i]['criteria_id']));
					$highlight_i++;
				}else{ //date exist in another scope (multiple scope)
					$key = array_search($added_date, $dates);
					$highlights[$key]['scope'] = 'many';
				}
			}
		}
		return $highlights;
	}

	function get_scope_index($criteria_id){
		$scopes = $this->db->get_where('portfolio_criteria', array('is_deleted' => 0))->result_array();
		foreach ($scopes as $key => $scope) {
			if($scope['criteria_id'] == $criteria_id){
				return $key;
			}
		}
		return -1;
	}
}
