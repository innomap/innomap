<?php
class Project extends CI_Model {
	function __construct() {
		parent::__construct();
	}
	
	function get($where = NULL,$order_by = NULL, $group_by = NULL){
		$this->db->select('*');
		$this->db->from('project');
		if($where != NULL){
			$this->db->where($where);
		}
		if($order_by != NULL){
			$this->db->order_by($order_by,'ASC');	
		}
		if($group_by != NULL){
			$this->db->group_by($group_by);	
		}
		return $this->db->get();
	}

	function add($data){
		$this->db->insert('project',$data);
		return $this->db->insert_id();
	}

	function edit($id,$data){
		$this->db->where('project_id',$id);
		return $this->db->update('project',$data);
	}

	function delete($id){
		$this->db->where('project_id',$id);
		return $this->db->delete('project');	
	}

	function get_state($where = NULL){
		$this->db->select('*');
		$this->db->from('state');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}
}
