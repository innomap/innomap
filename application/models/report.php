<?php
class Report extends CI_Model {
	function __construct() {
		parent::__construct();
		
		//select field by language option
		if($this->session->userdata('language') == LANGUAGE_MELAYU){
			$this->category_name = 'name_in_melayu';
		}else{
			$this->category_name = 'name';
		}
	}

	//GET CATEGORY
	function get_category($where = NULL){
		$this->db->select($this->category_name.' as name, category_id');
		$this->db->from('category');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}

	//GET YEARS
	function get_year($where = NULL){
		$this->db->select("DATE_FORMAT(discovered_date, '%Y') AS year", FALSE);
		$this->db->from('innovation');
		$this->db->group_by('year');
		if($where != NULL){
			$this->db->where($where);
		}
		$this->db->where('YEAR(discovered_date) >=',REPORT_YEAR_MIN);
		return $this->db->get();
	}
	
	//GET STATE
	function get_state($where = NULL){
		$this->db->select('*');
		$this->db->from('state');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}
	
	//GET INNOVATION BY CATEGORY
	function get_innovation_by_category($where){
		$this->db->select('i.innovation_id, i.name, i.created_date, i.discovered_date');
		$this->db->from('innovation as i');
		$this->db->join('innovation_category as ic','ic.innovation_id=i.innovation_id');
		$this->db->where($where);
		$this->db->where('i.status',INNO_APPROVED);
		return $this->db->get();
	}
	
	//GET INNOVATION BY STATE
	function get_innovation_by_state($where){
		$this->db->select('i.innovation_id, i.name, i.created_date,i.discovered_date, inv.state_id');
		$this->db->from('innovation as i');
		$this->db->join('innovator_innovation as ii','ii.innovation_id=i.innovation_id');
		$this->db->join('innovator as inv','inv.innovator_id=ii.innovator_id');
		$this->db->where($where);
		$this->db->where('i.status',INNO_APPROVED);
		return $this->db->get();
	}
	
	//GET INNOVATION [this is used in innovation by year report - without join]
	function get_innovation($where){
		$this->db->select('innovation_id, name, created_date, discovered_date');
		$this->db->from('innovation');
		$this->db->where($where);
		$this->db->where('is_mara_data',0);
		$this->db->where('status',INNO_APPROVED);
		return $this->db->get();
	}
	
	//GET INNOVATOR [this is used in innovator by state & gender report]
	function get_innovator($where, $is_gender_report = false){
		$this->db->select('i.innovator_id, i.name, i.email, i.kod_no, i.address, i.gender, s.name as state, i.d_home_phone_no as home_phone_number, i.d_official_phone_no as office_phone_number, i.d_mobile_phone_no as mobile_phone_number, i.d_fax_no as fax_number');
		$this->db->from('innovator as i');
		if($is_gender_report){
			$this->db->join('state as s','s.state_id=i.state_id','LEFT');
		}else{
			$this->db->join('state as s','s.state_id=i.state_id');
		}
		if($where != NULL){
			$this->db->where($where);
		}
		$this->db->where('i.status',INNO_APPROVED);
		return $this->db->get();
	}
	
	//GET INNOVATOR BY CATEGORY
	function get_innovator_by_category($where){
		$this->db->select('i.innovator_id, i.name, i.email, i.kod_no, i.address, i.gender, i.d_home_phone_no as home_phone_number, i.d_official_phone_no as office_phone_number, i.d_mobile_phone_no as mobile_phone_number, i.d_fax_no as fax_number');
		$this->db->from('innovator_innovation as ii');
		$this->db->join('innovation_category as ic','ic.innovation_id = ii.innovation_id');
		$this->db->join('category as c','ic.category_id = c.category_id');
		$this->db->join('innovator as i','i.innovator_id = ii.innovator_id');
		if($where != NULL){
			$this->db->where($where);
		}
		$this->db->where('i.status',INNO_APPROVED);
		$this->db->group_by('i.innovator_id');
		return $this->db->get();		
	}
	
	//GET EXPERT
	function get_expert($where){
		$this->db->select('e.expert_id, e.name, u.email, e.kad_no, e.address, s.name as state');
		$this->db->from('expert as e');
		$this->db->join('state as s','s.state_id=e.state_id');
		$this->db->join('user as u','u.user_id = e.expert_id');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}
	
	//GET UNIVERSITY BY STATE
	function get_university_by_state($where){
		$this->db->select('u.university_id, u.university_name as name, u.contact_no, u.address, s.name as state');
		$this->db->from('university as u');
		$this->db->join('state as s','s.state_id=u.state');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}
	
	//GET UNIVERSITY BY CATEGORY
	function get_university_by_category($where){
		$this->db->select('u.university_id, u.university_name as name, u.contact_no, u.address, c.'.$this->category_name.' as category');
		$this->db->from('university as u');
		$this->db->join('category as c','c.category_id=u.specialty');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}
	
	//GET EVALUATED INNOVATION for Innovation Evaluation Report by IDec Head	
	function get_evaluated_innovation($where = NULL, $top_score = FALSE, $group_by = 'i.innovation_id'){
		if($group_by == 'i.innovation_id'){
			$this->db->select('i.innovation_id as id, i.'. $this->category_name .' as name, inv.innovator_id, inv.name as innovator_name,
							   GROUP_CONCAT(e.expert_id) as expert_ids, GROUP_CONCAT(ie.evaluation_id) as evaluation_ids, GROUP_CONCAT(ie.total) as scores, 
							   SUM(total) as total_score, COUNT(evaluation_id) as score_num, (SUM(total) / COUNT(evaluation_id)) as average');
		}elseif($group_by == 'inv.innovator_id'){
			$this->db->select('inv.innovator_id as id, inv.name');
		}elseif($group_by == 'e.expert_id'){
			$this->db->select('e.expert_id as id, e.name');
		}
		
		$this->db->from('innovation_evaluation as ie');
		$this->db->join('innovation as i', 'i.innovation_id = ie.innovation_id');
		$this->db->join('innovator_innovation as ii', 'ii.innovation_id = i.innovation_id');
		$this->db->join('innovator as inv', 'inv.innovator_id = ii.innovator_id');
		$this->db->join('expert as e', 'e.expert_id = ie.evaluator_expert_id');
		$this->db->where('ie.status', EVALUATION_DONE);
		if($where != NULL){
			$this->db->where($where);
		}
		$this->db->group_by($group_by);
		
		if($top_score){
			$this->db->order_by('average DESC');
			$this->db->limit(100, 0); //limit 100 top score only
		}else{
			$this->db->order_by('i.'. $this->category_name);
		}
		return $this->db->get();
	}
	
	function get_evaluation_score($innovation_id, $expert_id){
		$this->db->select('*');
		$this->db->from('innovation_evaluation');
		$this->db->where('status', EVALUATION_DONE);
		$this->db->where('innovation_id', $innovation_id);
		$this->db->where('evaluator_expert_id', $expert_id);
		return $this->db->get()->row_array();
	}
	
	function get_evaluation_result($filter_by, $ids){
		$where_clause = 'i.innovation_id';
		if($filter_by == 'innovator'){
			$where_clause = 'inv.innovator_id';
		}elseif($filter_by == 'expert'){
			$where_clause = 'e.expert_id';
		}
		
		if($ids != NULL){
			$id_where = "(";
			foreach($ids as $id){
				$id_where .= $where_clause. " = ". $id .($id != end($ids) ? " OR " : ")");
			}
			return $this->get_evaluated_innovation($id_where);
		}
	}
	
	function get_evaluation_expert($ids){
		$where_clause = 'e.expert_id';
		if($ids != NULL){
			$id_where = "(";
			foreach($ids as $id){
				$id_where .= $where_clause. " = ". $id .($id != end($ids) ? " OR " : ")");
			}
			return $this->get_expert($id_where);
		}
	}

	function get_expert_task($where = NULL){
		$this->db->select('expert_task.*');
		$this->db->from('expert_task');
		$this->db->join('expert_task_expert','expert_task.expert_task_id = expert_task_expert.expert_task_id');
		$this->db->join('expert_task_innovation','expert_task.expert_task_id = expert_task_innovation.expert_task_id');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}

	function get_innovation_evaluation($where = NULL){
		$this->db->select('expert_task.title, innovation.'.$this->category_name.' as name, expert.name as expert_name, innovation_evaluation.total');
		$this->db->from('innovation_evaluation');
		$this->db->join('expert_task','expert_task.expert_task_id = innovation_evaluation.expert_task_id');
		$this->db->join('innovation','innovation.innovation_id = innovation_evaluation.innovation_id');
		$this->db->join('expert','expert.expert_id = innovation_evaluation.evaluator_expert_id');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}

	function get_portfolio_assignment($where = NULL){
		$this->db->select('*');
		$this->db->from('portfolio_assignment');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}
	
/*************** not used for generate report ******************************************************************************************/
	
	//GET INNOVATION INNOVATOR
	function get_innovation_innovator($innovation_id){
		$this->db->select('innovator.*');
		$this->db->from('innovator_innovation as ii');
		$this->db->join('innovator','innovator.innovator_id=ii.innovator_id');
		$this->db->where('ii.innovation_id',$innovation_id);
		$innovator = $this->db->get()->row_array();
		if($innovator['identity'] == INNOVATOR_IDENTITY_TEAM){
			$teams = '<b title="Group Leader">'.$innovator['name'].'</b>';
			$experts = $this->db->get_where('innovator_team_expert',array('innovator_id'=>$innovator['innovator_id']))->result_array();
			foreach($experts as $expert){
				$teams .= ", ".$expert['name'];
			}
			return $teams;
		}else{
			return $innovator['name'];
		}
	}
	
	//GET INNOVATION CATEGORY
	function get_innovation_category($innovation_id){
		$this->db->select('category.'.$this->category_name.' as name, category.category_id');
		$this->db->from('innovation_category as ic');
		$this->db->join('category','category.category_id=ic.category_id');
		$this->db->where('ic.innovation_id',$innovation_id);
		$categories = $this->db->get()->result_array();
		$cats = "";
		foreach($categories as $category){
			$cats .= $category['name'].", ";
		}
		return rtrim($cats,', ');
	}
	
/*************** EXPORT DATA TO EXCEL ******************************************************************************************/
	
	function export_innovation_by_category($where = NULL){
		$this->db->select('i.innovation_id,i.name, i.name_in_melayu, i.created_date, i.discovered_date, i.inspiration, i.inspiration_in_melayu, i.description, i.description_in_melayu, 
						   i.materials, i.materials_in_melayu, i.manufacturing_costs, i.how_to_use, i.target, i.myipo_protection, i.special_achievement, i.approval_note, i.approval_date');
		$this->db->from('innovation as i');
		$this->db->join('innovation_category as ic','ic.innovation_id=i.innovation_id');
		if($where != NULL){
			$this->db->where($where);
		}
		$this->db->where('i.status',INNO_APPROVED);
		$this->db->where('YEAR(i.discovered_date) >=',REPORT_YEAR_MIN);
		$this->db->group_by('i.innovation_id');
		return $this->db->get();
	}
	
	function export_innovation_by_state($where = NULL){
		$this->db->select('i.innovation_id,i.name, i.name_in_melayu, i.created_date, i.discovered_date, i.inspiration, i.inspiration_in_melayu, i.description, i.description_in_melayu, 
						   i.materials, i.materials_in_melayu, i.manufacturing_costs, i.how_to_use, i.target, i.myipo_protection, i.special_achievement, i.approval_note, i.approval_date');
		$this->db->from('innovation as i');
		$this->db->join('innovator_innovation as ii','ii.innovation_id=i.innovation_id');
		$this->db->join('innovator as inv','inv.innovator_id=ii.innovator_id');
		if($where != NULL){
			$this->db->where($where);
		}
		$this->db->where('i.status',INNO_APPROVED);
		$this->db->where('YEAR(i.discovered_date) >=',REPORT_YEAR_MIN);
		$this->db->group_by('i.innovation_id');
		return $this->db->get();
	}
	
	function export_innovation_by_year($where = NULL){
		$this->db->select('innovation_id, name, name_in_melayu, created_date, discovered_date, inspiration, inspiration_in_melayu, description, description_in_melayu, 
						   materials, materials_in_melayu, manufacturing_costs, how_to_use, target, myipo_protection, special_achievement, approval_note, approval_date');
		$this->db->from('innovation');
		if($where != NULL){
			$this->db->where($where);
		}
		$this->db->where('status',INNO_APPROVED);
		$this->db->where('YEAR(discovered_date) >=',REPORT_YEAR_MIN);
		return $this->db->get();
	}

	function get_expert_group($where = NULL){
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get('expert_group');
	}

	function get_expert_group_expert($where = NULL){
		$this->db->select('expert.expert_id,expert.name');
		$this->db->from('expert_group_expert');
		$this->db->join('expert','expert.expert_id = expert_group_expert.expert_id');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}	

	/*Temporary Function*/
	function get_innovator_date($where){
		$this->db->select('i.innovator_id, i.name, i.email, i.kod_no, i.address, i.gender, s.name as state, i.d_home_phone_no as home_phone_number, i.d_official_phone_no as office_phone_number, i.d_mobile_phone_no as mobile_phone_number, i.d_fax_no as fax_number, u.created_date');
		$this->db->from('innovator as i');
		$this->db->join('state as s','s.state_id=i.state_id');
		$this->db->join('user as u','u.user_id=i.innovator_id');
		if($where != NULL){
			$this->db->where($where);
		}
		$this->db->where('i.status',INNO_APPROVED);
		return $this->db->get();
	}
}
