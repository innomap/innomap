<?php
class Role extends CI_Model {
	
	function __construct() {
		parent::__construct();
	}
	
	/*ROLE*/
	function get($where = NULL){
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get('role');
	}
	
	function add($data){
		$this->db->insert('role',$data);
		return $this->db->insert_id();
	}
	
	function edit($id,$data){
		$this->db->where('role_id',$id);
		$this->db->update('role',$data);
	}
	
	function delete($id){
		$this->db->where('role_id',$id);
		return $this->db->delete('role');
	}
	
	/*ROLE_MODULE*/
	function get_role_module($where = NULL){
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get('role_module');
	}
	
	function add_role_module($data){
		$this->db->insert('role_module',$data);
	}
	
	function delete_role_module($where){
		$this->db->where($where);
		return $this->db->delete('role_module');
	}
}