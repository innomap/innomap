<?php
class Site extends CI_Model {
	function __construct() {
		parent::__construct();
	}
	
	/*state*/
	function get_state(){
		return $this->db->get('state')->result_array();
	}
	
	/*upload file*/
	function _upload($name,$attachment,$upload_path,$type="img", $thumb_path = NULL) {
		$this->load->library('upload');
		$config['file_name'] 		= $name;
		$config['upload_path'] 		= $upload_path;
		if($type == "img"){
			$config['allowed_types'] 	= 'png|jpg|gif|bmp|jpeg';
		}else{
			$config['allowed_types'] 	= 'png|jpg|gif|bmp|jpeg|pdf|docx|doc|xls|xlsx|ppt|pptx|txt|ods';
		}
		$config['remove_spaces']	= TRUE;
		
		$this->upload->initialize($config);
		if(!$this->upload->do_upload($attachment,true)) {
			echo $this->upload->display_errors();
			return false;
		}else{
			$upload_data = $this->upload->data();
			if($thumb_path != NULL && $type == "img"){
				$this->create_thumbnail($upload_data['image_width'],$upload_data['image_height'],$upload_path,$upload_data['file_name'], $thumb_path);
			}
			return $upload_data['file_name'];
		}
	}
	
	/*remove file*/
	function _remove($file_name,$path){
		if (file_exists(realpath(APPPATH . '../'.$path) . DIRECTORY_SEPARATOR . $file_name)) {
			unlink("./".$path."/".$file_name);
		}
	}
	
	/*attachment preview*/
	function show_attachment($file_name,$path,$pwidth,$pheight,$iwidth){
		if($file_name != NULL){
			$path_parts = pathinfo($path."/".$file_name);
			if($path_parts['extension'] == 'pdf'){
				return '<object data="'.base_url().$path.$file_name.'" type="application/pdf" width="'.$pwidth.'" height="'.$pheight.'">
						alt : <a href="'.base_url().$path.$file_name.'">'.$file_name.'</a></object>';
			}elseif($path_parts['extension'] == 'docx' || $path_parts['extension'] == 'doc' 
					|| $path_parts['extension'] == 'xlsx' || $path_parts['extension'] == 'xls'
					|| $path_parts['extension'] == 'pptx' || $path_parts['extension'] == 'ppt'
					|| $path_parts['extension'] == 'txt'){
				return '<a href="'.base_url().$path.$file_name.'">'.$file_name.'</a>';
			}else{
				return '<img src="'.base_url().$path.$file_name.'" style="width:'.$iwidth.'px;"/>';
			}
		}else{
			return NULL;
		}
	}

	public function create_thumbnail($image_width,$image_height,$upload_path,$file_name,$new_path){
		$this->load->library('image_lib');
		
		if ($image_width > 1024 || $image_height > 1024) {
            $config['image_library'] = 'gd2';
            $config['source_image'] = $upload_path.$file_name;
            $config['new_image'] = $new_path.$file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 1024;
            $config['height'] = 1024;

            $this->image_lib->clear();
            $this->image_lib->initialize($config); 

            if ( ! $this->image_lib->resize()){
                echo $this->image_lib->display_errors();
                return false;
            }
        }else{
        	$config['image_library'] = 'gd2';
            $config['source_image'] = $upload_path.$file_name;
            $config['new_image'] = $new_path.$file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = $image_width;
            $config['height'] = $image_height;

            $this->image_lib->clear();
            $this->image_lib->initialize($config); 

            if ( ! $this->image_lib->resize()){
                echo $this->image_lib->display_errors();
                return false;
            }
        }
	}
}
