<?php
class Task extends CI_Model {
	function __construct() {
		parent::__construct();

		//select field by language option
		if($this->session->userdata('language') == LANGUAGE_MELAYU){
			$this->innovation = 'innovation.name_in_melayu';
			$this->innovation_description = 'innovation.description_in_melayu';
		}else{
			$this->innovation = 'innovation.name';
			$this->innovation_description = 'innovation.description';
		}
	}

	function get_list(){
		$this->db->select('expert_task.*, (SELECT COUNT(expert_task_id) FROM expert_task_innovation WHERE expert_task.expert_task_id = expert_task_innovation.expert_task_id) as has_innovation',false);
		$this->db->from('expert_task');
		$this->db->having('has_innovation > 0');
		$result = $this->db->get()->result_array();
		
		foreach($result as $key=>$value){
			$result[$key]['expert'] = $this->get_task_expert(array('expert_task_expert.expert_task_id' => $value['expert_task_id']))->result_array();	
		}
		
		return $result;
	}

	function get_detail($task_id){
		$this->db->select('*');
		$this->db->from('expert_task');
		$this->db->where('expert_task_id',$task_id);
		$result = $this->db->get()->row_array();
		if($result){
			$result['expert'] = $this->get_task_expert(array('expert_task_expert.expert_task_id' => $task_id))->result_array();
			$result['innovation'] = $this->get_task_innovation(array('expert_task_innovation.expert_task_id' => $task_id))->result_array();
		}
		return $result;
	}

	function get_task_innovation($where = NULL){
		$this->db->select('expert_task_innovation.*,'.$this->innovation.' as innovation_name,innovation.innovation_id,innovation.submission_date, innovator.name as innovator, (SELECT COUNT(innovation_evaluation.evaluation_id) FROM innovation_evaluation WHERE innovation_evaluation.innovation_id = expert_task_innovation.innovation_id) as evaluation_result');
		$this->db->from('expert_task_innovation');
		$this->db->join('innovation','expert_task_innovation.innovation_id = innovation.innovation_id');
		$this->db->join('innovator_innovation','innovator_innovation.innovation_id = innovation.innovation_id');
		$this->db->join('innovator','innovator.innovator_id = innovator_innovation.innovator_id');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}

	function get_task_expert($where = NULL){
		$this->db->select('expert.expert_id,expert.name as expert_name');
		$this->db->from('expert_task_expert');
		$this->db->join('expert','expert.expert_id = expert_task_expert.expert_id');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}

	function get_list_detail($where = NULL){
		$this->db->select('expert_task_innovation.*,'.$this->innovation.' as innovation_name,innovation.innovation_id,'.$this->innovation_description.' as description,innovator.name as innovator, expert_task_expert.expert_id, (SELECT evaluation_id FROM innovation_evaluation WHERE innovation_id = expert_task_innovation.innovation_id AND evaluator_expert_id = expert_task_expert.expert_id) as evaluation_id, (SELECT status FROM innovation_evaluation WHERE innovation_id = expert_task_innovation.innovation_id AND evaluator_expert_id = expert_task_expert.expert_id) as status', false);
		$this->db->from('expert_task_innovation');
		$this->db->join('expert_task','expert_task_innovation.expert_task_id = expert_task.expert_task_id');
		$this->db->join('expert_task_expert', 'expert_task_expert.expert_task_id = expert_task.expert_task_id');
		$this->db->join('innovation','expert_task_innovation.innovation_id = innovation.innovation_id');
		$this->db->join('innovator_innovation','innovator_innovation.innovation_id = innovation.innovation_id');
		$this->db->join('innovator','innovator_innovation.innovator_id = innovator.innovator_id');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}

	function edit_expert_task_innovation($innovation_id,$data){
		$this->db->where('innovation_id',$innovation_id);
		return $this->db->update('expert_task_innovation',$data);
	}

	function get($where = NULL){
		$this->db->select('*');
		$this->db->from('expert_task');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}

	function add($data){
		$this->db->insert('expert_task',$data);
		return $this->db->insert_id();
	}

	function add_task_innovation($data){
		$this->db->insert('expert_task_innovation',$data);
		return $this->db->insert_id();	
	}

	function get_innovation_list(){
		$query = $this->db->query("SELECT innovation.innovation_id, ".$this->innovation." as name, innovation.submission_date, innovator.name as innovator FROM innovation JOIN innovator_innovation ON innovation.innovation_id = innovator_innovation.innovation_id JOIN innovator ON innovator_innovation.innovator_id = innovator.innovator_id WHERE NOT EXISTS(SELECT expert_task_innovation.innovation_id FROM expert_task_innovation WHERE innovation.innovation_id=expert_task_innovation.innovation_id) AND innovation.status = ".INNO_APPROVED." ORDER BY innovation.submission_date ASC");
		return $query;
	}

	function get_available_expert(){
		$query = $this->db->query("SELECT expert_id,name FROM expert WHERE expert_id NOT IN(SELECT expert.expert_id FROM expert JOIN expert_task ON expert_task.expert_id = expert.expert_id JOIN expert_task_innovation ON expert_task.expert_task_id = expert_task_innovation.expert_task_id LEFT JOIN innovation_evaluation ON expert_task_innovation.innovation_id = innovation_evaluation.innovation_id WHERE innovation_evaluation.status IS NULL OR innovation_evaluation.status = ".EVALUATION_DRAFT.")");
		return $query;
	}

	function delete($id){
		$this->db->where('expert_task_id',$id);
		return $this->db->delete('expert_task');
	}

	// get all task with task status
	function get_task_status(){
		$query = $this->db->query("SELECT *,(SELECT COUNT(expert_task_id) FROM expert_task_innovation WHERE expert_task_id = expert_task.expert_task_id) as inside_task, (SELECT COUNT(innovation_evaluation.innovation_id) FROM innovation_evaluation JOIN expert_task_innovation ON innovation_evaluation.innovation_id = expert_task_innovation.innovation_id WHERE expert_task.expert_task_id = expert_task_innovation.expert_task_id AND status = ".EVALUATION_DONE.") as status_done FROM expert_task");
		return $query;
	}

	function add_task_expert($data){
		$this->db->insert('expert_task_expert',$data);
		return $this->db->insert_id();	
	}
}