<?php
class University extends CI_Model {
	function __construct() {
		parent::__construct();
	}
	
	function get($where = NULL){
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get('university');
	}

	function get_universities($where = NULL){
		$this->db->select('university.*, category.name as category_name, state.name as state_name');
		$this->db->from('university');
		$this->db->join('category','university.specialty=category.category_id','left');
		$this->db->join('state','university.state=state.state_id','left');
		if($where != NULL){
			$this->db->where($where);
		}
		$this->db->order_by('university.university_id','DESC');
		return $this->db->get();
	}

	function add($data){
		$this->db->insert('university',$data);
		return $this->db->insert_id();
	}

	function edit($id, $data){
		$this->db->where('university_id', $id);
		$this->db->update('university', $data);
		return true;
	}

	function delete($id){
		$this->db->where('university_id',$id);
		$this->db->delete('university');
		return true;
	}
}
