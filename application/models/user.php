<?php
class User extends CI_Model {
	function __construct() {
		parent::__construct();
	}
	
	function get($where = null){
		$this->db->select('*');
		$this->db->from('user');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}

	function get_user($where){
		$users = $this->get($where)->result_array();
		foreach ($users as $key => $user) {
			$users[$key]['roles'] = $this->get_user_role(array('user_id'=>$user['user_id']))->result_array();
		}
		return $users;
	}

	function get_user_role($where){
		$this->db->select('user_role_id, user_role.role_id as role_id, role.name');
		$this->db->from('user_role');
		$this->db->join('role', 'user_role.role_id = role.role_id');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}

	function get_based_user_role($where = NULL){
		$users = $this->get_user_with_role($where)->result_array();
		foreach ($users as $key => $user) {
			$users[$key]['roles'] = $this->get_user_role(array('user_id'=>$user['user_id'], ))->result_array();
		}
		return $users;
	}

	function get_user_with_role($where = NULL){
		$this->db->select('user.*');
		$this->db->from('user');
		$this->db->join('user_role', 'user.user_id = user_role.user_id');
		$this->db->where('user_role.role_id !=', ROLE_EXPERT);
		$this->db->where('user_role.role_id !=', ROLE_INNOVATOR);
		if($where != NULL){
			$this->db->where($where);
		}
		$this->db->distinct();
		return $this->db->get();
	}

	function get_hash($username, $password) {
		return md5($username.':'.$password);
	}

	function add($data) {
		$data['password'] = $this->get_hash($data['username'], $data['password']);
		$role_id = $data['role_id'];
		unset($data['role_id']);
		$this->db->insert('user', $data);
		$id =  $this->db->insert_id();
		if(is_array($role_id)){
			foreach ($role_id as $key => $role) {
				$data_user_role = array('user_id' => $id, 'role_id' => $role);
				$this->add_role($data_user_role);
			}
		}else{
			$data_user_role = array('user_id' => $id, 'role_id' => $role_id);
			$this->add_role($data_user_role);
		}
		return $id;
	}

	function edit($id,$data) {
		if(isset($data['password'])){
			$data['password'] = $this->get_hash($data['username'], $data['password']);
		}
		if(isset($data['role_id'])){
			$role_id = $data['role_id'];
			unset($data['role_id']);
			$this->delete_role(array('user_id' => $id));
			if(is_array($role_id)){
				foreach ($role_id as $key => $role) {
					$data_user_role = array('user_id' => $id, 'role_id' => $role);
					$this->add_role($data_user_role);
				}
			}else{
				$data_user_role = array('user_id' => $id, 'role_id' => $role_id);
				$this->add_role($data_user_role);
			}
		}
		
		$this->db->where('user_id',$id);
		return $this->db->update('user', $data);
	}

	function activate($ids) {
		$data = array(
			'status' => USER_ACTIVE,
		);
		
		foreach($ids as $id) {
			$this->db->or_where('user_id', $id);
		}
		
		$this->db->update('user', $data);
	}

	function suspend($ids) {
		$data = array(
			'status' => USER_NOT_ACTIVE,
		);
		
		foreach($ids as $id) {
			$this->db->or_where('user_id', $id);
		}
		
		$this->db->update('user', $data);
	}

	function auth($username, $password, $where = NULL) {
		$this->db->select('user.*');
		$this->db->from('user');
		$this->db->where(array(
			'user.username' => $username,
			'user.password' => $this->get_hash($username, $password),
			'user.status' => USER_ACTIVE
		));
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get()->row_array();
	}

	function auth_with_role($username, $password, $where = NULL) {
		$this->db->select('user.*, user_role.role_id');
		$this->db->from('user');
		$this->db->join('user_role','user.user_id = user_role.user_id');
		$this->db->where(array(
			'user.username' => $username,
			'user.password' => $this->get_hash($username, $password),
			'user.status' => USER_ACTIVE
		));
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get()->row_array();
	}

	function delete($id){
		$this->db->where('user_id',$id);
		return $this->db->delete('user');
	}

	function set_user_password(){
		$users = $this->get()->result_array();
		foreach($users as $user){
			$this->edit($user['user_id'],array('username' => $user['username'], 'password' => 'testing'));
		}
	}

	function add_role($data) {
		$this->db->insert('user_role', $data);
		return $this->db->insert_id();
	}

	function edit_role($id,$data) {
		$this->db->where('user_role_id',$id);
		return $this->db->update('user_role', $data);
	}

	function delete_role($where = NULL){
		$this->db->where($where);
		return $this->db->delete('user_role');
	}

	function get_user_id($where = NULL){
		$this->db->select('user.user_id');
		$this->db->from('user');
		$this->db->join('user_role','user.user_id = user_role.user_id','LEFT');
		if($where != NULL){
			$this->db->where($where);
		}
		return $this->db->get();
	}
}
