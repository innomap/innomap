<?php
class User_notification extends CI_Model {
	function __construct() {
		parent::__construct();
	}
	
	function get($where = NULL, $order_by = NULL){
		$this->db->select('user_notification.*, user.username');
		$this->db->from('user_notification');
		$this->db->join('user','user.user_id = user_notification.user_id_from');
		
		if($where != NULL){
			$this->db->where($where);
		}
		
		if($order_by != NULL){
			$this->db->order_by($order_by,'DESC');	
		}

		return $this->db->get();
	}

	function get_by_user($user_id, $limit = 0, $offset = 0){
		$where = "user_notification.user_id_to = ".$user_id;

		$sQuery = "SELECT SQL_CALC_FOUND_ROWS user_notification.*, user.username FROM user_notification JOIN user ON user.user_id = user_notification.user_id_from WHERE ".$where." ORDER BY created_date DESC LIMIT ".$offset.",".$limit;

		$query = $this->db->query($sQuery);
		$data = $query->result_array();
		
		$sQuery = "SELECT FOUND_ROWS() AS total";
		$total = $this->db->query($sQuery)->row()->total;
		
		return array('data' => $data, 'total' => $total);
	}

	function add($data){
		$this->db->insert('user_notification',$data);
		$notification_id = $this->db->insert_id();
		$this->send_email($notification_id);
		return $notification_id;
	}

	function edit($id,$data){
		$this->db->where('notification_id',$id);
		return $this->db->update('user_notification',$data);
	}

	function delete($id){
		$this->db->where('notification_id',$id);
		return $this->db->delete('user_notification');
	}

	function send_multi_user($type, $target_id){
		$this->load->model(array('innovator','user','task','portfolio'));

		$users = $this->get_multi_user($type, $target_id);
		$users = array_intersect_key( $users , array_unique( array_map('serialize' , $users ) ) );
		
		foreach($users as $row){
			$data_notif = array('user_id_from' => $this->user_sess['user_id'],
								'user_id_to' => $row['user_id'],
								'notif_type' => $type,
								'target_id' => $target_id);

			if(array_key_exists('notes', $row)){
				$data_notif['notes'] = $row['notes'];
			}
			$this->add($data_notif);
		}
	}

	function get_multi_user($type, $target_id){
		$users = array();
		if($type == NOTIF_NEW_INNOVATOR || $type == NOTIF_NEW_INNOVATION){
			$users = $this->user->get_user_id(array('role_id' => ROLE_DATA_ADMIN,'user.user_id !=' => $this->user_sess['user_id']))->result_array();
		}else if($type == NOTIF_APPROVED_INNOVATOR){
			$innovator = $this->innovator->get(array('innovator_id' => $target_id))->row_array();
			$users = $this->user->get_user_id(array('role_id' => ROLE_IDEC_HEAD))->result_array();

			if($innovator['analyst_creator_id'] != $this->user_sess['user_id']){
				$users[count($users)] = array('user_id' => $innovator['analyst_creator_id']);
			}
		}else if($type == NOTIF_REJECTED_INNOVATOR){
			$innovator = $this->innovator->get(array('innovator_id' => $target_id))->row_array();
			if($innovator['analyst_creator_id'] != $this->user_sess['user_id']){
				$users[0] = array('user_id' => $innovator['analyst_creator_id']);
			}
		}else if($type == NOTIF_APPROVED_INNOVATION){
			$innovation = $this->innovation->get(array('innovation_id' => $target_id))->row_array();
			$users = $this->user->get_user_id(array('role_id' => ROLE_IDEC_HEAD))->result_array();
			$users[count($users)] = array('user_id' => $innovation['analyst_creator_id']);
		}else if($type == NOTIF_REJECTED_INNOVATION){
			$innovation = $this->innovation->get(array('innovation_id' => $target_id))->row_array();
			if($innovation['analyst_creator_id'] != $this->user_sess['user_id']){
				$users[0] = array('user_id' => $innovation['analyst_creator_id']);
			}
		}else if($type == NOTIF_EDIT_INNOVATION){
			$innovation = $this->innovation->get(array('innovation_id' => $target_id))->row_array();
			if($innovation['analyst_creator_id'] != $this->user_sess['user_id']){
				$users[] = array('user_id' => $innovation['analyst_creator_id']);
			}

			if($innovation['approver_id'] != $this->user_sess['user_id']){
				$users[] = array('user_id' => $innovation['approver_id']);
			}
		}else if($type == NOTIF_EDIT_INNOVATOR){
			$innovator = $this->innovator->get(array('innovator_id' => $target_id))->row_array();
			if($innovator['analyst_creator_id'] != $this->user_sess['user_id']){
				$users[] = array('user_id' => $innovator['analyst_creator_id']);
			}

			if($innovator['approver_id'] != $this->user_sess['user_id']){
				$users[] = $innovator['approver_id'];
			}
		}else if($type == NOTIF_DELETE_INNOVATION){
			$innovation = $this->innovation->get(array('innovation_id' => $target_id))->row_array();
			if($innovation['analyst_creator_id'] != $this->user_sess['user_id']){
				$users[] = array('user_id' => $innovation['analyst_creator_id'], 'notes' => $innovation['name']);
			}

			if($innovation['approver_id'] != $this->user_sess['user_id']){
				$users[] = array('user_id' => $innovation['approver_id'], 'notes' => $innovation['name']);
			}
		}else if($type == NOTIF_DELETE_INNOVATOR){
			$innovator = $this->innovator->get(array('innovator_id' => $target_id))->row_array();
			if($innovator['analyst_creator_id'] != $this->user_sess['user_id']){
				$users[] = array('user_id' => $innovator['analyst_creator_id'], 'notes' => $innovator['name']);
			}

			if($innovator['approver_id'] != $this->user_sess['user_id']){
				$users[] = array('user_id' => $innovator['approver_id'],'notes' => $innovator['name']);
			}
		}else if($type == NOTIF_DELETE_TASK){
			$expert = $this->task->get_task_expert(array('expert_task_expert.expert_task_id' => $target_id))->result_array();
			$task = $this->task->get(array('expert_task_id' => $target_id))->row_array();
			foreach ($expert as $key => $value) {
				$users[] = array('user_id' => $value['expert_id'],'notes' => $task['title']);
			}
		}else if($type == NOTIF_DELETE_PORTFOLIO){
			$portfolio = $this->portfolio->get(array('p.portfolio_id' => $target_id))->row_array();
			$users[] = array('user_id' => $portfolio['manager_id'],'notes' => $portfolio['title']);
		}

		return $users;
	}

	function send_email($id){
		$this->load->helper('phpmailer');
		$this->load->model('user');

		$value = $this->user_notification->get(array('notification_id' => $id))->result_array();
		$user_target = $this->user->get(array('user_id' => $value[0]['user_id_to']))->row_array();

		if($user_target['email'] != NULL){
			$notif = $this->check_notif_content($value);

			$notif = $notif[0];
			$message = $notif['from']." ".$notif['message']." <a href='".base_url()."notifications/read/".$id."'>".$notif['target']."</a>";
			
			$mail_param 	= array('from'		=> "admin@mobilus-interactive.com",
									'fromname' 	=> "Innomap",
									'to' 		=> $user_target['email'],
									'subject' 	=> "Notification",
									'message' 	=> $message);
			return phpmailer_send($mail_param, true) || TRUE;
		}
	}

	function check_notif_content($notif){
		$this->load->model(array('innovator','user','task','portfolio','committee_meeting'));

		if($this->session->userdata('language') == LANGUAGE_MELAYU){
			$notif_title = unserialize(NOTIF_TITLE_MELAYU);
		}else{
			$notif_title = unserialize(NOTIF_TITLE);
		}
		foreach($notif as $key => $value){
			$notif[$key]['from'] = $value['username'];
			$target_index = "";

			if($value['notif_type'] == NOTIF_TASK_ASSIGN){
				$target = $this->task->get(array('expert_task_id' => $value['target_id']))->row_array();
				$notif[$key]['message'] = $notif_title[NOTIF_TASK_ASSIGN];
				$target_index = 'title';

			}else if($value['notif_type'] == NOTIF_NEW_INNOVATOR){
				$target = $this->innovator->get(array('innovator_id' => $value['target_id']))->row_array();
				$notif[$key]['message'] = $notif_title[NOTIF_NEW_INNOVATOR];


			}else if($value['notif_type'] == NOTIF_APPROVED_INNOVATOR){
				$target = $this->innovator->get(array('innovator_id' => $value['target_id']))->row_array();
				$notif[$key]['message'] = $notif_title[NOTIF_APPROVED_INNOVATOR];


			}else if($value['notif_type'] == NOTIF_EVALUATION_DONE){
				$target = $this->evaluation->get_detail(array('innovation_evaluation.innovation_id' => $value['target_id']))->row_array();
				$notif[$key]['message'] = $notif_title[NOTIF_EVALUATION_DONE];
				$target_index = 'innovation_name';

			}else if($value['notif_type'] == NOTIF_PORTFOLIO_ASSIGN){
				$target = $this->portfolio->get(array('portfolio_id' => $value['target_id']))->row_array();
				$notif[$key]['message'] = $notif_title[NOTIF_PORTFOLIO_ASSIGN];
				$target_index = 'title';

			}else if($value['notif_type'] == NOTIF_REJECTED_INNOVATOR){
				$target = $this->innovator->get(array('innovator_id' => $value['target_id']))->row_array();
				$notif[$key]['message'] = $notif_title[NOTIF_REJECTED_INNOVATOR];

			}else if($value['notif_type'] == NOTIF_APPROVED_INNOVATION){
				$target = $this->innovation->get(array('innovation_id' => $value['target_id']))->row_array();
				$notif[$key]['message'] = $notif_title[NOTIF_APPROVED_INNOVATION];
				$target_index = 'name_in_melayu';

			}else if($value['notif_type'] == NOTIF_REJECTED_INNOVATION){
				$target = $this->innovation->get(array('innovation_id' => $value['target_id']))->row_array();
				$notif[$key]['message'] = $notif_title[NOTIF_REJECTED_INNOVATION];
				$target_index = 'name_in_melayu';

			}else if($value['notif_type'] == NOTIF_PORTFOLIO_COMMENT){
				$target = $this->portfolio->get(array('portfolio_id' => $value['target_id']))->row_array();
				$notif[$key]['message'] = $notif_title[NOTIF_PORTFOLIO_COMMENT];
				$target_index = 'title';

			}else if($value['notif_type'] == NOTIF_NEW_INNOVATION){
				$target = $this->innovation->get(array('innovation_id' => $value['target_id']))->row_array();
				$notif[$key]['message'] = $notif_title[NOTIF_NEW_INNOVATION];
				$target_index = 'name_in_melayu';

			}else if($value['notif_type'] == NOTIF_PORTFOLIO_DONE){
				$target = $this->portfolio->get(array('portfolio_id' => $value['target_id']))->row_array();
				$notif[$key]['message'] = $notif_title[NOTIF_PORTFOLIO_DONE];
				$target_index = 'title';

			}else if($value['notif_type'] == NOTIF_EDIT_INNOVATION){
				$target = $this->innovation->get(array('innovation_id' => $value['target_id']))->row_array();
				$notif[$key]['message'] = $notif_title[NOTIF_EDIT_INNOVATION];
				$target_index = 'name_in_melayu';

			}else if($value['notif_type'] == NOTIF_EDIT_INNOVATOR){
				$target = $this->innovator->get(array('innovator_id' => $value['target_id']))->row_array();
				$notif[$key]['message'] = $notif_title[NOTIF_EDIT_INNOVATOR];

			}else if($value['notif_type'] == NOTIF_DELETE_INNOVATION){
				$target = array('name' => $value['notes']);
				$notif[$key]['message'] = $notif_title[NOTIF_DELETE_INNOVATION];
			}else if($value['notif_type'] == NOTIF_DELETE_INNOVATOR){
				$target = array('name' => $value['notes']);
				$notif[$key]['message'] = $notif_title[NOTIF_DELETE_INNOVATOR];
			}else if($value['notif_type'] == NOTIF_DELETE_TASK){
				$target = array('name' => $value['notes']);
				$notif[$key]['message'] = $notif_title[NOTIF_DELETE_TASK];
			}else if($value['notif_type'] == NOTIF_DELETE_PORTFOLIO){
				$target = array('name' => $value['notes']);
				$notif[$key]['message'] = $notif_title[NOTIF_DELETE_PORTFOLIO];
			}else if($value['notif_type'] == NOTIF_COMMITTEE_MEETING){
				$target = array('name' => lang('button_view_detail'));
				$notif[$key]['message'] = $notif_title[NOTIF_COMMITTEE_MEETING];
			}


			$notif[$key]['notif_type'] = $value['notif_type'];

			if($target){
				if($target_index != ''){
					$notif[$key]['target'] = $target[$target_index];
				}else{
					$notif[$key]['target'] = $target['name'];
				}
			}else{
				unset($notif[$key]);
			}	
		}

		return $notif;
	}
}
