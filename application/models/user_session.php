<?php
class User_session extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	function get() {
		return $this->session->userdata(USER_SESSION);
	}
	
	function set($data) {
		$this->session->set_userdata(USER_SESSION, $data);
		$this->session->set_userdata('language','english');
	}
	
	function create($username, $password) {
		$data = array();
		$this->load->model('user');
		if($data = $this->user->auth($username, $password)){
			unset($data['password']);
			if($data){
				$data['roles'] = $this->user->get_user_role(array('user_id'=>$data['user_id']))->result_array();
			}
		}
		return $data;
	}
	
	function clear() {
		$this->session->unset_userdata(USER_SESSION);
	}
}