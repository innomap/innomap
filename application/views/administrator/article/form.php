<div class="block article">
	<div class="title">
		<div>Article In Melayu</div>
	</div>
	<div class="block-content">
		<form action="<?= base_url().ADMIN_DIR.'articles/save' ?>" method="post" id="form-article" enctype="multipart/form-data">			
			<input type="hidden" name="mode" value="<?= $mode; ?>">
			<input type="hidden" name="article_id" value="<?= ($mode != 'ADD' ? $article['article_id'] : '') ?>">
			<div class="content-form" style="width:97%; margin-top:0;">	
				<!-- hide english lang
				<table class="form-table">
					<tr>
						<td>Title</td>
						<td><input type="text" name="title" value="<?= ($mode != 'ADD' ? $article['title'] : ''); ?>" <?= ($mode == 'VIEW' ? 'readonly' : '') ?>></td>
					</tr>
					<tr>
						<td>Content</td>
						<td><textarea name="content" class="mce_editor" id="content" <?= ($mode == 'VIEW' ? 'disabled' : '') ?>><?= ($mode != 'ADD' ? $article['content'] : ''); ?></textarea></td>
					</tr>
				</table>
				<input type="text" style="opacity:0;position:absolute" id="hidden_content" name="hidden_content" />
			
				<div class="title" style="width:105.4%; margin-left:-33px;">
					<div>Article in Melayu</div>
				</div> -->

				<table class="form-table">
					<tr>
						<td>Title</td>
						<td><input type="text" name="title_in_melayu" value="<?= ($mode != 'ADD' ? $article['title_in_melayu'] : ''); ?>" <?= ($mode == 'VIEW' ? 'readonly' : '') ?>></td>
					</tr>
					<tr>
						<td>Content</td>
						<td>
							<textarea name="content_in_melayu" class="mce_editor" id="content-in-melayu" <?= ($mode == 'VIEW' ? 'disabled' : '') ?>><?= ($mode != 'ADD' ? $article['content_in_melayu'] : ''); ?></textarea>
							<input type="text" style="opacity:0;position:absolute" id="hidden_content_in_melayu" name="hidden_content_in_melayu" />
						</td>
					</tr>
					<tr>
						<td>Category</td>
						<td>
							<select name="category" <?= ($mode == 'VIEW' ? 'disabled' : '') ?>>
								<option value="">Select Category</option>
								<?php foreach($categories as $category){ ?>
									<option value="<?= $category['article_category_id'] ?>" <?= ($mode != 'ADD' ? ($category['article_category_id'] == $article['article_category_id'] ? 'selected' : '') : '') ?>><?= $category['name'] ?></option>
								<?php } ?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Status</td>
						<td>
							<select name="status" <?= ($mode == 'VIEW' ? 'disabled' : '') ?>>
								<?php foreach($status as $key => $value){ ?>
									<option value="<?= $key; ?>" <?= ($mode != 'ADD' ? ($key == $article['status'] ? 'selected' : '') : '') ?>><?= $value; ?></option>
								<?php } ?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Image</td>
						<td>
							<?php if($mode != 'ADD' && $article['image'] != NULL){ ?>
								<img src="<?= base_url().PATH_TO_ARTICLE_IMAGE.$article['image']; ?>" style="width:200px" />
							<?php } ?>
							<?php if($mode != 'VIEW'){ ?>
								<input type="file" name="image" />
								<input type="hidden" name="h_image" value="<?= ($mode != "ADD" ? $article['image'] : ''); ?>" />
							<?php } ?>
						</td>
					</tr>
				</table>
			</div>
			<div class="content-form" style="width:90%;">	
				<div style="float:right">
					<?php if($mode != 'VIEW'){ ?>
						<input type="submit" name="submit" id="submit-article" value="Save" class="button" />
					<?php } ?>
					<a href="<?= base_url().ADMIN_DIR.'articles'; ?>"><input type="button" class="button" value="Back" /></a>
				</div>
			</div>
		</form>
	</div>
</div>