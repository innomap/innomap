<div class="block article">
	<div class="title">
		<div style="float:left;">Articles</div>
		<a href="<?= base_url().ADMIN_DIR.'articles/add'; ?>" class="reset_data_tables">
			<div class="add_button">
				<div>Add New Article</div>
			</div>
		</a>
		<div class="clear"></div>
	</div>
	<div class="block-content">
		<?php if($message != NULL){ ?>
			<br/><div class="form-info success"><?= $message; ?></div>
		<?php } ?>
		<table cellpadding="0" cellspacing="0" border="0" class="display blue-table" width="100%" id="article_table">
			<thead>
				<th>No</th>
				<th>Created By</th>
				<th>Title</th>
				<th>Category</th>
				<th>Created Date</th>
				<th>Published Date</th>
				<th>Status</th>
				<th width="15%">Action</th>
			</thead>
			<tbody>
				<?php $no=1; foreach($articles as $article){ ?>
				<tr>
					<td><?= $no; ?></td>
					<td><?= $article['username']; ?></td>
					<td><?= $article['title']; ?></td>
					<td><?= $article['category_name']; ?></td>
					<td><?= $article['created_date']; ?></td>
					<td><?= $article['published_date']; ?></td>
					<td><a href="<?= base_url().ADMIN_DIR.'articles/update_status/'.$article['article_id']; ?>" title="<?= $status[$article['status']]; ?>"><center><div class="article_<?= $status[$article['status']]; ?>">&nbsp;</div></center></a></td>
					<td>
						<a href="<?= base_url().ADMIN_DIR.'articles/edit/'.$article['article_id']; ?>" title="Edit" class="edit"></a>
						<a href="<?= base_url().ADMIN_DIR.'articles/view/'.$article['article_id']; ?>" title="View" class="view-icon"></a>
						<a href="<?= base_url().ADMIN_DIR.'articles/delete/'.$article['article_id']; ?>" onClick="return initConfirmDelete()" title="Delete" class="delete"></a>
					</td>
				</tr>
				<?php $no++;} ?>
			</tbody>
		</table>
	</div>
</div>