<div class="block article">
	<div class="title">
		<div>Article Category In Melayu</div>
	</div>
	<div class="block-content">
		<form action="<?= base_url().ADMIN_DIR.'article_categories/save' ?>" method="post" id="form-article-category" enctype="multipart/form-data">

			<div class="content-form" style="width:97%;">	

				<input type="hidden" name="mode" value="<?= $mode; ?>">
				<input type="hidden" name="category_id" value="<?= ($mode != 'ADD' ? $category['article_category_id'] : '') ?>">
			<!-- hide english lang
				<table class="form-table">
					<tr>
						<td colspan="2"><b></b></td>
					</tr>
					<tr>
						<td>Name</td>
						<td><input type="text" name="name" value="<?= ($mode != 'ADD' ? $category['name'] : ''); ?>" <?= ($mode == 'VIEW' ? 'readonly' : '') ?>></td>
					</tr>
					<tr>
						<td>Description</td>
						<td>
							<textarea name="description" class="mce_editor" id="description" <?= ($mode == 'VIEW' ? 'disabled' : '') ?>><?= ($mode != 'ADD' ? $category['description'] : ''); ?></textarea>
							<input type="text" style="opacity:0;position:absolute" id="hidden_description" name="hidden_description" />
						</td>
					</tr>
				</table>

				<div class="title" style="width:105.4%; margin-left:-33px;">
					<div>Article Category In Melayu</div>
				</div>-->

				<table>
					<tr>
						<td>Name</td>
						<td><input type="text" name="name_in_melayu" value="<?= ($mode != 'ADD' ? $category['name_in_melayu'] : ''); ?>" <?= ($mode == 'VIEW' ? 'readonly' : '') ?>></td>
					</tr>
					<tr>
						<td>Description</td>
						<td>
							<textarea name="description_in_melayu" class="mce_editor" id="description-in-melayu" <?= ($mode == 'VIEW' ? 'disabled' : '') ?>><?= ($mode != 'ADD' ? $category['description_in_melayu'] : ''); ?></textarea>
							<input type="text" style="opacity:0;position:absolute" id="hidden_description_in_melayu" name="hidden_description_in_melayu" />
						</td>
					</tr>
					<tr>
						<td>Status</td>
						<td>
							<select name="status" <?= ($mode == 'VIEW' ? 'disabled' : '') ?>>
								<?php foreach($status as $key => $value){ ?>
									<option value="<?= $key; ?>" <?= ($mode != 'ADD' ? ($key == $category['status'] ? 'selected' : '') : '') ?>><?= $value; ?></option>
								<?php } ?>
							</select>
						</td>
					</tr>
				</table>
			</div>

			<div class="content-form" style="width:90%;">	
				<div style="float:right">
					<?php if($mode != 'VIEW'){ ?>
						<input type="submit" name="submit" id="submit-article-category" value="Save" class="button"/>
					<?php } ?>
					<a href="<?= base_url().ADMIN_DIR.'article_categories'; ?>"><input type="button" class="button" value="Back" /></a>
				</div>
			</div>
		</form>
	</div>
</div>