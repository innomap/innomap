<div class="block article">
	<div class="title">
		<div style="float:left;">Article Categories</div>
		<a href="<?= base_url().ADMIN_DIR.'article_categories/add'; ?>" class="reset_data_tables">
			<div class="add_button">
				<div>Add Article Category</div>
			</div>
		</a>
		<div class="clear"></div>
	</div>
	<div class="block-content">
		<?php if($message != NULL){ ?>
			<br/><div class="form-info success"><?= $message; ?></div>
		<?php } ?>
		<table cellpadding="0" cellspacing="0" border="0" class="display blue-table" width="100%" id="article_table">
			<thead>
				<th>No</th>
				<th>Name</th>
				<th>Description</th>
				<th>Status</th>
				<th>Published Date</th>
				<th width="15%">Action</th>
			</thead>
			<tbody>
				<?php $no=1; foreach($categories as $category){ ?>
				<tr>
					<td><?= $no; ?></td>
					<td><?= $category['name']; ?></td>
					<td><?= substr($category['description'],0,100)."..."; ?></td>
					<td><a href="<?= base_url().ADMIN_DIR.'article_categories/update_status/'.$category['article_category_id']; ?>" title="<?= $status[$category['status']]; ?>"><center><div class="article_<?= $status[$category['status']]; ?>">&nbsp;</div></center></a></td>
					<td><?= $category['published_date']; ?></td>
					<td>
						<a href="<?= base_url().ADMIN_DIR.'article_categories/edit/'.$category['article_category_id']; ?>" title="Edit" class="edit"></a>
						<a href="<?= base_url().ADMIN_DIR.'article_categories/view/'.$category['article_category_id']; ?>" title="View" class="view-icon"></a>
						<a href="<?= base_url().ADMIN_DIR.'article_categories/delete/'.$category['article_category_id']; ?>" onClick="return initConfirmDelete()" title="Delete" class="delete"></a>
					</td>
				</tr>
				<?php $no++;} ?>
			</tbody>
		</table>
	</div>
</div>