<div class="block user">
	<div class="title">
		<div><?= $mode; ?> Commercial Center</div>
	</div>
	<div class="block-content">
		<form action="<?= base_url().ADMIN_DIR."commercials/save"; ?>" name="form_commercial" id="form-commercial" method="post">
			<div class="content-form" style="background:#fef1c8; width:80%;">	
				<input type="hidden" name="mode" value="<?= $mode; ?>">
				<input type="hidden" name="commercial_id" value="<?= ($mode == 'EDIT' ? $id : ''); ?>">
				<table class="form-table">
					<tr>
						<td>Name</td>
						<td>
							<input type="text" name="name" value="<?= ($mode != 'ADD' ? $commercial['name'] : ""); ?>" <?= ($mode == 'VIEW' ? "disabled" : ""); ?> />
						</td>
					</tr>
					<tr>
						<td>Type</td>
						<td>
							<select name="type" <?= ($mode == 'VIEW' ? "disabled" : ""); ?> >
								<option value="">--Select Type--</option>
								<?php foreach ($type as $key=>$value) {
								 	if($mode == 'ADD'){ ?>
								 		<option value="<?= $key; ?>"><?= $value ?></option>
								<?php }else{ ?>
										<option <?php echo ($commercial['type']==$key ? "selected" :"");?> value="<?= $key; ?>"><?= $value; ?></option>
									<?php }
								 } ?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Telp</td>
						<td><input type="text" name="telp" value="<?= ($mode != 'ADD' ? $commercial['telp'] : ""); ?>" <?= ($mode == 'VIEW' ? "disabled" : ""); ?> /></td>
					</tr>
					<tr>
						<td>Address</td>
						<td>
							<textarea class="address-geo" rows="5" cols="40" name="address" <?= ($mode == 'VIEW' ? "disabled" : ""); ?> ><?= ($mode != 'ADD' ? $commercial['address'] : ""); ?></textarea>
						</td>
					</tr>
					<tr>
						<td>State</td>
						<td>
							<select name="state" <?= ($mode == 'VIEW' ? "disabled" : ""); ?> >
								<option value="">--Select State--</option>
								<?php foreach ($states as $state) {
										if($mode == 'ADD'){ ?>
											<option value="<?= $state['state_id']; ?>"><?= $state['name']; ?></option>
										<?php }else{ ?>
											<option <?php echo ($commercial['state_id'] == $state['state_id'] ? "selected" : ""); ?> value="<?= $state['state_id']; ?>"><?= $state['name']; ?></option>
										<?php } 
									} ?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Geo Location</td>
						<td>
							<input id="geo-location" type="text" name="geo_location" value="<?= ($mode != 'ADD' ? $commercial['geo_location'] : ""); ?>" <?= ($mode == 'VIEW' ? "disabled" : ""); ?> readonly/>
						</td>
					</tr>
				</table>
			</div>
			<div class="content-form" style="width:90%;">	
				<div style="float:right">
					<?php if($mode != 'VIEW') { ?><input type="submit" name="save_commercial" value="Save" class="button"><?php } ?>
					<a href="<?= base_url().ADMIN_DIR."commercials"; ?>"><input class="button" type="button" value="Back"/></a>
				</div>
			</div>
		</form>
	</div>
</div>

<script>
$(document).ready(function(){
	$("#btn_change_password").click(function() {
		$(".password_wrap").toggle("slow");
	});
});
</script>