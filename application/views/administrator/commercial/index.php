<div class="block user">
	<div class="title">
		<div style="float:left;">Commercial Center</div>
			<a href="<?= site_url(ADMIN_DIR.'commercials/add'); ?>" class="reset_data_tables">
				<div class="add_button">
					<div>Add New Commercial Center</div>
				</div>
			</a>
		<div class="clear"></div>
	</div>
	<div class="block-content">
		<?php if($message != NULL){ ?>
			<br/><div class="form-info success"><?= $message; ?></div>
		<?php } ?>
		<table cellpadding="0" cellspacing="0" border="0" class="display light-orange-table" width="100%" id="user_table">
			<thead>
				<th>No</th>
				<th>Name</th>
				<th>Type</th>
				<th>Address</th>
				<th>Telp</th>
				<th>State</th>
				<th>Action</th>
			</thead>
			<tbody>
				<?php $no=1; foreach($commercials as $commercial){ ?>
				<tr>
					<td><?= $no; ?></td>
					<td><?= $commercial['name']; ?></td>
					<td>
					<?php 
						foreach ($type as $key => $value) {
							echo ($key == $commercial['type'] ? $value : "");
						}
					?>
					</td>
					<td><?= $commercial['address'] ?></td>
					<td><?= $commercial['telp'] ?></td>
					<td><?= $commercial['state_name'] ?></td>
					<td>
						<a href="<?= base_url().ADMIN_DIR.'commercials/view/'.$commercial['commercial_id']; ?>" class="view-icon view" title="View"></a>
						<a href="<?= base_url().ADMIN_DIR.'commercials/edit/'.$commercial['commercial_id']; ?>" class="edit-ico edit" title="Edit"></a>
						<a href="<?= base_url().ADMIN_DIR.'commercials/delete/'.$commercial['commercial_id']; ?>" class="delete-ico delete" title="Delete" onclick="return initConfirmDelete()"></a>
					</td>
				</tr>
				<?php $no++;} ?>
			</tbody>
		</table>
	</div>
</div>