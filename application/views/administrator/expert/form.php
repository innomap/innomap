<div class="block article">
	<div class="title">
		<div><?= $mode ?> Expert</div>
		<div class="clear"></div>
	</div>
	<div class="block-content">
		<form action="<?= base_url().ADMIN_DIR."experts/save"; ?>" name="form_expert" id="form-expert" method="post" enctype="multipart/form-data">
			<div class="content-form" style="background:#c9edf9; width:95%;">	
				<input type="hidden" name="mode" value="<?= $mode; ?>">
				<input type="hidden" name="expert_id" value="<?= ($mode == 'EDIT' ? $id : ''); ?>">
				<input type="hidden" name="user_id" value="<?= ($mode == 'EDIT' ? $expert['user_id'] : ''); ?>">

				<table class="form-table">
					<tr>
						<td width="25%">Username</td>
						<td>
							<input type="text" name="username" value="<?= ($mode == 'EDIT' ? $expert['username'] : '') ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>>
							<label id="labelUsernameExist" class="error" style="display:none;">This username is already used.</label>
						</td>
					</tr>
					<tr>
						<td>Email</td>
						<td><input type="text" name="email" value="<?= ($mode == 'EDIT' ? $expert['email'] : '') ?>"></td>
					</tr>
					<?php if($mode == 'EDIT'){ ?>
					<tr>
						<td colspan="2"><a href="#" id="btn_change_password" style="text-decoration:underline;cursor:pointer;display:<?= ($mode == 'EDIT' ? 'block':'none') ?>">Ubah Password</a></td>
					</tr>
					<?php } ?>
					<tr class="password_wrap" style="<?= $mode == 'ADD' ? '' : 'display:none;'; ?>">
						<td>Password</td>
						<td><input type="password" name="password" id="password" /></td>
					</tr>
					<tr class="password_wrap" style="<?= $mode == 'ADD' ? '' : 'display:none;'; ?>">
						<td>Retype Password</td>
						<td><input type="password" name="retype_password" /></td>
					</tr>
					<tr>
						<td>User Status</td>
						<td>
							<select name="status" class="inputbox">
								<option value="1" <?= ($mode == 'EDIT' ? ($expert['status'] == 1 ? 'selected' : '') : '') ?>>Active</option>
								<option value="0" <?= ($mode == 'EDIT' ? ($expert['status'] == 0 ? 'selected' : '') : '') ?>>Suspend</option>
							</select>
						</td>
					</tr>
				</table>
				<table class="form-table">
					<tr>
						<td width="25%">Name</td>
						<td><input type="text" name="name" value="<?= ($mode == 'EDIT' ? $expert['name'] : '') ?>"></td>
					</tr>									
					<tr>
						<td>Kad No</td>
						<td>
							<input type="text" name="kad_no1"  value="<?=(count($kad_no) == 3 ? $kad_no[0] : '')?>" size="6" maxlength="6" style="width:auto;" /> -
							<input type="text" name="kad_no2" value="<?=(count($kad_no) == 3 ? $kad_no[1] : '')?>" size="2" maxlength="2" style="width:auto;" /> -
							<input type="text" name="kad_no3" value="<?=(count($kad_no) == 3 ? $kad_no[2] : '')?>" size="4" maxlength="4" style="width:auto;" />
						</td>
					</tr>
					<tr>
						<td>Expertise</td>
						<td>
							<select name="expertise">
								<?php foreach($expertise as $exp){ ?>
									<option value="<?= $exp['category_id']; ?>" <?= ($mode == 'EDIT' ? ($expert['expertise'] == $exp['category_id'] ? 'selected' : '') : '') ?>><?= $exp['name']; ?></option>
								<?php } ?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Address</td>
						<td><textarea name="address"><?= ($mode == 'EDIT' ? $expert['address'] : '') ?></textarea></td>
					</tr>
					<tr>
						<td>State</td>
						<td>
							<select name="state">
								<?php foreach ($states as $key => $state) { ?>
									<option value="<?= $state['state_id']; ?>" <?= ($mode == 'EDIT' ? ($expert['state_id'] == $state['state_id'] ? 'selected' : '' ) : '') ?>><?= $state['name']; ?></option>  
								<?php } ?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Organization Address</td>
						<td><textarea name="organization_address"><?= ($mode == 'EDIT' ? $expert['organization_address'] : '') ?></textarea></td>
					</tr>
					<tr>
						<td>Office Phone Number</td>
						<td><input type="text" name="office_phone_no" value="<?= ($mode == 'EDIT' ? $expert['office_phone_no'] : '') ?>"></td>
					</tr>
					<tr>
						<td>Home Phone Number</td>
						<td><input type="text" name="home_phone_no" value="<?= ($mode == 'EDIT' ? $expert['home_phone_no'] : '') ?>"></td>	
					</tr>
					<tr>
						<td>Mobile Phone Number</td>
						<td><input type="text"name="mobile_phone_no" value="<?= ($mode == 'EDIT' ? $expert['mobile_phone_no'] : '') ?>"></td>
					</tr>
					<tr>
						<td>Highest Education</td>
						<td><textarea name="highest_education"><?= ($mode == 'EDIT' ? $expert['highest_education'] : '') ?></textarea></td>
					</tr>
					<tr>
						<td>Bank Account</td>
						<td><input type="text" name="bank_account" value="<?= ($mode == 'EDIT' ? $expert['bank_account'] : '') ?>"></td>
					</tr>
					<tr>
						<td>Account No</td>
						<td><input type="text" name="account_no" value="<?= ($mode == 'EDIT' ? $expert['account_no'] : '') ?>"></td>
					</tr>
					<tr>
						<td>Picture</td>
						<td>
							<?php if($mode == 'EDIT' && $expert['picture'] != NULL){ ?>
								<img src="<?= base_url().PATH_TO_EXPERT_PICTURE.$expert['picture']?>" width="150px" /><br/>
							<?php } ?>
							<input type="file" name="picture" />
						</td>
					</tr>
				</table>
			</div>
			<div class="content-form" style="width:90%;">	
				<div style="float:right">
					<input type="submit" name="save_expert" value="Save" class="button" />
					<a href="<?= base_url().ADMIN_DIR."experts"; ?>"><input type="button" class="button" value="Back" /></a>
				</div>
			</div>
		</form>
	</div>
</div>

<script>
$(document).ready(function(){
	$("#btn_change_password").click(function() {
		$(".password_wrap").toggle("slow");
	});
});
</script>