<div class="block article">
	<div class="title">
		<div style="float:left;">Experts</div>
		<a href="<?= base_url().ADMIN_DIR.'experts/add'; ?>" class="reset_data_tables">
			<div class="add_button">
				<div>Add Expert</div>
			</div>
		</a>
		<div class="clear"></div>
	</div>
	<div class="block-content">
		<?php if($message != NULL){ ?>
			<br/><div class="form-info success"><?= $message; ?></div>
		<?php } ?>
		<table cellpadding="0" cellspacing="0" border="0" class="display blue-table" width="100%" id="experts_table">
			<thead>
				<th>No</th>
				<th>Name</th>
				<th>Kad No</th>
				<th>Expertise</th>
				<th>Email</th>
				<th>Action</th>
			</thead>
			<tbody>
				<?php $no=1; foreach($experts as $expert){ ?>
				<tr>
					<td><?= $no; ?></td>
					<td><?= $expert['name']; ?></td>
					<td><?= $expert['kad_no']; ?></td>
					<td>
						<?php 
							foreach ($categories as $category) {
								echo ($expert['expertise'] == $category['category_id'] ? $category['name'] : '');
							}
						?>
					</td>
					<td><?= $expert['email'] ?></td>
					<td>
						<a href="<?= base_url().ADMIN_DIR.'experts/edit/'.$expert['expert_id']; ?>" class="edit" title="Edit"></a>
						<a href="<?= base_url().ADMIN_DIR.'experts/view/'.$expert['expert_id']; ?>" class="view-icon" title="View"></a>
						<a href="<?=base_url().ADMIN_DIR.'experts/delete/'.$expert['expert_id']?>" onclick="return initConfirmDelete()" class="delete" title="Delete"></a>
					</td>
				</tr>
				<?php $no++;} ?>
			</tbody>
		</table>
	</div>
</div>