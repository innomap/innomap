<div class="block article">
	<div class="title">
		<div>Expert Detail</div>
		<div class="clear"></div>
	</div>
	<div class="block-content">
		<div class="content-form" style="background:#c9edf9; width:95%;">
			<table class="form-table detail-experts" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td width="25%">Name</td>
					<td><?= $expert['name']; ?></td>
				</tr>
				<tr>
					<td>Email</td>
					<td><?=  $expert['email']; ?></td>
				</tr>
				<tr>
					<td>Kad No</td>
					<td><?= $expert['kad_no']?></td>
				</tr>
				<tr>
					<td>Expertise</td>
					<td>
						<?php foreach($expertise as $exp){ ?>
							<?= ($expert['expertise'] == $exp['category_id'] ? $exp['name'] : ''); ?>
						<?php } ?>
					</td>
				</tr>
				<tr>
					<td>Address</td>
					<td><?= $expert['address']; ?></td>
				</tr>
				<tr>
					<td>State</td>
					<td>
						<?php foreach($states as $state){ ?>
							<?= ($expert['state_id'] == $state['state_id'] ? $state['name'] : ''); ?>
						<?php } ?>
					</td>
				</tr>
				<tr>
					<td>Organization Address</td>
					<td><?= $expert['organization_address']; ?></td>
				</tr>
				<tr>
					<td>Office Phone Number</td>
					<td><?= $expert['office_phone_no']; ?></td>
				</tr>
				<tr>
					<td>Home Phone Number</td>
					<td><?= $expert['home_phone_no']; ?></td>
				</tr>
				<tr>
					<td>Mobile Phone Number</td>
					<td><?= $expert['mobile_phone_no']; ?></td>
				</tr>
				<tr>
					<td>Highest Education</td>
					<td><?= $expert['highest_education']; ?></td>
				</tr>
				<tr>
					<td>Bank Account</td>
					<td><?= $expert['bank_account']; ?></td>
				</tr>
				<tr>
					<td>Account No</td>
					<td><?= $expert['account_no']; ?></td>
				</tr>
				<tr>
					<td>Picture</td>
					<td>
						<?php if($expert['picture'] != NULL){ ?>
							<img src="<?= base_url().PATH_TO_EXPERT_PICTURE.$expert['picture']?>" width="150px"/>
						<?php } ?>
					</td>
				</tr>
			</table>
		</div>

		<div class="content-form" style="width:90%;">	
			<div style="float:right">
				<a href="<?= base_url().ADMIN_DIR.'experts';?>"><input type="button" class="button" value="Back" /></a>
			</div>
		</div>
	</div>
</div>