<div class="block homepage_content">
	<div class="title">
		<div style="">Homepage Content Form</div>
		<div class="clear"></div>
	</div>
	<div class="block-content">
		<form action="<?= base_url().ADMIN_DIR.'homepage_contents/save' ?>" method="post">
			<div class="content-form" style="background:#efeec7; width:95%;">	

				<input type="hidden" name="homepage_content_id" value="<?= ($mode != 'ADD' ? $content['homepage_content_id'] : '') ?>">

				<table class="form-table">
					<tr>
						<td>Position</td>
						<td><input type="text" name="position" value="<?= ($mode != 'ADD' ? $content['position'] : '') ?>" readonly /></td>
					</tr>
					<tr>
						<td>Type</td>
						<td>
							<?php foreach($type as $key => $value){ ?>
								<input type="radio" name="type" class="opt-type" value="<?= $key ?>" <?= ($key != 0 ? ($key == $content['type'] ? 'checked' : ($key == HOMEPAGE_CONTENT_ARTICLE ? 'checked' : '')) : ($key == HOMEPAGE_CONTENT_ARTICLE ? 'checked' : '')); ?>><?= $value; ?>
							<?php } ?>
						</td>
					</tr>
					<tr>
						<td>Content Type</td>
						<td>
							<a href="#" class="open-article-list hp-form-link" ctype="<?= HOMEPAGE_CONTENT_ARTICLE; ?>">Select Article</a>
							<div class="clear"></div>
							<a href="#" class="open-category-list hp-form-link" ctype="<?= HOMEPAGE_CONTENT_CATEGORY; ?>">Select Category</a>
							<div class="clear"></div>
							<a href="#" class="open-innovation-list hp-form-link" ctype="<?= HOMEPAGE_CONTENT_INNOVATION; ?>">Select Innovation</a>
						</td>
					</tr>
					<tr>
						<td>Content</td>
						<td>
							<input type="hidden" name="content_id" id="content-id" value="<?= ($mode == 'EDIT' ? $content['content_id'] : ''); ?>"/>
							<div id="selected-content">
								<?php if(array_key_exists('content', $content)){ ?>
									<a href="#" class="detail-content hp-form-link" url="<?= $content['type']."/".$content['content']['id'] ?>"><?= $content['content']['title']; ?></a>
									<div class="clear"></div>
								<?php } ?>
							</div>
						</td>
					</tr>
				</table>				
			</div>
			<div class="content-form" style="width:90%;">	
				<div style="float:right">
					<input type="submit" name="submit" value="Save" class="button" />
					<a href="<?= base_url().ADMIN_DIR.'homepage_contents' ?>"><input type="button" class="button" value="Back" /></a>
				</div>
			</div>
		</form>
	</div>
</div>

<div id="content-list-dialog"></div>
<div id="content-detail-dialog"></div>