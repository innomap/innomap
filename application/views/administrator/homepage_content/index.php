<div class="block homepage_content">
	<div class="title">
		<div style="">Homepage Content</div>
		<div class="clear"></div>
	</div>
	<div class="block-content">
		<table cellpadding="0" cellspacing="0" border="0" class="display orange-green-table" width="100%" id="homepage_content_table">
			<thead>
				<th>No.</th>
				<th>Position</th>
				<th>Content Type</th>
				<th>Content</th>
				<th>Action</th>
			</thead>
			<tbody>
				<?php $no=1; foreach($contents as $content){ ?>
					<tr>
						<td style="text-align:center;"><b><?= $no; ?></b></td>
						<td style="text-align:center;"><?= $content['position']; ?></td>
						<td><?= (array_key_exists($content['type'], $type) ? $type[$content['type']] : ''); ?></td>
						<td><?= (array_key_exists('content', $content) ? $content['content']['title'] : ''); ?></td>
						<td><a href="<?= base_url().'administrator/homepage_contents/edit/'.$content['homepage_content_id'] ?>" class="edit" title="Edit"></a></td>
					</tr>
				<?php $no++;} ?>
			</tbody>
		</table>
	</div>
</div>
	