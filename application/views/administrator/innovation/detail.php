<div class="block innovation">
	<div class="title">
		<div><?= lang('title_detail_innovation') ?></div>
		<div class="clear"></div>
	</div>
	<div class="block-content">
		<div class="content-form" style="background:#efeec7; width:95%;">
			<div class="title">
				<div><?= lang('label_in_bahasa_melayu') ?></div>
				<div class="clear"></div>
			</div>
			<table class="detail-inno" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td width="40%"><?= lang('label_data_source') ?></td>
					<td><?=($detail['data_source'] != 0 ? $data_sources[$detail['data_source']] : '-')?></td>
				</tr>
				<tr>
					<td width="40%"><?= lang('label_innovation_name') ?></td>
					<td><?=$detail['name_in_melayu']?></td>
				</tr>
				<tr>
					<td><?= lang('label_inspiration') ?></td>
					<td><?=$detail['inspiration_in_melayu']?></td>
				</tr>
				<tr>
					<td><?= lang('label_product_description') ?></td>
					<td><?=$detail['description_in_melayu']?></td>
				</tr>
				<tr>
					<td><?= lang('label_material') ?></td>
					<td><?=$detail['materials_in_melayu']?></td>
				</tr>
			</table>

			<div class="title">
				<div>In English</div>
				<div class="clear"></div>
			</div>

			<table class="detail-inno" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td width="40%"><?= lang('label_innovation_name') ?></td>
					<td><?=$detail['name']?></td> 
				</tr>
				<tr>
					<td><?= lang('label_inspiration') ?></td>
					<td><?=$detail['inspiration']?></td>
				</tr>
				<tr>
					<td><?= lang('label_product_description') ?></td>
					<td><?=$detail['description']?></td>
				</tr>
				<tr>
					<td><?= lang('label_material') ?></td>
					<td><?=$detail['materials']?></td>
				</tr>
				<tr>
					<td><?= lang('label_created_date') ?></td>
					<td><?=$detail['created_date']?></td>
				</tr>
				<tr>
					<td><?= lang('label_manufacturing_cost') ?></td>
					<td>RM <?=$detail['manufacturing_costs']?></td>
				</tr>
				<tr>					
					<td><?= lang('label_how_to_use') ?></td>
					<td><?=$detail['how_to_use']?></td>
				</tr>
				<tr>
					<td><?= lang('label_innovation_category') ?></td>
					<td>
						<?php 
							foreach($inno_cats as $key=>$cat){
								foreach($categories as $category){ 
									echo ($cat['category_id'] == $category['category_id'] ? $key+1 .') '.$category['name'].'<br/>' : '');
								} ?>
								<p>Subcategory:</p>
								<?php 
								foreach($cat['subcategories'] as $sub){
									foreach($inno_subs as $inno_sub){ 
										echo ($inno_sub['subcategory_id'] == $sub['subcategory_id'] ? '-'.$sub['name'].'<br/>' : ''); 
									}
								}
							} ?>
					</td>
				</tr>
				<tr>
					<td><?= lang('label_target') ?></td>
					<td><?php 
								if(json_decode($detail['target']) != NULL){
									foreach(json_decode($detail['target']) as $cur_target){
										echo "- ".$targets[$cur_target]."<br/>";
									}
								}?>
					</td>
				</tr>
				<tr>
					<td><?= lang('label_myipo_protection') ?></td>
					<td><?=($detail['myipo_protection'] == 1 ? lang('value_yes') : lang('value_no'))?></td>
				</tr>
				<tr>
					<td><?= lang('label_special_achievement') ?> </td>
					<td><?=$detail['special_achievement']?></td>
				</tr>
				<tr>
					<td><?= lang('label_innovation_picture') ?> : </td>
					<td>
						<?php foreach($inno_pics as $pic){?>
							<img src="<?=base_url().PATH_TO_INNOVATION_PICTURE."/".$pic['picture']?>" width="200"/>
						<?php } ?>
					</td>
				</tr>
			</table>
		</div>
		<div class="content-form" style="width:90%;">	
				<div style="float:right">
				<a onclick="history.back();"><input type="button" class="button" value="<?= lang('button_back') ?>" /></a>
			</div>
		</div>
	</div>
</div>
