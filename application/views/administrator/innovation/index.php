<div class="block innovation">
	<div class="title">
		<div>Innovation</div>
		<div class="clear"></div>
	</div>
	<div class="block-content">
		<table cellpadding="0" cellspacing="0" border="0" class="display light-orange-table" width="100%" id="innovation-list">
			<thead>
				<th>No.</th>
				<th>Name</th>
				<th>Description</th>
				<th>Score Average</th>
				<th>Action</th>
			</thead>
			<tbody>
				<?php $no=1;foreach($innovations as $row){ ?>
					<tr>
						<td><?= $no; ?></td>
						<td><?= $row['name'] ?></td>
						<td><?= $row['description']; ?></td>
						<td><?= $row['score_average']; ?></td>
						<td>
							<a href="#" class="set-featured" content-id="<?= $row['innovation_id'] ?>"><img src="<?= base_url().'assets/img/'.($row['is_featured'] > 0 ? 'star.png' : 'star_none.png') ?>"></a>
							<a href="#" count-top="<?= $count_top_innovation; ?>" title="Top Innovation" class="set-top-innovation" content-id="<?= $row['innovation_id'] ?>" top-innovation="<?= $row['top_innovation'] > 0 ? '0' : '1' ?>"><img src="<?= base_url().'assets/img/'.($row['top_innovation'] > 0 ? 'like_full.png' : 'like.png') ?>"></a>
							<a href="<?= base_url().ADMIN_DIR.'innovations/view/'.$row['innovation_id'] ?>" title="View Detail" class="view-icon"></a>
						</td>
					</tr>
				<?php $no++;} ?>
			</tbody>
		</table>
	</div>
</div>