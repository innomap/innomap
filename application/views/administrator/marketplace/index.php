<div class="block innovation">
	<div class="title">
		<div>Innovation</div>
		<div class="clear"></div>
	</div>
	<div class="block-content">
		<?php if($message != NULL){ ?>
			<br/><div class="form-info success"><?= $message; ?></div>
		<?php } ?>
		<table cellpadding="0" cellspacing="0" border="0" class="display light-orange-table" width="100%" id="article_table">
			<thead>
				<th>No.</th>
				<th>Name</th>
				<th>Description</th>
				<th>Action</th>
			</thead>
			<tbody>
				<?php $no=1;foreach($innovations as $row){ ?>
					<tr>
						<td><?= $no; ?></td>
						<td><?= $row['name_in_melayu'] ?></td>
						<td><?= $row['description_in_melayu']; ?></td>
						<td>
							<?php if($row['is_mp_product'] == 0){ ?>
								<a href="<?= base_url().ADMIN_DIR.'marketplace/add/'.$row['innovation_id'] ?>" content-id="<?= $row['innovation_id'] ?>" title="Add to marketplace" class="ic-add-to-cart"></a>
							<?php }else{ ?>
								<a href="<?= base_url().ADMIN_DIR.'marketplace/disabled/'.$row['innovation_id'] ?>" content-id="<?= $row['innovation_id'] ?>" title="Delete from marketplace" class="ic-delete-from-cart"></a>
							<?php } ?>
							<a href="<?= base_url().ADMIN_DIR.'innovations/view/'.$row['innovation_id'] ?>" title="View Detail" class="view-icon"></a>
						</td>
					</tr>
				<?php $no++;} ?>
			</tbody>
		</table>
	</div>
</div>