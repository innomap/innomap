<div class="block menu" style="float:none !important;">
	<div class="title">
		<div><?= $mode; ?> Social Media</div>
	</div>
	<div class="block-content">
		<form id="form-social-media" method="post" action="<?=site_url(ADMIN_DIR."marketplace_social_medias/save")?>" enctype="multipart/form-data">	
			<div class="content-form" style="background:#c9edf9; width:90%;">	
				<input type="hidden" name="mode" value="<?=$mode?>" />
				<?php if($mode == 'EDIT'){ ?>
					<input type="hidden" name="id_social_media" value="<?=$detail['id_social_media']?>" />
				<?php } ?>
				<table class="form-table">
					<tr>
						<td>Name</td>
						<td><input type="text" name="name" value="<?=($mode != 'ADD' ? $detail['name'] : ""); ?>" /></td>
					</tr>
					<tr>
						<td>Icon</td>
						<td>
							<?php if($mode != "ADD"){
								if($detail['icon'] != ""){ ?>
									<img src="<?=base_url().PATH_TO_MARKETPLACE_SOCIAL_MEDIA_ICON."/".$detail['icon']?>" class="innovator-img" width="50" /><br/>
								<?php } ?>
								<input type="hidden" name="bef_picture" value="<?=$detail['icon']?>" />
							<?php } ?>
							<input type="file" name="icon" />
						</td>
					</tr>
					<tr>
						<td>URL</td>
						<td><input type="text" name="url" value="<?=($mode != 'ADD' ? $detail['url'] : ""); ?>" /></td>
					</tr>
				</table>
			</div>
			<div class="content-form" style="width:90%;">	
				<div style="float:right">
					<?php if($mode != 'VIEW'){ ?>
						<input class="button" type="submit" name="save" value="Save" />
					<?php } ?>
					<a href="<?= site_url(ADMIN_DIR.'marketplace_social_medias'); ?>"><input class="button" type="button" value="Back"/></a>
					<div class="clear"></div>
				</div>
			</div>
		</form>
	</div>
</div>