<div class="block menu">
	<div class="title">
		<div style="float:left;">Marketplace Social Media</div>
		<a href="<?= base_url().ADMIN_DIR.'marketplace_social_medias/add'; ?>" class="reset_data_tables">
			<div class="add_button">
				<div>Add New Social Media</div>
			</div>
		</a>
		<div class="clear"></div>
	</div>
	<div class="block-content">

		<table cellpadding="0" cellspacing="0" border="0" class="display blue-table" width="100%" id="menu_table">
			<thead>
				<tr>
					<th width="35%">Name</th>
					<th width="20%">URL</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($social_medias as $social_media){?>
					<tr>
						<td><strong><?=$social_media['name']?></strong></td>
						<td><strong><?=$social_media['url']?></strong></td>
						<td>
							<a href="<?=base_url().ADMIN_DIR.'marketplace_social_medias/edit/'.$social_media['id_social_media']?>" title="Edit" class="edit"></a> | 
							<a href="<?=base_url().ADMIN_DIR.'marketplace_social_medias/delete/'.$social_media['id_social_media']?>" onclick="return initConfirmDelete()" title="Delete" class="delete"></a>
						</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>