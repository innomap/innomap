<div class="block menu" style="float:none !important;">
	<div class="title">
		<div><?= $mode; ?> Menus</div>
	</div>
	<div class="block-content">
		<form id="form-menu" method="post" action="<?=site_url(ADMIN_DIR."menus/save")?>">	
			<div class="content-form" style="background:#c9edf9; width:90%;">	
				<input type="hidden" name="mode" value="<?=$mode?>" />
				<?php if($mode == 'EDIT'){ ?>
					<input type="hidden" name="menu_id" value="<?=$menu['menu_id']?>" />
				<?php } ?>
				<table class="form-table">
					<tr>
						<td>Name</td>
						<td><input type="text" name="name" value="<?=($mode != 'ADD' ? $menu['name'] : ""); ?>" /></td>
					</tr>
					<tr>
						<td>Malay Name</td>
						<td><input type="text" name="melayu_name" value="<?=($mode != 'ADD' ? $menu['name_in_melayu'] : ""); ?>" /></td>
					</tr>
					<tr>
						<td>Parent Menu</td>
						<td>
							<select name="parent_id">
								<option value="0">- No Parent -</option>
								<?php foreach($parents as $parent){ ?>
									<option value="<?=$parent['menu_id']?>" <?=($mode != 'ADD' ? ($parent['menu_id'] == $menu['parent_id'] ? 'selected' : '') : '')?>><?=$parent['name']?></option>
								<?php } ?>
							</select>
						</td>
					</tr>
					<tr>
						<td>URL <span><?=base_url()?></span></td>
						<td><input type="text" name="url" value="<?=($mode != 'ADD' ? $menu['url'] : ""); ?>" /></td>
					</tr>
					<tr>
						<td>Description</td>
						<td><textarea name="description"><?=($mode != 'ADD' ? $menu['description'] : ""); ?></textarea></td>
					</tr>
					<tr>
						<td>Malay Description</td>
						<td><textarea name="description_in_melayu"><?=($mode != 'ADD' ? $menu['description_in_melayu'] : ""); ?></textarea></td>
					</tr>
					<tr>
						<td>Order</td>
						<td><input type="text" name="order" value="<?=($mode != 'ADD' ? $menu['order'] : ""); ?>" /></td>
					</tr>
				</table>
				
				<div id="menu-module">
					<div class="title">
						<div style="float:left;">Menu Module</div>
						<a id="add-module" href="#">
							<div class="add_button">
								<div>Add New Menu Module</div>
							</div>
						</a>
						<div class="clear"></div>
					</div>
					<?php 
					if(count($menu_modules) > 0){ 
						foreach($menu_modules as $key=>$menu_module){ ?>
							<div class="each-module">
								<input type="hidden" name="module_id[]" value="<?=$menu_module['module_id']?>" />
								<input type="text" name="module_name[]" value="<?=$menu_module['name']?>" <?=($key == 0 ? 'readonly' : '')?> /> | url : <?=base_url()?><input type="text" name="module_url[]" value="<?=$menu_module['url']?>" <?=($key == 0 ? 'readonly' : '')?> />
								<?php if($key > 0){ ?> <a href="#" class="remove-module delete" title="Remove Menu Module" module_id="<?=$menu_module['module_id']?>" onclick="return initConfirmDelete()"></a><?php } ?>
							</div>
					<?php }
					}else{ ?>
						<div class="first-module"><input type="text" name="module_name[]" readonly value="view" /> | url : <?=base_url()?><input type="text" name="module_url[]" readonly /></div>
					<?php } ?>
					<div id="added-modules"></div>
				</div>
			</div>
			<div class="content-form" style="width:90%;">	
				<div style="float:right">
					<?php if($mode != 'VIEW'){ ?>
						<input class="button" type="submit" name="save" value="Save" />
					<?php } ?>
					<a href="<?= site_url(ADMIN_DIR.'menus'); ?>"><input class="button" type="button" value="Back"/></a>
					<div class="clear"></div>
				</div>
			</div>
		</form>
	</div>
</div>