<div class="block menu">
	<div class="title">
		<div style="float:left;">Menu</div>
		<a href="<?= base_url().ADMIN_DIR.'menus/add'; ?>" class="reset_data_tables">
			<div class="add_button">
				<div>Add New Menu</div>
			</div>
		</a>
		<div class="clear"></div>
	</div>
	<div class="block-content">

		<table cellpadding="0" cellspacing="0" border="0" class="display blue-table" width="100%" id="menu_table">
			<thead>
				<tr>
					<th width="35%">Menu Name</th>
					<th width="20%">Menu URL</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($menus as $menu){?>
					<tr>
						<td><strong><?=($lang == 'melayu' ? $menu['name_in_melayu'] : $menu['name'])?></strong></td>
						<td>
							<?php if($menu['url'] != ""){ ?>
								<a href="<?=base_url().$menu['url']?>"><?=base_url().$menu['url']?></a>
							<?php }else{ ?>
								-No URL-
							<?php } ?>
						</td>
						<td>
							<a href="<?=base_url().ADMIN_DIR.'menus/edit/'.$menu['menu_id']?>" title="Edit" class="edit"></a> | 
							<a href="<?=base_url().ADMIN_DIR.'menus/delete/menu/'.$menu['menu_id']?>" onclick="return initConfirmDelete()" title="Delete" class="delete"></a>
						</td>
					</tr>
					<?php 
						if(count($menu['childs']) > 0){ 
							foreach($menu['childs'] as $child){ ?>
								<tr>
									<td><?=($lang == 'melayu' ? $child['name_in_melayu'] : $child['name'])?></td>
									<td><a href="<?=base_url().$child['url']?>"><?=base_url().$child['url']?></a></td>
									<td>
										<a href="<?=base_url().ADMIN_DIR.'menus/edit/'.$child['menu_id']?>" class="edit" title="Edit"></a> | 
										<a href="<?=base_url().ADMIN_DIR.'menus/delete/menu/'.$child['menu_id']?>" onclick="return initConfirmDelete()" class="delete" title="Delete"></a>
									</td>
								</tr>
					<?php 	}
						}
					?>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>