<div class="logo grid_4">
	<a href="<?=base_url().ADMIN_DIR?>"><img src="<?= base_url()?>assets/img/logo.png" class="grid_4 alpha omega"/></a>
	<div class="clear"></div>
</div>
<?php if($this->is_logged_in){ ?>

	<div class="setting grid_7">
		<ul>
			<li>
				<a href="#">Welcome, <?= $user['username']; ?></a> |
			</li>
			<li>
				<a href="<?= base_url().ADMIN_DIR.'site/logout'; ?>" class="upper reset_data_tables">Logout</a>
			</li>
		</ul>
	</div>

<?php } ?>
	<div class="clear"></div>
	<div class="menu grid_12 alpha omega" style="<?= !$this->is_logged_in ? 'height:30px;' : ''; ?>">
		<?php if($this->is_logged_in){ ?>
			<label for="show-menu" class="show-menu">Show Menu  &#9776;</label>
			<ul id="menu">
				<li style="border:none;">
					<a href="<?= base_url().ADMIN_DIR; ?>"><div class="home_icon">&nbsp;</div></a>
				</li>
				<!--menu-->
				<li><a href="<?= base_url().ADMIN_DIR."menus"?>" class="reset_data_tables">Menus</a></li>
				<li><a href="<?= base_url().ADMIN_DIR."roles"?>" class="reset_data_tables">Roles</a></li>
				<li><a href="<?= base_url().ADMIN_DIR."users"?>" class="reset_data_tables">Users</a></li>
				<li><a href="<?= base_url().ADMIN_DIR."articles"?>" class="reset_data_tables">Articles</a></li>
				<li><a href="<?= base_url().ADMIN_DIR."article_categories"?>" class="reset_data_tables">Article Categories</a></li>
				<li><a href="<?= base_url().ADMIN_DIR."homepage_contents"?>" class="reset_data_tables">Homepage Content</a></li>
				<li><a href="<?= base_url().ADMIN_DIR."innovations"?>" class="reset_data_tables">Innovations</a></li>
				<li><a href="<?= base_url().ADMIN_DIR."experts"?>" class="reset_data_tables">Experts</a></li>
				<li><a href="<?= base_url().ADMIN_DIR."universities"?>" class="reset_data_tables">Universities</a></li>
				<li><a href="<?= base_url().ADMIN_DIR."commercials"?>" class="reset_data_tables">Commercial Center</a></li>
				<li><a href="<?= base_url().ADMIN_DIR."portfolio_criterias"?>" class="reset_data_tables">Portfolio Criterias</a></li>
				<li><a href="<?= base_url().ADMIN_DIR."projects"?>" class="reset_data_tables">Project Listing</a></li>
				<li><a href="<?= base_url().ADMIN_DIR."marketplace"?>" class="reset_data_tables">Marketplace</a></li>
				<li><a href="<?= base_url().ADMIN_DIR."marketplace_social_medias"?>" class="reset_data_tables">Marketplace Social Media</a></li>
			</ul>
		<?php } ?>
	</div>
	<div class="clear"></div>