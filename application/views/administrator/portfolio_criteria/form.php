<div class="block article">
	<div class="title">
		<div><?= $mode ?> Criteria</div>
		<div class="clear"></div>
	</div>
	<div class="block-content">
		<form action="<?= base_url().ADMIN_DIR."portfolio_criterias/save"; ?>" name="form_portfolio_criteria" id="form-portfolio-criteria" method="post">
			<div class="content-form" style="background:#c9edf9; width:95%;">	
				<input type="hidden" name="mode" value="<?= $mode; ?>">
				<input type="hidden" name="criteria_id" value="<?= ($mode == 'EDIT' ? $id : ''); ?>">

				<table class="form-table">
					<tr>
						<td width="25%">Name</td>
						<td><input type="text" name="name_in_melayu" value="<?= ($mode != 'ADD' ? $criteria['name_in_melayu'] : '') ?>" <?= $mode == 'VIEW' ? 'readonly' : '' ?>></td>
					</tr>									
					<tr>
						<td>Description</td>
						<td><textarea name="description_in_melayu" <?= $mode == 'VIEW' ? 'disabled' : '' ?>><?= ($mode != 'ADD' ? $criteria['description_in_melayu'] : '') ?></textarea></td>
					</tr>
					<tr>
						<td>Position</td>
						<td>
							<select name="position">
								<?php for($i=$position; $i > 0;$i--) { ?>
									<option value="<?= $i ?>" <?= $mode != 'ADD' ? $criteria['position'] == $i ? 'selected' : '' : '' ?>><?= $i; ?></option>
								<?php } ?> 
							</select>
							<input type="hidden" name="current_position" value="<?= ($mode == 'EDIT' ? $criteria['position'] : '') ?>">
							<input type="hidden" name="last_position" value="<?= ($mode == 'ADD' ? $position : '') ?>">
						</td>
					</tr>
				</table>
			</div>
			<div class="content-form" style="width:90%;">	
				<div style="float:right">
					<?php if($mode != 'VIEW'){ ?>
						<input type="submit" name="submit" value="Save" class="button" />
					<?php } ?>
					<a href="<?= base_url().ADMIN_DIR."portfolio_criterias"; ?>"><input type="button" class="button" value="Back" /></a>
				</div>
			</div>
		</form>
	</div>
</div>