<div class="block article">
	<div class="title">
		<div style="float:left;">Portfolio Criterias</div>
		<a href="<?= base_url().ADMIN_DIR.'portfolio_criterias/add'; ?>" class="reset_data_tables">
			<div class="add_button">
				<div>Add Portfolio Criteria</div>
			</div>
		</a>
		<div class="clear"></div>
	</div>
	<div class="block-content">
		<?php if($message != NULL){ ?>
			<br/><div class="form-info success"><?= $message; ?></div>
		<?php } ?>
		<table cellpadding="0" cellspacing="0" border="0" class="display blue-table" width="100%" id="article_table">
			<thead>
				<th>No</th>
				<th>Name</th>
				<th>Description</th>
				<th>Position</th>
				<th width="15%">Action</th>
			</thead>
			<tbody>
				<?php $no=1; foreach($criterias as $key=>$value){ ?>
				<tr>
					<td><?= $no; ?></td>
					<td><?= $value['name_in_melayu']; ?></td>
					<td><?= substr($value['description_in_melayu'],0,100)."..."; ?></td>
					<td><?= $value['position'] ?></td>
					<td>
						<a href="<?= base_url().ADMIN_DIR.'portfolio_criterias/edit/'.$value['criteria_id']; ?>" title="Edit" class="edit"></a>
						<a href="<?= base_url().ADMIN_DIR.'portfolio_criterias/view/'.$value['criteria_id']; ?>" title="View" class="view-icon"></a>
						<a href="<?= base_url().ADMIN_DIR.'portfolio_criterias/delete/'.$value['criteria_id'].'/'.$value['position']; ?>" onClick="return initConfirmDelete()" title="Delete" class="delete"></a>
					</td>
				</tr>
				<?php $no++;} ?>
			</tbody>
		</table>
	</div>
</div>