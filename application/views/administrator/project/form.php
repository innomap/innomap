<div class="block article">
	<div class="title">
		<div><?= $mode ?> Project</div>
		<div class="clear"></div>
	</div>
	<div class="block-content">
		<form action="<?= base_url().ADMIN_DIR."projects/save"; ?>" name="form_project" id="form-project" method="post">
			<div class="content-form" style="background:#c9edf9; width:95%;">	
				<input type="hidden" name="mode" value="<?= $mode; ?>">
				<input type="hidden" name="project_id" value="<?= ($mode == 'EDIT' ? $id : ''); ?>">

				<table class="form-table">
					<tr>
						<td width="25%">Title</td>
						<td><textarea name="title" <?= $mode == 'VIEW' ? 'disabled' : '' ?>><?= ($mode != 'ADD' ? $project['title'] : '') ?></textarea></td>
					</tr>	
					<tr>
						<td width="25%">Ref No</td>
						<td><input type="text" name="ref_no" value="<?= ($mode != 'ADD' ? $project['ref_no'] : '') ?>" <?= $mode == 'VIEW' ? 'readonly' : '' ?>></td>
					</tr>
					<tr>
						<td width="25%">Beneficiary Community</td>
						<td><input type="text" name="beneficiary_community" value="<?= ($mode != 'ADD' ? $project['beneficiary_community'] : '') ?>" <?= $mode == 'VIEW' ? 'readonly' : '' ?>></td>
					</tr>	
					<tr>
						<td width="25%">Lead Agency</td>
						<td><input type="text" name="lead_agency" value="<?= ($mode != 'ADD' ? $project['lead_agency'] : '') ?>" <?= $mode == 'VIEW' ? 'readonly' : '' ?>></td>
					</tr>								
					<tr>
						<td width="25%">Collaborator</td>
						<td><input type="text" name="collaborator" value="<?= ($mode != 'ADD' ? $project['collaborator'] : '') ?>" <?= $mode == 'VIEW' ? 'readonly' : '' ?>></td>
					</tr>
					<tr>
						<td width="25%">Category</td>
						<td><input type="text" name="category" value="<?= ($mode != 'ADD' ? $project['category'] : '') ?>" <?= $mode == 'VIEW' ? 'readonly' : '' ?>></td>
					</tr>
					<tr>
						<td>Address</td>
						<td><textarea name="address" class="address-geo" <?= $mode == 'VIEW' ? 'disabled' : '' ?>><?= ($mode != 'ADD' ? $project['address'] : '') ?></textarea></td>
					</tr>
					<tr>
						<td width="25%">State</td>
						<td>
							<select name="state_id" <?= $mode == 'VIEW' ? 'disabled' : '' ?>>
								<?php foreach ($states as $key => $value) { ?>
									<option value="<?= $value['state_id'] ?>" <?= isset($project) ? $project['state_id'] == $value['state_id'] ? 'selected' : '' : '' ?>><?= $value['name'] ?></option>	
								<?php } ?>
							</select>
						</td>
					</tr>
					<tr>
						<td width="25%">Geo Location</td>
						<td><input type="text" name="geo_location" id="geo-location" placeholder="latitude,longitude" value="<?= ($mode != 'ADD' ? $project['geo_location'] : '') ?>" <?= $mode == 'VIEW' ? 'readonly' : '' ?>></td>
					</tr>
					<tr>
						<td width="25%">Duration (Month)</td>
						<td><input type="text" name="duration" value="<?= ($mode != 'ADD' ? $project['duration'] : '') ?>" <?= $mode == 'VIEW' ? 'readonly' : '' ?>></td>
					</tr>
					<tr>
						<td width="25%">Amount</td>
						<td><input type="text" name="amount" value="<?= ($mode != 'ADD' ? $project['amount'] : '') ?>" <?= $mode == 'VIEW' ? 'readonly' : '' ?>></td>
					</tr>
				</table>
			</div>
			<div class="content-form" style="width:90%;">	
				<div style="float:right">
					<?php if($mode != 'VIEW'){ ?>
						<input type="submit" name="submit" value="Save" class="button" />
					<?php } ?>
					<a href="<?= base_url().ADMIN_DIR."projects"; ?>"><input type="button" class="button" value="Back" /></a>
				</div>
			</div>
		</form>
	</div>
</div>