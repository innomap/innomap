<div class="block article">
	<div class="title">
		<div style="float:left;">Project Listing</div>
		<a href="<?= base_url().ADMIN_DIR.'projects/add'; ?>" class="reset_data_tables">
			<div class="add_button">
				<div>Add Project</div>
			</div>
		</a>
		<div class="clear"></div>
	</div>
	<div class="block-content">
		<?php if($message != NULL){ ?>
			<br/><div class="form-info success"><?= $message; ?></div>
		<?php } ?>
		<table cellpadding="0" cellspacing="0" border="0" class="display blue-table" width="100%" id="article_table">
			<thead>
				<th>No</th>
				<th>Title</th>
				<th>Ref No</th>
				<th>Beneficiary Community</th>
				<th width="15%">Action</th>
			</thead>
			<tbody>
				<?php $no=1; foreach($projects as $key=>$value){ ?>
				<tr>
					<td><?= $no; ?></td>
					<td><?= $value['title']; ?></td>
					<td><?= $value['ref_no']; ?></td>
					<td><?= $value['beneficiary_community'] ?></td>
					<td>
						<a href="<?= base_url().ADMIN_DIR.'projects/edit/'.$value['project_id']; ?>" title="Edit" class="edit"></a>
						<a href="<?= base_url().ADMIN_DIR.'projects/view/'.$value['project_id']; ?>" title="View" class="view-icon"></a>
						<a href="<?= base_url().ADMIN_DIR.'projects/delete/'.$value['project_id']; ?>" onClick="return initConfirmDelete()" title="Delete" class="delete"></a>
					</td>
				</tr>
				<?php $no++;} ?>
			</tbody>
		</table>
	</div>
</div>