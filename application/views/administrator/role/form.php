<div class="block role">
	<div class="title">
		<div><?=$mode ?> Roles</div>
	</div>
	<div class="block-content">
		<form id="form-role" method="post" class="biz-form" action="<?=site_url(ADMIN_DIR."roles/save")?>">
			<div class="content-form" style="background:#efeec7; width:95%;">	
				<?php if($mode == 'EDIT'){ ?>
					<input type="hidden" name="role_id" value="<?=$role['role_id']?>" />
				<?php } ?>
				<input type="hidden" name="mode" value="<?=$mode?>">
				
				<table class="form-table">
					<tr>
						<td>Name</td>
						<td><input type="text" name="name" value="<?=($mode != 'ADD' ? $role['name'] : ""); ?>" /></td>
					</tr>
					<tr>
						<td>Malay Name</td>
						<td><input type="text" name="name_in_melayu" value="<?=($mode != 'ADD' ? $role['name_in_melayu'] : ""); ?>" /></td>
					</tr>
					<tr>
						<td>Description</td>
						<td><textarea name="description"><?= ($mode != 'ADD' ? $role['description'] : ""); ?></textarea></td>
					</tr>
					<tr>
						<td>Malay Description</td>
						<td><textarea name="description_in_melayu"><?= ($mode != 'ADD' ? $role['description_in_melayu'] : ""); ?></textarea></td>
					</tr>
				</table>				
				<div> 
					<table class="role-list" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<th>Menu Name</th>
							<th>Modules</th>
						</tr>
						<?php foreach($menus as $menu){ ?>
							<tr>
								<td><?=$menu['name']?></td>
								<td><?php foreach($menu['module'] as $module){ ?>
										<input type="checkbox" value="<?=$module['module_id']?>" name="module_id[]" <?=($module['is_accessable'] ? 'checked' : '')?>><?=$module['name']?></input>
									<?php } ?>
								</td>
							</tr>
						<?php } ?>
					</table>
				</div>
			</div>
			
			<div class="content-form" style="width:90%;">	
				<div style="float:right">
					<?php if($mode != 'VIEW'){ ?>
						<input class="button" type="submit" name="save" value="Save" />
					<?php } ?>
					<a href="<?= site_url(ADMIN_DIR.'roles'); ?>"><input class="button" type="button" value="Back"/></a>
				</div>
			</div>
		</form>
	</div>
</div>