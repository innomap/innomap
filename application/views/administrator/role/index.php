<div class="block role">
	<div class="title">
		<div style="float:left;">Role</div>
			<a href="<?= site_url(ADMIN_DIR.'roles/add'); ?>" class="reset_data_tables">
				<div class="add_button">
					<div>Add New Role</div>
				</div>
			</a>
		<div class="clear"></div>
	</div>
	<div class="block-content content_role">
		<table cellpadding="0" cellspacing="0" border="0" class="display orange-green-table" width="100%">
			<thead>
				<tr>
					<th rowspan="2" style="vertical-align:middle">Role</th>
					<th colspan="<?=count($menus)?>" style="border-bottom:1px solid #c1c130;">Menu</th>
				</tr>
				<tr>
				<?php foreach($menus as $menu){ ?>
					<th style="padding:10px 5px 5px 5px;"><?=($lang == 'melayu' ? $menu['name_in_melayu'] : $menu['name'])?></th>
				<?php } ?>
				</tr>
			</thead>
			<tbody>
				<?php foreach($roles as $role){ ?>
				<tr>
					<td style="text-align:center;">
						<b><?=$role['name']?></b><div class="clear"></div>
						<a href="<?= site_url(ADMIN_DIR.'roles/edit/'.$role['role_id']); ?>" class="edit" title="Edit"></a> |
						<a onclick="return initConfirmDelete()" href="<?= site_url(ADMIN_DIR.'roles/delete/'.$role['role_id']); ?>" class="delete" title="Delete"></a>
					</td>
					<?php foreach($role['access_role'] as $access){ ?>
						<td><center><?=($access != "" ? $access : '<div class="no_access" title="No Access">&nbsp;</div>')?></center></td>
					<?php } ?>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</div>