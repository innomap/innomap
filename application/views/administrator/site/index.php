<div class="grid_12 wrap_top_hp alpha omega">
	<a href="<?= base_url(). ADMIN_DIR . "menus" ?>">
		<div class="hp_left grid_4 alpha hp_menu">
			<span>MENUS</span>
		</div>
	</a>
	<div class="hp_right grid_8 alpha omega">
		<a href="<?= base_url(). ADMIN_DIR . "roles" ?>">
			<div class="grid_4 alpha omega hp_role"><span>ROLES</span></div>
		</a>
		<a href="<?= base_url(). ADMIN_DIR . "users" ?>">
			<div class="grid_4 omega hp_user"><span>USERS</span></div>
		</a>
		<div class="clear"></div>
		<a href="<?= base_url(). ADMIN_DIR . "articles" ?>">
			<div class="grid_8 alpha omega hp_article"><span>ARTICLE</span></div>
		</a>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
	<a href="<?= base_url(). ADMIN_DIR . "article_categories" ?>">
		<div class="grid_8 alpha omega hp_article_cat"><span>ARTICLE CATEGORY<span></div>
	</a>
	<a href="<?= base_url(). ADMIN_DIR . "innovations" ?>">
		<div class="grid_4 omega hp_innovation"><span>INNOVATIONS</span></div>
	</a>
	<div class="clear"></div>
	<a href="<?= base_url(). ADMIN_DIR . "experts" ?>">
		<div class="grid_4 alpha omega hp_expert"><span>EXPERT</span></div>
	</a>
	<a href="<?= base_url(). ADMIN_DIR . "homepage_contents" ?>">
		<div class="grid_8 omega hp_hp_content"><span>HOMEPAGE CONTENT</span></div>
	</a>
	<div class="clear"></div>
</div>