<div class="block article">
	<div class="title">
		<div><?= $mode ?> University</div>
		<div class="clear"></div>
	</div>
	<div class="block-content">
		<form action="<?= base_url().ADMIN_DIR."universities/save"; ?>" id="form-university" method="post">
			<div class="content-form" style="background:#c9edf9; width:95%;">	
				<input type="hidden" name="mode" value="<?= $mode; ?>">
				<input type="hidden" name="university_id" value="<?= ($mode == 'EDIT' ? $university['university_id'] : ''); ?>">
				<table class="form-table">
					<tr>
						<td width="25%">Name</td>
						<td><input type="text" name="university_name" value="<?= ($mode == 'EDIT' ? $university['university_name'] : '') ?>"></td>
					</tr>									
					<tr>
						<td>Address</td>
						<td><textarea class="address-geo" name="address"><?= ($mode == 'EDIT' ? $university['address'] : '') ?></textarea></td>
					</tr>
					<tr>
						<td>Specialty</td>
						<td>
							<select name="specialty">
								<?php foreach($specialties as $specialty){ ?>
									<option value="<?= $specialty['category_id']; ?>" <?= ($mode == 'EDIT' ? ($university['specialty'] == $specialty['category_id'] ? 'selected' : '') : '') ?>><?= $specialty['name']; ?></option>
								<?php } ?>
							</select>
						</td>
					</tr>
					<tr>
						<td>State</td>
						<td>
							<select name="state">
								<?php foreach ($states as $key => $state) { ?>
									<option value="<?= $state['state_id']; ?>" <?= ($mode == 'EDIT' ? ($university['state'] == $state['state_id'] ? 'selected' : '' ) : '') ?>><?= $state['name']; ?></option>  
								<?php } ?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Contact Number</td>
						<td><input type="text" name="contact_no" value="<?= ($mode == 'EDIT' ? $university['contact_no'] : '') ?>"></td>
					</tr>
					<tr>
						<td>Geo Location</td>
						<td>
							<input type="text" id="geo-location" name="geo_location" value="<?= ($mode == 'EDIT' ? $university['geo_location'] : '') ?>">
							<a target="_blank" href="http://universimmedia.pagesperso-orange.fr/geo/loc.htm">Find Geo Location</a>
						</td>
					</tr>
				</table>
			</div>
			<div class="content-form" style="width:90%;">	
				<div style="float:right">
					<input type="submit" value="Save" class="button" />
					<a href="<?= base_url().ADMIN_DIR."universities"; ?>"><input type="button" class="button" value="Back" /></a>
				</div>
			</div>
		</form>
	</div>
</div>