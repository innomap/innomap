<div class="block article">
	<div class="title">
		<div style="float:left;">Universities</div>
		<a href="<?= base_url().ADMIN_DIR.'universities/add'; ?>" class="reset_data_tables">
			<div class="add_button">
				<div>Add University</div>
			</div>
		</a>
		<div class="clear"></div>
	</div>
	<div class="block-content">
		<?php if($message != NULL){ ?>
			<br/><div class="form-info success"><?= $message; ?></div>
		<?php } ?>
		<table cellpadding="0" cellspacing="0" border="0" class="display blue-table" width="100%" id="experts_table">
			<thead>
				<th>No</th>
				<th>University Name</th>
				<th>Address</th>
				<th>State</th>
				<th>Specialty</th>
				<th>Contact No.</th>
				<th>Action</th>
			</thead>
			<tbody>
				<?php $no=1; foreach($universities as $university){ ?>
				<tr>
					<td><?= $no; ?></td>
					<td><?= $university['university_name']; ?></td>
					<td><?= $university['address']; ?></td>
					<td><?= $university['state_name']?></td>
					<td><?= $university['category_name']?></td>
					<td><?= $university['contact_no']?></td>
					<td>
						<a href="<?= base_url().ADMIN_DIR.'universities/edit/'.$university['university_id']; ?>" class="edit" title="Edit"></a>
						<a href="<?= base_url().ADMIN_DIR.'universities/view/'.$university['university_id']; ?>" class="view-icon" title="View"></a>
						<a href="<?=base_url().ADMIN_DIR.'universities/delete/'.$university['university_id']?>" onclick="return initConfirmDelete()" class="delete" title="Delete"></a>
					</td>
				</tr>
				<?php $no++;} ?>
			</tbody>
		</table>
	</div>
</div>