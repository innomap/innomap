<div class="block user">
	<div class="title">
		<div><?= $mode; ?> Users</div>
	</div>
	<div class="block-content">
		<form action="<?= base_url().ADMIN_DIR."users/save"; ?>" name="form_user" id="form-user" method="post">
			<div class="content-form" style="background:#fef1c8; width:80%;">	
				<input type="hidden" name="mode" value="<?= $mode; ?>">
				<input type="hidden" name="user_id" value="<?= ($mode == 'EDIT' ? $id : ''); ?>">
				<table class="form-table">
					<tr>
						<td>Username</td>
						<td>
							<input type="text" name="username" value="<?= ($mode == 'EDIT' ? $user['username'] : '') ?>">
							<label id="labelUsernameExist" class="error" style="display:none;">This username is already used.</label>
						</td>
					</tr>
					<tr>
						<td>Email</td>
						<td><input type="text" name="email" value="<?= ($mode == 'EDIT' ? $user['email'] : '') ?>"></td>
					</tr>
					<?php if($mode == 'EDIT'){ ?>
					<tr>
						<td colspan="2"><a href="#" id="btn_change_password" style="text-decoration:underline;cursor:pointer;display:<?= ($mode == 'EDIT' ? 'block':'none') ?>">Ubah Password</a></td>
					</tr>
					<?php } ?>
					<tr class="password_wrap" style="<?= $mode == 'ADD' ? '' : 'display:none;'; ?>">
						<td>Password</td>
						<td><input type="password" name="password" id="password" /></td>
					</tr>
					<tr class="password_wrap" style="<?= $mode == 'ADD' ? '' : 'display:none;'; ?>">
						<td>Retype Password</td>
						<td><input type="password" name="retype_password" /></td>
					</tr>
					<tr>
						<td>User Role</td>
						<td>
							<?php 
							foreach ($roles as $role) { 
								$exist = false;
								if($role['role_id'] != ROLE_INNOVATOR && $role['role_id'] != ROLE_EXPERT){
									if($mode != 'ADD'){
										foreach ($user['roles'] as $key => $user_role) { 
											if($user_role['role_id'] == $role['role_id']){ $exist = true; }
									 	} 
									 }
							?>
								<input type="checkbox" name="role_id[]" value="<?= $role['role_id']?>" <?= $exist == true ? 'checked' : '' ?> <?= $mode == 'VIEW' ? 'disabled' : '' ?>/> <?= $role['name']?><br />		
							<?php
								} 
							} 
							?>
						</td>
					</tr>
					<tr>
						<td>User Status</td>
						<td>
							<select name="status">
								<option value="1" <?= ($mode == 'EDIT' ? ($user['status'] == 1 ? 'selected' : '') : '') ?>>Active</option>
								<option value="0" <?= ($mode == 'EDIT' ? ($user['status'] == 0 ? 'selected' : '') : '') ?>>Suspend</option>
							</select>
						</td>
					</tr>
				</table>
			</div>
			<div class="content-form" style="width:90%;">	
				<div style="float:right">
					<input type="submit" name="save_user" value="Save" class="button">
					<a href="<?= base_url().ADMIN_DIR."users"; ?>"><input class="button" type="button" value="Back"/></a>
				</div>
			</div>
		</form>
	</div>
</div>

<script>
$(document).ready(function(){
	$("#btn_change_password").click(function() {
		$(".password_wrap").toggle("slow");
	});
});
</script>