<div class="block user">
	<div class="title">
		<div style="float:left;">User</div>
			<a href="<?= site_url(ADMIN_DIR.'users/add'); ?>" class="reset_data_tables">
				<div class="add_button">
					<div>Add New User</div>
				</div>
			</a>
		<div class="clear"></div>
	</div>
	<div class="block-content">
		<?php if($message != NULL){ ?>
			<br/><div class="form-info success"><?= $message; ?></div>
		<?php } ?>
		<table cellpadding="0" cellspacing="0" border="0" class="display light-orange-table" width="100%" id="user_table">
			<thead>
				<th>No</th>
				<th>Username</th>
				<th>Role</th>
				<th>Status</th>
				<th>Action</th>
			</thead>
			<tbody>
				<?php $no=1; foreach($users as $user){ ?>
				<tr>
					<td><?= $no; ?></td>
					<td><?= $user['username']; ?></td>
					<td>
					<?php 
						foreach ($roles as $role) {
							foreach ($user['roles'] as $key => $user_role) {
								if($user_role['role_id'] == $role['role_id']){
									echo $role['name'];
									if(count($user['roles']) > 1){
										echo ($key == 0 || $key < count($user['roles'])-1 ? ', ' : '');
									}
								}
							}
						}
					?>
					</td>
					<td><center><div class="user_<?= ($user['status'] == 1 ? 'active' : 'suspend'); ?>">&nbsp;</div></center></td>
					<td>
						<a href="<?= base_url().ADMIN_DIR.'users/edit/'.$user['user_id']; ?>" class="edit-ico edit" title="Edit"></a>
						<a href="<?= base_url().ADMIN_DIR.'users/delete/'.$user['user_id']; ?>" onClick="return initConfirmDelete()" title="Delete" class="delete"></a>
					</td>
				</tr>
				<?php $no++;} ?>
			</tbody>
		</table>
	</div>
</div>