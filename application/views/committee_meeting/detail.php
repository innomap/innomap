<div class="block innovator green">
	<div class="title"><?= lang('title_committee_meeting') ?></div>
	
	<div class="block-content">
		<div class="detail-content grid_9" style="padding-right:26px">
			<div class="label grid_3"><?= lang('label_committee_meeting_date') ?> : </div>
			<div class="value grid_5"><?= $detail['date']; ?> </div>

			<div class="label grid_3"><?= lang('label_title') ?> : </div>
			<div class="value grid_5"><?= $detail['title']; ?> </div>

			<div class="label grid_3"><?= lang('label_committee_meeting_attendance') ?> : </div>
			<div class="value grid_5"><?= $detail['attendance']; ?> </div>
			
			<div class="label grid_3"><?= lang('label_committee_meeting_note') ?> : </div>
			<div class="value grid_5"><?= $detail['notes']; ?> </div>

			<div class="label grid_3"><?= lang('label_attachment') ?> : </div>
			<div class="value grid_5">
				<?php if(isset($attachments)){ ?>
					<div class="list-attachment-wrap">
						<?php foreach ($attachments as $key => $value) {
							$thumb = (file_exists(realpath(APPPATH . '../assets/attachment/committee_meeting') . DIRECTORY_SEPARATOR . $value['filename'] ) ? $value['filename'] : "default.jpg");
							echo '<img src="'.base_url() . PATH_TO_COMMITTEE_MEETING_ATTACHMENT . $thumb.'" class="grid_3 margin-bottom alpha">';	
						} ?>
					</div>
				<?php } ?>
			</div>

			<div class="clear"></div>
			<div class="grid_3 right action_wrap">
				<a onclick="history.back();" class="btn"><< <?= lang('button_back') ?></a>
				<?php if(isset($detail)){ ?>
					<a class="btn print_page"><?= lang('button_print') ?></a>
				<?php } ?>
			</div>
		</div>
	</div>
</div>

<div id="detail-innovation" style="display:none" title="<?= lang('title_popup_detail_innovation') ?>"></div>