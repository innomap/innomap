<script type="text/javascript">
$(function(){
	$('textarea.mention').mentionsInput({
	  minChars:0,
	  triggerChar : '#',
	  onDataRequest:function (mode, query, callback) {
	    var data = <?= json_encode($innovations); ?>

	    data = _.filter(data, function(item) { return item.name.toLowerCase().indexOf(query.toLowerCase()) > -1 });

	    callback.call(this, data);
	  }
	});
});
</script>
<div class="block innovator green">
	<div class="title"><?=lang('title_committee_meeting')?></div>
	
	<div class="block-content">
		<div class="detail-content grid_11 alpha">
			<form method="POST" id="form-committee-meeting" action="<?=base_url()."committee_meetings/save"?>" enctype="multipart/form-data">
				<input type="hidden" name="committee_meeting_id" value="<?= isset($meeting) ? $meeting['committee_meeting_id'] : '-1' ?>">
				
				<div class="label grid_3"><?= lang('label_committee_meeting_date') ?> : </div>
				<div class="value grid_6">
					<input type="text" name="date" class="grid_6 alpha omega datepicker" value="<?= isset($meeting) ? $meeting['date'] : '' ?>">
				</div>

				<div class="label grid_3"><?= lang('label_title') ?> : </div>
				<div class="value grid_6">
					<input type="text" name="title" class="grid_6 alpha omega" value="<?= isset($meeting) ? $meeting['title'] : '' ?>">
				</div>

				<div class="label grid_3"><?= lang('label_committee_meeting_attendance') ?> : </div>
				<div class="value grid_6">
					<textarea name="attendance" class="grid_6 alpha omega"><?= isset($meeting) ? $meeting['attendance'] : '' ?></textarea>
				</div>

				<div class="label grid_3 label-mention"><?= lang('label_committee_meeting_note') ?> : </div>
				<div class="grid_6 alpha omega" style="margin-bottom:10px">
					<textarea class="mention" rows="10"><?= isset($meeting) ? $meeting['notes'] : '' ?></textarea>
					<textarea name="notes" id="mention-val" style="display:none"><?= isset($meeting) ? $meeting['notes'] : '' ?></textarea>
				</div>

				<div class="label grid_3"><?= lang('label_attachment') ?> : </div>
				<div class="grid_6 field-attachment" style="margin-bottom:10px">
					<a href="#" class="add-attachment"><?= "(+) ".lang('btn_add_attachment'); ?></a>
					<br/><br/>
					<?php if(isset($attachments)){ ?>
						<div class="list-attachment-wrap">
							<?php foreach ($attachments as $key => $value) {
								$thumb = (file_exists(realpath(APPPATH . '../assets/attachment/committee_meeting') . DIRECTORY_SEPARATOR . $value['filename'] ) ? $value['filename'] : "default.jpg");
								echo '<div class="grid_8 alpha"><img src="'.base_url() . PATH_TO_COMMITTEE_MEETING_ATTACHMENT . $thumb.'" class="grid_3 margin-bottom alpha"> <a href="#" class="delete-attachment" data-id="'.$value['attachment_id'].'">(-) '.lang('link_delete').'</a></div>';	
							} ?>
						</div>
					<?php } ?>
					<div class="input-attachment-wrap">
						<input type="file" class="margin-bottom" name="attachment_0">
					</div>
				</div>
				<input type="hidden" class="lang_delete" value="<?= lang('link_delete') ?>">
				
				<div class="grid_5 right">
					<a href="<?=base_url().'committee_meetings'?>" class="btn red grid_1 omega alpha"><?=lang('button_cancel')?></a>
					<input type="submit" id="submit-cm" class="btn" value="<?=lang('button_save')?>" />
				</div>
			</form>
		</div>
	</div>
</div>