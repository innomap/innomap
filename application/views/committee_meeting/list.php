<div class="block innovator">
	<div class="title">
		<div style="<?= $access['add'] == true ? 'float:left' : '';?>"><?=lang('title_committee_meeting')?></div>
		<?php if($access['add']){ ?>
			<a href="<?=base_url()."committee_meetings/add"?>" class="reset_data_tables">
				<div class="add_innovator_button">
					<div><?=lang('btn_add_committee_meeting')?></div>
				</div>
			</a>
		<?php } ?>
		<div class="clear"></div>
	</div>
	<div class="block-content">
		<?php
			if($flashdata != NULL){
				echo "<div class='form-info ".($flashdata['success'] ? 'success' : 'fail')."'>".$flashdata['msg']."</div>";
			}
		?>
		<table cellpadding="0" cellspacing="0" border="0" class="display" id="innovator_table" width="100%">
			<thead>
				<tr>
					<th width="5%">No.</th>
					<th width="35%"><?=lang('created_by_label')?></th>
					<th width="45%"><?=lang('label_title')?></th>
					<th width="45%"><?=lang('label_created_date')?></th>
					<th class="last" width="10%"><?=lang('action')?></th>
				</tr>
			</thead>
			<tbody>
				<?php 
					$no = 1;
					foreach($meetings as $key=>$value){ ?>
						<tr>
							<td><?=$no?></td>
							<td><?=$value['username']?></td>
							<td><?=$value['title']?></td>
							<td><?=$value['created_date']?></td>
							<td>
								<a href="<?=base_url()."committee_meetings/view/".$value['committee_meeting_id']?>" title="<?=lang('link_view')?>" class="view-icon"></a>
								<?php if($access['edit'] == 1){ ?><a href="<?=base_url()."committee_meetings/edit/".$value['committee_meeting_id']?>" class="edit" title="Edit"></a><?php } ?>
								<?php if($access['delete'] == 1){ ?><a href="<?=base_url()."committee_meetings/delete/".$value['committee_meeting_id']?>" onclick="return confirm_delete()" title="<?=lang('link_delete')?>" class="delete"></a> <?php } ?>
							</td>
						</tr>
					<?php 
					$no++;
					} ?>
			</tbody>
		</table>
	</div>
</div>