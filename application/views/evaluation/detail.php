<div class="info-eval">
	<h2><?= lang('title_evaluation_detail'); ?></h2>

	<label><?= lang('label_expert') ?>:</label>
	<label><?= $content['expert_name']; ?></label><br/>

	<label><?= lang('label_innovation_name') ?>:</label>
	<label><?= $content['innovation_name']; ?></label><br/>

	<label><?= lang('label_date') ?>:</label>
	<label><?= $current_date; ?></label>
</div>

<div id="eval-popup-tab">
	<ul>
	    <li><a href="#tabs-1"><?= lang('button_evaluation_form_1'); ?></a></li>
	    <li><a href="#tabs-2"><?= lang('button_evaluation_form_2'); ?></a></li>
  	</ul>

  	<div id="tabs-1">
		<!--<h3><?php // lang('button_evaluation_form_1'); ?></h3>-->

		<table border="0" cellspacing="0" cellpadding="0" class="table-evaluation orange-table">
			<thead>
				<th style="width:20%"><b><?= lang('label_evaluation_point') ?></b></th>
				<th style="width:55%"><b><?= lang('label_criteria') ?></b></th>
				<th><b><?= lang('label_point') ?></b></th>
			</thead>
			<tr>
				<td><b><?= lang('value_point_1') ?></b></td>
				<td colspan="2">
					<table>
						<tr><td><b>1. <?= lang('value_point_1_1') ?></b></td></tr>
						<tr>
							<td>a. <?= lang('value_point_1_1_a') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 6</i></div></td>
							<td><input type="text" id="ef-creativity-ori" name="creativity_ori" value="<?= ($mode != 'ADD' ? $evaluation['creativity_originality'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
						</tr>
						<tr>
							<td>b. <?= lang('value_point_1_1_b') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 4</i></div></td>
							<td><input type="text" id="ef-creativity-adap" name="creativity_adap" value="<?= ($mode != 'ADD' ? $evaluation['creativity_adaptation'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
						</tr>
						<tr>
							<td></td>
							<td><b>Subtotal: </b><input type="text" id="ef-total-creativity" name="total_creativity" value="<?= ($mode != 'ADD' ? $evaluation['total_creativity'] : 0) ?>" class="subtotal" readonly></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td><b><?= lang('value_point_2') ?></b></td>
				<td colspan="2">
					<table>
						<tr><td><b>2. <?= lang('value_point_2_2') ?></b></td></tr>
						<tr>
							<td>a. <?= lang('value_point_2_2_a') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 6</i></div></td>
							<td><input type="text" id="ef-implement-full" name="implementation_level_full" value="<?= ($mode != 'ADD' ? $evaluation['implementation_level_full'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
						</tr>
						<tr>
							<td>b. <?= lang('value_point_2_2_b') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 4</i></div></td>
							<td><input type="text" id="ef-implement-trial" name="implementation_level_trial" value="<?= ($mode != 'ADD' ? $evaluation['implementation_level_trial'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
						</tr>
						<tr><td><b>3. <?= lang('value_point_2_3') ?></b></td></tr>
						<tr>
							<td>a. <?= lang('value_point_2_3_a') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 6</i></div></td>
							<td><input type="text" id="ef-replicability-free" name="replicability_free" value="<?= ($mode != 'ADD' ? $evaluation['replicability_free'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
						</tr>
						<tr>
							<td>b. <?= lang('value_point_2_3_b') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 4</i></div></td>
							<td><input type="text" id="ef-replicability-not-free" name="replicability_not_free" value="<?= ($mode != 'ADD' ? $evaluation['replicability_not_free'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
						</tr>
						<tr>
							<td></td>
							<td><b>Subtotal: </b><input type="text" id="ef-total-output" name="total_output" class="subtotal" value="<?= ($mode != 'ADD' ? $evaluation['total_output'] : 0) ?>" readonly></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td><b><?= lang('value_point_3') ?></b></td>
				<td colspan="2">
					<table>
						<tr><td><b>4. <?= lang('value_point_3_4') ?></b></td></tr>
						<tr>
							<td>a. <?= lang('value_point_3_4_a') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 2</i></div></td>
							<td><input type="text" id="ef-efficiency-time" name="efficiency_time" value="<?= ($mode != 'ADD' ? $evaluation['efficiency_time'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
						</tr>
						<tr>
							<td>b. <?= lang('value_point_3_4_b') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 2</i></div></td>
							<td><input type="text" id="ef-efficiency-costs" name="efficiency_costs" value="<?= ($mode != 'ADD' ? $evaluation['efficiency_costs'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
						</tr>
						<tr>
							<td>c. <?= lang('value_point_3_4_c') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 2</i></div></td>
							<td><input type="text" id="ef-efficiency-productivity" name="efficiency_productivity" value="<?= ($mode != 'ADD' ? $evaluation['efficiency_productivity'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
						</tr>
						<tr>
							<td>d. <?= lang('value_point_3_4_d') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 2</i></div></td>
							<td><input type="text" id="ef-efficiency-design" name="efficiency_design" value="<?= ($mode != 'ADD' ? $evaluation['efficiency_design'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
						</tr>
						<tr>
							<td><b>5. <?= lang('value_point_3_5') ?></b> - <?= lang('value_point_3_5_desc') ?>:</td>
						</tr>
						<tr>
							<td>a. <?= lang('value_point_3_5_a') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 6</i></div></td>
							<td><input type="text" id="ef-significance-global" name="significance_global" value="<?= ($mode != 'ADD' ? $evaluation['significance_global'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
						</tr>
						<tr>
							<td>b. <?= lang('value_point_3_5_b') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 4</i></div></td>
							<td><input type="text" id="ef-significance-national" name="significance_national" value="<?= ($mode != 'ADD' ? $evaluation['significance_national'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
						</tr>
						<tr>
							<td>c. <?= lang('value_point_3_5_c') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 2</i></div></td>
							<td><input type="text" id="ef-significance-local" name="significance_local" value="<?= ($mode != 'ADD' ? $evaluation['significance_local'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
						</tr>
						<tr>
							<td></td>
							<td><b>Subtotal: </b><input type="text" id="ef-total-outcome" class="subtotal" name="total_outcome" value="<?= ($mode != 'ADD' ? $evaluation['total_outcome'] : 0) ?>" readonly></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td><b><?= lang('value_point_4') ?></b></td>
				<td colspan="2">
					<table>
						<tr><td><b>6. <?= lang('value_point_4_6') ?></b></td></tr>
						<tr>
							<td>a. <?= lang('value_point_4_6_a') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 2</i></div></td>
							<td><input type="text" id="ef-commitment-finance" name="commitment_finance" value="<?= ($mode != 'ADD' ? $evaluation['commitment_finance'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
						</tr>
						<tr>
							<td>b. <?= lang('value_point_4_6_b') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 2</i></div></td>
							<td><input type="text" id="ef-commitment-hr" name="commitment_hr" value="<?= ($mode != 'ADD' ? $evaluation['commitment_hr'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
						</tr>
						<tr>
							<td>c. <?= lang('value_point_4_6_c') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 2</i></div></td>
							<td><input type="text" id="ef-commitment-incentive" name="commitment_incentive" value="<?= ($mode != 'ADD' ? $evaluation['commitment_incentive'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
						</tr>
						<tr>
							<td>d. <?= lang('value_point_4_6_d') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 2</i></div></td>
							<td><input type="text" id="ef-commitment-reward" name="commitment_reward" value="<?= ($mode != 'ADD' ? $evaluation['commitment_reward'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
						</tr>
						<tr>
							<td>e. <?= lang('value_point_4_6_e') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 2</i></div></td>
							<td><input type="text" id="ef-commitment-equipment" name="commitment_equipment" value="<?= ($mode != 'ADD' ? $evaluation['commitment_equipment'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
						</tr>
						<tr>
							<td></td>
							<td><b>Subtotal: </b><input type="text" class="subtotal" id="total-commitment"  name="total_commitment" value="<?= ($mode != 'ADD' ? $evaluation['total_commitment'] : 0) ?>" readonly></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td><b><?= lang('value_point_5') ?></b></td>
				<td colspan="2">
					<table>
						<tr>
							<td><?= lang('value_point_5_desc') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 10</i></div></td>
							<td><input type="text" id="ef-relevancy" name="relevancy" value="<?= ($mode != 'ADD' ? $evaluation['relevancy'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
						</tr>
						<tr>
							<td></td>
							<td><b>Subtotal: </b><input type="text" class="subtotal" for="ef-relevancy" name="total_relevancy" readonly value="<?= ($mode != 'ADD' ? $evaluation['relevancy'] : 0) ?>"></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td><b><?= lang('value_point_6') ?></b></td>
				<td colspan="2">
					<table>
						<tr>
							<td><?= lang('value_point_6_desc') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 10</i></div></td>
							<td><input type="text" id="ef-effectiveness" name="effectiveness" value="<?= ($mode != 'ADD' ? $evaluation['effectiveness'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
						</tr>
						<tr>
							<td></td>
							<td><b>Subtotal: </b><input type="text" class="subtotal" for="ef-effectiveness" name="total_effectiveness" value="<?= ($mode != 'ADD' ? $evaluation['effectiveness'] : 0) ?>" readonly></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td><b><?= lang('value_point_7') ?></b></td>
				<td colspan="2">
					<table>
						<tr>
							<td><?= lang('value_point_7_desc') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 10</i></div></td>
							<td><input type="text" id="ef-quality" name="quality" value="<?= ($mode != 'ADD' ? $evaluation['quality'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
						</tr>
						<tr>
							<td></td>
							<td><b>Subtotal: </b><input type="text" class="subtotal" for="ef-quality" name="total_quality" value="<?= ($mode != 'ADD' ? $evaluation['quality'] : 0) ?>" readonly></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td><b><?= lang('value_point_8') ?></b></td>
				<td colspan="2">
					<table>
						<tr>
							<td><?= lang('value_point_8_desc') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 10</i></div></td>
							<td><input type="text" id="ef-potential" name="potential" value="<?= ($mode != 'ADD' ? $evaluation['potential'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
						</tr>
						<tr>
							<td></td>
							<td><b>Subtotal: </b><input type="text" class="subtotal" for="ef-potential" name="total_potential" value="<?= ($mode != 'ADD' ? $evaluation['potential'] : 0) ?>" readonly></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td></td>
				<td colspan="2">
					<table>
						<tr>
							<td><b><?= lang('label_total') ?></b></td>
							<td><input type="text" id="ef-total" name="total" value="<?= ($mode != 'ADD' ? $evaluation['total'] : 0) ?>" readonly><div><span id="score-balance"><?= ($mode != "ADD" ? 100-$evaluation['total'] : 100); ?></span> points available</div></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>

	<div id="tabs-2">
		<!--<h3><?php // lang('button_evaluation_form_2'); ?></h3>-->

		<div class="child_tabs2 odd">
			<label>A. <?= lang('value_point_f2_a') ?></label>
			<div>
				<input type="radio" name="market_needs" value="1" <?= ($evaluation['market_needs'] == 1 ? 'checked' : ''); ?> disabled><?= lang('value_yes') ?>
				<input type="radio" name="market_needs" value="0" <?= ($evaluation['market_needs'] == 0 ? 'checked' : ''); ?> disabled><?= lang('value_no') ?>
				<br/>
				<label><?= lang('label_review') ?> : </label>
				<p><?= $evaluation['market_needs_review']; ?></p>
			</div>
		</div>
		<div class="child_tabs2 even">
			<label>B. <?= lang('value_point_f2_b') ?></label>
			<div>
				<input type="radio" name="best_method" value="1" <?= ($evaluation['best_method'] == 1 ? 'checked' : ''); ?> disabled><?= lang('value_yes') ?>
				<input type="radio" name="best_method" value="0" <?= ($evaluation['best_method'] == 0 ? 'checked' : ''); ?> disabled><?= lang('value_no') ?>
				<br/>
				<label><?= lang('label_review') ?> : </label>
				<p><?= $evaluation['best_method_review']; ?></p>
			</div>
		</div>
		<div class="child_tabs2 odd">
			<label>C. <?= lang('value_point_f2_c') ?></label>
			<div>
				<input type="radio" name="benefit" value="1" <?= ($evaluation['benefit'] == 1 ? 'checked' : ''); ?> disabled><?= lang('value_yes') ?>
				<input type="radio" name="benefit" value="0" <?= ($evaluation['benefit'] == 0 ? 'checked' : ''); ?> disabled><?= lang('value_no') ?>
				<br/>
				<label><?= lang('label_review') ?> : </label>
				<p><?= $evaluation['benefit_review']; ?></p>
			</div>
		</div>
		<div class="child_tabs2 even">
			<label>D. <?= lang('value_point_f2_d') ?></label>
			<div>
				<input type="radio" name="competition" value="1" <?= ($evaluation['competition'] == 1 ? 'checked' : ''); ?> disabled><?= lang('value_yes') ?>
				<input type="radio" name="competition" value="0" <?= ($evaluation['competition'] == 0 ? 'checked' : ''); ?> disabled><?= lang('value_no') ?>
				<br/>
				<label><?= lang('label_review') ?> : </label>
				<p><?= $evaluation['competition_review']; ?></p>
			</div>
		</div>
		<div class="child_tabs2 odd">
			<label><?= lang('value_point_f2_e') ?> : </label>
			<p><?= $evaluation['expertise_review']; ?></p>
		</div>
	</div>
</div>