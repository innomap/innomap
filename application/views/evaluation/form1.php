<div class="block innovator green">
	<div class="title"><?= lang('title_evaluation_form') ?></div>
	<div class="block-content">
		<?php $this->load->view('evaluation/header',$header); ?>

		<div class="detail-content grid_11 alpha">
			<?php if($message){ ?>
				<div class="form-info success"><?= $message['msg']; ?></div>
			<?php } ?>
			<form action="<?= base_url().'evaluations/save_form1' ?>" id="form-evaluation-1" method="post">
				<input type="hidden" name="expert_id" value="<?= $content['expert_id'] ?>">
				<input type="hidden" name="expert_task_id" value="<?= $content['expert_task_id'] ?>">
				<input type="hidden" name="form_evaluation_1" value="1">
				<input type="hidden" name="innovation_id" value="<?= $content['innovation_id'] ?>">
				<input type="hidden" name="mode" value="<?= $mode; ?>">
				<input type="hidden" name="evaluation_id" value="<?= ($mode != 'ADD' ? $evaluation['evaluation_id'] : ''); ?>">

				<div class="label grid_3"><?= lang('label_expert') ?>:</div>
				<div class="value grid_6"><input type="text" class="grid_4 alpha" name="expert_name" value="<?= $content['expert_name']; ?>" readonly /></div>

				<div class="label grid_3"><?= lang('label_innovation_name') ?>:</div>
				<div class="value grid_6">
					<input type="text" class="grid_4 alpha" name="innovation" value="<?= $content['innovation_name']; ?>" readonly />
					<a href="#" class="view-detail-inno" inno_id="<?= $content['innovation_id'] ?>"><div class="view-icon" title="<?= lang('button_view_detail') ?>"></div></a>
					<a href="#" class="view-notes view-notes-icon" inno_id="<?= $content['innovation_id']; ?>" title="<?= lang('button_view_notes') ?>"></a>
				</div>

				<div class="label grid_3"><?= lang('label_date') ?>:</div>
				<div class="value grid_6"><input type="text" class="grid_4 alpha" name="date" value="<?= $current_date; ?>" readonly /></div>

				<table border="0" cellspacing="0" cellpadding="0" class="table-evaluation orange-table">
					<thead>
						<th style="width:20%"><b><?= lang('label_evaluation_point') ?></b></th>
						<th style="width:55%"><b><?= lang('label_criteria') ?></b></th>
						<th><b><?= lang('label_point') ?></b></th>
					</thead>
					<tr>
						<td><b><?= lang('value_point_1') ?></b></td>
						<td colspan="2">
							<table>
								<tr><td><b>1. <?= lang('value_point_1_1') ?></b></td></tr>
								<tr>
									<td>a. <?= lang('value_point_1_1_a') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 6</i></div></td>
									<td><input type="text" id="ef-creativity-ori" name="creativity_ori" value="<?= ($mode != 'ADD' ? $evaluation['creativity_originality'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
								</tr>
								<tr>
									<td>b. <?= lang('value_point_1_1_b') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 4</i></div></td>
									<td><input type="text" id="ef-creativity-adap" name="creativity_adap" value="<?= ($mode != 'ADD' ? $evaluation['creativity_adaptation'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
								</tr>
								<tr>
									<td></td>
									<td><b>Subtotal: </b><input type="text" id="ef-total-creativity" name="total_creativity" value="<?= ($mode != 'ADD' ? $evaluation['total_creativity'] : 0) ?>" class="subtotal" readonly></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td><b><?= lang('value_point_2') ?></b></td>
						<td colspan="2">
							<table>
								<tr><td><b>2. <?= lang('value_point_2_2') ?></b></td></tr>
								<tr>
									<td>a. <?= lang('value_point_2_2_a') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 6</i></div></td>
									<td><input type="text" id="ef-implement-full" name="implementation_level_full" value="<?= ($mode != 'ADD' ? $evaluation['implementation_level_full'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
								</tr>
								<tr>
									<td>b. <?= lang('value_point_2_2_b') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 4</i></div></td>
									<td><input type="text" id="ef-implement-trial" name="implementation_level_trial" value="<?= ($mode != 'ADD' ? $evaluation['implementation_level_trial'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
								</tr>
								<tr><td><b>3. <?= lang('value_point_2_3') ?></b></td></tr>
								<tr>
									<td>a. <?= lang('value_point_2_3_a') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 6</i></div></td>
									<td><input type="text" id="ef-replicability-free" name="replicability_free" value="<?= ($mode != 'ADD' ? $evaluation['replicability_free'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
								</tr>
								<tr>
									<td>b. <?= lang('value_point_2_3_b') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 4</i></div></td>
									<td><input type="text" id="ef-replicability-not-free" name="replicability_not_free" value="<?= ($mode != 'ADD' ? $evaluation['replicability_not_free'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
								</tr>
								<tr>
									<td></td>
									<td><b>Subtotal: </b><input type="text" id="ef-total-output" name="total_output" class="subtotal" value="<?= ($mode != 'ADD' ? $evaluation['total_output'] : 0) ?>" readonly></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td><b><?= lang('value_point_3') ?></b></td>
						<td colspan="2">
							<table>
								<tr><td><b>4. <?= lang('value_point_3_4') ?></b></td></tr>
								<tr>
									<td>a. <?= lang('value_point_3_4_a') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 2</i></div></td>
									<td><input type="text" id="ef-efficiency-time" name="efficiency_time" value="<?= ($mode != 'ADD' ? $evaluation['efficiency_time'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
								</tr>
								<tr>
									<td>b. <?= lang('value_point_3_4_b') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 2</i></div></td>
									<td><input type="text" id="ef-efficiency-costs" name="efficiency_costs" value="<?= ($mode != 'ADD' ? $evaluation['efficiency_costs'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
								</tr>
								<tr>
									<td>c. <?= lang('value_point_3_4_c') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 2</i></div></td>
									<td><input type="text" id="ef-efficiency-productivity" name="efficiency_productivity" value="<?= ($mode != 'ADD' ? $evaluation['efficiency_productivity'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
								</tr>
								<tr>
									<td>d. <?= lang('value_point_3_4_d') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 2</i></div></td>
									<td><input type="text" id="ef-efficiency-design" name="efficiency_design" value="<?= ($mode != 'ADD' ? $evaluation['efficiency_design'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
								</tr>
								<tr>
									<td><b>5. <?= lang('value_point_3_5') ?></b> - <?= lang('value_point_3_5_desc') ?>:</td>
								</tr>
								<tr>
									<td>a. <?= lang('value_point_3_5_a') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 6</i></div></td>
									<td><input type="text" id="ef-significance-global" name="significance_global" value="<?= ($mode != 'ADD' ? $evaluation['significance_global'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
								</tr>
								<tr>
									<td>b. <?= lang('value_point_3_5_b') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 4</i></div></td>
									<td><input type="text" id="ef-significance-national" name="significance_national" value="<?= ($mode != 'ADD' ? $evaluation['significance_national'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
								</tr>
								<tr>
									<td>c. <?= lang('value_point_3_5_c') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 2</i></div></td>
									<td><input type="text" id="ef-significance-local" name="significance_local" value="<?= ($mode != 'ADD' ? $evaluation['significance_local'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
								</tr>
								<tr>
									<td></td>
									<td><b>Subtotal: </b><input type="text" id="ef-total-outcome" class="subtotal" name="total_outcome" value="<?= ($mode != 'ADD' ? $evaluation['total_outcome'] : 0) ?>" readonly></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td><b><?= lang('value_point_4') ?></b></td>
						<td colspan="2">
							<table>
								<tr><td><b>6. <?= lang('value_point_4_6') ?></b></td></tr>
								<tr>
									<td>a. <?= lang('value_point_4_6_a') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 2</i></div></td>
									<td><input type="text" id="ef-commitment-finance" name="commitment_finance" value="<?= ($mode != 'ADD' ? $evaluation['commitment_finance'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
								</tr>
								<tr>
									<td>b. <?= lang('value_point_4_6_b') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 2</i></div></td>
									<td><input type="text" id="ef-commitment-hr" name="commitment_hr" value="<?= ($mode != 'ADD' ? $evaluation['commitment_hr'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
								</tr>
								<tr>
									<td>c. <?= lang('value_point_4_6_c') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 2</i></div></td>
									<td><input type="text" id="ef-commitment-incentive" name="commitment_incentive" value="<?= ($mode != 'ADD' ? $evaluation['commitment_incentive'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
								</tr>
								<tr>
									<td>d. <?= lang('value_point_4_6_d') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 2</i></div></td>
									<td><input type="text" id="ef-commitment-reward" name="commitment_reward" value="<?= ($mode != 'ADD' ? $evaluation['commitment_reward'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
								</tr>
								<tr>
									<td>e. <?= lang('value_point_4_6_e') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 2</i></div></td>
									<td><input type="text" id="ef-commitment-equipment" name="commitment_equipment" value="<?= ($mode != 'ADD' ? $evaluation['commitment_equipment'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
								</tr>
								<tr>
									<td></td>
									<td><b>Subtotal: </b><input type="text" class="subtotal" id="total-commitment"  name="total_commitment" value="<?= ($mode != 'ADD' ? $evaluation['total_commitment'] : 0) ?>" readonly></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td><b><?= lang('value_point_5') ?></b></td>
						<td colspan="2">
							<table>
								<tr>
									<td><?= lang('value_point_5_desc') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 10</i></div></td>
									<td><input type="text" id="ef-relevancy" name="relevancy" value="<?= ($mode != 'ADD' ? $evaluation['relevancy'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
								</tr>
								<tr>
									<td></td>
									<td><b>Subtotal: </b><input type="text" class="subtotal" for="ef-relevancy" name="total_relevancy" readonly value="<?= ($mode != 'ADD' ? $evaluation['relevancy'] : 0) ?>"></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td><b><?= lang('value_point_6') ?></b></td>
						<td colspan="2">
							<table>
								<tr>
									<td><?= lang('value_point_6_desc') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 10</i></div></td>
									<td><input type="text" id="ef-effectiveness" name="effectiveness" value="<?= ($mode != 'ADD' ? $evaluation['effectiveness'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
								</tr>
								<tr>
									<td></td>
									<td><b>Subtotal: </b><input type="text" class="subtotal" for="ef-effectiveness" name="total_effectiveness" value="<?= ($mode != 'ADD' ? $evaluation['effectiveness'] : 0) ?>" readonly></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td><b><?= lang('value_point_7') ?></b></td>
						<td colspan="2">
							<table>
								<tr>
									<td><?= lang('value_point_7_desc') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 10</i></div></td>
									<td><input type="text" id="ef-quality" name="quality" value="<?= ($mode != 'ADD' ? $evaluation['quality'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
								</tr>
								<tr>
									<td></td>
									<td><b>Subtotal: </b><input type="text" class="subtotal" for="ef-quality" name="total_quality" value="<?= ($mode != 'ADD' ? $evaluation['quality'] : 0) ?>" readonly></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td><b><?= lang('value_point_8') ?></b></td>
						<td colspan="2">
							<table>
								<tr>
									<td><?= lang('value_point_8_desc') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 10</i></div></td>
									<td><input type="text" id="ef-potential" name="potential" value="<?= ($mode != 'ADD' ? $evaluation['potential'] : 0) ?>" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>></td>
								</tr>
								<tr>
									<td></td>
									<td><b>Subtotal: </b><input type="text" class="subtotal" for="ef-potential" name="total_potential" value="<?= ($mode != 'ADD' ? $evaluation['potential'] : 0) ?>" readonly></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td></td>
						<td colspan="2">
							<table>
								<tr>
									<td><b><?= lang('label_total') ?></b></td>
									<td><input type="text" id="ef-total" name="total" value="<?= ($mode != 'ADD' ? $evaluation['total'] : 0) ?>" readonly><div><span id="score-balance"><?= ($mode != "ADD" ? 100-$evaluation['total'] : 100); ?></span> points available</div></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>

				<div class="grid_6 margin-top">
					<?php if($mode != 'VIEW'){ ?>
						<input type="submit" class="submit-evaluation-1 btn" name="draft" value="<?= lang('button_save_to_draft') ?>">
						<input type="submit" class="submit-evaluation-1 btn" name="next" value="<?= lang('button_next_step') ?> >>">
					<?php }else{ ?>
						<a onclick="history.back();" class="btn red grid_1 omega alpha" style="float:left"><?= lang('button_back') ?></a>
						<a href="<?= base_url().'evaluations/view/form2/'.$evaluation['evaluation_id']; ?>" class="btn" style="float:left"><?= lang('button_next_step') ?> >> </a>
						<div class="clear"></div>
					<?php } ?>
				</div>
			</form>
		</div>
	</div>
</div>