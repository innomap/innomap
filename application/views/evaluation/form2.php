<div class="block innovator green">
	<div class="title"><?= lang('title_evaluation_form') ?></div>
	<div class="block-content">
		<?php $this->load->view('evaluation/header'); ?>

		<div class="detail-content grid_11 alpha">
			<?php if($message){ ?>
				<div class="form-info success"><?= $message['msg']; ?></div>
			<?php } ?>
			<form action="<?= base_url().'evaluations/save_form2' ?>" id="form-evaluation-1" method="post" class="evaluation-form2">
				<input type="hidden" name="expert_id" value="<?= $content['expert_id'] ?>">
				<input type="hidden" name="form_evaluation_2" value="1">
				<input type="hidden" name="innovation_id" value="<?= $content['innovation_id'] ?>">
				<input type="hidden" name="mode" value="<?= $mode; ?>">
				<input type="hidden" name="evaluation_id" value="<?= ($mode != 'ADD' ? $evaluation['evaluation_id'] : ''); ?>">
				<input type="hidden" name="total" value="<?= $evaluation['total']; ?>">

				<div class="label grid_3"><?= lang('label_expert') ?>:</div>
				<div class="value grid_6"><input type="text" class="grid_4 alpha" name="expert_name" value="<?= $content['expert_name']; ?>" readonly /></div>

				<div class="label grid_3"><?= lang('label_innovation_name') ?>:</div>
				<div class="value grid_6">
					<input type="text" class="grid_4 alpha" name="innovation" value="<?= $content['innovation_name']; ?>" readonly />
					<a href="#" class="view-detail-inno" inno_id="<?= $content['innovation_id'] ?>"><div class="view-icon" title="<?= lang('button_view_detail') ?>"></div></a>
					<a href="#" class="view-notes view-notes-icon" inno_id="<?= $content['innovation_id']; ?>" title="<?= lang('button_view_notes') ?>"></a>
				</div>

				<div class="label grid_3"><?= lang('label_date') ?>:</div>
				<div class="value grid_6"><input type="text" class="grid_4 alpha" name="date" value="<?= $current_date; ?>" readonly /></div>

				<div class="clear"></div>

				<div class="child_tabs2 odd">
					<div class="label">A. <?= lang('value_point_f2_a') ?></div>
					<div class="eval-value">
						<input type="radio" name="market_needs" value="1" <?= ($evaluation['market_needs'] == 1 ? 'checked' : ''); ?> <?= ($mode == 'VIEW' ? 'disabled' : ''); ?>><?= lang('value_yes') ?>
						<input type="radio" name="market_needs" value="0" <?= ($evaluation['market_needs'] == 0 ? 'checked' : ''); ?> <?= ($mode == 'VIEW' ? 'disabled' : ''); ?>><?= lang('value_no') ?>
						<br/>
						<label><?= lang('label_review') ?></label><br/>
						<textarea name="market_needs_review" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>><?= $evaluation['market_needs_review']; ?></textarea>
					</div>
				</div>
			
				<div class="child_tabs2 even">
					<div class="label">B. <?= lang('value_point_f2_b') ?></div>
					<div class="eval-value">
						<input type="radio" name="best_method" value="1" <?= ($evaluation['best_method'] == 1 ? 'checked' : ''); ?> <?= ($mode == 'VIEW' ? 'disabled' : ''); ?>><?= lang('value_yes') ?>
						<input type="radio" name="best_method" value="0" <?= ($evaluation['best_method'] == 0 ? 'checked' : ''); ?> <?= ($mode == 'VIEW' ? 'disabled' : ''); ?>><?= lang('value_no') ?>
						<br/>
						<label><?= lang('label_review') ?></label><br/>
						<textarea name="best_method_review" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>><?= $evaluation['best_method_review']; ?></textarea>
					</div>
				</div>

				<div class="child_tabs2 odd">
					<div class="label">C. <?= lang('value_point_f2_c') ?></div>
					<div class="eval-value">
						<input type="radio" name="benefit" value="1" <?= ($evaluation['benefit'] == 1 ? 'checked' : ''); ?> <?= ($mode == 'VIEW' ? 'disabled' : ''); ?>><?= lang('value_yes') ?>
						<input type="radio" name="benefit" value="0" <?= ($evaluation['benefit'] == 0 ? 'checked' : ''); ?> <?= ($mode == 'VIEW' ? 'disabled' : ''); ?>><?= lang('value_no') ?>
						<br/>
						<label><?= lang('label_review') ?></label><br/>
						<textarea name="benefit_review" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>><?= $evaluation['benefit_review']; ?></textarea>
					</div>
				</div>
				
				<div class="child_tabs2 even">
					<div class="label">D. <?= lang('value_point_f2_d') ?></div>
					<div class="eval-value">
						<input type="radio" name="competition" value="1" <?= ($evaluation['competition'] == 1 ? 'checked' : ''); ?> <?= ($mode == 'VIEW' ? 'disabled' : ''); ?>><?= lang('value_yes') ?>
						<input type="radio" name="competition" value="0" <?= ($evaluation['competition'] == 0 ? 'checked' : ''); ?> <?= ($mode == 'VIEW' ? 'disabled' : ''); ?>><?= lang('value_no') ?>
						<br/>
						<label><?= lang('label_review') ?></label><br/>
						<textarea name="competition_review" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>><?= $evaluation['competition_review']; ?></textarea>
					</div>
				</div>
				<div class="child_tabs2 odd">
					<div class="label"><?= lang('value_point_f2_e') ?></div>
					<div class="eval-value">
						<textarea name="expertise_review" <?= ($mode == 'VIEW' ? 'readonly' : ''); ?>><?= $evaluation['expertise_review']; ?></textarea>
					</div>
				</div>

				<div class="clear"></div><br/>
				<div>
					<?php if($mode != 'VIEW'){ ?>
						<input type="submit" name="draft" class="btn" value="<?= lang('button_save_to_draft') ?>">
						<input type="submit" name="send" class="btn" value="<?= lang('button_send_evaluation') ?>" onClick="return confirmation('<?=lang('msg_confirm_send_evaluation') ?>')">
					<?php }else{  ?>
						<a onclick="history.back();" class="btn red grid_1 omega alpha"><?= lang('button_back') ?></a>
					<?php } ?>
				</div>
				<div class="clear"></div>
			</form>
		</div>
	</div>
</div>