<ul class="grid_10" id="tabs">
	<?php if($header['mode'] != 'ADD'){ ?>
		<li class="<?= ($form == "form1" ? "active" : ""); ?>"><a href="<?= base_url().'evaluations/'.($header['mode'] == 'EDIT' ? 'edit' : 'view').'/form1/'.$header['id']; ?>"><?= lang('button_evaluation_form_1') ?></a></li>
		<li class="<?= ($form == "form2" ? "active" : ""); ?>"><a href="<?= base_url().'evaluations/'.($header['mode'] == 'EDIT' ? 'edit' : 'view').'/form2/'.$header['id']; ?>"><?= lang('button_evaluation_form_2') ?></a></li>
	<?php }else{ ?>
		<li class="active"><a href="<?= base_url().'evaluations/add/'.$header['id']; ?>"><?= lang('button_evaluation_form_1') ?></a></li>
		<li><a href="#"><?= lang('button_evaluation_form_2') ?></a></li>
	<?php } ?>
</ul>