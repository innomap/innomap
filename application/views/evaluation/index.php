<div class="block evaluation">
	<div class="title"><?= lang('title_evaluation_task') ?></div>
	<div class="block-content">
		<?php if($message){ ?>
			<div class="form-info success"><?= $message['msg']; ?></div>
		<?php } ?>
		<table cellpadding="0" cellspacing="0" border="0" class="display" id="evaluation_table" width="100%">
			<thead>
				<th><?= lang('label_no'); ?></th>
				<th><?= lang('label_innovation_name') ?></th>
				<th><?= lang('label_product_description') ?></th>
				<th><?= lang('label_innovator') ?></th>
				<th><?= lang('label_score_average') ?></th>
				<th><?= lang('label_status') ?></th>
				<th class="last"><?= lang('label_action') ?></th>
			</thead>
			<tbody>
				<?php $i=1;foreach($task as $row){ ?>
					<tr>
						<td><?= $i; ?></td>
						<td><?= $row['innovation_name']; ?></td>
						<td><?= $row['description']; ?></td>
						<td><?= $row['innovator']; ?></td>
						<td><?= $row['score_average'] ?></td>
						<td><?= ($row['status'] == EVALUATION_DRAFT ? 'Draft' : 'Done'); ?></td>
						<td class="last">
							<a href="#" class="view-detail-inno" inno_id="<?= $row['innovation_id'] ?>"><div class="view-icon" title="<?= lang('button_view_detail') ?>"></div></a>
							<a href="#" class="view-notes view-notes-icon" inno_id="<?= $row['innovation_id']; ?>" title="<?= lang('button_view_notes') ?>"></a>
							<?php if($row['evaluation_id'] == NULL){ ?>
								<a href="<?= base_url().'evaluations/add/'.$row['innovation_id']; ?>" title="<?= lang('button_evaluate') ?>" class="add-result-evaluation"></a>
							<?php }else if($row['status'] == EVALUATION_DONE){ ?>
								<a href="<?= base_url().'evaluations/view/form1/'.$row['evaluation_id'] ?>" class="view-result-evaluation" title="<?= lang('button_view_evaluation') ?>"></a>
							<?php }else{ ?>
								<a href="<?= base_url().'evaluations/edit/form1/'.$row['evaluation_id']; ?>" title="<?= lang('button_evaluate') ?>" class="add-result-evaluation"></a>
							<?php } ?>
						</td>
					</tr>
				<?php $i++;} ?>
			</tbody>
		</table>

		<div class="grid_6 margin-top">
			<a onclick="history.back();" class="btn red grid_1 omega alpha"><?= lang('button_back') ?></a>
		</div>
		<div class="clear"></div>
	</div>
</div>