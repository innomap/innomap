<div class="block tasklist">
	<div class="title"><?= lang('title_task_list') ?></div>
	<div class="block-content">
		<table cellpadding="0" cellspacing="0" border="0" class="display" id="tasklist_table" width="100%">
			<thead>
				<tr>
					<th><?= lang('label_no') ?></th>
					<th><?= lang('label_task') ?></th>
					<th class="last"><?= lang('label_action') ?></th>
				</tr>
			</thead>
			<tbody>
				<?php $i=1;foreach($tasks as $task){ ?>
					<tr>
						<td><?= $i; ?></td>
						<td><?= $task['title']; ?></td>
						<td class="last">
							<a href="<?= base_url().'evaluations/task/'.$task['expert_task_id']; ?>">
								<div class="view-icon" title="<?= lang('button_view_detail') ?>"></div>
							</a>
						</td>
					</tr>
				<?php $i++;} ?>
			</tbody>
		</table>
	</div>
</div>
