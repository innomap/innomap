<div class="block gallery">
	<div class="title"><?= $galleries[0]['name'] ?></div>
	<div class="block-content">
		<?php foreach($galleries as $key=>$value){ 
			if(file_exists(realpath(APPPATH . '../assets/attachment/innovation_picture') . DIRECTORY_SEPARATOR . $value['picture'])){ ?>
				<div class="grid_3">
					<img src="<?= base_url().PATH_TO_INNOVATION_PICTURE.$value['picture'] ?>" width="210px">
				</div>
		<?php 	}
			} ?>
		<div class="clear"></div>
		<br/><br/>
		<div class="grid_3">
			<a onclick="history.back();" class="btn red grid_1 omega alpha"><?= lang('button_back') ?></a>
		</div>
	</div>
</div>