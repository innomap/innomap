<div class="block gallery">
	<div class="title"><?=lang('title_gallery') ?></div>
	<div class="block-content">
		<div id="slider" class="flexslider highlight-slider">
		    <ul class="slides slider">
			  	<?php foreach($galleries as $key=>$value){ 
					if(file_exists(realpath(APPPATH . '../assets/attachment/innovation_picture') . DIRECTORY_SEPARATOR . $value['picture'])){ ?>
							<li>
								<img src="<?= base_url().PATH_TO_INNOVATION_PICTURE.$value['picture'] ?>" height="500px">
								<p class="flex-caption"><?= $value['name'] ?></p>
							</li>
				<?php 	}
					} ?>
		    </ul>
		 </div>

		 <div id="carousel" class="flexslider thumbnail-gallery">
			<ul class="slides">
				<?php foreach($galleries as $key=>$value){ 
					if(file_exists(realpath(APPPATH . '../assets/attachment/innovation_picture') . DIRECTORY_SEPARATOR . $value['picture'])){ ?>
							<li><img src="<?= base_url().PATH_TO_INNOVATION_PICTURE.$value['picture'] ?>" height="100px"></li>
				<?php 	}
					} ?>    
		    </ul>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(window).load(function() {
		 $('#carousel').flexslider({
		    animation: "slide",
		    controlNav: false,
		    animationLoop: false,
		    slideshow: false,
		    itemWidth: 120,
		    itemMargin: 20,
		    asNavFor: '#slider'
		  });
		 
		  $('#slider').flexslider({
		    animation: "slide",
		    controlNav: false,
		    animationLoop: false,
		    slideshow: true,
		    sync: "#carousel",
		    prevText: "",
			nextText: ""
		  });
	});
</script>