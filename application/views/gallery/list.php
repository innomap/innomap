<div class="block gallery">
	<div class="title"><?=lang('title_gallery') ?></div>
	<div class="block-content">
	<?php foreach($galleries as $key=>$value){ 
			if(file_exists(realpath(APPPATH . '../assets/attachment/innovation_picture') . DIRECTORY_SEPARATOR . $value['picture'])){ ?>
				<a href="<?= base_url().lang('link_gallery').'/detail/'.$value['innovation_id'] ?>">
					<div class="grid_3">
						<img src="<?= base_url().PATH_TO_INNOVATION_PICTURE.$value['picture'] ?>" height="200px" style="max-width:200px">
						<br/><span><?= $value['name'] ?></span>
					</div>
				</a>
	<?php 	}
		} ?>
	<div class="clear"></div>
	</div>
</div>