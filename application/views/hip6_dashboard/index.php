<div class="block innovator" style="padding-bottom:0">
	<div class="title"><?=lang('title_management_dashboard')?></div>
	<div class="block-content" style="padding:10px 0">
		<?php if(isset($innovations)){ ?>
		<div class="db-title"><h2><?= lang('innovation') ?></h2></div>
		<div class="innovation-portfolio-wrap">
			<input type="hidden" class="total-innovation" value="<?= $total_innovation; ?>">
			<?php foreach ($innovations as $key => $value) { ?>
					<div class="grid_4 omega">
					  	<label><?= $value['name_in_melayu']; ?></label>
					  	<input type="hidden" class="criteria-done" value="<?= $value['criteria_done'] ?>">
					  	<input type="hidden" class="criteria-in-progress" value="<?= $value['criteria_in_progress'] ?>">
					  	<div class="pie-chart innovation"></div>
					</div>
			<?php } ?>
		</div>
		<div class="grid_12 alpha omega load-more-wrap"><a href="#" class="lm-innovation"><?= lang('label_load_more').'...' ?></a></div>
		<?php } ?>
		<div class="clear"></div>
		<div class="portfolio-wrap">
			<?php if(isset($portfolios)){ ?>
			<div class="db-title"><h2><?= lang('title_portfolio') ?></h2></div>
			<input type="hidden" class="total-portfolio" value="<?= $total_portfolio; ?>">
			<input type="hidden" class="lang_manager" value="<?= lang('label_manager') ?>">
			<?php 	foreach ($portfolios as $key => $value) { ?>
					<div class="grid_4 omega">
					  	<label><?= $value['title']; ?></label>
					  	<div class="clear"></div>
					  	<label><?= lang('label_manager')." : ".$value['manager_name'] ?></label>
					  	<input type="hidden" class="criteria-done" value="<?= $value['criteria_done'] ?>">
					  	<input type="hidden" class="criteria-in-progress" value="<?= $value['criteria_in_progress'] ?>">
					  	<div class="pie-chart portfolio"></div>
					</div>
			<?php } ?>
		</div>
		<div class="grid_12 alpha omega load-more-wrap"><a href="#" class="lm-portfolio"><?= lang('label_load_more').'...' ?></a></div>
		<?php } ?>

		<div class="clear"></div>
	</div>
</div>

<div id="innovation_category" style="display:none">
	<table class="innovation_category biz-table" style="width:100%">
		<thead>
			<tr>
				<th>No</th>
				<th><?=lang('label_innovation_name'); ?></th>
				<th><?=lang('label_innovator')?></th>
				<th><?=lang('label_innovation_category')?></th>
				<th><?=lang('label_score_average')?></th>
				<th><?=lang('label_discovered_date') ?></th>
				<th></th>
			</tr>
		</thead>
		<tbody class="content-dialog"></tbody>
	</table>
</div>