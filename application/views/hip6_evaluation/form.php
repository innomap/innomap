<div class="block innovator green">
	<div class="title"><?= lang('title_evaluation_form') ?></div>
	<div class="block-content">
		<div class="detail-content grid_11 alpha">
			<?php if($message){ ?>
				<div class="form-info success"><?= $message['msg']; ?></div>
			<?php } ?>
			<div class="file-title"><center><b>HIP6 Innovation Evaluation Form</b></div>
			<form action="<?= base_url().'hip6_evaluations/save' ?>" id="form-hip6-evaluation" method="post">
				<input type="hidden" name="evaluation_id" value="<?= (isset($evaluation) ? $evaluation['hip6_evaluation_id'] : "") ?>">
				<input type="hidden" name="redirect_to_form" value="<?= $redirect_to_form ?>">
				<table border="0" cellspacing="0" cellpadding="0" class="table-hip6-evaluation green-table">
					<tr>
						<td><?= lang('label_business_analyst') ?></td>
						<td colspan="2">
							<?= (isset($evaluation) ? $evaluation['username'] : ($this->user_data['role_id'] == ROLE_BUSINESS_ANALYST ? $this->user_data['username'] : '-')) ?>
							<input type="hidden" name="ba_id" value="<?= (isset($evaluation) ? $evaluation['ba_id'] : $this->user_data['user_id']) ?>">
						</td>
					</tr>
					<tr>
						<td><?= lang('innovation') ?></td>
						<td colspan="2">
							<?= $innovation['name_in_melayu'] ?>
							<input type="hidden" name="innovation_id" value="<?= $innovation['innovation_id'] ?>">
						</td>
					</tr>
					<tr class="head">
						<th style="width:20%"><b><?= lang('label_evaluation_point') ?></b></th>
						<th style="width:55%"><b><?= lang('label_criteria') ?></b></th>
						<th><b><?= lang('label_point') ?></b></th>
					</tr>
					<tr>
						<td><b><?= lang('hip6_label_fa_1') ?></b></td>
						<td colspan="2">
							<table>
								<tr><td><b><?= lang('hip6_label_cr_1_1') ?></b></td></tr>
								<tr>
									<td width="70%"><?= lang('hip6_label_desc_1_1') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 10</i></div></td>
									<td width="30%"><input type="text" id="he-originality-innovation" name="originality_innovation" value="<?= (isset($evaluation) ? $evaluation['originality_innovation'] : 0) ?>" <?= (isset($view_mode) ? 'readonly' : '')?>></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td><b><?= lang('hip6_label_fa_2') ?></b></td>
						<td colspan="2">
							<table>
								<tr><td><b><?= lang('hip6_label_cr_2_1') ?></b></td></tr>
								<tr>
									<td width="70%"><?= lang('hip6_label_desc_2_1') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 10</i></div></td>
									<td width="30%"><input type="text" id="he-comparable" name="comparable" value="<?= (isset($evaluation) ? $evaluation['comparable'] : 0) ?>" <?= (isset($view_mode) ? 'readonly' : '')?>></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td></td>
						<td colspan="2">
							<table>
								<tr><td><b><?= lang('hip6_label_cr_2_2') ?></b></td></tr>
								<tr>
									<td width="70%"><?= lang('hip6_label_desc_2_2') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 10</i></div></td>
									<td width="30%"><input type="text" id="he-innovation-impact" name="innovation_impact" value="<?= (isset($evaluation) ? $evaluation['innovation_impact'] : 0) ?>" <?= (isset($view_mode) ? 'readonly' : '')?>></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td><b><?= lang('hip6_label_fa_3') ?></b></td>
						<td colspan="2">
							<table>
								<tr><td><b><?= lang('hip6_label_cr_3_1') ?></b></td></tr>
								<tr>
									<td width="70%"><?= lang('hip6_label_desc_3_1') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 10</i></div></td>
									<td width="30%"><input type="text" id="he-unit-cost" name="unit_cost" value="<?= (isset($evaluation) ? $evaluation['unit_cost'] : 0) ?>" <?= (isset($view_mode) ? 'readonly' : '')?>></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td></td>
						<td colspan="2">
							<table>
								<tr><td><b><?= lang('hip6_label_cr_3_2') ?></b></td></tr>
								<tr>
									<td width="70%"><?= lang('hip6_label_desc_3_2') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 10</i></div></td>
									<td width="30%"><input type="text" id="he-cost-of-assistance" name="cost_of_assistance" value="<?= (isset($evaluation) ? $evaluation['cost_of_assistance'] : 0) ?>" <?= (isset($view_mode) ? 'readonly' : '')?>></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td><b><?= lang('hip6_label_fa_4') ?></b></td>
						<td colspan="2">
							<table>
								<tr><td><b><?= lang('hip6_label_cr_4_1') ?></b></td></tr>
								<tr>
									<td width="70%"><?= lang('hip6_label_desc_4_1') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 10</i></div></td>
									<td width="30%"><input type="text" id="he-excluded-group" name="excluded_group" value="<?= (isset($evaluation) ? $evaluation['excluded_group'] : 0) ?>" <?= (isset($view_mode) ? 'readonly' : '')?>></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td><b><?= lang('hip6_label_fa_5') ?></b></td>
						<td colspan="2">
							<table>
								<tr><td><b><?= lang('hip6_label_cr_5_1') ?></b></td></tr>
								<tr>
									<td width="70%"><?= lang('hip6_label_desc_5_1') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 10</i></div></td>
									<td width="30%"><input type="text" id="he-market-size" name="market_size" value="<?= (isset($evaluation) ? $evaluation['market_size'] : 0) ?>" <?= (isset($view_mode) ? 'readonly' : '')?>></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td><b><?= lang('hip6_label_fa_6') ?></b></td>
						<td colspan="2">
							<table>
								<tr><td><b><?= lang('hip6_label_cr_6_1') ?></b></td></tr>
								<tr>
									<td width="70%"><?= lang('hip6_label_desc_6_1') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 10</i></div></td>
									<td width="30%"><input type="text" id="he-attitude-of-entrepreneur" name="attitude_of_entrepreneur" value="<?= (isset($evaluation) ? $evaluation['attitude_of_entrepreneur'] : 0) ?>" <?= (isset($view_mode) ? 'readonly' : '')?>></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td></td>
						<td colspan="2">
							<table>
								<tr><td><b><?= lang('hip6_label_cr_6_2') ?></b></td></tr>
								<tr>
									<td width="70%"><?= lang('hip6_label_desc_6_2') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 10</i></div></td>
									<td width="30%"><input type="text" id="he-diffusion-platform" name="diffusion_platform" value="<?= (isset($evaluation) ? $evaluation['diffusion_platform'] : 0) ?>" <?= (isset($view_mode) ? 'readonly' : '')?>></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td></td>
						<td colspan="2">
							<table>
								<tr><td><b><?= lang('hip6_label_cr_6_3') ?></b></td></tr>
								<tr>
									<td width="70%"><?= lang('hip6_label_desc_6_3') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 10</i></div></td>
									<td width="30%"><input type="text" id="he-strategic-linkages" name="strategic_linkages" value="<?= (isset($evaluation) ? $evaluation['strategic_linkages'] : 0) ?>" <?= (isset($view_mode) ? 'readonly' : '')?>></td>
								</tr>
								<tr>
									<td class="clm-total" width="70%"><b><?= lang('label_total') ?></b></td>
									<td width="30%"><input type="text" id="he-total" name="total" value="<?= (isset($evaluation) ? $evaluation['total'] : 0) ?>" readonly><div class="action_wrap"><span id="score-balance"><?= (isset($evaluation) ? 100-$evaluation['total'] : 100); ?></span> points available</div></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>

				<div class="label grid_9"><b><?= lang('label_comment') ?> : </b></div>
				<div class="value grid_10"><textarea class="grid_10 omega" name="comment" <?= (isset($view_mode) ? 'disabled' : '')?>><?=(isset($evaluation) ? $evaluation['comment'] : '')?></textarea></div>

				<div class="push_7 grid_4 margin-top action_wrap">
					<?php if($this->user_data['role_id'] == ROLE_BUSINESS_ANALYST){ ?>
						<input type="submit" class="submit-evaluation-1 btn" name="submit" value="<?= lang('button_save') ?>">
					<?php } ?>
					<a href="<?= base_url().'innovations' ?>" class="btn red grid_1 omega alpha"><?= lang('button_back') ?></a>
					<?php if(isset($evaluation)){ ?>
						<a class="btn print_page <?= ($this->user_data['role_id'] != ROLE_BUSINESS_ANALYST ? 'grid_1' : '') ?>"><?= lang('button_print') ?></a>
					<?php } ?>
				</div>
			</form>
		</div>
	</div>
</div>