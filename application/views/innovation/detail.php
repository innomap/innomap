<h1><?= lang('title_detail_innovation') ?></h1>

<h3><?= lang('label_in_bahasa_melayu') ?></h3>
<label><?= lang('label_data_source') ?> : </label>
<label><?= ($detail['data_source'] != 0 ? $data_sources[$detail['data_source']] : '-')?> </label> <br/>

<label><?= lang('label_innovation_name') ?> : </label>
<label><?=$detail['name_in_melayu']?> </label> <br/>

<label><?= lang('label_inspiration') ?> : </label>
<label><?=$detail['inspiration_in_melayu']?></label><br/>

<label><?= lang('label_product_description') ?> : </label>
<label><?=$detail['description_in_melayu']?></label><br/>

<label><?= lang('label_material') ?> : </label>
<label><?=$detail['materials_in_melayu']?></label><br/>

<label><?= lang('label_how_to_use') ?> : </label>
<label><?=$detail['how_to_use_in_melayu']?></label><br/>

<label><?= lang('label_special_achievement') ?> : </label>
<label><?=$detail['special_achievement_in_melayu']?></label><br/>

<!--<h3>English</h3>
<label><?= lang('label_innovation_name') ?> : </label>
<label><?=$detail['name']?></label> <br/>

<label><?= lang('label_inspiration') ?> : </label>
<label><?=$detail['inspiration']?></label><br/>

<label><?= lang('label_product_description') ?> : </label>
<label><?=$detail['description']?></label><br/>

<label><?= lang('label_material') ?> : </label>
<label><?=$detail['materials']?></label><br/>

<br/><br/>-->

<label><?= lang('label_created_date') ?> : </label>
<label><?=$detail['created_date']?></label><br/>

<label><?= lang('label_manufacturing_cost') ?> : </label>
<label>RM <?=number_format($detail['manufacturing_costs'],2)?></label><br/>

<label><?= lang('label_selling_price') ?> : </label>
<label>RM <?=number_format($detail['selling_price'],2)?></label><br/>

<label><?= lang('label_innovation_category') ?> : </label>
<label>
	<?php 
		foreach($inno_cats as $key=>$cat){
			foreach($categories as $category){ 
				echo ($cat['category_id'] == $category['category_id'] ? $key+1 .') '.$category['name'].'<br/>' : '');
			} ?>
			<div class="subcat-view">
				<p>Subcategory:</p>
				<?php 
				foreach($cat['subcategories'] as $sub){
					foreach($inno_subs as $inno_sub){ 
						echo ($inno_sub['subcategory_id'] == $sub['subcategory_id'] ? '-'.$sub['name'].'<br/>' : ''); 
					}
				} ?>
			</div>
	<?php } ?>
</label><br/>

<label><?= lang('label_target') ?> : </label>
<label><?php 
			if(json_decode($detail['target']) != NULL){
				foreach(json_decode($detail['target']) as $cur_target){
					echo "- ".$targets[$cur_target]."<br/>";
				}
			}?>
</label><br/>

<label><?= lang('label_myipo_protection') ?> : </label>
<label><?=($detail['myipo_protection'] == 1 ? lang('value_yes') : lang('value_no'))?></label><br/>

<label><?= lang('label_innovation_picture') ?> : </label>
<?php foreach($inno_pics as $pic){?>
	<img class="innovation_img" src="<?=base_url().PATH_TO_INNOVATION_PICTURE_THUMB."/".$pic['picture']?>" width="200"/>
<?php } ?><br/>	
