<div class="block innovator green">
	<div class="title"><?=$mode?> <?=lang('innovation')?></div>
	
	<div class="block-content">
		<?php if($flashdata != NULL){
			echo "<div class='form-info ".($flashdata['success'] ? 'success' : 'fail')."'>".$flashdata['msg']."</div>";
		} ?>
		
		<div class="detail-content grid_11 alpha">
			<form method="POST" action="<?=base_url()."innovations/save"?>" id="innovation" enctype="multipart/form-data">
				<?php if($mode == 'EDIT'){ ?>
					<input type="hidden" id="innovation_id" name="innovation_id" value="<?=$detail['innovation_id']?>" />
					<input type="hidden" id="status" name="status" value="<?= $detail['status'] ?>" />
				<?php } ?>
				<input type="hidden" name="innovator_id" value="<?=$innovator_id?>" />
				<input type="hidden" name="mode" value="<?=$mode?>" />
				
				<h3 class="grid_11"><?= lang('label_in_bahasa_melayu') ?></h3>

				<?php if($mode != 'ADD' || $this->user_sess['role_id'] != ROLE_DATA_ANALYST){ ?>
				<div class="label grid_3"><?= lang('label_data_source') ?> : </div>
				<div class="value grid_6">
					<select name="data_source" class="grid_4" <?= $this->user_sess['role_id'] != ROLE_DATA_ADMIN ? 'disabled' : '' ?>>
						<option value="0">-- <?= lang('value_select')." ".lang('label_data_source') ?> --</option>
						<?php foreach ($data_sources as $key => $value) { ?>
								<option value="<?= $key ?>" <?= ($mode != 'ADD' ? ($key == $detail['data_source'] ? 'selected' : '') : '') ?>><?= $value ?></option>
						<?php } ?>
					</select>
					<?php if($this->user_sess['role_id'] != ROLE_DATA_ADMIN){ ?>
						<input type="hidden" name="data_source" value="<?= $detail['data_source'] ?>">
					<?php } ?>
				</div>
				<?php } ?>
				
				<?php if(isset($innovator) && count($innovator) > 0){ ?>
					<div class="label grid_3"><?= lang('label_innovator') ?></div>
					<div class="value grid_6">
						<?php foreach ($innovator as $key => $value) {
							echo "- ".$value['name']."<br/>";
						} ?>
					</div>
				<?php } ?>

				<div class="label grid_3"><?= lang('label_innovation_name') ?>* : </div>
				<div class="value grid_6"><input type="text" class="grid_4" name="name_in_melayu" value="<?=($mode == 'EDIT' ? $detail['name_in_melayu'] : '')?>" /> </div>
				
				<div class="label grid_3"><?= lang('label_inspiration') ?>* : </div>
				<div class="value grid_6">
					<textarea name="inspiration_in_melayu" class="grid_4 text-count-elem" maxlength="500"><?=($mode == 'EDIT' ? $detail['inspiration_in_melayu'] : '')?></textarea> 
					<span class="text-counter"></span>
				</div>
				
				<div class="label grid_3"><?= lang('label_product_description') ?>* : </div>
				<div class="value grid_6">
					<textarea name="description_in_melayu" class="grid_4 text-count-elem" maxlength="500"><?=($mode == 'EDIT' ? $detail['description_in_melayu'] : '')?></textarea>
					<span class="text-counter"></span>
				</div>
				
				<div class="label grid_3"><?= lang('label_material') ?> : </div>
				<div class="value grid_6">
					<textarea name="materials_in_melayu" class="grid_4 text-count-elem" maxlength="500"><?=($mode == 'EDIT' ? $detail['materials_in_melayu'] : '')?></textarea>
					<span class="text-counter"></span>
				</div>
				
				<div class="label grid_3"><?= lang('label_how_to_use') ?> : </div>
				<div class="value grid_6">
					<textarea name="how_to_use_in_melayu" class="grid_4 text-count-elem" maxlength="500"><?=($mode == 'EDIT' ? $detail['how_to_use_in_melayu'] : '')?></textarea>
					<span class="text-counter"></span>
				</div>
				
				<div class="label grid_3"><?= lang('label_special_achievement') ?> : </div>
				<div class="value grid_6"><textarea class="grid_4" name="special_achievement_in_melayu"><?=($mode == 'EDIT' ? $detail['special_achievement_in_melayu']: '')?></textarea></div>
				
				<div class="label grid_3"><?= lang('label_created_date') ?>* : </div>
				<div class="value grid_6"><input type="text" name="created_date" class="grid_4 datepicker-dob" readonly value="<?=($mode == 'EDIT' ? $detail['created_date'] : '')?>" /></div>
				
				<div class="label grid_3"><?= lang('label_discovered_date') ?>* : </div>
				<div class="value grid_6"><input type="text" name="discovered_date" class="grid_4 datepicker-dob" readonly value="<?=($mode == 'EDIT' ? $detail['discovered_date'] : '')?>" /></div>

				<div class="label grid_3"><?= lang('label_manufacturing_cost')." (RM)" ?> : </div>
				<div class="value grid_6"><input type="text" class="grid_4" name="manufacturing_costs" value="<?=($mode == 'EDIT' ? $detail['manufacturing_costs'] : '')?>" /></div>
				
				<div class="label grid_3"><?= lang('label_selling_price')." (RM)" ?> : </div>
				<div class="value grid_6"><input type="text" class="grid_4" name="selling_price" value="<?=($mode == 'EDIT' ? $detail['selling_price'] : '')?>" /></div>
				
				<div class="label grid_3">
					<?= lang('label_innovation_category') ?> : <br/>
					<a href="#" id="add-category">(+) <?=lang('link_add_more_category')?></a>
				</div>
				
				<div class="value grid_6" id="category-value-wrapper">
					<?php if($mode == 'EDIT'){ 
						foreach($inno_cats as $key=>$cat){ ?>
							<select name="category_id[]" class="category">
								<option value="">- <?=lang('value_select')?> <?= lang('label_category') ?> -</option>
								<?php foreach($categories as $category){ 
									if($category['category_id'] != $inno_cats[$key-1]['category_id']){?>
										<option value="<?=$category['category_id']?>" <?=($cat['category_id'] == $category['category_id'] ? 'selected': '')?>><?=$category['name']?></option>
								<?php } 
								} ?>
							</select>
							<a href="#" class="remove-category" <?=($key == 0 ? 'style="display:none"' : '')?>>(-) Remove Category</a>
							<?=($key == 0 ? '<br/>' : '')?>
					<?php }
					}else{ ?>
						<select name="category_id[]" class="category">
							<option value="">- <?=lang('value_select')?> <?= lang('label_category') ?> -</option>
							<?php foreach($categories as $category){ ?>
								<option value="<?=$category['category_id']?>" <?=($mode == 'EDIT' && $detail['category_id'] == $category['category_id'] ? 'selected': '')?>><?=$category['name']?></option>
							<?php } ?>
						</select>					
					<?php } ?>
				</div>
				
				<div class="label grid_3"><?= lang('label_target') ?> : </div>
				<div class="value grid_6">
					<?php foreach($targets as $key=>$target){ ?>
						<input type="checkbox" name="target[]" value="<?=$key?>" <?=($mode == 'EDIT' && json_decode($detail['target']) != NULL ? (in_array($key,json_decode($detail['target'])) ? 'checked' : '') : '')?> ><?=$target?></input>
					<?php } ?>
				</div>
				
				<div class="label grid_3"><?= lang('label_myipo_protection') ?></div>
				<div class="value grid_6">
					<input type="radio" name="myipo_protection" value="1" <?=($mode == 'EDIT' && $detail['myipo_protection'] == 1 ? 'checked': '')?>><?=lang('value_yes')?></input>
					<input type="radio" name="myipo_protection" value="2" <?=($mode == 'EDIT' && $detail['myipo_protection'] == 2 ? 'checked': '')?>><?=lang('value_no')?></input>
				</div>
				<div class="clear"></div><br/>
				
				<div class="label grid_3">
					<?= lang('label_innovation_picture') ?>: <br/>
					<a href="#" id="add-innovation-pict">(+) <?=lang('link_add_more_pict')?></a>
				</div>
				<div class="value grid_6">
					<?php $pict_num = 3;
					if($mode == 'EDIT'){
						$pict_num = (count($inno_pics) > $pict_num ? count($inno_pics)-1 : $pict_num);
					}?>
					<input type="hidden" name="pict_num" value="<?=$pict_num?>"/>
					<div id="inno-pict-wrapper">
						<?php 
						$until = 4;
						if($mode == 'EDIT'){
							$until = (count($inno_pics) > $until ? count($inno_pics) : $until);
						}
						for($i=0;$i<$until;$i++){ 
							echo '<div>';
							$bef_pict = "";
							if($mode == 'EDIT'){
								if($i < count($inno_pics)){
									$bef_pict = $inno_pics[$i]['picture'];
									echo '<img width="200" src="'.base_url().PATH_TO_INNOVATION_PICTURE_THUMB.'/'.$bef_pict.'" />
									<a href="'.base_url()."innovations/delete/picture/".$inno_pics[$i]['innovation_picture_id'].'" onclick="return confirm_delete()">(-) '.lang('link_remove_picture').'</a>
									<br/>';
								}
							}?>
							<input type="file" name="picture_<?=$i?>" /> 
							<input type="hidden" name="bef_picture_<?=$i?>" value="<?=$bef_pict?>" />
							</div>
						<?php } ?>
					</div>
				</div>
				<div class="label grid_3"><?= lang('label_keyword') ?> : </div>
				<div class="value grid_6">
					<input type="text" class="grid_3" id="keyword-inp"/>
					<input type="button" class="btn" id="add-keyword-btn" value="Add" />
					<i class="grid_6"><?=lang('info_keyword')?></i>
					<ul id="keyword-list" style="list-style-type:none;margin:15px 0 0 -30px" class="grid_4">
						<?php if($mode == 'EDIT' && json_decode($detail['keyword']) != NULL){
							foreach(json_decode($detail['keyword']) as $keyword){ ?>
								<li>
									<input type="hidden" name="keyword[]" value="<?=$keyword?>"><?=$keyword?>
									<a href="#" class="remove-keyword">(-)<?=lang('link_remove')?></a>
								</li>
							<?php } 
						} ?>
					</ul>
				</div>
				<?php if($this->user_sess['role_id'] == ROLE_BUSINESS_ANALYST){ ?>
					<div class="label grid_3"><?= lang('label_ba_score') ?> : </div>
					<div class="value grid_6">
						<input type="text" class="grid_4" readonly value="<?= (isset($hip6_score) ? isset($hip6_score['total']) ? $hip6_score['total'] : 0 : 0) ?>">
						<?php if($mode == 'EDIT'){ ?>
							<a href="<?= base_url('hip6_evaluations/form/'.$detail['innovation_id'].'/1') ?>"><?= lang('label_change_score') ?></a>
						<?php } ?>
					</div>
				<?php } ?>


				<?php if($mode != 'ADD' && ($this->user_sess['role_id'] == ROLE_BUSINESS_ANALYST || $this->user_sess['role_id'] == ROLE_PROGRAM_DIRECTOR)){ ?>
					<div class="grid_8"><h3 id="note"><?= lang('label_recent_information_memo'); ?></h3></div>
					<div class="grid_10" style="margin-bottom:30px">
						<table cellpadding="0" cellspacing="0" border="0" class="display tbl_innovation_note" id="portfolio_table" width="100%">
							<thead>
								<th>No.</th>
								<th><?= lang('label_name') ?></th>
								<th><?= lang('label_information_memo') ?></th>
								<th><?= lang('label_last_updated') ?></th>
								<?php if($this->user_sess['role_id'] == ROLE_BUSINESS_ANALYST){ ?>
									<th><?= lang('label_action') ?></th>
								<?php } ?>
							</thead>
							<tbody>
							<?php $no = 1;foreach($notes as $key=>$value){ ?>
								<tr>
									<td><?= $no; ?></td>
									<td><?= $value['name'] ?></td>
									<td class="note_val"><?= $value['note'] ?></td>
									<td><?= $value['updated_at'] ?></td>
									<?php if($this->user_sess['role_id'] == ROLE_BUSINESS_ANALYST){ ?>
										<td>
											<a href="#" class="edit edit_note" data-id="<?= $value['innovation_note_id'] ?>" data-inno-id="<?= $detail['innovation_id'] ?>"></a>
											<a href="<?=base_url()."innovations/delete_information_note/".$value['innovation_note_id']."/".$detail['innovation_id']?>" onclick="return confirm_delete()" title="<?=lang('link_delete')?>" class="delete"></a>
										</td>
									<?php } ?>
								</tr>
							<?php $no++;} ?>
							</tbody>
						</table>
					</div>
				<?php } ?>

				<?php if($this->user_sess['role_id'] == ROLE_BUSINESS_ANALYST){ ?>
					<div class="label grid_3"><?= lang('label_information_memo') ?> : </div>
					<div class="value grid_6"><textarea name="information_memo" class="grid_6 alpha omega"></textarea></div>
				<?php } ?>
				
				<div class="grid_5 right">
					<a href="<?=base_url().($innovator_id == 0 ? "innovations" : "innovators/manage/innovation/".$innovator_id)?>" class="btn red grid_1 omega alpha"><?=lang('button_cancel')?></a>
					<input type="submit" class="btn" value="<?=lang('button_save')?>" />
				</div>
			</form>
		</div>
	</div>
</div>

<div id="progress-bar" title="Progressing...">
	<img src="<?=base_url()."assets/img/progress_bar.gif"?>" />
</div>

<!-- Edit Note Dialog -->
<div id="edit_note_dialog" style="display:none">
	<form method="POST" action="<?=base_url()."innovations/edit_information_note"?>" id="edit_note_form">
		<p><?=lang('label_information_memo')?>:</p>
		<input type="hidden" name="dlg_note_id" />
		<input type="hidden" name="dlg_innovation_id" />
		<textarea name="dlg_note" rows="10" cols="43"></textarea>
	</form>
	<style>.error{display:block}</style>
</div>