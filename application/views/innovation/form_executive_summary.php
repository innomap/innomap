<div class="block innovator green">
	<div class="title"><?=lang('title_executive_summary')?></div>
	
	<div class="block-content">
		<?php if($flashdata != NULL){
			echo "<div class='form-info ".($flashdata['success'] ? 'success' : 'fail')."'>".$flashdata['msg']."</div>";
		} ?>
		<div class="file-title"><center><b><?= lang('title_executive_summary') ?></b></div>
		<div class="detail-content grid_11 alpha">
			<form method="POST" action="<?=base_url()."innovations/save_executive_summary"?>" id="executive-summary">
				<input type="hidden" name="summary_id" value="<?= (isset($summary) ? $summary['summary_id'] : '-1') ?>">				
				<input type="hidden" name="innovation_id" value="<?=$innovation['innovation_id']?>" />

				<div class="label grid_3"><?= lang('label_innovation_name') ?> : </div>
				<div class="value grid_6"><?= $innovation['name_in_melayu'] ?> </div>

				<div class="label grid_3"><?= lang('label_innovator') ?> : </div>
				<div class="value grid_6">
					<?php foreach ($innovators as $key => $value) {
						echo "- ".$value['name'];
					} ?> 
				</div>
				
				<div class="label grid_3"><?= lang('title_executive_summary') ?> : </div>
				<div class="grid_6" style="margin-bottom:10px">
					<?php if(!$view_mode){ ?>
						<textarea name="summary" rows="20" style="width:100%"><?= (isset($summary) ? $summary['summary'] : '') ?></textarea>
					<?php }else{ ?>
						<p><?= (isset($summary) ? $summary['summary'] : '') ?></p>
					<?php } ?>
				</div>
				
				<div class="grid_5 right action_wrap">
					<?php if(!$view_mode){ ?>
						<a href="<?=base_url().'innovations'?>" class="btn red grid_1 omega alpha"><?=lang('button_cancel')?></a>
						<input type="submit" class="btn" value="<?=lang('button_save')?>" />
					<?php } ?>
					<?php if(isset($summary)){ ?>
						<a class="btn print_page"><?= lang('button_print') ?></a>
					<?php } ?>
				</div>
			</form>
		</div>
	</div>
</div>