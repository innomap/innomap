<div class="block innovator">
	<div class="title">
		<div style="<?= $access['add'] == true ? 'float:left' : '';?>"><?=lang('innovation')?></div>
		<div class="clear"></div>
	</div>
	<div class="block-content">
		<?php
			if($flashdata != NULL){
				echo "<div class='form-info ".($flashdata['success'] ? 'success' : 'fail')."'>".$flashdata['msg']."</div>";
			}
		?>
		<?php if($this->user_sess['role_id'] == ROLE_PROGRAM_DIRECTOR){ ?>
			<br />
			<div style="float:right;">
				<a href="<?= base_url().'innovations/list_innovation'; ?>" class="btn"><?= lang('edit_innovation_to_hip6'); ?></a>
			</div>
			<div class="clear"></div>
		<?php } ?>
		<table cellpadding="0" cellspacing="0" border="0" class="display" id="innovator_table" width="100%">
			<thead>
				<tr>
					<th width="5%">No.</th>
					<th width="25%"><?=lang('innovation')?></th>
					<th width="25%"><?=lang('innovator_name')?></th>
					<th width="30%"><?=lang('label_description')?></th>
					<th class="last" width="25%"><?=lang('action')?></th>
				</tr>
			</thead>
			<tbody>
				<?php 
					$no = 1;
					foreach($innovations as $key=>$value){ ?>
						<tr>
							<td><?=$no?></td>
							<td><?=$value['name']?></td>
							<td><?=($value['innovator'] == "" ? "-Tidak ada inovator-" : $value['innovator'])?></td>
							<td><?=ellipsis($value['description_in_melayu'],100)?></td>
							<td>
								<a href="<?=base_url()."innovations/view/".$value['innovation_id']."/1"?>" title="<?=lang('link_view')?>" class="view-icon"></a>
								<?php if($access['edit'] == 1){ ?>
									<a href="<?=base_url()."innovations/edit/0/".$value['innovation_id']?>" class="edit" title="Edit"></a>
								<?php } ?>
								
								<?php if($this->user_sess['role_id'] == ROLE_BUSINESS_ANALYST){ ?>
									<a href="<?=base_url()."innovations/executive_summary/".$value['innovation_id']?>" class="icon-write-file" title="Executive Summary"></a>
									<a href="<?=base_url()."hip6_evaluations/form/".$value['innovation_id']?>" class="add-result-evaluation" title="<?= lang('title_innovation_evaluation') ?>"></a>
								<?php } ?>

								<?php if($this->user_sess['role_id'] == ROLE_PROGRAM_DIRECTOR){ ?>
									<a href="<?=base_url()."hip6_evaluations/form/".$value['innovation_id']?>" class="view-result-evaluation" title="<?= lang('button_view_evaluation') ?>"></a>
									<?php if($value['hip6_status'] == INNO_DRAFT){ ?>
										<a href="#" class="hip6-approval" innovation_id="<?=$value['innovation_id']?>" status="<?=INNO_APPROVED?>"><?=lang('link_approve')?></a> |
										<a href="#" class="hip6-approval" innovation_id="<?=$value['innovation_id']?>" status="<?=INNO_REJECTED?>"><?=lang('link_reject')?></a>
									<?php }else if($value['hip6_status'] == INNO_APPROVED){ ?>
										<div class="icon-approved" title="<?= lang('label_approved') ?>">&nbsp;</div>
									<?php }else if($value['hip6_status'] == INNO_REJECTED){ ?>
										<div class="icon-rejected" title="<?= lang('label_rejected') ?>">&nbsp;</div>
									<?php } ?>
								<?php }else if($this->user_sess["role_id"] == ROLE_HIP6_STAKEHOLDER){ ?>
									<a href="<?=base_url()."hip6_evaluations/form/".$value['innovation_id']?>" class="view-result-evaluation" title="<?= lang('button_view_evaluation') ?>"></a>
									<a href="<?=base_url()."innovations/executive_summary/".$value['innovation_id']?>" class="icon-write-file" title="Executive Summary"></a>
								<?php } ?>
							</td>
						</tr>
					<?php 
					$no++;
					} ?>
			</tbody>
		</table>
	</div>
</div>

<div id="hip6-approval-note" style="display:none">
	<p><?=lang('approve_confirm1')?> <b id="caption"></b> <?=lang('approve_hip6_confirm2')?>.</p>
	<form method="POST" action="<?=base_url()."innovations/hip6_approval"?>" id="hip6-approval-form">
		<input type="hidden" name="app_innovation_id" />
		<input type="hidden" name="app_status" />
	</form>
	<style>.error{display:block}</style>
</div>