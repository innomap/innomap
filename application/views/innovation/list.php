<div class="block innovator green">
	<div class="title">
		<div style="float:left"><?=lang('title_section')?> E: <?=lang('title_innovation_form')?></div>
		<?php if($innovator['status'] != INNO_SENT_APPROVAL){ ?>
			<a href="<?=base_url()."innovations/add/".$id?>">
				<div class="add_innovator_button">
					<div><?=lang('add')." ".lang('innovation')?></div>
				</div>
			</a>
			<div class="clear"></div>
		<?php } ?>
		<div class="clear"></div>
	</div>
	
	<div class="block-content">
		<?=$links?>
		<div class="detail-content grid_11 alpha">
			<table cellpadding="0" cellspacing="0" border="0" class="display blue-table" width="97%" id="innovation-list">
				<thead>
					<tr>
						<th>No</th>
						<th><?=lang('label_innovation_name')?></th>
						<th><?=lang('label_score_average')?></th>
						<th>Status</th>
						<th><?=lang('action')?></th>
					</tr>
				</thead>
				<tbody>
					<?php 
					$no = 1;
					foreach($innovations as $innovation){ 
						//check innovation data is valid or not
						$valid_status = false;
						if($innovation['name_in_melayu'] != "" && $innovation['description_in_melayu'] != "" && $innovation['inspiration_in_melayu'] != ""){
							$valid_status = true;
						} ?>
						<tr>
							<td><?=$no?></td>
							<td><?=$innovation['name']?></td>
							<td><?=$innovation['score_average']?></td>
							<td><font color="<?=$statuses[$innovation['status']]['color']?>"><?=$statuses[$innovation['status']]['name']?></font></td>
							<td><?php if($innovator['status'] > INNO_SENT_APPROVAL && $innovation['status'] == INNO_DRAFT && $access['send_approval']){ ?>
									<a href="<?=base_url()."innovations/send_approval/".$innovation['innovation_id']?>" onclick="return approval_confirmation(<?=$valid_status?>)" ><?=lang('button_send')?></a>
								<?php } ?>
								<a href="<?=base_url()."innovations/view/".$innovation['innovation_id']?>" title="<?=lang('link_view')?>" class="view-icon"></a>
								<?php if($innovator['status'] != INNO_SENT_APPROVAL && $innovation['status'] != INNO_SENT_APPROVAL){ ?>
									<?php if($access['edit']){ ?>
										<a href="<?=base_url()."innovations/edit/".$id."/".$innovation['innovation_id']?>" class="edit" title="Edit"></a>
									<?php } ?>
									<?php if($access['add']){ ?>
										<a href="<?=base_url()."innovations/delete/innovation/".$innovation['innovation_id']?>" onclick="return confirm_delete()" title="<?=lang('link_delete')?>" class="delete"></a>
									<?php } ?>
								<?php }else{ ?>
									<?php if($access['approval']){ ?>
										<a href="#" class="approval" innovation_id="<?=$innovation['innovation_id']?>" status="<?=INNO_APPROVED?>"><?=lang('link_approve')?></a> |
										<a href="#" class="approval" innovation_id="<?=$innovation['innovation_id']?>" status="<?=INNO_REJECTED?>"><?=lang('link_reject')?></a> |
									<?php } ?>
								<?php } ?>
							</td>
						</tr>
					<?php 
					$no++;
					} ?>
				</tbody>
			</table>

			<form method="POST" action="<?=base_url()."innovators/send_approval"?>" id="send-approval">
				<input type="hidden" name="innovator_id" value="<?=$id?>" />
				<div class="clear"></div><br />
				<a href="<?=base_url()."innovators"?>" class="btn red grid_1 omega alpha" style="margin-left:10px;"><?=lang('button_cancel')?></a>
				<?php if($can_sent_ap){ ?>
					<input type="submit" class="btn" value="<?=lang('button_send')?>">
				<?php }else{ 
					if($innovator['status'] == INNO_DRAFT){ ?>
						<p>*<?=lang('info_before_send')?>.</p>
				<?php } 
				} ?>
			</form>
		</div>
	</div>
</div>

<div id="approval-note" style="display:none">
	<p><?=lang('approve_confirm1')?> <b id="caption"></b> <?=lang('approve_confirm2')?>.</p>
	<form method="POST" action="<?=base_url()."innovations/approval"?>" id="approval-form">
		<p><?=lang('label_notes')?>:</p>
		<input type="hidden" name="app_innovation_id" />
		<input type="hidden" name="app_status" />
		<textarea name="app_note" rows="6" cols="40"></textarea>
	</form>
	<style>.error{display:block}</style>
</div>