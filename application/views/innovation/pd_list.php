<div class="block innovator">
	<div class="title">
		<div><?=lang('innovation')?></div>
		<div class="clear"></div>
	</div>
	<div class="block-content">
		<?php
			if($flashdata != NULL){
				echo "<div class='form-info ".($flashdata['success'] ? 'success' : 'fail')."'>".$flashdata['msg']."</div>";
			}
		?>
		<div class="clear"></div>
		<table cellpadding="0" cellspacing="0" border="0" class="display" id="innovator_table" width="100%">
			<thead>
				<tr>
					<th width="5%">No.</th>
					<th width="35%"><?=lang('innovation')?></th>
					<th width="35%"><?=lang('innovator_name')?></th>
					<th width="35%"><?=lang('label_description')?></th>
					<th width="10%"><?=lang('label_data_source')?></th>
					<th class="last" width="10%"><?=lang('action')?></th>
				</tr>
			</thead>
			<tbody>
				<?php 
					$no = 1;
					foreach($innovations as $key=>$value){ ?>
						<tr>
							<td><?=$no?></td>
							<td><?=$value['name']?></td>
							<td><?=($value['innovator'] == "" ? "-Tidak ada inovator-" : $value['innovator'])?></td>
							<td><?=ellipsis($value['description_in_melayu'],100)?></td>
							<td><?= ($value['data_source'] != 0 ? $data_source[$value['data_source']] : ''); ?></td>
							<td>
								<a href="#" class="changeToHIP6" innoID="<?= $value['innovation_id']; ?>" style="color:blue;"><?= lang('change_source_to_hip6'); ?></a>
								<span style="display:none; color:green; font-size:9pt;"><?= lang('success_change_to_hip6');?></span>
							</td>
						</tr>
					<?php 
					$no++;
					} ?>
			</tbody>
		</table>
	</div>
</div>