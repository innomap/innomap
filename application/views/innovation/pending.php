<div class="block innovator">
	<div class="title">
		<div style="float:left;"><?=lang('label_pending_innovation'); ?></div>
		<div class="clear"></div>
	</div>
	<div class="block-content">
		<?php if($flashdata != NULL){
			echo "<div class='form-info ".($flashdata['success'] ? 'success' : 'fail')."'>".$flashdata['msg']."</div>";
		} ?>
		<table cellpadding="0" cellspacing="0" border="0" class="display" id="innovator_table" width="100%">
			<thead>
				<tr>
					<th>No.</th>
					<th><?=lang('label_innovation_name')?></th>
					<th><?=lang('innovator_name')?></th>
					<th class="last" width="20%"><?=lang('action')?></th>
				</tr>
			</thead>
			<tbody>
				<?php 
					$no = 1;
					foreach($innovations as $innovation){ ?>
						<tr>
							<td><?=$no?></td>
							<td><?=$innovation['name']?></td>
							<td><?=$innovation['innovator']?></td>
							<td>
								<a href="<?=base_url()."innovations/view/".$innovation['innovation_id']?>" title="<?=lang('link_view')?>" class="view-icon"></a>
								<?php if($access['approval']){ ?>
									<a href="#" class="approval" innovation_id="<?=$innovation['innovation_id']?>" status="<?=INNO_APPROVED?>"><?=lang('link_approve')?></a> |
									<a href="#" class="approval" innovation_id="<?=$innovation['innovation_id']?>" status="<?=INNO_REJECTED?>"><?=lang('link_reject')?></a> |
								<?php } ?>
							</td>
						</tr>
					<?php 
					$no++;
					} ?>
			</tbody>
		</table>
	</div>
</div>

<div id="approval-note" style="display:none">
	<p><?=lang('approve_confirm1')?> <b id="caption"></b> <?=lang('approve_confirm2')?>.</p>
	<form method="POST" action="<?=base_url()."innovations/approval"?>" id="approval-form">
		<p><?=lang('label_notes')?>:</p>
		<input type="hidden" name="app_innovation_id" />
		<input type="hidden" name="app_status" />
		<textarea name="app_note" rows="6" cols="40"></textarea>
	</form>
	<style>.error{display:block}</style>
</div>

<div id="note-viewer" title="Approval Note"></div>