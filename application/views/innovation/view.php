<div class="block innovator green">
	<div class="title"><?= lang('title_detail_innovation') ?></div>
	
	<div class="block-content">
		<div class="detail-content grid_9" style="padding-right:26px">

			<div class="label grid_3 print_title"><?= lang('label_data_source') ?> : </div>
			<div class="value grid_5 print_title"><?=($detail['data_source'] != 0 ? $data_sources[$detail['data_source']] : '-')?> </div>

			<div class="label grid_3 print_title"><?= lang('label_innovation_name') ?> : </div>
			<div class="value grid_5 print_title"><?=$detail['name_in_melayu']?> </div>

			<?php if(count($innovator) > 0){ ?>
				<div class="label grid_3"><?= lang('label_innovator') ?> : </div>
				<div class="value grid_5">
					<?php foreach ($innovator as $key => $value) {
						echo "- ".$value['name']."<br/>";
					} ?>
				</div>
			<?php } ?>

			<div class="label grid_3"><?= lang('label_inspiration') ?> : </div>
			<div class="value grid_5"><?=$detail['inspiration_in_melayu']?></div>

			<div class="label grid_3"><?= lang('label_product_description') ?> : </div>
			<div class="value grid_5"><?=$detail['description_in_melayu']?></div>

			<div class="label grid_3"><?= lang('label_material') ?> : </div>
			<div class="value grid_5"><?=$detail['materials_in_melayu']?></div>
			
			<div class="label grid_3"><?= lang('label_how_to_use') ?> : </div>
			<div class="value grid_5"><?=$detail['how_to_use_in_melayu']?></div>
			
			<br/><div class="label grid_3"><?= lang('label_special_achievement') ?> : </div>
			<div class="value grid_5"><?=$detail['special_achievement_in_melayu']?></div>

			<!--<h3 class="grid_9">English</h3>
			<div class="label grid_3"><?= lang('label_innovation_name') ?> : </div>
			<div class="value grid_5"><?=$detail['name']?></div>

			<div class="label grid_3"><?= lang('label_inspiration') ?> : </div>
			<div class="value grid_5"><?=$detail['inspiration']?></div>

			<div class="label grid_3"><?= lang('label_product_description') ?> : </div>
			<div class="value grid_5"><?=$detail['description']?></div>

			<div class="label grid_3"><?= lang('label_material') ?> : </div>
			<div class="value grid_5"><?=$detail['materials']?></div>
			
			<div class="label grid_3"><?= lang('label_how_to_use') ?> : </div>
			<div class="value grid_5"><?=$detail['how_to_use']?></div>
			
			<br/><div class="label grid_3"><?= lang('label_special_achievement') ?> : </div>
			<div class="value grid_5"><?=$detail['special_achievement']?></div>

			<div class="grid_9"><br/><br/></div>-->

			<div class="label grid_3"><?= lang('label_created_date') ?> : </div>
			<div class="value grid_5"><?=$detail['created_date']?></div>
			
			<div class="label grid_3"><?= lang('label_discovered_date') ?> : </div>
			<div class="value grid_5"><?=$detail['discovered_date']?></div>

			<div class="label grid_3"><?= lang('label_manufacturing_cost') ?> : </div>
			<div class="value grid_5">RM <?=number_format($detail['manufacturing_costs'], 2);?></div>
			
			<div class="label grid_3"><?= lang('label_selling_price') ?> : </div>
			<div class="value grid_5">RM <?=number_format($detail['selling_price'], 2)?></div>

			<div class="label grid_3"><?= lang('label_innovation_category') ?> : </div>
			<div class="value grid_5">
				<?php 
				foreach($inno_cats as $key=>$cat){
					foreach($categories as $category){ 
						echo ($cat['category_id'] == $category['category_id'] ? $key+1 .') '.$category['name'].'<br/>' : '');
					} ?>
					<div class="subcat-view">
						<p>Subcategory:</p>
						<?php 
						foreach($cat['subcategories'] as $sub){
							foreach($inno_subs as $inno_sub){ 
								echo ($inno_sub['subcategory_id'] == $sub['subcategory_id'] ? '-'.$sub['name'].'<br/>' : ''); 
							}
						} ?>
					</div>
				<?php } ?>
			</div>

			<div class="label grid_3"><?= lang('label_target') ?> : </div>
			<div class="value grid_5">
				<?php 
				if(json_decode($detail['target']) != NULL){
					foreach(json_decode($detail['target']) as $cur_target){
						echo "- ".$targets[$cur_target]."<br/>";
					}
				}else{
					echo '-';
				}?>
			</div>

			<div class="label grid_3"><?= lang('label_myipo_protection') ?> </div>
			<div class="value grid_5"><?=($detail['myipo_protection'] == 1 ? 'Ya': 'Tidak')?></div>
			<div class="clear"></div><br />

			<div class="label grid_3"><?= lang('label_innovation_picture') ?> : </div>
			<div class="value grid_5 image_wrap">
				<?php foreach($inno_pics as $pic){?>
					<img class="innovation_img" src="<?=base_url().PATH_TO_INNOVATION_PICTURE_THUMB."/".$pic['picture']?>" width="200"/>
				<?php } ?>
			</div>

			<?php if($this->user_sess['role_id'] == ROLE_BUSINESS_ANALYST || $this->user_sess['role_id'] == ROLE_PROGRAM_DIRECTOR){ ?>
				<div class="label grid_3"><?= lang('label_score') ?> : </div>
				<div class="value grid_5"><?= (isset($hip6_score) ? isset($hip6_score['total']) ? $hip6_score['total'] : 0 : 0) ?></div>
				<div class="clear"></div><br />

				<div class="grid_8"><h3><?= lang('label_recent_information_memo'); ?></h3></div>
				<div class="grid_9" style="margin-bottom:30px">
					<table cellpadding="0" cellspacing="0" border="0" class="display" id="portfolio_table" width="100%">
						<thead>
							<th>No.</th>
							<th><?= lang('label_name') ?></th>
							<th><?= lang('label_information_memo') ?></th>
							<th><?= lang('label_last_updated') ?></th>
						</thead>
						<tbody>
						<?php $no = 1;foreach($notes as $key=>$value){ ?>
							<tr>
								<td><?= $no; ?></td>
								<td><?= $value['name'] ?></td>
								<td><?= $value['note'] ?></td>
								<td><?= $value['updated_at'] ?></td>
							</tr>
						<?php $no++;} ?>
						</tbody>
					</table>
				</div>	
			<?php } ?>

			<div class="clear"></div>
			<div class="grid_3 right action_wrap">
				<?php if($show_back_btn == 1){ ?>
					<a onclick="history.back();" class="btn"><< <?= lang('button_back') ?></a>
				<?php } ?>
				<a class="btn print_page"><?= lang('button_print') ?></a>
			</div>
		</div>
	</div>
</div>