<div class="block innovator green">
	<div class="title"><?=lang('title_account_form')?></div>
	
	<div class="block-content">
		<?=$links?>

		<div class="detail-content grid_11 alpha">
			<form method="POST" action="<?=base_url()."innovators/save_account"?>" id="innovator-account">
				<input type="hidden" name="mode" value="<?=$mode?>" id="form-mode" />
				<?php if($mode == 'EDIT'){ ?>
					<input type="hidden" name="user_id" value="<?=$detail['user_id']?>" />
					<input type="hidden" name="innovator_status" value="<?= $innovator_status ?>">
				<?php } ?>
				
				<div class="label grid_3"><?=lang('label_username')?>* : </div>
				<div class="value grid_6"><input type="text" class="grid_4" name="username" value="<?=($mode == 'EDIT' ? $detail['username'] : '')?>" /> </div>
				
				<div class="label grid_3"><?=lang('label_email_address')?>* : </div>
				<div class="value grid_6"><input type="text" class="grid_4" name="email" value="<?=($mode == 'EDIT' ? $detail['email'] : '')?>" /> </div>
				
				<?php if($mode == 'EDIT'){ ?>
					<a href="#" id="edit-pass-link" class="grid_10">Ubah Kata Laluan</a>
				<?php } ?>
				
				<div id="password-wrapper" style="<?=($mode == 'EDIT' ? 'display:none' : '')?>">
					<div class="label grid_3"><?=lang('label_password')?>* : </div>
					<div class="value grid_6"><input type="password" class="grid_4" name="password" /> </div>
					
					<div class="label grid_3"><?=lang('label_retype')." ".lang('label_password')?>* : </div>
					<div class="value grid_6"><input type="password" class="grid_4" name="retype_password"/></div>
				</div>
				
				<div class="grid_7 right">
					<a class="btn red grid_1 omega alpha" href="<?=base_url()."innovators"?>"><?=lang('button_cancel')?></a>
					<input class="btn" type="submit" value="<?=lang('button_next_step')?> >>" />
				</div>
			</form>
		</div>
	</div>
</div>