<div class="block innovator green">
	<div class="title"><?=lang('title_section')?> C: <?=lang('title_company_form')?></div>
	<div class="block-content">
		<?=$links?>

		<div class="detail-content grid_11 alpha">
			<form method="POST" action="<?=base_url()."innovators/save_company"?>" id="innovator-company" enctype="multipart/form-data">
				<input type="hidden" name="innovator_id" value="<?=$detail['innovator_id']?>" />
				<input type="hidden" name="innovator_status" value="<?=$detail['status']?>" />
				
				<div class="label grid_3"><?=lang('label_name')?>* : </div>
				<div class="value grid_6"><input type="text" class="grid_4" name="c_name" value="<?=$detail['c_name']?>" /> </div>
				
				<div class="label grid_3"><?=lang('label_registration_no')?>* : </div>
				<div class="value grid_6"><input type="text" class="grid_4" name="c_registration_no" value="<?=$detail['c_registration_no']?>" /> </div>
				
				<div class="label grid_3"><?=lang('label_registration_date')?>* : </div>
				<div class="value grid_6"><input type="text" class="datepicker grid_4" name="c_registration_date" value="<?=$detail['c_registration_date']?>" class="datepicker-dob" readonly <?=($detail['c_registration_date'] != "0000-00-00" ? "disabled" : "")?> /> </div>
				
				<div class="label grid_3"><?=lang('label_operation_date')?>* : </div>
				<div class="value grid_6"><input type="text" class="datepicker grid_4"name="c_operation_date" value="<?=$detail['c_operation_date']?>" class="datepicker-dob" readonly <?=($detail['c_operation_date'] != "0000-00-00" ? "disabled" : "")?> /> </div>
				
				<div class="label grid_3"><?=lang('label_address')?>* : </div>
				<div class="value grid_6"><textarea name="c_address" class="grid_4"><?=$detail['c_address']?></textarea> </div>
				
				<div class="label grid_3"><?=lang('label_postcode')?>* : </div>
				<div class="value grid_6"><input type="text" class="grid_4" name="c_postcode" value="<?=$detail['c_postcode']?>" /> </div>
				
				<div class="label grid_3"><?=lang('label_city')?>* : </div>
				<div class="value grid_6"><input type="text" class="grid_4"name="c_city" value="<?=$detail['c_city']?>" /> </div>
				
				<div class="label grid_3"><?=lang('label_state')?>* : </div>
				<div class="value grid_6">
					<select name="c_state_id" class="grid_4">
						<?php foreach($states as $state){?>
							<option value="<?=$state['state_id']?>" <?=($detail['c_state_id'] == $state['state_id'] ? 'selected' : '')?>><?=$state['name']?></option>
						<?php } ?>
					</select>
				</div>
				
				<div class="label grid_3"><?=lang('label_telephone_no')?>* : </div>
				<div class="value grid_6"><input type="text" class="grid_4" name="c_telp_no" value="<?=$detail['c_telp_no']?>" /> </div>
				
				<div class="label grid_3"><?=lang('label_fax_no')?> : </div>
				<div class="value grid_6"><input type="text" class="grid_4" name="c_fax_no" value="<?=$detail['c_fax_no']?>" /> </div>
				
				<div class="label grid_3"><?=lang('label_email_address')?> : </div>
				<div class="value grid_6">
					<input type="text" class="grid_4" name="c_email" value="<?=$detail['c_email']?>" /> 
					<br/><br/><?=lang('info_example')?>: nama@contoh.com.my 
				</div>
				
				<div class="label grid_3"><?=lang('label_business_nature')?> : </div>
				<div class="value grid_6"><input type="text" class="grid_4" name="c_business_nature" value="<?=$detail['c_business_nature']?>" /> </div>
				
				<div class="label grid_3"><?=lang('label_registered_certification')?> : </div>
				<div class="value grid_6">
					<?=$bef_reg_cert?>
					<input type="hidden" name="bef_reg_cert" value="<?=$detail['c_registered_certification']?>" />
					<input type="file" class="grid_4" name="reg_cert" /> 
					<font class="grid_6"style="font-size:12px;color:red"><?=lang('info_allowed_file')?>: png,jpg,gif,bmp,jpeg,pdf,docx,doc</font>
				</div>
				
				<div class="label grid_3"><?=lang('label_funding_sources')?> : </div>
				<div class="value grid_6">
					<?php foreach($fund_sources as $key=>$fund_source){ ?>
						<input type="radio" name="c_funding_sources" value="<?=$key?>" <?=($detail['c_funding_sources'] == $key ? 'checked' : '')?>><?=$fund_source?></input>
					<?php } ?>
				</div>
				
				<div class="label grid_3"><?=lang('label_grant_loan_detail')?> : </div>
				<div class="value grid_6"><textarea name="c_grant_loan_detail" class="grid_4"><?=$detail['c_grant_loan_detail']?></textarea></div>
				
				<div class="grid_7 right">
					<a href="<?=base_url()."innovators"?>" class="btn red grid_1 omega alpha"><?=lang('button_cancel')?></a>
					<input type="submit" class="btn" value="<?=lang('button_next_step')?> >>" />
				</a>
			</form>
		</div>
	</div>
</div>