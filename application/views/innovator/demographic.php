<div class="block innovator green">
	<div class="title"><?=lang('title_section')?> B: <?=lang('title_demographic_form')?></div>
	<div class="block-content">
		<?=$links?>
		
		<div class="detail-content grid_11 alpha">
			<form method="POST" action="<?=base_url()."innovators/save_demographic"?>" id="innovator-demographic">
				<input type="hidden" name="innovator_id" value="<?=$detail['innovator_id']?>" />
				<input type="hidden" name="innovator_status" value="<?=$detail['status']?>" />

				<div class="label grid_3"><?=lang('label_location_type')?>* : </div>
				<div class="value grid_6">
					<select name="d_location_type" class="grid_4">
						<?php foreach($locations as $key=>$location){ ?>
							<option value="<?=$key?>" <?=($detail['d_location_type'] == $key ? 'selected' : '')?>><?=$location?></option>
						<?php } ?>
					</select>
				</div>
				
				<div class="label grid_3"><?=lang('label_home_phone')?> : </div>
				<div class="value grid_6"><input type="text" class="grid_4" name="d_home_phone_no" value="<?=$detail['d_home_phone_no']?>" /> </div>
				
				<div class="label grid_3"><?=lang('label_official_phone')?> : </div>
				<div class="value grid_6"><input type="text" class="grid_4" name="d_official_phone_no" value="<?=$detail['d_official_phone_no']?>" /> </div>
				
				<div class="label grid_3"><?=lang('label_mobile_phone')?>* : </div>
				<div class="value grid_6"><input type="text" class="grid_4" name="d_mobile_phone_no" value="<?=$detail['d_mobile_phone_no']?>" /> </div>
				
				<div class="label grid_3"><?=lang('label_fax_no')?> : </div>
				<div class="value grid_6"><input type="text" class="grid_4" name="d_fax_no" value="<?=$detail['d_fax_no']?>" /> </div>
				
				<div class="label grid_3"><?=lang('label_education_level')?>* : </div>
				<div class="value grid_6">
					<select name="d_education_level" class="grid_4"> 
						<?php foreach($educations as $key=>$education){ ?>
							<option value="<?=$key?>" <?=($detail['d_education_level'] == $key ? 'selected' : '')?>><?=$education?></option>
						<?php } ?>
					</select>
				</div>
				
				<div class="label grid_3"><?=lang('label_job')?> : </div>
				<div class="value grid_6"><input type="text" class="grid_4" name="d_employment" value="<?=$detail['d_employment']?>" /> </div>
				
				<div class="label grid_3"><?=lang('label_institution_name')?> : </div>
				<div class="value grid_6"><input type="text" class="grid_4" name="d_institution_name" value="<?=$detail['d_institution_name']?>" /> </div>
				
				<div class="label grid_3"><?=lang('label_employer')?> : </div>
				<div class="value grid_6"><input type="text" class="grid_4" name="d_employer" value="<?=$detail['d_employer']?>" /> </div>
				
				<div class="label grid_3"><?=lang('label_position')?> : </div>
				<div class="value grid_6"><input type="text" class="grid_4" name="d_position" value="<?=$detail['d_position']?>" /> </div>
				
				<div class="label grid_3"><?=lang('label_innovator_has')?> : </div>
				<div class="value grid_6">
					<input type="radio" name="d_innovator_has" value="1" <?=($detail['d_innovator_has'] == 1 ? 'checked' : '')?>><?=lang('value_yes')?></input> 
					<input type="radio" name="d_innovator_has" value="2" <?=($detail['d_innovator_has'] == 2 ? 'checked' : ($detail['d_innovator_has'] == 0 ? 'checked' : ''))?>><?=lang('value_no')?></input>
				</div>
				
				<div class="grid_7 right">
					<a href="<?=base_url()."innovators"?>" class="btn red grid_1 omega alpha"><?=lang('button_cancel')?></a>
					<input type="submit" class="btn" value="<?=lang('button_next_step')?> >>" />
				</div>
			</form>
		</div>
	</div>
</div>