<div class="block innovator green">
	<div class="title"><?=lang('title_section')?> D: <?=lang('title_heir_form')?></div>

	<div class="block-content">
		<?=$links?>
		
		<div class="detail-content grid_11 alpha">
			<form method="POST" action="<?=base_url()."innovators/save_heir"?>" id="innovator-heir">
				<input type="hidden" name="innovator_id" value="<?=$detail['innovator_id']?>" />
				<input type="hidden" name="innovator_status" value="<?=$detail['status']?>" />
				
				<div class="label grid_3"><?=lang('label_name')?>* : </div>
				<div class="value grid_6"><input type="text" class="grid_4" name="h_name" value="<?=$detail['h_name']?>" /> </div>
				
				<div class="label grid_3"><?=lang('label_identification_code_no')?>* : </div>
				<div class="value grid_6">
					<input type="text" name="kod_no1" class="grid_1" value="<?=(count($kod_no) == 3 ? $kod_no[0] : '')?>" size="6" maxlength="6" /> -
					<input type="text" name="kod_no2" value="<?=(count($kod_no) == 3 ? $kod_no[1] : '')?>" size="2" maxlength="2" /> -
					<input type="text" name="kod_no3" value="<?=(count($kod_no) == 3 ? $kod_no[2] : '')?>" size="4" maxlength="4" /><br/>
				</div>
				
				<div class="label grid_3"><?=lang('label_relationship')?>* : </div>
				<div class="value grid_6">
					<select name="h_relationship" class="grid_4">
						<?php foreach($relations as $key=>$relation){ ?>
							<option value="<?=$key?>" <?=($key == $detail['h_relationship'] ? 'selected' : '')?>><?=$relation?></option>
						<?php } ?>
					</select>
				</div>
				
				<div class="label grid_3"><?=lang('label_age')?> : </div>
				<div class="value grid_6"><input type="text" class="grid_4" id="age" name="h_age" value="<?=$detail['h_age']?>" readonly /> </div>
				
				<div class="label grid_3"><?=lang('label_mobile_phone')?>* : </div>
				<div class="value grid_6"><input type="text" class="grid_4" name="h_mobile_phone_no" value="<?=$detail['h_mobile_phone_no']?>" /> </div>
				
				<div class="label grid_3"><?=lang('label_job')?> : </div>
				<div class="value grid_6"><input type="text" class="grid_4" name="h_employment" value="<?=$detail['h_employment']?>" /> </div>
				
				<!--<label><?=lang('label_institution_name')?> : </label>
				<input type="text" name="h_institution_name" value="<?=$detail['h_institution_name']?>" /> <br/>
				
				<label><?=lang('label_employer')?> : </label>
				<input type="text" name="h_employer" value="<?=$detail['h_employer']?>" /> <br/>
				
				<label><?=lang('label_position')?> : </label>
				<input type="text" name="h_position" value="<?=$detail['h_position']?>" /> <br/>-->
				
				<div class="grid_7 right">
					<a href="<?=base_url()."innovators"?>" class="btn red grid_1 omega alpha"><?=lang('button_cancel')?></a>
					<input type="submit" class="btn" value="<?=lang('button_next_step')?> >>" />
				</div>
			</form>
		</div>
	</div>
</div>