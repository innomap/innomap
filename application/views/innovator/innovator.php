<div class="block innovator green">
	<div class="title"><?=lang('title_section')?> A: <?=lang('title_innovator_form')?></div>
	
	<div class="block-content">
		<?=$links?>

		<div class="detail-content grid_11 alpha">
			<form method="POST" action="<?=base_url()."innovators/save_innovator"?>" id="innovator" enctype="multipart/form-data">
				<input type="hidden" id="innovator_id" name="innovator_id" value="<?=$detail['innovator_id']?>" />
				<input type="hidden" id="innovator_status" name="innovator_status" value="<?=$detail['status']?>" />
				
				<div class="label grid_3"><?=lang('label_identity')?>* : </div>
				<div class="value grid_6">
					<select name="identity" id="innovator-identity" class="grid_4">
						<?php foreach($identities as $key=>$identity){ ?>
							<option value="<?=$key?>" <?=($detail['identity'] == $key ? 'selected' : '')?>><?=$identity?></option>
						<?php } ?>
					</select>
				</div>
				
				<div id="innovator-team-wrap" style="display:none;padding-bottom:2px;margin: 5px 20px">
					<div class="label grid_3"><?=lang('label_group_leader_name')?></div>
					<div class="value grid_6"><input type="text" class="grid_4" name="team_leader" value="<?=$detail['name']?>" /></div>
					<div class="label grid_3">
						<?=lang('label_group_expert_name')?><br/>
						<a href="#" id="add-team-expert">(+) <?=lang('link_add_more')?></a>
					</div>
					<div class="value grid_6">
						<?php if(count($team_experts) > 0 && $detail['identity'] == INNOVATOR_IDENTITY_TEAM){
							foreach($team_experts as $team_expert){?>
								<div class="each-team-expert">
									<input type="hidden" class="team-expert-id" name="team_expert_id[]" value="<?=$team_expert['team_expert_id']?>" />
									<input type="text" class="grid_4" name="team_expert[]" value="<?=$team_expert['name']?>" />
									<a href="#" class="remove-team-expert">(-) <?=lang('link_remove')?></a><br/><br/>
								</div>
						<?php }
						}else{ ?>
							<div class="each-team-expert">
								<input type="text" class="grid_4" name="team_expert[]"/>
								<a href="#" class="remove-team-expert">(-) <?=lang('link_remove')?></a><br/><br/>
							</div>
						<?php } ?>
					</div>
				</div>
				
				<div id="innovator-name-wrap">
					<div class="label grid_3"><?=lang('label_name')?>* : </div>
					<div class="value grid_6"><input type="text" class="grid_4" name="name" value="<?=$detail['name']?>" /> </div>
				</div>
				
				<div class="label grid_3"><?=lang('label_email_address')?>: </div>
				<div class="value grid_6">
					<input type="text" class="grid_4" name="email" value="<?=$detail['email']?>"/> 
					<br/><br/><?=lang('info_example')?>: nama@contoh.com.my
				</div>
				
				<div class="label grid_3"><?=lang('label_identification_code_no')?>* : </div>
				<div class="value grid_6">
					<input type="text" name="kod_no1" class="grid_1" value="<?=(count($kod_no) == 3 ? $kod_no[0] : '')?>" size="6" maxlength="6" /> -
					<input type="text" name="kod_no2" value="<?=(count($kod_no) == 3 ? $kod_no[1] : '')?>" size="2" maxlength="2" /> -
					<input type="text" name="kod_no3" value="<?=(count($kod_no) == 3 ? $kod_no[2] : '')?>" size="4" maxlength="4" /><br/>
				</div>
				
				<div class="label grid_3"><?=lang('label_gender')?>* : </div>
				<div class="value grid_6">
					<input type="radio" name="gender" value="1" <?=($detail['gender'] == 1 ? 'checked' : '')?>><?=lang('value_male')?></input>
					<input type="radio" name="gender" value="2" <?=($detail['gender'] == 2 ? 'checked' : '')?>><?=lang('value_female')?></input><br/>
				</div>
				
				<!--<label><?=lang('label_birth_date')?>* : </label>
				<input type="text" name="date_of_birth" class="datepicker-dob" readonly value="<?=$detail['date_of_birth']?>" /><br/>-->
				
				<div class="label grid_3"><?=lang('label_age')?>: </div>
				<div class="value grid_6"><input type="text" class="grid_4" id="age" name="age" readonly /> 
				</div>
				
				<div class="label grid_3"><?=lang('label_place')?> : </div>
				<div class="value grid_6"><input type="text" class="grid_4" name="place_of_birth" value="<?=$detail['place_of_birth']?>" /> </div>
				
				<div class="label grid_3"><?=lang('label_citizenship')?>* : </div>
				<div class="value grid_6">
					<select name="citizenship" class="grid_4">
						<?php foreach($citizenships as $key=>$citizenship){?>
							<option value="<?=$key?>" <?=($detail['citizenship'] == $key ? 'selected' : '')?>><?=$citizenship?></option>
						<?php } ?>
					</select>
				</div>
				
				<div class="label grid_3"><?=lang('label_race')?>* : </div>
				<div class="value grid_6">
					<select name="race" class="grid_4">
						<?php foreach($races as $key=>$race){?>
							<option value="<?=$key?>" <?=($detail['race'] == $key ? 'selected' : '')?>><?=$race?></option>
						<?php } ?>
					</select>
				</div>
				
				<div class="label grid_3"><?=lang('label_marital_status')?>* : </div>
				<div class="value grid_6">
					<select name="marital_status" class="grid_4">
						<?php foreach($marital_statuses as $key=>$marital_status){?>
							<option value="<?=$key?>" <?=($detail['marital_status'] == $key ? 'selected' : '')?>><?=$marital_status?></option>
						<?php } ?>
					</select>
				</div>
				
				<div class="label grid_3"><?=lang('label_address')?>* : </div>
				<div class="value grid_6"><textarea name="address" class="grid_4 address-geo"><?=$detail['address']?></textarea></div>
				
				<div class="label grid_3"><?=lang('label_postcode')?>* : </div>
				<div class="value grid_6"><input type="text" class="grid_4 address-geo" name="postcode" value="<?=$detail['postcode']?>"/> </div>
				
				<div class="label grid_3"><?=lang('label_city')?>* : </div>
				<div class="value grid_6"><input type="text" class="grid_4 address-geo" name="city" value="<?=$detail['city']?>"/> </div>
				
				<div class="label grid_3"><?=lang('label_state')?>* : </div>
				<div class="value grid_6">
					<select name="state_id" class="grid_4">
						<?php foreach($states as $state){?>
							<option value="<?=$state['state_id']?>" <?=($detail['state_id'] == $state['state_id'] ? 'selected' : '')?>><?=$state['name']?></option>
						<?php } ?>
					</select>
				</div>
				
				<div class="label grid_3">Geo Location* : </div>
				<div class="value grid_6">
					<input type="text" id="geo-location" class="grid_4" name="geo_location" value="<?=$detail['geo_location']?>" placeholder=" latitude, longitude"/>
					<a target="_blank" href="http://universimmedia.pagesperso-orange.fr/geo/loc.htm">Find Geo Location</a>
				</div>
				
				<div class="label grid_3"><?=lang('label_picture')?>: </div>
				<div class="value grid_6">
					<?php if($detail['picture'] != ""){ ?>
						<img src="<?=base_url().PATH_TO_INNOVATOR_PICTURE."/".$detail['picture']?>" class="innovator-img" width="200" />
					<?php } ?>
					<input type="hidden" name="bef_picture" value="<?=$detail['picture']?>" />
					<input type="file" name="picture" id="picture_innovator" />
					<!--<div class="fileUpload btn tosca">
						<span>Choose File</span>
						<input id="uploadBtn" type="file" class="upload" />
					</div>
					<input type="hidden" readonly id="uploadFile" name="picture" class="grid_3" style="background:transparent;position:relative" />-->
				</div>
				
				<div class="grid_7 right">
					<a class="btn red grid_1 omega alpha" href="<?=base_url()."innovators"?>"><?=lang('button_cancel')?></a>
					<input class="btn" type="submit" value="<?=lang('button_next_step')?> >>" />
				</div>
			</form>
		</div>
	</div>
</div>

<div id="progress-bar" title="Progressing...">
	<img src="<?=base_url()."assets/img/progress_bar.gif"?>" />
</div>