<div class="block innovator">
	<div class="title">
		<div style="<?= $access['add'] == true ? 'float:left' : '';?>"><?=lang('innovator')?></div>
		<?php if($access['add']){ ?>
			<a href="<?=base_url()."innovators/add/account"?>" class="reset_data_tables">
				<div class="add_innovator_button">
					<div><?=lang('add')." ".lang('innovator')?></div>
				</div>
			</a>
		<?php } ?>
		<div class="clear"></div>
	</div>
	<div class="block-content">
		<?php
			if($flashdata != NULL){
				echo "<div class='form-info ".($flashdata['success'] ? 'success' : 'fail')."'>".$flashdata['msg']."</div>";
			}
		?>
		<table cellpadding="0" cellspacing="0" border="0" class="display" id="innovator_table" width="100%">
			<thead>
				<tr>
					<th>No.</th>
					<th><?=lang('innovator_name')?></th>
					<th><?=lang('innovation')?></th>
					<th>Status</th>
					<th class="last" width="20%"><?=lang('action')?></th>
				</tr>
			</thead>
			<tbody>
				<?php 
					$no = 1;
					foreach($innovators as $innovator){ ?>
						<tr>
							<td><?=$no?></td>
							<td><?=$innovator['name']?></td>
							<td><?=($innovator['innovation'] == "" ? "-Tidak ada inovasi-" : $innovator['innovation'])?></td>
							<td><div class="<?= $statuses[$innovator['status']]['class'];?>" title="<?= $statuses[$innovator['status']]['name']; ?>">&nbsp;</div></td>
							<td class="last">
								<?php if($innovator['name'] != "" && $innovator['d_mobile_phone_no'] != "" && $innovator['h_name']){ ?>
									<a href="<?=base_url()."innovators/view/innovator/".$innovator['innovator_id']?>" class="view-icon" title="<?=lang('link_view')?>"></a>
								<?php } ?>
								<?php if($innovator['status'] == INNO_APPROVED){ ?>
									<input type="hidden" name="approval_note" value="<?=$innovator['approval_note']?>" />
									<a href="#" class="view-note-approval view-note" title="<?=lang('link_view')." ".lang('link_approval_note')?>"></a>
								<?php } ?>
								<?php if($innovator['status'] != INNO_SENT_APPROVAL && $innovator['status'] != INNO_REJECTED){ ?>
									<?php if($access['edit']){ ?>
										<a href="<?=base_url()."innovators/manage/account/".$innovator['innovator_id']?>" title="Edit" class="edit"></a>
									<?php } ?>
									<?php if($access['add']){ ?>
										<a href="<?=base_url()."innovators/delete/".$innovator['innovator_id']?>" onclick="return confirm_delete()" title="<?=lang('link_delete')?>" class="delete"></a>
									<?php } ?>
								<?php } ?>
								
								<?php if($innovator['status'] == INNO_SENT_APPROVAL){ ?>
									<?php if($access['approval']){ ?>
										<a href="#" class="approval" innovator_id="<?=$innovator['innovator_id']?>" status="<?=INNO_APPROVED?>"><?=lang('link_approve')?></a>
										<a href="#" class="approval" innovator_id="<?=$innovator['innovator_id']?>" status="<?=INNO_REJECTED?>"><?=lang('link_reject')?></a>
									<?php } ?>
								<?php } ?>					
							</td>
						</tr>
					<?php 
					$no++;
					} ?>
			</tbody>
		</table>
	</div>
</div>

<div id="approval-note" style="display:none">
	<p><?=lang('approve_confirm1')?> <b id="caption"></b> <?=lang('approve_confirm2')?>.</p>
	<form method="POST" action="<?=base_url()."innovators/approval"?>" id="approval-form">
		<p><?=lang('label_notes')?>:</p>
		<input type="hidden" name="app_innovator_id" />
		<input type="hidden" name="app_status" />
		<textarea name="app_note" rows="6" cols="40"></textarea>
	</form>
	<style>.error{display:block}</style>
</div>

<div id="note-viewer" title="Approval Note"></div>