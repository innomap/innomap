<div class="block innovator green">
	<?php foreach($forms as $form){
		if($form['url'] == $page){
			echo '<div class="title">'.lang('title_section').' '.$form['part'].': '.$form['name'].'</div>';
		}
	} ?>

	<div class="block-content">
		<ul class="grid_10" id="tabs">
			<?php foreach($forms as $form){ 
				if($form['url'] == $page){
					echo '<li class="active"><a href="#">Info '.$form['name'].'</a></li>';
				}else{
					echo '<li><a href="'.base_url().'innovators/view/'.$form['url'].'/'.$detail['innovator_id'].($show_back_btn == 0 ? '/0' : '').'">Info '.$form['name'].'</a> </li>';
				}
			} ?>
		</ul>
		
		<div class="detail-content grid_11 alpha" style="padding:15px 10px;">
		<!--INNOVATOR-->	
		<?php if($page == 'innovator'){ $next = 1; ?>
			<div class="label grid_3"><?=lang('label_picture')?>: </div>
			<div class="value grid_5">
				<?php if($detail['picture'] != ""){ ?>
					<img src="<?=base_url().PATH_TO_INNOVATOR_PICTURE."/".$detail['picture']?>" width="200" />
				<?php }else{ ?>
					<img src="<?=base_url()."assets/img/default avatar.jpg"?>" width="200" />
				<?php } ?>
			</div>
			
			<div class="label grid_3"><?=lang('label_identity')?>: </div>
			<div class="value grid_5"><?=$identities[$detail['identity']]?></div>
			
			<?php if($detail['identity'] == INNOVATOR_IDENTITY_TEAM){ ?>
				<div class="label grid_3"><?=lang('label_group_leader_name')?>: </div>
				<div class="value grid_5"><?=$detail['name']?></div>
				<div class="label grid_3"><?=lang('label_group_expert_name')?>: </div>
				<div class="value grid_5">
					<?php if($teams != NULL){
						foreach($teams as $team){
							echo '- '.$team['name'].'<br/>';
						}
					}else{ 
						echo $detail['name'];
					} ?>
				</div>
			<?php }else{ ?>
				<div class="label grid_3"><?=lang('label_name')?>: </div>
				<div class="value grid_5"><?=$detail['name']?></div>
			<?php } ?>
			
			<div class="label grid_3"><?=lang('label_email_address')?>: </div>
			<div class="value grid_5"><?=$detail['email']?></div>
			
			<div class="label grid_3"><?=lang('label_identification_code_no')?> : </div>
			<div class="value grid_5"><?=$detail['kod_no']?></div>
			
			<div class="label grid_3"><?=lang('label_gender')?> : </div>
			<div class="value grid_5"><?=($detail['gender'] == 1 ? 'Lelaki' : 'Perempuan')?></div>
			
			<div class="label grid_3"><?=lang('label_age')?>: </div>
			<div class="value grid_5"><?=$detail['age']." ".lang('info_years_old')?></div>
			
			<div class="label grid_3"><?=lang('label_place')?> : </div>
			<div class="value grid_5"><?=$detail['place_of_birth']?></div>
			
			<div class="label grid_3"><?=lang('label_citizenship')?> : </div>
			<div class="value grid_5"><?=$citizenships[$detail['citizenship']]?></div>
			
			<div class="label grid_3"><?=lang('label_race')?> : </div>
			<div class="value grid_5"><?=$races[$detail['race']]?></label></div>
			
			<div class="label grid_3"><?=lang('label_marital_status')?> : </div>
			<div class="value grid_5"><?=$marital_statuses[$detail['marital_status']]?></div>
			
			<div class="label grid_3"><?=lang('label_address')?> : </div>
			<div class="value grid_5"><?=$detail['address']?></div>
			
			<div class="label grid_3"><?=lang('label_postcode')?> : </div>
			<div class="value grid_5"><?=$detail['postcode']?></div>
			
			<div class="label grid_3"><?=lang('label_city')?> : </div>
			<div class="value grid_5"><?=$detail['city']?></div>
			
			<div class="label grid_3"><?=lang('label_state')?> : </div>
			<div class="value grid_5"><?=$states[$detail['state_id']-1]['name']?></div>

		<!--DEMOGRAPHIC-->		
		<?php }elseif($page == 'demographic'){ $next = ($detail['c_name'] != "" ? 2 : 3); ?>
			<div class="label grid_3"><?=lang('label_location_type')?> : </div>
			<div class="value grid_5"><?=$locations[$detail['d_location_type']]?></div>
			
			<div class="label grid_3"><?=lang('label_home_phone')?> : </div>
			<div class="value grid_5"><?=$detail['d_home_phone_no']?></div>
			
			<div class="label grid_3"><?=lang('label_official_phone')?> : </div>
			<div class="value grid_5"><?=$detail['d_official_phone_no']?></div>
			
			<div class="label grid_3"><?=lang('label_mobile_phone')?>: </div>
			<div class="value grid_5"><?=$detail['d_mobile_phone_no']?></div>
			
			<div class="label grid_3"><?=lang('label_fax_no')?> : </div>
			<div class="value grid_5"><?=$detail['d_fax_no']?></div>
			
			<div class="label grid_3"><?=lang('label_education_level')?> : </div>
			<div class="value grid_5"><?=$educations[$detail['d_education_level']]?></div>
			
			<div class="label grid_3"><?=lang('label_job')?> : </div>
			<div class="value grid_5"><?=$detail['d_employment']?></div>
			
			<div class="label grid_3"><?=lang('label_institution_name')?> : </div>
			<div class="value grid_5"><?=$detail['d_institution_name']?></div>
			
			<div class="label grid_3"><?=lang('label_employer')?> : </div>
			<div class="value grid_5"><?=$detail['d_employer']?></div>
			
			<div class="label grid_3"><?=lang('label_position')?> : </div>
			<div class="value grid_5"><?=$detail['d_position']?></div>
			
			<div class="label grid_3"><?=lang('label_innovator_has')?> : </div>
			<div class="value grid_5"><?=($detail['d_innovator_has'] == 1 ? 'Ya' : 'Tidak')?></div>

		<!--COMPANY-->	
		<?php }elseif($page == 'company'){ $next = 3; ?>
			<div class="label grid_3"><?=lang('label_name')?> : </div>
			<div class="value grid_5"><?=$detail['c_name']?></div>
			
			<div class="label grid_3"><?=lang('label_registration_no')?> : </div>
			<div class="value grid_5"><?=$detail['c_registration_no']?></div>
			
			<div class="label grid_3"><?=lang('label_registration_date')?> : </div>
			<div class="value grid_5"><?=$detail['c_registration_date']?></div>
			
			<div class="label grid_3"><?=lang('label_operation_date')?> : </div>
			<div class="value grid_5"><?=$detail['c_operation_date']?></div>
			
			<div class="label grid_3"><?=lang('label_address')?> : </div>
			<div class="value grid_5"><?=$detail['c_address']?></div>
			
			<div class="label grid_3"><?=lang('label_postcode')?> : </div>
			<div class="value grid_5"><?=$detail['c_postcode']?></div>
			
			<div class="label grid_3"><?=lang('label_city')?> : </div>
			<div class="value grid_5"><?=$detail['c_city']?></div>
			
			<div class="label grid_3"><?=lang('label_state')?> : </div>
			<div class="value grid_5"><?=$states[$detail['c_state_id']-1]['name']?></div>
			
			<div class="label grid_3"><?=lang('label_telephone_no')?> : </div>
			<div class="value grid_5"><?=$detail['c_telp_no']?></div>
			
			<div class="label grid_3"><?=lang('label_fax_no')?> : </div>
			<div class="value grid_5"><?=$detail['c_fax_no']?></div>
			
			<div class="label grid_3"><?=lang('label_email_address')?> : </div>
			<div class="value grid_5"><?=$detail['c_email']?></div>
			
			<div class="label grid_3"><?=lang('label_business_nature')?> : </div>
			<div class="value grid_5"><?=$detail['c_business_nature']?></div>
			
			<div class="label grid_3"><?=lang('label_registered_certification')?> : </div>
			<div class="value grid_5"><?=$detail['reg_cert']?></div>
			
			<div class="label grid_3"><?=lang('label_funding_sources')?> : </div>
			<div class="value grid_5"><?=($detail['c_funding_sources'] > 0 ? $fund_sources[$detail['c_funding_sources']] : '-')?></div>
			
			<div class="label grid_3"><?=lang('label_grant_loan_detail')?> : </div>
			<div class="value grid_5"><?=$detail['c_grant_loan_detail']?></div>
			
		<!--HEIR-->
		<?php }elseif($page == 'heir'){ $next = 4; ?>
			<div class="label grid_3"><?=lang('label_name')?> : </div>
			<div class="value grid_5"><?=$detail['h_name']?></div>
			
			<div class="label grid_3"><?=lang('label_identification_code_no')?> : </div>
			<div class="value grid_5"><?=$detail['h_kod_no']?></div>
			
			<div class="label grid_3"><?=lang('label_relationship')?> : </div>
			<div class="value grid_5"><?=$relations[$detail['h_relationship']]?></div>
			
			<div class="label grid_3"><?=lang('label_age')?> : </div>
			<div class="value grid_5"><?=$detail['h_age']." ".lang('info_years_old')?> </div>
			
			<div class="label grid_3"><?=lang('label_mobile_phone')?> : </div>
			<div class="value grid_5"><?=$detail['h_mobile_phone_no']?></div>
			
			<div class="label grid_3"><?=lang('label_job')?> : </div>
			<div class="value grid_5"><?=$detail['h_employment']?></div>
			
		<!--INNOVATION-->
		<?php }else{ $next = 0; ?>
			<table cellpadding="0" cellspacing="0" border="0" class="display" id="portfolio_table" width="100%">
				<thead>
					<tr>
						<th>No</th>
						<th><?=lang('label_innovation_name')?></th>
						<th><?=lang('label_score_average')?></th>
						<th>Status</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php 
					$no = 1;
					foreach($innovations as $innovation){ ?>
						<tr>
							<td><?=$no?></td>
							<td><?=$innovation['name']?></td>
							<td><?=$innovation['score_average']?></td>
							<td><font color="<?=$statuses[$innovation['status']]['color']?>"><?=$statuses[$innovation['status']]['name']?></font></td>
							<td><a href="<?=base_url()."innovations/view/".$innovation['innovation_id']?>"><?=lang('label_view_detail'); ?></a></td>
						</tr>
					<?php 
					$no++; } ?>
				</tbody>
			</table>
			<br/>
			<br/>
			<br/>
			<div class="clear"></div>
		<?php } ?>
		
		<div class="clear"></div>
		<div style="text-align:right">
			<?php if($show_back_btn == 1){ ?>
				<a class="btn" href="<?=base_url()."innovators"?>"><?=lang('button_back')?></a> 
			<?php } ?>
			<?php if($next > 0){ ?>
				<a class="btn" href="<?=base_url()."innovators/view/".$forms[$next]['url']."/".$detail['innovator_id'].($show_back_btn == 0 ? '/0' : '')?>"><?=lang('button_next_step')?> >> </a>
			<?php } ?>
		</div>
	</div>
	<div class="clear"></div>
</div>