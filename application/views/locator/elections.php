<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="<?= base_url()."assets/js/front/locator/markerwithlabel.js"; ?>"></script>
<script type="text/javascript" src="<?= base_url()."assets/js/front/locator/map.js"; ?>"></script>

<div class="block innovator green">
  <div class="title"><?= lang('title_elections_map') ?></div>
  <div class="block-content">
    <div class="main-map" style="padding-bottom:20px;" content_map="elections">
    	<form method="POST" id="form-elections-map">
    		<div id="search-box" class="search" class="row square">
    			<div class="row">
    				<br/>
              <b><?=lang('label_sort_by')?></b> 
            <select id="sort_by">
              <option value="P">Parlimen</option>
              <option value="N">DUN</option>
            </select>
    				<b><?=lang('label_political_party')?></b> 
    				<select id="political_party">
              <option value="5">All</option>
    					<option value="2">Barisan Nasional</option>
    					<option value="3">Pakatan Rakyat</option>
    				</select>
            <b><?=lang('label_state').": "?></b>
            <select id="state">
              <option value="0" latlong="0">All</option>
              <?php foreach($states as $state){ ?>
                <option value="<?= $state['state_id'] ?>" latlong="<?= $state['geo_location']; ?>" <?= ($state['state_id'] == 10 ? 'selected' : ''); ?>><?= $state['name']; ?></option>
              <?php } ?>
            </select>
    			</div>
          <div class="row">
            <b><input type="checkbox" id="toggle-select-cat" checked> <?=lang('label_display_inno'); ?></b><br/>
            <div id="category-wrap" class="category_wrap">
              <input type="checkbox" class="category-value" name="category_id[]" value="0" checked> All <br/>
            <?php foreach($categories as $row){ ?>
              <input type="checkbox" class="category-value" name="category_id[]" value="<?= $row->category_id; ?>"> <?= $row->name; ?> <br/>
            <?php } ?>
            </div><br/>
             <a id="submit-elections-form" class="btn" style="margin-top:25px"><?=lang('label_search'); ?></a>
          </div>
    		</div>
    	</form>
      <br/>
      <div id="elections-googft-mapCanvas"></div>
       <div class="legend-wrap">
          <div class="legend-column all">
            <div class="box-wrap" style="font-weight:bold"><?=lang('label_political_party')?></div>
            <div class="box-wrap" style="font-weight:bold"><div class="legend-box bn-color"></div>Barisan Nasional</div>
            <div class="box-wrap"><img src="<?= base_url() ?>assets/img/map_marker/party/25-umno.jpg"><span>&nbsp United Malays National Organisation</span></div>
            <div class="box-wrap"><img src="<?= base_url() ?>assets/img/map_marker/party/25-mca.jpg"><span>&nbsp Malaysian Chinese Association</span></div>
            <div class="box-wrap"><img src="<?= base_url() ?>assets/img/map_marker/party/25-mic.jpg"><span>&nbsp Malaysian Indian Congress</span></div>
            <br/>
            <div class="box-wrap" style="font-weight:bold"><div class="legend-box pkr-color"></div>Pakatan Rakyat</div>
            <div class="box-wrap"><img src="<?= base_url() ?>assets/img/map_marker/party/25-pkr.jpg"><span>&nbsp Parti Keadilan Rakyat</span></div>
            <div class="box-wrap"><img src="<?= base_url() ?>assets/img/map_marker/party/25-dap.jpg"><span>&nbsp Democratic Action Party</span></div>
            <div class="box-wrap"><img src="<?= base_url() ?>assets/img/map_marker/party/25-pas.jpg"><span>&nbsp Parti Islam Se-Malaysia</span></div>
            <br/>
            <div class="box-wrap" style="font-weight:bold"><div class="legend-box others-color"></div>Others</div>
            <div class="box-wrap"><img src="<?= base_url() ?>assets/img/map_marker/party/25-star.jpg"><span>&nbsp STAR</span></div>
            <div class="box-wrap"><img src="<?= base_url() ?>assets/img/map_marker/party/25-bebas.jpg"><span>&nbsp Bebas</span></div>
          </div>
          <div class="legend-column percentage" style="display:none">
            <div class="box-wrap" style="font-weight:bold"><?=lang('label_vote_percentage'); ?></div>
            <div class="box-wrap"><div class="legend-box style2 color-1"></div><span>0% - 20%</span></div>
            <div class="box-wrap"><div class="legend-box style2 color-2"></div><span>21% - 40%</span></div>
            <div class="box-wrap"><div class="legend-box style2 color-3"></div><span>41% - 60%</span></div>
            <div class="box-wrap"><div class="legend-box style2 color-4"></div><span>61% - 80%</span></div>
            <div class="box-wrap"><div class="legend-box style2 color-5"></div><span>81% - 100%</span></div>
          </div>
          <div class="legend-column">
            <div class="box-wrap" style="font-weight:bold"><?=lang('label_locator'); ?></div>
            <div class="box-wrap"><img src="<?= base_url()."assets/img/map_marker/red-tri.png"; ?>"><span><?=lang('label_university'); ?></span></div>
            <div class="box-wrap"><img src="<?= base_url()."assets/img/map_marker/square-yellow.png"; ?>"><span><?=lang('label_expert'); ?></span></div>
          </div>
          <div class="legend-column">
            <div class="box-wrap" style="font-weight:bold"><?=lang('label_inno_category'); ?></div>
            <?php foreach($categories as $category){?>
              <div class="box-wrap"><img src="<?= base_url()."assets/img/map_marker/".$category->pin_url; ?>"><span><?= $category->name; ?></span></div>
            <?php } ?>
          </div>
        </div>
    </div>
  </div>
</div>

<!--for label language-->
<div>
  <input type="hidden" id="label_innovation" value="<?=lang('label_innovation')?>">
  <input type="hidden" id="label_category" value="<?=lang('label_category')?>">
  <input type="hidden" id="label_address" value="<?=lang('label_address')?>">
  <input type="hidden" id="label_state" value="<?=lang('label_state')?>">
  <input type="hidden" id="label_university" value="<?=lang('label_university')?>">
  <input type="hidden" id="label_contact_no" value="<?=lang('label_contact_no')?>">
  <input type="hidden" id="label_expert" value="<?=lang('label_expert')?>">
  <input type="hidden" id="label_expert_name" value="<?=lang('label_expert')?>">
  <input type="hidden" id="label_expertise" value="<?=lang('label_expertise')?>">
  <input type="hidden" id="label_experience" value="<?=lang('label_experience')?>">
  <input type="hidden" id="label_innovator" value="<?=lang('innovator')?>">
</div>