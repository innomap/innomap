<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;v=3"></script>
<script type="text/javascript" src="<?= base_url()."assets/js/front/locator/markerwithlabel.js"; ?>"></script>
<script type="text/javascript" src="<?= base_url()."assets/js/front/locator/map.js" ?>"></script>

<div class="block innovator green">
	<div class="title"><?= lang('title_map') ?></div>
	<div class="block-content">
		<div class="main-map" style="padding-bottom:20px;" content_map="state_income">
			<form method="POST" id="form-filter-map">
				<div id="search-box" class="search" class="row square">
					<div class="row">
						<br/>
						<b><?=lang('label_select_gsp_year')?></b> 
						<select id="gsp_year" name="gsp_year">
							<option value="2">2010</option>
							<option value="3">2009</option>
							<option value="4">2008</option>
							<option value="5">2007</option>
							<option value="6">2006</option>
							<option value="7">2005</option>
						</select>
					</div>
					<div class="row">
						<b><?=lang('label_select_category')?></b><br/>
						<div class="category_wrap">
							<input type="checkbox" class="category-value" name="category_id[]" value="0" checked> All <br/>
						<?php foreach($categories as $row){ ?>
							<input type="checkbox" class="category-value" name="category_id[]" value="<?= $row->category_id; ?>"> <?= $row->name; ?> <br/>
						<?php } ?>
						</div><br/>
					</div>
					<div class="row">
						<b><?= lang('commercial_center')?>:</b> <input type="checkbox" id="commercial_filter"><?= lang('commercial_center')?><br /><br />
							<select id="commercial_center" name="commercial_center" style="display:none; width:200px;">
								<option value="0">All</option>
								<option value="1">UTC</option>
								<option value="2">RTC</option>
							</select><br /><br />
						<a id="submit-filter-form" class="btn"><?=lang('label_search'); ?></a>
					</div>
				</div>
			</form><br/>
		  <div id="googft-mapCanvas"></div>
		  <div class="legend-wrap">
				<div class="legend-column">
					<div class="box-wrap" style="font-weight:bold">Income (USD)</div>
					<div class="box-wrap"><div class="legend-box income color-1"></div><span>0 - 20,000</span></div>
					<div class="box-wrap"><div class="legend-box income color-2"></div><span>20,000 - 40,000</span></div>
					<div class="box-wrap"><div class="legend-box income color-3"></div><span>40,000 - 60,000</span></div>
					<div class="box-wrap"><div class="legend-box income color-4"></div><span>60,000 - 80,000</span></div>
					<div class="box-wrap"><div class="legend-box income color-5"></div><span>80,000 - 100,000</span></div>
					<div class="box-wrap"><div class="legend-box income color-6"></div><span>> 100,000</span></div>
				</div>
				<div class="legend-column">
					<div class="box-wrap" style="font-weight:bold">Location</div>
					<div class="box-wrap"><img src="<?= base_url()."assets/img/map_marker/red-tri.png"; ?>"><span>University</span></div>
					<div class="box-wrap"><img src="<?= base_url()."assets/img/map_marker/square-yellow.png"; ?>"><span>Subject Matter Expert</span></div>
				</div>
				<div class="legend-column">
					<div class="box-wrap" style="font-weight:bold">Innovation Category</div>
					<?php foreach($categories as $category){?>
						<div class="box-wrap"><img src="<?= base_url()."assets/img/map_marker/".$category->pin_url; ?>"><span><?= $category->name; ?></span></div>
					<?php } ?>
				</div>
				<div class="legend-column">
					<div class="box-wrap" style="font-weight:bold"><?= lang('commercial_center')?></div>
					<div class="box-wrap"><img class="box-wrap-img" src="<?= base_url()."assets/img/map_marker/utc.png"; ?>"><span>UTC</span><div class="clear"></div></div>
					<div class="box-wrap"><img class="box-wrap-img" src="<?= base_url()."assets/img/map_marker/rtc.png"; ?>"><span>RTC</span><div class="clear"></div></div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--for label language-->
<div>
  <input type="hidden" id="label_innovation" value="<?=lang('label_innovation_name')?>">
  <input type="hidden" id="label_category" value="<?=lang('label_category')?>">
  <input type="hidden" id="label_address" value="<?=lang('label_address')?>">
  <input type="hidden" id="label_state" value="<?=lang('label_state')?>">
  <input type="hidden" id="label_university" value="<?=lang('label_university')?>">
  <input type="hidden" id="label_contact_no" value="<?=lang('label_contact_no')?>">
  <input type="hidden" id="label_expert" value="<?=lang('label_expert')?>">
  <input type="hidden" id="label_expert_name" value="<?=lang('label_expert')?>">
  <input type="hidden" id="label_expertise" value="<?=lang('label_expertise')?>">
  <input type="hidden" id="label_experience" value="<?=lang('label_experience')?>">
  <input type="hidden" id="label_innovator" value="<?=lang('innovator')?>">
</div>