<div class="block innovator" style="padding-bottom:0">
	<div class="title"><?=lang('title_management_dashboard')?></div>
	<div class="block-content dasboard-content" style="padding:10px 0">
		<div class="grid_6 alpha omega">
			<form class="webform" action="<?= base_url()."management_dashboard/innovation_year_handler" ?>" id="form-innovation-year" method="post">				
				<input type="hidden" name="report_title" class="graph-title" value="<?=lang('title_innovation_by_year')?>">
				<input type="hidden" class="graph-x-title" value="<?=lang('label_innovation_year')?>">
				<input type="hidden" class="graph-y-title" value="<?=lang('info_total_innovation')?>">
			</form>
			<div id="db-inno-year-container" class="grid_3">Innovation by year.</div>
			<div class="clear"></div>
		</div>

		<div class="grid_6 alpha omega">
			<form class="webform" action="<?= base_url()."management_dashboard/innovation_category_handler" ?>" id="form-innovation-category-2015" method="post">				
				<input type="hidden" name="year" value="2015">
				<input type="hidden" class="graph-title" value="<?=lang('title_innovation_by_category').' 2015'?>">
			</form>
			<div id="db-inno-cat-2015-container" class="grid_3">Innovation by category 2015.</div>
			<div class="clear"></div>
		</div>

		<div class="grid_6 alpha omega">
			<form class="webform" action="<?= base_url()."management_dashboard/innovation_category_handler" ?>" id="form-innovation-category-2014" method="post">				
				<input type="hidden" name="year" value="2014">
				<input type="hidden" class="graph-title" value="<?=lang('title_innovation_by_category').' 2014'?>">
			</form>
			<div id="db-inno-cat-2014-container" class="grid_3">Innovation by category 2014.</div>
			<div class="clear"></div>
		</div>

		<div class="grid_6 alpha omega">
			<form class="webform" action="<?= base_url()."management_dashboard/innovation_category_handler" ?>" id="form-innovation-category-2013" method="post">				
				<input type="hidden" name="year" value="2013">
				<input type="hidden" class="graph-title" value="<?=lang('title_innovation_by_category').' 2013'?>">
			</form>
			<div id="db-inno-cat-2013-container" class="grid_3">Innovation by category 2013.</div>
			<div class="clear"></div>
		</div>

		<div class="grid_6 alpha omega">
			<form class="webform" action="<?= base_url()."management_dashboard/innovation_state_handler" ?>" id="form-innovation-state-2015" method="post">				
				<input type="hidden" name="year" value="2015">
				<input type="hidden" class="graph-title" value="<?=lang('title_innovation_by_state').' 2015'?>">
			</form>
			<div id="db-inno-state-2015-container" class="grid_3">Innovation by state 2015.</div>
			<div class="clear"></div>
		</div>

		<div class="grid_6 alpha omega">
			<form class="webform" action="<?= base_url()."management_dashboard/innovation_state_handler" ?>" id="form-innovation-state-2014" method="post">				
				<input type="hidden" name="year" value="2014">
				<input type="hidden" class="graph-title" value="<?=lang('title_innovation_by_state').' 2014'?>">
			</form>
			<div id="db-inno-state-2014-container" class="grid_3">Innovation by state 2014.</div>
			<div class="clear"></div>
		</div>

		<div class="grid_6 alpha omega">
			<form class="webform" action="<?= base_url()."management_dashboard/innovation_state_handler" ?>" id="form-innovation-state-2013" method="post">				
				<input type="hidden" name="year" value="2013">
				<input type="hidden" class="graph-title" value="<?=lang('title_innovation_by_state').' 2013'?>">
			</form>
			<div id="db-inno-state-2013-container" class="grid_3">Innovation by state 2013.</div>
			<div class="clear"></div>
		</div>

		<div class="grid_6 alpha omega">
			<form class="webform" action="<?= base_url()."management_dashboard/innovation_evaluation_handler" ?>" id="form-innovation-evaluation" method="post">				
				<input type="hidden" class="graph-title" value="<?=lang('title_innovation_eval_burndown')?>">
				<input type="hidden" class="tooltip-title" value="<?=lang('label_innovation')?>">
			</form>
			<div id="db-inno-evaluation-container" class="grid_3">Innovation evaluation burndown.</div>
			<div class="clear"></div>
		</div>

		<div class="grid_6 alpha omega">
			<form class="webform" action="<?= base_url()."management_dashboard/innovation_portfolio_handler" ?>" id="form-innovation-portfolio" method="post">				
				<input type="hidden" class="graph-title" value="<?=lang('title_portfolio_burndown')?>">
				<input type="hidden" class="tooltip-title" value="<?=lang('title_portfolio')?>">
			</form>
			<div id="db-inno-portfolio-container" class="grid_3">Portfolio burndown.</div>
			<div class="clear"></div>
		</div>

		<div class="clear"></div>
	</div>
</div>

<div id="innovation_year" style="display:none">
	<table class="innovation_year biz-table" style="width:100%">
		<thead>
			<tr>
				<th>No</th>
				<th><?=lang('label_innovation_name'); ?></th>
				<th><?=lang('label_innovator')?></th>
				<th><?=lang('label_discovered_date') ?></th>
				<th><?=lang('label_score_average')?></th>
				<th></th>
			</tr>
		</thead>
		<tbody class="content-dialog"></tbody>
	</table>
</div>

<div id="innovation_category" style="display:none">
	<table class="innovation_category biz-table" style="width:100%">
		<thead>
			<tr>
				<th>No</th>
				<th><?=lang('label_innovation_name'); ?></th>
				<th><?=lang('label_innovator')?></th>
				<th><?=lang('label_innovation_category')?></th>
				<th><?=lang('label_score_average')?></th>
				<th><?=lang('label_discovered_date') ?></th>
				<th></th>
			</tr>
		</thead>
		<tbody class="content-dialog"></tbody>
	</table>
</div>

<div id="innovation_state" style="display:none">
	<table class="innovation_state biz-table" style="width:100%">
		<thead>
			<tr>
				<th>No</th>
				<th><?=lang('label_innovation_name'); ?></th>
				<th><?=lang('label_innovator')?></th>
				<th><?=lang('label_state')?></th>
				<th><?=lang('label_score_average')?></th>
				<th><?=lang('label_discovered_date') ?></th>
				<th></th>
			</tr>
		</thead>
		<tbody class="content-dialog"></tbody>
	</table>
</div>

<div id="innovation_eval_burndown" style="display:none">
	<table class="innovation_eval_burndown biz-table" style="width:100%">
		<thead>
			<tr>
				<th>No</th>
				<th><?=lang('label_task'); ?></th>
				<th><?=lang('label_innovation')?></th>
				<th><?=lang('label_expert')?></th>
				<th><?=lang('label_score')?></th>
			</tr>
		</thead>
		<tbody class="content-dialog"></tbody>
	</table>
</div>

<div id="portfolio_burndown" style="display:none">
	<table class="portfolio_burndown biz-table" style="width:100%">
		<thead>
			<tr>
				<th>No</th>
				<th><?=lang('title_portfolio'); ?></th>
			</tr>
		</thead>
		<tbody class="content-dialog"></tbody>
	</table>
</div>