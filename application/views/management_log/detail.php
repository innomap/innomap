<div class="block innovator green">
	<div class="title"><?=lang('title_management_logs')?></div>
	
	<div class="block-content">
		<div class="file-title"><center><b>HIP6: INCLUSIVE INNOVATION ACCOUNT MANAGEMENT LOG</b></div>
		<div class="detail-content grid_11 alpha">
			<form method="POST" action="<?=base_url()."management_logs/save"?>">
				<input type="hidden" name="management_log_id" value="<?= isset($log) ? $log['management_log_id'] : '-1' ?>">

				<div class="label grid_3"><?= lang('label_account_name') ?> : </div>
				<div class="value grid_6"><?= isset($log) ? $log['name_in_melayu'] : '' ?> </div>

				<div class="label grid_3"><?= lang('innovator_name') ?> : </div>
				<div class="value grid_6 field-innovator">
					<?php if(isset($innovators)){
							foreach($innovators as $key=>$value){
								echo "- ".$value['name']."<br/>";
						 } } ?>
				</div>

				<div class="label grid_3"><?= lang('label_account_manager') ?> : </div>
				<div class="value grid_6"><?= $this->user_sess['username'] ?></div>

				<div class="label grid_3"><?= lang('label_date') ?> : </div>
				<div class="value grid_6"><?= (isset($log) ? $log['date'] : '') ?></div>

				<div class="label grid_3"><?= lang('label_venue') ?> : </div>
				<div class="value grid_6"><?= (isset($log) ? $log['venue'] : '') ?></div>

				<div class="label grid_3"><?= lang('label_notes') ?> : </div>
				<div class="grid_6" style="margin-bottom:10px"><?= (isset($log) ? $log['notes'] : '') ?></div>
				
				<div class="grid_5 right action_wrap">
					<a href="<?=base_url().'management_logs'?>" class="btn red grid_1 omega alpha"><?=lang('button_back')?></a>
					<a class="btn grid_1 omega alpha print_page"><?= lang('button_print') ?></a>
				</div>
			</form>
		</div>
	</div>
</div>