<div class="block innovator green">
	<div class="title"><?=lang('title_management_logs')?></div>
	
	<div class="block-content">
		<?php if(isset($innovations) || isset($mode_edit)){ ?>
		<div class="detail-content grid_11 alpha">
			<form method="POST" action="<?=base_url()."management_logs/save"?>">
				<input type="hidden" name="management_log_id" value="<?= isset($log) ? $log['management_log_id'] : '-1' ?>">

				<div class="label grid_3"><?= lang('label_account_name') ?> : </div>
				<div class="value grid_6">
					<?php if(isset($innovations)){ ?>
						<select name="innovation_id" class="grid_5 alpha opt-innovation">
						<?php foreach ($innovations as $key => $value) { ?>
							<option value="<?= $value['innovation_id'] ?>"><?= $value['name_in_melayu'] ?></option>
						<?php } ?>
						</select>
						<?php }else{
							echo $log['name_in_melayu'];
						} ?>
				</div>

				<div class="label grid_3"><?= lang('innovator_name') ?> : </div>
				<div class="value grid_6 field-innovator">
					<?php if(isset($innovators)){
							foreach($innovators as $key=>$value){
								echo "- ".$value['name']."<br/>";
						 } } ?>
				</div>

				<div class="label grid_3"><?= lang('label_account_manager') ?> : </div>
				<div class="value grid_6"><?= $this->user_sess['username'] ?></div>

				<div class="label grid_3"><?= lang('label_date') ?> : </div>
				<div class="value grid_6"><input type="text" class="grid_5 alpha datepicker" name="date" value="<?= (isset($log) ? $log['date'] : '') ?>"></div>

				<div class="label grid_3"><?= lang('label_venue') ?> : </div>
				<div class="value grid_6"><input type="text" class="grid_5 alpha" name="venue" value="<?= (isset($log) ? $log['venue'] : '') ?>" <?= (isset($view_mode) ? 'readonly' : '') ?>></div>

				<div class="label grid_3"><?= lang('label_notes') ?> : </div>
				<div class="grid_6" style="margin-bottom:10px"><textarea class="grid_5 alpha" name="notes" rows="20" <?= (isset($view_mode) ? 'disabled' : '') ?>><?= (isset($log) ? $log['notes'] : '') ?></textarea></div>
				
				<div class="grid_5 right">
					<a href="<?=base_url().'management_logs'?>" class="btn red grid_1 omega alpha"><?=lang('button_cancel')?></a>
					<?php if(!isset($view_mode)){ ?>
						<input type="submit" class="btn" value="<?=lang('button_save')?>" />
					<?php } ?>
				</div>
			</form>
		</div>
		<?php }else{
			echo "<p>".lang('info_dont_have_task')."</p>";
		} ?>
	</div>
</div>

<div id="progress-bar" title="Progressing...">
	<img src="<?=base_url()."assets/img/progress_bar.gif"?>" />
</div>