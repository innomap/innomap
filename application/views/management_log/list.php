<div class="block innovator">
	<div class="title">
		<div style="<?= $access['add'] == true ? 'float:left' : '';?>"><?=lang('title_management_logs')?></div>
		<?php if($access['add']){ ?>
			<a href="<?=base_url()."management_logs/add"?>" class="reset_data_tables">
				<div class="add_innovator_button">
					<div><?=lang('btn_add_management_log')?></div>
				</div>
			</a>
		<?php } ?>
		<div class="clear"></div>
	</div>
	<div class="block-content">
		<?php
			if($flashdata != NULL){
				echo "<div class='form-info ".($flashdata['success'] ? 'success' : 'fail')."'>".$flashdata['msg']."</div>";
			}
		?>
		<table cellpadding="0" cellspacing="0" border="0" class="display" id="innovator_table" width="100%">
			<thead>
				<tr>
					<th width="5%">No.</th>
					<th width="35%"><?=lang('innovation')?></th>
					<th width="45%"><?=lang('label_date')?></th>
					<th width="45%"><?=lang('label_venue')?></th>
					<th class="last" width="10%"><?=lang('action')?></th>
				</tr>
			</thead>
			<tbody>
				<?php 
					$no = 1;
					foreach($logs as $key=>$value){ ?>
						<tr>
							<td><?=$no?></td>
							<td><?=$value['name_in_melayu']?></td>
							<td><?=$value['date']?></td>
							<td><?=$value['venue']?></td>
							<td>
								<a href="<?=base_url()."management_logs/view/".$value['management_log_id']."/1"?>" title="<?=lang('link_view')?>" class="view-icon"></a>
								<a href="<?=base_url()."management_logs/edit/".$value['management_log_id']?>" class="edit" title="Edit"></a>
							</td>
						</tr>
					<?php 
					$no++;
					} ?>
			</tbody>
		</table>
	</div>
</div>