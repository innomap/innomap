<div class="block innovator green">
	<div class="title"><?=lang('title_management_plan')?></div>
	
	<div class="block-content">
		<?php if($flashdata != NULL){
			echo "<div class='form-info ".($flashdata['success'] ? 'success' : 'fail')."'>".$flashdata['msg']."</div>";
		} ?>
		
		<div class="file-title"><center><b>HIP6 ACCOUNT MANAGEMENT PLAN</b></div>
		<div class="detail-content grid_11 alpha">
			<form method="POST" action="<?=base_url()."management_plans/save_management_plan"?>" id="innovation" enctype="multipart/form-data">
				<input type="hidden" name="management_plan_id" value="<?= (isset($plan) ? $plan['management_plan_id'] : '-1') ?>">
				<input type="hidden" name="innovation_id" value="<?= (isset($innovation) ? $innovation['innovation_id'] : '') ?>">

				<h3 class="grid_11"><?= lang('label_background_of') ?> <?= (isset($innovation) ? $innovation['name_in_melayu'] : '') ?></h3>
				<div class="value grid_10"><?= (isset($innovation) ? $innovation['inspiration_in_melayu'] : '') ?></div>

				<h3 class="grid_11"><?= lang('label_account_details') ?></h3>

				<div class="label grid_3"><?= lang('label_name_of_inventor') ?> : </div>
				<div class="value grid_6"><?= (isset($innovator) ? $innovator['name'] : '') ?></div>

				<div class="label grid_3"><?= lang('label_birth_date') ?> : </div>
				<div class="value grid_6"><?= (isset($innovator) ? $innovator['birth_date'] : '') ?></div>

				<div class="label grid_3"><?= lang('label_profession') ?> : </div>
				<div class="value grid_6"><?= (isset($innovator) ? $innovator['d_employment'] : '') ?></div>

				<div class="label grid_3"><?= lang('label_gender') ?> : </div>
				<div class="value grid_6"><?= (isset($innovator) ? ($innovator['gender'] == 1 ? lang('value_male') : lang('value_female')) : '-') ?></div>

				<div class="label grid_3"><?= lang('label_current_address') ?> : </div>
				<div class="value grid_6"><?= (isset($innovator) ? $innovator['address'] : '') ?></div>

				<div class="label grid_3"><?= lang('label_mobile_phone') ?> : </div>
				<div class="value grid_6"><?= (isset($innovator) ? $innovator['d_mobile_phone_no'] : '') ?> </div>

				<div class="label grid_3"><?= lang('label_home_phone') ?> : </div>
				<div class="value grid_6"><?= (isset($innovator) ? $innovator['d_home_phone_no'] : '') ?> </div>

				<div class="label grid_3"><?= lang('label_next_of_kin_contact') ?> : </div>
				<div class="value grid_6"><?= (isset($innovator) ? $innovator['h_mobile_phone_no'] : '') ?> </div>

				<div class="label grid_3"><?= lang('label_innovation_title') ?> : </div>
				<div class="value grid_6"><?= (isset($innovation) ? $innovation['name_in_melayu'] : '') ?> </div>

				<div class="label grid_3"><?= lang('label_stage_no') ?> : </div>
				<div class="value grid_6"><?= isset($plan) ? $plan['stage_no'] : '' ?></div>

				<div class="label grid_3"><?= lang('label_certificate_no') ?> : </div>
				<div class="value grid_6"><?= isset($plan) ? $plan['certificate_no'] : '' ?></div>
				
				<h3 class="grid_11"><?= lang('title_popup_detail_innovation') ?></h3>
				<div class="value grid_10">
					<p><?= (isset($innovation) ? $innovation['description_in_melayu'] : '') ?></p><br/>
					<?php foreach ($innovation_images as $key => $value) { ?>
						<img src="<?= base_url().PATH_TO_INNOVATION_PICTURE.$value['picture'] ?>" class="grid_2 omega margin-bottom">
					<?php } ?>
				</div>

				<h3 class="grid_11"><?= lang('label_key_challenges_of_account') ?></h3>
				<div class="value">
					<table class="tbl-challenge general_table" width="100%">
						<thead>
							<th>No.</th>
							<th><?= lang('label_challenges') ?></th>
							<th><?= lang('label_mitigation') ?></th>
						</thead>
						<tbody>
							<?php $no=1;foreach ($plan_challenges as $key => $value) { ?>
								<tr>
									<td><?= $no; ?>. </td>
									<td><?= $value['challenge'] ?></td>
									<td><?= $value['mitigation'] ?></td>
								</tr>
							<?php $no++; } ?>
						</tbody>
					</table>
				</div>

				<h3 class="grid_11"><?= lang('label_account_targets') ?></h3>
				<div class="value">
					<table class="tbl-target general_table" width="100%">
						<thead>
							<th>No.</th>
							<th><?= lang('label_target') ?></th>
							<th><?= lang('label_deadline') ?></th>
							<th><?= lang('label_remarks') ?></th>
						</thead>
						<tbody>
							<?php $no=1;foreach ($plan_targets as $key => $value) { ?>
								<tr>
									<td><?= $no; ?>. </td>
									<td><?= $value['target'] ?></td>
									<td><?= $value['deadline'] ?></td>
									<td><?= $value['remark'] ?></td>
								</tr>
							<?php $no++; } ?>
						</tbody>
					</table>
				</div>

				<h3 class="grid_11"><?= lang('label_revision_note') ?></h3>
				<?php if(count($plan_notes) > 0){ ?>
				<table width="100%" class="general_table">
					<thead>
						<th>No.</th>
						<th><?= lang('label_date_of_revision') ?></th>
						<th><?= lang('label_revised_by') ?></th>
						<th><?= lang('label_remarks') ?></th>
					</thead>
					<tbody>
						<?php $no=1;foreach ($plan_notes as $key => $value) { ?>
						<tr>
							<td><?= $no; ?></td>
							<td><?= $value['created_at'] ?></td>
							<td><?= $value['username'] ?></td>
							<td><?= $value['remarks'] ?></td>
						</tr>
						<?php $no++;} ?>
					</tbody>
				</table>
				<?php } ?>

				<div class="grid_5 right action_wrap">
					<a href="<?= site_url('management_plans') ?>" class="btn red grid_1 omega alpha"><?=lang('button_back')?></a>
					<a class="btn grid_1 omega alpha print_page"><?= lang('button_print') ?></a>
				</div>
			</form>
		</div>
	</div>
</div>