<div class="block innovator green">
	<div class="title"><?=lang('title_management_plan')?></div>
	
	<div class="block-content">
		<?php if($flashdata != NULL){
			echo "<div class='form-info ".($flashdata['success'] ? 'success' : 'fail')."'>".$flashdata['msg']."</div>";
		} ?>
		
		<?php if(isset($innovation_list) || isset($mode_edit)){ ?>
		<div class="detail-content grid_11 alpha">
			<form method="POST" action="<?=base_url()."management_plans/save_management_plan"?>" id="innovation" enctype="multipart/form-data">
				<?php if(isset($innovation_list)){ ?>
					<div class="label grid_3"><?= lang('label_innovation') ?> : </div>
					<div class="value grid_6">
						<select class="grid_4 opt-innovation">
							<?php foreach ($innovation_list as $key => $value) { ?>
								<option value="<?= $value['innovation_id'] ?>"><?= $value['name_in_melayu'] ?></option>
							<?php } ?>
						</select>
					</div>
				<?php } ?>
				<input type="hidden" name="management_plan_id" value="<?= (isset($plan) ? $plan['management_plan_id'] : '-1') ?>">
				<input type="hidden" class="field-innovation-id" name="innovation_id" value="<?= (isset($innovation) ? $innovation['innovation_id'] : '') ?>">

				<h3 class="grid_11"><?= lang('label_background_of') ?> <?= (isset($innovation) ? $innovation['name_in_melayu'] : '') ?></h3>
				<div class="value grid_10 field-background"><?= (isset($innovation) ? $innovation['inspiration_in_melayu'] : '') ?></div>

				<h3 class="grid_11"><?= lang('label_account_details') ?></h3>

				<div class="label grid_3"><?= lang('label_name_of_inventor') ?> : </div>
				<div class="value grid_6 field-innovator"><?= (isset($innovator) ? $innovator['name'] : '') ?></div>

				<div class="label grid_3"><?= lang('label_birth_date') ?> : </div>
				<div class="value grid_6 field-bd"><?= (isset($innovator) ? $innovator['birth_date'] : '') ?></div>

				<div class="label grid_3"><?= lang('label_profession') ?> : </div>
				<div class="value grid_6 field-profession"><?= (isset($innovator) ? $innovator['d_employment'] : '') ?></div>

				<div class="label grid_3"><?= lang('label_gender') ?> : </div>
				<div class="value grid_6 field-gender"><?= (isset($innovator) ? $innovator['gender_label'] : '-') ?></div>

				<div class="label grid_3"><?= lang('label_current_address') ?> : </div>
				<div class="value grid_6 field-addrress"><?= (isset($innovator) ? $innovator['address'] : '') ?></div>

				<div class="label grid_3"><?= lang('label_mobile_phone') ?> : </div>
				<div class="value grid_6 field-mobile-phone"><?= (isset($innovator) ? $innovator['d_mobile_phone_no'] : '') ?> </div>

				<div class="label grid_3"><?= lang('label_home_phone') ?> : </div>
				<div class="value grid_6 field-home-phone"><?= (isset($innovator) ? $innovator['d_home_phone_no'] : '') ?> </div>

				<div class="label grid_3"><?= lang('label_next_of_kin_contact') ?> : </div>
				<div class="value grid_6 field-h-mobile-phone"><?= (isset($innovator) ? $innovator['h_mobile_phone_no'] : '') ?> </div>

				<div class="label grid_3"><?= lang('label_innovation_title') ?> : </div>
				<div class="value grid_6 field-innovation"><?= (isset($innovation) ? $innovation['name_in_melayu'] : '') ?> </div>

				<div class="label grid_3"><?= lang('label_stage_no') ?> : </div>
				<div class="value grid_6">
					<select name="stage_no" class="grid_4 alpha">
						<option value="2" <?= isset($plan) ? ($plan['stage_no'] == 2 ? 'selected' : '') : '' ?>>2</option>
						<option value="3" <?= isset($plan) ? ($plan['stage_no'] == 3 ? 'selected' : '') : '' ?>>3</option>
					</select>
				</div>

				<div class="label grid_3"><?= lang('label_certificate_no') ?> : </div>
				<div class="value grid_6"><input type="text" class="grid_4 alpha" name="certificate_no" value="<?= isset($plan) ? $plan['certificate_no'] : '' ?>" /> </div>
				
				<h3 class="grid_11"><?= lang('title_popup_detail_innovation') ?></h3>
				<div class="value grid_10">
					<p class="field-innovation-desc"><?= (isset($innovation) ? $innovation['description_in_melayu'] : '') ?></p><br/>
					<div class="field-images">
					<?php foreach ($innovation_images as $key => $value) { ?>
						<img src="<?= base_url().PATH_TO_INNOVATION_PICTURE.$value['picture'] ?>" class="grid_2 omega margin-bottom">
					<?php } ?>
					</div>
				</div>

				<input type="hidden" id="lang-delete" value="<?= lang('link_delete') ?>">
				<h3 class="grid_11"><?= lang('label_key_challenges_of_account') ?></h3>
				<div class="value grid_10">
					<a href="#" class="add-challenge">(+) <?= lang('btn_add_key_challenges') ?></a>
					<table class="tbl-challenge" width="100%">
						<thead>
							<th>No.</th>
							<th><?= lang('label_challenges') ?></th>
							<th><?= lang('label_mitigation') ?></th>
							<th width="10%"></th>
						</thead>
						<tbody>
							<?php if(count($plan_challenges) == 0){ ?>
							<tr>
								<td>1. <input type="hidden" name="challenge_ids[]" value="-1"></td>
								<td><textarea class="grid_4" name="challenges[]"></textarea></td>
								<td><textarea class="grid_4" name="mitigations[]"></textarea></td>
								<td><a href="#" class="delete-challenge" data-id="-1">(-) <?= lang('link_delete') ?></a></td>
							</tr>
							<?php }else{ 
								$no=1;foreach ($plan_challenges as $key => $value) { ?>
									<tr>
										<td><?= $no; ?>. <input type="hidden" name="challenge_ids[]" value="<?= $value['challenge_id'] ?>"></td>
										<td><textarea class="grid_4" name="challenges[]"><?= $value['challenge'] ?></textarea></td>
										<td><textarea class="grid_4" name="mitigations[]"><?= $value['mitigation'] ?></textarea></td>
										<td><a href="#" class="delete-challenge" data-id="<?= $value['challenge_id'] ?>">(-) <?= lang('link_delete') ?></a></td>
									</tr>
							<?php $no++;} 
							 } ?>
						</tbody>
					</table>
				</div>

				<h3 class="grid_11"><?= lang('label_account_targets') ?></h3>
				<div class="value grid_10">
					<a href="#" class="add-target">(+) <?= lang('btn_add_target') ?></a>
					<table class="tbl-target" width="100%">
						<thead>
							<th>No.</th>
							<th><?= lang('label_target') ?></th>
							<th><?= lang('label_deadline') ?></th>
							<th><?= lang('label_remarks') ?></th>
						</thead>
						<tbody>
							<?php if(count($plan_targets) == 0){ ?>
							<tr>
								<td>1. <input type="hidden" name="target_ids[]" value="-1"></td>
								<td><textarea class="grid_3" name="targets[]"></textarea></td>
								<td><input class="grid_2 datepicker" type="text" name="deadlines[]"></td>
								<td><textarea class="grid_3" name="remarks[]"></textarea></td>
								<td><a href="#" class="delete-target" data-id="-1">(-) <?= lang('link_delete') ?></a></td>
							</tr>
							<?php }else{ 
									$no=1;foreach ($plan_targets as $key => $value) { ?>
									<tr>
										<td><?= $no; ?>. <input type="hidden" name="target_ids[]" value="<?= $value['target_id'] ?>"></td>
										<td><textarea class="grid_3" name="targets[]"><?= $value['target'] ?></textarea></td>
										<td><input class="grid_2 datepicker" type="text" name="deadlines[]" value="<?= $value['deadline'] ?>"></td>
										<td><textarea class="grid_3" name="remarks[]"><?= $value['remark'] ?></textarea></td>
										<td><a href="#" class="delete-target" data-id="<?= $value['target_id'] ?>">(-) <?= lang('link_delete') ?></a></td>
									</tr>
							<?php $no++;} 
							} ?>
						</tbody>
					</table>
				</div>

				<h3 class="grid_11"><?= lang('label_revision_note') ?></h3>
				<?php if(count($plan_notes) > 0){ ?>
				<table width="100%" class="general_table">
					<thead>
						<th>No.</th>
						<th><?= lang('label_date_of_revision') ?></th>
						<th><?= lang('label_revised_by') ?></th>
						<th><?= lang('label_remarks') ?></th>
					</thead>
					<tbody>
						<?php $no=1;foreach ($plan_notes as $key => $value) { ?>
						<tr>
							<td><?= $no; ?></td>
							<td><?= $value['created_at'] ?></td>
							<td><?= $value['username'] ?></td>
							<td><?= $value['remarks'] ?></td>
						</tr>
						<?php $no++;} ?>
					</tbody>
				</table>
				<?php } ?>

				<div class="label grid_3"><?= lang('label_remarks') ?> : </div>
				<div class="value grid_6"><textarea class="grid_6 alpha omega" name="revision_note_remarks"></textarea></div>

				<div class="grid_5 right">
					<a href="<?= site_url('management_plans') ?>" class="btn red grid_1 omega alpha"><?=lang('button_cancel')?></a>
					<input type="submit" class="btn" value="<?=lang('button_save')?>" />
				</div>
			</form>
		</div>
		<?php }else{
				echo "<p>".lang('info_dont_have_task')."</p>";
			} ?>
	</div>
</div>

<div id="progress-bar" title="Progressing...">
	<img src="<?=base_url()."assets/img/progress_bar.gif"?>" />
</div>