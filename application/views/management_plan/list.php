<div class="block innovator">
	<div class="title">
		<div style="<?= $access['add'] == true ? 'float:left' : '';?>"><?=lang('title_management_plan')?></div>
		<?php if($access['add']){ ?>
			<a href="<?=base_url()."management_plans/add"?>" class="reset_data_tables">
				<div class="add_innovator_button">
					<div><?=lang('btn_add_management_plan')?></div>
				</div>
			</a>
		<?php } ?>
		<div class="clear"></div>
	</div>
	<div class="block-content">
		<?php
			if($flashdata != NULL){
				echo "<div class='form-info ".($flashdata['success'] ? 'success' : 'fail')."'>".$flashdata['msg']."</div>";
			}
		?>
		<table cellpadding="0" cellspacing="0" border="0" class="display" id="innovator_table" width="100%">
			<thead>
				<tr>
					<th width="5%">No.</th>
					<th width="35%"><?=lang('innovation')?></th>
					<th width="35%"><?=lang('innovator_name')?></th>
					<th width="45%"><?=lang('label_description')?></th>
					<th class="last" width="10%"><?=lang('action')?></th>
				</tr>
			</thead>
			<tbody>
				<?php 
					$no = 1;
					foreach($innovations as $key=>$value){ ?>
						<tr>
							<td><?=$no?></td>
							<td><?=$value['name']?></td>
							<td><?=($value['innovator'] == "" ? "-Tidak ada inovator-" : $value['innovator'])?></td>
							<td><?=ellipsis($value['description'],100)?></td>
							<td>
								<a href="<?=base_url()."management_plans/edit/".$value['management_plan_id']?>" class="edit" title="<?= lang('link_edit') ?>"></a>
								<a href="<?=base_url()."management_plans/view/".$value['management_plan_id']?>" title="<?=lang('link_view')?>" class="view-icon"></a>
							</td>
						</tr>
					<?php 
					$no++;
					} ?>
			</tbody>
		</table>
	</div>
</div>