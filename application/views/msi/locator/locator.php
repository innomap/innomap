<div class="block innovator green">
	<div class="title"><?= lang('label_locator') ?></div>
	<div class="block-content">
		<div class="report-table-wrapper">
			<form class="inno-form" method="POST">
				<label class="label"><?=lang('label_search')?></label> 
				<input type="text" style="width:640px;height:25px;margin-bottom:10px;margin-left:17px" name="keyword" value="<?=(isset($data_search) ? $data_search['state'] : '')?>"/>
				<br/><label class="label"><?=lang('label_state')?></label> 
				<select name="state" style="margin-bottom:9px">
					<option value="0">- <?=lang('label_all')." ".lang('label_state')?> -</option>
					<?php foreach($state as $row){  ?>
						<option value="<?= $row['state_id']; ?>" <?= ($row['state_id'] == $pos_state ? 'selected' : ''); ?>><?= $row['name']; ?></option>
					<?php } ?>
				</select>
				<label class="label"><?=lang('label_year')?></label> 
				<select name="year">
					<option value="">- <?=lang('label_all'); ?> -</option>
					<option value="2013" <?= ($pos_year == "2013" ? 'selected' : ''); ?>>2013</option>
					<option value="2012" <?= ($pos_year == "2012" ? 'selected' : ''); ?>>2012</option>
				</select>
				<input type="submit" class="btn" name="search_location" value="<?=lang('label_search')?>" style="display:block"/>
				<br >
				<?php $i=1;foreach($types as $type){ ?>
					<input type="checkbox" name="innovation[]" <?=(isset($data_search) ? (in_array($i,$data_search['types']) ? 'checked="checked"' : '') : 'checked="checked"')?> value="<?=$i?>"/>&nbsp;&nbsp;<span class="label"><?=$type?></span>&nbsp;&nbsp;&nbsp;
				<?php $i++;} ?>
				<br/><br/>
				<div><?=lang('label_search_result')?>: <?= $highlight['innovation'] ?> <?= $types[1]; ?>, <?= $highlight['university'] ?> <?= $types[2]; ?>, <?= $highlight['expert'] ?> <?= $types[3]; ?>, <?= $highlight['project'] ?> <?= $types[4]; ?>.</div>
			</form>
		</div>
		<div id="map-canvas" style="margin: 10px auto 20px;">	
			<?php echo $map['html']; ?>
			<div style="display:none"><?php print_r($map); ?></div>
		</div>
	</div>
</div>
