<div class="logo grid_4">
	<a href="<?=base_url().MSI_DIR?>"><img src="<?= base_url()?>assets/img/logo.png" class="grid_4 alpha omega"/></a>
	<div class="clear"></div>
</div>
<?php if($this->is_logged_in){ ?>

	<div class="setting grid_7">
		<ul>
			<li>
				<a href="#">Welcome, <?= $user['username']; ?></a> |
			</li>
			<li>
				<a href="<?= base_url().MSI_DIR.'site/logout'; ?>" class="upper reset_data_tables">Logout</a>
			</li>
		</ul>
	</div>

<?php } ?>
	<div class="clear"></div>
	<div class="menu grid_12 alpha omega" style="<?= !$this->is_logged_in ? 'height:30px;' : ''; ?>">
		<?php if($this->is_logged_in){ ?>
			<label for="show-menu" class="show-menu">Show Menu  &#9776;</label>
			<ul id="menu">
				<li style="border:none;">
					<a href="<?= base_url().MSI_DIR; ?>"><div class="home_icon">&nbsp;</div></a>
				</li>
				<!--menu-->
				<li><a href="<?= base_url().MSI_DIR."projects"?>" class="reset_data_tables">Senarai Projek</a></li>
				<li><a href="<?= base_url().MSI_DIR."locators"?>" class="reset_data_tables">Lokasi</a></li>
			</ul>
		<?php } ?>
	</div>
	<div class="clear"></div>