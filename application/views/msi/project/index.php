<div class="block article">
	<div class="title">
		<div style="float:left;"><?= lang('project_listing') ?></div>
		<a href="<?= base_url().MSI_DIR.'projects/add'; ?>" class="reset_data_tables">
			<div class="add_button">
				<div><?= lang('add')." ".lang('label_project') ?></div>
			</div>
		</a>
		<div class="clear"></div>
	</div>
	<div class="block-content">
		<?php if($message != NULL){ ?>
			<br/><div class="form-info success"><?= $message; ?></div>
		<?php } ?>
		<table cellpadding="0" cellspacing="0" border="0" class="display blue-table" width="100%" id="article_table">
			<thead>
				<th>No</th>
				<th><?= lang('label_title') ?></th>
				<th><?= lang('label_ref_no') ?></th>
				<th><?= lang('label_beneficiary_community') ?></th>
				<th width="15%"><?= lang('action') ?></th>
			</thead>
			<tbody>
				<?php $no=1; foreach($projects as $key=>$value){ ?>
				<tr>
					<td><?= $no; ?></td>
					<td><?= $value['title']; ?></td>
					<td><?= $value['ref_no']; ?></td>
					<td><?= $value['beneficiary_community'] ?></td>
					<td>
						<a href="<?= base_url().MSI_DIR.'projects/edit/'.$value['project_id']; ?>" title="<?= lang('link_edit') ?>" class="edit"></a>
						<a href="<?= base_url().MSI_DIR.'projects/view/'.$value['project_id']; ?>" title="<?= lang('link_view') ?>" class="view-icon"></a>
						<a href="<?= base_url().MSI_DIR.'projects/delete/'.$value['project_id']; ?>" onClick="return initConfirmDelete()" title="<?= lang('link_delete') ?>" class="delete"></a>
					</td>
				</tr>
				<?php $no++;} ?>
			</tbody>
		</table>
	</div>
</div>