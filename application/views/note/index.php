
<script>
	$(function(){
	  $('#masonry').masonry({
	    // options
	    itemSelector : '.item',
	    isFitWidth: true
	  });
	});
	$(document).ready(function(){
		 $( "#dialog" ).dialog({
	      autoOpen: false,
	      title: "Detail",
	      show: {
	        effect: "blind",
	        duration: 500
	      },
	      hide: {
	        effect: "blind",
	        duration: 300
	      }
	    });
		$('.content').click(function() {
			$("#dialog").dialog('open');
			var note_id = $(this).attr('note-id');
			$.get(base_url+'notes/get_detail/'+note_id,function(data){
				console.log(data)
				$('#dialog').html(
								'<h1>'+data.title+'</h1>'+
					        	'<p class="col-md-12">'+data.author+'</p>'+
					        	'<p class="col-md-12">'+data.last_update+'</p>'+
					        	'<p class="col-md-12" style="max-height: 300px;overflow-y: scroll;">'+data.content+'</p>'
					      		);
			},'json');
		});
	});
</script>
<div class="block note orange">
	<div class="title"><?= lang('label_notes') ?></div>
		<?php if($notes != NULL){ ?>
		<div class="block-content" id="masonry">
			<?php foreach ($notes as $note) { ?>
				<div class="item">
					<h2><?= $note['title']; ?></h2>
					<p class="author"><?= $note['author']; ?></p>
					<p class="content" note-id="<?= $note['note_id']; ?>" style="cursor:pointer;">
						<?php 
						$str = $note['content'];
						if (strlen($str) > 250){
							$str = substr($str, 0, 200) . '...';
						} 
						echo $str; ?>
					</p>
					<p class="date">
						<?php 
						$mil = $note['last_update'];
						$seconds = $mil / 1000;
						echo date("d-m-Y H:i:s", $seconds);
						?>
					</p>
				</div>
			<?php } ?>
		</div>
		<?php }else{
			echo '<div class="block-content">Tidak Ada Notes</div>';
		} ?>
	</div>
</div>
<div id="dialog">
<div>
