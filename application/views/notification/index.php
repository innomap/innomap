<div class="block notification">
	<div class="title"><?= lang('menu_notif') ?></div>
	<div class="block-content">
		<ul class="list-notification grid_11">
			<?php foreach($notif as $key => $value){ ?>
				<li>
					<div class="<?= $notif_class[$value['notif_type']]; ?>">
						<?= $value['username']." ".$value['message']." <a href='".base_url()."notifications/read/".$value['notification_id']."'>".$value['target']."</a>"; ?>
						<br/><i><?= $value['created_date'] ?></i>
					</div>
				</li>
			<?php } ?>
		</ul>
		<div class="clear"></div>

		<div class="loading grid_2 push_5" style="display:none"></div>

		<?php if($notif_total > NOTIF_PER_PAGE){ ?>
			<div class="grid_11 load-more-wrap"><a href="#" class="load-more-notif">Load More</a></div>
		<?php } ?>
	</div>
</div>