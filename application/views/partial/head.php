<?php
//set homepage url for specific role
if($user['role_id'] == ROLE_DATA_ANALYST){
	$home_url = site_url('innovators');
}elseif($user['role_id'] == ROLE_EXPERT){
	$home_url = site_url('evaluations');
}elseif($user['role_id'] == ROLE_MANAGER){
	$home_url = site_url('portfolios/manager_task');
}else{
	$home_url = site_url('site');
}
?>

<div class="logo grid_4">
	<a href="<?=base_url()?>">
		<img src="<?= base_url()?>assets/img/logo.png" class="grid_4 alpha omega"/>
		<div class="clear"></div>
	</a>
</div>
<?php if($user){ ?>
	<div class="setting grid_7">
		<ul>
			<li>
				<a href="#" class="upper"><?=lang('menu_language')?></a> |
			</li>
			<li>
				<a href="#" class="upper"><?=lang('menu_setting')?></a> |
			</li>
			<li>
				<a href="#">Hi, <?= $user['username'] ?></a> |
			</li>
			<?php if(count($user_roles) > 1){ ?>
				<li>
					<a href="<?=base_url()."site/select_role"?>"><?=lang('menu_switch_role')?></a> |
				</li>
			<?php } ?>
			<li>
				<a href="<?= base_url()."site/logout"; ?>" class="upper reset_data_tables"><?=lang('menu_logout')?></a>
			</li>
		</ul>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
	<div class="menu grid_12 alpha omega">
		<label for="show-menu" class="show-menu">Show Menu  &#9776;</label>
		<ul id="menu">
			<li style="border:none;">
				<a href="<?=$home_url; ?>"><div class="home_icon"><span class="home-menu">Home</span></div></a>
			</li>
			<?php foreach($menus as $menu){?>
				<li class="<?= $menu['url']; ?> menu-icon">
					<a href="<?= ($menu['url'] != "" ? base_url().$menu['url'] : "#" ); ?>" class="reset_data_tables"><?=($lang == 'melayu' ? $menu['name_in_melayu'] : $menu['name'])?> </a>
					<ul class="hidden">
					<?php foreach($menu['childs'] as $child){ ?>
						<li><a href="<?= base_url().$child['url']; ?>" class="reset_data_tables"><?= ($lang == 'melayu' ? $child['name_in_melayu'] : $child['name'])?> </a></li>
					<?php } ?>
					</ul>					
				</li>
			<?php } ?>
			<li class="menu-icon"><a href="<?= base_url().'edit_password' ?>"><?=lang('title_edit_password')?></a></li>
			<li class="notification menu-icon"><a href="<?= base_url().'notifications' ?>"><?=lang('menu_notif')?></a> <span class="notif_number">0</a></li>
			<div class="clear"></div>
		</ul>
	</div>
	<div class="clear"></div>
<?php }else{ ?>
	<div class="login-wrap grid_3 push_6">
		<div class="arrow-login">&nbsp;</div>
		<div class="login-button grid_2">
			<div>LOGIN</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="grid_4 form-login">
		<form action="<?= base_url()."site/validate/" ?>" method="post" id="form-login">
			<?php if($message != NULL){ ?>
				<div class="grid_7 error-msg"><span><?= $message; ?></span></div>
			<?php } ?>

			<div class="form-field">
				<input type="text" class="grid_3 inputbox alpha" name="username" id="login-username" placeholder="<?=lang('label_username')?>" />
				<div class="clear"></div>
			</div><br/>

			<div class="form-field">
				<input type="password" class="grid_3 inputbox alpha" name="password" id="login-password" placeholder="<?=lang('label_password')?>" />
				<div class="clear"></div>
			</div><br/>

			<div class="form-field">
				<div class="grid_2 omega forgot"><a href="<?= base_url().'site/forgot_password'?>"><?=lang('forgot')." ".lang('label_password')?></a></div>
				<input type="submit" name="submit" id="login-btn" class="submit-btn grid_1 alpha omega" />
			</div>
		</form>
	</div>
	<div class="clear"></div>
<?php } ?>