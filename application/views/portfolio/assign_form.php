<div class="block innovator green">
	<div class="title"><?=lang('label_assign_portfolio')?></div>
	
	<div class="block-content">
		<div class="detail-content grid_11 alpha">
			<form method="POST" action="<?=base_url()."portfolios/save_assign"?>" id="portfolio-assignment">
				<input type="hidden" name="portfolio_id" value="<?= (isset($portfolio) ? $portfolio['portfolio_id'] : 0) ?>">
				<div class="label grid_3"><?=lang('label_title')?>* : </div>
				<div class="value grid_6"><input type="text" class="grid_4" name="title" value="<?= (isset($portfolio) ? $portfolio['title'] : '') ?>"/></div>
				
				<div class="label grid_3"><?=lang('label_account_manager')?>* : </div>
				<div class="value grid_6">
					<select name="manager_id" class="grid_4" <?= isset($portfolio) ? 'disabled' : '' ?>>
					<?php foreach($managers as $manager){ ?>
						<option value="<?=$manager['user_id']?>" <?= (isset($portfolio) ? ($manager['user_id'] == $portfolio['manager_id'] ? 'selected' : '') : "") ?>><?=$manager['username']?></option>
					<?php } ?>
					</select> 
				</div>
				
				<div class="label grid_3"><?=lang('innovation')?>* : </div>
				<div class="value grid_6">
					<?php foreach($innovations as $innovation){ ?>
						<input type="checkbox" name="innovation[]" value="<?=$innovation['innovation_id']?>" <?= (isset($portfolio) ? ($innovation['portfolio_id'] == $portfolio['portfolio_id'] ? 'checked disabled' : '') : '') ?> /><?=$innovation['name']?><br/>
					<?php } ?>
				</div>
				
				<div class="grid_5 right">
					<a class="btn red grid_1 omega alpha" href="<?=base_url()."portfolios"?>"><?=lang('button_cancel')?></a>
					<input type="submit" class="btn" value="<?=lang('button_save')?>" />
				</div>
			</form>
		</div>
	</div>
</div>