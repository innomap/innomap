<div class="block detail_portfolio">
	<div class="title"><?=lang('title_portfolio') ." '".$detail['title']."'"?></div>
	<input type="hidden" name="portfolio_id" value="<?=$detail['portfolio_id']?>" />
	<input type="hidden" name="portfolio_name" value="<?=$detail['title']?>" />
	<div class="block-content">
		<?php if($flashdata != NULL){
			echo "<div class='form-info ".($flashdata['success'] ? 'success' : 'fail')."'>".$flashdata['msg']."</div><br/>";
		}?>

		<div class="grid_4" style="margin-left:3%;">
			<div class="subblock">
				<input type="hidden" id="assignment-date" value="<?=date('Y-m-d',strtotime($detail['assignment_date']))?>" />			
				<div class="title"><?=lang('label_assignment_date')?></div>
				<div class="subblock-content">
					<div id="calendar"></div>

					<div style="margin-top:20px">
						<?php foreach ($scopes as $key_scope => $scope) { ?>
							<div>
								<div style="width:15px;height:15px;float:left" class="calendar-legend-color-<?=$key_scope?>"></div>
								&nbsp;<span><?=$scope['name_in_melayu']?></span>
								<hr/>
							</div>
						<?php } ?>
						<!-- <div>
							<div style="width:15px;height:15px;float:left" class="calendar-legend-all"></div>
							&nbsp;<span>BANYAK SKOP</span>
							<hr/>
						</div> -->
					</div>
				</div>
			</div>
			<div class="subblock">
				<div class="title"><?=lang('task_updates')?></div>
				<div class="subblock-content">
					<a href="#" id="add-note">(+) Tambah Nota</a>
					<div class="task-updates check-page">
						<?php if(count($notes) > 0){
							foreach($notes as $note){ ?>
								<p>- <?=$note['title']?> <a href="#" class="view-icon"></a><a href="#" class="edit"></a><a href="#" class="delete"></a>
								   <input type="hidden" class="portfolio-note-id" value="<?=$note['portfolio_note_id']?>" />
								</p>
						<?php } 
						}else{
							echo '<p class="red-text">'.lang('info_no_notes').'</p>';
						} ?>
					</div>
				</div>
			</div>
		</div>

		<div class="grid_7 omega">
			<div class="subblock">
				<div class="title"><?=lang('title_portfolio_progress')?></div>
				<div class="subblock-content">
					<?php $item_num = 0;
					foreach($innovations as $innovation){
						$innovation_progress = 0; ?>
						<div class="each-portfolio">
							<a class="innovation-name" href="#" style="float:left;margin-right: 20px;margin-bottom:40px;" data-id="<?= $innovation['innovation_id']; ?>"><?=$innovation['name_in_melayu']?></a> 
							<a class="btn red right" href="<?=base_url().'portfolios/print_pdf/'.$detail['portfolio_id'].'/'.$innovation['innovation_id']?>">Print to PDF</a>
							<div class="progress-bar"><span>0%</span></div>
							<h4><?=lang('scope')?></h4>
							<?php foreach($innovation['items'] as $key=>$item){
								$item_num = $item_num + 1;?>
								<div class="item-wrap">
									<input type="hidden" value="<?=$item['criteria_id']?>" innovation-id="<?=$innovation['innovation_id']?>"/>
									<span class="criteria-name"><?=$item['name_in_melayu']?></span>
									<button class="btn edit-scope"><?= lang('link_edit') ?></button> 
									<a class="btn red" href="<?=base_url().'portfolios/print_pdf/'.$detail['portfolio_id'].'/'.$innovation['innovation_id'].'/'.$item['scope_id']?>">Print</a>

									<?php $display = 'none';
									if(!empty($_GET['item'])){ //check to show scope div or not after save
										if((int)$_GET['item'] == $item_num){
											$display = "block";
										}
									}
									?>
									<div class="item-attachment" style="display:<?=$display?>">
										<table width="100%">
											<tr>
												<td><h4><?=lang('label_task_list')?></h4></td>
												<td align="right"><span class="scope-progress"></span>% / <?=$item['weightage']?> %</td>
											</tr>
											<tr>
												<td colspan="3">
													<table cellpadding="5" width="100%" class="table-styled">
														<tr>
															<th><?=lang('label_task_name')?></th>
															<th><?=lang('label_weightage')?></th>
															<th><?=lang('label_progress')?></th>
															<th><?=lang('label_action')?></th>
														</tr>
														<?php 
															$scope_progress = 0; $scope_amount = 0;	
															foreach ($item['tasks'] as $key => $task) { 
																$task_progress_weight = ($task['progress'] / 100) * $task['weightage'];
																$scope_progress = $scope_progress + $task_progress_weight;?>
																<tr class="<?=($key %2 == 0 ? 'odd' : 'even')?>">
																	<td><?=$task['task']?></td>
																	<td><?=$task['weightage']?>%</td>
																	<td><?=$task['progress']?>%</td>
																	<td>
																		<?php if($task['is_pending_pm_approval'] == FALSE){ ?>
																			<a href="#" 
																				class="edit-task edit" 
																				innovation-name="<?=$innovation['name_in_melayu']?>" 
																				item-name="<?=$item['name']?>" 
																				task-name="<?=$task['task']?>" 
																				task-progress="<?=($task['progress'] ? $task['progress'] : 0)?>" 
																				task-id="<?=$task['task_id']?>" 
																				task-note="<?=$task['note']?>" 
																				innovation-id="<?=$innovation['innovation_id']?>"
																				innovation-portfolio-id="<?=$task['innovation_portfolio_id']?>"
																				title="<?=lang('link_edit')?>"
																				item-num="<?=$item_num?>"></a>
																		<?php }else{
																			echo "<font color='blue' style='font-size:13px'>". lang('pending_pd_approval') ."</font> ";
																		} ?>

																		<?php if($task['progress'] > 0){ ?>
																			<a href="#" 
																				class="view-attachment view-icon" 
																				task-id="<?=$task['task_id']?>"
																				innovation-id="<?=$innovation['innovation_id']?>"
																				innovation-name="<?=$innovation['name_in_melayu']?>" 
																				item-name="<?=$item['name']?>" 
																				title="<?=lang('link_view')?>"></a>
																		<?php } ?>
																	</td>
																</tr>
														<?php } ?>
													</table>
												</td>
											</tr>
											<tr>
												<td><?=lang('label_progress')?></td>
												<td>: <b><?=$scope_progress?></b>%</td>
											</tr>
											<tr>
												<td><?=lang('label_budget')?></td>
												<td>: RM <b><?=$item['budget']?></b></td>
											</tr>
											<tr>
												<td><?=lang('label_amount_spend')?></td>
												<td>: RM <b id="scope-amount-spend"><?=$item['tasks'][0]['amount_spend']?></b>
													<a href="#" 
														class="edit-amount-spend edit"></a>
													<form method="POST" class="amount-spend-form" style="display:none" action="<?=base_url() . 'portfolios/save_amount_spend'?>">
														<input type="hidden" name="amount_task_id" value="<?=$item['tasks'][0]['task_id']?>" />
														<input type="hidden" name="amount_portfolio_id" value="<?=$detail['portfolio_id']?>" />
														<input type="hidden" name="amount_innovation_id" value="<?=$innovation['innovation_id']?>" />
														<input type="hidden" name="amount_innovation_portfolio_id" value="<?=$item['tasks'][0]['innovation_portfolio_id']?>" />
														<input type="text" name="amount_spend" value="<?=$item['tasks'][0]['amount_spend']?>" />
														<input type="hidden" name="item_num" value="<?=$item_num?>" />
														<button class="btn amount-spend-save" type="submit">Save</button>
													</form>
													
												</td>
											</tr>
											<tr>
												<td><?=lang('label_ballance')?></td>
												<td>: RM <b>
													<?php
														$ballance = $item['budget'] - $item['tasks'][0]['amount_spend'];
														echo number_format((float)$ballance, 2, '.', '');
													?></b></td>
											</tr>
										</table>
									</div>

									<?php
										$scope_progress_weight = ($scope_progress / 100) * $item['weightage'];
										$innovation_progress = $innovation_progress + $scope_progress_weight;
									?>
									<input type="hidden" name="scope_progress" value="<?=$scope_progress_weight?>" />
								</div>
							<?php } ?>
							<input type="hidden" class="innovation-portfolio-progress" value="<?=$innovation_progress?>" />
						</div>
						<hr style="color:#78d1f0; background:#78d1f0; height:2px; border:0;" />
					<?php } ?>

					<!-- <h2>Kemajuan Keseluruhan Portofolio</h2>
					<div class="general progress-bar" style="margin-top:40px"><span>0%</span></div> -->
					<div id="general-progress"></div>
					<div class="clear"></div>
					<div class="legend">
						<div class="legend-blue"><?=lang('info_uploaded')?></div>
						<div class="legend-red"><?=lang('info_not_uploaded')?></div>
					</div>
					<div class="clear" style="margin-bottom:20px"></div>
				</div>
			</div>
		</div>

		<div class="clear"></div>

		<a href="<?=base_url()."portfolios"?>"><input type="button" value="<< <?=lang('button_back')?>" class="btn red" /></a>
		
		<!-- UPLOAD ITEM ATTACHMENT FORM -->
		<div id="item-attachment" style="display:none" title="<?=lang('title_portfolio_attachment')?>">
			<form method="POST" action="<?=base_url()."portfolios/save_attachment"?>" id="portfolio-attachment" enctype="multipart/form-data">
				<input type="hidden" name="innovation_id" />
				<input type="hidden" name="innovation_portfolio_id" />
				<input type="hidden" name="task_id" />
				<input type="hidden" name="portfolio_id" value="<?=$detail['portfolio_id']?>" />
				<input type="hidden" name="task_progress" />
				<input type="hidden" name="item_num" />
				
				<table>
					<tr>
						<td><?=lang('label_innovation_name')?></td>
						<td  colspan="2"><b id="inno-name"></b></td>
					</tr>
					<tr>
						<td><?=lang('label_item')?></td>
						<td colspan="2"><b id="item-name"></b></td>
					</tr>
					<tr>
						<td><?=lang('label_task')?></td>
						<td colspan="2"><b id="task-name"></b></td>
					</tr>
					<tr>
						<td><?=lang('label_progress')?></td>
						<td colspan="2"><b id="task-progress"></b>%</td>
					</tr>
					<tbody id="edit-ua-attachment">
				
					</tbody>
					<tbody class="tb-attachment-item">
						<tr>
							<td><?=lang('label_attachment')?></td>
							<td><input type="file" class="task-attachment" name="attachment_0" required /></td>
							<td><a href="#" class="add-attachment">(+)Tambah</a></td>
						</tr>
					</tbody>
					<tr>
						<td colspan="3"><font style="font-size:12px;color:red;display:block"><?=lang('info_allowed_file')?>: png,jpg,gif,bmp,jpeg,pdf,docx,doc,xlsx,xls,pptx,ppt,txt</font></td>
					</tr>
					<tr>
						<td><?=lang('label_notes')?></td>
						<td colspan="2"><textarea name="note" rows="6" class="full-parent" required></textarea></td>
					</tr>
					<tr>
						<td>Status</td>
						<td colspan="2">
							<select id="task-done" name="task_done">
								<option value="0">Belum Selesai</option>
								<option value="1">Sudah Selesai</option>
							</select>
						</td>
					</tr>
				</table>
			</form>
		</div>

		<!-- VIEW UPLOADED ITEM ATTACHMENT -->
		<div id="uploaded-attachment" style="display:none" title="<?=lang('title_attachment_detail')?>">
			<table>
				<tr>
					<th><?=lang('label_innovation_name')?>:</th>
					<td><label id="ua-inno-name"></label></td>
				</tr>
				<tr>
					<th><?=lang('label_item')?>:</th>
					<td><label id="ua-item"></label></td>
				</tr>
				<tr>
					<th><?=lang('label_attachment')?>:</th>
					<td></td>
				</tr>
				<tbody id="ua-attachment">
				
				</tbody>
				<tr>
					<th><?=lang('label_notes')?>:</th>
					<td><label id="ua-notes"></label></td>
				</tr>
			</table>
		</div>
		
		<!-- INNOVATION ASSET FORM -->
		<div id="portfolio-asset" style="display:none" title="<?=lang('link_add_asset')?>">
			<form method="POST" action="<?=base_url()."portfolios/save_asset"?>" id="innovation-asset" enctype="multipart/form-data">
				<input type="hidden" name="asset_innovation_id" />				
				<table cellspacing="5">
					<tr>
						<td><?=lang('label_title')?></td>
						<td><input type="text" name="asset_title" size="32"/></td>
					</tr>
					<tr>
						<td><?=lang('label_type')?></td>
						<td><select name="asset_type">
								<?php foreach($asset_types as $key=>$type){ ?>
									<option value="<?=$key?>"><?=$type?></option>
								<?php } ?>
							</select>
						</td>
					</tr>
					<tr id="source-type">
						<td><?=lang('label_content_type')?></td>
						<td><input type="radio" name="source" value="url">URL</input> 
							<input type="radio" name="source" value="attachment"><?=lang('label_attachment')?></input>
						</td>
					</tr>
					<tr id="appened-source"></tr>
					<tr>
						<td><?=lang('label_notes')?></td>
						<td><textarea name="asset_description" cols="30" rows="3"></textarea></td>
					</tr>
				</table>
			</form>
		</div>
		
		<!-- VIEW SAVED ASSET -->
		<div id="saved-asset" style="display:none" title="<?=lang('link_title_uploaded_asset')?>"></div>
		
		<!-- PORTFOLIO NOTE FORM -->
		<div id="portfolio-note" style="display:none" title="<?=lang('title_portfolio_note')?>">
			<form method="POST" action="<?=base_url()."portfolios/save_note"?>" id="note-form" enctype="multipart/form-data">
				<input type="hidden" name="note_portfolio_id" value="<?=$detail['portfolio_id']?>" />
				<input type="hidden" name="note_id" value="0" />
				<table cellspacing="5">
					<tr>
						<td><?=lang('label_title')?></td>
						<td><input type="text" name="note_title" size="32" /></td>
					</tr>
					<tr>
						<td><?=lang('label_content')?></td> 
						<td><textarea name="note_content" cols="30" rows="3"></textarea></td>
					</tr>
					<tr>
						<td><?=lang('label_attachment')?></td>
						<td><input type="file" name="note_attachment" /> 
							<font style="font-size:12px;color:red;display:block"><?=lang('info_allowed_file')?>: png,jpg,gif,bmp,jpeg,pdf,docx,doc</font>
						</td>
					</tr>
					<tr>
						<td><?=lang('label_creation_date')?></td>
						<td><input type="text" name="note_creation_date" class="datepicker" size="32" readonly /></td>
					</tr>
					</table>
			</form>
		</div>
		
		<!-- VIEW SAVED NOTE -->
		<div id="saved-note" style="display:none" title="Detail">
			<b><?=lang('label_title')?>: </b><label></label><br/>
			<b><?=lang('label_content')?>: </b><label></label><br/>
			<b><?=lang('label_attachment')?>: </b><label></label><br/>
			<b><?=lang('label_creation_date')?>: </b><label></label>
		</div>

		<!--POPUP DETAIL INNOVATION-->
		<div id="detail-innovation" style="display:none" title="<?= lang('title_detail_innovation') ?>">
		</div>

		<!-- EDIT CRITERIA ATTACHMENT -->
		<div id="edit-criteria-dialog" style="display:none" title="<?=lang('title_attachment_detail')?>">
			<form method="POST" action="<?=base_url()."portfolios/save_attachment"?>" id="edit-criteria-form" enctype="multipart/form-data">
				<input type="hidden" name="innovation_portfolio_id">
				<input type="hidden" name="portfolio_id">
				<input type="hidden" name="innovation_id">
				<input type="hidden" name="item">
				<input type="hidden" name="h_attachment">
				<table>
					<tr>
						<th><?=lang('label_innovation_name')?>:</th>
						<td colspan="2"><label for="innovation_name"></label></td>
					</tr>
					<tr>
						<th><?=lang('label_item')?>:</th>
						<td colspan="2"><label for="item"></label></td>
					</tr>
					<tr>
						<th><?=lang('label_attachment')?>:</th>
						<td colspan="2"></td>
					</tr>
					<tbody class="criteria-attachment">
					
					</tbody>
					<tbody class="tb-edit-item">
						<tr>
							<th></th>
							<td><input type="file" name="attachment_0"></td>
							<td><a href="#" class="add-attachment">(+)Tambah</a></td>
						</tr>
					</tbody>
					<tr>
						<th><?=lang('label_notes')?>:</th>
						<td colspan="2"><textarea name="note" rows="7" class="full-parent"></textarea></td>
					</tr>
				</table>
			</form>
		</div>

		<!-- POPUP CURRENT TASK -->
		<div id="current-tasks" style="display:none" title="<?= lang('label_task_list') ?>">
			<table id="innovator_table">
				<thead>
					<tr style="color:white">
						<th><?=lang('scope')?></th>
						<th><?=lang('label_task_name')?></th>
						<th><?=lang('label_weightage')?></th>
						<th><?=lang('label_start_date')?></th>
						<th><?=lang('label_end_date')?></th>
					</tr>
				</thead>
				<tbody id="highlight-date-task">
				</tbody>
			</table>
		</div>
	</div>
</div>

<div id="progress-bar" title="Progressing...">
	<img src="<?=base_url()."assets/img/progress_bar.gif"?>" />
</div>

<script>
	var highlight_dates = <?php echo json_encode($highlight_dates); ?>;
</script>