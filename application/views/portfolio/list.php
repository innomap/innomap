<style type="text/css">
	.detail-innovation{color: blue !important;}
</style>
<div class="block innovator green">
	<div class="title">
		<div style="<?= $can_add == true ? 'float:left' : ''; ?>"><?=lang('title_portfolio')?></div>
		<?php if($can_add){ ?>
			<a href="<?=base_url()."portfolios/assign"?>">
				<div class="add_innovator_button">
					<div><?=lang('label_assign_portfolio');?></div>
				</div>
			</a>
		<?php } ?>
		<div class="clear"></div>
	</div>
	<div class="block-content">
		<?php if($flashdata != NULL){
			echo "<div class='form-info ".($flashdata['success'] ? 'success' : 'fail')."'>".$flashdata['msg']."</div>";
		} ?>

		<table cellpadding="0" cellspacing="0" border="0" class="display" id="portfolio_table" width="100%">
			<thead>
				<tr>
					<th>No.</th>
					<th><?=lang('label_title')?></th>
					<th><?=lang('label_manager')?></th>
					<th><?=lang('innovation')?></th>
					<th><?=lang('label_assignment_date')?></th>
					<th>Status</th>
					<th class="last"><?=lang('action')?></th>
				</tr>
			</thead>
			<tbody>
				<?php $no=1;foreach($portfolios as $portfolio){ ?>
					<tr>
						<td><?= $no; ?></td>
						<td><?= $portfolio['title']; ?></td>
						<td><?= $portfolio['manager_name']; ?></td>
						<td><?php foreach($portfolio['innovations'] as $inno){ ?>
							<a href="#" class="detail-innovation" data-id="<?= $inno['innovation_id'] ?>"> - <?= ($lang == LANGUAGE_MELAYU ? $inno['name_in_melayu'] : $inno['name']); ?> </a><br/>
							<?php } ?>
						</td>
						<td><?= $portfolio['assignment_date']; ?></td>
						<td><?= ($portfolio['status'] == PORTFOLIO_TASK_DONE ? '<div class="status_approved" title="'.lang('info_task_status_done').'">&nbsp;</div>' : '<div class="status_pending" title="'.lang('info_task_status_not_yet').'">&nbsp;</div>'); ?></td>
						<td class="last">
							<a href="<?=base_url()."portfolios/view_evaluation/".$portfolio['portfolio_id']?>" class="view-result-evaluation" title="<?=lang('link_view_evaluation_result')?>"></a>
							<?php if($portfolio['assigner_id'] == $user_sess['user_id']){ ?>
								<a href="<?=base_url()."portfolios/edit_portfolio/".$portfolio['portfolio_id']?>" title="<?=lang('link_edit')?>" class="edit"></a>
								<a href="<?=base_url()."portfolios/delete_portfolio/".$portfolio['portfolio_id']?>" onclick="return confirm_delete()" title="<?=lang('link_delete')?>" class="delete"></a>
							<?php } ?>
						</td>
					</tr>
				<?php $no++;} ?>
			</tbody>
		</table>


		<!--POPUP DETAIL INNOVATION-->
		<div id="detail-innovation" style="display:none" title="<?= lang('title_popup_detail_innovation') ?>">
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		//popup innovation detail
		$('.detail-innovation').click(function(e){			
			e.preventDefault();
			var id = $(this).data('id');
			$.get(base_url+"portfolios/view_innovation/"+id, function(response){
				$('#detail-innovation').html(response);
			});

			$("#detail-innovation").dialog({
				autoOpen	: true,
				resizable	: false,
				width		: '700',
				height		: 'auto',
				modal		: true,
				position	: 'center',
				buttons: {
					"OK" : function(){
						$(this).dialog('close');
					}
				}
			});
		});
	});
</script>