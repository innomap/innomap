<style type="text/css">
	#detail_inno th{
		min-width: 150px;
		text-align: left;
	}
</style>

<table border="0" id="detail_inno">
	<?php if(count($innovator) > 0){ ?>
	<tr>
		<th><?= lang('label_innovator') ?></th>
		<td>:</td>
		<td>
			<?php foreach ($innovator as $key => $value) {
				echo "- ".$value['name']."<br/>";
			} ?>
		</td>
	</tr>
	<?php } ?>
	<tr>
		<th><?= lang('label_innovation_name') ?></th>
		<td>:</td>
		<td><?=$detail['name_in_melayu']?> </td>
	</tr>
	<tr>
		<th><?= lang('label_inspiration') ?></th>
		<td>:</td>
		<td><?=$detail['inspiration_in_melayu']?></td>
	</tr>
	<tr>
		<th><?= lang('label_product_description') ?></th>
		<td>:</td>
		<td><?=$detail['description_in_melayu']?></td>
	</tr>
	<tr>
		<th><?= lang('label_material') ?></th>
		<td>:</td>
		<td><?=$detail['materials_in_melayu']?></td>
	</tr>
	<tr>
		<th><?= lang('label_how_to_use') ?></th>
		<td>:</td>
		<td><?=$detail['how_to_use_in_melayu']?></td>
	</tr>	
	<tr>
		<th><?= lang('label_special_achievement') ?></th>
		<td>:</td>
		<td><?=$detail['special_achievement_in_melayu']?></td>
	</tr>
	<tr>
		<th><?= lang('label_created_date') ?> </th>
		<td>:</td>
		<td><?=$detail['created_date']?></td>
	</tr>
	<tr>
		<th><?= lang('label_discovered_date') ?></th>
		<td>:</td>
		<td><?=$detail['discovered_date']?></td>
	</tr>
	<tr>
		<th><?= lang('label_manufacturing_cost') ?></th>
		<td>:</td>
		<td>RM <?=number_format($detail['manufacturing_costs'], 2);?></td>
	</tr>
	<tr>
		<th><?= lang('label_selling_price') ?></th>
		<td>:</td>
		<td>RM <?=number_format($detail['selling_price'], 2)?></td>
	</tr>
	<tr>
		<th><?= lang('label_innovation_category') ?></th>
		<td>:</td>
		<td>
			<?php 
			foreach($inno_cats as $key=>$cat){
				foreach($categories as $category){ 
					echo ($cat['category_id'] == $category['category_id'] ? $key+1 .') '.$category['name'].'<br/>' : '');
				} ?>
				<div class="subcat-view">
					<p>Subcategory:</p>
					<?php 
					foreach($cat['subcategories'] as $sub){
						foreach($inno_subs as $inno_sub){ 
							echo ($inno_sub['subcategory_id'] == $sub['subcategory_id'] ? '-'.$sub['name'].'<br/>' : ''); 
						}
					} ?>
				</div>
			<?php } ?>
		</td>
	</tr>
	<tr>
		<th><?= lang('label_target') ?></th>
		<td>:</td>
		<td>
			<?php 
			if(json_decode($detail['target']) != NULL){
				foreach(json_decode($detail['target']) as $cur_target){
					echo "- ".$targets[$cur_target]."<br/>";
				}
			}else{
				echo '-';
			}?>
		</td>
	</tr>
	<tr>
		<th><?= lang('label_myipo_protection') ?> </th>
		<td>:</td>
		<td><?=($detail['myipo_protection'] == 1 ? 'Ya': 'Tidak')?></td>
	</tr>
	<tr>
		<th><?= lang('label_innovation_picture') ?></th>
		<td>:</td>
		<td>
			<?php foreach($inno_pics as $pic){?>
				<img class="innovation_img" src="<?=base_url().PATH_TO_INNOVATION_PICTURE."/".$pic['picture']?>" width="200"/>
			<?php } ?>
		</td>	
	</tr>
</table>