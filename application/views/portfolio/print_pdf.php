<div id="page-wrapper"> 
   <table>
        <tr>
            <td>
                <h1 class="page-header" align="center">
                    <?=lang('title_portfolio') ." '".$innovation['name_in_melayu']."'"?>
                </h1>
            </td>
            <td align="right">
                <img src="<?=base_url()?>assets/img/logo.png" class="grid_4 alpha omega">
            </td>
        </tr>
    </table>

    <h3><?=lang('title_detail_innovation')?></h3>
    <table cellpadding="5">
        <?php if(count($innovator) > 0){ ?>
            <tr>
                <td><?= lang('label_innovator') ?></td>
                <td>: 
                    <?php foreach ($innovator as $key => $value) {
                        echo "- ".$value['name']."<br/>";
                    } ?>
                </td>
            </tr>
        <?php } ?>

        <?php if($full){ ?>
            <tr>
                <td><?= lang('label_inspiration') ?></td>
                <td>: <?=$innovation['inspiration_in_melayu']?></td>
            </tr>

            <tr>
                <td><?= lang('label_product_description') ?></td>
                <td>: <?=$innovation['description_in_melayu']?></td>
            </tr>

            <tr>
                <td><?= lang('label_material') ?></td>
                <td>: <?=$innovation['materials_in_melayu']?></td>
            </tr>

            <tr>
                <td><?= lang('label_how_to_use') ?></td>
                <td>: <?=$innovation['how_to_use_in_melayu']?></td>
            </tr>
            
            <tr>
                <td><?= lang('label_special_achievement') ?></td>
                <td>: <?=$innovation['special_achievement_in_melayu']?></td>
            </tr>

            <tr>
                <td><?= lang('label_created_date') ?></td>
                <td>: <?=$innovation['created_date']?></td>
            </tr>
            
            <tr>
                <td><?= lang('label_discovered_date') ?></td>
                <td>: <?=$innovation['discovered_date']?></td>
            </tr>

            <tr>
                <td><?= lang('label_manufacturing_cost') ?></td>
                <td>: RM <?=number_format($innovation['manufacturing_costs'], 2);?></td>
            </tr>

            <tr>
                <td><?= lang('label_selling_price') ?></td>
                <td>: RM <?=number_format($innovation['selling_price'], 2)?></td>
            </td>

            <tr>
                <td><?= lang('label_innovation_category') ?></td>
                <td>: 
                    <?php 
                    foreach($inno_cats as $key=>$cat){
                        foreach($categories as $category){ 
                            echo ($cat['category_id'] == $category['category_id'] ? $key+1 .') '.$category['name'].'<br/>' : '');
                        } ?>
                        <div class="subcat-view">
                            <p>Subcategory:</p>
                            <?php 
                            foreach($cat['subcategories'] as $sub){
                                foreach($inno_subs as $inno_sub){ 
                                    echo ($inno_sub['subcategory_id'] == $sub['subcategory_id'] ? '-'.$sub['name'].'<br/>' : ''); 
                                }
                            } ?>
                        </div>
                    <?php } ?>
                </td>
            </tr>

            <tr>
                <td><?= lang('label_target') ?></td>
                <td>: 
                    <?php 
                    if(json_decode($innovation['target']) != NULL){
                        foreach(json_decode($innovation['target']) as $cur_target){
                            echo "- ".$targets[$cur_target]."<br/>";
                        }
                    }else{
                        echo '-';
                    }?>
                </td>
            </tr>

            <tr>
                <td><?= lang('label_myipo_protection') ?></td>
                <td>: <?=($innovation['myipo_protection'] == 1 ? 'Ya': 'Tidak')?></td>
            </tr>

            <tr>
                <td><?= lang('label_innovation_picture') ?></td>
                <td>: 
                    <?php foreach($inno_pics as $pic){?>
                        <img class="innovation_img" src="<?=base_url().PATH_TO_INNOVATION_PICTURE_THUMB."/".$pic['picture']?>" width="200"/>
                    <?php } ?>
                </td>
            </tr>
        <?php } ?>
    </table>

    <h3><?=lang('title_portfolio_progress')?></h3>
    <?php foreach($items as $key => $item){ ?>
        <div class="item-wrap">
            <hr/>
            <h4><?=lang('scope')?>: <?=$item['name_in_melayu']?></h4>

            <div class="item-attachment">
                <table width="100%">
                    <tr>
                        <td colspan="2"><h4><?=lang('label_task_list')?></h4></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table cellpadding="5" width="100%" border="1">
                                <tr>
                                    <th><?=lang('label_task_name')?></th>
                                    <th><?=lang('label_weightage')?></th>
                                    <th><?=lang('label_progress')?></th>
                                    <th><?=lang('label_notes')?></th>
                                </tr>
                                <?php 
                                    $scope_progress = 0; $scope_amount = 0; 
                                    foreach ($item['tasks'] as $key => $task) { 
                                        $task_progress_weight = ($task['progress'] / 100) * $task['weightage'];
                                        $scope_progress = $scope_progress + $task_progress_weight;?>
                                        <tr class="<?=($key %2 == 0 ? 'odd' : 'even')?>">
                                            <td><?=$task['task']?></td>
                                            <td><?=$task['weightage']?>%</td>
                                            <td><?=$task['progress']?>%</td>
                                            <td><?=$task['note']?></td>
                                        </tr>
                                <?php } ?>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td><?=lang('label_progress')?></td>
                        <td>: <b><?=$scope_progress?></b>%</td>
                    </tr>
                    <tr>
                        <td><?=lang('label_budget')?></td>
                        <td>: RM <b><?=$item['budget']?></b></td>
                    </tr>
                    <tr>
                        <td><?=lang('label_amount_spend')?></td>
                        <td>: RM <b><?=$item['tasks'][0]['amount_spend']?></b></td>
                    </tr>
                    <tr>
                        <td><?=lang('label_ballance')?></td>
                        <td>: RM <b>
                            <?php
                                $ballance = $item['budget'] - $item['tasks'][0]['amount_spend'];
                                echo number_format((float)$ballance, 2, '.', '');
                            ?></b></td>
                    </tr>
                    <tr>
                        <td>Progress Skop Keseluruhan</td>
                        <td>: <b><?php
                                $scope_progress_weight = ($scope_progress / 100) * $item['weightage'];
                                echo $scope_progress_weight . "% / " . $item['weightage'] . "%";
                              ?></b></td>
                    </tr>
                </table>
            </div>
        </div>
    <?php } ?>
</div>