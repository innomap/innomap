<div class="block innovator green">
	<div class="title"><?=lang('link_setup_scope')?></div>
	
	<div class="block-content">
		<div class="form-info warning"><?=lang('scope_portfolio_warning')?></div>

		<div class="detail-content grid_11 alpha">
			<form name="portfolio_scope" method="POST" action="<?=base_url()."portfolios/save_scope"?>" id="portfolio-scope">
				<input type="hidden" name="portfolio_id" value="<?= $portfolio_id ?>">
				
				<?php foreach ($criterias as $key => $criteria) { ?>
					<div class="scope-criteria-parent">
						<div class="label grid_12">
							<input type="checkbox" name="criteria_id[]" class="scope-criteria-chk" value="<?=$criteria['criteria_id']?>"/> <?=$criteria['name']?>
						</div>
						<div class="value grid_12 display-none scope-criteria-detail">
							<div class="grid_12">
								<div class="label grid_2">
									<?=lang('label_weightage')?>
								</div>
								<div class="value grid_3">
									<input type="number" class="scope-weightage" name="weightage[<?=$criteria['criteria_id']?>]" max="100" min="1" /> %
								</div>
								<span class="red-text scope-weightage-error" style="display:none">Jumlah wajaran harus 100</span>
							</div>

							<div class="grid_12">
								<div class="label grid_2">
									<?=lang('label_task_list')?>
								</div>
								<div class="value grid_9 task-list">
									<span class="red-text task-weightage-error" style="display:none">Jumlah wajaran tugas harus 100</span>
									<div>
										<input type="text" class="grid_2" name="task_name[<?=$criteria['criteria_id']?>][]" placeholder="<?=lang('label_task_name')?>"/>
										<input type="text" readonly class="grid_2 datepicker" name="task_start_date[<?=$criteria['criteria_id']?>][]" placeholder="<?=lang('label_start_date')?>"/>
										<input type="text" readonly class="grid_2 datepicker" name="task_end_date[<?=$criteria['criteria_id']?>][]" placeholder="<?=lang('label_end_date')?>"/>
										<input type="number" class="grid_1 task-weightage" name="task_weightage[<?=$criteria['criteria_id']?>][]" placeholder="<?=lang('label_weightage')?>" max="100" min="0" value="0"/>%
										<a href="#" class="add-task"><img src="<?=base_url()."assets/img/add-access-icon.png"?>" width="20"></a>&nbsp;
									</div>
								</div>
							</div>

							<div class="grid_12">
								<div class="label grid_2">
									<?=lang('label_budget')?>
								</div>
								<div class="value grid_3">
									RM <input type="number" name="budget[<?=$criteria['criteria_id']?>]" />
								</div>
							</div>
						</div>
					</div>
				<?php } ?>
				
				<div class="grid_3 right">
					<br/>
					<a class="btn red grid_1 omega alpha" href="<?=base_url()."portfolios"?>"><?=lang('button_cancel')?></a>
					<input type="submit" id="btn-save-scope" class="btn" value="<?=lang('button_save')?>" disabled />
				</div>
			</form>
		</div>
	</div>
</div>