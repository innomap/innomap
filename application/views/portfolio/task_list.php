<div class="block portfolio">
	<div class="title"><?=lang('label_portfolio_task')?></div>
	
	<div class="block-content">
		<?php if($flashdata != NULL){
			echo "<div class='form-info ".($flashdata['success'] ? 'success' : 'fail')."'>".$flashdata['msg']."</div>";
		} ?>

		<?php if(count($portfolios) > 0){ ?>
			<table cellpadding="0" cellspacing="0" border="0" class="display" id="portfolio_table" width="100%">
				<thead>
					<th>No.</th>
					<th><?=lang('label_title')?></th>
					<th><?=lang('label_assignment_date')?></th>
					<th><?=lang('action')?></th>
				</thead>
				<tbody>
				<?php $no=1;foreach($portfolios as $portfolio){ ?>
					<tr>
						<td><?= $no; ?></td>
						<td><?= $portfolio['title']; ?></td>
						<td><?= $portfolio['assignment_date']; ?></td>
						<td style="padding:10px"><?php if($portfolio['status'] != PORTFOLIO_TASK_DONE){ 
								if($portfolio['is_scope_exist']){ ?>
									<a href="<?= base_url().'portfolios/check/'.$portfolio['portfolio_id']; ?>"><?=lang('link_check_portfolio')?></a>
								<?php } else {  ?>
									<a href="<?= base_url().'portfolios/scope/'.$portfolio['portfolio_id']; ?>" class="btn"><?=lang('link_setup_scope')?></a>
								<?php } ?>
							<?php }else{ ?>
								<a href="<?= base_url().'portfolios/view_evaluation/'.$portfolio['portfolio_id']; ?>" class="view-result-evaluation" title="<?=lang('link_view_evaluation_result')?>"></a>
							<?php } ?>
						</td>
					</tr>
				<?php $no++;} ?>
				</tbody>
			</table>
		<?php }else{ ?>
			<p><?=lang('info_dont_have_task')?>.</p>
		<?php } ?>
	</div>
</div>

<?php if($show_notif){ ?>
	<div id="manager-notification" style="display:none" title="<?=lang('menu_notif')?>">
		<h3><?=lang('subtitle_notif_task')?></h3>
		<?php if(count($notif_tasks) > 0){ ?>
			<table class="table-styled">
				<tr>
					<th><?=lang('portfolio_title')?></th>
					<th><?=lang('label_criteria')?></th>
					<th><?=lang('label_task_name')?></th>
					<th><?=lang('label_start_date')?></th>
					<th><?=lang('label_end_date')?></th>
					<th><?=lang('label_weightage')?></th>
				</tr>
				<?php foreach ($notif_tasks as $key => $notif_task) { ?>
					<tr class="<?=($key % 2 == 0 ? 'even' : 'odd')?>">
						<td><?=$notif_task['portfolio_title']?></td>
						<td><?=$notif_task['criteria_name']?></td>
						<td><?=$notif_task['task']?></td>
						<td><?=$notif_task['start_date']?></td>
						<td><?=$notif_task['end_date']?></td>
						<td><?=$notif_task['weightage']?>%</td>
					</tr>
				<?php } ?>
			</table>
		<?php }else{
			echo "<i>".lang('msg_no_notif_task')."</i>";
		} ?>

		<h3><?=lang('subtitle_notif_note')?></h3>
		<?php if(count($notif_notes) > 0){ ?>
			<table class="table-styled">
				<tr>
					<th><?=lang('portfolio_title')?></th>
					<th><?=lang('label_title')?></th>
					<th><?=lang('label_content')?></th>
					<th><?=lang('label_creation_date')?></th>
				</tr>
				<?php foreach ($notif_notes as $key => $notif_note) { ?>
					<tr class="<?=($key % 2 == 0 ? 'even' : 'odd')?>">
						<td><?=$notif_note['portfolio_title']?></td>
						<td><?=$notif_note['title']?></td>
						<td><?=$notif_note['content']?></td>
						<td><?=$notif_note['creation_date']?></td>
					</tr>
				<?php } ?>
			</table>
		<?php }else{
			echo "<i>".lang('msg_no_notif_note')."</i>";
		} ?>
	</div>

	<script>
	$(document).ready(function(){
		$("#manager-notification").dialog({
			autoOpen	: true,
			resizable	: false,
			width		: 'auto',
			height		: 'auto',
			modal		: true,
			position	: 'center',
			buttons: {
				"OK" : function(){
					$(this).dialog('close');
				}
			}
		});
		$("#manager-notification").dialog('open');
	});
	</script>
<?php } ?>
