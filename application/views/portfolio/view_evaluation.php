<div class="block detail_portfolio">
	<div class="title"><?=lang('title_portfolio') ." '".$detail['title']."'"?></div>
	<input type="hidden" name="portfolio_id" value="<?=$detail['portfolio_id']?>" />

	<div class="block-content">

		<div class="grid_4" style="margin-left:6%;">
			<div class="subblock">
				<input type="hidden" id="assignment-date" value="<?=date('m/d/Y',strtotime($detail['assignment_date']))?>" />			
				<div class="title"><?=lang('label_assignment_date')?></div>
				<div class="subblock-content">
					<div id="calendar"></div>
					<div style="margin-top:20px">
						<?php foreach ($scopes as $key_scope => $scope) { ?>
							<div>
								<div style="width:15px;height:15px;float:left" class="calendar-legend-color-<?=$key_scope?>"></div>
								&nbsp;<span><?=$scope['name_in_melayu']?></span>
								<hr/>
							</div>
						<?php } ?>
						<!-- <div>
							<div style="width:15px;height:15px;float:left" class="calendar-legend-all"></div>
							&nbsp;<span>BANYAK SKOP</span>
							<hr/>
						</div> -->
					</div>
				</div>
				<input type="hidden" id="highlight-date" value="<?=$note_dates?>" />
			</div>
			
			<div class="subblock">
				<div class="title"><?=lang('task_updates')?></div>
				<div class="subblock-content">
					<div class="task-updates">
						<?php if(count($notes) > 0){
							foreach($notes as $note){ ?>
								<p>- <?=$note['title']?> <a href="#" class="view-icon"></a>
								   <input type="hidden" class="portfolio-note-id" value="<?=$note['portfolio_note_id']?>" />
								</p>
						<?php } 
						}else{
							echo '<p class="red-text">'.lang('info_no_notes').'</p>';
						} ?>
					</div>
				</div>
			</div>
		</div>

		<div class="grid_6 omega">
			<div class="subblock">
				<div class="title"><?=lang('title_portfolio_progress')?></div>
				<div class="subblock-content">
					<?php $item_num = 0;
					foreach($innovations as $innovation){
						$innovation_progress = 0; ?>
						<div class="each-portfolio">
							<a class="innovation-name" href="#" style="float:left;margin-right: 20px;margin-bottom:40px;" data-id="<?= $innovation['innovation_id']; ?>"><?=$innovation['name_in_melayu']?></a> 
							<div class="progress-bar"><span>0%</span></div>
							<h4><?=lang('scope')?></h4>
							<?php foreach($innovation['items'] as $key=>$item){
								$item_num = $item_num + 1;?>
								<div class="item-wrap">
									<span class="criteria-name"><?=$item['name_in_melayu']?></span>
									<div class="item-attachment">
										<table width="100%">
											<tr>
												<td><h4><?=lang('label_task_list')?></h4></td>
												<td align="right"><span class="scope-progress"></span>% / <?=$item['weightage']?> %</td>
											</tr>
											<tr>
												<td colspan="3">
													<table cellpadding="5" width="100%" class="table-styled">
														<tr>
															<th><?=lang('label_task_name')?></th>
															<th><?=lang('label_weightage')?></th>
															<th><?=lang('label_progress')?></th>
															<th><?=lang('label_action')?></th>
														</tr>
														<?php 
															$scope_progress = 0; $scope_amount = 0;	
															foreach ($item['tasks'] as $key => $task) { 
																$task_progress_weight = ($task['progress'] / 100) * $task['weightage'];
																$scope_progress = $scope_progress + $task_progress_weight;?>
																<tr class="<?=($key %2 == 0 ? 'odd' : 'even')?>">
																	<td><?=$task['task']?></td>
																	<td><?=$task['weightage']?>%</td>
																	<td><?=$task['progress']?>%</td>
																	<td>
																		<?php if($task['is_pending_pm_approval']){ ?>
																			<a href="<?=base_url()?>portfolios/approve_task/<?=$task['innovation_portfolio_id']?>" 
																				onclick="return confirmation(msg_confirm_portfolio_task_change['<?=$this->site_lang?>'])"><?=lang('link_approve')?></a>
																		<?php } ?>

																		<?php if($task['progress'] > 0){ ?>
																			<a href="#" 
																				class="view-attachment view-icon" 
																				task-id="<?=$task['task_id']?>"
																				innovation-id="<?=$innovation['innovation_id']?>"
																				innovation-name="<?=$innovation['name_in_melayu']?>" 
																				item-name="<?=$item['name']?>" 
																				title="<?=lang('link_view')?>"></a>
																		<?php } ?>
																	</td>
																</tr>
														<?php } ?>
													</table>
												</td>
											</tr>
											<tr>
												<td><?=lang('label_progress')?></td>
												<td>: <b><?=$scope_progress?></b>%</td>
											</tr>
											<tr>
												<td><?=lang('label_budget')?></td>
												<td>: RM <b><?=$item['budget']?></b></td>
											</tr>
											<tr>
												<td><?=lang('label_amount_spend')?></td>
												<td>: RM <b id="scope-amount-spend"><?=$item['tasks'][0]['amount_spend']?></b></td>
											</tr>
											<tr>
												<td><?=lang('label_ballance')?></td>
												<td>: RM <b>
													<?php
														$ballance = $item['budget'] - $item['tasks'][0]['amount_spend'];
														echo number_format((float)$ballance, 2, '.', '');
													?></b></td>
											</tr>
										</table>
									</div>

									<?php
										$scope_progress_weight = ($scope_progress / 100) * $item['weightage'];
										$innovation_progress = $innovation_progress + $scope_progress_weight;
									?>
									<input type="hidden" name="scope_progress" value="<?=$scope_progress_weight?>" />
								</div>
							<?php } ?>
							<input type="hidden" class="innovation-portfolio-progress" value="<?=$innovation_progress?>" />
						</div>
						<hr style="color:#78d1f0; background:#78d1f0; height:2px; border:0;" />
					<?php } ?>

					<div id="general-progress"></div>
					<div class="clear"></div>
					<div class="legend">
						<div class="legend-blue"><?=lang('info_uploaded')?></div>
						<div class="legend-red"><?=lang('info_not_uploaded')?></div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
		
		<?php if($flashdata != NULL){
			echo "<div class='form-info ".($flashdata['success'] ? 'success' : 'fail')."'>".$flashdata['msg']."</div><br/>";
		}?>
			
		<div class="subblock" class="grid_10" >
			<div class="title">Komentar dari IDec Head</div>
			<div class="comment-wrapper">
				<?php if(count($comments) > 0){
					foreach($comments as $comment){?>
					<div class="grid_10">
						<?=$comment['content']?>
						<br/><span class="posted-time">on <?=$comment['timestamp']?></span>
					</div>
				<?php }
				}else{?>	
					<p class="grid_10 red-text">Tiada komen dari IDeC Head</p>
				<?php } ?>
			</div>
			<?php if($is_creator){ ?>
				<form method="POST" action="<?=base_url()."portfolios/save_comment"?>" id="portfolio-comment">
					<div class="grid_10">
						<textarea class="grid_8" rows="8" name="comment_content"></textarea>
					</div>
					<input type="hidden" name="comment_portfolio_id" value="<?=$detail['portfolio_id']?>" />
					<input type="submit" value="Send" class="grid_2 push_6 btn" style="margin-left:27px" />
				</form>
			<?php } ?>
			<div class="clear"></div>
		</div>
			
		<a href="<?=base_url()."portfolios"?>" class="btn red"><< <?=lang('button_back')?></a>
		
		<!-- UPLOADED ITEM ATTACHMENT -->
		<div id="uploaded-attachment" style="display:none" title="<?=lang('title_attachment_detail')?>">
			<table>
				<tr>
					<th><?=lang('label_innovation_name')?>:</th>
					<td><label id="ua-inno-name"></label></td>
				</tr>
				<tr>
					<th><?=lang('label_item')?>:</th>
					<td><label id="ua-item"></label></td>
				</tr>
				<tr>
					<th><?=lang('label_attachment')?>:</th>
					<td><div id="ua-attachment"></div></td>
				</tr>
				<tr>
					<th><?=lang('label_notes')?>:</th>
					<td><label id="ua-notes"></label></td>
				</tr>
			</table>
		</div>
		
		<!-- VIEW SAVED ASSET URL -->
		<div id="saved-asset" style="display:none" title="<?=lang('link_title_uploaded_asset')?>"></div>
		
		<!-- VIEW SAVED NOTE -->
		<div id="saved-note" style="display:none" title="Detail">
			<b><?=lang('label_title')?>: </b><label></label><br/>
			<b><?=lang('label_content')?>: </b><label></label><br/>
			<b><?=lang('label_attachment')?>: </b><label></label><br/>
			<b><?=lang('label_creation_date')?>: </b><label></label>
		</div>

		<!--POPUP DETAIL INNOVATION-->
		<div id="detail-innovation" style="display:none" title="<?= lang('title_popup_detail_innovation') ?>">
		</div>

		<!-- POPUP CURRENT TASK -->
		<div id="current-tasks" style="display:none" title="<?= lang('label_task_list') ?>">
			<table id="innovator_table">
				<thead>
					<tr style="color:white">
						<th><?=lang('scope')?></th>
						<th><?=lang('label_task_name')?></th>
						<th><?=lang('label_weightage')?></th>
						<th><?=lang('label_start_date')?></th>
						<th><?=lang('label_end_date')?></th>
					</tr>
				</thead>
				<tbody id="highlight-date-task">
				</tbody>
			</table>
		</div>
	</div>
</div>

<script>
	var highlight_dates = <?php echo json_encode($highlight_dates); ?>;
</script>