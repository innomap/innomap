<div class="block innovator green">
	<div class="title"><?=lang('title_group_scoring')?></div>
	<div class="block-content">		
			
		<ul class="grid_10" id="tabs">
			<?php foreach($group_list as $key=>$value){ ?>
				<li class="<?= ($group == $value['expert_group_id'] ? 'active' : ''); ?>"><a href="<?= base_url()."reports/group_scoring/".$value['expert_group_id'] ?>"><?= $value['name']; ?></a></li>
			<?php } ?>
		</ul>
		<div class="detail-content grid_11 alpha" style="padding:15px 10px;">
			<form method="POST" action="<?=base_url()."reports/export_group_scoring"?>">
				<input type="hidden" name="group" value="<?=$group?>" />
				<input type="submit" class="btn" value="<?=lang('button_export_to_excel')?>" />
			</form>
			
			<table cellpadding="0" cellspacing="0" border="0" class="table green-table scrollable-table" width="100%">
				<thead>
					<tr>
						<th rowspan="2">Innovation</th>
						<th colspan="<?= count($experts)+2; ?>"><?= $title ?></a>
					</tr>
					<tr>
						<?php foreach($experts as $key=>$value){ ?>
							<th><?= $value['name'] ?></th>
						<?php } ?>
						<th><?= lang('label_total_score') ?></th>
						<th><?= lang('label_score_average') ?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($innovations as $key=>$value){ ?>
						<tr>
							<td><?= $value['name'] ?></td>
							<?php foreach($experts as $expert){ ?>
								<td>
									<?php if(isset($value[$expert['expert_id']]['total'])){
											if($value[$expert['expert_id']]['total'] != '-'){ ?>
												<a class="btn-evaluation-detail" href="#" eval_id="<?= $value[$expert['expert_id']]['evaluation_id'] ?>" title="<?= lang('button_view_evaluation') ?>"><?= $value[$expert['expert_id']]['total']; ?></a>
									<?php 	}else{
												echo "-";
											}
										}else{ echo "-"; } ?>
								</td>
							<?php } ?>
							<td><?= $value['total']; ?></td>
							<td><?= $value['average']; ?></td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
	<div class="clear"></div>
</div>