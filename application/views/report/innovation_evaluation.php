<div class="block innovator">
	<div class="title"><?=lang('title_innovation_evaluation')?></div>
	<div class="block-content">
		<form method="post" id="report-innovation-evaluation" action="">
			<div class="grid_11">
				<div class="grid_2"><?=lang('label_filter_by')?>: </div>
				<div class="grid_8">
					<input type="radio" name="filter_by" value="innovation"><?= lang('label_innovation') ?></input>
					<input type="radio" name="filter_by" value="innovator"><?= lang('label_innovator') ?></input>
					<input type="radio" name="filter_by" value="expert"><?= lang('label_expert') ?></input>
				</div>
			</div>
			<div class="grid_11 innovation_filter filter">
				<div class="label grid_2"><?= lang('label_innovation_name') ?></div>
				<div class="value grid_5 expert-list"><input type="text" name="ids" id="innovation-inp" class="ms"/></div>
				<i>* <?= lang('filter_info'). " " .lang('label_innovation') ?></i>
			</div>
			<div class="grid_11 innovator_filter filter">
				<div class="label grid_2"><?= lang('innovator_name') ?></div>
				<div class="value grid_5 expert-list"><input type="text" name="ids" id="innovator-inp"/></div>
				<i>* <?= lang('filter_info'). " " .lang('label_innovator') ?></i>
			</div>
			<div class="grid_11 expert_filter filter">
				<div class="label grid_2"><?= lang('label_name') ?></div>
				<div class="value grid_5 expert-list"><input type="text" name="ids" id="expert-inp"/></div>
				<i>* <?= lang('filter_info'). " " .lang('label_expert') ?></i>
			</div>
			<div class="grid_11" style="margin-top:10px">
				<input type="hidden" id="data_type" value="evaluation" />
				<input type="hidden" name="report_title" id="graph-title" value="<?=lang('title_innovation_evaluation')?>">
				<input type="submit" name="generate" class="btn" value="<?=lang('button_generate_report')?>" />
				<?php if(!$is_topscore){ ?>
					<input type="submit" class="btn" id="button-export" value="<?=lang('button_export_to_excel')?>" />
				<?php } ?>
			</div>
		</form>
		
		<?php if($is_topscore){ ?>
			<h2 class='grid_11'><?= lang('title_top_score') ?></h2>
			<form method="POST" action="<?=base_url()."reports/export_evaluation"?>">
				<input type="hidden" name="report_title" id="graph-title" value="<?=lang('title_innovation_evaluation')?>" />
				<input type="submit" class="btn" value="<?=lang('button_export_to_excel')?>" />
			</form>
		<?php } ?>
		
		<?php if(count($results) > 0){ ?>
			<table cellpadding="0" cellspacing="0" border="0" class="table blue-table scrollable-table" width="100%">
				<thead>
					<tr>
						<th><?= lang('label_no') ?></th>
						<th><?= lang('label_innovation_name') ?></th>
						<?php foreach($all_experts as $expert){ ?>
							<th><?= $expert['name'] ?></th>
						<?php } ?>
						<th><?= lang('label_total_score') ?></th>
						<th><?= lang('label_score_average') ?></th>
					</tr>
				</thead>
				<tbody>
					<?php $no=1; foreach($results as $result){ ?>
						<tr>
							<td><?= $no ?></td>
							<td>
								<a target="_blank" title="<?= lang('title_detail_innovation') ?>" href="<?=base_url()."innovations/view/".$result['id']?>">
									<?= $result['name'] ?>
								</a>
							</td>
							<?php foreach($result['scores'] as $score){ ?>
								<td align="center">
									<?php if(count($score) > 0) { ?>
										<a class="btn-evaluation-detail" href="#" eval_id="<?= $score['evaluation_id'] ?>" title="<?= lang('button_view_evaluation') ?>">
											<?= $score['total'] ?>
										</a>
									<?php }else{ 
										echo "-";
									} ?>
								</td>
							<?php } ?>
							<td align="center"><?= $result['total_score'] ?></td>
							<td align="center"><?= $result['average']; ?></td>
						</tr>
					<?php $no++;} ?>
					<tfoot>
						<tr class="additional">
							<td style="text-align:right" colspan="<?= 3 + count($all_experts) ?>"><b><?= lang('label_overall_average'); ?></b></td>
							<td align="center"><?= $overall_avg ?></td>
						</tr>
					</tfoot>
				</tbody>
			</table>
		<?php } ?>
	</div>
	<div class="clear"></div>
</div>

<script>
$(function(){
	show_filter_content();
	generate_suggest('#innovation-inp', <?=json_encode($innovations)?>);
	generate_suggest('#innovator-inp', <?=json_encode($innovators)?>);
	generate_suggest('#expert-inp', <?=json_encode($experts)?>);
});
</script>