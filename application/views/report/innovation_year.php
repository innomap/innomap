<div class="block innovator">
	<div class="title"><?=lang('title_innovation_by_year')?></div>
	<div class="block-content">
		<div class="" id="content-chart" content_type="innovation_year">
			<form class="webform" action="<?= base_url()."reports/innovation_year_handler" ?>" id="report-innovation-year" method="post">							
				<?=lang('label_innovation_year')?> :
				<select name="filter_year">
					<option value="all">- <?= lang('label_all') ?> -</option>
					<?php foreach($years as $year){?>
						<option value="<?=$year['year']?>"><?=$year['year']?></option>
					<?php } ?>
				</select>
				
				<br/><br/>
				<a id="innovation_year" class="btn"><?=lang('button_generate_report')?></a>
				<input type="submit" class="btn" id="button-export" value="<?=lang('button_export_to_excel')?>" />
				<input type="hidden" id="data_type" value="innovation" />
				<input type="hidden" name="report_type" value="innovation_year" />
				<a class="print_page btn"><?=lang('button_print_graph')?></a>
				
				<input type="hidden" name="report_title" id="graph-title" value="<?=lang('title_innovation_by_year')?>">
				<input type="hidden" id="graph-x-title" value="<?=lang('label_innovation_year')?>">
				<input type="hidden" id="graph-y-title" value="<?=lang('info_total_innovation')?>">
			</form>
			<br>
			<br>
			<div id="chart-container" align="left">The chart will appear within this DIV. This text will be replaced by the chart.</div>
		</div>
	</div>
</div>

<div id="report-dialog" style="display:none">
	<table class="dataTables_custom biz-table" style="width:100%">
		<thead>
			<tr>
				<th>No</th>
				<th><?=lang('label_innovation_name'); ?></th>
				<th><?=lang('label_innovator')?></th>
				<th><?=lang('label_discovered_date') ?></th>
				<th><?=lang('label_score_average')?></th>
				<th></th>
			</tr>
		</thead>
		<tbody id="people-dialog"></tbody>
	</table>
</div>