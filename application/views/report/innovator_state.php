<div class="block innovator">
	<div class="title"><?=lang('title_innovator_by_state')?></div>
	<div class="block-content">
		<div class="" id="content-chart" content_type="innovator_state">
			<form class="webform" action="<?= base_url()."reports/innovator_state_handler" ?>" id="report-innovator-state" method="post">
				<div class="grid_11">
					<div class="column_3">
						<input type="checkbox" class="state-value" name="innovator_state[]" checked="checked" value="0"><?= lang('label_all') ?><br/>
						<?php foreach($states as $state){ ?>
							<input type="checkbox" name="innovator_state[]" class="state-value" value="<?= $state['state_id']; ?>"><?= $state['name']; ?><br/>
						<?php } ?>
					</div>
				</div>
				
				<br/><br/>
				<a id="innovator_state" class="btn"><?=lang('button_generate_report')?></a>
				<input type="submit" class="btn" id="button-export" value="<?=lang('button_export_to_excel')?>" />
				<input type="hidden" id="data_type" value="innovator" />
				<input type="hidden" name="report_type" value="innovator_state" />
				<a class="print_page btn"><?=lang('button_print_graph')?></a>
				
				<input type="hidden" name="report_title" id="graph-title" value="<?=lang('title_innovator_by_state')?>">
				<input type="hidden" id="graph-x-title" value="<?=lang('label_state')?>">
				<input type="hidden" id="graph-y-title" value="<?=lang('info_total_innovator')?>">
			</form>
			<br>
			<br>
			<div id="chart-container" align="left">The chart will appear within this DIV. This text will be replaced by the chart.</div>
		</div>
	</div>
</div>

<div id="report-dialog" style="display:none">
	<table class="dataTables_custom biz-table" style="width:100%">
		<thead>
			<tr>
				<th>No</th>
				<th><?=lang('label_name'); ?></th>
				<th><?=lang('label_email_address')?></th>
				<th><?=lang('label_identification_code_no') ?></th>
				<th><?=lang('label_address')?></th>
				<th><?=lang('label_state')?></th>
				<th></th>
			</tr>
		</thead>
		<tbody id="people-dialog"></tbody>
	</table>
</div>