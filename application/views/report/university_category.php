<div class="block innovator">
	<div class="title"><?=lang('title_university_by_category')?></div>
	<div class="block-content">
		<div class="" id="content-chart" content_type="university_category">
			<form class="webform" action="<?= base_url()."reports/university_category_handler" ?>" id="report-university-category" method="post">
				<div class="grid_11">
					<div class="column_3">
						<input type="checkbox" class="category-value" name="university_category[]" checked="checked" value="0"><?= lang('label_all') ?><br/>
						<?php foreach($categories as $category){ ?>
							<input type="checkbox" name="university_category[]" class="category-value" value="<?= $category['category_id']; ?>"><?= $category['name']; ?><br/>
						<?php } ?>
					</div>
				</div>
				
				<br/><br/>
				<a id="university_category" class="btn"><?=lang('button_generate_report')?></a>
				<input type="submit" class="btn" id="button-export" value="<?=lang('button_export_to_excel')?>" />
				<input type="hidden" id="data_type" value="university" />
				<input type="hidden" name="report_type" value="university_category" />
				<a class="print_page btn"><?=lang('button_print_graph')?></a>
				
				<input type="hidden" name="report_title" id="graph-title" value="<?=lang('title_university_by_category')?>">
				<input type="hidden" id="graph-x-title" value="<?=lang('label_category')?>">
				<input type="hidden" id="graph-y-title" value="<?=lang('info_total_university')?>">
			</form>
			<br>
			<br>
			<div id="chart-container" align="left">The chart will appear within this DIV. This text will be replaced by the chart.</div>
		</div>
	</div>
</div>

<div id="report-dialog" style="display:none">
	<table class="dataTables_custom biz-table" style="width:100%">
		<thead>
			<tr>
				<th>No</th>
				<th><?=lang('label_name'); ?></th>
				<th><?=lang('label_contact_no') ?></th>
				<th><?=lang('label_address')?></th>
				<th><?=lang('label_category')?></th>
				<!--<th></th>-->
			</tr>
		</thead>
		<tbody id="people-dialog"></tbody>
	</table>
</div>