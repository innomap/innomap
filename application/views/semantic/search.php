<div class="block innovator">
	<div class="title">
		<div><?=lang('title_semantic_search')?></div>
		<div class="clear"></div>
	</div>
	<div class="block-content" id="semantic">
		<h3 id="bread-title" style="display:none">Carian sebelumnya: </h3>
		<div id="breadcrumbs" style="display:none;background:whiteSmoke;padding:3px 10px !important;text-align:left"></div>
		<form id="semsearch-form" method="POST">
			<div class="label grid_2"><?=lang('label_keyword')?></div>
			<input type="text" id="semsearch-inp" class="grid_6" />
			<input type="hidden" id="semsearch-type" />
			<input type="submit" class="btn" value="<?=lang('label_search')?>"/>			
		</form>
		
		<a id="detail-toggle" class="btn" style="cursor:pointer"><?=lang('label_data_details')?></a>
		<a id="graph-toggle" class="btn" style="cursor:pointer"><?=lang('label_graph_details')?></a>
		<div id="semsearch-result"></div>
		<div id="semsearch-graph" style="display:none"></div>
	</div>
</div>

<div id="sem-graph-img-detail"></div>