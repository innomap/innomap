<div class="left grid_4 alpha">
	<div class="block orange grid_4 alpha omega">
		<div class="block-content quote">
			<div class="blockquote">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec ipsum sollicitudin risus pulvinar dictum. Fusce elit ante,hendrerit non porta non,</p>
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>
<div class="grid_8 omega slideshow">
	<div class="flexslider big-slider">
         <ul class="slides">
            <li>
  	    	    <img src="<?= base_url()?>assets/img/kitchen_adventurer_cheesecake_brownie.jpg" />
  	    	</li>
  	    	<li>
  	    	    <img src="<?= base_url()?>assets/img/kitchen_adventurer_lemon.jpg" />
  	    	</li>
  	    	<li>
  	    	    <img src="<?= base_url()?>assets/img/kitchen_adventurer_donut.jpg" />
  	    	</li>
  	    	<li>
  	    	    <img src="<?= base_url()?>assets/img/kitchen_adventurer_caramel.jpg" />
  	    	</li>
         </ul>
    </div>
</div>
<div class="clear"></div>
<?php if($content[0]['type'] == HOMEPAGE_CONTENT_INNOVATION){ ?>
	<div class="grid_12 alpha innovation">
		<div class="block blue">
			<div class="title title_innovation"><?= $content[0]['content']['title'] ?></div>
			<div class="block-content content-innovation">
				<div class="innovation-innovator grid_3">
					<div class="img-innovator">
						<?php $thumb = file_exists(realpath(APPPATH . '../assets/attachment/innovator_picture') . DIRECTORY_SEPARATOR . $content[0]['content']['picture']) ? BASE_URL() . "assets/attachment/innovator_picture/" . $content[0]['content']['picture'] : BASE_URL() . "assets/attachment/innovator_picture/default.jpg"; ?>
						<img src="<?php echo $thumb; ?>" /></div>
					<div class="innovator-desc">
						<ul class="list-innovator-desc">
							<li><?= lang('label_innovator') ?> : <?= $content[0]['content']['innovator_name']; ?></li>
							<li><?= lang('label_email_address') ?> : <?= $content[0]['content']['email']; ?></li>
							<li><?= lang('label_identification_code_no') ?> : <?= $content[0]['content']['kod_no']; ?></li>
							<li><?= lang('label_address') ?> : <?= $content[0]['content']['address']; ?></li>
							<li><?= lang('label_postcode') ?> : <?= $content[0]['content']['postcode']; ?></li>
							<li><?= lang('label_city') ?> : <?= $content[0]['content']['city']; ?></li>
							<li><?= lang('label_state') ?> : <?= $content[0]['content']['state_name']; ?></li>
						</ul>
					</div>
				</div>
				<div class="innovation-desc grid_8">
					<?= $content[0]['content']['content'] ?>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
<?php }else{ ?>
	<div class="grid_12 alpha article">
		<div class="block blue">
			<div class="title title_article"><?= $content[0]['content']['title'] ?></div>
			<div class="block-content content-article">
				<?= $content[0]['content']['content'] ?>
			</div>
		</div>
	</div>
<?php } ?>
<?php //if(array_key_exists("articles", $content[0])){ 
		//foreach($content[0]['articles'] as $row){ ?>
			<!--<ul>
				<li><a href="<?php // base_url().'site/content/article/'.$row['id'] ?>"><?php // $row['title']; ?></a></li>
			</ul>-->
<?php 	//} } ?>

<script>
$(function(){
	/*slider in landing page*/
	$('.flexslider').flexslider({
		animation: "slide"
	});
});
</script>