<div class="block innovator green">
	<div class="title"><?=lang('title_edit_password')?></div>
	
	<div class="block-content">
		<?php if($flashdata != NULL){
			echo "<div class='form-info ".($flashdata['success'] ? 'success' : 'fail')."'>".$flashdata['msg']."</div>";
		} ?>
		<div class="detail-content grid_11 alpha">
			<form action="<?=base_url()."site/save_password"?>" method="POST" id="edit-password">
				<div class="label grid_3"><?=lang('label_old_password')?>* : </div>
				<div class="value grid_6"><input type="password" class="grid_4" name="old_password" /> </div>
				
				<div class="label grid_3"><?=lang('label_new_password')?>* : </div>
				<div class="value grid_6"><input type="password" class="grid_4" name="new_password" /> </div>
				
				<div class="label grid_3"><?=lang('label_retype')." ".lang('label_new_password')?>* : </div>
				<div class="value grid_6"><input type="password" class="grid_4" name="retype_password"/></div>
				
				<div class="grid_7 right">
					<a class="btn red grid_1 omega alpha" onclick="history.back();"><?=lang('button_cancel')?></a>
					<input class="btn" type="submit" value="<?=lang('button_save')?> >>" />
				</div>
			</form>
		</div>
	</div>
</div>