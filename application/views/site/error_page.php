<div class="block innovator">
	<div class="title">
		<div>Page not Found</div>
		<div class="clear"></div>
	</div>
	<div class="block-content">
		<div><?= $message; ?></div>
		<div><a href="<?= base_url(); ?>">Return to homepage</a></div>
	</div>
</div>