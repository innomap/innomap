<center>
	<div class="block dark-green wrap-forgot-pass">
		<div class="title">Forgot Password</div>
		<div class="block-content">
			<?php if($this->session->flashdata('forget_pswd_msg') == 'true') { 
				echo "<div class='form-info success'>New password has been sent to your email.</div>"; 
			}elseif($this->session->flashdata('forget_pswd_msg')){ 
				echo "<div class='form-info fail'>Email not found! </div>"; 
			} ?>
			<form method="post" action="<?=base_url()."site/send_new_password"?>">
				<label>Email</label>
				<input type="text" name="email" placeholder="Email"/><br/>
				<input type="submit" value="Send" />	
			</form>
		</div>
	</div>
</center>
<style>
	.wrap-forgot-pass {width:400px; height:300px !important;}
	.wrap-forgot-pass form{
		margin-top:30px;
	}
	.wrap-forgot-pass form input[type="text"]{
		background: white;
		padding:10px 15px;
		width:80%;
		margin-top: 10px;
	}

	.wrap-forgot-pass form input[type="submit"]{
		margin-top:15px;
		padding:10px 30px;
		text-align: center;
		background: white;
		border: 1px solid #888888;
		cursor: pointer;
	}

	.wrap-forgot-pass form input[type="submit"]:hover{
		background: #eeeeee;
	}

	.wrap-forgot-pass form label{
		text-align: left;
		font-size: 14pt;
	}
</style>