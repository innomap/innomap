<div class="left grid_4 alpha">
	<div class="block orange grid_4 alpha omega">
		<div class="title"><a href="<?= base_url().'site/content/detail/'.$contents[0]['homepage_content_id'] ?>"><?= (array_key_exists('title', $contents[0]['content']) ? $contents[0]['content']['title'] : ""); ?></a></div>
		<div class="block-content">
			<p><?= (array_key_exists('content', $contents[0]['content']) ? $contents[0]['content']['content'] : ""); ?></p>
			<ul class="yellow-list">
				<?php foreach($contents[0]['articles'] as $article){ ?>
					<li><a href="<?= base_url().'site/content/article/'.$article['id'] ?>"><?= $article['title']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="clear"></div>
	<div class="block dark-green grid_4 alpha omega">
		<div class="title"><a href="<?= base_url().'site/content/detail/'.$contents[1]['homepage_content_id'] ?>"><?= (array_key_exists('title', $contents[1]['content']) ? $contents[1]['content']['title'] : ""); ?></a></div>
		<div class="block-content">
			<p><?= (array_key_exists('content', $contents[1]['content']) ? $contents[1]['content']['content'] : "") ?></p>
			<ul class="yellow-list">
				<?php foreach($contents[1]['articles'] as $article){ ?>
					<li><a href="<?= base_url().'site/content/article/'.$article['id'] ?>"><?= $article['title']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="clear"></div>
	<div class="block purple grid_4 alpha omega">
		<div class="title"><a href="<?= base_url().'site/content/detail/'.$contents[2]['homepage_content_id'] ?>"><?= (array_key_exists('title', $contents[0]['content']) ? $contents[0]['content']['title'] : "") ?></a></div>
		<div class="block-content">
			<div class="location"><img src="<?= base_url()?>assets/img/location.png" /></div>
			<p><?= (array_key_exists('content', $contents[2]['content']) ? ellipsis($contents[2]['content']['content'],170) : ""); ?></p>
		</div>
	</div>
</div>
<div class="grid_8 omega right">
	<div class="grid_8 alpha omega slideshow">
		<div class="flexslider big-slider">
	         <ul class="slides">
	  	    	<li>
	  	    	    <img src="<?= base_url()?>assets/img/angelHook.jpg" />
	  	    	</li>
	  	    	<li>
	  	    	    <img src="<?= base_url()?>assets/img/heroesGame.jpg" />
	  	    	</li>
	  	    	<li>
	  	    	    <img src="<?= base_url()?>assets/img/Hydro.jpg" />
	  	    	</li>
	  	    	<li>
	  	    	    <img src="<?= base_url()?>assets/img/inkubatorTelur.jpg" />
	  	    	</li>
	  	    	<li>
	  	    	    <img src="<?= base_url()?>assets/img/kompangXray.jpg" />
	  	    	</li>
	  	    	<li>
	  	    	    <img src="<?= base_url()?>assets/img/miniDrum.jpg" />
	  	    	</li>
	  	    	<li>
	  	    	    <img src="<?= base_url()?>assets/img/nira.jpg" />
	  	    	</li>
	  	    	<li>
	  	    	    <img src="<?= base_url()?>assets/img/propellr.jpg" />
	  	    	</li>
	  	    	<li>
	  	    	    <img src="<?= base_url()?>assets/img/sabit.jpg" />
	  	    	</li>
	  	    	<li>
	  	    	    <img src="<?= base_url()?>assets/img/satay.jpg" />
	  	    	</li>
	  	    	<li>
	  	    	    <img src="<?= base_url()?>assets/img/sosChar.jpg" />
	  	    	</li>
	  	    	<li>
	  	    	    <img src="<?= base_url()?>assets/img/Taj3D.jpg" />
	  	    	</li>
	  	    	<li>
	  	    	    <img src="<?= base_url()?>assets/img/TuhaAu.jpg" />
	  	    	</li>
	  	    	<li>
	  	    	    <img src="<?= base_url()?>assets/img/wau.jpg" />
	  	    	</li>
	         </ul>
        </div>
	</div>
	<div class="clear"></div>
	<div class="child_left grid_4 alpha">
		<div class="block blue grid_4 alpha">
			<div class="title"><a href="<?= base_url().'site/content/detail/'.$contents[3]['homepage_content_id'] ?>"><?= (array_key_exists('title', $contents[3]['content']) ? $contents[3]['content']['title'] : ''); ?></a></div>
			<div class="block-content">
				<?= (array_key_exists('content', $contents[3]['content']) ? ellipsis($contents[3]['content']['content'],900) : ''); ?>
			</div>
		</div>
		<div class="block grid_4 omega">
			<div class="flexslider small-slider">
	         	<ul class="slides">
		           <li>
		  	    	    <img src="<?= base_url()?>assets/img/heroesGame.jpg" />
		  	    	</li>
		  	    	<li>
		  	    	    <img src="<?= base_url()?>assets/img/Hydro.jpg" />
		  	    	</li>
		  	    	<li>
		  	    	    <img src="<?= base_url()?>assets/img/inkubatorTelur.jpg" />
		  	    	</li>
		  	    	<li>
		  	    	    <img src="<?= base_url()?>assets/img/kompangXray.jpg" />
		  	    	</li>
		  	    	<li>
		  	    	    <img src="<?= base_url()?>assets/img/miniDrum.jpg" />
		  	    	</li>
		  	    	<li>
		  	    	    <img src="<?= base_url()?>assets/img/nira.jpg" />
		  	    	</li>
		  	    	<li>
		  	    	    <img src="<?= base_url()?>assets/img/propellr.jpg" />
		  	    	</li>
		  	    	<li>
		  	    	    <img src="<?= base_url()?>assets/img/sabit.jpg" />
		  	    	</li>
		  	    	<li>
		  	    	    <img src="<?= base_url()?>assets/img/wau.jpg" />
		  	    	</li>
		  	    	<li>
		  	    	    <img src="<?= base_url()?>assets/img/TuhaAu.jpg" />
		  	    	</li>	  	    	
		  	    	<li>
		  	    	    <img src="<?= base_url()?>assets/img/sosChar.jpg" />
		  	    	</li>
		  	    	<li>
		  	    	    <img src="<?= base_url()?>assets/img/Taj3D.jpg" />
		  	    	</li>
		  	    	 <li>
		  	    	    <img src="<?= base_url()?>assets/img/angelHook.jpg" />
		  	    	</li>
	         	</ul>
	        </div>
		</div>
	</div>
	<div class="child_right grid_4 omega">
		<div class="block light-purple grid_4 alpha omega">
			<div class="title"><a href="<?= base_url().'site/content/detail/'.$contents[4]['homepage_content_id'] ?>"><?= (array_key_exists('title', $contents[4]['content']) ? $contents[4]['content']['title'] : ''); ?></a></div>
			<div class="block-content">
				<?= (array_key_exists('content', $contents[4]['content']) ? ellipsis($contents[4]['content']['content'],350) : ''); ?>
			</div>
		</div>
		<div class="clear"></div>
		<div class="block green grid_4 alpha omega">
			<div class="title"><a href="<?= base_url().'site/content/detail/'.$contents[5]['homepage_content_id'] ?>"><?= (array_key_exists('title', $contents[5]['content']) ? $contents[5]['content']['title'] : ''); ?></a></div>
			<div class="block-content">
				<?= (array_key_exists('content', $contents[5]['content']) ? ellipsis($contents[5]['content']['content'],240) : ''); ?>
			</div>
		</div>
		<div class="clear"></div>
		<div class="block red grid_4 alpha omega">
			<div class="title"><a href="<?= base_url().'site/content/detail/'.$contents[6]['homepage_content_id'] ?>"><?= (array_key_exists('title', $contents[6]['content']) ? $contents[6]['content']['title'] : ''); ?></a></div>
			<div class="block-content">
				<?= (array_key_exists('content', $contents[6]['content']) ? ellipsis($contents[6]['content']['content'],350) : ''); ?>
			</div>
		</div>
	</div>
</div>

<script>
$(function(){
	/*slider in landing page*/
	$('.flexslider').flexslider({
		animation: "slide"
	});
});
</script>