<div class="content_1 grid_12 alpha omega" style="background-color:transparent;">
	<div class="grid_4 alpha" style="position:relative;">
		<div class="homepage_overlay">&nbsp;</div>
		<a href="<?= base_url().'site/top_innovation'; ?>"><img src="<?= base_url()?>assets/img/inovasi_teratas.jpg" class="top_50_img" /></a>
		<div class="clear"></div>
	</div>
	<div class="grid_8 omega">
		<div class="data grid_4 alpha <?= count($data_menus) > 0 ? 'homepage_submenu' : '';?>" style="background-color:#ea222d; position:relative;">
			<div class="homepage_overlay">&nbsp;</div>
			<img src="<?= base_url()?>assets/img/icon-data.png" />
			<div class="arrow <?= count($data_menus) > 0 ? 'arrow_down data_arrow' : '';?>">
				<p>DATA</p>
			</div>
			<?php if(count($data_menus) > 0 ) { ?>
				<div class="submenu_homepage data">
					<ul>
						<?php foreach ($data_menus as $data_menu) {	?>
							<a href="<?= base_url().$data_menu['url'];?>"><li><?= ($lang == 'melayu' ? $data_menu['name_in_melayu'] : $data_menu['name'])?></li></a>
						<?php } ?>
					</ul>
				</div>
			<?php } ?>
		</div>
		<div class="data grid_4 omega <?= count($search_menus) > 0 ? 'homepage_submenu' : '';?>" style="background-color:#4e59a8; position:relative">
			<div class="homepage_overlay">&nbsp;</div>
			<img src="<?= base_url()?>assets/img/icon-search.png" />
			<div class="arrow <?= count($search_menus) > 0 ? 'arrow_down search search_arrow' : '';?>">
				<p style="text-transform : uppercase;"><?= ($lang == 'melayu' ? lang('homepage_search') : lang('label_search'))?></p>
			</div>
			<?php if(count($search_menus) > 0 ) { ?>
				<div class="submenu_homepage search">
					<ul>
						<?php foreach ($search_menus as $search_menu) {	?>
							<a href="<?= base_url().$search_menu['url'];?>"><li><?= ($lang == 'melayu' ? $search_menu['name_in_melayu'] : $search_menu['name'])?></li></a>
						<?php } ?>
					</ul>
				</div>
			<?php } ?>
		</div>
		<div class="clear"></div>
		<br>
		<div class="data grid_8 alpha omega" style="background-color:#f16729; position:relative;">
			<div class="homepage_overlay">&nbsp;</div>
			<a href="<?= ($mng_dashboard_accessable ? base_url().'management_dashboard' : '#'); ?>"><img src="<?= base_url()?>assets/img/icon-dashboard.png" /></a>
			<div class="clear"></div>
			<p>DASHBOARD PENGURUSAN</p>
			<div class="clear"></div>
		</div>
	</div>
</div>
<div class="clear"></div>
<br>
<div class="content_2 grid_12 alpha omega">
	<div class="grid_8 alpha">
		<div class="data grid_8 alpha <?= count($report_menus) > 0 ? 'homepage_submenu' : '';?>" style="background-color:#20abe1; position:relative;">
			<div class="homepage_overlay">&nbsp;</div>
			<img src="<?= base_url()?>assets/img/icon-report.png" />
			<div class="arrow <?= count($report_menus) > 0 ? 'arrow_left report' : '';?>">
				<p style="text-transform:uppercase;"><?= ($lang == 'melayu' ? lang('homepage_report') : lang('homepage_report'))?></p>
			</div>
			<?php if(count($report_menus) > 0 ) { ?>
				<div class="submenu_homepage report">
					<ul>
						<?php foreach ($report_menus as $report_menu) {	?>
							<a href="<?= base_url().$report_menu['url'];?>"><li><?= ($lang == 'melayu' ? $report_menu['name_in_melayu'] : $report_menu['name'])?></li></a>
						<?php } ?>
					</ul>
				</div>
			<?php } ?>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="grid_4 omega">
		<div class="data grid_4 alpha" style="background-color:#8cc444; position:relative;">
			<div class="homepage_overlay">&nbsp;</div>
			<img src="<?= base_url()?>assets/img/icon-idec.png" />
			<p>IDEC</p>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div class="clear"></div>
<br>

<script type="text/javascript">
	$(document).ready(function(){
		$('.homepage_submenu').hover(function(){
			$('.homepage_overlay').show();
			$(this).find('.homepage_overlay').hide();
			$(this).find('.submenu_homepage').show();
			$(this).find('.arrow').addClass('active');
		});
		$('.homepage_submenu').mouseleave(function(){
			$('.homepage_overlay').hide();
			$(this).find('.submenu_homepage').hide();
			$(this).find('.arrow').removeClass('active');
		});
	});
</script>