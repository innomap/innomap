<div class="block innovator">
	<div class="title"><?=lang('title_management_dashboard')?></div>
	
	<div id="accordion" style="padding: 10px">
		<h3><?=($this->session->userdata('language') == 'english' ? 'Innovation by Year' : 'Inovasi berdasarkan Tahun')?></h3>
		<div>
			<iframe scrolling="no" width="914" height="560" src="<?=base_url()."reports/innovation_year"?>"></iframe>
		</div>
		<h3><?=($this->session->userdata('language') == 'english' ? 'Innovation by Category & Year' : 'Inovasi berdasarkan Kategori & Tahun')?></h3>
		<div>
			<iframe scrolling="no" width="914" height="740" src="<?=base_url()."reports/innovation_category"?>"></iframe>
		</div>
		<h3><?=($this->session->userdata('language') == 'english' ? 'Innovation by State & Year' : 'Inovasi berdasarkan Negeri & Tahun')?></h3>
		<div>
			<iframe scrolling="no" width="914" height="790" src="<?=base_url()."reports/innovation_state"?>"></iframe>
		</div>
		<h3><?=($this->session->userdata('language') == 'english' ? 'Locator' : 'Pencarian Lokasi')?></h3>
		<div style="padding-top:10px">
			<iframe scrolling="no" width="914" height="800" src="<?=base_url()."locators"?>"></iframe>
		</div>
	</div>
</div>

<script>
$(function() {
	//accordion
	$("#accordion").accordion({clearStyle: true, autoHeight: false, collapsible: true});
	
	//hide element
	$("#accordion div iframe").load(function(){ 
		$(this).contents().find('.header').hide(); 
		$(this).contents().find('.title').hide(); 
		$(this).contents().find('#container').css({'margin-left':'0','margin-top':'-15px'}); 
		$(this).contents().find('#container .content').css({'width':'930px','margin-left':'-10px'}); 
	});	
});
</script>

<style>
	.ui-accordion .ui-accordion-content { padding: 0; height:auto !important; }
	.ui-accordion-header [class*="ui-icon-triangle-1"]{ display:inline-block; }
	.ui-state-hover, .ui-state-focus{background: #f8f7f6; color: #654b24; }
	.ui-state-focus .ui-icon, .ui-state-hover .ui-icon { background-image: url(assets/css/pepper-grinder/images/ui-icons_b83400_256x240.png);
</style>