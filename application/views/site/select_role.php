<div id="dialog-select-role-form" title="<?= lang('select_role')?>">
	<center>
		<div class="select_role">
			<form action="<?= base_url()."site/select_role" ?>" method="post">
				<br/>
				<label><?= lang('select_role')?></label>
				<select name="role">
					<?php foreach ($roles as $key => $role) { ?>
						<option value="<?= $role['role_id']?>"><?= $role['name']?></option>
					<?php } ?>
				</select>
				<br /><br />
				<input type="submit" name="submit" value="<?= lang('value_select')?>" />
			</form>
		</div>
	</center>
</div>
<script type="text/javascript">
	$(function(){
		$( "#dialog-select-role-form" ).dialog({
		      autoOpen: true,
		      height: 180,
		      width: 350,
		      modal: true
		});
	});
</script>
<style type="text/css">
	.ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-icon-only.ui-dialog-titlebar-close{
		display: none;
	}
</style>