<div class="left grid_12 alpha">
	<ul class="block top_innovasi grid_12 alpha omega">
		<li class="grid_4 alpha"><img src="<?= base_url(); ?>assets/img/50_inno_teratas.png" class="grid_4 alpha omega" /></li>
		<?php foreach ($innovations as $key => $innovation) { ?>
			<li class="img-bg grid_4 alpha" style="<?= (count($innovation['picture']) > 0 ? "background: url('".base_url().PATH_TO_INNOVATION_PICTURE.$innovation['picture'][0]['picture']."')" : "")?>">
				<a href="<?= base_url().'site/top_innovation/'.$innovation['innovation_id']; ?>">
					<div class="wrap-inno">
						<div class="inno_no"><?= $key+1 ?></div>
						<div class="inno_name"><?= $innovation['name'] ?></div> <div class="clear"></div>
					</div>
				</a>
			</li>
		<?php } ?>
	</ul>
	<div class="clear"></div>
</div>	
<img class="progress-spin push_5" style="width:4%" src="<?=base_url()."assets/img/icons/progress-spin.gif"?>" />
<input type="hidden" id="inno-num" value="<?=count($innovations)?>">