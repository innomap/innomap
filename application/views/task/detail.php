<div class="block innovator green">
	<div class="title"><?= lang('title_detail_task') ?></div>

	<div class="block-content">
		<div class="grid_11" style="padding-right:26px">

			<table border="0">
				<tr>
					<td><?= lang('label_task') ?></td>
					<td>:</td>
					<td><?= $task['title']; ?></td>
				</tr>
				<tr>
					<td><?= lang('label_expert') ?></td>
					<td>:</td>
					<td>
						<?php foreach($task['expert'] as $key => $value){ ?>
							<span><?= ($key+1).". ".$value['expert_name']; ?></span><br/>
						<?php } ?>
					</td>
				</tr>
			</table>
			<br />

			<div class="label"><b><?= lang('label_innovation_list') ?> : </b>
		
			<table cellpadding="0" cellspacing="0" border="0" class="display" id="portfolio_table" width="100%">
				<thead>
					<tr>
						<th><?= lang('label_no'); ?></th>
						<th><?= lang('label_innovation_name') ?></th>
						<th><?= lang('label_innovator') ?></th>
						<th><?= lang('label_score_average') ?></th>
						<th><?= lang('label_submission_date') ?></th>
						<th><?= lang('label_action') ?></th>
					</tr>
				</thead>
				<tbody>
					<?php $no=1;foreach($task['innovation'] as $row){ ?>
						<tr>
							<td><?= $no; ?></td>
							<td><?= $row['innovation_name']; ?></td>
							<td><?= $row['innovator']; ?></td>
							<td><?= $row['score_average']; ?></td>
							<td><?= $row['submission_date']; ?></td>
							<td><a href="#" class="view-detail-inno" inno_id="<?= $row['innovation_id'] ?>"><div class="view-icon" title="<?= lang('button_view_detail') ?>"></div></a>
								<a href="#" class="view-note view-notes" inno_id="<?= $row['innovation_id']; ?>" title="<?= lang('button_view_notes') ?>"></a> 
								<?php if($row['evaluation_result'] > 0){ ?>
									<a href="<?= base_url()."tasks/evaluation_result/".$row['innovation_id'] ?>" class="view-result-evaluation" inno_id="<?= $row['innovation_id']; ?>" title="<?= lang('button_view_evaluation') ?>"></a>
								<?php } ?>
							</td>
						</tr>
					<?php $no++;} ?>
				</tbody>
			</table>

			<div class="grid_11 margin-top">
				<a href="<?= base_url().'tasks'; ?>" class="btn red grid_1 omega alpha"><?= lang('button_back') ?></a>
			</div>
		</div>
	</div>
</div>