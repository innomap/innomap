<div class="block innovator green">
	<div class="title"><?= lang('title_evaluation_result'); ?></div>

	<div class="block-content">
		<div class="grid_11" style="padding-right:26px">
			<table id="portfolio_table">
				<thead>
					<th><?= lang('label_no'); ?></th>
					<th><?= lang('label_innovation_name') ?></th>
					<th><?= lang('label_expert') ?></th>
					<th><?= lang('label_action') ?></th>
				</thead>
				<tbody>
					<?php $i=1; foreach($list as $row){ ?>
						<tr>
							<td><?= $i; ?></td>
							<td><?= $row['innovation_name']; ?></td>
							<td><?= $row['expert_name']; ?></td>
							<td><a href="#" class="btn-evaluation-detail view-result-evaluation" eval_id="<?= $row['evaluation_id'] ?>" title="<?= lang('button_view_evaluation'); ?>"></a></td>
						</tr>
					<?php $i++;} ?>
				</tbody>
			</table>
			<div class="grid_10 margin-top">
				<a onclick="history.back();" class="btn red grid_1 omega alpha"><?= lang('button_back') ?></a>
			</div>
		</div>
	</div>
</div>