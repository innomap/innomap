<div class="block innovator green">
	<div class="title"><?= lang('title_task_form') ?></div>
	<div class="block-content">
		<div class="detail-content grid_11 alpha">
			<form action="<?= base_url().'tasks/save' ?>" method="post" id="form-task">
				<div class="label grid_3"><?= lang('label_task') ?></div>
				<div class="value grid_6"><input type="text" name="title" value="" class="grid_5 alpha"/></div>

				<div class="label grid_3"><?= lang('label_expert') ?></div>
				<div class="value grid_6 expert-list"><input type="text" name="expert_id" id="expert-filter"/></div>

				<div class="label grid_3"><?= lang('label_innovation_list') ?></div>
				<table id="portfolio_table">
					<thead>
						<tr>
							<th></th>
							<th><?= lang('label_no') ?></th>
							<th><?= lang('label_innovation_name') ?></th>
							<th><?= lang('label_innovator') ?></th>
							<th><?= lang('label_submission_date') ?></th>
							<th><?= lang('label_action') ?></th>
						</tr>
					</thead>
					<tbody>
						<?php $i=1; foreach($innovations as $innovation){ ?>
							<tr>
								<td><input type="checkbox" name="innovation_id[]" value="<?= $innovation['innovation_id']; ?>"></td>
								<td><?= $i; ?></td>
								<td><?= $innovation['name']; ?></td>
								<td><?= $innovation['innovator']; ?></td>
								<td><?= $innovation['submission_date']; ?></td>
								<td><a href="#" class="view-detail-inno" inno_id="<?= $innovation['innovation_id'] ?>"><div class="view-icon" title="<?= lang('button_view_detail') ?>"></div></a> | <a href="#" class="view-notes" inno_id="<?= $innovation['innovation_id']; ?>"><?= lang('button_view_notes') ?></a></td>
							</tr>
						<?php $i++;} ?>
					</tbody>
				</table>
				<div class="grid_6 margin-top">
					<input type="hidden" name="innovation_id_arr" id="innovation_id_arr">
					<input type="submit" name="save" class="btn submit-task" value="<?= lang('button_save') ?>">
					<a href="<?= base_url().'tasks' ?>" class="btn red grid_1 omega alpha"><?= lang('button_back') ?></a>
				</div>
			</form>
		</div>
	</div>
</div>

<script>
$(function(){
	$('#expert-filter').magicSuggest({
		allowFreeEntries: false,
		cls:"",
	    width: 224,
	    selectionPosition: 'bottom',
	    selectionStacked: true,
	    displayField: 'name',
	    data: <?= json_encode($experts); ?>
    });
   // $('div.ms-sel-ctn.ms-stacked').addClass('push_3 grid_4');
   var table = $('#portfolio_table').dataTable();
   $('.submit-task').click(function(){
   		var data = table.$('input[type=checkbox]:checked').serializeArray();
   		$('#innovation_id_arr').val(JSON.stringify(data));
   });
});
</script>