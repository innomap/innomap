<div class="block tasklist">
	<div class="title">
		<div style="float:left"><?=lang('title_task_list')?></div>
			<?php if($innovations > 0){ ?>
				<a href="<?= base_url().'tasks/add' ?>">
					<div class="add_task_button">
						<div><?=lang('button_new_task')?></div>
					</div>
				</a>
			<?php } ?>
		<div class="clear"></div>
	</div>
	
	<div class="block-content">
		<div class="clear"></div>
		<?php if($flashdata != NULL){
			echo "<br/><br/><div class='form-info ".($flashdata['success'] ? 'success' : 'fail')."'>".$flashdata['msg']."</div>";
		} ?>
		<table cellpadding="0" cellspacing="0" border="0" class="display" id="tasklist_table" width="100%">
			<thead>
				<tr>
					<th><?= lang('label_no') ?></th>
					<th><?= lang('label_task') ?></th>
					<th><?= lang('label_expert') ?></th>
					<th class="last"><?= lang('label_action') ?></th>
				</tr>
			</thead>
			<tbody>
				<?php $i=1;foreach($tasks as $task){ ?>
					<tr>
						<td><?= $i; ?></td>
						<td><?= $task['title']; ?></td>
						<td><?php foreach($task['expert'] as $row){
							echo "-".$row['expert_name']."<br/>";
						} ?></td>
						<td class="last">
							<a href="<?= base_url().'tasks/view/'.$task['expert_task_id']; ?>">
								<div class="view-icon" title="<?= lang('button_view_detail') ?>"></div>
							</a>
							<a href="<?=base_url()."tasks/delete/".$task['expert_task_id']?>" onclick="return confirm_delete()" title="<?=lang('link_delete')?>" class="delete"></a>
						</td>
					</tr>
				<?php $i++;} ?>
			</tbody>
		</table>
	</div>
</div>
