$(document).ready(function() {
	$("#form-user").validate({
        rules: {
			'username' : {
				required: true,
				noSpace:true
			},
			'email' : {
				required: true,
				email: true
			},
			'password' : {
				required: true,
			},
			'retype_password' : {
				required: true,
				equalTo:'#password'
			},
			'role' : {
				required: true
			},
			'status' : {
				required: true
			},
		}
	}); 

	//get tiny mce content and add the content to input type hidden, for use validation jquery
	$('#submit-article').click(function() {
	/* hide english lang
		var content = tinyMCE.get('content').getContent();
		var r = $('#hidden_content').val(content); */
		var content_melayu = tinyMCE.get('content-in-melayu').getContent();
		var r = $('#hidden_content_in_melayu').val(content_melayu);

	});

	$("#form-article").validate({
        rules: {
			'title' : {
				required: true
			},
			'title_in_melayu' : {
				required: true
			},
			'hidden_content' : {
				required: true
			},
			'hidden_content_in_melayu' : {
				required: true
			},
			'category' : {
				required: true
			},
			'status' : {
				required: true
			},
		}
	});

	/* FORM MENU ***********************************************************************************************/	
	$("#form-menu").validate({
        rules: {
			'name' : {
				required: true
			},
			'melayu_name':{
				required: true
			},
			'module_url[]':{
				required: true
			},
			'module_name[]':{
				required: true
			}
		}
	});
	
	/* FORM ROLE ***********************************************************************************************/	
	$("#form-role").validate({
        rules: {
			'name' : {
				required: true
			},
			'name_in_melayu':{
				required: true
			},
		}
	});

	//Form Article Category
	$('#submit-article-category').click(function() {
		/* hide english lang
		var description = tinyMCE.get('description').getContent();
		var r = $('#hidden_description').val(description);*/
		var description_melayu = tinyMCE.get('description-in-melayu').getContent();
		var r = $('#hidden_description_in_melayu').val(description_melayu);
	});

	$("#form-article-category").validate({
        rules: {
			'name' : {
				required: true
			},
			'name_in_melayu' : {
				required: true
			},
			'hidden_description' : {
				required: true
			},
			'hidden_description_in_melayu' : {
				required: true
			},
			'status' : {
				required: true
			},
			'position' : {
				required: true
			},
		}
	});

	$("#form-expert").validate({
        rules: {
			'username' : {
				required: true,
				noSpace: true
			},
			'email' : {
				required: true,
				email: true
			},
			'password' : {
				required: true,
			},
			'retype_password' : {
				required: true,
				equalTo:'#password'
			},
			'status' : {
				required: true
			},
			'expertise':{
				required: true
			},
			'name':{
				required:true
			},
			'kad_no1':{
				required:true
			},
			'kad_no2':{
				required:true
			},
			'kad_no3':{
				required:true
			}
		},
	}); 

	$("input[name='username']").blur(function(){
		var thisInput = $(this);
		var username = $(this).val();
		var readonly = $(this).attr('readonly');
		var user_id = $('input[name=user_id]').val();
		if (typeof readonly == typeof undefined || readonly == false) {
		    $.post(base_url+"users/exist_username",{username : username,user_id:user_id}, function(data) {
			 	if(data == true){
			 		thisInput.closest('td').find('#labelUsernameExist').show();
			 		$("input[type='submit']").attr('disabled','disabled');
			 	}else{
			 		thisInput.closest('td').find('#labelUsernameExist').hide();
			 		$("input[type='submit']").removeAttr('disabled');
			 	}
			},'json');
		}
		
	});
	
	$("#form-university").validate({
        rules: {
			'university_name' : {
				required: true,
			},
			'address' : {
				required: true,
			},
			'contact_no' : {
				required: true,
			}
		}
	});

	$("#form-commercial").validate({
        rules: {
			'name' : {
				required : true
			},
			'type' : {
				required : true
			},
			'state' : {
				required : true
			},
			'address' : {
				required : true
			},
			'telp' : {
				required : true
			}
		}
	});

	$.validator.addMethod("noSpace", function(value, element) { 
	  return value.indexOf(" ") < 0; 
	}, "This Username cannot contain spaces");	

	$("#form-portfolio-criteria").validate({
        rules: {
			'name_in_melayu' : {
				required: true,
			},
			'description_in_melayu' : {
				required: true
			},
		}
	});

	$("#form-project").validate({
        rules: {
			'title' : {
				required: true,
			},
		}
	});

	$("#form-social-media").validate({
        rules: {
			'name' : {
				required: true
			},
			'url' : {
				required: true,
				url: true
			}
		}
	}); 
}); 