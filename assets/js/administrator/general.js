var DATATABLES_STORAGE_NAME = 'Datatables_innomap_q2dlUiIYB';

$(document).ready(function(){
	//set menu whether on mobile or not
	set_menu();
	$(window).on('resize', function(){
		set_menu();
	});
	
	/*geo location*/
	$(".address-geo").blur(function(){
		var address = ""; 
		$(".address-geo").each(function(){
			address += " "+$(this).val();
		});
		$.post(base_url+"universities/get_geo_location", { 'address': address }, function(data) {
			if(data.status == false){
				$("#geo-location").val("");
				$("#geo-location").attr("placeholder",data.message);
			}else{
				$("#geo-location").val(data.latitude+","+data.longitude);
			}
		},"json");
	});

	$('.reset_data_tables').on('click',function(e){
		reset_dt_view();
	});
});

function initConfirmDelete() {
	if(confirm('Are you sure you want to delete?') == false) {
		return false;
	}
}

/*set menu based on window size*/
function set_menu(){
	if ($(window).width() < 761) {
		$("#menu").hide();
		$('.show-menu').unbind('click').click(function(e){
			e.preventDefault();
			$("#menu").toggle("slide", { direction: "up" }, 500);
		});
	}else {
		$("#menu").show();
	}
}

function reset_dt_view() {
	localStorage.removeItem(DATATABLES_STORAGE_NAME);
}