$(document).ready(function(){
	initShowContentTypeLink();
	initShowContentList();
	initCheckSelected();
	initDetailContent();
});

function initShowContentTypeLink(){
	$('.opt-type').change(function(e){
		e.preventDefault();
		$(this).attr('checked','checked');
		initCheckSelected();
	});
}

function initCheckSelected(){
	var selected = $('.opt-type:checked').val();
		
	if(selected == 1){
		$('.open-article-list').show();
		$('.open-category-list').hide();
		$('.open-innovation-list').hide();
	}else if(selected == 2){
		$('.open-article-list').hide();
		$('.open-category-list').show();
		$('.open-innovation-list').hide();
	}else if(selected == 3){
		$('.open-article-list').hide();
		$('.open-category-list').hide();
		$('.open-innovation-list').show();
	}
}

function initShowContentList(){
	$('.open-article-list, .open-category-list, .open-innovation-list').click(function(e){
		e.preventDefault();
		var obj = $(this).attr('ctype');
		var data_post = {obj:obj};

		$.ajax({
			url			: base_url+'homepage_contents/get_content_list/',
			data 		: data_post,
			type		: 'POST',
			dataType	: 'json',
			success		: function (data) {
				//$('#message-dialog').append(data.message);
				var html_content = '';
				for(var i =0; i<data.length;i++){
					html_content += "<input type='radio' name='popup-content-id' class='popup-content-id' url='"+obj+"/"+data[i].id+"' value='"+data[i].id+"' ctitle='"+data[i].title+"'><a href='#' url='"+obj+"/"+data[i].id+"' class='detail-content'> "+data[i].title+"</a><br/>";
				}
				$('#content-list-dialog').html(html_content);
				$('#content-list-dialog').dialog('open');
			}
		});
	});

	$('#content-list-dialog').dialog({
		autoOpen: false,
		title: "Content List",
		resizable: false,
		modal: true,
		buttons: {
			"Save": function() {
				var id = $(".popup-content-id:checked").val();
				var title = $(".popup-content-id:checked").attr('ctitle');
				var url = $('.popup-content-id:checked').attr('url');
				var selected_content = "<label>Content: </label><a href='#' url='"+url+"' class='detail-content'>"+title+"</a>";

				$('#content-id').val(id);
				$('#selected-content').html(selected_content);
				$( this ).dialog( "close" );
			},
			Cancel: function() {
			  $( this ).dialog( "close" );
			}
		}
	});
}

function initDetailContent(){
	$('#content-list-dialog, #selected-content').on('click','.detail-content',function(e){
		e.preventDefault();
	
		var url = $(this).attr('url');
		$.ajax({
			url			: base_url+'homepage_contents/get_content_detail/'+url,
			dataType	: 'json',
			success		: function (data) {
				var html_content = '';
				html_content += '<label>Title: </label><div>'+data.title+'</div><br/>';
				html_content += '<label>Content: </label><div>'+data.content+'</div><br/>';
				$('#content-detail-dialog').html(html_content);
				$('#content-detail-dialog').dialog('open');
			}
		});
	});	

	$('#content-detail-dialog').dialog({
		width: "600px",
		autoOpen: false,
		title: "Content Detail",
		resizable: false,
		modal: true,
		buttons: {
			OK: function() {
			  $( this ).dialog( "close" );
			}
		}
	});
}