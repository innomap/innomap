$(document).ready(function(){
	initSetFeaturedInnovation();
	initSetTopInnovation();	
});

function initSetFeaturedInnovation(){
	$("#innovation-list").on('click','.set-featured', function(){
		var content = "<img src='"+front_url+"assets/img/layout_preview.jpg' width='200px'><br/>";
		content += "<label>Select Position: </label>";
		content += "<select id='frm-position'><option value='5'>5</option><option value='6'>6</option><option value='7'>7</option></select>";
		content += "<input type='hidden' id='frm-hdn-content-id' value="+$(this).attr('content-id')+">";

		$('#admin-dialog').html(content);
		$('#admin-dialog').dialog('open');
	});

	 $( "#admin-dialog" ).dialog({
	      width:"500px",
	      autoOpen: false,
	      title: "Detail",
	      resizable: false,
	      modal: true,
	      buttons: {
	        OK: function() {
	        	var data_post = {homepage_content_id : $('#frm-position').val() , type : 3, content_id : $('#frm-hdn-content-id').val()};
	        	$.ajax({
			        url     : base_url+'homepage_contents/save/',
			        data    : data_post,
			        type    : 'POST',
			        success   : function (data) {
			            location.reload(true);
			        }
			    }); 
	        }
	      }
    });
}

function initSetTopInnovation(){
	$("#innovation-list").on('click','.set-top-innovation', function(){
		var innovation_id = parseInt($(this).attr('content-id'));
		var top_innovation = parseInt($(this).attr('top-innovation'));
		var count_top = parseInt($(this).attr('count-top'));
		if(count_top < 50 || top_innovation == 0){
			$.ajax({
		        url     : base_url+'innovations/set_top_innovation',
		        data    : {innovation_id : innovation_id, top_innovation : top_innovation},
		        type    : 'POST',
		        success   : function (data) {
		            location.reload(true);
		        }
		    }); 
		}else{
			alert('Top innovations have been 50');
		}
	});
}