$(document).ready(function(){
	show_module();
	
	//fill 
	$("input[name=url]").keyup(function(){
		show_module();
	});
	
	//add module
	$("#add-module").click(function(e){
		e.preventDefault();
		$("#added-modules").append('<div class="each-module"><input type="text" name="module_name[]" /> | url : '+front_url+'<input type="text" name="module_url[]" /><a href="#" class="remove-module delete" title="Remove Menu Module" onclick="return initConfirmDelete()"></a></div>');
	});
	
	//remove module
	$("body").on('click', '.remove-module', function(e){
		e.preventDefault();
		var $clicker  = $(this);
		var module_id = $(this).attr('module_id');
		
		//if module id already in db, ajax to delete from db
		if (typeof module_id !== 'undefined' && module_id !== false) {
			$.get(base_url+"menus/delete/module/"+module_id, function(res){
				if(res == 'success'){
					$clicker.parent('.each-module').remove();
				}
			});
		}else{
			$clicker.parent('.each-module').remove();
		}
	});
});

//show module management
function show_module(){
	if($("input[name=url]").val() == ""){
		$("#menu-module").hide();
	}else{
		$("#menu-module").show();
		$("input[name='module_url[]']").eq(0).val($("input[name=url]").val()+"/view");
	}
}