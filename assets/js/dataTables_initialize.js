var MALAY_LANG = {
    "sEmptyTable":      "Tiada data",
    "sInfo":            "Paparan dari _START_ hingga _END_ dari _TOTAL_ rekod",
    "sInfoEmpty":       "Paparan 0 hingga 0 dari 0 rekod",
    "sInfoFiltered":    "(Ditapis dari jumlah _MAX_ rekod)",
    "sInfoPostFix":     "",
    "sInfoThousands":   ",",
    "sLengthMenu":      "Papar _MENU_ rekod",
    "sLoadingRecords":  "Diproses...",
    "sProcessing":      "Sedang diproses...",
    "sSearch":          "Carian:",
   "sZeroRecords":      "Tiada padanan rekod yang dijumpai.",
   "oPaginate": {
       "sFirst":        "Pertama",
       "sPrevious":     "Sebelum",
       "sNext":         "Kemudian",
       "sLast":         "Akhir"
   },
   "oAria": {
       "sSortAscending":  ": diaktifkan kepada susunan lajur menaik",
       "sSortDescending": ": diaktifkan kepada susunan lajur menurun"
   }
};
$(document).ready(function(){
	$('#innovator_table').dataTable({
		"sPaginationType": "full_numbers",
		"iDisplayLength": 20,
		"bStateSave": true,
		"fnStateSave": function(oSettings, oData) { save_dt_view(oSettings, oData); },
      	"fnStateLoad": function(oSettings) { return load_dt_view(oSettings); },
      	"oLanguage": MALAY_LANG
	});

	$('#tasklist_table').dataTable({
		"sPaginationType": "full_numbers",
		"iDisplayLength": 20,
		"bStateSave": true,
		"fnStateSave": function(oSettings, oData) { save_dt_view(oSettings, oData); },
      	"fnStateLoad": function(oSettings) { return load_dt_view(oSettings); },
      	"oLanguage": MALAY_LANG
	});

	$('#portfolio_table').dataTable({
		"sPaginationType": "full_numbers",
		"iDisplayLength": 20,
		"bStateSave": true,
		"fnStateSave": function(oSettings, oData) { save_dt_view(oSettings, oData); },
      	"fnStateLoad": function(oSettings) { return load_dt_view(oSettings); },
      	"oLanguage": MALAY_LANG
	});

	$('#evaluation_table').dataTable({
		"sPaginationType": "full_numbers",
		"iDisplayLength": 20,
		"oLanguage": MALAY_LANG
	});

	$('#user_table').dataTable({
		"sPaginationType": "full_numbers",
		"iDisplayLength": 20,
		"bStateSave": true,
		"fnStateSave": function(oSettings, oData) { save_dt_view(oSettings, oData); },
      	"fnStateLoad": function(oSettings) { return load_dt_view(oSettings); },
      	"oLanguage": MALAY_LANG
	});

	$('#menu_table').dataTable({
		"sPaginationType": "full_numbers",
		"iDisplayLength": 20,
		"aaSorting": [],
		"bStateSave": true,
		"fnStateSave": function(oSettings, oData) { save_dt_view(oSettings, oData); },
      	"fnStateLoad": function(oSettings) { return load_dt_view(oSettings); },
      	"oLanguage": MALAY_LANG
	});

	$('#article_table').dataTable({
		"sPaginationType": "full_numbers",
		"iDisplayLength": 20,
		"bStateSave": true,
		"fnStateSave": function(oSettings, oData) { save_dt_view(oSettings, oData); },
      	"fnStateLoad": function(oSettings) { return load_dt_view(oSettings); },
      	"oLanguage": MALAY_LANG
	});

	$('#homepage_content_table').dataTable({
		"sPaginationType": "full_numbers",
		"iDisplayLength": 20,
		"bStateSave": true,
		"fnStateSave": function(oSettings, oData) { save_dt_view(oSettings, oData); },
      	"fnStateLoad": function(oSettings) { return load_dt_view(oSettings); },
      	"oLanguage": MALAY_LANG
	});	

	$('#innovation-list').dataTable({
		"sPaginationType": "full_numbers",
		"iDisplayLength": 20,
		"bStateSave": true,
		"fnStateSave": function(oSettings, oData) { save_dt_view_innovation(oSettings, oData); },
      	"fnStateLoad": function(oSettings) { return load_dt_view_innovation(oSettings); },
      	"oLanguage": MALAY_LANG
	});	

	$('#experts_table').dataTable({
		"sPaginationType": "full_numbers",
		"iDisplayLength": 20,
		"bStateSave": true,
		"fnStateSave": function(oSettings, oData) { save_dt_view(oSettings, oData); },
      	"fnStateLoad": function(oSettings) { return load_dt_view(oSettings); },
      	"oLanguage": MALAY_LANG
	});	
});

function save_dt_view(oSettings, oData) {
	localStorage.setItem( DATATABLES_STORAGE_NAME, JSON.stringify(oData) );
}
function load_dt_view(oSettings) {
	return JSON.parse( localStorage.getItem(DATATABLES_STORAGE_NAME) );
}
function save_dt_view_innovation(oSettings, oData) {
	localStorage.setItem( DATATABLES_INNOVATION_STORAGE_NAME, JSON.stringify(oData) );
}
function load_dt_view_innovation(oSettings) {
	return JSON.parse( localStorage.getItem(DATATABLES_INNOVATION_STORAGE_NAME) );
}
