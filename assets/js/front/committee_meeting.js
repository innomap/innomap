$(document).ready(function(){
	initAddAttachment();
	initDeleteAttachment();
	//initAcInnovation();
	initPreSubmitForm();
	initInnovationDetail();
});

function initAddAttachment(){
	$('.add-attachment').on('click', function(e){
		e.preventDefault();

		var no = $('.input-attachment-wrap').find('input[type=file]').length;
		var content = '<div><input type="file" class="margin-bottom" name="attachment_'+no+'"></div>';

		$('.input-attachment-wrap').append(content);
		
	});
}

function initDeleteAttachment(){
	$('.field-attachment').on('click','.delete-attachment',function(e){
		e.preventDefault();
		
		$('.field-attachment').append('<input type="hidden" name="deleted_attachment[]" value="'+$(this).data('id')+'">');
		$(this).parent().remove();
	});
}

function initPreSubmitForm(){
	$('#submit-cm').on('click',function(){
		var message = $('textarea.mention').mentionsInput('val');
			message = message.trim();

		$('#mention-val').val(message);
	});
}

function initInnovationDetail(){
	//popup innovation detail
	$('.innovation-name').click(function(e){			
		e.preventDefault();
		var id = $(this).data('id');
		$.get(base_url+"portfolios/view_innovation/"+id, function(response){
			$('#detail-innovation').html(response);
		});

		$("#detail-innovation").dialog({
			autoOpen	: true,
			resizable	: false,
			width		: '700',
			height		: 'auto',
			modal		: true,
			position	: 'center',
			buttons: {
				"OK" : function(){
					$(this).dialog('close');
				}
			}
		});
	});
}

function initAcInnovation(){
	$("#ac-innovation").mention({
		delimiter: '#',
		queryBy: ['name', 'username'],
		users: [{
			name: 'Lindsay Made',
			username: 'LindsayM'
		}, {
			name: 'Rob Dyrdek',
			username: 'robdyrdek'
		}, {
			name: 'Rick Bahner',
			username: 'RickyBahner'
		}, {
			name: 'Jacob Kelley',
			username: 'jakiestfu'
		}, {
			name: 'John Doe',
			username: 'HackMurphy'
		}, {
			name: 'Charlie Edmiston',
			username: 'charlie'
		}, {
			name: 'Andrea Montoya',
			username: 'andream'
		}, {
			name: 'Jenna Talbert',
			username: 'calisunshine'
		}, {
			name: 'Street League',
			username: 'streetleague'
		}, {
			name: 'Loud Mouth Burrito',
			username: 'Loudmouthfoods'
		}]
	});
}