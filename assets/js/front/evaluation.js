var BALANCE_SCORE = 100;

$(document).ready(function(){
	assignValueToCategoryTotal();
	initEvaluationDetail();
});

function check_value(input){
	if(isNaN(parseInt(input))){
		return 0;
	}else{
		return parseInt(input);
	}
}

function assignValueToCategoryTotal(){
	$('#ef-creativity-ori, #ef-creativity-adap').keyup(function(){
		var creativity_ori = check_value($('#ef-creativity-ori').val()),
			creativity_adap = check_value($('#ef-creativity-adap').val()),
			creativity = creativity_ori + creativity_adap;
		$('#ef-total-creativity').val(creativity);
		calculateTotal();
	});

	$('#ef-implement-full,#ef-implement-trial,#ef-replicability-free, #ef-replicability-not-free').keyup(function(){
		var implement_full = check_value($('#ef-implement-full').val()),
			implement_trial = check_value($('#ef-implement-trial').val()),
			replicability_free = check_value($('#ef-replicability-free').val()),
			replicability_not_free = check_value($('#ef-replicability-not-free').val()),
			total =  implement_full + implement_trial + replicability_free + replicability_not_free;
		$('#ef-total-output').val(total);
		calculateTotal();
	});

	$('#ef-efficiency-time,#ef-efficiency-costs,#ef-efficiency-productivity,#ef-efficiency-design, #ef-significance-global,#ef-significance-national,#ef-significance-local').keyup(function(){
		var efficiency_time = check_value($('#ef-efficiency-time').val()),
			efficiency_costs = check_value($('#ef-efficiency-costs').val()),
			efficiency_productivity = check_value($('#ef-efficiency-productivity').val()),
			efficiency_design = check_value($('#ef-efficiency-design').val()),
			significance_global = check_value($('#ef-significance-global').val()),
			significance_national = check_value($('#ef-significance-national').val()),
			significance_local = check_value($('#ef-significance-local').val());
		var total =  efficiency_time + efficiency_costs  + efficiency_productivity  + efficiency_design  + significance_global  + significance_national  + significance_local;
		$('#ef-total-outcome').val(total);
		calculateTotal();
	});

	$('#ef-commitment-finance,#ef-commitment-hr,#ef-commitment-incentive,#ef-commitment-reward,#ef-commitment-equipment').keyup(function(){
		var commitment_finance = check_value($('#ef-commitment-finance').val()),
			commitment_hr = check_value($('#ef-commitment-hr').val()),
			commitment_incentive = check_value($('#ef-commitment-incentive').val()),
			commitment_reward = check_value($('#ef-commitment-reward').val()),
			commitment_equipment = check_value($('#ef-commitment-equipment').val()),
			total = commitment_finance + commitment_hr + commitment_incentive + commitment_reward + commitment_equipment;
		$('#total-commitment').val(total);
		calculateTotal();
	});

	$('#ef-relevancy, #ef-effectiveness, #ef-quality, #ef-potential').keyup(function(){
		$('input[for='+$(this).attr('id')+']').val($(this).val());
		calculateTotal();
	});
}

function calculateTotal(){
	var creativity = ((isNaN(parseInt($('#ef-total-creativity').val()))) ? 0 : parseInt($('#ef-total-creativity').val()));
	var output = ((isNaN(parseInt($('#ef-total-output').val()))) ? 0 : parseInt($('#ef-total-output').val()));
	var outcome = ((isNaN(parseInt($('#ef-total-outcome').val()))) ? 0 : parseInt($('#ef-total-outcome').val()));
	var commitment = ((isNaN(parseInt($('#total-commitment').val()))) ? 0 : parseInt($('#total-commitment').val()));
	var relevancy = ((isNaN(parseInt($('#ef-relevancy').val()))) ? 0 : parseInt($('#ef-relevancy').val()));
	var effectiveness = ((isNaN(parseInt($('#ef-effectiveness').val()))) ? 0 : parseInt($('#ef-effectiveness').val()));
	var quality = ((isNaN(parseInt($('#ef-quality').val()))) ? 0 : parseInt($('#ef-quality').val()));
	var potential = ((isNaN(parseInt($('#ef-potential').val()))) ? 0 : parseInt($('#ef-potential').val()));
	var grand_total = creativity + output + outcome + commitment + relevancy + effectiveness + quality + potential;
	$('#ef-total').val(grand_total); 
	if(grand_total > 100 || grand_total < 0){
		$('#score-balance').text(0);
	}else{
		$('#score-balance').text(BALANCE_SCORE-grand_total);
	}
}

function initEvaluationDetail(){
  $('.btn-evaluation-detail').click(function(e){
    e.preventDefault();
    var id = $(this).attr('eval_id');
    var data_post = {id : id};
    $.ajax({
        url     : base_url+'evaluations/view_detail_popup/',
        data    : data_post,
        type    : 'POST',
        success   : function (data) {
            $("#tabs-popup").html(data);
            $("#eval-popup-tab").tabs();
            $("#view-detail-dialog").dialog("open");
        }
      });
  });

  $( "#view-detail-dialog" ).dialog({
      width:"900px",
      autoOpen: false,
      title: "Detail",
      resizable: false,
      modal: true,
      buttons: {
        OK: function() {
          $( this ).dialog( "close" );
        }
      }
    });
}
