$(document).ready(function() { 
	//add username validator, only accept alphanumeric, dot (.) & underscore (_)
	$.validator.addMethod("validUsername", function(value, element) {
		return this.optional(element) || value == value.match(/^[a-zA-Z0-9._]+$/);
	}, "Username can only contain alphanumeric, dot & underscore character only");
 
	/* FORM INNOVATOR ACCOUNT *******************************************************************/
	$("#innovator-account").validate({
        rules: {
			'username' : {
				required: true,
				validUsername: true
			},
			'email' :{
				required: true,
				email: true
			},
			'password' : {
				required: true,
				minlength: 6
			},
			'retype_password' : {
				required: true,
				equalTo: "input[name=password]"
			}
		}
	});
	
	/* FORM INNOVATOR *************************************************************************/
	$("#innovator").validate({
        rules: {
			'name' : {
				required: true
			},
			'team_expert[]' : {
				required: true
			},
			'email' : {
				email: true
			},
			'kod_no1' : {
				required: true,
				maxlength: 6,
				minlength: 6
			},
			'kod_no2' : {
				required: true,
				maxlength: 2,
				minlength: 2
			},
			'kod_no3' : {
				required: true,
				maxlength: 4,
				minlength: 4
			},
			'gender' :{
				required: true
			},
			'date_of_birth' : {
				required: true,
				date: true
			},
			'address' : {
				required: true
			},
			'postcode' : {
				required: true
			},
			'city' : {
				required: true,
			},
			'team_leader' : {
				required: true
			},
			'picture':{
				accept:"jpg,png,jpeg,gif,bmp"
			}
		},
		messages:{
			'picture' :{
				accept: "Only jpg/png/jpeg/gif/bmp file type is allowed"
			}
		},
		submitHandler: function(form){	
			save_innovator($("#innovator"));			
		}
	});
	
	/* FORM INNOVATOR DEMOGRAPHIC *************************************************************************/
	$("#innovator-demographic").validate({
        rules: {
			'd_home_phone_no' :{
				number: true
			},
			'd_official_phone_no' :{
				number: true
			},
			'd_mobile_phone_no' : {
				required: true,
				number: true
			},
			'd_fax_no' : {
				number: true
			}
		}
	});	
	
	/* FORM INNOVATOR COMPANY *************************************************************************/
	$("#innovator-company").validate({
        rules: {
			'c_name' :{
				required: true
			},
			'c_registration_no' :{
				required: true
			},
			'c_registration_date' : {
				required: true,
				date: true
			},
			'c_operation_date' : {
				required: true,
				date: true
			},
			'c_address' :{
				required: true
			},
			'c_postcode' :{
				required: true,
				number: true
			},
			'c_city' :{
				required: true
			},
			'c_telp_no' :{
				required: true,
				number: true
			},
			'reg_cert':{
				accept:"jpg,png,jpeg,gif,bmp,pdf,docx,doc"
			}
		}
	});
	
	/* FORM INNOVATOR HEIR *************************************************************************/
	$("#innovator-heir").validate({
        rules: {
			'h_name' :{
				required: true
			},
			'kod_no1' : {
				required: true,
				maxlength: 6,
				minlength: 6
			},
			'kod_no2' : {
				required: true,
				maxlength: 2,
				minlength: 2
			},
			'kod_no3' : {
				required: true,
				maxlength: 4,
				minlength: 4
			},
			'h_relationship' : {
				required: true
			},
			'h_age' : {
				number: true
			},
			'h_mobile_phone_no' : {
				required: true,
				number: true
			}
		}
	});
	
	/* FORM INNOVATION *************************************************************************/
	$("#innovation").validate({
        rules: {
			'name_in_melayu' :{
				required: true
			},
			'inspiration_in_melayu' :{
				required: true
			},
			'description_in_melayu' : {
				required: true
			},
			'name' : {
				required: true
			},
			'inspiration' : {
				required: true
			},
			'description' : {
				required: true
			},
			'created_date' : {
				required: true,
				date: true
			},
			'discovered_date' : {
				required: true,
				date: true
			},
			'category_id[]' : {
				required: true
			},
			'subcategory[]' : {
				required: true
			},
			'myipo_protection' : {
				required: true
			}
		},
		submitHandler: function(form){	
			save_innovation($("#innovation"));			
		}

	});

	$("#form-task").validate({
        rules: {
			'title' :{
				required: true
			},
			'expert_id' :{
				required: true
			},
			'innovation_id[]' : {
				required: true
			},
		},
		messages: {
            'innovation_id[]': {
                required: "Please select at least 1 innovation",
            },
        },
	});
	
	/*PORTFOLIO ASSIGNMENT*/
	$("#portfolio-assignment").validate({
        rules: {
			'title' :{
				required: true
			},
			'manager_id' :{
				required: true
			},
			'innovation[]' : {
				required: true
			},
		},
		messages: {
            'innovation[]': {
                required: "Please select at least 1 innovation",
            },
        }
	});

	/*PORTFOLIO ATTACHMENT*/
	$("#portfolio-attachment").validate({
        rules: {
			'attachment' :{
				required: true,
				accept:"jpg,png,jpeg,gif,bmp,pdf,docx,doc"
			},
			'note' :{
				required: true
			}
		}
	});	
	
	/*PORTFOLIO INNOVATION ASSET*/
	$("#innovation-asset").validate({
        rules: {
			'asset_title' :{
				required: true
			},
			'asset_url' :{
				required: true,
				url: true
			},
			'asset_attachment':{
				required: true,
				accept:"jpg,png,jpeg,gif,bmp,pdf,docx,doc"
			},
			'asset_description' :{
				required: true
			}
		},
		messages:  {
            'asset_attachment':{
                accept: "Only jpg/png/jpeg/gif/bmp/pdf/docx/doc file type is allowed"
            } 
        },
	});	
	
	/*PORTFOLIO COMMENT*/
	$("#portfolio-comment").validate({
        rules: {
			'comment_content' :{
				required: true
			}
		}
	});	
	
	/*PORTFOLIO NOTE*/
	$("#note-form").validate({
        rules: {
			'note_title' :{
				required: true
			},
			'note_content':{
				required: true
			},
			'note_creation_date':{
				required: true
			}
		}
	});	

	/*INNOVATOR APPROVAL*/
	$("#approval-form").validate({
        rules: {
			'app_note' :{
				required: true
			}
		}
	});	

	/*EVALUATION 1*/
	$("#form-evaluation-1").validate({
        rules: {
			'total_creativity' : {
				required: true,
				number:true,
			},
			'creativity_ori' : {
				required: true,
				number:true,
				max:6,
				min:0
			},
			'creativity_adap' : {
				required: true,
				number:true,
				max:4,
				min:0
			},
			'implementation_level_full' : {
				required: true,
				number:true,
				max:6,
				min:0
			},
			'implementation_level_trial' : {
				required: true,
				number:true,
				max:4,
				min:0
			},
			'replicability_free' : {
				required: true,
				number:true,
				max:6,
				min:0
			},
			'replicability_not_free' : {
				required: true,
				number:true,
				max:4,
				min:0
			},
			'total_output' : {
				required: true,
				number:true,
			},
			'efficiency_costs' : {
				required: true,
				number:true,
				max:2,
				min:0
			},
			'efficiency_time' : {
				required: true,
				number:true,
				max:2,
				min:0
			},
			'efficiency_productivity' : {
				required: true,
				number:true,
				max:2,
				min:0
			},
			'efficiency_design' : {
				required: true,
				number:true,
				max:2,
				min:0
			},
			'significance_global' : {
				required: true,
				number:true,
				max:6,
				min:0
			},
			'significance_national' : {
				required: true,
				number:true,
				max:4,
				min:0
			},
			'significance_local' : {
				required: true,
				number:true,
				max:2,
				min:0
			},
			'total_outcome' : {
				required: true,
				number:true,
			},
			'commitment_finance' : {
				required: true,
				number:true,
				max:2,
				min:0
			},
			'commitment_hr' : {
				required: true,
				number:true,
				max:2,
				min:0
			},
			'commitment_incentive' : {
				required: true,
				number:true,
				max:2,
				min:0
			},
			'commitment_reward' : {
				required: true,
				number:true,
				max:2,
				min:0
			},
			'commitment_equipment' : {
				required: true,
				number:true,
				max:2,
				min:0
			},
			'total_commitment' : {
				required: true,
				number:true,
			},
			'relevancy' : {
				required: true,
				number:true,
				max:10
			},
			'total_relevancy' : {
				required: true,
				number:true,
				max:10
			},
			'effectiveness' : {
				required: true,
				number:true,
				max:10
			},
			'total_effectiveness' : {
				required: true,
				number:true,
				max:10
			},
			'quality' : {
				required: true,
				number:true,
				max:10
			},
			'total_quality' : {
				required: true,
				number:true,
				max:10
			},
			'potential' : {
				required: true,
				number:true,
				max:10
			},
			'total_potential' : {
				required: true,
				number:true,
				max:10
			},
			'total' : {
				max:100
			}
		},
	}); 
	
	/*EDIT PASSWORD FORM*/
	$("#edit-password").validate({
        rules: {
			'old_password' :{
				required: true
			},
			'new_password':{
				required: true,
				minlength: 6
			},
			'retype_password':{
				required: true,
				equalTo: "input[name=new_password]"
			}
		}
	});	

	/*COMMITTEE MEETING FORM*/
	$("#form-committee-meeting").validate({
        rules: {
			'date' :{
				required: true
			},
			'title':{
				required: true
			},
			'attendance':{
				required: true
			}
		}
	});	

	/*HIP6 EVALUATION*/
	$("#form-hip6-evaluation").validate({
        rules: {
			'originality_innovation' : {
				required: true,
				number:true,
				max:10,
				min:0
			},
			'comparable' : {
				required: true,
				number:true,
				max:10,
				min:0
			},
			'innovation_impact' : {
				required: true,
				number:true,
				max:10,
				min:0
			},
			'unit_cost' : {
				required: true,
				number:true,
				max:10,
				min:0
			},
			'cost_of_assistance' : {
				required: true,
				number:true,
				max:10,
				min:0
			},
			'excluded_group' : {
				required: true,
				number:true,
				max:10,
				min:0
			},
			'market_size' : {
				required: true,
				number:true,
				max:10,
				min:0
			},
			'attitude_of_entrepreneur' : {
				required: true,
				number:true,
				max:10,
				min:0
			},
			'diffusion_platform' : {
				required: true,
				number:true,
				max:10,
				min:0
			},
			'strategic_linkages' : {
				required: true,
				number:true,
				max:10,
				min:0
			},
			'total' : {
				max:100
			}
		},
	}); 
});