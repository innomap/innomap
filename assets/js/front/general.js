/*Global Variables & All language label in all file*/
var LANGUAGE_MELAYU 		= 'melayu',
	msg_confirm_delete 		= {melayu: "Adakah anda pasti anda mahu memadam?", english: "Are you sure you want to delete?"},
	label_approve 			= {melayu: "meluluskan", english: "approve"},
	label_inno_approval 	= {melayu: "Kelulusan Inovator", english: "Innovator Approval"},
	label_reject			= {melayu: "menolak", english: "reject"},
	label_inno_rejection	= {melayu: "Penolakan Inovator", english: "Innovator Rejection"},
	msg_confirm_send_req	= {melayu: "Anda tidak boleh mengemas data selepas dihantar untuk kelulusan. Pastikan data anda telah benar", english: "You cannot edit data after sent for approval. Make sure your data is correct"},
	msg_confirm_send_req_innovation	= {melayu: "Adakah anda pasti anda mahu menghantar inovasi ini untuk kelulusan?", english: "Are you sure you want to send this innovation for approval?"},
	msg_confirm_delete_attch = {melayu: "Adakah anda pasti mahu tidak mencentang item ini? Lampiran dan catatan akan terpadam juga", english: "Are you sure you want to unchecked this item? Uploaded attachment and notes will be deleted too"},
	link_view_detail		= {melayu: "Lihat Terperinci", english: "View Detail"},
	value_male				= {melayu: "Lelaki", english: "Male"},
	value_female			= {melayu: "Perempuan", english: "Female"},
	label_innovation_approval = {melayu: "Kelulusan Inovasi", english: "Innovation Approval"},
	label_innovation_rejection = {melayu: "Penolakan Inovasi", english: "Innovation Rejection"},
	info_no_note 			= {melayu: "Tiada nota pada tarikh ini", english: "No note on this date"},
	semantic_res_title_inno	= {melayu: "Inovasi", english: "Innovations"},
	semantic_res_title_expert = {melayu: "Pakar", english: "Experts"},
	label_hip6_innovation_approval = {melayu: "Kelulusan Inovasi HIP6", english: "Innovation Approval"},
	label_hip6_innovation_rejection = {melayu: "Penolakan Inovasi HIP6", english: "Innovation Rejection"},
	msg_confirm_portfolio_task_change	= {melayu: "Adakah anda pasti mahu meluluskan perubahan lampiran pada tugasan ini?", english: "Are you sure you want to approve attachment changes in this task?"};

var DATATABLES_STORAGE_NAME = 'Datatables_innomap_q2dlUiIYB',
DATATABLES_INNOVATION_STORAGE_NAME = 'Datatables_innovation_innomap_q2dlUiIYB';
	
$(document).ready(function() { 
	//fix on hover tooltip bug in highcharts
	jQuery.cssProps.opacity = 'opacity';
	
	//login form
	$(".login-button").click(function() {
		$(".form-login").toggle(500);
	});
	
	//datepicker
	$(".datepicker-dob").datepicker({changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd", maxDate:0, yearRange: "-53:+0"});
	$(".datepicker").datepicker({dateFormat: "yy-mm-dd"});
	
	//input type file in form
	$("#uploadBtn").change(function(){
		$("#uploadFile").val($(this).val());
		console.log($(this).val())
		$(this).closest('img.innovator-img').attr('src',$(this).val());
	});
				
	initGetNotifNumber();
	
	//set menu whether on mobile or not
	set_menu();
	$(window).on('resize', function(){
		set_menu();
	});

	$('.reset_data_tables').on('click',function(e){
		reset_dt_view();
	});

	print_page();
});

/*set menu based on window size*/
function set_menu(){
	if ($(window).width() < 761) {
		$("#menu").hide();
		$('.show-menu').unbind('click').click(function(e){
			e.preventDefault();
			$("#menu").toggle("slide", { direction: "up" }, 500);
		});
	}else {
		$("#menu").show();
	}
}

/*delete confirmation*/
function confirm_delete() {
	if(confirm(msg_confirm_delete[lang]) == false) {
		return false;
	}
}

function confirmation(msg) {
	if(confirm(msg) == false) {
		return false;
	}
}

function initGetNotifNumber(){
	 $.ajax({
		type: "POST",
		url: base_url+"notifications/new_notif",
		data: $.param({}),
		dataType:'json',
		success: function(response){
				if(response > 0){
					$('.notif_number').text(response).fadeIn();
				}
				timer = setTimeout("initGetNotifNumber()",120000);
			}
	});
}

function scrollable_table(){
	//scrollable table dataTables configuration
	var scrollable = $('.scrollable-table').dataTable({
		"iDisplayLength": "All",
        "sScrollX": "100%",
		"sScrollY": "480",
		"scrollCollapse": true,
        "bPaginate": false,
		"bFilter": false,
		"sDom": '<"top">rt<"bottom"flp><"clear">',
		"columnDefs": [ {
            "visible": false,
            "targets": 1
        } ]
    });
	
	if($(".dataTables_scrollBody table").height() <= 480){
		$(".dataTables_scrollBody").height("");
	}else{
		$(".dataTables_scrollBody").height("470px");
	}
}

function reset_dt_view() {
	localStorage.removeItem(DATATABLES_STORAGE_NAME);
	localStorage.removeItem(DATATABLES_INNOVATION_STORAGE_NAME);
}

function print_page(){
	$('.print_page').click(function(e){
        e.preventDefault();
        window.print();
    });
}

/*Set Progress Bar*/
function progress_bar(act){
	switch(act){
		case 'create' 	: $("#progress-bar").dialog({autoOpen: false, modal:true, width: 250, height: 90, open: function(event, ui) { $(this).parent().children().children('.ui-dialog-titlebar-close').hide(); } }); break;
		case 'open'		: $("#progress-bar").dialog('open'); break;
		case 'close'	: $("#progress-bar").dialog('close'); break;
	}
}