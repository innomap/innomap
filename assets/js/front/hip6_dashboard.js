$(document).ready(function(){
	$(".pie-chart").each(function(){
		initShowPieChart($(this));
	});

	initLoadMoreInnovationChart();
	initLoadMorePortfolioChart();

	initCheckTotalInnovation();
	initCheckTotalPortfolio();
});

function initLoadMoreInnovationChart(){
	var page_num = 0;
	$('.lm-innovation').on('click',function(e){
		e.preventDefault();

		page_num++;
		$.post(base_url+'hip6_dashboard/load_more_innovation',{page_number : page_num}, function(response){

			for(var i=0; i < response.length; i++){
			var content = '<div class="grid_4 omega">';
			  	content += '<label>'+response[i].name_in_melayu+'</label>';
			  	content += '<input type="hidden" class="criteria-done" value="'+response[i].criteria_done+'">';
			  	content += '<input type="hidden" class="criteria-in-progress" value="'+response[i].criteria_in_progress+'">';
			  	content += '<div class="pie-chart innovation pre-load"></div>';
			  	content += '</div>';

			  	$('.innovation-portfolio-wrap').append(content);
			}

			$(".pie-chart.pre-load").each(function(){
				initShowPieChart($(this));
			});

			initCheckTotalInnovation();
		},'json');
	});
}

function initLoadMorePortfolioChart(){
	var page_num = 0;
	$('.lm-portfolio').on('click',function(e){
		e.preventDefault();

		page_num++;
		$.post(base_url+'hip6_dashboard/load_more_portfolio',{page_number : page_num}, function(response){

			for(var i=0; i < response.length; i++){
			var content = '<div class="grid_4 omega">';
			  	content += '<label>'+response[i].title+'</label>';
			  	content += '<div class="clear"></div>';
			  	content += '<label>'+response[i].manager_name+'</label>';
			  	content += '<input type="hidden" class="criteria-done" value="'+response[i].criteria_done+'">';
			  	content += '<input type="hidden" class="criteria-in-progress" value="'+response[i].criteria_in_progress+'">';
			  	content += '<div class="pie-chart portfolio pre-load"></div>';
			  	content += '</div>';

			  	$('.portfolio-wrap').append(content);
			}

			$(".pie-chart.pre-load").each(function(){
				initShowPieChart($(this));
			});

			initCheckTotalPortfolio();
		},'json');
	});
}

function initCheckTotalInnovation(){
	var total_innovation = $('.total-innovation').val();
	if(parseInt(total_innovation) == $('.pie-chart.innovation').length){
		$('.lm-innovation').parent().hide();
	}
}

function initCheckTotalPortfolio(){
	var total_portfolio = $('.total-portfolio').val();
	if(parseInt(total_portfolio) == $('.pie-chart.portfolio').length){
		$('.lm-portfolio').parent().hide();
	}
}

function initShowPieChart(elem){
	$(elem).removeClass('pre-load');
	var parent = elem.parent();
	initDrawPieChart(elem,[parseFloat(parent.find(".criteria-done").val()), parseFloat(parent.find(".criteria-in-progress").val())], 200);
}

//draw pie chart
function initDrawPieChart(container,data,h){
	var chart;

	container.highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
			height: h,
			backgroundColor:'rgba(255, 255, 255, 0.1)',
        },
        credits: {
            enabled: false
        },
        title: {
            text: ""
        },
		tooltip: {
			formatter: function () {
			   return this.series.name + '<br>' + this.point.name + ': <b>' + Highcharts.numberFormat(this.percentage, 2) + '%</b>';
			}
		},
		plotOptions: {
			pie: {
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					formatter: function() {
                        return Math.round(this.percentage*100)/100 + ' %';
                    },
                    distance: -40,
                    color:'white',
                    style: {
                        fontWeight:'bold',
                        fontSize:'10pt'
                    }
				}
			}
		},
        series: [{
            type: 'pie',
            name: 'Progress',
			data: [{name: 'Uploaded', y: data[0], color: "#50B432"},
				   {name: 'Not Uploaded Yet', y: data[1], color: "#ED561B"}
			]	
		}]
	});
}