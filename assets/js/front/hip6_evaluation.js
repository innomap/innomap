var BALANCE_SCORE = 100;

$(document).ready(function(){
	calculateTotal();
});

function check_value(input){
	if(isNaN(parseInt(input))){
		return 0;
	}else{
		return parseInt(input);
	}
}

function calculateTotal(){
	$('#he-originality-innovation, #he-comparable, #he-innovation-impact, #he-unit-cost, #ef-commitment-equipment, #he-cost-of-assistance, #he-excluded-group, #he-market-size, #he-attitude-of-entrepreneur, #he-diffusion-platform, #he-strategic-linkages').keyup(function(){
		var originality_innovation = check_value($('#he-originality-innovation').val()),
		comparable = check_value($('#he-comparable').val()),
		innovation_impact = check_value($('#he-innovation-impact').val()),
		unit_cost = check_value($('#he-unit-cost').val()),
		commitment_equipment = check_value($('#ef-commitment-equipment').val()),
		cost_of_assistance = check_value($('#he-cost-of-assistance').val()),
		excluded_group = check_value($('#he-excluded-group').val()),
		market_size = check_value($('#he-market-size').val()),
		attitude_of_entrepreneur = check_value($('#he-attitude-of-entrepreneur').val()),
		diffusion_platform = check_value($('#he-diffusion-platform').val()),
		strategic_linkages = check_value($('#he-strategic-linkages').val());
		//$('input[for='+$(this).attr('id')+']').val($(this).val());
		var grand_total = originality_innovation + comparable + innovation_impact + unit_cost + commitment_equipment + cost_of_assistance + excluded_group + market_size + attitude_of_entrepreneur + diffusion_platform + strategic_linkages;
		$('#he-total').val(grand_total); 
		if(grand_total > 100 || grand_total < 0){
			$('#score-balance').text(0);
		}else{
			$('#score-balance').text(BALANCE_SCORE-grand_total);
		}
	});
}

