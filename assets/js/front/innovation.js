$(document).ready(function() { 
	
	progress_bar('create');

	//change innovation source to hip6 login as project director
	$('.changeToHIP6').click(function(e){
		var innovation_id = $(this).attr('innoID');
		var elem = $(this);
		$.post(base_url+"innovations/update_source",{'innovation_id':innovation_id},function( data ) {
		  if(data == "true"){
		  	$(elem).siblings('span').show();
		  	$(elem).hide();
		  }
		});
		e.preventDefault();
	});

	//init textarea text counter on first load
	$(".text-count-elem").each(function(){
		textCounter($(this));
	});
	
	//textarea text counter on change
	$(".text-count-elem").keyup(function(){
		textCounter($(this));
	});
	
	//add more category
	$("#add-category").click(function(e){
		e.preventDefault();
		addCategory();
	});
	
	//remove category
	$("#category-value-wrapper").on('click','.remove-category', function(e){
		e.preventDefault();
		if(confirm(msg_confirm_delete[lang]) == false) {
			return false;
		}else{
			var idx = $(this).index('.remove-category');
			$('.category').eq(idx).remove();
			$('.remove-category').eq(idx).remove();
		}
	});
	
	//add more picture
	$("#add-innovation-pict").click(function(e){
		e.preventDefault();
		addPicture();
	});
	
	//confirmation before send approval request
	$("#send-approval").submit(function(){
		var c = confirm(msg_confirm_send_req[lang]);
		return c; 
	});
	
	//add keyword
	$('#add-keyword-btn').click(function(){
		addKeyword();
	});
	$('#keyword-inp').bind("keyup keypress", function(e) {
		var code = e.keyCode || e.which; 
		if (code  == 13) {  //on keypress enter
			addKeyword();
			e.preventDefault();
			return false;
		}
	});
	
	//remove keyword
	$("#keyword-list").on('click','.remove-keyword', function(e){
		e.preventDefault();
		$(this).parent('li').remove();
	});
	
	//innovation approve & reject
	$(".approval").click(function(){
		var status = $(this).attr('status');
		var innovationID = $(this).attr('innovation_id');
		$("input[name=app_innovation_id]").val($(this).attr('innovation_id'));
		$("input[name=app_status]").val(status);
		$("#caption").text((status == 3 ? label_reject[lang] : label_approve[lang]));
		
		//set note modal
		$("#approval-note").dialog({
			title		: (status == 3 ? label_innovation_rejection[lang] : label_innovation_approval[lang]),
			autoOpen	: false,
			resizable	: false,
			width		: 350,
			height		: 'auto',
			modal		: true,
			position	: 'center',
			buttons: {
				"Yes" : function() {
					if(status == 2){
						add_innovation_ontology(innovationID);
					}
					$("#approval-form").submit();
				},
				"No" : function(){
					$("#approval-note").dialog('close');
				}
			}
		});
		$("#approval-note").dialog('open');
	});

	initViewNotes();
	initViewDetail();
	initHip6Approval();
	initEditNote();
});

//save innovation to database and edit in ontology
function save_innovation($form){
	progress_bar('open');
	$form.ajaxSubmit({ 
		dataType	: 'json',
		success		: function(response) {
			if(response){
				if($("#status").val() == 2){ //save approved innovator only
					edit_innovation_ontology($('#innovation_id').val(),response.res_url);
				}else{
					window.location = response.res_url; 
				}
			}
		}
	});
}

function edit_innovation_ontology(innovationID, resURL){
	$.post(base_url+"semantics/get_innovation_semantic/"+innovationID, function(detail) {
		$.ajax({ 
			url: semantic_url+"editInnovationPost", 
			crossDomain : true,
			type: 'POST', 
			dataType: 'json', 
			data: detail,
			contentType: 'application/json',
			mimeType: 'application/json',
			success: function(data) {
				window.location = resURL; 
				console.log("Innovation id "+innovationID+" status (success): "+data);
			},
			error:function(data,status,er) { 
				window.location = resURL; 
				console.log("innovation id "+innovationID+"error: "+data+" status: "+status+" er:"+er);
			}
		});
	});
}

/*send innovation data to ontology*/
function add_innovation_ontology(innovationID){
	progress_bar('open');
	$.post(base_url+"semantics/get_innovation_semantic/"+innovationID, function(detail) {
		$.ajax({ 
			url: semantic_url+"addInnovationPost", 
			crossDomain : true,
			type: 'POST', 
			dataType: 'json', 
			data: detail,
			contentType: 'application/json',
			mimeType: 'application/json',
			success: function(data) {
				add_innovator_innovation_ontology(innovationID);
				console.log("Innovation id "+innovationID+" status (success): "+data);
			},
			error:function(data,status,er) { 
				console.log("innovation id "+innovationID+"error: "+data+" status: "+status+" er:"+er);
			}
		});
	});
}

function add_innovator_innovation_ontology(innovation_id){
	$.post(base_url+"semantics/get_innovation_innovator_semantic",{ innovator_id: "", innovation_id: innovation_id }, function(data) {
		for(var i=0; i<data.length; i++){
			var innovatorID = data[i].innovator_id;
			var innovationID = data[i].innovation_id;
			$.ajax({ 
				url: semantic_url+"addInnovatorInnovation?callback=a&innovatorID="+innovatorID+"&innovationID="+innovationID, 
				crossDomain : true,
				type: 'GET', 
				dataType: 'json',
				contentType: 'application/json',
				mimeType: 'application/json',
				success: function(data) {
					console.log("status success : "+data);
				},
				error:function(data,status,er) { 
					console.log("error: "+data+" status: "+status+" er:"+er);
				}
			});
		}
	},'json');
	progress_bar('close');
}

/*limit textarea content length*/
function textCounter(elem) {
	var field 		= elem;
	var counter 	= field.nextAll('.text-counter:first');
	var maxlimit 	= 500;
	if(field.text().length > maxlimit) {
		field.value = field.val().substring(0, maxlimit);
		counter.val(0);
	}else{
		if(field.val() != ""){
			counter.text(maxlimit - field.val().length+" characters left");
		}
	}
}

/*add more category*/
function addCategory(){
	if($(".category:last").val() != ""){
		if($(".category:last option").length-2 > 0){
			var selected_cat  = $(".category").map( function () { return $( this ).val(); }).get();
			var scat = '<select name="category_id[]" class="category">';
			$(".category:first option").each(function(){
				if($.inArray($(this).val(),selected_cat) == -1){
					scat += '<option value="'+$(this).val()+'">'+$(this).text()+'</option>';
				}
			});
			scat += '</select><a href="#" class="remove-category">(-) Remove Category</a><div class="subcategory"></div>';
			$("#category-value-wrapper").append(scat);
		}else{
			alert('There is no available category');
		}
	}else{
		alert('Please select your category first before add more category.');
	}
}

/*send approval confirmation request*/
function approval_confirmation(valid){
	if(valid){
		if(confirm(msg_confirm_send_req_innovation[lang]) == false) {
			return false;
		}
	}else{
		alert('Anda harus mengisi seluruh data sebelum dapat menghantar inovasi.');
		return false;
	}
}

/*add innovation picture*/
function addPicture(){
	var next_num     = eval($("input[name=pict_num]").val())+1;
	var elem_string  = "<div>";
	elem_string 	+= '<input type="file" name="picture_'+next_num+'" />';
	elem_string		+= '<input type="hidden" name="bef_picture_'+next_num+'" value="" />';
	$("#inno-pict-wrapper").append(elem_string);
	$("input[name=pict_num]").val(next_num++);
}

/*add keyword*/
function addKeyword(){
	var keywords = $("#keyword-inp").val().split(",");
	$.each(keywords,function(k,keyword){
		if(keyword != ''){
			$("#keyword-list").append("<li><input type='hidden' name='keyword[]' value='"+keyword+"'>"+keyword+"<a href='#' class='remove-keyword'>(-) remove</a></li>");
		}
	});
	$("#keyword-inp").val("");
}

function initApproveInnovation(){
	$( "#approval-dialog" ).dialog({
      autoOpen: false,
      title: "Appove Confirmation",
      resizable: false,
      modal: true,
      dialogClass:'approval-popup green',
      buttons: {
        "Approve": function() {
        	var innovation_id = $("#innovation-id-dialog").val();
        	var approval_remarks = $('#approval-remarks').val();
        	var status = $('#status').val();
        	var data_post = {innovation_id: innovation_id,approval_remarks: approval_remarks, status: status};

	        $.ajax({
    				url			: base_url+'innovations/approval/',
    				data 		: data_post,
    				type		: 'POST',
    				dataType	: 'json',
    				success		: function (data) {
    					$('#message-dialog').append(data.messages);
    					$('#message-dialog').dialog('open');
    				}
    			});
        },
        Cancel: function() {
          $( this ).dialog( "close" );
        }
      }
    });

    $("#message-dialog").dialog({
      autoOpen: false,
      resizable: false,
      modal: true,
      dialogClass:'approval-popup message',
      buttons: {
        Ok : function() {
          	$(this).dialog( "close" );
          	$('#approval-dialog').dialog('close');
          	location.reload(true);
        }
      }
    });

	$('.approve-innovation').click(function(){
    	var innovation_id = $(this).attr('innovation_id');
    	var status = $(this).attr('status');
		$('#innovation-id-dialog').val(innovation_id);
		$('#status').val(status);
    	$('#approval-dialog').dialog('open');
	});
}

function initRejectInnovation(){
	$( "#reject-dialog" ).dialog({
      autoOpen: false,
      title: "Reject Confirmation",
      resizable: false,
      modal: true,
      dialogClass:'approval-popup red',
      buttons: {
        "Reject": function() {
          var innovation_id = $("#innovation-id-dialog").val();
          var approval_remarks = $('#reject-dialog #approval-remarks').val();
          var status = $('#status').val();
          var data_post = {innovation_id: innovation_id, approval_remarks: approval_remarks, status: status};

          $.ajax({
            url     : base_url+'innovations/approval/',
            data    : data_post,
            type    : 'POST',
            dataType  : 'json',
            success   : function (data) {
              $('#message-dialog').append(data.messages);
              $('#message-dialog').dialog('open');
            }
          });
        },
        Cancel: function() {
          $( this ).dialog( "close" );
        }
      }
    });

    $("#message-dialog").dialog({
		autoOpen: false,
		resizable: false,
		modal: true,
		dialogClass:'approval-popup message',
		buttons: {
			Ok : function() {
				$(this).dialog( "close" );
				$('#reject-dialog').dialog('close');
				location.reload(true);
			}
		}
    });

	$('.reject-innovation').click(function(){
		$('#innovation-id-dialog').val($(this).attr('innovation_id'));
		$('#status').val($(this).attr('status'));
		$('#reject-dialog').dialog('open');
	});
}

function initViewNotes(){
    $('.view-notes').click(function(e){
      e.preventDefault();
      var id = $(this).attr('inno_id');
      var data_post = {id : id};
      var content = "";
      $.ajax({
          url     : base_url+'innovations/view_notes/',
          data    : data_post,
          type    : 'POST',
          dataType  : 'json',
          success   : function (data) {
            if(data.status == 1){
              content += "<label>Innovation: </label><span>"+data.innovation+"</span><br/>";
              content += "<label>Status: </label><span>"+data.status_approval+"</span><br/>";
              content += "<label>Notes: </label><p>"+data.notes+"</p>";
            }
            $("#view-notes-dialog").html(content);
            $("#view-notes-dialog").dialog("open");
          }
        });
    });

    $( "#view-notes-dialog" ).dialog({
      autoOpen: false,
      title: "Notes",
      resizable: false,
      modal: true,
      buttons: {
        OK: function() {
          $( this ).dialog( "close" );
        }
      }
    });
}

function initViewDetail(){
  $('.view-detail-inno').click(function(e){
    e.preventDefault();
    var id = $(this).attr('inno_id');
    var data_post = {id : id};
    $.ajax({
        url     : base_url+'innovations/view_detail_popup/',
        data    : data_post,
        type    : 'POST',
        success   : function (data) {
            $("#tabs-popup").html(data);
            $("#view-detail-dialog").dialog("open");
        }
      });
  });

  $( "#view-detail-dialog" ).dialog({
      width:"600px",
      autoOpen: false,
      title: "Detail",
      resizable: false,
      modal: true,
      buttons: {
        OK: function() {
          $( this ).dialog( "close" );
        }
      }
    });
}

/*Set Progress Bar*/
function progress_bar(act){
	switch(act){
		case 'create' 	: $("#progress-bar").dialog({autoOpen: false, modal:true, width: 250, height: 90}); break;
		case 'open'		: $("#progress-bar").dialog('open'); break;
		case 'close'	: $("#progress-bar").dialog('close'); break;
	}
}

function initHip6Approval(){
	$('#innovator_table').on('click','.hip6-approval',function(){
		var status = $(this).attr('status');
		var innovationID = $(this).attr('innovation_id');
		$("input[name=app_innovation_id]").val($(this).attr('innovation_id'));
		$("input[name=app_status]").val(status);
		$("#caption").text((status == 3 ? label_reject[lang] : label_approve[lang]));
		
		//set note modal
		$("#hip6-approval-note").dialog({
			title		: (status == 3 ? label_hip6_innovation_rejection[lang] : label_hip6_innovation_approval[lang]),
			autoOpen	: false,
			resizable	: false,
			width		: 350,
			height		: 'auto',
			modal		: true,
			position	: 'center',
			buttons: {
				"Yes" : function() {
					$("#hip6-approval-form").submit();
				},
				"No" : function(){
					$("#hip6-approval-note").dialog('close');
				}
			}
		});
		$("#hip6-approval-note").dialog('open');
	});
}

function initEditNote(){
	$('.tbl_innovation_note').on('click','.edit_note',function(e){
		e.preventDefault();

		var id = $(this).data('id'),
			inno_id = $(this).data('inno-id'),
			note = $(this).closest("tr").find(".note_val").text();
		
		$("input[name=dlg_note_id]").val(id);
		$("input[name=dlg_innovation_id]").val(inno_id);
		$("textarea[name=dlg_note]").val(note);
		
		//set note modal
		$("#edit_note_dialog").dialog({
			autoOpen	: false,
			resizable	: false,
			width		: 350,
			height		: 'auto',
			modal		: true,
			position	: 'center',
			buttons: {
				"Batal" : function(){
					$("#edit_note_dialog").dialog('close');
				},
				"Simpan" : function() {
					$("#edit_note_form").submit();
				},
			}
		});
		$("#edit_note_dialog").dialog('open');
	});
}