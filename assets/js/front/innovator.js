$(document).ready(function() { 
	progress_bar('create');

	//calculate age
	if($("input[name=kod_no1]").length > 0){
		ageFromKadNo($("input[name=kod_no1]").val());	
		$("input[name=kod_no1]").on('keyup',function(){
			ageFromKadNo($(this).val());
		});
	}
	
	//identity: inovator berkumpulan
	innovatorTeam($("#innovator-identity"));
	$("#innovator-identity").change(function(){
		innovatorTeam($(this));
	});
	
	//add more innovator team expert
	$("#innovator-team-wrap").on("click","#add-team-expert", function(e){
		e.preventDefault();
		$("#innovator-team-wrap .value:last").append("<div class='each-team-expert'><input type='text' class='grid_4' name='team_expert[]' /><a href='#' class='remove-team-expert'>(-) Remove</a><br/><br/></div>");
	});
	
	//remove innovator team expert
	$("#innovator-team-wrap").on("click",".remove-team-expert", function(e){
		e.preventDefault();
		var parent = $(this).parent();
		if(confirm_delete() !== false){
			if(parent.find('.team-expert-id').length > 0){
				$.get(base_url+"innovators/delete_team_expert/"+parent.find('.team-expert-id').val(), function(res){
					if(res == 'success'){
						parent.remove();
					}else{
						alert('team expert cannot deleted');
					}
				});
			}else{
				parent.remove();
			}
		}
	});
	
	//innovator approve & reject
	$(".approval").click(function(){
		var status = $(this).attr('status');
		var innovatorID = $(this).attr('innovator_id');
		$("input[name=app_innovator_id]").val($(this).attr('innovator_id'));
		$("input[name=app_status]").val(status);
		$("#caption").text((status == 3 ? label_reject[lang] : label_approve[lang]));
		
		$("#approval-note").dialog({
			title		: (status == 3 ? label_inno_rejection[lang] : label_inno_approval[lang]),
			autoOpen	: false,
			resizable	: false,
			width		: 350,
			height		: 'auto',
			modal		: true,
			position	: 'center',
			buttons: {
				"Yes" : function() {
					if(status == 2){
						add_innovator_ontology(innovatorID);
					}
					$("#approval-form").submit();
				},
				"No" : function(){
					$("#approval-note").dialog('close');
				}
			}
		});
		$("#approval-note").dialog('open');
	});
	
	//view approval note
	$(".view-note").click(function(e){
		e.preventDefault();
		$("#note-viewer").html($(this).prev("input[name=approval_note]").val());
		$("#note-viewer").dialog({
			autoOpen	: false,
			resizable	: false,
			width		: 350,
			height		: 'auto',
			position	: 'center',
			buttons: {
				"OK" : function(){
					$(this).dialog('close');
				}
			}
		});
		$("#note-viewer").dialog('open');
	});
	
	//change password in edit innovator form
	$('#innovator-account').on('click','#edit-pass-link', function(e){
		e.preventDefault();
		$('#password-wrapper').show();
	});

	initGeoLocation();
});


//save innovator to database and edit in ontology
function save_innovator($form){
	progress_bar('open');
	$form.ajaxSubmit({ 
		dataType	: 'json',
		success		: function(response) {
			if(response){
				if($("#innovator_status").val() == 2){ //save approved innovator only
					edit_innovator_ontology($('#innovator_id').val(),response.res_url);
				}else{
					window.location = response.res_url; 
				}
			}
		}
	});
}

function edit_innovator_ontology(innovatorID, resUrl){
	$.post(base_url+"semantics/get_innovator_semantic/"+innovatorID, function(detail) {
		progress_bar('open');
		$.ajax({ 
			url: semantic_url+"editInnovatorPost", 
			crossDomain : true,
			type: 'POST', 
			dataType: 'json', 
			data: detail,
			contentType: 'application/json',
			mimeType: 'application/json',
			success: function(data) {
				edit_innovator_innovation_ontology(innovatorID, resUrl);
				console.log("Innovator_id "+innovatorID+" status (success): "+data);
			},
			error:function(data,status,er) { 
				console.log("Innovator_id "+innovatorID+"error: "+data+" status: "+status+" er:"+er);
			}
		});
	});
}

function edit_innovator_innovation_ontology(innovator_id, res_url){
	$.post(base_url+"semantics/get_innovation_innovator_semantic",{ innovator_id: innovator_id, innovation_id: "" }, function(data) {
		for(var i=0; i<data.length; i++){
			var innovatorID = data[i].innovator_id;
			var innovationID = data[i].innovation_id;

			//edit innovator innovation
			get_data(semantic_url+"editInnovatorInnovation?innovatorID="+innovatorID+"&innovationID="+innovationID, function(result){
				if(result != 'not_found'){
					console.log("add innovation("+innovationID+") innovator_id("+innovatorID+") success");
					window.location = res_url;
				}else{
					console.log("add innovation("+innovationID+") innovator_id("+innovatorID+") error");
					window.location = res_url;
				}
			});
		}
	},'json');
	progress_bar('close');
}

/*send innovator to ontology*/
function add_innovator_ontology(innovatorID){
	$.post(base_url+"semantics/get_innovator_semantic/"+innovatorID, function(detail) {
		progress_bar('open');
		$.ajax({ 
			url: semantic_url+"addInnovatorPost", 
			crossDomain : true,
			type: 'POST', 
			dataType: 'json', 
			data: detail,
			contentType: 'application/json',
			mimeType: 'application/json',
			success: function(data) {
				add_innovator_innovation_ontology(innovatorID);
				console.log("Innovator_id "+innovatorID+" status (success): "+data);
			},
			error:function(data,status,er) { 
				console.log("Innovator_id "+innovatorID+"error: "+data+" status: "+status+" er:"+er);
			}
		});
		progress_bar('close');
	});
}

function add_innovator_innovation_ontology(innovator_id){
	$.post(base_url+"semantics/get_innovation_innovator_semantic",{ innovator_id: innovator_id, innovation_id: "" }, function(data) {
		for(var i=0; i<data.length; i++){
			var innovatorID = data[i].innovator_id;
			var innovationID = data[i].innovation_id;
			//add innovation
			progress_bar('open');
			$.post(base_url+"semantics/get_innovation_semantic/"+innovationID, function(detail) {
				$.ajax({ 
					url: semantic_url+"addInnovationPost", 
					crossDomain : true,
					type: 'POST', 
					dataType: 'json', 
					data: detail,
					contentType: 'application/json',
					mimeType: 'application/json',
					success: function(data) {
						console.log("Innovation id "+innovationID+" status (success): "+data);
					},
					error:function(data,status,er) { 
						console.log("innovation id "+innovationID+"error: "+data+" status: "+status+" er:"+er);
					}
				});
			});
			progress_bar('close');

			//add innovator innovation
			get_data(semantic_url+"addInnovatorInnovation?innovatorID="+innovatorID+"&innovationID="+innovationID, function(result){
				if(result != 'not_found'){
					console.log("add innovation("+innovationID+") innovator_id("+innovatorID+") success");
				}else{
					console.log("add innovation("+innovationID+") innovator_id("+innovatorID+") error");
				}
			});
			/*$.ajax({ 
				url: semantic_url+"addInnovatorInnovation?innovatorID="+innovatorID+"&innovationID="+innovationID, 
				crossDomain : true,
				type: 'GET', 
				dataType: 'json',
				contentType: 'application/json',
				mimeType: 'application/json',
				success: function(data) {
					console.log("status success : "+data);
				},
				error:function(data,status,er) { 
					console.log("error: "+data+" status: "+status+" er:"+er);
				}
			});*/
		}
	},'json');
}

/*send innovation data to ontology*/
function add_innovation_ontology(innovationID){
	$.post(base_url+"semantics/get_innovation_semantic/"+innovationID, function(detail) {
		$.ajax({ 
			url: semantic_url+"addInnovationPost", 
			crossDomain : true,
			type: 'POST', 
			dataType: 'json', 
			data: detail,
			contentType: 'application/json',
			mimeType: 'application/json',
			success: function(data) {
				console.log("Innovation id "+innovationID+" status (success): "+data);
			},
			error:function(data,status,er) { 
				console.log("innovation id "+innovationID+"error: "+data+" status: "+status+" er:"+er);
			}
		});
	});
}

function get_data(search_url,callback){
	progress_bar('open');
	$.ajax({
		url			: search_url,
		dataType	: 'jsonp',
		success		: function (result) {		
			progress_bar('close')
			callback(result);
		},
		error: function(xhr, ajaxOptions, thrownError) {	
			progress_bar('close');
			callback("not_found");
		}
	});
}

/*innovator age*/
function ageFromKadNo(kad_no){
	if(kad_no.length == 6){
		var dob	  = new Date("19"+kad_no.substr(0,2)+"/"+kad_no.substr(2,2)+"/"+kad_no.substr(4,2));
		var today = new Date();
		var age   = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
		if(isNaN(age)){
			$("#age").val("");
			$("#age").attr("placeholder","Wrong kad no");
		}else{
			$("#age").val(age);
		}
	}else{
		$("#age").val("");
		$("#age").attr("placeholder","");
	}
}

/*innovator team*/
function innovatorTeam(elem){
	if(elem.val() ==  4){ //if innovator berkumpulan
		$("#innovator-team-wrap").show();
		$("#innovator-name-wrap").hide();
	}else{
		$("#innovator-team-wrap").hide();
		$("#innovator-name-wrap").show();
	}
}

/*Set Progress Bar*/
function progress_bar(act){
	switch(act){
		case 'create' 	: $("#progress-bar").dialog({autoOpen: false, modal:true, width: 250, height: 90}); break;
		case 'open'		: $("#progress-bar").dialog('open'); break;
		case 'close'	: $("#progress-bar").dialog('close'); break;
	}
}

function initGeoLocation(){
	$(".address-geo").blur(function(){
		var address = $('.address-geo').val();
		$.post(base_url+"innovators/get_geo_location", { 'address': address }, function(data) {
			if(data.status == false){
				$("#geo-location").val("");
				$("#geo-location").attr("placeholder",data.message);
			}else{
				$("#geo-location").val(data.latitude+","+data.longitude);
			}
		},"json");
	});
}