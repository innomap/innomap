$(document).ready(function(){	
	$(".datepicker").datepicker({changeMonth: true, changeYear: true, dateFormat: "yy-mm-dd"});
	initInnovationTable();
	initInnovatorTable();
	initUniversityTable();
	initExpertTable();
	initGeoLatitude();
	initGeoLongitude();
	initAdvancedSearchTable();
	
	initPendingInnovationHandler();	
	initAddApprovedInnovationToSemantic();	
	initInnovationNote();
	
	export_button();
});

function initAdvancedSearchTable(){
	if($('#advanced-search-table').length > 0){
		list_data = $('#advanced-search-table').dataTable({
			"bFilter" 		  : true,
			"bJQueryUI" 	  : true,
			"aLengthMenu"	  : [[10,25,50, 100, -1], [10,25,50,100, "All"]],
			"sPaginationType" : "full_numbers",
			"bRetrieve"		  : true,
			"oLanguage"		  : {
				"sLengthMenu"	: "Show _MENU_ data",
				"sInfoFiltered"	: "(Search result _MAX_)"
			}
		});
		
		/*filtering search data list*/
		$('#search-type').change(function(){ 
			list_data.fnFilter($(this).val(),5); 
		});
		$('#search-state').change(function(){ 
			list_data.fnFilter($(this).val(),4); 
		});
		$('#search-category').change(function(){ 
			list_data.fnFilter($(this).val(),2); 
		});
		$('#search-keyword').keypress(function(){
			list_data.fnFilter($(this).val());
		});
		$("#reset-search").click(function(){
			list_data.fnFilter('',5);
			list_data.fnFilter('',4); 
			list_data.fnFilter('',2); 
		});
	}
}

function initInnovationTable() {
	if ($('#list_innovation').length > 0) {
		list_innovation = $('#list_innovation').dataTable({
			"bFilter" : true,
			"bAutoWidth" : false,
			"bJQueryUI" : true,
			"aLengthMenu": [[10,25,50, 100, -1], [10,25,50,100, "All"]],
			"sPaginationType": "full_numbers",
			"bRetrieve": true,
			"oLanguage": {
				"sLengthMenu": "Show _MENU_ data",
				"sZeroRecords": "No record for innovation",
				"sInfoFiltered": "(Search result _MAX_)"
			}
		});
		
		/*filtering innovation list*/
		$('select[name=innovation_state]').change( function(){ 
			list_innovation.fnFilter($(this).val(),6); 
		});
		$('select[name=innovation_category]').change( function(){ 
			list_innovation.fnFilter($(this).val(),4); 
		});
	}
}

function initInnovatorTable() {
	if ($('#list_innovator').length > 0) {
		list_innovation = $('#list_innovator').dataTable({
			"bFilter" : true,
			"bAutoWidth" : false,
			"bJQueryUI" : true,
			"aLengthMenu": [[10,25,50, 100, -1], [10,25,50,100, "All"]],
			"sPaginationType": "full_numbers",
			"bRetrieve": true,
			"oLanguage": {
				"sLengthMenu": "Show _MENU_ data",
				"sZeroRecords": "No record for innovator",
				"sInfoFiltered": "(Search result _MAX_)"
			}
		});
	}
}

function initUniversityTable() {
	if ($('#list_university').length > 0) {
		list_university = $('#list_university').dataTable({
			"bFilter" : true,
			"bAutoWidth" : false,
			"bJQueryUI" : true,
			"aLengthMenu": [[10,25,50, 100, -1], [10,25,50,100, "All"]],
			"sPaginationType": "full_numbers",
			"oLanguage": {
				"sLengthMenu": "Show _MENU_ data",
				"sZeroRecords": "No record for university",
				"sInfoFiltered": "(Search result _MAX_)"
			}
		});

		/*filtering university list*/
		$('select[name=university_state]').change( function(){ 
			list_university.fnFilter($(this).val(),3); 
		});
		$('select[name=university_category]').change( function(){ 
			list_university.fnFilter($(this).val(),4); 
		});
	}
}

function initExpertTable() {
	if ($('#list_expert').length > 0) {
		list_expert = $('#list_expert').dataTable({
			"bFilter" : true,
			"bAutoWidth" : false,
			"bJQueryUI" : true,
			"aLengthMenu": [[10,25,50, 100, -1], [10,25,50,100, "All"]],
			"sPaginationType": "full_numbers",
			"oLanguage": {
				"sLengthMenu": "Show _MENU_ data",
				"sZeroRecords": "No record for subject matter expert",
				"sInfoFiltered": "(Search result _MAX_)"
			}
		});

		/*filtering university list*/
		$('select[name=expert_state]').change( function(){ 
			list_expert.fnFilter($(this).val(),5); 
		});
	}
}

function initGeoLatitude(){
	$("#address-geo").blur(function(){
		$.post(base_url+"site/get_geo_location", { 'address': $(this).val() }, function(data) {
			if(data.status == true){
				$("#geo-latitude-location").val(data.latitude);
			}else{
				$("#geo-latitude-location").attr("placeholder",data.message);
			}
		},"json");
	});
}

function initGeoLongitude(){
	$("#address-geo").blur(function(){
		$.post(base_url+"site/get_geo_location", { 'address': $(this).val() }, function(data) {
			if(data.status == true){
				$("#geo-longitude-location").val(data.longitude);
			}else{
				$("#geo-longitude-location").attr("placeholder",data.message);
			}
		},"json");
	});
}

$(function () {
    $(document).ready(function() {
		checkbox_handler();
		checkbox_gender_handler();
		checkbox_category_handler();
		checkbox_handler_field();
		checkbox_handler_dialog();
		
		$('#innovation_state').click(function(){
			innovation_state();
		});
		$('#innovation_category').click(function(){
			innovation_category();
		});
		$('#innovation_year_category').click(function(){
			innovation_year_category();
		});
		$('#university_state').click(function(){
			university_state();
		});
		$('#university_category').click(function(){
			university_category();
		});
		$('#expert_state').click(function(){
			expert_state();
		});					
		$('#innovator_gender').click(function(){
			innovator_gender();
		});
		$('#innovator_state').click(function(){
			innovator_state();
		});
		$('#innovator_category').click(function(){
			innovator_category();
		});
		$('#innovation_state_category').click(function(){
			innovation_state_category();
		});
		$('#innovation_year_state').click(function(){
			innovation_year_state();
		});
		$('#innovation_year').click(function(){
			innovation_year();
		});
		
		var content_type= $('#content-chart').attr('content_type');
		if(content_type == 'innovation_state'){
			innovation_state();
		}else if(content_type == 'innovation_category'){
			innovation_category();
		}else if(content_type == 'university_state'){
			university_state();
		}else if(content_type == 'university_category'){
			university_category();
		}else if(content_type == 'expert_state'){
			expert_state();
		}else if(content_type == 'innovator_gender'){
			innovator_gender();			
		}else if(content_type == 'innovator_state'){
			innovator_state();
		}else if(content_type == 'innovator_category'){
			innovator_category();
		}else if(content_type == 'innovation_state_category'){
			innovation_state_category();
		}else if(content_type == 'innovation_year_category'){
			innovation_year_category();
		}else if(content_type == 'innovation_year'){
			innovation_year();
		}else if(content_type == 'innovation_year_state'){
			innovation_year_state();
		}
		
    });
});

function checkbox_handler(){
	$('.state-value').first().attr('checked','checked');
	$('.state-value').first().attr('disabled','disabled');
	$('.state-value').click(function(){
		var state_opt = $(this).val();
		if(state_opt != 0){
			if($(this).is(':checked')){
				$('.state-value').first().attr('disabled','disabled');
				$('.state-value').first().removeAttr('checked');
			}else{
				var selected_option_cat = $('.state-value').not(':first').filter(':checked').length;
				if(selected_option_cat == 0){					
					$('.state-value').first().prop('checked', true);
					$('.state-value').first().attr('disabled','disabled');
				}
			}
		}else{
			if($(this).is(':checked')){
				$('.state-value').not(':first').attr('disabled','disabled');
			}else{
				$('.state-value').not(':first').removeAttr('disabled');
			}
		}
	});
}

function checkbox_handler_field(){
	$('.dsb-option').first().attr('checked','checked');
	$('.dsb-option').first().attr('disabled','disabled');
	$('.dsb-option').click(function(){
		var state_opt = $(this).val();
		if(state_opt != ''){
			if($(this).is(':checked')){
				$('.dsb-option').first().attr('disabled','disabled');
				$('.dsb-option').first().removeAttr('checked');
			}else{
				var selected_option_cat = $('.dsb-option').not(':first').filter(':checked').length;
				if(selected_option_cat == 0){					
					$('.dsb-option').first().prop('checked', true);
					$('.dsb-option').first().attr('disabled','disabled');
				}
			}
		}else{
			if($(this).is(':checked')){
				$('.dsb-option').not(':first').attr('disabled','disabled');
			}else{
				$('.dsb-option').not(':first').removeAttr('disabled');
			}
		}
	});
}

function checkbox_handler_dialog(){
	$('.dsb-option-dialog').first().attr('checked','checked');
	$('.dsb-option-dialog').first().attr('disabled','disabled');
	$('.dsb-option-dialog').click(function(){
		var state_opt = $(this).val();
		if(state_opt != ''){
			if($(this).is(':checked')){
				$('.dsb-option-dialog').first().attr('disabled','disabled');
				$('.dsb-option-dialog').first().removeAttr('checked');
			}else{
				var selected_option_cat = $('.dsb-option-dialog').not(':first').filter(':checked').length;
				if(selected_option_cat == 0){					
					$('.dsb-option-dialog').first().prop('checked', true);
					$('.dsb-option-dialog').first().attr('disabled','disabled');
				}
			}
		}else{
			if($(this).is(':checked')){
				$('.dsb-option-dialog').not(':first').attr('disabled','disabled');
			}else{
				$('.dsb-option-dialog').not(':first').removeAttr('disabled');
			}
		}
	});
}

function checkbox_category_handler(){
	$('.category-value').first().attr('checked','checked');
	$('.category-value').first().attr('disabled','disabled');
	$('.category-value').click(function(){
		var cat_opt = $(this).val();
		if(cat_opt != 0){
			if($(this).is(':checked')){
				$('.category-value').first().attr('disabled','disabled');
				$('.category-value').first().removeAttr('checked');
			}else{
				var selected_option_cat = $('.category-value').not(':first').filter(':checked').length;
				if(selected_option_cat == 0){					
					$('.category-value').first().prop('checked', true);
					$('.category-value').first().attr('disabled','disabled');
				}
			}
		}else{
			if($(this).is(':checked')){
				$('.category-value').not(':first').attr('disabled','disabled');
			}else{
				$('.category-value').not(':first').removeAttr('disabled');
			}
		}
	});
}

function checkbox_gender_handler(){
	$('.gender-value').first().attr('checked','checked');
	$('.gender-value').first().attr('disabled','disabled');
	$('.gender-value').click(function(){
		var gender_opt = $(this).val();
		if(gender_opt != 'all'){
			if($(this).is(':checked')){
				$('.gender-value').first().attr('disabled','disabled');
				$('.gender-value').first().removeAttr('checked');
			}else{
				var selected_option_cat = $('.gender-value').not(':first').filter(':checked').length;
				if(selected_option_cat == 0){					
					$('.gender-value').first().prop('checked', true);
					$('.gender-value').first().attr('disabled','disabled');
				}
			}
		}else{
			if($(this).is(':checked')){
				$('.gender-value').not(':first').attr('disabled','disabled');
			}else{
				$('.gender-value').not(':first').removeAttr('disabled');
			}
		}
	});
}

/*Innovation State Report*/
function innovation_state(){
	$('#report-innovation-state').ajaxForm({
		dataType : "json",
		success : function(response){
			if(response.type == 'all'){
				draw_category_type("container_innovation_state",response,"innovation_year_state",'state');
			}else{
				draw_state(response, 'container_innovation_state', 'innovation_state');
			}
			remove_highcharts_copyright();
		}
	}).submit();
}

/*Innovation State Category Report*/
function innovation_state_category(){
$('#report-innovation-state-category').ajaxForm({
				dataType : "json",
				success : function(response){
					draw_state_with_legend(response, 'container_innovation_state_category', 'innovation_state_category');
					remove_highcharts_copyright();
				}
		}).submit();
}

/*Innovation Category Report*/
function innovation_category(){
$('#report-innovation-category').ajaxForm({
				dataType : "json",
				success : function(response){
					if(response.type == 'all'){
						draw_category_type("container_innovation_category",response,"innovation_year_category",'category');
					}else{
						draw_state(response, 'container_innovation_category', 'innovation_category');
					}
					remove_highcharts_copyright();
				}
		}).submit();
}

/*Innovation Year Category Report*/
function innovation_year_category(){
$('#report-innovation-year-category').ajaxForm({
				dataType : "json",
				success : function(response){
					draw_category_type("container_innovation_year_category",response,"innovation_year_category",'category');
					remove_highcharts_copyright();
				}
		}).submit();
}

/*Innovation Year State Report*/
function innovation_year_state(){
$('#report-innovation-year-state').ajaxForm({
				dataType : "json",
				success : function(response){
					draw_category_type("container_innovation_year_state",response,"innovation_year_state",'state');
					remove_highcharts_copyright();
				}
		}).submit();
}

/*Innovator Gender Report*/
function innovator_gender(){
$('#report-innovator-gender').ajaxForm({
				dataType : "json",
				success : function(response){
					draw_pie_chart(response, 'container_innovator_gender','innovator_gender');
					remove_highcharts_copyright();
				}
		}).submit();
}

/*Innovator State Report*/
function innovator_state(){
$('#report-innovator-state').ajaxForm({
				dataType : "json",
				success : function(response){
					draw_state(response, 'container_innovator_state', 'innovator_state');
					remove_highcharts_copyright();
				}
		}).submit();
}

/*Innovator Category Report*/
function innovator_category(){
$('#report-innovator-category').ajaxForm({
				dataType : "json",
				success : function(response){
					draw_state(response, 'container_innovator_category', 'innovator_category');
					remove_highcharts_copyright();
				}
		}).submit();
}

/*University State Report*/
function university_state(){
$('#report-university-state').ajaxForm({
				dataType : "json",
				success : function(response){
					draw_state(response, 'container_university_state', 'university_state');
					remove_highcharts_copyright();
				}
		}).submit();
}

/*University Category Report*/
function university_category(){
$('#report-university-category').ajaxForm({
				dataType : "json",
				success : function(response){					
					draw_state(response, 'container_university_category', 'university_category');
					remove_highcharts_copyright();
				}
		}).submit();
}

/*Expert State Report*/
function expert_state(){
$('#report-expert-state').ajaxForm({
				dataType : "json",
				success : function(response){
					draw_state(response, 'container_expert_state', 'expert_state');
					remove_highcharts_copyright();
				}
		}).submit();
}

/*Innovation by Year Report*/
function innovation_year(){
	$('#report-innovation-year').ajaxForm({
			dataType : "json",
			success : function(response){
				draw_state(response, 'container_innovation_year', 'innovation_year');
				remove_highcharts_copyright();
			}
	}).submit();
}

function draw_state(result, container, content_type){
	var chart;
	var title = $('#graph-title').val(),
		xtitle = $('#graph-x-title').val(),
		ytitle = $('#graph-y-title').val();
	var colors = Highcharts.getOptions().colors,
		categories = result.title,
		name = "Report",
		data = result.value;
		
		/*for(var i=0;i<result.value.length;i++){
			data.push({y: result.value[i]});
		}*/
	
	function setChart(name, categories, data, color) {
		chart.xAxis[0].setCategories(categories, false);
		chart.series[0].remove(false);
		chart.addSeries({
			name: name,
			data: data,
			color: color
		}, false);
		chart.redraw();
	}
	
	//load dialog
	load_dialog();
	
	chart = new Highcharts.Chart({
		chart: {
			renderTo: container,
			type: 'column'
		},
		title: {
			text: title
		},
		xAxis: {
			categories: categories,
			title: {
				text: xtitle
			}
		},
		yAxis: {
			title: {
				text: ytitle
			}
		},
		tooltip: {
			formatter: function() {
				var point = this.point,
					s = this.x +':<b>'+ this.y +'</b><br/>';
				return s;
			}
		},
		legend:{
			enabled :false,
		},
		plotOptions: {
			column: {
				//colorByPoint: true,
				dataLabels: {
					enabled: true,
					color: '#000',
					formatter: function() {
						return this.y;
					}
				},
				point:{
					events:{
						click: function(){
							$('#people-dialog').html("");
							var state_id = this.id;
							var year = this.seri;
							//console.log(state_id);							
							//load progress bar
							progress_bar('create');
							progress_bar('open');
							$.ajax({
								type: "POST",
								dataType: "json",
								url: base_url+"reports/get_person/"+(year ? year : ''),
								data: $.param({
											content_type:content_type,
											id:state_id
										}),
								success:function(response){
									var table = $('.dataTables_custom').dataTable();
									table.fnClearTable();
									table.fnDestroy();
									progress_bar('close');
									var content = "", no=1;
									if(content_type == 'innovation_state' || content_type == 'innovation_year'){
										if(content_type == 'innovation_state'){
											$('#detail_inno_state').val(response[0].state_name);
											$('#filter-year').val(year);
										}else{
											$('#year-range').val(state_id);
										}
										
										for(i=0;i<response.length;i++){
											var category = '';
											for(var a=0;a<response[i].category[0].length;a++){
												if(a == response[i].category[0].length-1){
													category += response[i].category[0][a].category_name;
												}else{
													category += response[i].category[0][a].category_name+", ";
												}
											}
											var innovator = '';
											for(var a=0;a<response[i].innovator[0].length;a++){
												if(a == response[i].innovator[0].length-1){
													innovator += response[i].innovator[0][a].name;
												}else{
													innovator += response[i].innovator[0][a].name+", ";
												}
											}											
											content += "<tr><td>"+no+"</td><td>"+response[i].innovation+"</td><td>"+innovator+"</td><td>"+response[i].product_description+"</td><td>"+category+"</td><td>"+response[i].address+"</td><td>"+response[i].state_name+"</td></tr>";
										no++;}
									}else if(content_type == 'innovation_category'){
										$('#detail_inno_cat').val(state_id);
										$('#filter-year').val(year);
										for(i=0;i<response.length;i++){
											var category = '';
											for(var a=0;a<response[i].category[0].length;a++){
												if(a == response[i].category[0].length-1){
													category += response[i].category[0][a].category_name;													
												}else{
													category += response[i].category[0][a].category_name+", ";
												}
											}
											var innovator = '';
											for(var a=0;a<response[i].innovator[0].length;a++){
												if(a == response[i].innovator[0].length-1){
													innovator += response[i].innovator[0][a].name;
												}else{
													innovator += response[i].innovator[0][a].name+", ";
												}
											}
											content += "<tr><td>"+no+"</td><td>"+response[i].innovation+"</td><td>"+innovator+"</td><td>"+response[i].product_description+"</td><td>"+category+"</td><td>"+response[i].address+"</td><td>"+response[i].state_name+"</td></tr>";
										no++;}
									}else if(content_type == 'university_state'){
										$('input[name=university_state_id]').val(state_id);
										for(i=0;i<response.length;i++){
											content += "<tr><td>"+no+"</td><td>"+response[i].university_name+"</td><td>"+response[i].address+"</td><td>"+response[i].state_name+"</td><td class=univ-cat>"+response[i].category_name+"</td><td>"+response[i].contact_no+"</td></tr>";
										no++;}
									}else if(content_type == 'university_category'){
										$('#univ_category').val(state_id);
										for(i=0;i<response.length;i++){
											content += "<tr><td>"+no+"</td><td>"+response[i].university_name+"</td><td>"+response[i].address+"</td><td>"+response[i].state_name+"</td><td>"+response[i].category_name+"</td><td>"+response[i].contact_no+"</td></tr>";
										no++;}
									}else if(content_type == 'expert_state'){
										$('input[name=expert_state]').val(response[0].state_name);
										for(i=0;i<response.length;i++){
											content += "<tr><td>"+no+"</td><td>"+response[i].expert_name+"</td><td>"+response[i].expertise+"</td><td>"+response[i].experience+"</td><td>"+response[i].address+"</td><td>"+response[i].state_name+"</td></tr>";
										no++;}
									}else if(content_type == 'innovator_state'){
										$('#h_innovator_state').val(state_id);
										for(i=0;i<response.length;i++){
											content += "<tr><td>"+no+"</td><td>"+response[i].name+"</td><td>"+response[i].age+"</td><td>"+response[i].gender+"</td><td>"+response[i].race+"</td><td>"+response[i].telp_no+"</td><td>"+response[i].address+"</td></tr>";
										no++;}
									}else if(content_type == 'innovator_category'){
										$('#h_innovator_category').val(state_id);
										for(i=0;i<response.length;i++){
											content += "<tr><td>"+no+"</td><td>"+response[i].name+"</td><td>"+response[i].age+"</td><td>"+response[i].gender+"</td><td>"+response[i].race+"</td><td>"+response[i].telp_no+"</td><td>"+response[i].address+"</td></tr>";
										no++;}
									}

									$('#report-dialog').find('table').css('width','100%');
									$('#people-dialog').append(content);
									//remove 'null' in kategori field universiti
									$('.univ-cat').each(function(j){
										if($(this).text() == 'null'){
											$(this).text('-');
										}
									});
									$(".dataTables_custom").dataTable({
										"bJQueryUI": true,
										"sPaginationType": "full_numbers",
										"iDisplayLength": 20,
										"aLengthMenu": [[10,20,50, 100, -1], [10,20,50,100, "All"]],
									});
									$('.ui-dialog').find('.ui-icon').css({'left':0,'top':0});
									$('.dataTables_wrapper').addClass('delete-before');
									$("#report-dialog").dialog('open');
								}
							});
						}
					}
				} 
			}			
		},
		series: [{
			name: name,
			data: data
		}],
		exporting: {
			enabled: true
		}
	});
}

function draw_category_type(container,result,type,data_type){
	var title = $('#graph-title').val(),
		xtitle = $('#graph-x-title').val(),
		ytitle = $('#graph-y-title').val();
	var content_type = type;
	var data = '';
	if(data_type == 'category'){
		data = result.category;
	}else if(data_type == 'state'){
		data = result.state;
	}
	//load dialog
	load_dialog();
		
	 chart = new Highcharts.Chart({
		chart: {
			renderTo: container,
			type: 'column'
		},
		title: {
			text: title
		},
		xAxis: {
			categories: result.title,
			title: {
				text: xtitle
			},
			labels: {
				style: {
					"font-size" : "11px"
				}
			},
		},
		yAxis: {
			min: 0,
			title: {
				text: ytitle
			},
		},
		exporting: {
			enabled: true,
		},
		legend: {
			layout: 'horizontal',
			backgroundColor: '#FFFFFF',
			align: 'left',
			verticalAlign: 'bottom',
			x: 30,
			y: 14,
			floating: true,
			shadow: true,
		},
		/*tooltip: {
			formatter: function() {
				return ''+
					this.x +': '+ this.y;
			}
		},*/
		plotOptions: {
			column: {
				dataLabels: {
					enabled: true,
					color: '#000',
					formatter: function() {
						return this.y;
					}
				},
				point:{
					events:{
						click: function(){
							$('#people-dialog').html("");
							var state_id = this.id;
							var year = this.seri;
							progress_bar('create');
							progress_bar('open');
							$.ajax({
								type: "POST",
								dataType: "json",
								url: base_url+"reports/get_person/"+this.seri,
								data: $.param({
											content_type:content_type,
											id:state_id
										}),
								success:function(response){
								var table = $('.dataTables_custom').dataTable();
								table.fnClearTable();
								table.fnDestroy();
								progress_bar('close');
								var content = "", no=1;

								if(content_type == 'innovation_year_category'){
									$('#detail_inno_cat').val(state_id);
									$('#filter-year').val(year);
									$('#report-type').val(content_type);
									for(i=0;i<response.length;i++){
										var category = '';
										for(var a=0;a<response[i].category[0].length;a++){
											if(a == response[i].category[0].length-1){
												category += response[i].category[0][a].category_name;													
											}else{
												category += response[i].category[0][a].category_name+", ";
											}
										}
										var innovator = '';
										for(var a=0;a<response[i].innovator[0].length;a++){
											if(a == response[i].innovator[0].length-1){
												innovator += response[i].innovator[0][a].name;
											}else{
												innovator += response[i].innovator[0][a].name+", ";
											}
										}
										content += "<tr><td>"+no+"</td><td>"+response[i].innovation+"</td><td>"+innovator+"</td><td>"+response[i].product_description+"</td><td>"+category+"</td><td>"+response[i].address+"</td><td>"+response[i].state_name+"</td></tr>";
									no++;}
								}else if(content_type == 'innovation_year_state'){
									$('#detail_inno_state').val(response[0].state_name);
									$('#filter-year').val(year);
									$('#report-type').val(content_type);
									for(i=0;i<response.length;i++){
										var category = '';
										for(var a=0;a<response[i].category[0].length;a++){
											if(a == response[i].category[0].length-1){
												category += response[i].category[0][a].category_name;													
											}else{
												category += response[i].category[0][a].category_name+", ";
											}
										}
										var innovator = '';
										for(var a=0;a<response[i].innovator[0].length;a++){
											if(a == response[i].innovator[0].length-1){
												innovator += response[i].innovator[0][a].name;
											}else{
												innovator += response[i].innovator[0][a].name+", ";
											}
										}
										content += "<tr><td>"+no+"</td><td>"+response[i].innovation+"</td><td>"+innovator+"</td><td>"+response[i].product_description+"</td><td>"+category+"</td><td>"+response[i].address+"</td><td>"+response[i].state_name+"</td></tr>";
										no++;
									}
								}
								
								$('#report-dialog').find('table').css('width','100%');
								$('#people-dialog').append(content);
								//remove 'null' in kategori field universiti
								$('.univ-cat').each(function(j){
									if($(this).text() == 'null'){
										$(this).text('-');
									}
								});
								$(".dataTables_custom").dataTable({
									"bJQueryUI": true,
									"sPaginationType": "full_numbers",
									"iDisplayLength": 20,
									"aLengthMenu": [[10,20,50, 100, -1], [10,20,50,100, "All"]],
								});
								$('.ui-dialog').find('.ui-icon').css({'left':0,'top':0});
								$('.dataTables_wrapper').addClass('delete-before');
								$("#report-dialog").dialog('open');
								}
							});
						}
					}
				} 
			}
		},
			series: data
	});
}

function remove_highcharts_copyright(){
	$('tspan, .highcharts-container span').each(function(i, item){ 
		if ($(this).text() === 'Highcharts.com')
			$(this).remove();
	});
}

function submitForm(form_id,url,url_export){
       document.getElementById(form_id).action = url_export;
       document.getElementById(form_id).submit();
       document.getElementById(form_id).action = url;
}

function load_dialog(){
	$("#report-dialog").dialog({
		  title: "Detail",
		  autoOpen	: false,
		  resizable: false,
		  width:900,
		  height:500,
		  modal: true,
		  //draggable : false,
		  position: "center",
		   //position: [300,20],
		  buttons: {
			"OK" : function() {
			  $( this ).dialog( "close" );
			}
		  }
	});
}

function draw_state_with_legend(result, container, content_type){
	var chart;
	var title = $('#graph-title').val(),
		xtitle = $('#graph-x-title').val(),
		ytitle = $('#graph-y-title').val();
	var colors = Highcharts.getOptions().colors,
		categories = result.title,
		name = "Report",
		data = result.value;
		
		/*for(var i=0;i<result.value.length;i++){
			data.push({y: result.value[i]});
		}*/

	function setChart(name, categories, data, color) {
		chart.xAxis[0].setCategories(categories, false);
		chart.series[0].remove(false);
		chart.addSeries({
			name: name,
			data: data,
			color: color
		}, false);
		chart.redraw();
	}
	
	//load dialog
	load_dialog();

	chart = new Highcharts.Chart({
		chart: {
				renderTo: container,
                type: 'column'
            },
            title: {
                text: title
            },
            xAxis: {
                categories:categories,
				title: {
					text: xtitle
				}
            },
            yAxis: {
                title: {
					text: ytitle
				}
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0,
					point:{
						events:{
							click: function(){
								$('#people-dialog').html("");
								var state_id = this.category;
								var category_id = this.series.name;
								$.ajax({
									type: "POST",
									dataType: "json",
									url: base_url+"reports/get_person",
									data: $.param({
												content_type:content_type,
												id:state_id,
												category_id: category_id
											}),
									success:function(response){
										var table = $('.dataTables_custom').dataTable();
										table.fnClearTable();
										table.fnDestroy();

										var content = "", no=1;
										if(content_type == 'innovation_state_category'){
											for(i=0;i<response.length;i++){
												content += "<tr><td>"+no+"</td><td>"+response[i].innovation+"</td><td>"+response[i].innovator_name+"</td><td>"+response[i].product_description+"</td><td>"+response[i].category_name+"</td><td>"+response[i].address+"</td><td>"+response[i].state_name+"</td></tr>";
											no++;}
										}
										$('#report-dialog').find('table').css('width','100%');
										$('#people-dialog').append(content);
										$(".dataTables_custom").dataTable({
											"bJQueryUI": true,
											"sPaginationType": "full_numbers",
											"iDisplayLength": 20,
											"aLengthMenu": [[10,20,50, 100, -1], [10,20,50,100, "All"]],
										});
										$('.ui-dialog').find('.ui-icon').css({'left':0,'top':0});
										$('.dataTables_wrapper').addClass('delete-before');
										$("#report-dialog").dialog('open');
									}
								});
							}
						}
					} 
                }
            },
            series:data,
			exporting: {
				enabled: true
			}
	});
}

function initInnovationNote(){	
	$("#approval-note-dialog").dialog({
		autoOpen	: false,
		resizable	: false,
		width		: 300,
		modal		: true,
		position	: 'center',
		buttons: {
			"Close" : function(){
				$(this).dialog('close');
			}
		}
	});
	
	$(".view_innovation_note").live('click',function(){
		var note = $(this).attr('inno_note');
		if(note != ''){
			$(".no_note").hide();
			$(".note_wrap").html(note);
			$(".note_wrap").show();
		}else{
			$(".no_note").show();
			$(".note_wrap").hide();
		}
		$("#approval-note-dialog").dialog('open');
	});
}

function initPendingInnovationHandler(){	
	$(".reject_innovation").live('click',function(){
		$("#approve-innovation-id").val($(this).attr('inno_id'));
		$("#approve-type").val('reject');
		$("#approval-comment-dialog").dialog({
			title		: 'Rejection Note',
			autoOpen	: false,
			resizable	: false,
			width		: 300,
			height		: 330,
			modal		: true,
			position	: 'center',
			buttons: {
				"Reject" : function() {
					$("#approval-comment-form").submit();
				},
				"Cancel" : function(){
					$("#approval-comment-dialog").dialog('close');
				}
			}
		});
		$("#approval-comment-dialog").dialog('open');
	});
}

function initAddApprovedInnovationToSemantic(){		
	$(".aprove_innovation").live('click',function(){
		$("#approve-innovation-id").val($(this).attr('inno_id'));
		$("#approve-type").val('approve');
		$("#approval-comment-dialog").dialog({
			title		: 'Approval Note',
			autoOpen	: false,
			resizable	: false,
			width		: 300,
			height		: 330,
			modal		: true,
			position	: 'center',
			buttons: {
				"Approve" : function() {
					$(this).dialog('close');
					var inno_id = $("#approve-innovation-id").val();
					addInnovation(inno_id,'pending_page');
				},
				"Cancel" : function(){
					$(this).dialog('close');
				}
			}
		});
		$("#approval-comment-dialog").dialog('open');
	});
}

function addInnovation(inno_id,page){
	progress_bar('create');
	$.get(base_url+'site/get_innovation_detail/'+inno_id,function(detail){
		var description = (detail.desc).replace("’","'");
		description = description.replace("“",'"');
		description = description.replace("”",'"');
		description = encodeURIComponent(description).replace(/[!'()]/g, escape).replace(/\*/g, "%2A");
		var address = encodeURIComponent(detail.address).replace(/[!'()]/g, escape).replace(/\*/g, "%2A");
		$.ajax({
			url			: semantic_url+(page=='innovator_page' ? 'sesEditInnovation' : 'sesAddInnovation')+"?id="+detail.id+"&label="+detail.label+"&desc="+description+"&address="+address+"&category="+detail.category+"&innovator="+detail.innovator+"&state="+detail.state+"&image="+detail.image+"&source="+detail.source,
			dataType	: 'jsonp',
			success		: function (result) {
				if(page == 'pending_page'){
					$("#approval-comment-form").submit();
				}else if(page == 'agreement_page'){
					$("#form-agreement").submit();
				}else if(page == 'innovator_page'){
					window.location = base_url+"innovations/edit/maklumat_pencatat/"+inno_id;
				}
			}
		});
	},'json');
}

function progress_bar(param){
	var object = $("body");
	if(param == "close"){
		$("#progress-bar").dialog("close");
	}else if(param == "open"){
		$("#progress-bar").dialog("open");
	}else if(param == "create"){
		object.append("<div id='progress-bar' style='display:none'><center><img src='"+base_url+"assets/img/progress_bar.gif' /></center></div>");
		$("#progress-bar").dialog({title : "Processing...", width : 250, height : 90, modal: true});
	}
}

function initAddMoreCategory(){
	var place 		= $("#form-category-wrapper");
	$.get(base_url+'site/get_all_category',function(category){
		$("#add-more-category").click(function(){
			var all_cat_el 		= $(".category-elem");
			var exist_category 	= all_cat_el.map(function(){ return $(this).val(); }).get();
			var place_text 		= "";
			//all_cat_el.attr('disabled',true);
			place_text += '<select style="margin-bottom:5px" class="category-elem" name="category[]">';
			for(var i in category){
				if($.inArray(category[i].category_id,exist_category) == -1){
					place_text += '<option value="'+category[i].category_id+'">'+category[i].category_name+'</option>';
				}
			}
			place_text += '</select>';
			place_text += '<a style="cursor:pointer;margin-left:5px" class="remove-category">(-) Remove</a>';
			place.append(place_text);
		});
	},'json');
}

function initRemoveCategory(){
	$(".remove-category").live('click',function(){
		$(this).prev("select.category-elem").remove();
		$(this).remove();
	});
}

//draw pie chart
function draw_pie_chart(result, container, content_type){
	var chart;
	var title = $('#graph-title').val();
	var colors = result.color,
		name = "Report",
		data = result.data;
		
	function setChart(name, categories, data, color) {
		chart.xAxis[0].setCategories(categories, false);
		chart.series[0].remove(false);
		chart.addSeries({
			data: data,
			color: color
		}, false);
		chart.redraw();
	}
	
	//load dialog
	load_dialog();

	chart = new Highcharts.Chart({
		chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
			renderTo: container,
        },
        title: {
            text: title
        },
		colors:colors,
        tooltip: {
    	    pointFormat: '<b>{point.y}</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
				point: {
                    events: {
                        click: function() {
                            $('#people-dialog').html("");
							var state_id = this.id;
							
							$.ajax({
								type: "POST",
								dataType: "json",
								url: base_url+"reports/get_person",
								data: $.param({
											content_type:content_type,
											id:state_id
										}),
								success:function(response){
									var table = $('.dataTables_custom').dataTable();
									table.fnClearTable();
									table.fnDestroy();
							
									$('#h_innovator_gender').val(state_id);
									var content = "", no=1;
									if(content_type == 'innovator_gender'){
										for(i=0;i<response.length;i++){
											content += "<tr><td>"+no+"</td><td>"+response[i].name+"</td><td>"+response[i].age+"</td><td>"+response[i].gender+"</td><td>"+response[i].race+"</td><td>"+response[i].telp_no+"</td><td>"+response[i].address+"</td></tr>";
										no++;}
									}
									$('#report-dialog').find('table').css('width','100%');
									$('#people-dialog').append(content);
									$(".dataTables_custom").dataTable({
										"bJQueryUI": true,
										"sPaginationType": "full_numbers",
										"iDisplayLength": 20,
										"aLengthMenu": [[10,20,50, 100, -1], [10,20,50,100, "All"]],
									});
									$('.ui-dialog').find('.ui-icon').css({'left':0,'top':0});
									$('.dataTables_wrapper').addClass('delete-before');
									$("#report-dialog").dialog('open');
								}
							});
                        }
                    }
                }
            }
        },
        series: [{
			type: 'pie',
            data:data
        }]
	});
}

function export_button(){
	$('#button-export').click(function(event){
		event.preventDefault();
		var div 	= $('#content-chart'),
			form_id	= div.children('form').attr('id'),
			src_url	= div.children('form').attr('action'),
			go_to_url = $(this).attr('url');
		submitForm(form_id,src_url,go_to_url);
	});
}

function submitForm(form_id,url,add_url){
	document.getElementById(form_id).action = base_url+"reports_export_excel/"+add_url;
	document.getElementById(form_id).submit();
	document.getElementById(form_id).action = url;
}