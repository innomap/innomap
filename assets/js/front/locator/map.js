$(document).ready(function(){
	var content_map = $('.main-map').attr('content_map');
	
	checkbox_handler('.category-value');

	if(content_map == 'state_income'){
		state_income();
		commercial_center();
	}else if(content_map == 'elections'){
		elections();
	}
});

function state_income(){
	 $('#submit-filter-form').click(function(e){
	 		e.preventDefault();
		var style_id = $('#gsp_year').val(); 
		var commercial_center = $('#commercial_center').val();
		var category_id = $(".category-value:checked").map(function(){
			return this.value;
		}).toArray(); 
		get_locator(style_id,category_id, commercial_center);
  	});
	get_locator(2,0,0);
}

function commercial_center(){
 	$('#commercial_filter').change(function(){
 		var visible = $('#commercial_center').css('display');
 		if(visible == "none"){
 			$('#commercial_center').show();
 		}else{
 			$('#commercial_center').hide();
 		}
 	});
 }

function initialize(style_id,data) {
	google.maps.visualRefresh = true;

	var map = new google.maps.Map(document.getElementById('googft-mapCanvas'), {
	  center: new google.maps.LatLng(4.246193821340446, 108.95407646718752),
	  zoom: 6,
	  mapTypeId: google.maps.MapTypeId.ROADMAP
	});
	map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(document.getElementById('googft-legend'));

	layer = new google.maps.FusionTablesLayer({
	  map: map,
	  heatmap: { enabled: false },
	  query: {
	    select: "col6",
	    from: "1fwWmbVK4R6HCuUdKu8dajGH_StYV-6F0YUH9Qx4",
	    where: ""
	  },
	  options: {
	    styleId: style_id,
	    templateId: style_id
	  }
	});

	innovation_marker(map,data);
}

function innovation_marker(map,data){
	var options = {
	      styles : []
	    };
		
	//markers innovation
	if(data.marker.innovation.length > 0){
		for(var i=0; i < data.marker.innovation.length; i++){
			var innovation = data.marker.innovation;
			var category = innovation[i]['category_id'];
			var inovator = innovation[i]['innovator'];
			var picture = innovation[i]['pictures'];
			var innovation_list = innovation[i]['innovation_list'];
	 
		/*	var str_innovator = "";
			  for (var j=0; j < inovator[0].length ; j++) {
			  	str_innovator += inovator[0][j]['name'];
			  	str_innovator += (j == inovator[0].length-1 ? "" : ", ");
			};*/

			var str_picture = "";
			for (var h=0; h < picture[0].length ; h++) {
			  	str_picture += "<img class=pin_pict src="+base_url+"assets/attachment/innovation_picture/"+picture[0][h]['picture']+" width='100px'>";
			};

		/*	var str_picture_inovator = "";
			for (var k=0; k < inovator[0].length ; k++) {
			  	str_picture_inovator += "<img class=pin_pict src="+base_url+"assets/attachment/innovators_photo/"+(inovator[0][k]['photo'] != null ? inovator[0][k]['photo']: "no-photo.jpg")+">";
			};*/

			var str_inno_list = "";
			  for (var l=0; l < innovation_list[0].length ; l++) {
			  	str_inno_list += "<a href='"+base_url+"innovations/view/"+innovation_list[0][l]['innovation_id']+"'>"+innovation_list[0][l]['name']+"</a>";
			  	str_inno_list += (l == innovation_list[0].length-1 ? "" : ", ");
			};

			var geo_location = (innovation[i]['geo_location']).split(",");
			var marker = new MarkerWithLabel({
				map:        map,
				draggable:  false,
				animation:  google.maps.Animation.DROP,
				position:   new google.maps.LatLng(geo_location[0],geo_location[1]),
				icon: base_url+"assets/img/map_marker/"+innovation[i]['pin_url'],
			});

			var info = "<span style='font-weight:bold'>"+$('#label_innovation').val()+"</span><br/>";
			info += $('#label_innovation').val()+": "+innovation[i]['innovation']+"<br/>";
			info += $('#label_innovator').val()+": "+innovation[i]['innovator_name']+"<br/>";
			info += "<img src='"+base_url+"assets/attachment/innovator_picture/"+(innovation[i]['picture'] != "" ? innovation[i]['picture'] : 'default.jpg')+"' width='100px'><br/>";
			info += $('#label_category').val()+": "+innovation[i]['category_name']+"<br/>";
			info += $('#label_address').val()+": "+innovation[i]['address']+"<br/>";
			info += $('#label_state').val()+": "+innovation[i]['state_name']+"<br/>";
			info += str_picture;

	/*		var info = "<span style='font-weight:bold'>Innovator</span><br/>";
			info += "Innovator: "+innovation[i]['innovator_name']+"<br/>";
			info += $('#label_address').val()+": "+innovation[i]['address']+"<br/>";
			info += $('#label_state').val()+": "+innovation[i]['state_name']+"<br/>";
			info += "Innovation : "+str_inno_list;*/
			addInfoWindow(map,marker,info);
		}
	}

	//markers univesity
	if(data.marker.university.length > 0){
		for(var i=0; i < data.marker.university.length; i++){

			var university = data.marker.university,
				latitude = university[i]['latitude'];
				longitude = university[i]['longitude'];
			var marker = new google.maps.Marker({
				map:        map,
				draggable:  false,
				animation:  google.maps.Animation.DROP,
				position:   new google.maps.LatLng(latitude,longitude),
				 icon: base_url+"assets/img/map_marker/red-tri.png",
			});
			var info = "<span style='font-weight:bold'>"+$('#label_university').val()+"</span><br/>";
			info += $('#label_university').val()+": "+university[i]['university_name']+"<br/>";
			info += $('#label_address').val()+": "+university[i]['address']+"<br/>";
			info += $('#label_state').val()+": "+university[i]['state_name']+"<br/>";
			info += $('#label_contact_no').val()+": "+university[i]['contact_no']+"<br/>";
			addInfoWindow(map,marker,info);
		}
	}

	//markers expert
	if(data.marker.expert.length > 0){
		for(var i=0; i < data.marker.expert.length; i++){
			var expert = data.marker.expert;
			var geo_location = (expert[i]['geo_location']).split(",");
			var marker = new google.maps.Marker({
				map:        map,
				draggable:  false,
				animation:  google.maps.Animation.DROP,
				position:   new google.maps.LatLng(geo_location[0],geo_location[1]),
				 icon: base_url+"assets/img/map_marker/square-yellow.png",
			});

			var info = "<span style='font-weight:bold'>"+$('#label_expert').val()+"</span><br/>";
			info += $('#label_expert_name').val()+": "+expert[i]['name']+"<br/>";
			info += $('#label_address').val()+": "+expert[i]['address']+"<br/>";
			info += $('#label_state').val()+": "+expert[i]['state_name']+"<br/>";
			info += $('#label_expertise').val()+": "+expert[i]['category_name']+"<br/>";
			//info += $('#label_experience').val()+": "+expert[i]['experience']+"<br/>";
			addInfoWindow(map,marker,info);
		}
	}

	//markers utc
	if(data.marker.utc){
		for(var i=0; i < data.marker.utc.length; i++){
			var utc = data.marker.utc;
			var geo = utc[i]['geo_location'].split(",");
			if(utc[i]['type'] == 1){ var type_name = "UTC";}else{var type_name="RTC";}
			var marker = new google.maps.Marker({
				map:        map,
				draggable:  false,
				animation:  google.maps.Animation.DROP,
				position:   new google.maps.LatLng(geo[0],geo[1]),
				icon: base_url+"assets/img/map_marker/utc.png",
			});
			var info = "<span style='font-weight:bold'>"+$('#label_commercial_center').val()+"</span><br/>";
			info += $('#label_transcenter_name').val()+": "+utc[i]['name']+"<br/>";
			info += $('#label_transcenter_type').val()+": "+type_name+"<br/>";
			info += $('#label_address').val()+": "+utc[i]['address']+"<br/>";
			info += $('#label_state').val()+": "+utc[i]['state_name']+"<br/>";
			info += $('#label_contact_no').val()+": "+utc[i]['telp']+"<br/>";
			addInfoWindow(map,marker,info);
		}
	}

	//markers rtc
	if(data.marker.rtc){
		for(var i=0; i < data.marker.rtc.length; i++){
			var rtc = data.marker.rtc;
			var geo = rtc[i]['geo_location'].split(",");
			if(rtc[i]['type'] == 1){ var type_name = "UTC";}else{var type_name="RTC";}
			var marker = new google.maps.Marker({
				map:        map,
				draggable:  false,
				animation:  google.maps.Animation.DROP,
				position:   new google.maps.LatLng(geo[0],geo[1]),
				icon: base_url+"assets/img/map_marker/rtc.png",
			});
			var info = "<span style='font-weight:bold'>"+$('#label_commercial_center').val()+"</span><br/>";
			info += $('#label_transcenter_name').val()+": "+rtc[i]['name']+"<br/>";
			info += $('#label_transcenter_type').val()+": "+type_name+"<br/>";
			info += $('#label_address').val()+": "+rtc[i]['address']+"<br/>";
			info += $('#label_state').val()+": "+rtc[i]['state_name']+"<br/>";
			info += $('#label_contact_no').val()+": "+rtc[i]['telp']+"<br/>";
			addInfoWindow(map,marker,info);
		}
	}
}

function addInfoWindow(map,marker, message) {
	var info = message;

	var infoWindow = new google.maps.InfoWindow({
		content: message
	});

	google.maps.event.addListener(marker, 'click', function () {
		infoWindow.open(map, marker);
	});
}

function get_locator(style_id,category_id,commercial_center){
  $.ajax({
	  type: "POST",
	  url: base_url+"locators/get_data_state_income/",
	  data: {"category_id":category_id, "commercial_center":commercial_center},
	  success: function(data){
		initialize(style_id,data);
	  },
	  dataType: "json"
	});
}

function elections(){
	var current_style_id = $('#political_party').val();

  	$('#submit-elections-form').on('click',function(){
		var style_id = $('#political_party').val();

	    $('.legend-box').each(function(i){
	    	if(style_id != 5){
	    		$('.legend-column.percentage').show();
	    		$('.legend-column.all').hide();
	    	}else{
	    		$('.legend-column.percentage').hide();
	    		$('.legend-column.all').show();
	    	}
	    	$(this).removeClass('style'+current_style_id);
	        $(this).addClass('style'+style_id);
	    });

		var category_id = $(".category-value:checked").map(function(){
			return this.value;
		}).toArray(); 

		var state_id = $('#state').val();

		get_elections_locator(style_id,category_id,state_id);
	    current_style_id = $('#political_party').val();
	});

  	$('#toggle-select-cat').on('click',function(){
		toggle_category_opt();
	});
	get_elections_locator(current_style_id,0,10);
}

function initialize_elections(style_id,data,state_id) {
	var sort_by = $('#sort_by').val();

    google.maps.visualRefresh = true;

    var stateArray = [],
    	latlong,
    	zoom,
		states = $('#state').find('option');

		for (var i = 0; i < states.length; i++) {
			stateArray[states.eq(i).attr('value')] = states.eq(i).attr('latlong');
		}

		if(state_id != 0){
			latlong = stateArray[state_id].split(",");
			zoom = 8;
		}else{
			var latlong_str = "4.246193821340446,108.95407646718752";
			latlong = latlong_str.split(",");
			zoom = 6;
		}
	
    var map = new google.maps.Map(document.getElementById('elections-googft-mapCanvas'), {
      center: new google.maps.LatLng(latlong[0],latlong[1]),
      zoom: zoom,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(document.getElementById('googft-legend'));

    layer = new google.maps.FusionTablesLayer({
      map: map,
      heatmap: { enabled: false },
      query: {
        select: "col6",
        from: "1k5K4rCfbR6USFbBmpoPKBhqwN99HBSA7LMLov5Q",
        where: "type = '"+sort_by+"'"
      },
      options: {
        styleId: style_id,
        templateId: style_id
      },
      suppressInfoWindows: true
    });
    election_reg_marker(map,data);
    var toggled = $('#toggle-select-cat').is(':checked');
    if(toggled){
    	innovation_marker(map,data);
	}
}

function get_elections_locator(style_id,category_id,state_id){
	var sort_by = $('#sort_by').val();
	$.ajax({
	  type: "POST",
	  url: base_url+"locators/get_data_state_income/",
	  data: {
	  		"category_id":category_id,
	  		"state_id":state_id,
	  		"election":"true",
	  		"sort_by": sort_by},
	  success: function(data){
		initialize_elections(style_id,data,state_id);
	  },
	  dataType: "json"
	});
}

function toggle_category_opt(){
	var toggled = $('#toggle-select-cat').is(':checked');
	if(toggled){
		$('#category-wrap').show();
	}else{
		$('#category-wrap').hide();
	}
}

function election_reg_marker(map,data){
	for(var i=0; i < data.marker.election.length; i++){
		var election = data.marker.election;
		var geo_location = (election[i]['geo_location']).split(",");

		var marker1 = new MarkerWithLabel({
		   icon: base_url+"assets/img/map_marker/party/"+election[i]['winner_logo'],
	       position: new google.maps.LatLng(geo_location[0],geo_location[1]),
	       draggable: false,
	       map: map,
	       title: election[i]['winner_party'],
	       labelContent: election[i]['region_kode'],
	       labelAnchor: new google.maps.Point(22, 0),
	       labelClass: "labels", // the CSS class for the label
	       labelStyle: {opacity: 0.75}
	     });
	}	
}

function checkbox_handler(elem){
	$(elem).first().attr('checked','checked');
	$(elem).first().attr('disabled','disabled');
	$(elem).click(function(){
		var cat_opt = $(this).val();
		if(cat_opt != 0){
			if($(this).is(':checked')){
				$(elem).first().attr('disabled','disabled');
				$(elem).first().removeAttr('checked');
			}else{
				var selected_option_cat = $(elem).not(':first').filter(':checked').length;
				if(selected_option_cat == 0){					
					$(elem).first().prop('checked', true);
					$(elem).first().attr('disabled','disabled');
				}
			}
		}else{
			if($(this).is(':checked')){
				$(elem).not(':first').attr('disabled','disabled');
			}else{
				$(elem).not(':first').removeAttr('disabled');
			}
		}
	});
}