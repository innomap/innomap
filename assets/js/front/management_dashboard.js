var formInnovationYear = '#form-innovation-year';
var formInnovationCat2015 = '#form-innovation-category-2015';
var formInnovationCat2014 = '#form-innovation-category-2014';
var formInnovationCat2013 = '#form-innovation-category-2013';
var formInnovationState2015 = '#form-innovation-state-2015';
var formInnovationState2014 = '#form-innovation-state-2014';
var formInnovationState2013 = '#form-innovation-state-2013';
var formInnovationEval = '#form-innovation-evaluation';
var formInnovationPortfolio = '#form-innovation-portfolio';
var title;
var dataTableParam = {
				"bJQueryUI": true,
				"sPaginationType": "full_numbers",
				"iDisplayLength": 20,
				"aLengthMenu": [[10,20,50, 100, -1], [10,20,50,100, "All"]],
			}
var CHART_WIDTH = 460, CHART_HEIGHT = 450;

$(document).ready(function(){
	innovation_by_year();
	innovation_by_category_2015();
	innovation_by_category_2014();
	innovation_by_category_2013();
	innovation_by_state_2015();
	innovation_by_state_2014();
	innovation_by_state_2013();
	innovation_evaluation();
	innovation_portfolio();
});

function innovation_by_year(){
	$(formInnovationYear).ajaxForm({
		dataType : "json",
		success : function(result){
			title = {title : $(formInnovationYear).find($('.graph-title')).val(), 
					xtitle : $(formInnovationYear).find($('.graph-x-title')).val(),
					ytitle : $(formInnovationYear).find($('.graph-y-title')).val()};

			draw_bar_chart(result,title,CHART_WIDTH,CHART_HEIGHT,'db-inno-year-container','innovation_year');
		}
	}).submit();
}

function innovation_by_category_2015(){
	$(formInnovationCat2015).ajaxForm({
		dataType : "json",
		success : function(result){
			title = $(formInnovationCat2015).find($('.graph-title')).val();
			if(result.data_exist > 0){
				draw_pie_chart(result,title,CHART_WIDTH,CHART_HEIGHT,'db-inno-cat-2015-container','innovation_category');
			}else{
				$(formInnovationCat2015).parent().hide();
			}
		}
	}).submit();
}

function innovation_by_category_2014(){
	$(formInnovationCat2014).ajaxForm({
		dataType : "json",
		success : function(result){
			title = $(formInnovationCat2014).find($('.graph-title')).val();
			if(result.data_exist > 0){
				draw_pie_chart(result,title,CHART_WIDTH,CHART_HEIGHT,'db-inno-cat-2014-container','innovation_category');
			}else{
				$(formInnovationCat2014).parent().hide();
			}
		}
	}).submit();
}

function innovation_by_category_2013(){
	$(formInnovationCat2013).ajaxForm({
		dataType : "json",
		success : function(result){
			title = $(formInnovationCat2013).find($('.graph-title')).val();
			if(result.data_exist > 0){
				draw_pie_chart(result,title,CHART_WIDTH,CHART_HEIGHT,'db-inno-cat-2013-container','innovation_category');
			}else{
				$(formInnovationCat2013).parent().hide();
			}
		}
	}).submit();
}

function innovation_by_state_2015(){
	$(formInnovationState2015).ajaxForm({
		dataType : "json",
		success : function(result){
			title = $(formInnovationState2015).find($('.graph-title')).val();
			if(result.data_exist > 0){
				draw_pie_chart(result,title,CHART_WIDTH,CHART_HEIGHT,'db-inno-state-2015-container','innovation_state');
			}else{
				$(formInnovationState2015).parent().hide();
			}
		}
	}).submit();
}

function innovation_by_state_2014(){
	$(formInnovationState2014).ajaxForm({
		dataType : "json",
		success : function(result){
			title = $(formInnovationState2014).find($('.graph-title')).val();
			if(result.data_exist > 0){
				draw_pie_chart(result,title,CHART_WIDTH,CHART_HEIGHT,'db-inno-state-2014-container','innovation_state');
			}else{
				$(formInnovationState2014).parent().hide();
			}
		}
	}).submit();
}

function innovation_by_state_2013(){
	$(formInnovationState2013).ajaxForm({
		dataType : "json",
		success : function(result){
			title = $(formInnovationState2013).find($('.graph-title')).val();
			if(result.data_exist > 0){
				draw_pie_chart(result,title,CHART_WIDTH,CHART_HEIGHT,'db-inno-state-2013-container','innovation_state');
			}else{
				$(formInnovationState2013).parent().hide();
			}
		}
	}).submit();
}

function innovation_evaluation(){
	$(formInnovationEval).ajaxForm({
		dataType : "json",
		success : function(result){
			title = {title : $(formInnovationEval).find($('.graph-title')).val(),
					tooltip_title : $(formInnovationEval).find($('.tooltip-title')).val()};

			draw_gauge_speedometer(result,title,CHART_WIDTH,CHART_HEIGHT,'db-inno-evaluation-container','innovation_eval_burndown');
		}
	}).submit();
}

function innovation_portfolio(){
	$(formInnovationPortfolio).ajaxForm({
		dataType : "json",
		success : function(result){
			title = {title : $(formInnovationPortfolio).find($('.graph-title')).val(),
					tooltip_title : $(formInnovationPortfolio).find($('.tooltip-title')).val()};

			draw_gauge_speedometer(result,title,CHART_WIDTH,CHART_HEIGHT,'db-inno-portfolio-container','portfolio_burndown');
		}
	}).submit();
}

function draw_bar_chart(result,title,width,height,renderTo,contentType){
	var chart;
	var colors = Highcharts.getOptions().colors,
		categories = result.title,
		name = "Management Dashboard",
		data = result.value;
	
	function setChart(name, categories, data, color) {
		chart.xAxis[0].setCategories(categories, false);
		chart.series[0].remove(false);
		chart.addSeries({
			name: name,
			data: data,
			color: color
		}, false);
		chart.redraw();
	}
	
	load_dialog(contentType);
	
	chart = new Highcharts.Chart({
		chart: {
			renderTo: renderTo,
			type: 'column',
			width: width,
			height: height,
			backgroundColor:null
		},
		title: {
			text: title.title,
			style: {fontSize:"16px"}
		},
		xAxis: {
			categories: categories,
			title: {
				text: title.xtitle
			}
		},
		yAxis: {
			allowDecimals: false,
			title: {
				text: title.ytitle
			}
		},
		tooltip: {
			formatter: function() {
				var point = this.point,
					s = this.x +':<b>'+ this.y +'</b><br/>';
				return s;
			}
		},
		legend:{
			enabled :false,
		},
		plotOptions: {
			column: {
				dataLabels: {
					enabled: true,
					color: '#000',
					formatter: function() {
						return this.y;
					}
				},
				point:{
					events:{
						click: function(){
							show_detail(this.id,this.seri,contentType);
						}
					}
				} 
			}			
		},
		series: [{
			name: name,
			data: data
		}],
		credits:false
	});
}

/* Pie chart type */
function draw_pie_chart(result, title,width, height, renderTo,contentType){
	var chart;
	var name = "Management Dashboard",
		data = result.data;
		
	function setChart(name, categories, data) {
		chart.xAxis[0].setCategories(categories, false);
		chart.series[0].remove(false);
		chart.addSeries({
			data: data,
		}, false);
		chart.redraw();
	}
	
	load_dialog(contentType);

	chart = new Highcharts.Chart({
		chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
			renderTo: renderTo,
			width: width,
			height: height,
			backgroundColor:null
        },
        title: {
            text: title,
            style: {fontSize:"16px"}
        },
        tooltip: {
    	    pointFormat: '<b>{point.percentage:.0f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true,
				point: {
                    events: {
                        click: function() {
                           show_detail(this.id,result.year,contentType);
                        }
                    }
                },
                style: (Highcharts.theme && Highcharts.theme.contrastTextColor)
            }
        },
        legend:{
        	itemStyle:{fontWeight: 'thin'}
        },
        series: [{
			type: 'pie',
            data: data
        }],
        credits: false
	});
}

function draw_gauge_speedometer(result, title,width, height, renderTo, contentType){
	var chart;
	var name = "Management Dashboard",
		data = result.data,
		plotBandsData = (result.max < 200 ? 200 : result.max);
		
	function setChart(name, categories, data) {
		chart.xAxis[0].setCategories(categories, false);
		chart.series[0].remove(false);
		chart.addSeries({
			data: data,
		}, false);
		chart.redraw();
	}

	load_dialog(contentType);

	chart = new Highcharts.Chart({
		 chart: {
            type: 'gauge',
            plotBackgroundColor: null,
            plotBackgroundImage: null,
            plotBorderWidth: 0,
            plotShadow: false,
            renderTo: renderTo,
            width : width,
            height: height,
            backgroundColor:null
        },

        title: {
            text: title.title,
            style: {fontSize:"16px"}
        },

        pane: {
            startAngle: -150,
            endAngle: 150,
            background: [{
                backgroundColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                    stops: [
                        [0, '#FFF'],
                        [1, '#333']
                    ]
                },
                borderWidth: 0,
                outerRadius: '109%'
            }, {
                backgroundColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                    stops: [
                        [0, '#333'],
                        [1, '#FFF']
                    ]
                },
                borderWidth: 1,
                outerRadius: '107%'
            }, {
                // default background
            }, {
                backgroundColor: '#DDD',
                borderWidth: 0,
                outerRadius: '105%',
                innerRadius: '103%'
            }]
        },

        // the value axis
        yAxis: {
            min: result.min,
            max: result.max,

            minorTickInterval: 'auto',
            minorTickWidth: 1,
            minorTickLength: 10,
            minorTickPosition: 'inside',
            minorTickColor: '#666',

            tickPixelInterval: 30,
            tickWidth: 2,
            tickPosition: 'inside',
            tickLength: 10,
            tickColor: '#666',
            labels: {
                step: 2,
                rotation: 'auto'
            },
            plotBands: [{
                from: 0,
                to: result.max*0.6,
                color: '#55BF3B' // green
            }, {
                from: result.max*0.6,
                to: result.max*0.8,
                color: '#DDDF0D' // yellow
            }, {
                from: result.max*0.8,
                to: result.max,
                color: '#DF5353' // red
            }]
        },

        series: [{
            name: title.tooltip_title,
            data: result.data,
            tooltip: {
                valueSuffix: ' dari '+result.max+' '+title.tooltip_title
            }
        }],
        credits: false,
        plotOptions: {
            gauge: {
				point: {
                    events: {
                        click: function() {
                        	show_detail(this.id,result.year,contentType);
                        }
                    }
                }
            }
        },
	});
}

function show_detail(data_id,year,content_type){
	$('.content-dialog').html("");
	progress_bar('create');
	progress_bar('open');
	
	$.ajax({
		type		: "POST",
		dataType	: "json",
		url			: base_url+"reports/get_person/"+(typeof(year) == "undefined" ? '' : year),
		data		: $.param({content_type:content_type,id:data_id}),
		success		: function(response){
			var table = $('.'+content_type).dataTable();
				table.fnClearTable();
				table.fnDestroy();
			progress_bar('close');
			var content = "", no=1;
			
			switch(content_type){
				case 'innovation_state':
					for(i=0;i<response.length;i++){
						content += "<tr><td>"+no+"</td><td>"+response[i].name+"</td><td>"+response[i].innovator+"</td><td>"+response[i].state+"</td><td>"+response[i].score_average+"</td><td>"+response[i].discovered_date+"</td><td><a target='_blank' href='"+base_url+"innovations/view/"+response[i].innovation_id+"/0/0'>"+link_view_detail[lang]+"</a></td></tr>";
						no++;
					}
				break;				
				case 'innovation_category':
					for(i=0;i<response.length;i++){
						content += "<tr><td>"+no+"</td><td>"+response[i].name+"</td><td>"+response[i].innovator+"</td><td>"+response[i].category+"</td><td>"+response[i].score_average+"</td><td>"+response[i].discovered_date+"</td><td><a target='_blank' href='"+base_url+"innovations/view/"+response[i].innovation_id+"/0/0'>"+link_view_detail[lang]+"</a></td></tr>";
						no++;
					}
				break;				
				case 'innovation_year':
					for(i=0;i<response.length;i++){
						content += "<tr><td>"+no+"</td><td>"+response[i].name+"</td><td>"+response[i].innovator+"</td><td>"+response[i].discovered_date+"</td><td>"+response[i].score_average+"</td><td><a target='_blank' href='"+base_url+"innovations/view/"+response[i].innovation_id+"/0/0'>"+link_view_detail[lang]+"</a></td></tr>";
						no++;
					}
				break;
				case 'innovation_eval_burndown':
					for(i=0;i<response.length;i++){
						content += "<tr><td>"+no+"</td><td>"+response[i].title+"</td><td>"+response[i].name+"</td><td>"+response[i].expert_name+"</td><td>"+response[i].total+"</td></tr>";
						no++;
					}
				break;	
				case 'portfolio_burndown':
					for(i=0;i<response.length;i++){
						content += "<tr><td>"+no+"</td><td>"+response[i].title+"</td></tr>";
						no++;
					}
				break;		
			}

			$('#'+content_type).find('table').css('width','100%');
			$('#'+content_type).find($('.content-dialog')).append(content);
			$("."+content_type).dataTable(dataTableParam);
			$("#"+content_type).dialog('open');
		}
	});
}

/*Dialog*/
function load_dialog(elemId){
	$('#'+elemId).dialog({
		  title: "Detail",
		  autoOpen	: false,
		  resizable: false,
		  width:900,
		  height:500,
		  modal: true,
		  //draggable : false,
		  position: "center",
		   //position: [300,20],
		  buttons: {
			"OK" : function() {
			  $( this ).dialog( "close" );
			}
		  }
	});
}

/*Progress Bar*/
function progress_bar(param){
	var object = $("body");
	if(param == "close"){
		$("#progress-bar").dialog("close");
	}else if(param == "open"){
		$("#progress-bar").dialog("open");
	}else if(param == "create"){
		object.append("<div id='progress-bar' style='display:none'><center><img src='"+base_url+"assets/img/progress_bar.gif' /></center></div>");
		$("#progress-bar").dialog({title : "Processing...", width : 250, height : 90, modal: true});
	}
}