$(document).ready(function(){
	progress_bar('create');

	initChangeInnovationContent();
});

function initChangeInnovationContent(){
	$('.opt-innovation').on('change',function(e){
		e.preventDefault();

		progress_bar('open');
		var innovation_id = $(this).val();
		$.get(base_url+"management_logs/get_innovators/"+innovation_id, function(response){
			progress_bar('close');

			var innovators = "";
			for(var i=0;i < response.length;i++){
				innovators += '- '+response[i]['name']+'<br/>';
			}
			
			$('.field-innovator').html(innovators);
		},'json');
	});
}

/*Set Progress Bar*/
function progress_bar(act){
	switch(act){
		case 'create' 	: $("#progress-bar").dialog({autoOpen: false, modal:true, width: 250, height: 90}); break;
		case 'open'		: $("#progress-bar").dialog('open'); break;
		case 'close'	: $("#progress-bar").dialog('close'); break;
	}
}