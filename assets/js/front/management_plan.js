$(document).ready(function(){
	progress_bar('create');

	initAddChallenge();
	initAddTarget();
	initDeleteChallenge();
	initDeleteTarget();
	initChangeInnovationContent();
});

function initAddChallenge(){
	$('.add-challenge').on('click', function(e){
		e.preventDefault();

		var no = $('.tbl-challenge').find('tbody').find('tr').length + 1;
		var content = "<tr>";
			content += "<td>"+no+".<input type='hidden' name='challenge_ids[]' value='-1'></td>";
			content += "<td><textarea class='grid_4' name='challenges[]'></textarea></td>";
			content += "<td><textarea class='grid_4' name='mitigations[]'></textarea></td>";
			content += "<td><a href='#' class='delete-challenge'>(-) "+$('#lang-delete').val()+"</a></td>";
			content += "</tr>";

		$('.tbl-challenge').find('tbody').append(content);
		
	});
}

function initAddTarget(){
	$('.add-target').on('click', function(e){
		e.preventDefault();

		var no = $('.tbl-target').find('tbody').find('tr').length + 1;
		var content = "<tr>";
			content += "<td>"+no+". <input type='hidden' name='target_ids[]' value='-1'></td>";
			content += "<td><textarea class='grid_3' name='targets[]''></textarea></td>";
			content += "<td><input class='grid_2 datepicker' type='text' name='deadlines[]'></td>";
			content += "<td><textarea class='grid_3' name='remarks[]'></textarea></td>";
			content += "<td><a href='#' class='delete-target'>(-) "+$('#lang-delete').val()+"</a></td>";
			content += "</tr>";

		$('.tbl-target').find('tbody').append(content);
		$(".datepicker").datepicker({dateFormat: "yy-mm-dd"});
		
	});
}

function initDeleteChallenge(){
	$('.tbl-challenge').on('click','.delete-challenge',function(e){
		e.preventDefault();
		$('.tbl-challenge').append('<input type="hidden" name="deleted_challenges[]" value="'+$(this).data('id')+'">');
		$(this).parent().parent().remove();
	});
}

function initDeleteTarget(){
	$('.tbl-target').on('click','.delete-target',function(e){
		e.preventDefault();
		$('.tbl-target').append('<input type="hidden" name="deleted_targets[]" value="'+$(this).data('id')+'">');
		$(this).parent().parent().remove();
	});
}

function initChangeInnovationContent(){
	$('.opt-innovation').on('change',function(e){
		e.preventDefault();

		progress_bar('open');
		var innovation_id = $(this).val();
		$.get(base_url+"management_plans/get_innovation_data/"+innovation_id, function(response){
			progress_bar('close');
			$('.field-background').text(response.innovation['inspiration_in_melayu']);
			$('.field-innovator').text(response.innovator['name']);
			$('.field-bd').text(response.innovator['birth_date']);
			$('.field-profession').text(response.innovator['d_employment']);
			$('.field-gender').text(response.innovator['gender_label']);
			$('.field-address').text(response.innovator['address']);
			$('.field-mobile-phone').text(response.innovator['d_mobile_phone_no']);
			$('.field-home-phone').text(response.innovator['d_home_phone_no']);
			$('.field-h-mobile-phone').text(response.innovator['h_mobile_phone_no']);
			$('.field-innovation').text(response.innovation['name_in_melayu']);
			$('.field-innovation-desc').text(response.innovation['description_in_melayu']);
			$('.field-innovation-id').text(response.innovation['innovation_id']);

			var content_img = "";
			for(var i=0;i < response.innovation_images.length;i++){
				content_img += '<img src="'+base_url+'assets/attachment/innovation_picture/'+response.innovation_images[i]['picture']+'" class="grid_2 omega margin-bottom">';
			}
			
			$('.field-images').html(content_img);
		},'json');
	});
}

/*Set Progress Bar*/
function progress_bar(act){
	switch(act){
		case 'create' 	: $("#progress-bar").dialog({autoOpen: false, modal:true, width: 250, height: 90}); break;
		case 'open'		: $("#progress-bar").dialog('open'); break;
		case 'close'	: $("#progress-bar").dialog('close'); break;
	}
}