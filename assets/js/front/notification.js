$(document).ready(function(){
	loadMore();
});

function loadMore(){
	var page_num = 0;

	$('.load-more-notif').click(function(e){
		e.preventDefault();

		var elem = $(this);

		page_num++;

		$('.loading').show();

		$.post(base_url+"notifications/load_more/", {'page_number' : page_num}, function(response){
			if(response.content != ""){
				$('.loading').hide();
				$('.list-notification').append(response.content);
			}else{
				$(elem).parent().hide();
			}

			if(response.show_lm == 0){
				$('.loading').hide();
				$(elem).parent().hide();
			}
		},'json');
	});
}