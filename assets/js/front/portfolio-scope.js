$(document).ready(function(){
	$('.scope-criteria-chk').change(function() {
		var parent = $(this).parent().parent();
	    if(this.checked) {
	        parent.find('.scope-criteria-detail').removeClass('display-none');
	    }else{
	    	parent.find('.scope-criteria-detail').addClass('display-none');
	    }
	});

	$('.task-list').on('click', '.add-task', function(e){
		e.preventDefault();
		var parent = $(this).parents('.scope-criteria-parent');
		var criteria_id = parent.find('.scope-criteria-chk').val();
		var added_elem = '<div><input type="text" class="grid_2" name="task_name['+criteria_id+'][]" placeholder="Nama Tugas" required/>' +
						  '<input type="text" readonly class="grid_2 datepicker" name="task_start_date['+criteria_id+'][]" placeholder="Tarikh Mula" required/>' +
						  '<input type="text" readonly class="grid_2 datepicker" name="task_end_date['+criteria_id+'][]" placeholder="Tarikh Tamat" required/>' +
						  '<input type="number" class="grid_1 task-weightage" name="task_weightage['+criteria_id+'][]" placeholder="Wajaran" required max="100" min="0" />% ' +
						  '<a href="#" class="add-task"><img src="'+base_url +'assets/img/add-access-icon.png" width="20"></a>&nbsp;' +
						  '<a href="#" class="delete-task"><img src="'+base_url +'assets/img/no-access-icon.png" width="20"></a></div>';
		$(this).parent().after(added_elem);
	});

	$('.task-list').on('click', '.delete-task', function(e){
		e.preventDefault();
		$(this).parent().remove();
	});

	//check scope weightage is 100
	$('input.scope-weightage').on('keyup', function(){
		validateScopeWeightage();
	});

	//check task weightage is 100
	$('.task-list').on('keyup', '.task-weightage', function(){
		validateTaskWeightage($(this));
	});

	//handle datepicker in dynamic element
	$('body').on('click','.datepicker', function() {
        $(this).datepicker('destroy').datepicker({showOn:'focus'}).focus();
    });

});

function validateScopeWeightage(){
	var sum_weightage = 0;
	$('input.scope-weightage').each(function(index, elem){
		var val = parseFloat($(elem).val()) || 0;
		sum_weightage = sum_weightage + val;
	});

	if(sum_weightage != 100){
		$(".scope-weightage-error").show();
		$("#btn-save-scope").attr('disabled','disabled');
	}else{
		$(".scope-weightage-error").hide();
		if(noErrorText('scope')){
			$("#btn-save-scope").removeAttr('disabled');
		}
	}
}

function validateTaskWeightage(el){
	var sum_task_weightage = 0;
	var parent = el.parent().parent(); //.task-list
	parent.find('input.task-weightage').each(function(index, elem){
		var val = parseFloat($(elem).val()) || 0;
		sum_task_weightage = sum_task_weightage + val;
	});

	if(sum_task_weightage != 100){
		parent.find(".task-weightage-error").show();
		$("#btn-save-scope").attr('disabled','disabled');
	}else{
		parent.find(".task-weightage-error").hide();
		if(noErrorText('task')){
			$("#btn-save-scope").removeAttr('disabled');
		}
	}
}

function noErrorText(cond){
	if(cond == 'scope'){
		validateTaskWeightage($('.task-weightage').eq(0));
	}
	var task_error = $(".task-weightage-error:visible").length;
	var scope_error = $(".scope-weightage-error:visible").length;
	return task_error == 0 && scope_error == 0;
}