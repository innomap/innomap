$(document).ready(function(){	
	progress_bar('create');

	//call update_progress when page loaded
	$(".progress-bar").each(function(){
		var _val = parseFloat($(this).parent().find('.innovation-portfolio-progress').val());
		update_progress_bar($(this), _val);
	});

	//scope progress
	init_scope_progress();
	
	//general progress
	general_progress_pie();
	
	//view uploaded attachment
	$(".view-attachment").click(function(e){
		e.preventDefault();
		var clicker = $(this);
		$.get(base_url+"portfolios/view_uploaded/"+clicker.attr('innovation-id')+"/"+clicker.attr('task-id'), function(detail){
			$("#ua-inno-name").text(clicker.attr('innovation-name'));
			$("#ua-item").text(clicker.attr('item-name'));
			$("#ua-attachment").html(detail.attachment);
			$("#ua-notes").text(detail.notes);
		},'json');
		
		$("#uploaded-attachment").dialog({
			autoOpen	: false,
			resizable	: false,
			width		: 'auto',
			height		: 'auto',
			modal		: true,
			position	: 'center',
			buttons: {
				"OK" : function(){
					$(this).dialog('close');
				}
			}
		});
		$("#uploaded-attachment").dialog('open');
	});
	
	//notes calendar
	$('#calendar').datepicker({
		dateFormat: 'm/d/yy',
		beforeShowDay: function (date) {
			var theday = (date.getMonth()+1) +'/'+ date.getDate()+ '/' + date.getFullYear();
			var i = 0;
			while(i < highlight_dates.length){
				if(highlight_dates[i].date == theday){
					return [true, "special-date" + (highlight_dates[i].scope != "many" ? " color-"+highlight_dates[i].scope : ""), ''];
				}
				i++;
			}
			return [true, '', ''];
		},
		onSelect: function(dt,inst){ 
			var exist = false;
			for (var i = 0; i < highlight_dates.length; i++) {
				if(highlight_dates[i].date == dt){
					exist = true;
				}
			}
			if(exist){
				var cur_month = inst.selectedMonth + 1;
				var date = inst.selectedYear+'-'+(cur_month < 10 ? '0' : '')+cur_month+'-'+(inst.selectedDay < 10 ? '0' : '')+inst.selectedDay;
				show_notes(date);							
				show_tasks(date);
			}else{
				$(".task-updates").html("<p class='red-text'>"+info_no_note[lang]+"</p>");
			}
		}
	});
	
	//add new note
	$("#add-note").click(function(e){
		e.preventDefault();
		var creation_date = $("#calendar").datepicker('getDate');
		$("#note-form input, #note-form textarea").not('[name=note_portfolio_id]').val("");
		$("#note-form input[name=note_creation_date]").val(creation_date.getFullYear()+'/'+(creation_date.getMonth()+1)+'/'+creation_date.getDate());
		
		note_form_dialog();
	});
	
	//view uploaded note
	$('.task-updates').on('click','p .view-icon', function(e){
		e.preventDefault();
		var note_id = $(this).parent().find(".portfolio-note-id").val();
		$.get(base_url+"portfolios/view_note/"+note_id, function(detail){
			$("#saved-note label").eq(0).html(detail.title);
			$("#saved-note label").eq(1).html(detail.content);
			$("#saved-note label").eq(2).html(detail.attachment);
			$("#saved-note label").eq(3).html(detail.creation_date);
		},'json');
		
		$("#saved-note").dialog({
			autoOpen	: false,
			resizable	: false,
			width		: 'auto',
			height		: 'auto',
			modal		: true,
			position	: 'center',
			buttons: {
				"OK" : function(){
					$(this).dialog('close');
				}
			}
		});
		$("#saved-note").dialog('open');
	});
	
	//edit note
	$('.task-updates').on('click','p .edit', function(e){
		e.preventDefault();
		var note_id = $(this).parent().find(".portfolio-note-id").val();
		$.get(base_url+"portfolios/view_note/"+note_id, function(detail){
			$("#note-form input[name=note_id]").val(detail.portfolio_note_id);
			$("#note-form input[name=note_title]").val(detail.title);
			$("#note-form textarea[name=note_content]").val(detail.content);
			$("#note-form input[name=note_creation_date]").val(detail.creation_date);
		},'json');
		
		note_form_dialog();
	});
	
	//delete note
	$('.task-updates').on('click','p .delete', function(e){
		e.preventDefault();
		if(confirm(msg_confirm_delete[lang]) == false) {
			return false;
		}else{
			var par		= $(this).parent();
			var note_id = par.find(".portfolio-note-id").val();
			$.get(base_url+"portfolios/delete_note/"+note_id, function(res){
				$(".block-content").prepend("<div class='form-info "+(res.success ? 'success' : 'fail')+"'>"+res.msg+"</div><br/>");
				par.remove();
				$("html, body").animate({ scrollTop: 0 });				
			},'json');
		}
	});

	//edit scope
	$('.edit-scope').click(function() {
	  	$(this).parent().find('.item-attachment').toggle('slow');
	});

	//edit attachment
	$('.edit-amount-spend').click(function(e){
		e.preventDefault();
		$(this).parent().find('.amount-spend-form').toggle();
	});

	$(".edit-task").click(function(e){
		e.preventDefault();
		var clicker = $(this);

		$("#edit-ua-attachment").html("");
		$("#inno-name").text(clicker.attr('innovation-name'));
		$("#item-name").text(clicker.attr('item-name'));
		$("#task-name").text(clicker.attr('task-name'));
		$("#task-progress").text(clicker.attr('task-progress'));
		$("input[name=innovation_id]").val(clicker.attr('innovation-id'));
		$("input[name=task_id]").val(clicker.attr('task-id'));
		$("input[name=innovation_portfolio_id]").val(clicker.attr('innovation-portfolio-id'));
		$("input[name=item_num]").val(clicker.attr('item-num'));
		$("textarea[name=note]").val(clicker.attr('task-note'));

		var task_progress = parseInt(clicker.attr('task-progress'));
		$("input[name=task_progress]").val(task_progress);
		if(task_progress >= 50){
			$.get(base_url+"portfolios/view_uploaded/"+clicker.attr('innovation-id')+"/"+clicker.attr('task-id'), function(detail){
				$("#edit-ua-attachment").html(detail.attachment);
			},'json');
		}

		if(task_progress == 100){ //task done
			$("#task-done").val(1);
			$("#task-done").attr('disabled', 'disabled');
			//$("textarea[name=note]").attr('disabled', 'disabled');
		}else{
			$("#task-done").val(0);
			$("#task-done").removeAttr('disabled');
			//$("textarea[name=note]").removeAttr('disabled');
		}
		set_dialog(clicker, task_progress == 100);
		$("#item-attachment").dialog('open');
	});

	//when attachment selected, progress automatically set to 50%
	$("#item-attachment").on('change', '.task-attachment', function(){
		if($(this).val() != "") {
			if(parseInt($("input[name=task_progress]").val()) == 0){
				$("#task-progress").text("50%");
				$("input[name=task_progress]").val("50");
			}
		}
	});

	view_innovation_detail();
	add_attachment();
	delete_attachment();

	//print portfolio scopes to pdf
	var form = $('.detail_portfolio'), 
	    cache_width = form.width(), 
	    a4 = [841.89, 595.28]; // for a4 size paper width and height (lanscape)

	$('#create_pdf').on('click', function() {
		$('body').scrollTop(0);
		$("#create_pdf").hide();
		$(".item-attachment").show();
		createPDF();
		$("#create_pdf").show();
	});

	function createPDF() {
		getCanvas().then(function(canvas) {
			var img = canvas.toDataURL("image/png"),
				doc = new jsPDF({
					//orientation: 'l',
			 		unit: 'px',
			 		format: 'a3'
				});
			doc.addImage(img, 'JPEG', 20, 20);
			doc.save('Portfolio: ' + $("input[name=portfolio_name]").val() +'.pdf');
			form.width(cache_width);
		});
	}

	function getCanvas() {
		form.width((a4[0] * 1.33333) - 80).css('max-width', 'none');
		return html2canvas(form, {
			imageTimeout: 2000,
			removeContainer: true
		});
	}
});

//show notes based on clicked date
function show_notes(date){
	$.post(base_url+"portfolios/get_notes",{date:date},function(notes){
		var notes_str = "";
		for(var i=0;i<notes.length;i++){
			notes_str += '<p>- '+notes[i].title+' <a href="#" class="view-icon"></a>';
			if($('.task-updates').hasClass('check-page')){
				notes_str += '<a href="#" class="edit"></a><a href="#" class="delete"></a>';
			} 
			notes_str += '<input type="hidden" class="portfolio-note-id" value="'+notes[i].portfolio_note_id+'" /></p>';
		}
		$('.task-updates').html(notes_str);
	},'json');
}

//show scope tasks based on clicked date
function show_tasks(date){
	var portfolio_id = $("input[name=portfolio_id]").val();
	$.post(base_url + "portfolios/get_scope_tasks/" + portfolio_id, {date:date},function(tasks){
		var task_str = "";
		for(var i = 0; i < tasks.length ;i++){
			if(i % 2 == 0){
				task_str += '<tr class="even">';
			}else{
				task_str += '<tr class="odd">';
			}			
			task_str += '<td>'+ tasks[i].name +'</td>';
			task_str += '<td>'+ tasks[i].task +'</td>';
			task_str += '<td>'+ tasks[i].weightage +'%</td>';
			task_str += '<td>'+ tasks[i].start_date +'</td>';
			task_str += '<td>'+ tasks[i].end_date +'</td>';
			task_str += '</tr>';
		}
		$('#highlight-date-task').html(task_str);

		//show dialog
		$("#current-tasks").dialog({
			autoOpen	: true,
			resizable	: false,
			width		: 'auto',
			height		: 'auto',
			modal		: true,
			position	: 'center',
			buttons: {
				"OK" : function(){
					$(this).dialog('close');
				}
			}
		});
	},'json');
}

//show note form dialog [add & edit]
function note_form_dialog(){
	if(lang == "melayu"){
		$("#portfolio-note").dialog({
			autoOpen	: false,
			resizable	: false,
			width		: 'auto',
			height		: 'auto',
			modal		: true,
			position	: 'center',
			buttons: {
				"Simpan" : function(){
					$("#note-form").submit();
				},
				"Batal": function(){
					$(this).dialog('close');
				}
			}
		});
	}else{
		$("#portfolio-note").dialog({
			autoOpen	: false,
			resizable	: false,
			width		: 'auto',
			height		: 'auto',
			modal		: true,
			position	: 'center',
			buttons: {
				"Save" : function(){
					$("#note-form").submit();
				},
				"Cancel": function(){
					$(this).dialog('close');
				}
			}
		});
	}
	$("#portfolio-note").dialog('open');
}

//update progress bar
function update_progress_bar(elem, _val){
	elem.find("span").text(_val.toFixed(2)+"%");
	elem.progressbar({
		value: _val 
	});
	var width_bar = elem.find(".ui-progressbar-value").outerWidth();
	elem.find("span").css({'margin-left':width_bar-29+'px'}); //29 = setengah dari width span
}

//dialog when item checkbox clicked
function set_dialog(item_clicker, is_done){
	var buttons = {
		"Close" : 	function(){
						item_clicker.removeAttr('checked');
						$("#item-attachment").dialog('close');
					}
	};

	//if(!is_done){
		buttons["Save"] = function() { 
			//$("#portfolio-attachment").submit(); 
			if($("#portfolio-attachment").valid()) {
				progress_bar('open');
				$("#portfolio-attachment").ajaxSubmit({ 
					dataType	: 'json',
					success		: function(response) {
						if(response){
							window.location = response.res_url; 
						}
					}
				});
			}
		};
	//}
	$("#item-attachment").dialog({
		autoOpen	: false,
		resizable	: false,
		minWidth	: 800,
		width		: 'auto',
		height		: 'auto',
		modal		: true,
		position	: 'center',
		open		: function(event, ui) { 
			$(this).parent().children().children('.ui-dialog-titlebar-close').hide(); //hide close button
		},
		buttons		: buttons
	});
}

function general_progress_pie(){
	var done_progress = 0;
	$(".innovation-portfolio-progress").each(function(){
		done_progress = done_progress + parseFloat($(this).val());
	});
	done_progress = done_progress / parseInt($(".innovation-portfolio-progress").length);
	draw_pie($("#general-progress"), [done_progress, 100-done_progress], 300);
}

//draw pie chart
function draw_pie(container,data,h){
	var chart;

	container.highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
			height: h
        },
        credits: {
            enabled: false
        },
        title: {
            text: ""
        },
		tooltip: {
			formatter: function () {
			   return this.series.name + '<br>' + this.point.name + ': <b>' + Highcharts.numberFormat(this.percentage, 2) + '%</b>';
			}
		},
		plotOptions: {
			pie: {
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					formatter: function() {
                        return Math.round(this.percentage*100)/100 + ' %';
                    },
                    distance: -40,
                    color:'white',
                    style: {
                        fontWeight:'bold',
                        fontSize:'10pt'
                    }
				}
			}
		},
        series: [{
            type: 'pie',
            name: 'Progress',
			data: [{name: 'Done', y: data[0], color: "#75cabf"},
				   {name: 'Not Finished', y: data[1], color: "#f16260"}
			]
		}]
	});
}

function view_innovation_detail(){
	//popup innovation detail
	$('.innovation-name').click(function(e){			
		e.preventDefault();
		var id = $(this).data('id');
		$.get(base_url+"portfolios/view_innovation/"+id, function(response){
			$('#detail-innovation').html(response);
		});

		$("#detail-innovation").dialog({
			autoOpen	: true,
			resizable	: false,
			width		: '800',
			height		: 'auto',
			modal		: true,
			position	: 'center',
			buttons: {
				"OK" : function(){
					$(this).dialog('close');
				}
			}
		});
	});
}

function add_attachment(){
	var addNo = 1;
	$('#item-attachment').on('click','.add-attachment', function(e){
		e.preventDefault();

		var content = "<tr><td></td>";
		content += '<td><input type="file" class="task-attachment" name="attachment_'+addNo+'" required /></td>';
		content += '<td><a href="#" class="delete-attachment">(-) Padam</a></td>';
		content += '</tr>';

		$('.tb-attachment-item').append(content);
		addNo++;
	});

	var editNo = 1;
	$('#edit-criteria-dialog').on('click','.add-attachment', function(e){
		e.preventDefault();

		var content = "<tr><td></td>";
		content += '<td><input type="file" name="attachment_'+editNo+'" /></td>';
		content += '<td><a href="#" class="delete-attachment" data-id="0">(-) Padam</a></td>';
		content += '</tr>';

		$('.tb-edit-item').append(content);
		editNo++;
	});	
}

function delete_attachment(){
	$('#item-attachment').on('click','.delete-attachment', function(e){
		e.preventDefault();

		$(this).closest("tr").remove();
	});

	$('#edit-criteria-dialog').on('click','.delete-attachment', function(e){
		e.preventDefault();

		var id = $(this).data('id');
		if(id > 0){
			var content = "<input type='hidden' name='deleted_attachment[]' value='"+id+"'>";
			$('.tb-edit-item').append(content);
		}
		$(this).closest("tr").remove();
	});
}

function init_scope_progress(){
	$("input[name=scope_progress]").each(function(){
		$(this).parent().find(".scope-progress").text($(this).val());
	});
}

function get_random_color() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}