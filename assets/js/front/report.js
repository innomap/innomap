var CHART_WIDTH = 900, CHART_HEIGHT = 420, CHART_HORIZONTAL_HEIGHT = 900;

$(function () {
    $(document).ready(function() {		
		$('#innovation_state').click(function(){
			innovation_state();
		});
		$('#innovation_category').click(function(){
			innovation_category();
		});
		$('#innovation_year').click(function(){
			innovation_year();
		});
		$('#innovator_state').click(function(){
			innovator_state();
		});
		$('#innovator_category').click(function(){
			innovator_category();
		});
		$('#innovator_gender').click(function(){
			innovator_gender();
		});
		$('#expert_state').click(function(){
			expert_state();
		});
		$('#university_state').click(function(){
			university_state();
		});
		$('#university_category').click(function(){
			university_category();
		});
		
		var content_type= $('#content-chart').attr('content_type');
		if(content_type == 'innovation_state'){
			checkbox_handler('.state-value');
			innovation_state();
		}else if(content_type == 'innovation_category'){
			checkbox_handler('.category-value');
			innovation_category();
		}else if(content_type == 'innovation_year'){
			innovation_year();
		}else if(content_type == 'innovator_state'){
			checkbox_handler('.state-value');
			innovator_state();
		}else if(content_type == 'innovator_category'){
			checkbox_handler('.category-value');
			innovator_category();
		}else if(content_type == 'innovator_gender'){
			checkbox_handler('.gender-value');
			innovator_gender();
		}else if(content_type == 'expert_state'){
			checkbox_handler('.state-value');
			expert_state();
		}else if(content_type == 'university_state'){
			checkbox_handler('.state-value');
			university_state();
		}else if(content_type == 'university_category'){
			checkbox_handler('.category-value');
			university_category();
		}	
    });
	
	$('#button-export').click(function(e){ //change url when button export clicked
		e.preventDefault();
		var form 	= $(this).parents('form');
		change_form_action(form.attr('id'), form.attr('action'), 'export_'+$('#data_type').val());
	});

	//scrollable table in evaluation report
	scrollable_table();
});

/*Innovation State Report*/
function innovation_state(){
	$('#report-innovation-state').ajaxForm({
		dataType : "json",
		success : function(response){
			draw_category_type(response,"innovation_state",true);
		}
	}).submit();
}

/*Innovation Category Report*/
function innovation_category(){
	$('#report-innovation-category').ajaxForm({
		dataType : "json",
		success : function(response){
			draw_category_type(response,"innovation_category",true);
		}
	}).submit();
}

/*Innovation Year Report*/
function innovation_year(){
	$('#report-innovation-year').ajaxForm({
		dataType : "json",
		success : function(response){
			draw_basic_type(response, 'innovation_year');
		}
	}).submit();
}

/*Innovator State Report*/
function innovator_state(){
	$('#report-innovator-state').ajaxForm({
		dataType : "json",
		success : function(response){
			draw_basic_type(response, 'innovator_state');
		}
	}).submit();
}

/*Innovator Category Report*/
function innovator_category(){
	$('#report-innovator-category').ajaxForm({
		dataType : "json",
		success : function(response){
			draw_basic_type(response, 'innovator_category');
		}
	}).submit();
}

/*Innovator Gender Report*/
function innovator_gender(){
	$('#report-innovator-gender').ajaxForm({
		dataType : "json",
		success : function(response){
			draw_pie(response,'innovator_gender');
		}
	}).submit();
}

/*Expert State Report*/
function expert_state(){
	$('#report-expert-state').ajaxForm({
		dataType : "json",
		success : function(response){
			draw_basic_type(response, 'expert_state');
		}
	}).submit();
}

/*University State Report*/
function university_state(){
	$('#report-university-state').ajaxForm({
		dataType : "json",
		success : function(response){
			draw_basic_type(response, 'university_state');
		}
	}).submit();
}

/*University Category Report*/
function university_category(){
	$('#report-university-category').ajaxForm({
		dataType : "json",
		success : function(response){
			draw_basic_type(response, 'university_category');
		}
	}).submit();
}

/*Column chart basic*/
function draw_basic_type(result, content_type){
	var chart;
	var title = $('#graph-title').val(),
		xtitle = $('#graph-x-title').val(),
		ytitle = $('#graph-y-title').val();
	var colors = Highcharts.getOptions().colors,
		categories = result.title,
		name = "Report",
		data = result.value;
	
	function setChart(name, categories, data, color) {
		chart.xAxis[0].setCategories(categories, false);
		chart.series[0].remove(false);
		chart.addSeries({
			name: name,
			data: data,
			color: color
		}, false);
		chart.redraw();
	}
	
	load_dialog();
	
	chart = new Highcharts.Chart({
		chart: {
			renderTo: 'chart-container',
			type: 'column',
			width: CHART_WIDTH,
			height: CHART_HEIGHT
		},
		title: {
			text: title
		},
		xAxis: {
			categories: categories,
			title: {
				text: xtitle
			}
		},
		yAxis: {
			allowDecimals: false,
			title: {
				text: ytitle
			}
		},
		tooltip: {
			formatter: function() {
				var point = this.point,
					s = this.x +':<b>'+ this.y +'</b><br/>';
				return s;
			}
		},
		legend:{
			enabled :false,
		},
		plotOptions: {
			column: {
				//colorByPoint: true,
				dataLabels: {
					enabled: true,
					color: '#000',
					formatter: function() {
						return this.y;
					}
				},
				point:{
					events:{
						click: function(){
							show_column_data(this.id,this.seri,content_type);
						}
					}
				} 
			}			
		},
		series: [{
			name: name,
			data: data
		}],
		credits:false
	});
}

/*Column char with category*/
function draw_category_type(result,type, is_horizontal){
	var title = $('#graph-title').val(),
		xtitle = $('#graph-x-title').val(),
		ytitle = $('#graph-y-title').val();
	var content_type = type;
	load_dialog();
		
	 chart = new Highcharts.Chart({
		chart: {
			renderTo: 'chart-container',
			type: 'column',
			inverted : is_horizontal,
			width: CHART_WIDTH,
			height: (is_horizontal ? CHART_HORIZONTAL_HEIGHT : CHART_HEIGHT)
		},
		title: {
			text: title
		},
		xAxis: {
			categories: result.title,
			title: {
				text: xtitle
			},
			labels: {
				style: {
					"font-size" : "11px"
				}
			},
		},
		yAxis: {
			allowDecimals: false,
			min: 0,
			title: {
				text: ytitle
			},
		},
		legend: {
			layout: 'horizontal',
			backgroundColor: '#FFFFFF',
			align: 'left',
			verticalAlign: 'bottom',
			x: 30,
			y: 14,
			floating: true,
			shadow: true,
		},
		plotOptions: {
			column: {
				dataLabels: {
					enabled: true,
					allowOverlap:true,
					color: '#000',
					formatter: function() {
						return this.y;
					}
				},
				point:{
					events:{
						click: function(){
							show_column_data(this.id,this.seri,content_type);
						}
					}
				} 
			}
		},
		series: result.category,
		credits:false
	});
}

/* Pie chart type */
function draw_pie(result, content_type){
	var chart;
	var title = $('#graph-title').val();
	var name = "Report",
		data = result.data;
		
	function setChart(name, categories, data) {
		chart.xAxis[0].setCategories(categories, false);
		chart.series[0].remove(false);
		chart.addSeries({
			data: data,
		}, false);
		chart.redraw();
	}
	
	//load dialog
	load_dialog();

	chart = new Highcharts.Chart({
		chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
			renderTo: 'chart-container',
        },
        title: {
            text: title
        },
		//colors:colors,
        tooltip: {
    	    pointFormat: '<b>{point.y}</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
				point: {
                    events: {
                        click: function() {
                           show_column_data(this.id,this.seri,content_type);
                        }
                    }
                }
            }
        },
        series: [{
			type: 'pie',
            data: data
        }],
        credits:false
	});
}

/*Show innovator or innovation or university, ect when bar column clicked*/
function show_column_data(data_id,year,content_type){
	$('#people-dialog').html("");
	progress_bar('create');
	progress_bar('open');
	
	$.ajax({
		type		: "POST",
		dataType	: "json",
		url			: base_url+"reports/get_person/"+(typeof(year) == "undefined" ? '' : year),
		data		: $.param({content_type:content_type,id:data_id}),
		success		: function(response){
			var table = $('.dataTables_custom').dataTable();
			table.fnClearTable();
			table.fnDestroy();
			progress_bar('close');
			var content = "", no=1;
			
			switch(content_type){
				case 'innovation_state':
					for(i=0;i<response.length;i++){
						content += "<tr><td>"+no+"</td><td>"+response[i].name+"</td><td>"+response[i].innovator+"</td><td>"+response[i].state+"</td><td>"+response[i].score_average+"</td><td>"+response[i].discovered_date+"</td><td><a target='_blank' href='"+base_url+"innovations/view/"+response[i].innovation_id+"/0/0'>"+link_view_detail[lang]+"</a></td></tr>";
						no++;
					}
				break;				
				case 'innovation_category':
					for(i=0;i<response.length;i++){
						content += "<tr><td>"+no+"</td><td>"+response[i].name+"</td><td>"+response[i].innovator+"</td><td>"+response[i].category+"</td><td>"+response[i].score_average+"</td><td>"+response[i].discovered_date+"</td><td><a target='_blank' href='"+base_url+"innovations/view/"+response[i].innovation_id+"/0/0'>"+link_view_detail[lang]+"</a></td></tr>";
						no++;
					}
				break;				
				case 'innovation_year':
					for(i=0;i<response.length;i++){
						content += "<tr><td>"+no+"</td><td>"+response[i].name+"</td><td>"+response[i].innovator+"</td><td>"+response[i].discovered_date+"</td><td>"+response[i].score_average+"</td><td><a target='_blank' href='"+base_url+"innovations/view/"+response[i].innovation_id+"/0/0'>"+link_view_detail[lang]+"</a></td></tr>";
						no++;
					}
				break;				
				case 'innovator_state':
					for(i=0;i<response.length;i++){
						content += "<tr><td>"+no+"</td><td>"+response[i].name+"</td><td>"+response[i].email+"</td><td>"+response[i].kod_no+"</td><td>"+response[i].address+"</td><td>"+response[i].state+"</td><td><a target='_blank' href='"+base_url+"innovators/view/innovator/"+response[i].innovator_id+"/0'>"+link_view_detail[lang]+"</a></td></tr>";
						no++;
					}
				break;	
				case 'innovator_category':
					for(i=0;i<response.length;i++){
						content += "<tr><td>"+no+"</td><td>"+response[i].name+"</td><td>"+response[i].email+"</td><td>"+response[i].kod_no+"</td><td>"+response[i].address+"</td><td><a target='_blank' href='"+base_url+"innovators/view/innovator/"+response[i].innovator_id+"/0'>"+link_view_detail[lang]+"</a></td></tr>";
						no++;
					}
				break;
				case 'innovator_gender':
					for(i=0;i<response.length;i++){
						content += "<tr><td>"+no+"</td><td>"+response[i].name+"</td><td>"+response[i].email+"</td><td>"+response[i].kod_no+"</td><td>"+response[i].address+", "+response[i].state+"</td><td>"+(response[i].gender == 1 ? value_male[lang] : value_female[lang])+"</td><td><a target='_blank' href='"+base_url+"innovators/view/innovator/"+response[i].innovator_id+"/0'>"+link_view_detail[lang]+"</a></td></tr>";
						no++;
					}
				break;
				case 'expert_state':
					for(i=0;i<response.length;i++){
						content += "<tr><td>"+no+"</td><td>"+response[i].name+"</td><td>"+response[i].email+"</td><td>"+response[i].kad_no+"</td><td>"+response[i].address+"</td><td>"+response[i].state+"</td></tr>";
						no++;
					}
				break;
				case 'university_state':
					for(i=0;i<response.length;i++){
						content += "<tr><td>"+no+"</td><td>"+response[i].name+"</td><td>"+response[i].contact_no+"</td><td>"+response[i].address+"</td><td>"+response[i].state+"</td></tr>";
						no++;
					}
				break;	
				case 'university_category':
					for(i=0;i<response.length;i++){
						content += "<tr><td>"+no+"</td><td>"+response[i].name+"</td><td>"+response[i].contact_no+"</td><td>"+response[i].address+"</td><td>"+response[i].category+"</td></tr>";
						no++;
					}
				break;	
			}
		
			$('#report-dialog').find('table').css('width','100%');
			$('#people-dialog').append(content);
			
			$(".dataTables_custom").dataTable({
				"bJQueryUI": true,
				"sPaginationType": "full_numbers",
				"iDisplayLength": 20,
				"aLengthMenu": [[10,20,50, 100, -1], [10,20,50,100, "All"]],
			});
			$("#report-dialog").dialog('open');
		}
	});
}

/*Dialog*/
function load_dialog(){
	$("#report-dialog").dialog({
		  title: "Detail",
		  autoOpen	: false,
		  resizable: false,
		  width:900,
		  height:500,
		  modal: true,
		  //draggable : false,
		  position: "center",
		   //position: [300,20],
		  buttons: {
			"OK" : function() {
			  $( this ).dialog( "close" );
			}
		  }
	});
}

/*Checkbox handler*/
function checkbox_handler(elem){
	$(elem).first().attr('checked','checked');
	$(elem).first().attr('disabled','disabled');
	$(elem).click(function(){
		var cat_opt = $(this).val();
		if(cat_opt != 0){
			if($(this).is(':checked')){
				$(elem).first().attr('disabled','disabled');
				$(elem).first().removeAttr('checked');
			}else{
				var selected_option_cat = $(elem).not(':first').filter(':checked').length;
				if(selected_option_cat == 0){					
					$(elem).first().prop('checked', true);
					$(elem).first().attr('disabled','disabled');
				}
			}
		}else{
			if($(this).is(':checked')){
				$(elem).not(':first').attr('disabled','disabled');
			}else{
				$(elem).not(':first').removeAttr('disabled');
			}
		}
	});
}

/*Progress Bar*/
function progress_bar(param){
	var object = $("body");
	if(param == "close"){
		$("#progress-bar").dialog("close");
	}else if(param == "open"){
		$("#progress-bar").dialog("open");
	}else if(param == "create"){
		object.append("<div id='progress-bar' style='display:none'><center><img src='"+base_url+"assets/img/progress_bar.gif' /></center></div>");
		$("#progress-bar").dialog({title : "Processing...", width : 250, height : 90, modal: true});
	}
}

/*Change form action when export to excel button clicked*/
function change_form_action(form_id,url,add_url){
	document.getElementById(form_id).action = base_url+"reports/"+add_url;
	document.getElementById(form_id).submit();
	document.getElementById(form_id).action = url;
}

/* Evaluation report - filter */
function show_filter_content(){
	$('input[name=filter_by]').change(function(e){
		e.preventDefault();
		$(".filter").hide();
		$("."+$(this).val()+"_filter").show();
	});
}

function generate_suggest(elem,data){
	var ms = $(elem).magicSuggest({
		allowFreeEntries: false,
	    width: 300,
	    selectionPosition: 'bottom',
	    selectionStacked: true,
	    displayField: 'name',
	    data: data
    });
	
	$(ms).on('selectionchange', function(e, m){
		var values = m.getValue();
		if(values[values.length-1] != -1){
			m.removeFromSelection([{id: -1}], true);
		}else{
			values.splice(values.indexOf(-1),1);
			for(var i=0; i<values.length; i++){
				m.removeFromSelection([{id: values[i]}], true);
			}
		}
	});
}