/*initialize breadcrumbs variable*/
var bc_links = new Array();

$(document).ready(function(){
	progress_bar('create');
	
	set_autocomplete_data({},function(auto_data){
        $("#semsearch-inp").autocomplete({
            source: auto_data,
            select: function(event, ui){
                $("#semsearch-type").val(ui.item.type);
            }
        });
    });
	
	$("#semsearch-form").submit(function(){	
		var type 	= $("#semsearch-type").val();
		var keyword = $("#semsearch-inp").val();
		progress_bar('open');
		
		if(type == "InnovationCategory"){
			category_result(keyword,type);
		}else{
			instance_detail(keyword,type);
		}
		return false;
	});
	
	//show instance detail
	$("#semantic").on('click','.instance-detail',function(e){
		e.preventDefault();
		if($(this).attr('type') == "InnovationCategory"){
			category_result($(this).text(),$(this).attr('type'));
		}else{
			instance_detail($(this).text(),$(this).attr('type'));
		}
	});
	
	//toggle handler
	$("#semantic").find("#detail-toggle").unbind('click').click(function(){
		$("#semsearch-result").show();
		$("#semsearch-graph").hide();
	});
	
	$("#semantic").find("#graph-toggle").unbind('click').click(function(){
		$("#semsearch-result").hide();
		$("#semsearch-graph").show();
	});
});

function show_breadcrumbs(links){
	$("#breadcrumbs").html("");
	var links_txt = "";
	for(var k in links){
		links_txt += '<a href="#" subject_name="'+links[k].subject_name+'" class="instance-detail breadcrumb-link" parent_name="" type="'+links[k].type+'">'+links[k].label+'</a> <br/>';
	}
	$("#breadcrumbs").html(links_txt);
	$("#bread-title").show();
	$("#breadcrumbs").show();
}

//check if already exist in current breadcrumbs
function check_breadcrumb_exist(bc_new_label){
	var bc_exist = false;
	for(var i=0; i<bc_links.length; i++){
		if(bc_links[i].label === bc_new_label)
			bc_exist = true;
	}
	return bc_exist;
}

// Returning ready autocomplete data for semantic search
function set_autocomplete_data(param,callback){
	get_data(semantic_url+'sesGetInstancesWithClass', function(instances){
		progress_bar('close');
		var data = $.map(instances, function(instance){ 
			return {value: instance.label, type: instance.type}; 
		});
		callback(data);
	});
}

function category_result(keyword,type){
	var place 	= $("#semsearch-result");
	var str 	= "";

	$.post(base_url+"semantics/get_semantic_category_id",{category_name : keyword},function(category_id) { //get category id first
		$("#semsearch-graph").html("");
		get_data(semantic_url+'sesGetInstancesDetail'+'?keyword=InnovationCategory_'+category_id+'&type='+type,function(instances){
			progress_bar('close');
			if(instances != 'not_found'){
				str += "<p>Hasil carian semantik untuk kata kunci: '"+keyword+"'</p>";
				str += "<h1>"+semantic_res_title_inno[lang]+"</h1>";
				for(var k in instances){
					if(instances[k].type == "Innovation"){
						str += "<a href='#' class='instance-detail' type='"+instances[k].type+"'>"+instances[k].label+"</a><br/>";
					}
				}
				str += "<h1>"+semantic_res_title_expert[lang]+"</h1>";
				for(var j in instances){
					if(instances[j].type == "Expert"){
						str += "<a href='#' class='instance-detail' type='"+instances[j].type+"'>"+instances[j].label+"</a><br/>";
					}
				}
				
				//graph
				var valid_data = set_graph_data_for_category(instances,{name: keyword, size: 10000, children: new Array, logo: 0},"direct");
				draw_graph("#semsearch-graph",valid_data,960,600);
			}else{
				str += "<br/><i>Tiada hasil pencarian untuk kata kunci: '"+keyword +"'<i>";
			}
			place.html(str);
			
			//breadcrumb - search history
			if(check_breadcrumb_exist(keyword) == false){
				bc_links.push({subject_name: keyword, label: keyword, type: type});
			}
			show_breadcrumbs(bc_links);
		});
	},'json');
}

function instance_detail(keyword,type){
	var place 	= $("#semsearch-result");
	var content = '<hr/>';
	$("#semsearch-graph").html("");
	get_data(semantic_url+'sesGetInstancesDetail'+'?keyword='+keyword+'&type='+type,function(detail){
		progress_bar('close');
		if(detail != 'not_found'){
			content += "<p>Hasil carian semantik untuk kata kunci: '"+keyword+"'</p>";
			for(var key in detail) {
				if(key != "return_type"){
					content += '<div class="label grid_3">'+set_title(key)+':</div>';
					content += '<div class="grid_6">'+set_object(key,detail[key],keyword,type)+'</div><div class="clear"></div><br/>';
				}
			}
		}else{
			content += "<br/><i>Tiada hasil pencarian untuk kata kunci: '"+keyword +"'<i>";
		}
		place.html(content);
		
		//graph
		var valid_data = set_graph_data(detail,{name: keyword, size: 10000, children: new Array, logo: 0},"direct",type);
		draw_graph("#semsearch-graph",valid_data,960,600);
		
		//breadcrumb - search history
		if(check_breadcrumb_exist(keyword) == false){
			bc_links.push({subject_name: keyword, label: keyword, type: type});
		}
		show_breadcrumbs(bc_links);
	});
}

//Return object in detail
function set_object(predicate,object,parent,ins_type){
	var type = set_object_type(predicate);
	if(predicate == 'has picture' || predicate == 'gambar'){
		var images 	   = object.split(', ');
		var image_text = "";
		for(k in images){
			image_text += '<img src="'+base_url+'assets/attachment/'+(ins_type == 'Innovator' ? 'innovator_picture/' : 'innovation_picture/')+images[k]+'" width="100px" style="margin: 0 10px 5px 0">';
		}
		return image_text;
	}else if(instance_predicate(predicate) == true){
		var instances 	= object.split(', ');
		var ins_txt 	= "";
		for(k in instances){
			ins_txt += '<a href="#" subject_name="'+instances[k]+'" class="instance-detail" parent_name="'+parent+'" type="'+type+'">'+instances[k]+'</a>'+(k < eval(instances.length-1) ? ', ':'');
		}
		return ins_txt;
	}else{
		return object;
	}
}

function set_object_type(predicate){
	var type = 'Innovation';
	if(predicate == 'has innovator' || predicate == 'penginovator'){
		type = 'Innovator';
	}else if(predicate == 'has innovation category' || predicate == 'kategori'){
		type = 'InnovationCategory';
	}
	return type;
}

function instance_predicate(predicate){
	var val = false;
	switch(predicate){
		case 'has innovation' 	: val = true; break;
		case 'inovasi' 			: val = true; break;
		//case 'has state' 		: val = true; break;
		//case 'negeri'			: val = true; break;
		case 'has innovation category' 	: val = true; break;
		case 'kategori'			: val = true; break;
		case 'has innovator' 	: val = true; break;
		case 'penginovator'		: val = true; break;
	}
	return val;
}

//ajax with given url
function get_data(search_url,callback){
	progress_bar('open');
	$.ajax({
		url			: search_url,
		dataType	: 'jsonp',
		success		: function (result) {		
			progress_bar('close')
			callback(result);
		},
		error: function(xhr, ajaxOptions, thrownError) {	
			progress_bar('close');
			$("#semsearch-result").html("");
			callback("not_found");
		}
	});
}

//Return ready graph data to draw
function set_graph_data(detail,main_node,data_type,ins_type){
	var graph_data 	= main_node;
	var i			= 0;
	for (var k in detail){
		if(k != "return_type"){
			graph_data.children[i] = {name: k, size: 5000, children: new Array, logo: 0, id: (data_type == 'added' ? set_valid_keyname(graph_data.name)+"_"+set_valid_keyname(k) : "")};
			if(instance_predicate(k) == true || k == 'has picture' || k == 'gambar'){
				var childs = detail[k].split(', ');
				for(var j in childs){
					graph_data.children[i].children[j] 	= {name: childs[j], size: 1983, children: null, clickable: (k == 'has picture' || k == 'gambar' ? false : true), logo: (k == 'has picture' || k == 'gambar' ? (ins_type == "Innovator" ? 'innovator_picture/' : 'innovation_picture/')+childs[j] : 0), id: (data_type == 'added' ? set_valid_keyname(graph_data.name)+"_"+set_valid_keyname(detail[k]) : ""), o_type: set_object_type(k)};		
				}
			}else{
				graph_data.children[i].children[0] 	= {name: detail[k], size: 1983, clickable: false, logo: 0, id: (data_type == 'added' ? set_valid_keyname(graph_data.name)+"_"+set_valid_keyname(detail[k]) : ""), o_type: set_object_type(k)};
			}
			i++;
		}
	}
	return graph_data;
}

function set_graph_data_for_category(detail,main_node,data_type,exception){
	var graph_data 	= main_node;
	var i			= 0;
	for (var k in detail){
		if(k != "return_type" && detail[k].label != exception){
			graph_data.children[i] = {name: "has "+detail[k].type, size: 5000, children: new Array, logo: 0, id: (data_type == 'added' ? set_valid_keyname(graph_data.name)+"_has"+set_valid_keyname(detail[k].type)+"_"+k : "")};
			graph_data.children[i].children[0] 	= {name: detail[k].label, size: 1983, clickable: true, logo: 0, id: (data_type == 'added' ? set_valid_keyname(graph_data.name)+"_"+set_valid_keyname(detail[k].label)+"_"+k : ""), o_type: detail[k].type};
			i++;
		}
	}
	return graph_data;
}

//Drawing collapsible tree graph
function draw_graph(place,json,w,h){
	var root;

	var force = d3.layout.force()
		.linkDistance(80)
		.charge(-120)
		.gravity(.05)
		.size([w, h]);

	var vis = d3.select(place).append("svg:svg")
		.attr("width", w)
		.attr("height", h);

	root = json;
	update();

	function update() {
		var nodes = flatten(root),
			links = d3.layout.tree().links(nodes);

		// Restart the force layout.
		force
			.nodes(nodes)
			.links(links)
			.start();

		// Update the links�
		var link = vis.selectAll("line.link")
			.data(links, function(d) { return d.target.id; });

		// Enter any new links.
		link.enter().insert("svg:line", ".node")
			.attr("class", "link")
			.attr("x1", function(d) { return d.source.x; })
			.attr("y1", function(d) { return d.source.y; })
			.attr("x2", function(d) { return d.target.x; })
			.attr("y2", function(d) { return d.target.y; });

		// Exit any old links.
		link.exit().remove();

		// Update the nodes�
		var node = vis.selectAll("g.node")
			.data(nodes, function(d) { return d.id; })

		node.select("circle")
			.style("fill", color);

		// Enter any new nodes.
		var nodeEnter = node.enter().append("svg:g")
			.attr("class", "node")
			.attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; })
			.attr("style", function(d){ return (d.clickable ? "cursor:pointer" : ""); })
			.on("click", click)
			.call(force.drag);

		nodeEnter.append("svg:circle")
			.attr("r", function(d) { return Math.sqrt(d.size) / 10 || 4.5; })
			.style("fill", color);
		
		nodeEnter.append("image")
			.attr("xlink:href", function(d){ return (d.logo != 0 ? base_url+"assets/attachment/"+d.logo : ""); })
			.attr("x", -8)
			.attr("y", -15)
			.attr("width", 50)
			.attr("height", 50)
			.attr("style", function(d){ return (d.logo != 0 ? "cursor:pointer" : ""); })
			.on('click',function(d){
				if(d.logo != 0){
					$("#sem-graph-img-detail").html("<center><img src='"+(d.logo != 0 ? base_url+"assets/attachment/"+d.logo : "")+"' /></center>");
					var dialog_title = d.logo.split('/');
					set_general_dialog("#sem-graph-img-detail",dialog_title[1],800,500,true);
					$("#sem-graph-img-detail").dialog('open');
				}
			});
		
		nodeEnter.append("svg:text")
			.attr("text-anchor", "middle")
			.attr("dy", ".35em")
			.attr("style", function(d){ return (d.logo != 0 ? "display:none" : "")+";"+(d.size == 10000 ? "font-weight:bold;font-size:12px;fill:red" : ""); })
			.text(function(d) { return d.name; });

		// Exit any old nodes.
		node.exit().remove();

		// Re-select for update.
		link = vis.selectAll("line.link");
		node = vis.selectAll("g.node");

		force.on("tick", function() {
			link.attr("x1", function(d) { return d.source.x; })
				.attr("y1", function(d) { return d.source.y; })
				.attr("x2", function(d) { return d.target.x; })
				.attr("y2", function(d) { return d.target.y; });

			node.attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });
		});
	}

	// Color leaf nodes orange, and packages white or blue.
	function color(d) {
		return d._children ? "#3182bd" : d.children ? "#c6dbef" : "#fd8d3c";
	}

	// Toggle children on click.
	function click(d){	
		if(d.children){ // if collapse
			d._children = d.children;
			d.children  = null;
		}else{ // if not collapse
			if(d.clickable == true){
				progress_bar('open');
				d.children = new Array;
				if (d.o_type == "InnovationCategory") {
					$.post(base_url+"semantics/get_semantic_category_id",{category_name : d.name},function(category_id) { //get category id first
						get_data(semantic_url+'sesGetInstancesDetail'+'?keyword=InnovationCategory_'+category_id+'&type='+d.o_type,function(instances){
							if(instance_detail != 'not_found'){
								set_graph_data_for_category(instances,d,"added",json.name);
								update();
								progress_bar('close');
							}else{
								alert('Detail not found!');
							}
						});
					}, 'json');
				}else{
					get_data(semantic_url+"sesGetInstancesDetail?lang="+(lang == 'melayu' ? 'MY' : 'EN')+"&keyword="+d.name+'&type='+d.o_type,function(instance_detail){
						if(instance_detail != 'not_found'){
							set_graph_data(instance_detail,d,"added",d.o_type);
							update();
							progress_bar('close');
						}else{
							alert('Detail not found!');
						}
					});
				}
			}else{
				d.children  = d._children;
				d._children = null;
			}
		}
		update();
	}

	// Returns a list of all nodes under the root.
	function flatten(root) {
		var nodes = [], i = 0;

		function recurse(node) {
			if (node.children) node.children.forEach(recurse);
				if (!node.id) node.id = ++i;
					nodes.push(node);
		}

		recurse(root);
		return nodes;
	}
}

//Return valid search keyword to pass in url parameter
function set_valid_keyname(keyword){
	//var valid_name 	= valid_name.replace(/ /g,"_");
	var valid_name 	= keyword;
	valid_name		= valid_name.replace(/\,/gi,"");
	valid_name		= valid_name.replace(/&/g,"AND");
	valid_name		= valid_name.replace(/\(/gi,"");
	valid_name		= valid_name.replace(/\)/gi,"");
	valid_name		= valid_name.replace(/\@/gi,"");
	valid_name		= valid_name.replace(/\?/g,"");
	valid_name 		= valid_name.replace(/_/g," ");
	return valid_name;
}

//Return capitalize in each word. ie: set_title('hello world') => 'Hello World'
function set_title(text){
	var i, words, w, result = '';
    words = text.split(' ');
    for (i = 0; i < words.length; i += 1) {
        w = words[i];
        result += w.substr(0,1).toUpperCase() + w.substr(1);
        if (i < words.length - 1) {
            result += ' ';
        }
    }
    return result;
}

//progress bar configuration
function progress_bar(param){
	var object = $("#progress-wrapper");
	if(param == "close"){
		object.hide();
		$("#progress-bar").dialog("close");
	}else if(param == "open"){
		$("#progress-bar").dialog("open");
	}else if(param == "create"){
		object.append("<div id='progress-bar' style='display:none'><center><img src='"+base_url+"assets/img/progress_bar.gif' /></center></div>");
		set_general_dialog("#progress-bar","Processing...",250,90,false);
	}
}

//general dialog
function set_general_dialog(div,title,w,h,button){
	if(button == true){
		$(div).dialog({
			title		: title,
			autoOpen	: false,
			resizable	: false,
			width		: w,
			height		: h,
			modal		: true,
			buttons		: {
				"OK" : function(){
					$(this).dialog('close');
				}
			}
		});
	}else{
		$(div).dialog({title : title, width : w, height : h, modal : true});
	}
}