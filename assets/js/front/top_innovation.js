 /* Load More Innovation when scrolling*/
 $(window).scroll(function () {
	if ($(document).height() <= $(window).scrollTop() + $(window).height()) {
		var curr_num =  parseInt($("#inno-num").val(), 10);
		$.get(base_url+ "site/top_innovation_data/"+ curr_num, function(responses){
			for (var i = 0; i < responses.length; i++) {
				var li_string = "";
				var bg = "";
				if(responses[i].picture.length > 0){
					bg = "background: url('"+ base_url+ "assets/attachment/innovation_picture/"+ responses[i].picture[0].picture+ "')";
				}
				li_string += '<li class="img-bg grid_4 alpha" style="display: none; '+ bg+ '">';
				li_string += '<a href="'+ base_url+ 'site/top_innovation/'+ responses[i].innovation_id+ '">';
				li_string += '<div class="wrap-inno"><div class="inno_no">'+ eval(curr_num+i+1) +'</div>';
				li_string += '<div class="inno_name">'+ responses[i].name +'</div> <div class="clear"></div>';
				li_string += '</a></li>';
				$(li_string).appendTo('.top_innovasi').fadeIn(100);
			}
			$("#inno-num").val(curr_num + responses.length);
			if (responses.length <= 0 ){
				$('.progress-spin').hide();
			}else{
				$('.progress-spin').show();
			}
		},'json');
	}
});