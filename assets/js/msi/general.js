$(document).ready(function(){

	$(".msi-address-geo").blur(function(){
		var address = ""; 
		$(".msi-address-geo").each(function(){
			address += " "+$(this).val();
		});
		
		$.post(msi_url+"site/get_geo_location", { 'address': address }, function(data) {
			if(data.status == false){
				$("#geo-location").val("");
				$("#geo-location").attr("placeholder",data.message);
			}else{
				$("#geo-location").val(data.latitude+","+data.longitude);
			}
		},"json");
	});
});