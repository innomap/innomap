-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 08, 2017 at 10:49 AM
-- Server version: 5.5.25a
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `innomap_v2_migration`
--

-- --------------------------------------------------------

--
-- Table structure for table `portfolio_scope`
--

CREATE TABLE IF NOT EXISTS `portfolio_scope` (
  `scope_id` int(11) NOT NULL AUTO_INCREMENT,
  `portfolio_id` int(11) NOT NULL,
  `criteria_id` int(11) NOT NULL,
  `weightage` double NOT NULL,
  `budget` decimal(10,2) NOT NULL,
  PRIMARY KEY (`scope_id`),
  KEY `portfolio_scope_id` (`portfolio_id`),
  KEY `portfolio_criteria_id` (`criteria_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `portfolio_scope_task`
--

CREATE TABLE IF NOT EXISTS `portfolio_scope_task` (
  `task_id` int(11) NOT NULL AUTO_INCREMENT,
  `scope_id` int(11) NOT NULL,
  `task` varchar(256) NOT NULL,
  `duration` tinyint(3) NOT NULL,
  `weightage` double NOT NULL,
  PRIMARY KEY (`task_id`),
  KEY `scope_criteria_id` (`scope_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `portfolio_scope`
--
ALTER TABLE `portfolio_scope`
  ADD CONSTRAINT `portfolio_scope_ibfk_2` FOREIGN KEY (`criteria_id`) REFERENCES `portfolio_criteria` (`criteria_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `portfolio_scope_ibfk_3` FOREIGN KEY (`portfolio_id`) REFERENCES `portfolio_assignment` (`portfolio_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `portfolio_scope_task`
--
ALTER TABLE `portfolio_scope_task`
  ADD CONSTRAINT `portfolio_scope_task_ibfk_1` FOREIGN KEY (`scope_id`) REFERENCES `portfolio_scope` (`scope_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
